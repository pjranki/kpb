import os
import sys

import os
import sys
import string


class ExpressionParser(object):

    EXPR_TOKENS = [
        'and',
        'not',
        'or',
        '==',
        '!=',
        '<=',
        '>=',
        '<<',
        '>>',
        '[]',
        ':',
        '<',
        '>',
        '(',
        ')',
        '.',
        '+',
        '-',
        '*',
        '/',
        '%',
        '?',
        '&',
        '|',
        '^',
        '~',
        ',',
        ' ',
    ]

    OP_PRECEDENCE_HIGH_TO_LOW = [
        '~',
        '*', '/', '%',
        '+', '-',
        '>>', '<<',
        '&',
        '^', '|',
        '<=', '<', '>', '>=',
        '==', '!=',
        'not', 'and', 'or',
    ]

    BOOL_OP = [
        'and',
        'not',
        'or',
        '==',
        '!=',
        '<=',
        '>=',
        '<',
        '>',
    ]

    @classmethod
    def split_expr_list(cls, expr):
        if isinstance(expr, int):
            expr = str(expr)
        if isinstance(expr, float):
            expr = str(expr)
        expr = expr.replace(']', ')')
        expr = expr.replace('[', '[](')
        assert expr.count('(') == expr.count(')')
        expr = str(expr)
        expr_list = list()
        prev = 0
        i = 0
        while i < len(expr):
            jump = 1
            for token in cls.EXPR_TOKENS:
                value = expr[i: i + len(token)].lower()
                if len(value) != len(token):
                    continue
                if value != token:
                    continue
                if value in ['and', 'or', 'not']:
                    prefix = expr[i - 1: i] if i > 0 else ''
                    suffix = expr[i + len(token): i + len(token) + 1]
                    if prefix in (string.ascii_letters + string.digits + '_'):
                        continue
                    if suffix in (string.ascii_letters + string.digits + '_'):
                        continue
                if value == ':' and expr[i + 1: i + 2].lower() == ':':
                    jump = 2
                    continue
                expr_list.append(expr[prev: i])
                if token == ':':
                    expr_list.append(')')
                expr_list.append(token)
                if token == '?':
                    expr_list.append('(')
                i += len(token)
                prev = i
                jump = 0
                break
            i += jump
        if prev != len(expr):
            expr_list.append(expr[prev:])
        new_expr_list = list()
        i = 0
        while i < len(expr_list):
            expr = expr_list[i]
            expr_next = expr_list[i + 1] if i + 1 < len(expr_list) else None
            if expr == '-' and expr_next and len(expr_next) > 0:
                digit_text = expr_next[0].strip()
                if digit_text in string.digits:
                    expr += expr_next
                    i += 1
            if expr.strip() != '':
                new_expr_list.append(expr)
            i += 1
        expr_list = new_expr_list
        if len(expr_list) >= 3:
            # don't split tokens that are part of an enumerator name e.g. 'and::and' -> ['and', '::', 'and']
            new_expr_list = list()
            skip_next = False
            for i in range(0, len(expr_list)):
                if skip_next:
                    skip_next = False
                    continue
                expr = expr_list[i]
                if expr.startswith('::'):
                    # remove previous expr
                    assert len(new_expr_list) != 0
                    prev_expr = new_expr_list[len(new_expr_list) - 1]
                    new_expr_list = new_expr_list[:-1]
                    # prepend to this expr
                    expr = prev_expr + expr
                if expr.endswith('::'):
                    # remove next expr
                    assert (i + 1) != len(expr_list)
                    next_expr = expr_list[i + 1]
                    skip_next = True
                    # append to this expr
                    expr = expr + next_expr
                new_expr_list.append(expr)
            expr_list = new_expr_list
        if len(expr_list) >= 3:
            # join integers where they represent floats
            new_expr_list = list()
            skip_next = False
            for i in range(0, len(expr_list)):
                if skip_next:
                    skip_next = False
                    continue
                dot = expr_list[i]
                if (dot != '.') or (i == 0) or (i == (len(expr_list) - 1)):
                    new_expr_list.append(expr_list[i])
                    continue
                whole_part = expr_list[i - 1]
                valid_integer = True
                try:
                    whole_part = int(whole_part)
                except ValueError:
                    valid_integer = False
                if not valid_integer:
                    new_expr_list.append(expr_list[i])
                    continue
                fraction_part = expr_list[i + 1]
                valid_integer = True
                try:
                    fraction_part = int(fraction_part)
                except ValueError:
                    valid_integer = False
                if not valid_integer:
                    new_expr_list.append(expr_list[i])
                    continue
                # change previous integer to the float
                new_expr_list[i - 1] = '{}{}{}'.format(whole_part, dot, fraction_part)
                # don't add the '.' to the expression list
                pass
                # skip the next integer that was used for the float fraction part
                skip_next = True
            expr_list = new_expr_list
        return expr_list

    @classmethod
    def create_expr_tree(cls, expr_list):
        # TODO: ',' and function calls
        assert ',' not in expr_list

        # brackets
        new_expr_list = list()
        sub_expr_list = list()
        depth = 0
        for expr in expr_list:
            if depth > 0:
                if expr == '(':
                    depth += 1
                if expr == ')':
                    depth -= 1
                if depth == 0:
                    new_expr_list.append(cls.create_expr_tree(sub_expr_list))
                    sub_expr_list = list()
                    continue
                sub_expr_list.append(expr)
                continue
            assert expr != ')'
            if expr == '(':
                depth += 1
                continue
            new_expr_list.append(expr)
        assert depth == 0
        expr_list = new_expr_list

        # ternary conditional
        if '?' in expr_list:
            op = '?'
            index = expr_list.index(op)
            arg0 = cls.create_expr_tree(expr_list[:index])
            arg1 = expr_list[index + 1]
            assert isinstance(arg1, list)
            assert expr_list[index + 2] == ':'
            arg2 = cls.create_expr_tree(expr_list[index + 3:])
            expr_list = [op, arg0, arg1, arg2]

        # operator precendence
        for op in reversed(cls.OP_PRECEDENCE_HIGH_TO_LOW):
            if op in expr_list:
                index = expr_list.index(op)
                arg0 = cls.create_expr_tree(expr_list[:index])
                arg1 = cls.create_expr_tree(expr_list[index + 1:])
                expr_list = [op, arg0, arg1]

        # member and array access
        if '[]' in expr_list:
            op = '[]'
            index = expr_list.index(op)
            arg0 = cls.create_expr_tree(expr_list[:index])
            arg1 = expr_list[index + 1]    # this works because we converted '[...]' -> '[](....)'
            assert isinstance(arg1, list)  # this works because we converted '[...]' -> '[](....)'
            sub_expr_list = [op, arg0, arg1]
            remainder = expr_list[index + 2:]
            new_expr_list = list()
            new_expr_list.append([op, arg0, arg1])
            new_expr_list.extend(remainder)
            if len(remainder) > 0:
                new_expr_list = cls.create_expr_tree(new_expr_list)
            expr_list = new_expr_list
        if '.' in expr_list:
            rindex = -1
            for i in range(0, len(expr_list)):
                if expr_list[i] == '.':
                    rindex = i
            assert rindex >= 0
            op = '.'
            arg0 = cls.create_expr_tree(expr_list[:rindex])
            arg1 = cls.create_expr_tree(expr_list[rindex + 1:])
            expr_list = [op, arg0, arg1]

        # done
        return expr_list

    @classmethod
    def reduce_expr_tree(cls, expr_list):
        if not isinstance(expr_list, list):
            return expr_list
        new_expr_list = list()
        for expr in expr_list:
            expr = cls.reduce_expr_tree(expr)
            if not isinstance(expr, list):
                new_expr_list.append(expr)
                continue
            if len(expr) == 0:
                continue
            if len(expr) == 1:
                new_expr_list.append(expr[0])
                continue
            new_expr_list.append(expr)
        expr_list = new_expr_list
        if len(expr_list) == 2 and expr_list[0] == '.':
            # HACK: not sure why this breaks, this is my workaround for now
            assert isinstance(expr_list[1], list)
            assert len(expr_list[1]) == 2
            expr_list = [expr_list[0], expr_list[1][0], expr_list[1][1]]
        return expr_list

    @classmethod
    def flatten_expr_tree(cls, expr_tree):
        if not isinstance(expr_tree, list):
            return str(expr_tree)
        assert len(expr_tree) > 0
        if len(expr_tree) == 1:
            return cls.flatten_expr_tree(expr_tree[0])
        op = expr_tree[0]
        args = [cls.flatten_expr_tree(arg) for arg in expr_tree[1:]]
        if op in ['and', 'or', '==', '!=', '<=', '>=', '<', '>', '<<', '>>', '+', '-', '*', '/', '%', '&', '|', '^']:
            assert len(args) == 2
            return '({}) {} ({})'.format(args[0], op, args[1])
        if op == 'not':
            assert len(args) == 1
            return 'not ({})'.format(args[0])
        if op == '~':
            assert len(args) == 1
            return '~({})'.format(args[0])
        if op == '[]':
            assert len(args) == 2
            return '{}[{}]'.format(args[0], args[1])
        if op == '.':
            assert len(args) == 2
            return '{}.{}'.format(args[0], args[1])
        if op == '?':
            assert len(args) == 3
            return '({}) ? ({}) : ({})'.format(args[0], args[1], args[2])
        raise NotImplementedError("operator '{}'".format(op))
