import os
import sys

def to_camel_case(text):
    if text.startswith('~'):
        return '~' + to_camel_case(text[1:])
    return ''.join(x.capitalize() or '_' for x in text.split('_'))


class TextHelper(object):

    def __init__(self):
        self.text = ''
        self.tabs = list()
        self.default_tab = ' ' * 4

    def __str__(self):
        return self.text

    def tab(self, tab=None):
        self.tabs.append(tab)

    def shift_tab(self):
        if len(self.tabs) > 0:
            self.tabs = self.tabs[:-1]

    def nl(self):
        self.text += '\n'

    @property
    def indent(self):
        line = ''
        for tab in self.tabs:
            if not tab:
                line += self.default_tab
            elif isinstance(tab, int):
                line += ' ' * tab
            elif isinstance(tab, str):
                line += tab
            else:
                raise NotImplementedError
        return line

    def line(self, text):
        text = '{}{}\n'.format(self.indent, text)
        if text.strip() == '':
            text = '\n'
        self.text += text

    def block(self, text):
        lines = text.split('\n')
        if lines[-1].strip() == '':
            lines = lines[:-1]
        for line in lines:
            self.line(line)

    def format(self, line, *args, **kwargs):
        self.line(line.format(*args, **kwargs))
