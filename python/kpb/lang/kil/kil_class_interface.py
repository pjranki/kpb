import os
import sys

from .kil_types import *
from .kil_ksy_library import *

class KilClassInterface(object):

    def __init__(self, root_name=None, class_name=None, class_variable=None, ksy=None):
        self.root_name = root_name
        self.class_name = class_name
        self.class_variable = class_variable
        self.ksy = ksy

    @property
    def type_(self):
        return KilTypeClass(name=self.class_name)

    def this(self, mutable=True):
        assert self.class_variable
        if not self.class_variable.mutable and mutable:
            raise NotImplementedError('you are trying to mutate an immutable class instance')
        return self.class_variable

    def root(self, mutable=True):
        assert mutable == False  # getting a mutable parent is not implemented (and not needed as far as I can see)
        method_call = KilMethodCall()
        method_call.class_type = self.type_
        method_call.mutable = mutable
        method_call.public = True
        method_call.hint_treat_as_member_variable = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'root'
        method_call.decl_variable.type_ = KilTypeClass(name=self.root_name)
        method_call.decl_variable.mutable = mutable
        method_call.decl_variable.reference = True
        method_call.decl_variable.list_ = False
        method_call.arg_variables = []
        method_call = KilOperation(self.this(mutable), '.', method_call)
        return method_call

    def set_root(self, root_variable):
        assert isinstance(root_variable.type_, KilTypeClass)
        method_call = KilMethodCall()
        method_call.class_type = self.type_
        method_call.mutable = True
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'set_root'
        method_call.decl_variable.type_ = KilTypeNone()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = [root_variable]
        method_call = KilOperation(self.this(mutable=True), '.', method_call)
        return method_call

    def parent(self, mutable=True):
        assert mutable == False  # getting a mutable parent is not implemented (and not needed as far as I can see)
        ksy_class_dict = self.ksy.find_ksy_parent_class_dict_from_class_name(self.root_name, self.class_name)
        parent_class_name = ksy_class_dict['id']
        parent_class_type = KilTypeClass(name=parent_class_name)
        method_call = KilMethodCall()
        method_call.class_type = self.type_
        method_call.mutable = mutable
        method_call.public = True
        method_call.hint_treat_as_member_variable = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'parent'
        method_call.decl_variable.type_ = KilTypeNone()
        method_call.decl_variable.mutable = mutable
        method_call.decl_variable.reference = True
        method_call.decl_variable.list_ = False
        method_call.arg_variables = []
        method_call = KilOperation(self.this(mutable), '.', method_call)
        method_call = KilTypecast(method_call, type_=parent_class_type)
        return method_call

    def parent_type(self, mutable=True):
        method_call = KilMethodCall()
        method_call.class_type = self.type_
        method_call.mutable = mutable
        method_call.public = True
        method_call.hint_treat_as_member_variable = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'parent_type'
        method_call.decl_variable.type_ = KilTypeInteger(signed=False, bits=32)
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = []
        method_call = KilOperation(self.this(mutable), '.', method_call)
        return method_call

    def set_parent(self, parent_variable):
        assert isinstance(parent_variable.type_, KilTypeClass)
        parent_class_name = parent_variable.type_.name
        parent_index_map = self.ksy.ksy_parent_index_map(self.root_name, self.class_name)
        parent_index = parent_index_map[parent_class_name]
        parent_class_type = KilTypeClass(name=parent_class_name)
        method_call = KilMethodCall()
        method_call.class_type = self.type_
        method_call.mutable = True
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'set_parent'
        method_call.decl_variable.type_ = KilTypeBoolean()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = [KilTypecast(value=parent_variable, type_=KilTypeNone()), KilLiteral(parent_index, KilTypeInteger(signed=False, bits=32))]
        method_call = KilOperation(self.this(mutable=True), '.', method_call)
        return method_call

    @property
    def parents(self):
        parent_index_map = self.ksy.ksy_parent_index_map(self.root_name, self.class_name)
        parent_name_map = {v: k for k, v in parent_index_map.items()}
        parents = list()
        for parent_index in parent_index_map.values():
            parent_class_name = parent_name_map[parent_index]
            parent_type = KilTypeClass(name=parent_class_name)
            variable = KilVariable()
            variable.name = '_parent'
            if len(parent_index_map.values()) == 1:
                variable.type_ = parent_type
            else:
                variable.type_ = KilTypeNone()
            variable.mutable = True
            variable.reference = True
            variable.list_ = False
            parent = KilOperation(self.this(mutable=True), '.', variable)
            if len(parent_index_map.values()) != 1:
                parent = KilTypecast(value=parent, type_=parent_type)
            parents.append((parent_index, parent))
        return parents

    def clear(self):
        method_call = KilMethodCall()
        method_call.class_type = self.type_
        method_call.mutable = True
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'clear'
        method_call.decl_variable.type_ = KilTypeNone()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = list()
        method_call = KilOperation(self.this(mutable=True), '.', method_call)
        return method_call

    def read(self, io_variable):
        method_call = KilMethodCall()
        method_call.class_type = self.type_
        method_call.mutable = True
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'read'
        method_call.decl_variable.type_ = KilTypeBoolean()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = [io_variable]
        method_call = KilOperation(self.this(mutable=True), '.', method_call)
        return method_call

    def write(self, io_variable):
        method_call = KilMethodCall()
        method_call.class_type = self.type_
        method_call.mutable = False
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'write'
        method_call.decl_variable.type_ = KilTypeNone()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = [io_variable]
        method_call = KilOperation(self.this(mutable=False), '.', method_call)
        return method_call

    def size(self):
        method_call = KilMethodCall()
        method_call.class_type = self.type_
        method_call.mutable = False
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'size'
        method_call.decl_variable.type_ = KilTypeSize()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = list()
        method_call = KilOperation(self.this(mutable=False), '.', method_call)
        return method_call

    def randomize(self):
        method_call = KilMethodCall()
        method_call.class_type = self.type_
        method_call.mutable = True
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'randomize'
        method_call.decl_variable.type_ = KilTypeBoolean()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = list()
        method_call = KilOperation(self.this(mutable=False), '.', method_call)
        return method_call

    def update(self):
        method_call = KilMethodCall()
        method_call.class_type = self.type_
        method_call.mutable = True
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'update'
        method_call.decl_variable.type_ = KilTypeBoolean()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = []
        method_call = KilOperation(self.this(mutable=True), '.', method_call)
        return method_call

    def update_registers(self):
        regs = list()
        count = self.ksy.ksy_class_id_count(self.root_name, self.class_name)
        update_enabled_reg_count = int(count / 32)
        if (update_enabled_reg_count * 32) < count:
            update_enabled_reg_count += 1
        for i in range(0, update_enabled_reg_count):
            variable = KilVariable()
            variable.name = '_update_enabled_reg_{}'.format(i)
            variable.type_ = KilTypeInteger(signed=False, bits=32)
            variable.mutable = True
            variable.reference = False
            variable.list_ = False
            reg = KilOperation(self.this(mutable=True), '.', variable)
            regs.append(reg)
        return regs

    def update_and_backup_registers(self):
        reg_and_backup_list = list()
        regs = self.update_registers()
        for i in range(0, len(regs)):
            reg_backup = KilVariable()
            reg_backup.name = '_update_enabled_reg_{}_bkup'.format(i)
            reg_backup.type_ = KilTypeInteger(signed=False, bits=32)
            reg_backup.mutable = True
            reg_backup.reference = False
            reg_backup.list_ = False
            reg_and_backup_list.append((regs[i], reg_backup))
        return reg_and_backup_list

    def update_register(self, class_id, mutable=True):
        assert class_id is not None
        variable = KilVariable()
        variable.name = '_update_enabled_reg_{}'.format(class_id >> 5)
        variable.type_ = KilTypeInteger(signed=False, bits=32)
        variable.mutable = True
        variable.reference = False
        variable.list_ = False
        reg = KilOperation(self.this(mutable=mutable), '.', variable)
        return reg

    def update_backup_variable(self, class_id):
        assert class_id is not None
        reg_backup = KilVariable()
        reg_backup.name = '_update_enabled_id_{}_bkup'.format(class_id)
        reg_backup.type_ = KilTypeBoolean()
        reg_backup.mutable = True
        reg_backup.reference = False
        reg_backup.list_ = False
        return reg_backup

    def update_mask(self, class_id=None, flip_bits=False):
        if class_id is None:
            if flip_bits:
                mask = KilLiteral(0x00000000, KilTypeInteger(signed=False, bits=32))
            else:
                mask = KilLiteral(0xffffffff, KilTypeInteger(signed=False, bits=32))
        else:
            value = 1 << (class_id & 0x1f)
            if flip_bits:
                value = (~value) & 0xffffffff
            mask = KilLiteral(value, KilTypeInteger(signed=False, bits=32))
        return mask

    def update_enabled(self, class_id):
        assert class_id is not None
        reg = self.update_register(class_id, mutable=False)
        mask = self.update_mask(class_id)
        return KilOperation(KilOperation(reg, '&', mask), '==', KilLiteral(0, KilTypeInteger(signed=False, bits=32)))

    def update_disable_block(self, class_id_list=None, backup=False):
        block = KilBlock()
        if class_id_list is None:
            mask = self.update_mask()
            if backup:
                for reg, reg_backup in self.update_and_backup_registers():
                    block.children.extend([
                        KilOperation(KilDeclaration(reg_backup), '=', reg),
                        KilOperation(reg, '=', mask),
                    ])
            else:
                for reg in self.update_registers():
                    block.children.extend([
                        KilOperation(reg, '=', mask),
                    ])
        else:
            assert isinstance(class_id_list, list)
            for class_id in class_id_list:
                reg = self.update_register(class_id)
                mask = self.update_mask(class_id)
                if backup:
                    enabled = self.update_backup_variable(class_id)
                    block.children.extend([
                        KilOperation(KilDeclaration(enabled), '=', self.update_enabled(class_id)),
                    ])
                block.children.extend([
                    KilOperation(reg, '=', KilOperation(reg, '|', mask)),
                ])
        return block

    def update_enable_block(self, class_id_list=None, restore=False):
        block = KilBlock()
        if class_id_list is None:
            mask_flipped = self.update_mask(flip_bits=True)
            if restore:
                for reg, reg_backup in self.update_and_backup_registers():
                    block.children.extend([
                        KilOperation(reg, '=', reg_backup),
                    ])
            else:
                for reg in self.update_registers():
                    block.children.extend([
                        KilOperation(reg, '=', mask_flipped),
                    ])
        else:
            assert isinstance(class_id_list, list)
            for class_id in class_id_list:
                reg = self.update_register(class_id)
                mask_flipped = self.update_mask(class_id, flip_bits=True)
                if restore:
                    mask = self.update_mask(class_id)
                    enabled = self.update_backup_variable(class_id)
                    block.children.extend([
                        KilOperation(reg, '=', KilOperation(enabled, '?',
                            KilOperation(reg, '&', mask_flipped),
                            KilOperation(reg, '|', mask)
                        )),
                    ])
                else:
                    block.children.extend([
                        KilOperation(reg, '=', KilOperation(reg, '&', mask_flipped)),
                    ])
        return block
