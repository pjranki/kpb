import os
import sys

from .kil_types import *
from .kil_ksy_library import *
from .kil_member_interface import *


class KsyInstanceToMemberBridge(object):

    def __init__(self, root_name, class_name, instance_name, ksy_member_dict, ksy):
        self.root_name = root_name
        self.class_name = class_name
        self.instance_name = instance_name
        self.ksy_member_dict = ksy_member_dict
        self.ksy = ksy

    def __getattribute__(self, name):
        pass_through = ('root_name', 'class_name', 'instance_name', 'ksy_member_dict', 'ksy','find_ksy_member_dict_from_member_name',)
        if name in pass_through:
            return super(KsyInstanceToMemberBridge, self).__getattribute__(name)
        return self.ksy.__getattribute__(name)

    def find_ksy_member_dict_from_member_name(self, root_name, class_name, member_name):
        if root_name != self.root_name or class_name != self.class_name or member_name != self.instance_name:
            return self.ksy.find_ksy_member_dict_from_member_name(root_name, class_name, member_name)
        return self.ksy_member_dict


class KilInstanceInterface(object):

    def __init__(self, root_name, class_name, instance_name, class_variable, expr_compiler, ksy):
        self.root_name = root_name
        self.class_name = class_name
        self.instance_name = instance_name
        self.class_variable = class_variable
        self.expr_compiler = expr_compiler
        self.ksy = ksy

    @property
    def ksy_value_instance_dict(self):
        return self.ksy.find_ksy_value_instance_dict_from_name(self.root_name, self.class_name, self.instance_name)

    @property
    def ksy_member_dict(self):
        ksy_member_dict = dict()
        ksy_member_dict['id'] = self.instance_name
        type_ = self.type_
        if isinstance(type_, KilTypeBoolean):
            ksy_member_dict['type'] = 'bool'
        elif isinstance(type_, KilTypeInteger):
            ksy_member_dict['type'] = type_.ksy_type
            ksy_member_dict['endian'] = 'be'
        elif isinstance(type_, KilTypeClass):
            ksy_member_dict['type'] = type_.name
        elif isinstance(type_, KilTypeString):
            ksy_member_dict['type'] = 'strz'
            ksy_member_dict['encoding'] = type_.ksy_encoding
        elif isinstance(type_, KilTypeFloat):
            raise NotImplementedError
        elif isinstance(type_, KilTypeBuffer):
            ksy_member_dict['size'] = '1'
        else:
            raise NotImplementedError('instance type {}'.format(repr(type_)))
        if self.list_:
            ksy_member_dict['repeat'] = 'expr'
            ksy_member_dict['repeat-expr'] = '1'
        return ksy_member_dict

    @property
    def class_type(self):
        return KilTypeClass(name=self.class_name)

    @property
    def list_(self):
        cache_list = self.ksy.cache_lookup_class_member_is_list(self.class_name, self.instance_name)
        if cache_list is not None:
            return cache_list
        value = self.expr_compiler.compile_expr(self.ksy_value_instance_dict['value'])
        self.ksy.cache_class_member_type(self.class_name, self.instance_name, value.type_)
        self.ksy.cache_class_member_is_list(self.class_name, self.instance_name, value.list_)
        return value.list_

    @property
    def type_(self):
        cache_type = self.ksy.cache_lookup_class_member_type(self.class_name, self.instance_name)
        if cache_type is not None:
            return cache_type
        value = self.expr_compiler.compile_expr(self.ksy_value_instance_dict['value'])
        self.ksy.cache_class_member_type(self.class_name, self.instance_name, value.type_)
        self.ksy.cache_class_member_is_list(self.class_name, self.instance_name, value.list_)
        return value.type_

    def get(self):
        ksy = KsyInstanceToMemberBridge(self.root_name, self.class_name, self.instance_name, self.ksy_member_dict, self.ksy)
        this_member = KilMemberInterface(self.root_name, self.class_name, self.instance_name, self.class_variable, ksy)
        return this_member.get()

    def set(self, value_variable):
        ksy = KsyInstanceToMemberBridge(self.root_name, self.class_name, self.instance_name, self.ksy_member_dict, self.ksy)
        this_member = KilMemberInterface(self.root_name, self.class_name, self.instance_name, self.class_variable, ksy)
        return this_member.set(value_variable)

    def atindex(self):
        raise NotImplementedError
