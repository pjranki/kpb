import os
import sys

from .kil_types import *
from .kil_ksy_library import *
from .kil_io_interface import *
from .kil_expr_compiler import *
from .kil_member_generic_compiler import *
from .kil_class_interface import *
from .kil_process_interface import *


class KilMemberClassCompiler(KilMemberGenericCompiler):

    def __init__(self, *args, **kwargs):
        super(KilMemberClassCompiler, self).__init__(*args, **kwargs)
        self.cache_type = None

    def compile_methods(self):
        list_ = KilMemberGenericInterface(self.root_name, self.class_name, self.member_name, None, self.ksy).list_
        methods = list()
        if not list_:
            methods.append(self.compile_getter_method())
            methods.append(self.compile_size_method())
        else:
            methods.append(self.compile_getlist_method())
            methods.append(self.compile_count_method())
            methods.append(self.compile_atindex_method())
            methods.append(self.compile_sizeat_method())
        methods.append(self.compile_mutable_method())
        if list_:
            methods.append(self.compile_mutable_atindex_method())
        methods.append(self.compile_clear_method())
        methods.append(self.compile_has_method())
        methods.append(self.compile_randomize_method())
        return methods

    def compile_getter_method(self):
        method = super(KilMemberClassCompiler, self).compile_getter_method()
        method.decl_variable.variable.reference = True
        if isinstance(method.code_block.children[-1].return_, KilVariable):
            # HACK: for tmp variable used in process-value
            method.code_block.children[-1].return_.reference = True
        return method

    def compile_mutable_method(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = True
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        method = KilMethod()
        method.class_type = this_member.class_type
        method.mutable = True
        method.public = True
        method.decl_variable = KilVariable()
        if not this_member.list_:
            method.decl_variable.name = 'mutable_' + self.member_name
        else:
            method.decl_variable.name = 'add_' + self.member_name
        method.decl_variable.type_ = this_member.type_
        method.decl_variable.mutable = True
        method.decl_variable.reference = True
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        method.arg_variables = list()
        value_variable = KilVariable()
        value_variable.name = '_value'
        value_variable.type_ = this_member.type_
        value_variable.mutable = True
        value_variable.reference = True
        value_variable.list_ = False
        if not this_member.list_:
            create_member = KilOperation(this_member.value, '=', KilConstructorCall(class_type=this_member.type_))
            decl_value_variable = KilOperation(KilDeclaration(value_variable), '=', this_member.value)
            method.code_block = KilBlock([
                create_member,
                KilIf(expr=this_member.value, false_body=KilBlock([
                    KilReturn(KilLiteral(None, KilTypeNone())),
                ])),
                decl_value_variable
            ])
        else:
            add_member = KilOperation(this_member.value, 'add', KilLiteral(None, this_member.type_))
            last_index = KilOperation(KilOperation(this_member.value, 'len'), '-', KilLiteral(1, KilTypeSize()))
            last_item = KilOperation(this_member.value, '[]', last_index)
            decl_value_variable = KilOperation(KilDeclaration(value_variable), '=', last_item)
            method.code_block = KilBlock([
                KilIf(expr=add_member, false_body=KilBlock([
                    KilReturn(KilLiteral(None, KilTypeNone())),
                ])),
                decl_value_variable
            ])
        i = 0
        for child in self.compile_class_initialize_param_values().children:
            method.code_block.children.insert(i, child)
            i += 1
        method.code_block.children.extend(self.compile_class_initialize_block(value_variable).children)
        process_value_expr = this_member.ksy_member_dict.get('-process-value', None)
        if process_value_expr:
            process_value = KilProcessValueInterface(self.member_name, process_value_expr)
            method.code_block.children.extend([
                process_value.constructor(self.expr_compiler(this_mutable=True)),
                KilOperation(value_variable, '=', process_value.setter(value_variable)),
                process_value.destructor()
            ])
        method.code_block.children.append(KilReturn(value_variable))
        return method

    def compile_mutable_atindex_method(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = True
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        method = KilMethod()
        method.class_type = this_member.class_type
        method.mutable = True
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = 'mutable_' + self.member_name
        method.decl_variable.type_ = this_member.type_
        method.decl_variable.mutable = True
        method.decl_variable.reference = True
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        index_variable = KilVariable()
        index_variable.name = '_index'
        index_variable.type_ = KilTypeSize()
        index_variable.mutable = False
        index_variable.reference = False
        index_variable.list_ = False
        method.arg_variables = [KilDeclaration(index_variable)]
        value_variable = KilVariable()
        value_variable.name = '_value'
        value_variable.type_ = this_member.type_
        value_variable.mutable = True
        value_variable.reference = True
        value_variable.list_ = False
        count_variable = KilOperation(this_member.value, 'len')
        item_atindex = KilOperation(this_member.value, '[]', index_variable)
        decl_value_variable = KilOperation(KilDeclaration(value_variable), '=', item_atindex)
        method.code_block = KilBlock([
            KilIf(expr=KilOperation(index_variable, '>=', count_variable), true_body=KilBlock([
                KilReturn(KilLiteral(None, KilTypeNone())),
            ])),
            decl_value_variable
        ])
        i = 0
        for child in self.compile_class_initialize_param_values().children:
            method.code_block.children.insert(i, child)
            i += 1
        method.code_block.children.extend(self.compile_class_initialize_block(value_variable).children)
        method.code_block.children.append(KilReturn(value_variable))
        return method

    def compile_clear_method(self):
        method = super(KilMemberClassCompiler, self).compile_clear_method()

        # need the this variable
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = True
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)

        # if the member does not take parameters, it will always be the same - so no need to create an instance
        if 0 == len(this_member.param_expr_args):
            return method

        # don't need to do this if we are clearing a list (there are no objects to initialize if the list is empty)
        # TODO: this might still need to be implemented for lists e.g. empty list, but need to write X number of classes to stream
        if this_member.list_:
            return method

        # need to rebuild the clear block so it will initialize the class after a clear
        # this is because different parameters can be provide to different classes
        fallback_clear_block = method.code_block
        method.code_block = KilBlock([])

        # allocate an instance of the class
        value_variable = KilVariable()
        value_variable.name = '_value'
        value_variable.type_ = this_member.type_
        value_variable.mutable = True
        value_variable.reference = True
        value_variable.list_ = False
        method.code_block.children.append(KilOperation(KilDeclaration(value_variable), '=', this_member.mutable()))

        # clear the class and set the parameters
        this_class = KilClassInterface(self.root_name, self.class_name, this, self.ksy)
        this_member_class = KilClassInterface(self.root_name, this_member.type_.name, class_variable=value_variable, ksy=self.ksy)
        clear_and_init_block = KilBlock([])
        clear_and_init_block.children.append(this_member_class.clear())
        i = 0
        for child in self.compile_class_initialize_param_values().children:
            clear_and_init_block.children.insert(i, child)
            i += 1
        clear_and_init_block.children.extend(self.compile_class_initialize_block(value_variable, clear_call=True).children)
        clear_and_init_block.children.append(this_class.update())

        # do a full clear and init if mutable worked, otherwise do a vanilla clear
        method.code_block.children.append(KilIf(expr=value_variable,
            true_body=clear_and_init_block,
            false_body=fallback_clear_block))

        # done
        return method

    def compile_class_initialize_param_values(self):
        block = KilBlock([])

        # This function exists so that parameters can be calulcated BEFORE the instance is constructed.
        # Think of the case where the list size changes - but the list size is used as a constructor parameter for the instance.

        # working with a mutable member
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = True
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)

        # set parameters on new class
        param_class_name = this_member.type_.name
        param_index = 0
        for param_expr_arg in this_member.param_expr_args:
            ksy_param_dict = self.ksy.find_ksy_member_dict_from_param_index(self.root_name, param_class_name, param_index)
            this_param_member = KilMemberInterface(self.root_name, param_class_name, ksy_param_dict['id'], None, self.ksy)
            param_index += 1

        # done
        return block

    def compile_class_initialize_block(self, value_variable, clear_call=False):
        block = KilBlock([])

        # working with a mutable member
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = True
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        child_class_name = this_member.type_.name
        member_class_instance = KilClassInterface(self.root_name, child_class_name, value_variable, self.ksy)

        # only set the root if its not the root and belongs to the same root/import
        if self.root_name != child_class_name and self.ksy.is_class_name_in_root_name(self.root_name, child_class_name):
            if self.root_name == self.class_name:
                # if the owner class is the root, we can just use the 'this' member
                root = this
            else:
                # if owner class is not the root class, we can get its root class from the '_root' member
                root_variable = KilVariable()
                root_variable.name = '_root'
                root_variable.type_ = KilTypeClass(name=self.root_name)
                root_variable.mutable = True
                root_variable.reference = True
                root_variable.list_ = False
                root = KilOperation(this, '.', root_variable)
            block.children.append(member_class_instance.set_root(root))

        # set parent on new class
        if not clear_call:
            block.children.extend([
                KilIf(expr=member_class_instance.set_parent(this), false_body=KilBlock([
                    KilReturn(KilLiteral(None, KilTypeNone())),
                ])),
            ])
        else:
            block.children.append(member_class_instance.set_parent(this))

        # set parameters on new class
        param_class_name = this_member.type_.name
        param_index = 0
        for param_expr_arg in this_member.param_expr_args:
            ksy_param_dict = self.ksy.find_ksy_member_dict_from_param_index(self.root_name, param_class_name, param_index)
            this_param_member = KilMemberInterface(self.root_name, param_class_name, ksy_param_dict['id'], value_variable, self.ksy)
            param_value = this_param_member.get()
            expr_compiler = self.expr_compiler(this_mutable=True)
            expr_compiler.pre_param_list_variable = expr_compiler.class_variable
            param_variable = expr_compiler.compile_expr(param_expr_arg, this_param_member.type_)
            if not clear_call:
                block.children.extend([
                    KilIf(expr=this_param_member.set(param_variable), false_body=KilBlock([
                        KilReturn(KilLiteral(None, KilTypeNone())),
                    ])),
                ])
            else:
                block.children.append(this_param_member.set(param_variable))
            param_index += 1

        # changes have been made to this member, update/set everything else as needed
        # NOTE: we do not do this on clear as set only applies when a change is made
        if not clear_call:
            block.children.extend(self.compile_update_block().children)

        # done
        return block

    def compile_size_method(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = False
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        this_variable = this_member.get()
        process_expr = this_member.ksy_member_dict.get('process', None)

        # define the method
        method = KilMethod()
        method.class_type = this_member.class_type
        method.mutable = False
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = self.member_name + '_calc_size'
        method.decl_variable.type_ = KilTypeSize()
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        method.code_block = KilBlock([])

        # method body code to determine the serialized size of the member
        if process_expr is None:
            # call size method on member class
            method_call = KilMethodCall()
            method_call.class_type = this_variable.type_
            method_call.mutable = False
            method_call.public = True
            method_call.decl_variable = KilVariable()
            method_call.decl_variable.name = 'size'
            method_call.decl_variable.type_ = KilTypeSize()
            method_call.decl_variable.mutable = False
            method_call.decl_variable.reference = False
            method_call.decl_variable.list_ = False
            method_call.arg_variables = list()
            method_call = KilOperation(this_variable, '.', method_call)
            method.code_block.children.append(KilReturn(method_call))
        else:
            # serialize the member and encode it using 'process' to determine serialized size
            # e.g we are after the 'compressed' size instead of the 'raw' size
            process = KilProcessInterface(self.member_name, process_expr)
            _io = KilIoInterface(reference=False)
            size_variable = KilVariable('_size', KilTypeSize())
            this_member_class = KilClassInterface(self.root_name, this_variable.type_.name, class_variable=this_variable, ksy=self.ksy)
            method.code_block.children.extend([
                _io.constructor(),
                process.constructor(self.expr_compiler(this_mutable=False)),
                KilIf(expr=this_member_class.write(_io.this), false_body=KilBlock([
                    process.destructor(),
                    _io.destructor(),
                    KilReturn(KilLiteral(0, KilTypeSize()))
                ])),
                KilOperation(KilDeclaration(size_variable), '=', process.encode_size(_io.this)),
                KilIf(expr=_io.ok(), false_body=KilBlock([
                    process.destructor(),
                    _io.destructor(),
                    KilReturn(KilLiteral(0, KilTypeSize()))
                ])),
                process.destructor(),
                _io.destructor(),
                KilReturn(size_variable)
            ])

        return method

    def compile_sizeat_method(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = False
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        index_variable = KilVariable('_index', KilTypeSize())
        this_variable = this_member.atindex(index_variable)
        process_expr = this_member.ksy_member_dict.get('process', None)

        # define the method
        method = KilMethod()
        method.class_type = this_member.class_type
        method.mutable = False
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = self.member_name + '_calc_size_at'
        method.decl_variable.type_ = KilTypeSize()
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        method.arg_variables = [KilDeclaration(index_variable)]
        method.code_block = KilBlock([])

        # method body code to determine the serialized size of the member
        if process_expr is None:
            # call size method on member class (at the index)
            method_call = KilMethodCall()
            method_call.class_type = this_variable.type_
            method_call.mutable = False
            method_call.public = True
            method_call.decl_variable = KilVariable()
            method_call.decl_variable.name = 'size'
            method_call.decl_variable.type_ = KilTypeSize()
            method_call.decl_variable.mutable = False
            method_call.decl_variable.reference = False
            method_call.decl_variable.list_ = False
            method_call.arg_variables = list()
            method_call = KilOperation(this_variable, '.', method_call)
            method.code_block.children.append(KilReturn(method_call))
        else:
            # serialize the member and encode it using 'process' to determine serialized size
            # e.g we are after the 'compressed' size instead of the 'raw' size
            process = KilProcessInterface(self.member_name, process_expr)
            _io = KilIoInterface(reference=False)
            size_variable = KilVariable('_size', KilTypeSize())
            this_member_class = KilClassInterface(self.root_name, this_variable.type_.name, class_variable=this_variable, ksy=self.ksy)
            method.code_block.children.extend([
                _io.constructor(),
                process.constructor(self.expr_compiler(this_mutable=False)),
                KilIf(expr=this_member_class.write(_io.this), false_body=KilBlock([
                    process.destructor(),
                    _io.destructor(),
                    KilReturn(KilLiteral(0, KilTypeSize()))
                ])),
                KilOperation(KilDeclaration(size_variable), '=', process.encode_size(_io.this)),
                KilIf(expr=_io.ok(), false_body=KilBlock([
                    process.destructor(),
                    _io.destructor(),
                    KilReturn(KilLiteral(0, KilTypeSize()))
                ])),
                process.destructor(),
                _io.destructor(),
                KilReturn(size_variable)
            ])

        return method

    def compile_read_call(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = True
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        method_call = KilMethodCall()
        method_call.class_type = this_member.type_
        method_call.mutable = True
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'read'
        method_call.decl_variable.type_ = KilTypeBoolean()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = [KilIoInterface(reference=True).this]
        method_call = KilOperation(this_member.mutable(), '.', method_call)
        return method_call

    def compile_write_call(self, value_variable):
        method_call = KilMethodCall()
        method_call.class_type = value_variable.type_
        method_call.mutable = True
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'write'
        method_call.decl_variable.type_ = KilTypeBoolean()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = [KilIoInterface(reference=True).this]
        method_call = KilOperation(value_variable, '.', method_call)
        return method_call

    def compile_update_block(self):
        block = super(KilMemberClassCompiler, self).compile_update_block()
        for return_ in block.walk():
            if not isinstance(return_, KilReturn):
                continue
            assert isinstance(return_.type_, KilTypeBoolean)
            assert isinstance(return_.return_, KilLiteral)
            assert return_.return_.value == False
            return_.return_ = KilLiteral(None, KilTypeNone())
        return block
