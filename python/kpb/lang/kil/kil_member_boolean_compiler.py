import os
import sys

from .kil_types import *
from .kil_ksy_library import *
from .kil_member_generic_interface import *
from .kil_member_generic_compiler import *

class KilMemberBooleanCompiler(KilMemberGenericCompiler):

    def compile_methods(self):
        list_ = KilMemberGenericInterface(self.root_name, self.class_name, self.member_name, None, self.ksy).list_
        methods = list()
        if not list_:
            methods.append(self.compile_getter_method())
            methods.append(self.compile_setter_method())
        else:
            methods.append(self.compile_getlist_method())
            methods.append(self.compile_count_method())
            methods.append(self.compile_atindex_method())
            methods.append(self.compile_add_method())
        methods.append(self.compile_clear_method())
        methods.append(self.compile_has_method())
        methods.append(self.compile_randomize_method())
        return methods

    def compile_atindex_method(self):
        method = super(KilMemberBooleanCompiler, self).compile_atindex_method()
        method.decl_variable.variable.reference = False
        return method

    def compile_read_call(self):
        raise NotImplementedError("parsing boolean is not supported, use an integer")

    def compile_write_call(self, value_variable):
        raise NotImplementedError("serializing boolean is not supported, use an integer")
