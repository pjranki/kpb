import os
import sys

from .kil_types import *
from .kil_ksy_library import *
from .kil_expr_compiler import *
from .kil_class_interface import *
from .kil_member_interface import *


class KilUpdateCompiler(object):

    def __init__(self, root_name, class_name, member_name=None, instance_name=None, value_variable=None, ksy=None):
        self.root_name = root_name
        self.class_name = class_name
        self.member_name = member_name
        self.instance_name = instance_name
        self.value_variable = value_variable
        self.ksy = ksy

    def compile_update_block(self):
        this_variable = KilVariable()
        this_variable.name = KilVariable.THIS_NAME
        this_variable.type_ = KilTypeClass(name=self.class_name)
        this_variable.mutable = True
        this_variable.reference = True
        update_block = KilBlock()
        if self.member_name:
            ksy_update_dict_list = self.ksy.ksy_update_dict_list(self.root_name, self.class_name, member_name=self.member_name)
        elif self.instance_name:
            ksy_update_dict_list = self.ksy.ksy_update_dict_list(self.root_name, self.class_name, instance_name=self.instance_name)
        else:
            ksy_update_dict_list = self.ksy.ksy_update_dict_list(self.root_name, self.class_name)
        for ksy_update_dict in ksy_update_dict_list:
            update_member_name = ksy_update_dict['id']
            case_variable = None
            value_variable = None
            if self.member_name:
                ksy_member_dict = self.ksy.find_ksy_member_dict_from_member_name(self.root_name, self.class_name, self.member_name)
                case_expr = ksy_member_dict.get('-case', None)
                if case_expr:
                    expr_compiler = KilExprCompiler(self.root_name, self.class_name, self.member_name, this_variable, length_is_object_size=True, ksy=self.ksy)
                    case_variable = expr_compiler.compile_expr(case_expr)
            if self.instance_name:
                assert self.value_variable
                value_variable = self.value_variable
            update_member_block = KilBlock()
            expr_compiler = KilExprCompiler(self.root_name, self.class_name, update_member_name, this_variable, length_is_object_size=True, value_name=self.instance_name, value_variable=value_variable, case_variable=case_variable, ksy=self.ksy)
            assert update_member_name is not None
            if update_member_name in ['_case', self.instance_name]:
                if update_member_name == '_case':
                    if not case_variable:
                        raise Exception("Can only set `_case` variable in `switch-on` member")
                    modify_variable = case_variable
                elif update_member_name == self.instance_name:
                    modify_variable = value_variable
                else:
                    raise NotImplementedError('-set variable `{}`'.format(update_member_name))
                value_expr = ksy_update_dict['value']
                value = expr_compiler.compile_expr(value_expr, type_=modify_variable.type_)
                update_member_block.children.append(KilOperation(modify_variable, '=', value))
            else:
                if self.ksy.is_value_instance(self.root_name, self.class_name, update_member_name):
                    instance_expr_compiler = KilExprCompiler(self.root_name, self.class_name, update_member_name, this_variable, ksy=self.ksy)
                    member = KilInstanceInterface(self.root_name, self.class_name, update_member_name, this_variable, instance_expr_compiler, self.ksy)
                else:
                    member = KilMemberInterface(self.root_name, self.class_name, update_member_name, this_variable, self.ksy)
                if member.list_:
                    # TODO: loop to add the right number of members
                    index_variable = KilVariable('_i_{}'.format(update_member_name), KilTypeSize())
                    start = KilOperation(KilDeclaration(index_variable), '=', KilLiteral(0, KilTypeSize()))
                    condition = KilOperation(index_variable, '<', KilOperation(member.value, 'len'))
                    update = KilOperation(index_variable, '++')
                    atindex = KilOperation(member.value, '[]', index_variable)
                    if isinstance(member.type_, KilTypeClass):
                        class_name = member.type_.name
                        item = KilClassInterface(self.root_name, class_name, atindex, self.ksy)
                        update_member_block.children.append(KilFor(start, condition, update, body=KilBlock([
                            KilIf(expr=item.update(), false_body=KilBlock([
                                KilReturn(KilLiteral(False, KilTypeBoolean()))
                            ]))
                        ])))
                    else:
                        item = KilMemberInterface(self.root_name, self.class_name, update_member_name, atindex, self.ksy)
                        value_expr = ksy_update_dict['value']
                        value = expr_compiler.compile_expr(value_expr, type_=member.type_)
                        update_member_block.children.append(KilFor(start, condition, update, body=KilBlock([
                            KilIf(expr=item.set(value), false_body=KilBlock([
                                KilReturn(KilLiteral(False, KilTypeBoolean()))
                            ]))
                        ])))
                else:
                    if isinstance(member.type_, KilTypeClass):
                        class_name = member.type_.name
                        item = KilClassInterface(self.root_name, class_name, member.mutable(), self.ksy)
                        update_member_block.children.append(KilIf(expr=item.update(), false_body=KilBlock([
                            KilReturn(KilLiteral(False, KilTypeBoolean()))
                        ])))
                    else:
                        value_expr = ksy_update_dict['value']
                        value = expr_compiler.compile_expr(value_expr, type_=member.type_)
                        update_member_block.children.append(KilIf(expr=member.set(value), false_body=KilBlock([
                            KilReturn(KilLiteral(False, KilTypeBoolean()))]
                        )))
            if_expr = ksy_update_dict.get('if', 'true')
            if if_expr == 'true':
                update_block.children.extend(update_member_block.children)
            else:
                do_update = expr_compiler.compile_expr(if_expr, type_=KilTypeBoolean())
                update_block.children.append(KilIf(expr=do_update, true_body=update_member_block))
        return update_block
