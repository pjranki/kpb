import os
import sys

from .kil_types import *
from .kil_ksy_library import *
from .kil_enum_compiler import *
from .kil_member_compiler import *
from .kil_instance_compiler import *
from .kil_update_compiler import *
from .kil_class_interface import *
from .kil_io_interface import *


class KilClassCompiler(object):

    def __init__(self, root_name=None, class_name=None, ksy=None):
        self.root_name = root_name
        self.class_name = class_name
        self.ksy = ksy

    def compile_class(self):
        class_ = KilClass()
        class_.name = self.class_name
        class_.enums = self.compile_enums()
        class_.members = self.compile_members()
        class_.constructor = self.compile_constructor()
        class_.destructor = self.compile_destructor()
        class_.methods = self.compile_methods()
        return class_

    def compile_enums(self):
        ksy_class_dict = self.ksy.find_ksy_class_dict_from_class_name(self.root_name, self.class_name)
        enums = list()

        # parent enum
        parent_index_map = self.ksy.ksy_parent_index_map(self.root_name, self.class_name)
        enum = KilEnum()
        enum.name = 'parent'
        enum.class_type = KilTypeClass(name=self.class_name)
        enum_value = KilEnumValue()
        enum_value.name = 'none'
        enum_value.enum = enum
        enum_value.value = 0
        enum.enums.append(enum_value)
        for parent_class_name, parent_type in parent_index_map.items():
            assert parent_type != 0
            enum_value = KilEnumValue()
            enum_value.name = parent_class_name
            enum_value.enum = enum
            enum_value.value = parent_type
            enum.enums.append(enum_value)
        enums.append(enum)

        # other enums
        for enum_name in sorted(ksy_class_dict.get('enums', dict()).keys()):
            compiler = KilEnumCompiler(self.root_name, self.class_name, enum_name, self.ksy)
            enums.append(compiler.compile_enum())
        return enums

    def compile_members(self):
        members = list()
        members.extend(self.compile_update_members())
        members.extend(self.compile_root_members())
        members.extend(self.compile_parent_members())
        for ksy_member_dict in self.ksy.ksy_member_dict_list(self.root_name, self.class_name):
            member_name = ksy_member_dict['id']
            compiler = KilMemberCompiler(self.root_name, self.class_name, member_name, self.ksy)
            members.append(compiler.compile_member())
        return members

    def compile_constructor(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = True
        this.reference = True
        this_class = this
        this = KilClassInterface(self.root_name, self.class_name, this, self.ksy)
        constructor = KilConstructor()
        constructor.class_type = KilTypeClass(name=self.class_name)
        constructor.public = True
        constructor.arg_variables = list()
        constructor.code_block = KilBlock()

        # no parent when object created (can be set later)
        parent_type = KilVariable()
        parent_type.name = '_parent_type'
        parent_type.type_ = KilTypeInteger(signed=False, bits=32)
        parent_type.mutable = True
        parent_type.reference = False
        parent_type.list_ = False
        parent_type = KilOperation(this_class, '.', parent_type)
        constructor.code_block.children.append(KilOperation(parent_type, '=', KilLiteral(0, KilTypeInteger(signed=False, bits=32))))

        # WARNING: Do not add update code in constructor - this can lead to infinite constructor calls.
        #          If you want hard-coded values that would have been calculated, use '-default' in ksy files
        constructor.code_block.children.extend(this.update_disable_block().children)

        # Clear all members (set them to their default values)
        constructor.code_block.children.extend(self.compile_clear_block().children)

        # allow update calls to work ONLY after constructor is finished
        constructor.code_block.children.extend(this.update_enable_block().children)

        return constructor

    def compile_destructor(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = True
        this.reference = True
        this = KilClassInterface(self.root_name, self.class_name, this, self.ksy)
        destructor = KilDestructor()
        destructor.class_type = KilTypeClass(name=self.class_name)
        destructor.public = True
        destructor.code_block = KilBlock()
        destructor.code_block.children.extend(this.update_disable_block().children)  # update not needed if object going away
        for ksy_member_dict in self.ksy.ksy_member_dict_list(self.root_name, self.class_name):
            member_name = ksy_member_dict['id']
            compiler = KilMemberCompiler(self.root_name, self.class_name, member_name, self.ksy)
            clear_block = compiler.compile_destructor_block()
            destructor.code_block.children.extend(clear_block.children)
        return destructor

    def compile_methods(self):
        methods = list()
        methods.append(self.compile_clear_method())
        methods.append(self.compile_copy_from_method())
        methods.append(self.compile_parse_method())
        methods.append(self.compile_parse_method(fill=True))
        methods.append(self.compile_serialize_method())
        methods.append(self.compile_read_method())
        methods.append(self.compile_write_method())
        methods.append(self.compile_size_method())
        methods.append(self.compile_randomize_method())
        methods.append(self.compile_update_method())
        methods.extend(self.compile_root_methods())
        methods.extend(self.compile_parent_methods())
        for ksy_member_dict in self.ksy.ksy_member_dict_list(self.root_name, self.class_name):
            member_name = ksy_member_dict['id']
            compiler = KilMemberCompiler(self.root_name, self.class_name, member_name, self.ksy)
            methods.extend(compiler.compile_methods())
        for ksy_value_instance_dict in self.ksy.ksy_value_instances_dict_list(self.root_name, self.class_name):
            instance_name = ksy_value_instance_dict['id']
            compiler = KilInstanceCompiler(self.root_name, self.class_name, instance_name, self.ksy)
            methods.extend(compiler.compile_methods())
        return methods

    def compile_clear_method(self):
        this_variable = KilVariable()
        this_variable.name = KilVariable.THIS_NAME
        this_variable.type_ = KilTypeClass(name=self.class_name)
        this_variable.mutable = True
        this_variable.reference = True
        this = KilClassInterface(self.root_name, self.class_name, this_variable, self.ksy)
        method = KilMethod()
        method.class_type = KilTypeClass(name=self.class_name)
        method.mutable = True
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = 'clear'
        method.decl_variable.type_ = KilTypeNone()
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        method.code_block = KilBlock()
        method.code_block.children.extend(this.update_disable_block(backup=True).children)
        code_block = self.compile_clear_block()
        cleanup_block = this.update_enable_block(restore=True)
        method.code_block.children.append(KilTryCatchBlock(try_body=code_block, final_body=cleanup_block, rethrow=True))
        method.code_block.children.append(this.update())
        return method

    def compile_copy_from_method(self):
        this_variable = KilVariable()
        this_variable.name = KilVariable.THIS_NAME
        this_variable.type_ = KilTypeClass(name=self.class_name)
        this_variable.mutable = True
        this_variable.reference = True
        this = KilClassInterface(self.root_name, self.class_name, this_variable, self.ksy)
        method = KilMethod()
        method.class_type = KilTypeClass(name=self.class_name)
        method.mutable = True
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = 'copy_from'
        method.decl_variable.type_ = KilTypeBoolean()
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        other_variable = KilVariable('_other', KilTypeClass(name=self.class_name), mutable=False, reference=True)
        method.arg_variables = [KilDeclaration(other_variable)]

        # create code to copy all members (or clear them if they are not needed)
        code_block = KilBlock([])
        for ksy_member_dict in self.ksy.ksy_member_dict_list(self.root_name, self.class_name):
            member_name = ksy_member_dict['id']
            this_member = KilMemberInterface(self.root_name, self.class_name, member_name, this_variable, self.ksy)
            other_member = KilMemberInterface(self.root_name, self.class_name, member_name, other_variable, self.ksy)
            copy_code_block = KilBlock([])
            if not this_member.list_:
                copy = this_member.set(other_member.get())
                copy_code_block.children.append(KilIf(expr=copy, false_body=KilBlock([
                    KilReturn(KilLiteral(False, KilTypeBoolean()))
                ])))
            else:
                index_variable = KilVariable('_i_{}'.format(member_name), KilTypeSize())
                start = KilOperation(KilDeclaration(index_variable), '=', KilLiteral(0, KilTypeSize()))
                condition = KilOperation(index_variable, '<', other_member.count())
                update = KilOperation(index_variable, '++')
                copy = this_member.add(other_member.atindex(index_variable))
                copy_code_block.children.extend([
                    this_member.clear(),
                    KilFor(start, condition, update, body=KilBlock([
                        KilIf(expr=copy, false_body=KilBlock([
                            KilReturn(KilLiteral(False, KilTypeBoolean()))
                        ]))
                    ]))
                ])
            clear_code_block = KilBlock([this_member.clear()])
            code_block.children.append(KilIf(expr=other_member.has(), true_body=copy_code_block, false_body=clear_code_block))

        # disable updates while we are are making the copy
        method.code_block = KilBlock()
        method.code_block.children.extend(this.update_disable_block(backup=True).children)
        cleanup_block = this.update_enable_block(restore=True)
        method.code_block.children.append(KilTryCatchBlock(try_body=code_block, final_body=cleanup_block, rethrow=True))

        # finally, return the result of a call to update
        method.code_block.children.append(KilReturn(this.update()))

        # done
        return method

    def compile_clear_block(self):
        this_variable = KilVariable()
        this_variable.name = KilVariable.THIS_NAME
        this_variable.type_ = KilTypeClass(name=self.class_name)
        this_variable.mutable = True
        this_variable.reference = True
        code_block = KilBlock()
        for ksy_member_dict in self.ksy.ksy_member_dict_list(self.root_name, self.class_name):
            member_name = ksy_member_dict['id']
            this_member = KilMemberInterface(self.root_name, self.class_name, member_name, this_variable, self.ksy)
            code_block.children.append(this_member.clear())
        return code_block

    def compile_parse_method(self, fill=False):
        method = KilMethod()
        method.class_type = KilTypeClass(name=self.class_name)
        method.mutable = True
        method.public = True
        method.decl_variable = KilVariable()
        if fill:
            method.decl_variable.name = 'parse_from_string_or_fill'
        else:
            method.decl_variable.name = 'parse_from_string'
        method.decl_variable.type_ = KilTypeBoolean()
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        data_variable = KilVariable('_data', KilTypeBuffer(), mutable=False, reference=True)
        if fill:
            fill_variable = KilVariable('_fill', KilTypeInteger(signed=False, bits=8))
            method.arg_variables = [KilDeclaration(data_variable), KilDeclaration(fill_variable)]
        else:
            method.arg_variables = [KilDeclaration(data_variable)]

        _io = KilIoInterface(reference=False)

        status_variable = KilVariable('_status', KilTypeBoolean())

        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = True
        this.reference = True
        this = KilClassInterface(self.root_name, self.class_name, this, self.ksy)

        if fill:
            method.code_block = KilBlock([
                _io.constructor(),
                _io.set_readonly_or_fill(data_variable, fill_variable),
                KilOperation(KilDeclaration(status_variable), '=', this.read(_io.this)),
                _io.destructor(),
                KilReturn(status_variable)
            ])
        else:
            method.code_block = KilBlock([
                _io.constructor(),
                _io.set_readonly(data_variable),
                KilOperation(KilDeclaration(status_variable), '=', this.read(_io.this)),
                _io.destructor(),
                KilReturn(status_variable)
            ])
        return method

    def compile_serialize_method(self):
        method = KilMethod()
        method.class_type = KilTypeClass(name=self.class_name)
        method.mutable = False
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = 'serialize_to_string'
        method.decl_variable.type_ = KilTypeBoolean()
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        output_variable = KilVariable('_output', KilTypeBuffer(), reference=True)
        method.arg_variables = [KilDeclaration(output_variable)]

        _io = KilIoInterface(reference=False)

        data_variable = KilVariable('_data', KilTypeBuffer())

        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = False
        this.reference = True
        this = KilClassInterface(self.root_name, self.class_name, this, self.ksy)

        method.code_block = KilBlock([
            _io.constructor(),
            KilIf(expr=this.write(_io.this), false_body=KilBlock([
                _io.destructor(),
                KilReturn(KilLiteral(False, KilTypeBoolean()))
            ])),
            KilOperation(KilDeclaration(data_variable), '=', _io.get()),
            KilIf(expr=_io.ok(), false_body=KilBlock([
                _io.destructor(),
                KilReturn(KilLiteral(False, KilTypeBoolean()))
            ])),
            KilOperation(output_variable, '=', data_variable),
            _io.destructor(),
            KilReturn(KilLiteral(True, KilTypeBoolean()))
        ])

        return method

    def compile_read_method(self):
        this_variable = KilVariable()
        this_variable.name = KilVariable.THIS_NAME
        this_variable.type_ = KilTypeClass(name=self.class_name)
        this_variable.mutable = True
        this_variable.reference = True
        this = KilClassInterface(self.root_name, self.class_name, this_variable, self.ksy)
        method = KilMethod()
        method.class_type = KilTypeClass(name=self.class_name)
        method.mutable = True
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = 'read'
        method.decl_variable.type_ = KilTypeBoolean()
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        method.arg_variables = [KilDeclaration(KilIoInterface(reference=True).this)]
        method.code_block = KilBlock()
        read_block_clear = KilBlock()
        read_block_read = KilBlock()

        # read sequence members and position instances (in correct dependency order)
        for ksy_member_dict in self.ksy.ksy_sorted_seq_instance_dict_list(self.root_name, self.class_name):
            member_name = ksy_member_dict['id']
            compiler = KilMemberCompiler(self.root_name, self.class_name, member_name, self.ksy)
            this_member = KilMemberInterface(self.root_name, self.class_name, member_name, this_variable, self.ksy)
            read_block_clear.children.append(this_member.clear())
            read_block_read.children.extend(compiler.compile_read_block().children)

        method.code_block.children.extend(this.update_disable_block(backup=True).children)
        code_block = KilBlock()
        cleanup_block = KilBlock()
        code_block.children.extend(read_block_clear.children)
        code_block.children.extend(read_block_read.children)
        cleanup_block.children.extend(this.update_enable_block(restore=True).children)
        method.code_block.children.append(KilTryCatchBlock(try_body=code_block, final_body=cleanup_block, rethrow=True))
        method.code_block.children.append(KilReturn(KilLiteral(True, KilTypeBoolean())))
        # NOTE: we don't want to call update on read - preserve the exact values parsed
        return method

    def compile_write_method(self):
        method = KilMethod()
        method.class_type = KilTypeClass(name=self.class_name)
        method.mutable = False
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = 'write'
        method.decl_variable.type_ = KilTypeBoolean()
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        method.arg_variables = [KilDeclaration(KilIoInterface(reference=True).this)]
        method.code_block = KilBlock()
        write_block_write = KilBlock()

        # write sequence members and position instances (in correct dependency order)
        for ksy_member_dict in self.ksy.ksy_sorted_seq_instance_dict_list(self.root_name, self.class_name):
            member_name = ksy_member_dict['id']
            compiler = KilMemberCompiler(self.root_name, self.class_name, member_name, self.ksy)
            write_block_write.children.extend(compiler.compile_write_block().children)

        method.code_block.children.extend(write_block_write.children)
        method.code_block.children.append(KilReturn(KilLiteral(True, KilTypeBoolean())))
        # NOTE: we don't call update on write - we are not changing this class
        return method

    def compile_size_method(self):
        method = KilMethod()
        method.class_type = KilTypeClass(name=self.class_name)
        method.mutable = False
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = 'size'
        method.decl_variable.type_ = KilTypeSize()
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        method.arg_variables = []
        method.code_block = KilBlock()

        _io = KilIoInterface(reference=False)

        size_variable = KilVariable('_size', KilTypeSize())

        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = False
        this.reference = True
        this = KilClassInterface(self.root_name, self.class_name, this, self.ksy)

        method.code_block = KilBlock([
            _io.constructor(),
            KilIf(expr=this.write(_io.this), false_body=KilBlock([
                _io.destructor(),
                KilReturn(KilLiteral(0, KilTypeSize()))
            ])),
            KilOperation(KilDeclaration(size_variable), '=', _io.get_size()),
            KilIf(expr=_io.ok(), false_body=KilBlock([
                _io.destructor(),
                KilReturn(KilLiteral(0, KilTypeSize()))
            ])),
            _io.destructor(),
            KilReturn(size_variable)
        ])
        return method

    def compile_randomize_method(self):
        method = KilMethod()
        method.class_type = KilTypeClass(name=self.class_name)
        method.mutable = True
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = 'randomize'
        method.decl_variable.type_ = KilTypeBoolean()
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        method.arg_variables = list()
        method.code_block = KilBlock()
        randomize_block = KilBlock()

        # randomize all the members of this class
        this_variable = KilVariable()
        this_variable.name = KilVariable.THIS_NAME
        this_variable.type_ = KilTypeClass(name=self.class_name)
        this_variable.mutable = True
        this_variable.reference = True
        for ksy_member_dict in self.ksy.ksy_member_dict_list(self.root_name, self.class_name):
            member_name = ksy_member_dict['id']
            this_member = KilMemberInterface(self.root_name, self.class_name, member_name, this_variable, self.ksy)
            randomize_block.children.append(KilIf(expr=this_member.has(), true_body=KilBlock([
                KilIf(expr=this_member.randomize(), false_body=KilBlock([
                    KilReturn(KilLiteral(False, KilTypeBoolean()))
                ]))
            ])))

        # return true on success
        randomize_block.children.append(KilReturn(KilLiteral(True, KilTypeBoolean())))

        # done
        method.code_block = randomize_block
        return method

    def compile_update_members(self):
        this_variable = KilVariable()
        this_variable.name = KilVariable.THIS_NAME
        this_variable.type_ = KilTypeClass(name=self.class_name)
        this_variable.mutable = True
        this_variable.reference = True
        this = KilClassInterface(self.root_name, self.class_name, this_variable, self.ksy)
        members = list()
        for reg in this.update_registers():
            assert isinstance(reg, KilOperation)
            member = KilMember()
            member.class_type = KilTypeClass(name=self.class_name)
            member.variable = KilDeclaration(reg.b)
            member.public = False
            members.append(member)
        return members

    def compile_update_method(self):
        this_variable = KilVariable()
        this_variable.name = KilVariable.THIS_NAME
        this_variable.type_ = KilTypeClass(name=self.class_name)
        this_variable.mutable = True
        this_variable.reference = True
        this = KilClassInterface(self.root_name, self.class_name, this_variable, self.ksy)
        method = KilMethod()
        method.class_type = KilTypeClass(name=self.class_name)
        method.mutable = True
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = 'update'
        method.decl_variable.type_ = KilTypeBoolean()
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        method.arg_variables = []
        method.code_block = KilBlock([])

        # don't recursively call this class's Update() method
        class_id = self.ksy.ksy_class_id(self.root_name, self.class_name)

        # return true and do nothing if we are recursively calling update on the same class
        method.code_block.children.append(KilIf(expr=this.update_enabled(class_id), false_body=KilBlock([
            KilReturn(KilLiteral(True, KilTypeBoolean()))
        ])))

        # disable update on this class (to avoid infinite recursion)
        update_block = KilBlock()
        update_block_error_handled = KilBlock()
        update_block_error_handled.children.extend(this.update_disable_block([class_id]).children)
        try_catch_block = KilTryCatchBlock(rethrow=True)
        try_catch_block.try_body = update_block
        update_block_error_handled.children.append(try_catch_block)

        # re-enable update on return OR error/exception
        try_catch_block.final_body = this.update_enable_block([class_id])

        # update member values
        update_compiler = KilUpdateCompiler(self.root_name, self.class_name, ksy=self.ksy)
        update_block.children.extend(update_compiler.compile_update_block().children)

        # update parents (now that we have changed values)
        for parent_type, parent in this.parents:
            parent_class_name = parent.type_.name
            parent_class_interface = KilClassInterface(self.root_name, parent_class_name, parent, self.ksy)
            update_block.children.extend([
                KilIf(expr=KilOperation(this.parent_type(), '==', KilLiteral(parent_type, type_=KilTypeInteger(signed=False, bits=32))), true_body=KilBlock([
                    KilIf(expr=parent, true_body=KilBlock([
                        KilIf(expr=parent_class_interface.update(), false_body=KilBlock([
                            KilReturn(KilLiteral(False, KilTypeBoolean()))
                        ]))
                    ]))
                ]))
            ])

        # add exception handling to method
        method.code_block.children.extend(update_block_error_handled.children)

        # return true if everything update sucessfully
        method.code_block.children.append(KilReturn(KilLiteral(True, KilTypeBoolean())))
        return method

    def compile_root_members(self):
        if self.root_name == self.class_name:
            return list()  # no '_root' member needed - this is the root class, can just use a this pointer
        members = list()
        variable = KilVariable()
        variable.name = '_root'
        variable.type_ = KilTypeClass(name=self.root_name)
        variable.mutable = True
        variable.reference = True
        variable.list_ = False
        member = KilMember()
        member.class_type = KilTypeClass(name=self.class_name)
        member.variable = KilDeclaration(variable)
        member.public = False
        members.append(member)
        return members

    def compile_root_methods(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = False
        this.reference = True
        this_mutable = KilVariable()
        this_mutable.name = KilVariable.THIS_NAME
        this_mutable.type_ = KilTypeClass(name=self.class_name)
        this_mutable.mutable = True
        this_mutable.reference = True
        methods = list()
        root_type = KilTypeClass(name=self.root_name)
        variable = KilVariable()
        variable.name = '_root'
        variable.type_ = root_type
        variable.mutable = False
        variable.reference = True
        variable.list_ = False
        if self.root_name == self.class_name:
            root = this
        else:
            root = KilOperation(this, '.', variable)
        method = KilMethod()
        method.class_type = this.type_
        method.mutable = False
        method.public = True
        method.hint_treat_as_member_variable = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = 'root'
        method.decl_variable.type_ = root_type
        method.decl_variable.mutable = False
        method.decl_variable.reference = True
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        method.code_block = KilBlock([
            KilReturn(root)
        ])
        methods.append(method)
        if self.root_name != self.class_name:
            variable = KilVariable()
            variable.name = '_root'
            variable.type_ = root_type
            variable.mutable = True
            variable.reference = True
            variable.list_ = False
            root_mutable = KilOperation(this_mutable, '.', variable)
            method = KilMethod()
            method.class_type = this_mutable.type_
            method.mutable = True
            method.public = True
            method.decl_variable = KilVariable()
            method.decl_variable.name = 'set_root'
            method.decl_variable.type_ = KilTypeNone()
            method.decl_variable.mutable = False
            method.decl_variable.reference = False
            method.decl_variable.list_ = False
            method.decl_variable = KilDeclaration(method.decl_variable)
            value_variable = KilVariable()
            value_variable.name = '_root'
            value_variable.type_ = root_type
            value_variable.mutable = True
            value_variable.reference = True
            value_variable.list_ = False
            method.arg_variables = [KilDeclaration(value_variable)]
            method.code_block = KilBlock([
                KilOperation(root_mutable, '=', value_variable),
            ])
            methods.append(method)
        return methods

    def compile_parent_members(self):
        members = list()

        # parent type (only set to type if it has only 1 parent)
        parent_index_map = self.ksy.ksy_parent_index_map(self.root_name, self.class_name)
        parent_type = KilTypeNone()
        if len(parent_index_map.values()) == 1:
            parent_type = KilTypeClass(name=list(parent_index_map.keys())[0])

        # parent member
        variable = KilVariable()
        variable.name = '_parent'
        variable.type_ = parent_type
        variable.mutable = True
        variable.reference = True
        variable.list_ = False
        member = KilMember()
        member.class_type = KilTypeClass(name=self.class_name)
        member.variable = KilDeclaration(variable)
        member.public = False
        members.append(member)

        # parent type
        variable = KilVariable()
        variable.name = '_parent_type'
        variable.type_ = KilTypeInteger(signed=False, bits=32)
        variable.mutable = True
        variable.reference = False
        variable.list_ = False
        member = KilMember()
        member.class_type = KilTypeClass(name=self.class_name)
        member.variable = KilDeclaration(variable)
        member.public = False
        members.append(member)
        return members

    def compile_parent_methods(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = False
        this.reference = True
        this_mutable = KilVariable()
        this_mutable.name = KilVariable.THIS_NAME
        this_mutable.type_ = KilTypeClass(name=self.class_name)
        this_mutable.mutable = True
        this_mutable.reference = True
        methods = list()

        # parent type (only set to type if it has only 1 parent)
        parent_index_map = self.ksy.ksy_parent_index_map(self.root_name, self.class_name)
        parent_type = KilTypeNone()
        if len(parent_index_map.values()) == 1:
            parent_type = KilTypeClass(name=list(parent_index_map.keys())[0])

        # get parent method
        method = KilMethod()
        method.class_type = this.type_
        method.mutable = False
        method.public = True
        method.hint_treat_as_member_variable = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = 'parent'
        method.decl_variable.type_ = KilTypeNone()
        method.decl_variable.mutable = False
        method.decl_variable.reference = True
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        variable = KilVariable()
        variable.name = '_parent'
        variable.type_ = parent_type
        variable.mutable = False
        variable.reference = True
        variable.list_ = False
        if isinstance(parent_type, KilTypeClass):
            method.code_block = KilBlock([
                KilReturn(KilTypecast(value=KilOperation(this, '.', variable), type_=KilTypeNone())),
            ])
        else:
            method.code_block = KilBlock([
                KilReturn(KilOperation(this, '.', variable)),
            ])
        methods.append(method)

        # get parent type method
        method = KilMethod()
        method.class_type = this.type_
        method.mutable = False
        method.public = True
        method.hint_treat_as_member_variable = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = 'parent_type'
        method.decl_variable.type_ = KilTypeInteger(signed=False, bits=32)
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        variable = KilVariable()
        variable.name = '_parent_type'
        variable.type_ = KilTypeInteger(signed=False, bits=32)
        variable.mutable = False
        variable.reference = False
        variable.list_ = False
        method.code_block = KilBlock([
            KilReturn(KilOperation(this, '.', variable)),
        ])
        methods.append(method)

        # set parent method
        method = KilMethod()
        method.class_type = this_mutable.type_
        method.mutable = True
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = 'set_parent'
        method.decl_variable.type_ = KilTypeBoolean()
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        value_variable = KilVariable()
        value_variable.name = '_parent'
        value_variable.type_ = KilTypeNone()
        value_variable.mutable = True
        value_variable.reference = True
        value_variable.list_ = False
        type_variable = KilVariable()
        type_variable.name = '_parent_type'
        type_variable.type_ = KilTypeInteger(signed=False, bits=32)
        type_variable.mutable = True
        type_variable.reference = False
        type_variable.list_ = False
        method.arg_variables = [KilDeclaration(value_variable), KilDeclaration(type_variable)]
        variable = KilVariable()
        variable.name = '_parent_type'
        variable.type_ = KilTypeInteger(signed=False, bits=32)
        variable.mutable = True
        variable.reference = False
        variable.list_ = False
        parent_type_mutable = KilOperation(this_mutable, '.', variable)
        variable = KilVariable()
        variable.name = '_parent'
        variable.type_ = parent_type
        variable.mutable = True
        variable.reference = True
        variable.list_ = False
        parent_mutable = KilOperation(this_mutable, '.', variable)
        if isinstance(parent_type, KilTypeClass):
            method.code_block = KilBlock([
                KilOperation(parent_type_mutable, '=', type_variable),
                KilOperation(parent_mutable, '=', KilTypecast(value=value_variable, type_=parent_type)),
                KilReturn(KilLiteral(True, KilTypeBoolean())),
            ])
        else:
            method.code_block = KilBlock([
                KilOperation(parent_type_mutable, '=', type_variable),
                KilOperation(parent_mutable, '=', value_variable),
                KilReturn(KilLiteral(True, KilTypeBoolean())),
            ])
        methods.append(method)
        return methods
