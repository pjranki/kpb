import os
import sys

from .kil_types import *
from .kil_ksy_library import *
from .kil_class_compiler import *


class KilFileCompiler(object):

    def __init__(self, root_name=None, namespace=None, file_path_key=None, ksy=None):
        self.root_name = root_name
        self.namespace = namespace
        self.file_path_key = file_path_key
        self.ksy = ksy

    def compile_file(self):
        ksy_file_dict = self.ksy.find_ksy_file_dict_from_root_name(self.root_name)
        file_ = KilFile()
        file_.input_file_name = ksy_file_dict[self.file_path_key]
        file_.name = os.path.splitext(os.path.basename(file_.input_file_name))[0]
        for import_name in ksy_file_dict['meta'].get('imports', list()):
            file_.children.append(KilImport(import_name))
        for parent_import, class_name_list in self.ksy.ksy_parent_imports(self.root_name).items():
            imports = list()
            for class_name in class_name_list:
                import_class = KilTypeClass()
                import_class.name = class_name
                import_class.external = False
                imports.append(import_class)
            file_.children.append(KilImport(parent_import, public=False, imports=imports))
        for process_import in self.ksy.ksy_process_imports(self.root_name):
            file_.children.append(KilImport(process_import, public=False, external=True))
        if self.namespace:
            namespace = KilNamespace(name=self.namespace)
            file_.children.append(namespace)
        else:
            namespace = file_
        for ksy_class_dict in self.ksy.ksy_class_dict_list_from_root_name(self.root_name):
            class_name = ksy_class_dict['id']
            compiler = KilClassCompiler(self.root_name, class_name, self.ksy)
            class_ = compiler.compile_class()
            namespace.children.append(class_)
        return file_
