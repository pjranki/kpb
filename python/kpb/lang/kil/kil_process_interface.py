import os
import sys

from .kil_types import *


class KilProcessInterface(object):

    def __init__(self, member_name=None, process_expr=None):
        self.member_name = member_name
        self.process_expr = process_expr

    @property
    def class_name(self):
        return self.process_expr.split('(')[0]

    @property
    def arg_expr_list(self):
        expr = self.process_expr
        if expr.find('(') == -1:
            return list()
        assert expr.endswith(')')
        expr = expr[:-1]
        expr = '('.join(expr.split('(')[1:])
        return [arg_expr.strip() for arg_expr in expr.split(',')]

    @property
    def type_(self):
        class_type = KilTypeClass(name=self.class_name)
        class_type.external = True
        return class_type

    @property
    def this(self):
        variable = KilVariable()
        variable.name = '_process_{}'.format(self.member_name)
        variable.type_ = self.type_
        variable.mutable = True
        variable.reference = False
        variable.list_ = False
        return variable

    def constructor(self, expr_compiler):
        call_constructor = KilConstructorCall(class_type=self.type_)
        for arg_expr in self.arg_expr_list:
            call_constructor.arg_variables.append(expr_compiler.compile_expr(arg_expr))
        call_constructor = KilOperation(KilDeclaration(self.this), '=', call_constructor)
        return call_constructor

    def destructor(self):
        call_destructor = KilDestructorCall(class_type=self.type_)
        call_destructor = KilOperation(self.this, '.', call_destructor)
        return call_destructor

    def decode(self, io_variable):
        method_call = KilMethodCall()
        method_call.class_type = self.type_
        method_call.decl_variable = KilVariable('decode', KilTypeBoolean())
        method_call.arg_variables = [io_variable]
        method_call.mutable = True
        method_call.public = True
        method_call = KilOperation(self.this, '.', method_call)
        return method_call

    def encode(self, io_variable):
        method_call = KilMethodCall()
        method_call.class_type = self.type_
        method_call.decl_variable = KilVariable('encode', KilTypeBoolean())
        method_call.arg_variables = [io_variable]
        method_call.mutable = True
        method_call.public = True
        method_call = KilOperation(self.this, '.', method_call)
        return method_call

    def encode_size(self, io_variable):
        method_call = KilMethodCall()
        method_call.class_type = self.type_
        method_call.decl_variable = KilVariable('encode_size', KilTypeSize())
        method_call.arg_variables = [io_variable]
        method_call.mutable = True
        method_call.public = True
        method_call = KilOperation(self.this, '.', method_call)
        return method_call
