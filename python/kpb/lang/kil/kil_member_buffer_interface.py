import os
import sys

from .kil_types import *
from .kil_ksy_library import *
from .kil_member_generic_interface import *


class KilMemberBufferInterface(KilMemberGenericInterface):

    def compile_methods(self):
        methods = list()
        if not self.list_:
            methods.append(self.compile_getter_method())
            methods.append(self.compile_size_method())
            methods.append(self.compile_setter_method())
        else:
            methods.append(self.compile_getlist_method())
            methods.append(self.compile_count_method())
            methods.append(self.compile_atindex_method())
            methods.append(self.compile_sizeat_method())
            methods.append(self.compile_add_method())
        methods.append(self.compile_clear_method())
        methods.append(self.compile_has_method())
        return methods

    def constructor_default(self, expr_compiler):
        return KilLiteral(self.ksy_member_dict.get('-default', b""), self.type_)

    def destructor_default(self):
        return KilLiteral(b"", self.type_)

    def get(self):
        method_call = super(KilMemberBufferInterface, self).get()
        method_call.b.decl_variable.reference = True
        return method_call

    def set(self, value_variable):
        method_call = super(KilMemberBufferInterface, self).set(value_variable)
        # FUTURE: this may break things in future
        # method_call.b.arg_variables[0].reference = True
        return method_call
