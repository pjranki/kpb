import os
import sys

from .kil_types import *

from .. expression_parser import ExpressionParser


class KilKsyLibrary(object):

    def __init__(self, file_path_key=None, ksy_import_map=None, ksy_compile_list=None):
        self.file_path_key = file_path_key
        self.ksy_import_map = ksy_import_map
        self.ksy_compile_list = ksy_compile_list
        self.cache_ksy_file_dict_list_across_all_files = None
        self.cache_ksy_class_dict_list_across_all_files = None
        self.cache_ksy_file_map = dict()
        self.cache_ksy_root_class_dict_list = None
        self.cache_ksy_class_dict_list = None
        self.cache_ksy_class_dict_list_map = dict()
        self.cache_ksy_class_dict_map = dict()
        self.cache_ksy_parent_class_dict_list_map = dict()
        self.cache_ksy_enum_dict_map = dict()
        self.cache_ksy_param_dict_list_map = dict()
        self.cache_ksy_seq_dict_list_map = dict()
        self.cache_ksy_member_dict_map = dict()
        self.cache_ksy_global_member_type_dict_map = dict()
        self.cache_ksy_global_member_is_list_map = dict()

    def copy(self):
        ksy = KilKsyLibrary()
        ksy.file_path_key = self.file_path_key
        ksy.ksy_import_map = self.ksy_import_map
        ksy.ksy_compile_list = self.ksy_compile_list
        ksy.cache_ksy_file_dict_list_across_all_files = self.cache_ksy_file_dict_list_across_all_files
        ksy.cache_ksy_class_dict_list_across_all_files = self.cache_ksy_class_dict_list_across_all_files
        ksy.cache_ksy_file_map = self.cache_ksy_file_map
        ksy.cache_ksy_root_class_dict_list = self.cache_ksy_root_class_dict_list
        ksy.cache_ksy_class_dict_list = self.cache_ksy_class_dict_list
        ksy.cache_ksy_class_dict_list_map = self.cache_ksy_class_dict_list_map
        ksy.cache_ksy_class_dict_map = self.cache_ksy_class_dict_map
        ksy.cache_ksy_parent_class_dict_list_map = self.cache_ksy_parent_class_dict_list_map
        ksy.cache_ksy_enum_dict_map = self.cache_ksy_enum_dict_map
        ksy.cache_ksy_param_dict_list_map = self.cache_ksy_param_dict_list_map
        ksy.cache_ksy_seq_dict_list_map = self.cache_ksy_seq_dict_list_map
        ksy.cache_ksy_member_dict_map = self.cache_ksy_member_dict_map
        ksy.cache_ksy_global_member_type_dict_map = self.cache_ksy_global_member_type_dict_map
        ksy.cache_ksy_global_member_is_list_map = self.cache_ksy_global_member_is_list_map
        return ksy

    def change_import_context(self, root_name):
        # find the file dictionary for the new import/root context
        ksy_file_dict = self.ksy_import_map.get(root_name, None)
        if not ksy_file_dict:
            for ksy_compile_file_dict in self.ksy_compile_list:
                if ksy_compile_file_dict['meta']['id'] == root_name:
                    ksy_file_dict = ksy_compile_file_dict
                    break
        if not ksy_file_dict:
            raise NotImplementedError("root class '{}' not found in imports or compiled ksy files".format(root_name))

        # create a new map of imports for the new context
        new_ksy_import_map = dict()
        for import_name in ksy_file_dict['meta'].get('imports', list()):
            ksy_import_file_dict = self.ksy_import_map.get(import_name, None)
            if not ksy_import_file_dict:
                for ksy_compile_file_dict in self.ksy_compile_list:
                    compile_file_root_name = ksy_compile_file_dict['meta']['id']
                    if compile_file_root_name == import_name:
                        ksy_import_file_dict = ksy_compile_file_dict
                        break
                    if compile_file_root_name.endswith('_') and compile_file_root_name[:-1] == import_name:
                        ksy_import_file_dict = ksy_compile_file_dict
                        break
            if not ksy_import_file_dict:
                raise NotImplementedError("import '{}' not found in imports or compiled ksy files".format(import_name))
            new_ksy_import_map[import_name] = ksy_import_file_dict
        self.ksy_import_map = new_ksy_import_map

        # wipe invalid caches
        self.cache_ksy_file_map = dict()
        self.cache_ksy_root_class_dict_list = list()
        self.cache_ksy_class_dict_list = list()
        self.cache_ksy_class_dict_list_map = dict()
        self.cache_ksy_class_dict_map = dict()
        self.cache_ksy_parent_class_dict_list_map = dict()
        self.cache_ksy_enum_dict_map = dict()
        self.cache_ksy_param_dict_list_map = dict()
        self.cache_ksy_seq_dict_list_map = dict()
        self.cache_ksy_member_dict_map = dict()

        # this is the new root file
        self.cache_ksy_file_map[root_name] = ksy_file_dict

    def ksy_file_dict_list(self):
        return self.ksy_import_map.values()

    def ksy_file_dict_list_across_all_files(self):
        if self.cache_ksy_file_dict_list_across_all_files is not None:
            return self.cache_ksy_file_dict_list_across_all_files
        ksy_file_dict_list = list()
        ksy_file_dict_list.extend(self.ksy_file_dict_list())
        root_name_list = [d['meta']['id'] for d in ksy_file_dict_list]
        for ksy_file_dict in self.ksy_compile_list:
            root_name = ksy_file_dict['meta']['id']
            if root_name in root_name_list:
                continue
            ksy_file_dict_list.append(ksy_file_dict)
            root_name_list.append(root_name)
        self.cache_ksy_file_dict_list_across_all_files = ksy_file_dict_list
        return ksy_file_dict_list

    def find_ksy_file_dict_from_root_name(self, root_name):
        ksy_file_dict = self.cache_ksy_file_map.get(root_name, None)
        if ksy_file_dict is not None:
            return ksy_file_dict
        for ksy_file_dict in self.ksy_file_dict_list():
            import_root_name = ksy_file_dict['meta']['id']
            self.cache_ksy_file_map[import_root_name] = ksy_file_dict
            if import_root_name == root_name:
                return ksy_file_dict
        raise Exception("root class type '{}' not found in ksy imports".format(root_name))

    def ksy_root_class_dict_list(self):
        if self.cache_ksy_root_class_dict_list is not None:
            return self.cache_ksy_root_class_dict_list
        ksy_root_class_dict_list = list()
        for ksy_file_dict in self.ksy_file_dict_list():
            ksy_class_dict = dict()
            ksy_class_dict.update(ksy_file_dict)
            ksy_class_dict['id'] = ksy_file_dict['meta']['id']
            ksy_root_class_dict_list.append(ksy_class_dict)
        self.cache_ksy_root_class_dict_list = ksy_root_class_dict_list
        return ksy_root_class_dict_list

    def ksy_class_dict_list(self):
        if self.cache_ksy_class_dict_list is not None:
            return self.cache_ksy_class_dict_list
        ksy_class_dict_list = list()
        for ksy_file_dict in self.ksy_file_dict_list():
            root_name = ksy_file_dict['meta']['id']
            ksy_class_dict_list.extend(self.ksy_class_dict_list_from_root_name(root_name))
        self.cache_ksy_class_dict_list = ksy_class_dict_list
        return ksy_class_dict_list

    def ksy_class_dict_list_across_all_files(self):
        if self.cache_ksy_class_dict_list_across_all_files is not None:
            return self.cache_ksy_class_dict_list_across_all_files
        ksy_class_dict_list = list()
        for ksy_file_dict in self.ksy_file_dict_list_across_all_files():
            ksy_class_dict = dict()
            ksy_class_dict.update(ksy_file_dict)
            ksy_class_dict['id'] = ksy_file_dict['meta']['id']
            ksy_class_dict_list.append(ksy_class_dict)
            for name, ksy_type in ksy_file_dict.get('types', dict()).items():
                ksy_class_dict = dict()
                ksy_class_dict.update(ksy_type)
                ksy_class_dict['id'] = name
                ksy_class_dict_list.append(ksy_class_dict)
        self.cache_ksy_class_dict_list_across_all_files = ksy_class_dict_list
        return ksy_class_dict_list

    def ksy_class_dict_list_from_root_name(self, root_name):
        ksy_class_dict_list = self.cache_ksy_class_dict_list_map.get(root_name, None)
        if ksy_class_dict_list is not None:
            return ksy_class_dict_list
        ksy_file_dict = self.find_ksy_file_dict_from_root_name(root_name)
        ksy_class_dict_list = list()
        ksy_class_dict = dict()
        ksy_class_dict.update(ksy_file_dict)
        ksy_class_dict['id'] = ksy_file_dict['meta']['id']
        ksy_class_dict_list.append(ksy_class_dict)
        for name, ksy_type in ksy_file_dict.get('types', dict()).items():
            ksy_class_dict = dict()
            ksy_class_dict.update(ksy_type)
            ksy_class_dict['id'] = name
            ksy_class_dict_list.append(ksy_class_dict)
        self.cache_ksy_class_dict_list_map[root_name] = ksy_class_dict_list
        return ksy_class_dict_list

    def is_class_name_in_root_name(self, root_name, class_name):
        ksy_class_dict_list = self.ksy_class_dict_list_from_root_name(root_name)
        class_name_list = [d['id'] for d in ksy_class_dict_list]
        return class_name in class_name_list

    def find_root_name_from_class_name(self, root_name, class_name):
        # if the class IS the root, we are done
        if class_name == root_name:
            return root_name
        # search the current root first
        for ksy_class_dict in self.ksy_class_dict_list_from_root_name(root_name):
            if ksy_class_dict['id'] == class_name:
                return root_name
        # search all root classes
        for ksy_file_dict in self.ksy_file_dict_list_across_all_files():
            if ksy_file_dict['meta']['id'] == class_name:
                return ksy_file_dict['meta']['id']
            # TODO: iterate sub-classes
        raise Exception("class '{}' not found in import '{}' or any other root class".format(class_name, root_name))

    def find_ksy_class_dict_from_class_name(self, root_name, class_name):
        ksy_class_dict = self.cache_ksy_class_dict_map.get(root_name, dict()).get(class_name, None)
        if ksy_class_dict is not None:
            return ksy_class_dict
        ksy_class_dict_list = self.ksy_class_dict_list_from_root_name(root_name)
        for ksy_class_dict in ksy_class_dict_list:
            if ksy_class_dict['id'] == class_name:
                self.cache_ksy_class_dict_map[root_name] = self.cache_ksy_class_dict_map.get(root_name, dict())
                self.cache_ksy_class_dict_map[root_name][class_name] = ksy_class_dict
                return ksy_class_dict
        ksy_class_dict_list = self.ksy_root_class_dict_list()
        for ksy_class_dict in ksy_class_dict_list:
            if ksy_class_dict['id'] == class_name:
                self.cache_ksy_class_dict_map[root_name] = self.cache_ksy_class_dict_map.get(root_name, dict())
                self.cache_ksy_class_dict_map[root_name][class_name] = ksy_class_dict
                return ksy_class_dict
        ksy_class_dict_list = self.ksy_class_dict_list()
        for ksy_class_dict in ksy_class_dict_list:
            if ksy_class_dict['id'] == class_name:
                self.cache_ksy_class_dict_map[root_name] = self.cache_ksy_class_dict_map.get(root_name, dict())
                self.cache_ksy_class_dict_map[root_name][class_name] = ksy_class_dict
                return ksy_class_dict
        ksy_class_dict_list = self.ksy_class_dict_list_across_all_files()
        for ksy_class_dict in ksy_class_dict_list:
            if ksy_class_dict['id'] == class_name:
                self.cache_ksy_class_dict_map[root_name] = self.cache_ksy_class_dict_map.get(root_name, dict())
                self.cache_ksy_class_dict_map[root_name][class_name] = ksy_class_dict
                return ksy_class_dict
        raise Exception("class '{}' not found in import '{}'".format(class_name, root_name))

    def ksy_parent_class_dict_list(self, root_name, class_name):
        ksy_parent_class_dict_list = self.cache_ksy_parent_class_dict_list_map.get(root_name, dict()).get(class_name, None)
        if ksy_parent_class_dict_list is not None:
            return ksy_parent_class_dict_list
        ksy_class_dict_list = self.ksy_class_dict_list_across_all_files()
        ksy_parent_class_dict_list = list()
        for ksy_class_dict in ksy_class_dict_list:
            current_class_name = ksy_class_dict['id']
            ksy_member_dict_list = self.ksy_member_dict_list(root_name, current_class_name)
            # search every member in this class if it uses the class name
            # if it does, this class is a parent
            parent = False
            for ksy_member_dict in ksy_member_dict_list:
                member_class_name = ksy_member_dict.get('type', None)
                if isinstance(member_class_name, str):
                    member_class_name = member_class_name.split('(')[0]
                if member_class_name == class_name:
                    parent = True
                    break # we have marked it as a parent, no need to keep searching
            if parent:
                ksy_parent_class_dict_list.append(ksy_class_dict)
            # check next class
        self.cache_ksy_parent_class_dict_list_map[root_name] = self.cache_ksy_parent_class_dict_list_map.get(root_name, dict())
        self.cache_ksy_parent_class_dict_list_map[root_name][class_name] = ksy_parent_class_dict_list
        return ksy_parent_class_dict_list

    def ksy_parent_index_map(self, root_name, class_name):
        ksy_parent_class_dict_list = self.ksy_parent_class_dict_list(root_name, class_name)
        ordered_parent_class_name_list = sorted([d['id'] for d in ksy_parent_class_dict_list])
        parent_index_map = {ordered_parent_class_name_list[i]: (i + 1) for i in range(0, len(ordered_parent_class_name_list))}
        return parent_index_map

    def find_ksy_parent_class_dict_from_class_name(self, root_name, class_name):
        ksy_parent_class_dict_list = self.ksy_parent_class_dict_list(root_name, class_name)
        if len(ksy_parent_class_dict_list) == 0:
            raise Exception("class '{}' from import '{}' has no parents".format(class_name, root_name))
        if len(ksy_parent_class_dict_list) > 1:
            raise Exception("class '{}' from import '{}' has more than 1 parent".format(class_name, root_name))
        assert len(ksy_parent_class_dict_list) == 1
        return ksy_parent_class_dict_list[0]

    def ksy_parent_imports(self, root_name):
        ksy_file_dict = self.find_ksy_file_dict_from_root_name(root_name)
        class_name_list = [ksy_file_dict['meta']['id']]
        class_name_list.extend([d['id'] for d in self.ksy_class_dict_list_from_root_name(root_name)])
        ksy_parent_file_dict_map = dict()
        ksy_parent_class_list_map = dict()
        # check every class in this ksy file (found using the root name)
        for class_name in class_name_list:
            # check every parent for every class in this ksy file
            for ksy_parent_class_dict in self.ksy_parent_class_dict_list(root_name, class_name):
                parent_class_name = ksy_parent_class_dict['id']
                # check to see if there is a compiled file that has this parent
                for ksy_file_dict in self.ksy_compile_list:
                    if ksy_file_dict['meta']['id'] == root_name:
                        # don't need to import myself
                        continue
                    if ksy_file_dict['meta']['id'] == parent_class_name:
                        # root class is a parent in this file
                        import_name = os.path.splitext(os.path.basename(ksy_file_dict[self.file_path_key]))[0]
                        ksy_parent_file_dict_map[import_name] = ksy_file_dict
                        ksy_parent_class_list_map[import_name] = ksy_parent_class_list_map.get(import_name, list())
                        if parent_class_name not in ksy_parent_class_list_map[import_name]:
                            ksy_parent_class_list_map[import_name].append(parent_class_name)
                        continue
                    for name in ksy_file_dict.get('types', dict()).keys():
                        if name == parent_class_name:
                            import_name = os.path.splitext(os.path.basename(ksy_file_dict[self.file_path_key]))[0]
                            ksy_parent_file_dict_map[import_name] = ksy_file_dict
                            ksy_parent_class_list_map[import_name] = ksy_parent_class_list_map.get(import_name, list())
                            if parent_class_name not in ksy_parent_class_list_map[import_name]:
                                ksy_parent_class_list_map[import_name].append(parent_class_name)
                            break
                    # check next compiled file
        ksy_parent_imports = dict()
        for import_name in ksy_parent_file_dict_map.keys():
            if import_name in self.ksy_import_map.keys():
                continue
            ksy_parent_imports[import_name] = ksy_parent_class_list_map.get(import_name, list())
        return ksy_parent_imports

    def ksy_process_imports(self, root_name):
        process_import_list = list()
        for ksy_class_dict in self.ksy_class_dict_list_from_root_name(root_name):
            for ksy_param_dict in ksy_class_dict.get('params', list()):
                process_value = ksy_param_dict.get('-process-value', None)
                if process_value:
                    process_import = process_value.split('(')[0]
                    if process_import not in process_import_list:
                        process_import_list.append(process_import)
            for ksy_seq_dict in ksy_class_dict.get('seq', list()):
                process = ksy_seq_dict.get('process', None)
                if process:
                    process_import = process.split('(')[0]
                    if process_import not in process_import_list:
                        process_import_list.append(process_import)
                process_value = ksy_seq_dict.get('-process-value', None)
                if process_value:
                    process_import = process_value.split('(')[0]
                    if process_import not in process_import_list:
                        process_import_list.append(process_import)
            for ksy_instance_dict in ksy_class_dict.get('instances', dict()).values():
                process_value = ksy_instance_dict.get('-process-value', None)
                if process_value:
                    process_import = process_value.split('(')[0]
                    if process_import not in process_import_list:
                        process_import_list.append(process_import)
        return process_import_list

    def find_ksy_enum_dict_from_enum_name(self, root_name, class_name, enum_name):
        ksy_enum_dict = self.cache_ksy_enum_dict_map.get(root_name, dict()).get(class_name, dict()).get(enum_name, None)
        if ksy_enum_dict is not None:
            return ksy_enum_dict
        ksy_class_dict = self.find_ksy_class_dict_from_class_name(root_name, class_name)
        for name, ksy_enum_dict in ksy_class_dict.get('enums', dict()).items():
            if name == enum_name:
                self.cache_ksy_enum_dict_map[root_name] = self.cache_ksy_enum_dict_map.get(root_name, dict())
                self.cache_ksy_enum_dict_map[root_name][class_name] = self.cache_ksy_enum_dict_map[root_name].get(class_name, dict())
                self.cache_ksy_enum_dict_map[root_name][class_name][enum_name] = ksy_enum_dict
                return ksy_enum_dict
        ksy_file_dict = self.find_ksy_file_dict_from_root_name(root_name)
        for name, ksy_enum_dict in ksy_file_dict.get('enums', dict()).items():
            if name == enum_name:
                self.cache_ksy_enum_dict_map[root_name] = self.cache_ksy_enum_dict_map.get(root_name, dict())
                self.cache_ksy_enum_dict_map[root_name][class_name] = self.cache_ksy_enum_dict_map[root_name].get(class_name, dict())
                self.cache_ksy_enum_dict_map[root_name][class_name][enum_name] = ksy_enum_dict
                return ksy_enum_dict
        raise Exception("enum '{}' not found in import '{}'".format(enum_name, root_name))

    def find_enum_owner_class_name(self, root_name, class_name, enum_name):
        ksy_class_dict = self.find_ksy_class_dict_from_class_name(root_name, class_name)
        for name, _ in ksy_class_dict.get('enums', dict()).items():
            if name == enum_name:
                return class_name
        ksy_file_dict = self.find_ksy_file_dict_from_root_name(root_name)
        for name, _ in ksy_file_dict.get('enums', dict()).items():
            if name == enum_name:
                return root_name
        raise Exception("enum '{}' not found in import '{}'".format(enum_name, root_name))

    def generate_switchon_member_name(self, name_generator, member_name, case_expr, type_expr):
        if name_generator == 'default':
            # focus on being unique - use both member name and enumerator to make it unqiue
            if case_expr == '_':
                # for the default case, use the member name
                name = member_name
            elif case_expr.count('::') != 0:
                # use the enumerator name
                enum_value_name = case_expr.split('::')[1]
                enum_value_name = enum_value_name.split('.')[0]
                name = '{}_{}'.format(member_name, enum_value_name)
            else:
                # case is a number, use the member name appended with the case number
                name = '{}_{}'.format(member_name, case_expr)
        elif name_generator == 'enum':
            # force the use of enumerators for names if present
            if case_expr == '_':
                # for the default case, use the member name
                name = member_name
            elif case_expr.count('::') != 0:
                # use the enumerator name
                enum_value_name = case_expr.split('::')[1]
                enum_value_name = enum_value_name.split('.')[0]
                name = enum_value_name
            else:
                # case is a number, use the member name appended with the case number
                name = '{}_{}'.format(member_name, case_expr)
        elif name_generator == 'type':
            # force the use of type names
            name = type_expr.split('(')[0].strip()
        else:
            raise Exception("member '{}' has an unknown switch-on name generator of '{}' [-name: {}]".format(member_name, name_generator, name_generator))
        return name

    def expand_switchon_to_ksy_member_dict_list(self, ksy_member_dict):
        member_name = ksy_member_dict['id']
        size_expr = ksy_member_dict.get('size', None)
        size_eos = ksy_member_dict.get('size-eos', None)
        process_expr = ksy_member_dict.get('process', None)
        process_value_expr = ksy_member_dict.get('-process-value', None)
        has_expr = ksy_member_dict.get('if', 'true')
        ksy_type = ksy_member_dict.get('type', None)
        if not isinstance(ksy_type, dict):
            return [ksy_member_dict]
        if process_value_expr:
            raise NotImplementedError("Don't know how to generate process-value into different types for switch-on")
        switch_on_expr = ksy_type['switch-on']
        name_generator = ksy_type.get('-name', 'default')
        ksy_cases = ksy_type.get('cases', dict())
        set_list = ksy_member_dict.get('-set', list())
        default_expr = '({})'.format(has_expr)
        ksy_member_dict_list = list()
        member_name_list = list()
        for case_expr, type_expr in ksy_cases.items():
            case_expr = str(case_expr)
            ksy_member_dict = dict()
            ksy_member_dict['id'] = self.generate_switchon_member_name(name_generator, member_name, case_expr, type_expr)
            ksy_member_dict['type'] = type_expr
            if size_expr is not None:
                ksy_member_dict['size'] = size_expr
            if size_eos is not None:
                ksy_member_dict['size-eos'] = size_eos
            if process_expr is not None:
                ksy_member_dict['process'] = process_expr
            if case_expr == '_':
                ksy_member_dict['if'] = None  # fill this in later
            else:
                ksy_member_dict['-set'] = set_list
                ksy_member_dict['-case'] = case_expr
                ksy_member_dict['if'] = '({}) and (({}) == ({}))'.format(has_expr, case_expr, switch_on_expr)
                default_expr += ' and ({} != {})'.format(case_expr, switch_on_expr)
            ksy_member_dict_list.append(ksy_member_dict)
            member_name_list.append(ksy_member_dict['id'])
        for ksy_member_dict in ksy_member_dict_list:
            if not ksy_member_dict['if']:
                ksy_member_dict['if'] = default_expr
        for ksy_member_dict in ksy_member_dict_list:
            name = ksy_member_dict['id']
            count = member_name_list.count(name)
            assert count > 0
            if count == 1:
                continue
            raise Exception("member '{}' has duplicate switch-on member names of '{}' [-name: {}]".format(member_name, name, name_generator))
        return ksy_member_dict_list

    def ksy_param_dict_list(self, root_name, class_name):
        ksy_member_dict_list = self.cache_ksy_param_dict_list_map.get(root_name, dict()).get(class_name, None)
        if ksy_member_dict_list is not None:
            return ksy_member_dict_list
        ksy_class_dict = self.find_ksy_class_dict_from_class_name(root_name, class_name)
        ksy_member_dict_list = list()
        for ksy_param_dict in ksy_class_dict.get('params', list()):
            for ksy_member_dict in self.expand_switchon_to_ksy_member_dict_list(ksy_param_dict):
                ksy_member_dict_list.append(ksy_member_dict)
        self.cache_ksy_param_dict_list_map[root_name] = self.cache_ksy_param_dict_list_map.get(root_name, dict())
        self.cache_ksy_param_dict_list_map[root_name][class_name] = ksy_member_dict_list
        return ksy_member_dict_list

    def convert_contents_to_default(self, ksy_member_dict):
        contents = ksy_member_dict['contents']
        default_value = None
        if isinstance(contents, str):
            default_value = contents.encode('ascii')
        elif isinstance(contents, bytes):
            default_value = contents
        elif isinstance(contents, list):
            default_value = b''
            for value in contents:
                if isinstance(value, str):
                    default_value += value.encode('ascii')
                elif isinstance(value, bytes):
                    default_value += value
                elif isinstance(value, int):
                    default_value += struct.pack('<B', value)
                else:
                    raise NotImplementedError(repr(value))
        else:
            raise NotImplementedError(repr(contents))
        assert default_value is not None
        if 'type' in ksy_member_dict:
            del ksy_member_dict['type']
        ksy_member_dict['size'] = len(default_value)
        ksy_member_dict['-default'] = default_value
        return ksy_member_dict

    def ksy_seq_dict_list(self, root_name, class_name):
        ksy_member_dict_list = self.cache_ksy_seq_dict_list_map.get(root_name, dict()).get(class_name, None)
        if ksy_member_dict_list is not None:
            return ksy_member_dict_list
        ksy_class_dict = self.find_ksy_class_dict_from_class_name(root_name, class_name)
        ksy_member_dict_list = list()
        for ksy_seq_dict in ksy_class_dict.get('seq', list()):
            for ksy_member_dict in self.expand_switchon_to_ksy_member_dict_list(ksy_seq_dict):
                if 'contents' in ksy_member_dict:
                    ksy_member_dict = self.convert_contents_to_default(ksy_member_dict)
                ksy_member_dict_list.append(ksy_member_dict)
        self.cache_ksy_seq_dict_list_map[root_name] = self.cache_ksy_seq_dict_list_map.get(root_name, dict())
        self.cache_ksy_seq_dict_list_map[root_name][class_name] = ksy_member_dict_list
        return ksy_member_dict_list

    def split_pre_post_pos_instances(self, root_name, class_name, ksy_seq_dict, ksy_pos_instances_dict_list):
        ksy_pre_pos_instances_dict_list = list()
        ksy_post_pos_instances_dict_list = list()


        # TODO: hack to get tests to pass
        for ksy_pos_instances_dict in ksy_pos_instances_dict_list:
            pos_expr = str(ksy_pos_instances_dict['pos']).strip()
            is_value = False
            try:
                int(pos_expr, 0)
                is_value = True
            except ValueError:
                pass
            if is_value:
                ksy_pre_pos_instances_dict_list.append(ksy_pos_instances_dict)
            else:
                ksy_post_pos_instances_dict_list.append(ksy_pos_instances_dict)
        '''
        for ksy_pos_instances_dict in ksy_pos_instances_dict_list:
            pos_expr = str(ksy_pos_instances_dict['pos']).strip()

            # if the position instance is at an absolute position, it has 
            is_value = False
            try:
                int(pos_expr, 0)
                is_value = True
            except ValueError:
                pass
            if is_value:
                
        '''
        # TODO 
        # for ksy
        '''
            expr_list = ExpressionParser.split_expr_list(expr)
            expr_tree = ExpressionParser.create_expr_tree(expr_list)
            expr_tree = ExpressionParser.reduce_expr_tree(expr_tree)
        '''

        return ksy_pre_pos_instances_dict_list, ksy_post_pos_instances_dict_list

    def ksy_sorted_pos_instances_dict_list(self, root_name, class_name):
        # TODO: pos instances may depend on each other to be read first
        return self.ksy_pos_instances_dict_list(root_name, class_name)

    def ksy_sorted_seq_instance_dict_list(self, root_name, class_name):
        ksy_seq_dict_list = self.ksy_seq_dict_list(root_name, class_name)
        ksy_pos_instances_dict_list = self.ksy_sorted_pos_instances_dict_list(root_name, class_name)

        # stop early if there is no sorting to be done
        if len(ksy_seq_dict_list) == 0:
            return ksy_pos_instances_dict_list
        if len(ksy_pos_instances_dict_list) == 0:
            return ksy_seq_dict_list

        # TODO: create dependency maps
        # TODO: _io in 'if' statements

        # sort
        ksy_member_dict_list = list()
        for ksy_seq_dict in ksy_seq_dict_list:

            # if no more position instances, just append the remaining sequence members
            if len(ksy_pos_instances_dict_list) == 0:
                # no more sorting needed, just append
                ksy_member_dict_list.append(ksy_seq_dict)
                continue

            # sort instances before/after this sequence member
            ksy_pre_pos_instances_dict_list, ksy_pos_instances_dict_list = self.split_pre_post_pos_instances(root_name, class_name, ksy_seq_dict, ksy_pos_instances_dict_list)

            # place position instances required for the sequence member first
            ksy_member_dict_list.extend(ksy_pre_pos_instances_dict_list)

            # then place the sequence member
            ksy_member_dict_list.append(ksy_seq_dict)

        # append position instances that are not needed for any sequence member
        ksy_member_dict_list.extend(ksy_pos_instances_dict_list)

        # done
        return ksy_member_dict_list

    def ksy_member_dict_list(self, root_name, class_name):
        ksy_member_dict_list = list()
        ksy_member_dict_list.extend(self.ksy_param_dict_list(root_name, class_name))
        ksy_member_dict_list.extend(self.ksy_seq_dict_list(root_name, class_name))
        ksy_member_dict_list.extend(self.ksy_pos_instances_dict_list(root_name, class_name))
        return ksy_member_dict_list

    def ksy_class_id_dict(self, root_name, class_name):
        ksy_member_dict_list = list()
        ksy_member_dict_list.extend(self.ksy_param_dict_list(root_name, class_name))
        ksy_member_dict_list.extend(self.ksy_seq_dict_list(root_name, class_name))
        ksy_member_dict_list.extend(self.ksy_pos_instances_dict_list(root_name, class_name))
        ksy_member_dict_list.extend(self.ksy_value_instances_dict_list(root_name, class_name))
        ksy_class_id_dict = dict()
        id_ = 1
        for ksy_member_dict in ksy_member_dict_list:
            name = ksy_member_dict['id']
            ksy_class_id_dict[name] = id_
            id_ += 1
        return ksy_class_id_dict

    def ksy_class_id_count(self, root_name, class_name):
        ksy_class_id_dict = self.ksy_class_id_dict(root_name, class_name)
        return len(ksy_class_id_dict.values()) + 1

    def ksy_class_id(self, root_name, class_name, name=None):
        if not name:
            return 0
        return self.ksy_class_id_dict(root_name, class_name)[name]

    def find_ksy_member_dict_from_member_name(self, root_name, class_name, member_name):
        ksy_member_dict = self.cache_ksy_member_dict_map.get(root_name, dict()).get(class_name, dict()).get(member_name, None)
        if ksy_member_dict is not None:
            return ksy_member_dict
        for ksy_member_dict in self.ksy_param_dict_list(root_name, class_name):
            if ksy_member_dict['id'] == member_name:
                self.cache_ksy_member_dict_map[root_name] = self.cache_ksy_member_dict_map.get(root_name, dict())
                self.cache_ksy_member_dict_map[root_name][class_name] = self.cache_ksy_member_dict_map[root_name].get(class_name, dict())
                self.cache_ksy_member_dict_map[root_name][class_name][member_name] = ksy_member_dict
                return ksy_member_dict
        for ksy_member_dict in self.ksy_seq_dict_list(root_name, class_name):
            if ksy_member_dict['id'] == member_name:
                self.cache_ksy_member_dict_map[root_name] = self.cache_ksy_member_dict_map.get(root_name, dict())
                self.cache_ksy_member_dict_map[root_name][class_name] = self.cache_ksy_member_dict_map[root_name].get(class_name, dict())
                self.cache_ksy_member_dict_map[root_name][class_name][member_name] = ksy_member_dict
                return ksy_member_dict
        for ksy_member_dict in self.ksy_pos_instances_dict_list(root_name, class_name):
            if ksy_member_dict['id'] == member_name:
                self.cache_ksy_member_dict_map[root_name] = self.cache_ksy_member_dict_map.get(root_name, dict())
                self.cache_ksy_member_dict_map[root_name][class_name] = self.cache_ksy_member_dict_map[root_name].get(class_name, dict())
                self.cache_ksy_member_dict_map[root_name][class_name][member_name] = ksy_member_dict
                return ksy_member_dict
        raise Exception("member '{}' not found in class '{}' in import '{}'".format(member_name, class_name, root_name))

    def find_ksy_member_dict_from_param_index(self, root_name, class_name, param_index):
        ksy_class_dict = self.find_ksy_class_dict_from_class_name(root_name, class_name)
        ksy_params_list = ksy_class_dict.get('params', list())
        if param_index >= len(ksy_params_list):
            raise Exception("class name '{}' only has {} parameter(s), you are accessing index {}".format(class_name, len(ksy_params_list), param_index))
        member_name = ksy_params_list[param_index]['id']
        return self.find_ksy_member_dict_from_member_name(root_name, class_name, member_name)

    def ksy_value_instances_dict_list(self, root_name, class_name):
        ksy_value_instances_dict_list = list()
        ksy_class_dict = self.find_ksy_class_dict_from_class_name(root_name, class_name)
        ksy_instances_dict = ksy_class_dict.get('instances', dict())
        for name, ksy_dict in ksy_instances_dict.items():
            if 'pos' in ksy_dict.keys():
                # instances at a position are treated as members
                # do not return them here
                continue
            ksy_value_instance_dict = dict()
            ksy_value_instance_dict.update(ksy_dict)
            ksy_value_instance_dict['id'] = name
            ksy_value_instances_dict_list.append(ksy_value_instance_dict)
        return ksy_value_instances_dict_list

    def ksy_pos_instances_dict_list(self, root_name, class_name):
        ksy_member_dict_list = list()
        ksy_class_dict = self.find_ksy_class_dict_from_class_name(root_name, class_name)
        ksy_instances_dict = ksy_class_dict.get('instances', dict())
        for name, ksy_dict in ksy_instances_dict.items():
            if 'pos' not in ksy_dict.keys():
                # instances at a position are treated as members
                # do not return value instances
                continue
            ksy_value_instance_dict = dict()
            ksy_value_instance_dict.update(ksy_dict)
            ksy_value_instance_dict['id'] = name
            for ksy_member_dict in self.expand_switchon_to_ksy_member_dict_list(ksy_value_instance_dict):
                if 'contents' in ksy_member_dict:
                    ksy_member_dict = self.convert_contents_to_default(ksy_member_dict)
                ksy_member_dict_list.append(ksy_member_dict)
        ksy_member_dict_list = list(sorted(ksy_member_dict_list, key = lambda d: d['id']))
        return ksy_member_dict_list

    def find_ksy_value_instance_dict_from_name(self, root_name, class_name, instance_name):
        for ksy_value_instance_dict in self.ksy_value_instances_dict_list(root_name, class_name):
            if instance_name == ksy_value_instance_dict['id']:
                return ksy_value_instance_dict
        raise Exception("value instance '{}' not found in class '{}' from import '{}".format(instance_name, class_name, root_name))

    def is_value_instance(self, root_name, class_name, instance_name):
        instance_names = [d['id'] for d in self.ksy_value_instances_dict_list(root_name, class_name)]
        return instance_name in instance_names

    def find_ksy_string_encoding(self, root_name, class_name=None, member_name=None):
        if not class_name and not member_name:
            ksy_file_dict = self.find_ksy_file_dict_from_root_name(root_name)
            encoding = ksy_file_dict['meta'].get('encoding', None)
            if not encoding:
                raise Exception("meta.encoding missing from import '{}'".format(root_name))
            return encoding
        if not member_name:
            ksy_class_dict = self.find_ksy_class_dict_from_class_name(root_name, class_name)
            encoding = ksy_class_dict.get('encoding', None)
            if not encoding:
                return self.find_ksy_string_encoding(root_name)
            return encoding
        ksy_member_dict = self.find_ksy_member_dict_from_member_name(root_name, class_name, member_name)
        encoding = ksy_member_dict.get('encoding', None)
        if not encoding:
            return self.find_ksy_string_encoding(root_name, class_name)
        return encoding

    def find_ksy_endian(self, root_name, class_name=None, member_name=None):
        if not class_name and not member_name:
            ksy_file_dict = self.find_ksy_file_dict_from_root_name(root_name)
            endian = ksy_file_dict['meta'].get('endian', None)
            if not endian:
                raise Exception("meta.endian missing from import '{}'".format(root_name))
            assert endian in ['be', 'le']
            return endian
        if not member_name:
            ksy_class_dict = self.find_ksy_class_dict_from_class_name(root_name, class_name)
            endian = ksy_class_dict.get('endian', None)
            if not endian:
                return self.find_ksy_endian(root_name)
            assert endian in ['be', 'le']
            return endian
        ksy_member_dict = self.find_ksy_member_dict_from_member_name(root_name, class_name, member_name)
        endian = KilTypeInteger.convert_ksy_type_to_endian(ksy_member_dict['type'], default=None)
        if endian:
            return endian
        endian = KilTypeFloat.convert_ksy_type_to_endian(ksy_member_dict['type'], default=None)
        if endian:
            return endian
        endian = ksy_member_dict.get('endian', None)
        if not endian:
            return self.find_ksy_endian(root_name, class_name)
        assert endian in ['be', 'le']
        return endian

    def find_ksy_bitalign(self, root_name, class_name=None, member_name=None):
        if not class_name and not member_name:
            ksy_file_dict = self.find_ksy_file_dict_from_root_name(root_name)
            bitalign = ksy_file_dict['meta'].get('-bitalign', 8)
            if bitalign is None:
                raise Exception("meta.bitalign missing from import '{}'".format(root_name))
            assert bitalign in [1, 8]
            return bitalign
        if not member_name:
            ksy_class_dict = self.find_ksy_class_dict_from_class_name(root_name, class_name)
            bitalign = ksy_class_dict.get('-bitalign', None)
            if bitalign is None:
                return self.find_ksy_bitalign(root_name)
            assert bitalign in [1, 8]
            return bitalign
        ksy_member_dict = self.find_ksy_member_dict_from_member_name(root_name, class_name, member_name)
        bitalign = ksy_member_dict.get('-bitalign', None)
        if bitalign is None:
            return self.find_ksy_bitalign(root_name, class_name)
        assert bitalign in [1, 8]
        return bitalign

    def ksy_update_dict_list(self, root_name, class_name, member_name=None, instance_name=None):
        if member_name:
            ksy_member_dict = self.find_ksy_member_dict_from_member_name(root_name, class_name, member_name)
            return ksy_member_dict.get('-set', list())
        if instance_name:
            ksy_instance_dict = self.find_ksy_value_instance_dict_from_name(root_name, class_name, instance_name)
            return ksy_instance_dict.get('-set', list())
        ksy_class_dict = self.find_ksy_class_dict_from_class_name(root_name, class_name)
        return ksy_class_dict.get('-update', list())

    def cache_class_member_type(self, class_name, member_name, type_):
        key = '{}.{}'.format(class_name, member_name)
        self.cache_ksy_global_member_type_dict_map[key] = kil_type_to_dict(type_)

    def cache_lookup_class_member_type(self, class_name, member_name):
        key = '{}.{}'.format(class_name, member_name)
        type_dict = self.cache_ksy_global_member_type_dict_map.get(key, None)
        if type_dict:
            return kil_type_from_dict(type_dict)
        return None

    def cache_class_member_is_list(self, class_name, member_name, list_):
        assert list_ in [True, False]
        key = '{}.{}'.format(class_name, member_name)
        self.cache_ksy_global_member_is_list_map[key] = list_

    def cache_lookup_class_member_is_list(self, class_name, member_name):
        key = '{}.{}'.format(class_name, member_name)
        return self.cache_ksy_global_member_is_list_map.get(key, None)
