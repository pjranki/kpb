import os
import sys

from .kil_types import *
from .kil_ksy_library import *
from .kil_io_interface import *
from .kil_member_generic_interface import *


class KilMemberStringInterface(KilMemberGenericInterface):

    def constructor_default(self, expr_compiler):
        return KilLiteral(self.ksy_member_dict.get('-default', ""), self.type_)

    def get(self):
        method_call = super(KilMemberStringInterface, self).get()
        method_call.b.decl_variable.reference = True
        return method_call

    def destructor_default(self):
        return KilLiteral("", self.type_)

    def set(self, value_variable):
        method_call = super(KilMemberStringInterface, self).set(value_variable)
        # FUTURE: this may break things in future
        # method_call.b.arg_variables[0].reference = True
        return method_call
