import os
import sys

from .kil_types import *
from .kil_ksy_library import *
from .kil_io_interface import *
from .kil_member_generic_interface import *


class KilMemberFloatInterface(KilMemberGenericInterface):

    def constructor_default(self, expr_compiler):
        default = self.ksy_member_dict.get('-default', 0.0)
        if isinstance(default, int):
            default = float(default)
        if isinstance(default, str):
            default = float(default)
        return KilLiteral(default, self.type_)

    def destructor_default(self):
        return KilLiteral(0.0, self.type_)

    def atindex(self, index_variable):
        method_call = super(KilMemberFloatInterface, self).atindex(index_variable)
        method_call.b.decl_variable.reference = False
        return method_call
