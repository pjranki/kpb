import os
import sys

from .kil_types import *
from .kil_ksy_library import *
from .kil_io_interface import *
from .kil_member_string_interface import *
from .kil_member_generic_compiler import *


class KilMemberStringCompiler(KilMemberGenericCompiler):

    def compile_methods(self):
        list_ = KilMemberGenericInterface(self.root_name, self.class_name, self.member_name, None, self.ksy).list_
        methods = list()
        if not list_:
            methods.append(self.compile_getter_method())
            methods.append(self.compile_size_method())
            methods.append(self.compile_setter_method())
        else:
            methods.append(self.compile_getlist_method())
            methods.append(self.compile_count_method())
            methods.append(self.compile_atindex_method())
            methods.append(self.compile_sizeat_method())
            methods.append(self.compile_add_method())
        methods.append(self.compile_clear_method())
        methods.append(self.compile_has_method())
        methods.append(self.compile_randomize_method())
        return methods

    def compile_getter_method(self):
        method = super(KilMemberStringCompiler, self).compile_getter_method()
        if isinstance(method.code_block.children[-1].return_, KilVariable):
            # HACK: for tmp variable used in process-value
            method.decl_variable.variable.reference = False
            method.code_block.children[-1].return_.reference = False
        return method

    def compile_setter_method(self):
        method = super(KilMemberStringCompiler, self).compile_setter_method()
        method.arg_variables[0].variable.reference = True
        return method

    def compile_read_call(self):
        this_member = KilMemberStringInterface(self.root_name, self.class_name, self.member_name, None, self.ksy)
        ksy_member_dict = this_member.ksy_member_dict
        type_ = this_member.type_
        size_expr = ksy_member_dict.get('size', None)
        size_variable = None
        if size_expr is not None:
            size_variable = self.expr_compiler(this_mutable=True).compile_expr(size_expr, KilTypeSize())
        term_expr = ksy_member_dict.get('terminator', 0 if ksy_member_dict['type'].lower() == 'strz' else None)
        term_variable = None
        if term_expr is not None:
            term_variable = self.expr_compiler(this_mutable=True).compile_expr(term_expr, type_.char_type)
        _io = KilIoInterface()
        return _io.read_string(type_, size_variable, term_variable)

    def compile_write_call(self, value_variable):
        this_member = KilMemberStringInterface(self.root_name, self.class_name, self.member_name, None, self.ksy)
        ksy_member_dict = this_member.ksy_member_dict
        type_ = this_member.type_
        size_expr = ksy_member_dict.get('size', None)
        size_variable = None
        if size_expr is not None:
            size_variable = self.expr_compiler(this_mutable=False).compile_expr(size_expr, KilTypeSize())
        term_expr = ksy_member_dict.get('terminator', 0 if ksy_member_dict['type'].lower() == 'strz' else None)
        term_variable = None
        if term_expr is not None:
            term_variable = self.expr_compiler(this_mutable=False).compile_expr(term_expr, type_.char_type)
        _io = KilIoInterface()
        return _io.write_string(value_variable, size_variable, term_variable)
