import os
import sys

from .kil_types import *
from .kil_ksy_library import *
from .kil_io_interface import *
from .kil_member_generic_interface import *


class KilMemberIntegerInterface(KilMemberGenericInterface):

    def constructor_default(self, expr_compiler):
        default = self.ksy_member_dict.get('-default', 0)
        if isinstance(default, str):
            if default.count('::') != 0:
                return expr_compiler.compile_expr(default, self.type_)
            default = int(default, 0)
        return KilLiteral(default, self.type_)

    def destructor_default(self):
        return KilLiteral(0, self.type_)

    def atindex(self, index_variable):
        method_call = super(KilMemberIntegerInterface, self).atindex(index_variable)
        method_call.b.decl_variable.reference = False
        return method_call
