import os
import sys

from .kil_types import *
from .kil_ksy_library import *
from .kil_member_generic_interface import *


class KilMemberClassInterface(KilMemberGenericInterface):

    @property
    def param_expr_args(self):
        ksy_type = self.ksy_member_dict['type']
        index = ksy_type.find('(')
        if index != -1:
            assert ksy_type.endswith(')')
            return [arg.strip() for arg in ksy_type[index:][1:-1].split(',')]
        return list()

    def constructor_default(self, expr_compiler):
        return KilLiteral(None, self.type_)

    def destructor_default(self):
        return KilLiteral(None, self.type_)

    def get(self):
        method_call = super(KilMemberClassInterface, self).get()
        method_call.b.decl_variable.reference = True
        return method_call

    def mutable(self):
        method_call = KilMethodCall()
        method_call.class_type = self.class_type
        method_call.mutable = True
        method_call.public = True
        method_call.decl_variable = KilVariable()
        if not self.list_:
            method_call.decl_variable.name = 'mutable_' + self.member_name
        else:
            method_call.decl_variable.name = 'add_' + self.member_name
        method_call.decl_variable.type_ = self.type_
        method_call.decl_variable.mutable = True
        method_call.decl_variable.reference = True
        method_call.decl_variable.list_ = False
        method_call.arg_variables = []
        method_call = KilOperation(self.this(mutable=True), '.', method_call)
        return method_call

    def copy_from(self, value_variable):
        method_call = KilMethodCall()
        method_call.mutable = True
        method_call.public = True
        method_call = KilMethodCall()
        method_call.class_type = self.type_
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'copy_from'
        method_call.decl_variable.type_ = KilTypeBoolean()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = [value_variable]
        method_call = KilOperation(self.mutable(), '.', method_call)
        return method_call

    def set(self, value_variable):
        if isinstance(value_variable, KilOperation) and isinstance(value_variable.b, KilMethodCall) and value_variable.b.decl_variable.name == 'read':
            # HACK: this is only used in read, which we already handle with 'mutable'
            #       just pass through the value
            return value_variable
        return self.copy_from(value_variable)

    def add(self, value_variable):
        if isinstance(value_variable, KilOperation) and isinstance(value_variable.b, KilMethodCall) and value_variable.b.decl_variable.name == 'read':
            # HACK: this is only used in read, which we already handle with 'mutable'
            #       just pass through the value
            return value_variable
        return self.copy_from(value_variable)

    def count(self):
        return super(KilMemberClassInterface, self).size()

    def size(self):
        method_call = KilMethodCall()
        method_call.class_type = self.class_type
        method_call.mutable = False
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = self.member_name + '_calc_size'
        method_call.decl_variable.type_ = KilTypeSize()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = list()
        method_call = KilOperation(self.this(mutable=False), '.', method_call)
        return method_call

    def sizeat(self, index_variable):
        method_call = KilMethodCall()
        method_call.class_type = self.class_type
        method_call.mutable = False
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = self.member_name + '_calc_size_at'
        method_call.decl_variable.type_ = KilTypeSize()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = [index_variable]
        method_call = KilOperation(self.this(mutable=False), '.', method_call)
        return method_call

    def randomize_member(self):
        method_call = KilMethodCall()
        method_call.mutable = True
        method_call.public = True
        method_call = KilMethodCall()
        method_call.class_type = self.type_
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'randomize'
        method_call.decl_variable.type_ = KilTypeBoolean()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = []
        method_call = KilOperation(self.mutable(), '.', method_call)
        return method_call
