import os
import sys

from .kil_types import *
from .kil_ksy_library import *
from .kil_enum_compiler import *


class KilEnumValueInterface(object):

    def __init__(self, root_name=None, class_name=None, enum_value_name=None, ksy=None):
        self.root_name = root_name
        self.class_name = class_name
        self.enum_value_name = enum_value_name
        self.ksy = ksy

    @property
    def value(self):
        assert self.enum_value_name.count('::') == 1
        enum_name, enum_value_name = self.enum_value_name.split('::')
        compiler = KilEnumCompiler(self.root_name, self.class_name, enum_name, self.ksy)
        enum = compiler.compile_enum()
        for enum_value in enum.enums:
            if enum_value.name == enum_value_name:
                return enum_value
        raise Exception("enum '{}' in class '{}' doesn't have a value called '{}'".format(enum_name, self.class_name, enum_value_name))
