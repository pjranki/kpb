import os
import sys

from .kil_types import *
from .kil_ksy_library import *
from .kil_expr_compiler import *
from .kil_update_compiler import *
from .kil_io_interface import *
from .kil_member_interface import *
from .kil_process_interface import *
from .kil_process_value_interface import *


class KilMemberGenericCompiler(object):

    def __init__(self, root_name, class_name, member_name, ksy):
        self.root_name = root_name
        self.class_name = class_name
        self.member_name = member_name
        self.ksy = ksy

    def expr_compiler(self, this_mutable):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = True
        this.reference = True
        return KilExprCompiler(self.root_name, self.class_name, self.member_name, class_variable=this, ksy=self.ksy)

    def compile_member(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = True
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        variable = KilVariable()
        variable.name = self.member_name
        variable.type_ = this_member.type_
        variable.mutable = True
        variable.reference = False
        variable.list_ = this_member.list_
        member = KilMember()
        member.class_type = this_member.class_type
        member.variable = KilDeclaration(variable)
        member.public = False
        return member

    def compile_has_method(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = False
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        method = KilMethod()
        method.class_type = this_member.class_type
        method.mutable = False
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = 'has_' + self.member_name
        method.decl_variable.type_ = KilTypeBoolean()
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        method.code_block = KilBlock()
        if_expr = this_member.ksy_member_dict.get('if', 'true')
        if self.expr_compiler(this_mutable=False).expr_contains_io(if_expr):
            # has method has no access to io streams (they do not existing outside of parsing/serializing)
            # so I've made the decision that the variable always exists in the case
            has_variable = KilLiteral(True, KilTypeBoolean())
        else:
            has_variable = self.expr_compiler(this_mutable=False).compile_expr(if_expr, KilTypeBoolean())
        method.code_block.children.append(KilReturn(has_variable))
        return method

    def compile_clear_block(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = True
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        if not this_member.list_:
            return KilBlock([KilOperation(this_member.value, '=', this_member.constructor_default(self.expr_compiler(this_mutable=True)))])
        else:
            return KilBlock([KilOperation(this_member.value, 'clr')])

    def compile_destructor_block(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = True
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        if not this_member.list_:
            return KilBlock([KilOperation(this_member.value, '=', this_member.destructor_default())])
        else:
            return KilBlock([KilOperation(this_member.value, 'clr')])

    def compile_clear_method(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = True
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        method = KilMethod()
        method.class_type = this_member.class_type
        method.mutable = True
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = 'clear_' + self.member_name
        method.decl_variable.type_ = KilTypeNone()
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        method.code_block = KilBlock()

        # add code to clear this member
        method.code_block.children.extend(self.compile_clear_block().children)

        # call update on this class (not this member)
        this_class = KilClassInterface(self.root_name, self.class_name, this, self.ksy)
        method.code_block.children.append(this_class.update())

        # done
        return method

    def compile_getter_method(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = False
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        method = KilMethod()
        method.class_type = this_member.class_type
        method.mutable = False
        method.public = True
        method.hint_treat_as_member_variable = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = self.member_name
        method.decl_variable.type_ = this_member.type_
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        if this_member.list_:
            method.decl_variable.reference = True  # pass a reference rather than a copy
        method.decl_variable.list_ = this_member.list_
        method.decl_variable = KilDeclaration(method.decl_variable)
        process_value_expr = this_member.ksy_member_dict.get('-process-value', None)
        if process_value_expr and not this_member.list_:
            process_value = KilProcessValueInterface(self.member_name, process_value_expr)
            tmp_variable = KilVariable()
            tmp_variable.name = '_process_value_tmp_{}'.format(self.member_name)
            tmp_variable.type_ = this_member.value.type_
            tmp_variable.mutable = False
            tmp_variable.reference = method.decl_variable.reference
            method.code_block = KilBlock([
                process_value.constructor(self.expr_compiler(this_mutable=False)),
                KilOperation(KilDeclaration(tmp_variable), '=', process_value.getter(this_member.value)),
                process_value.destructor(),
                KilReturn(tmp_variable)
            ])
        else:
            method.code_block = KilBlock([
                KilReturn(this_member.value)
            ])
        return method

    def compile_getlist_method(self):
        return self.compile_getter_method()

    def compile_setter_method(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = True
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        method = KilMethod()
        method.class_type = this_member.class_type
        method.mutable = True
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = 'set_' + self.member_name
        method.decl_variable.type_ = KilTypeBoolean()
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        value_variable = KilVariable()
        value_variable.name = '_value'
        value_variable.type_ = this_member.type_
        value_variable.mutable = False
        value_variable.reference = False
        value_variable.list_ = False
        method.arg_variables = [KilDeclaration(value_variable)]
        process_value_expr = this_member.ksy_member_dict.get('-process-value', None)
        if process_value_expr:
            process_value = KilProcessValueInterface(self.member_name, process_value_expr)
            method.code_block = KilBlock([
                KilOperation(this_member.value, '=', value_variable),
                process_value.constructor(self.expr_compiler(this_mutable=True)),
                KilOperation(this_member.value, '=', process_value.setter(this_member.value)),
                process_value.destructor()
            ])
        else:
            method.code_block = KilBlock([
                KilOperation(this_member.value, '=', value_variable),
            ])

        # changes have been made to this member, update/set everything else as needed
        method.code_block.children.extend(self.compile_update_block().children)

        # done
        method.code_block.children.append(KilReturn(KilLiteral(True, KilTypeBoolean())))
        return method

    def compile_size_method(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = False
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        method = KilMethod()
        method.class_type = this_member.class_type
        method.mutable = False
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = self.member_name + '_size'
        method.decl_variable.type_ = KilTypeSize()
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        method.code_block = KilBlock([KilReturn(KilOperation(this_member.value, 'len'))])
        return method

    def compile_randomize_method(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = True
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        method = KilMethod()
        method.class_type = this_member.class_type
        method.mutable = True
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = 'randomize_' + self.member_name
        method.decl_variable.type_ = KilTypeBoolean()
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        method.code_block = self.compile_randomize_block()
        return method

    def compile_randomize_block(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = True
        this.reference = True
        this_class = KilClassInterface(self.root_name, self.class_name, this, self.ksy)
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        update_compiler = KilUpdateCompiler(self.root_name, self.class_name, self.member_name, ksy=self.ksy)
        member_type = this_member.type_
        _io = KilIoInterface()
        ksy_member_dict = this_member.ksy_member_dict
        randomize_one_error_block = KilBlock([KilReturn(KilLiteral(False, KilTypeBoolean()))])
        randomize_one_block = KilBlock([])
        randomize_block = KilBlock([])

        # determine randomised content
        randomize_content = ksy_member_dict.get('-randomize-content', None)
        content_type = None
        content_min = None
        content_max = None
        if not randomize_content:
            pass # don't randomize the content
        elif randomize_content == 'range':
            if isinstance(member_type, KilTypeInteger) or \
                    isinstance(member_type, KilTypeBoolean) or \
                    isinstance(member_type, KilTypeFloat):
                content_type = member_type
            elif isinstance(member_type, KilTypeBuffer):
                content_type = KilTypeInteger(signed=False, bits=8)
            elif isinstance(member_type, KilTypeString):
                content_type = KilTypeInteger(signed=False, bits=32)
            else:
                raise NotImplementedError(member_type)
            content_min = self.expr_compiler(this_mutable=True).compile_expr(ksy_member_dict['-randomize-content-range-min'], content_type)
            content_max = self.expr_compiler(this_mutable=True).compile_expr(ksy_member_dict['-randomize-content-range-max'], content_type)
        else:
            raise NotImplementedError("randomize content type '{}'".format(randomize_content))

        # determine randomized size of content
        randomize_size = ksy_member_dict.get('-randomize-size', None)
        size_min = None
        size_max = None
        if not randomize_size:
            pass # don't randomize the size
        elif randomize_size == 'range':
            size_min = self.expr_compiler(this_mutable=True).compile_expr(ksy_member_dict['-randomize-size-range-min'], KilTypeSize())
            size_max = self.expr_compiler(this_mutable=True).compile_expr(ksy_member_dict['-randomize-size-range-max'], KilTypeSize())
        else:
            raise NotImplementedError("randomize size type '{}'".format(randomize_size))

        # randomize member and add it to the class
        count_variable = KilVariable('_count_{}'.format(self.member_name), KilTypeSize())
        index_variable = KilVariable('_i_{}'.format(self.member_name), KilTypeSize())
        if not randomize_content:
            pass  # don't randomize the content
        elif randomize_content == 'range':
            if not this_member.list_:
                if isinstance(member_type, KilTypeInteger) or \
                        isinstance(member_type, KilTypeBoolean) or \
                        isinstance(member_type, KilTypeFloat):
                    assert content_min is not None
                    assert content_max is not None
                    randomize_one_block.children.append(KilIf(expr=this_member.set(_io.random_value_from_range(content_min, content_max)), false_body=randomize_one_error_block))
                elif isinstance(member_type, KilTypeBuffer):
                    if size_min is None or size_max is None:
                        raise Exception("A size range must be provided to randomize a buffer")
                    randomize_one_block.children.append(KilIf(expr=_io.random_bytes_from_range(this_member.value, size_min, size_max, content_min, content_max), false_body=randomize_one_error_block))
                    randomize_one_block.children.extend(update_compiler.compile_update_block().children)  # trigger '-set' block
                    randomize_one_block.children.append(KilIf(expr=this_class.update(), false_body=randomize_one_error_block))  # trigger '-update' block
                elif isinstance(member_type, KilTypeString):
                    if size_min is None or size_max is None:
                        raise Exception("A size (i.e. length) range must be provided to randomize a string")
                    randomize_one_block.children.append(KilIf(expr=_io.random_string_from_range(this_member.value, size_min, size_max, content_min, content_max), false_body=randomize_one_error_block))
                    randomize_one_block.children.extend(update_compiler.compile_update_block().children)  # trigger '-set' block
                    randomize_one_block.children.append(KilIf(expr=this_class.update(), false_body=randomize_one_error_block))  # trigger '-update' block
                else:
                    raise NotImplementedError("randomize '{}'".format(member_type))
            else:
                if isinstance(member_type, KilTypeInteger) or \
                        isinstance(member_type, KilTypeBoolean) or \
                        isinstance(member_type, KilTypeFloat):
                    randomize_one_block.children.append(KilIf(expr=this_member.add(_io.random_value_from_range(content_min, content_max)), false_body=randomize_one_error_block))
                elif isinstance(member_type, KilTypeBuffer):
                    if size_min is None or size_max is None:
                        raise Exception("A size range must be provided to randomize a buffer")
                    mutable_atindex = KilOperation(this_member.value, '[]', index_variable)
                    randomize_one_block.children.append(KilIf(expr=this_member.add(KilLiteral(b'', member_type)), false_body=randomize_one_error_block))
                    randomize_one_block.children.append(KilIf(expr=_io.random_bytes_from_range(mutable_atindex, size_min, size_max, content_min, content_max), false_body=randomize_one_error_block))
                    randomize_one_block.children.extend(update_compiler.compile_update_block().children)  # trigger '-set' block
                    randomize_one_block.children.append(KilIf(expr=this_class.update(), false_body=randomize_one_error_block))
                elif isinstance(member_type, KilTypeString):
                    if size_min is None or size_max is None:
                        raise Exception("A size (i.e. length) range must be provided to randomize a string")
                    mutable_atindex = KilOperation(this_member.value, '[]', index_variable)
                    randomize_one_block.children.append(KilIf(expr=this_member.add(KilLiteral('', member_type)), false_body=randomize_one_error_block))
                    randomize_one_block.children.append(KilIf(expr=_io.random_string_from_range(mutable_atindex, size_min, size_max, content_min, content_max), false_body=randomize_one_error_block))
                    randomize_one_block.children.extend(update_compiler.compile_update_block().children)  # trigger '-set' block
                    randomize_one_block.children.append(KilIf(expr=this_class.update(), false_body=randomize_one_error_block))  # trigger '-update' block
                else:
                    raise NotImplementedError("randomize '{}'".format(member_type))
        else:
            raise NotImplementedError("randomize content type '{}'".format(randomize_content))

        # randomize one instance of a class (does not require any '-randomize-....' tags)
        if isinstance(member_type, KilTypeClass):
            randomize_one_block.children.append(KilIf(expr=this_member.randomize_member(), false_body=randomize_one_error_block))

        # randomize one or many
        if not this_member.list_:
            randomize_block.children.append(this_member.clear())
            randomize_block.children.extend(randomize_one_block.children)
        else:
            randomize_count = ksy_member_dict.get('-randomize-count', None)
            if not randomize_count:
                repeat_count = this_member.count()
            elif randomize_count == 'range':
                count_min = self.expr_compiler(this_mutable=True).compile_expr(ksy_member_dict['-randomize-count-range-min'], KilTypeSize())
                count_max = self.expr_compiler(this_mutable=True).compile_expr(ksy_member_dict['-randomize-count-range-max'], KilTypeSize())
                repeat_count = _io.random_value_from_range(count_min, count_max)
            else:
                raise NotImplementedError("randomize count type '{}'".format(randomize_count))
            start = KilOperation(KilDeclaration(index_variable), '=', KilLiteral(0, KilTypeSize()))
            condition = KilOperation(index_variable, '<', count_variable)
            update = KilOperation(index_variable, '++')
            randomize_block.children.extend([
                KilOperation(KilDeclaration(count_variable), '=', repeat_count),
                this_member.clear(),
                KilFor(start, condition, update, body=randomize_one_block)
            ])

        # return true on success
        randomize_block.children.append(KilReturn(KilLiteral(True, KilTypeBoolean())))

        # done
        return randomize_block

    def compile_read_block(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = True
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        _io = KilIoInterface()
        read_one_error_block = KilBlock([KilReturn(KilLiteral(False, KilTypeBoolean()))])
        read_one_block = KilBlock([])
        repeat = this_member.ksy_member_dict.get('repeat', None)
        size_expr = this_member.ksy_member_dict.get('size', None)
        size_eos = this_member.ksy_member_dict.get('size-eos', False)
        sub_stream = isinstance(this_member.type_, KilTypeClass) and (size_expr is not None or size_eos)
        process_expr = this_member.ksy_member_dict.get('process', None)
        pos_expr = this_member.ksy_member_dict.get('pos', None)
        io_expr = this_member.ksy_member_dict.get('io', '_io')

        # read member and add it to the class
        if not repeat:
            # single value
            read_one_block.children.append(KilIf(expr=this_member.set(self.compile_read_call()), false_body=read_one_error_block))
        else:
            # list of values
            read_one_block.children.append(KilIf(expr=this_member.add(self.compile_read_call()), false_body=read_one_error_block))

        # handle external processing - ksy yaml files have a 'process' tag for this
        if process_expr is not None:
            assert sub_stream  # process is expected to happen in a sub-stream
            process = KilProcessInterface(self.member_name, process_expr)
            read_one_block.children.insert(0, KilIf(expr=process.decode(_io.this), false_body=read_one_error_block))
            finally_process_destructor = KilTryCatchBlock(try_body=read_one_block, final_body=KilBlock([process.destructor()]), rethrow=True)
            read_one_block = KilBlock([
                process.constructor(self.expr_compiler(this_mutable=True)),
                finally_process_destructor
            ])

        # create a substream to restrict the amount of data that can be parsed
        if sub_stream:
            if size_expr is not None:
                stream_size = self.expr_compiler(this_mutable=True).compile_expr(size_expr, KilTypeSize())
                push_substream = _io.push_substream(size_variable=stream_size)
            else:
                push_substream = _io.push_substream()
            finally_pop_substream = KilTryCatchBlock(try_body=read_one_block, final_body=KilBlock([_io.pop_substream()]), rethrow=True)
            read_one_block = KilBlock([
                push_substream,
                finally_pop_substream
            ])

        # align before/after when parsing
        # only needed for sub-types/classes
        if isinstance(this_member.type_, KilTypeClass):
            bitalign = self.ksy.find_ksy_bitalign(self.root_name, self.class_name, self.member_name)
            if bitalign == 1:
                pass
            elif bitalign == 8:
                read_one_block.children.insert(0, _io.align_to_byte())
                read_one_block.children.append(_io.align_to_byte())
            else:
                raise NotImplementedError('bitalign {}'.format(bitalign))

        # check for errors after parsing
        read_one_block.children.append(KilIf(expr=_io.ok(), false_body=read_one_error_block))

        # reading one or many
        if not repeat:
            read_block = read_one_block
        elif repeat == 'expr':
            repeat_count = self.expr_compiler(this_mutable=True).compile_expr(this_member.ksy_member_dict['repeat-expr'], KilTypeSize())
            count_variable = KilVariable('_count_{}'.format(self.member_name), KilTypeSize())
            index_variable = KilVariable('_i_{}'.format(self.member_name), KilTypeSize())
            start = KilOperation(KilDeclaration(index_variable), '=', KilLiteral(0, KilTypeSize()))
            condition = KilOperation(index_variable, '<', count_variable)
            update = KilOperation(index_variable, '++')
            read_block = KilBlock([
                KilOperation(KilDeclaration(count_variable), '=', repeat_count),
                KilFor(start, condition, update, body=read_one_block)
            ])
        elif repeat == 'eos':
            condition = KilOperation(KilOperation(op='not', a=_io.eof()), 'and', _io.ok())
            read_block = KilBlock([
                KilWhile(condition, body=read_one_block),
                KilIf(expr=_io.ok(), false_body=KilBlock([
                    KilReturn(KilLiteral(False, KilTypeBoolean()))
                ]))
            ])
        elif repeat == 'until':
            expr_compiler = self.expr_compiler(this_mutable=True)
            expr_compiler.current_variable = this_member.atindex(KilOperation(this_member.count(), '-', KilLiteral(1, KilTypeSize())))
            repeat_until = expr_compiler.compile_expr(this_member.ksy_member_dict['repeat-until'], KilTypeBoolean())
            continue_while = KilOperation(op='not', a=repeat_until)
            condition = KilOperation(continue_while, 'and', KilOperation(KilOperation(op='not', a=_io.eof()), 'and', _io.ok()))
            read_block = KilBlock([
                KilDoWhile(condition, body=read_one_block),
                KilIf(expr=_io.ok(), false_body=KilBlock([
                    KilReturn(KilLiteral(False, KilTypeBoolean()))
                ]))
            ])
        else:
            raise NotImplementedError(repeat)

        # move into the correct position before reading
        if pos_expr is not None:
            pos_io = self.expr_compiler(this_mutable=True).compile_io_expr(io_expr)
            pos = self.expr_compiler(this_mutable=True).compile_expr(pos_expr, KilTypeSize())
            push_substream = _io.push_substream(stream_variable=pos_io, pos_variable=pos)
            finally_pop_substream = KilTryCatchBlock(try_body=read_block, final_body=KilBlock([_io.pop_substream()]), rethrow=True)
            read_block = KilBlock([
                push_substream,
                finally_pop_substream
            ])

        # wrap in if/has block
        read_block = KilBlock([KilIf(expr=this_member.read_has_expr(self.expr_compiler(this_mutable=True)), true_body=read_block)])

        # done
        return read_block

    def compile_write_block(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = False
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        _io = KilIoInterface()
        write_one_error_block = KilBlock([KilReturn(KilLiteral(False, KilTypeBoolean()))])
        write_one_block = KilBlock([])
        index_variable = KilVariable('_i_{}'.format(self.member_name), KilTypeSize())
        repeat = this_member.ksy_member_dict.get('repeat', None)
        size_expr = this_member.ksy_member_dict.get('size', None)
        size_eos = this_member.ksy_member_dict.get('size-eos', False)
        sub_stream = isinstance(this_member.type_, KilTypeClass) and (size_expr is not None or size_eos)
        process_expr = this_member.ksy_member_dict.get('process', None)
        pos_expr = this_member.ksy_member_dict.get('pos', None)
        io_expr = this_member.ksy_member_dict.get('io', '_io')

        # write member and add it to the class
        if not repeat:
            # single value
            if isinstance(this_member.type_, KilTypeClass):
                write_one_block.children.append(KilIf(expr=self.compile_write_call(this_member.get()), false_body=write_one_error_block))
            else:
                # uses an _io method, error checking happens when we check _io.ok()
                write_one_block.children.append(self.compile_write_call(this_member.get()))
        else:
            # list of values
            if isinstance(this_member.type_, KilTypeClass):
                write_one_block.children.append(KilIf(expr=self.compile_write_call(this_member.atindex(index_variable)), false_body=write_one_error_block))
            else:
                # uses an _io method, error checking happens when we check _io.ok()
                write_one_block.children.append(self.compile_write_call(this_member.atindex(index_variable)))

        # handle external processing - ksy yaml files have a 'process' tag for this
        if process_expr is not None:
            assert sub_stream  # process is expected to happen in a sub-stream
            process = KilProcessInterface(self.member_name, process_expr)
            write_one_block.children.append(KilIf(expr=process.encode(_io.this), false_body=write_one_error_block))
            finally_process_destructor = KilTryCatchBlock(try_body=write_one_block, final_body=KilBlock([process.destructor()]), rethrow=True)
            write_one_block = KilBlock([
                process.constructor(self.expr_compiler(this_mutable=False)),
                finally_process_destructor
            ])

        # create a substream to force the exact size of data to be written
        if sub_stream:
            if size_expr is not None:
                stream_size = self.expr_compiler(this_mutable=False).compile_expr(size_expr, KilTypeSize())
                push_substream = _io.push_substream(size_variable=stream_size)
            else:
                push_substream = _io.push_substream()
            finally_pop_substream = KilTryCatchBlock(try_body=write_one_block, final_body=KilBlock([_io.pop_substream()]), rethrow=True)
            write_one_block = KilBlock([
                push_substream,
                finally_pop_substream
            ])

        # align before/after when serializing
        # only needed for sub-types/classes
        if isinstance(this_member.type_, KilTypeClass):
            bitalign = self.ksy.find_ksy_bitalign(self.root_name, self.class_name, self.member_name)
            if bitalign == 1:
                pass
            elif bitalign == 8:
                write_one_block.children.insert(0, _io.align_to_byte())
                write_one_block.children.append(_io.align_to_byte())
            else:
                raise NotImplementedError('bitalign {}'.format(bitalign))

        # check for errors after writing
        write_one_block.children.append(KilIf(expr=_io.ok(), false_body=write_one_error_block))

        if not repeat:
            write_block = write_one_block
        elif repeat == 'expr':
            repeat_count = self.expr_compiler(this_mutable=False).compile_expr(this_member.ksy_member_dict['repeat-expr'], KilTypeSize())
            count_variable = KilVariable('_count_{}'.format(self.member_name), KilTypeSize())
            start = KilOperation(KilDeclaration(index_variable), '=', KilLiteral(0, KilTypeSize()))
            condition = KilOperation(index_variable, '<', count_variable)
            update = KilOperation(index_variable, '++')
            write_block = KilBlock([
                KilOperation(KilDeclaration(count_variable), '=', repeat_count),
                KilFor(start, condition, update, body=write_one_block)
            ])
        elif repeat == 'eos':
            repeat_count = this_member.count()
            count_variable = KilVariable('_count_{}'.format(self.member_name), KilTypeSize())
            start = KilOperation(KilDeclaration(index_variable), '=', KilLiteral(0, KilTypeSize()))
            condition = KilOperation(index_variable, '<', count_variable)
            update = KilOperation(index_variable, '++')
            write_block = KilBlock([
                KilOperation(KilDeclaration(count_variable), '=', repeat_count),
                KilFor(start, condition, update, body=write_one_block)
            ])
        elif repeat == 'until':
            repeat_count = this_member.count()
            count_variable = KilVariable('_count_{}'.format(self.member_name), KilTypeSize())
            expr_compiler = self.expr_compiler(this_mutable=False)
            expr_compiler.current_variable = this_member.atindex(index_variable)
            repeat_until = expr_compiler.compile_expr(this_member.ksy_member_dict['repeat-until'], KilTypeBoolean())
            start = KilOperation(KilDeclaration(index_variable), '=', KilLiteral(0, KilTypeSize()))
            condition = KilOperation(index_variable, '<', count_variable)
            update = KilOperation(index_variable, '++')
            write_one_block.children.append(KilIf(expr=repeat_until, true_body=KilBlock([
                KilBreak()  # break out of loop when condition is met
            ])))
            write_block = KilBlock([
                KilOperation(KilDeclaration(count_variable), '=', repeat_count),
                KilFor(start, condition, update, body=write_one_block)
            ])
        else:
            raise NotImplementedError(repeat)

        # move into the correct position before reading
        if pos_expr is not None:
            pos_io = self.expr_compiler(this_mutable=False).compile_io_expr(io_expr)
            pos = self.expr_compiler(this_mutable=False).compile_expr(pos_expr, KilTypeSize())
            push_substream = _io.push_substream(stream_variable=pos_io, pos_variable=pos)
            finally_pop_substream = KilTryCatchBlock(try_body=write_block, final_body=KilBlock([_io.pop_substream()]), rethrow=True)
            write_block = KilBlock([
                push_substream,
                finally_pop_substream
            ])

        # wrap in if/has block
        write_block = KilBlock([KilIf(expr=this_member.write_has_expr(self.expr_compiler(this_mutable=False)), true_body=write_block)])

        # done
        return write_block

    def compile_count_method(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = False
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        method = KilMethod()
        method.class_type = this_member.class_type
        method.mutable = False
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = self.member_name + '_size'
        method.decl_variable.type_ = KilTypeSize()
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        method.code_block = KilBlock()
        list_count = KilOperation(this_member.value, 'len')
        method.code_block.children.append(KilReturn(list_count))
        return method

    def compile_atindex_method(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = False
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        method = KilMethod()
        method.class_type = this_member.class_type
        method.mutable = False
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = self.member_name + '_at'
        method.decl_variable.type_ = this_member.type_
        method.decl_variable.mutable = False
        method.decl_variable.reference = True
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        index_variable = KilVariable('_index', KilTypeSize())
        count_variable = KilOperation(this_member.value, 'len')
        method.arg_variables = [KilDeclaration(index_variable)]
        method.code_block = KilBlock([
            KilIf(expr=KilOperation(index_variable, '>=', count_variable), true_body=KilBlock([
                KilReturn(this_member.constructor_default(self.expr_compiler(this_mutable=False))),
            ])),
            KilReturn(KilOperation(this_member.value, '[]', index_variable))
        ])
        return method

    def compile_sizeat_method(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = False
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        method = KilMethod()
        method.class_type = this_member.class_type
        method.mutable = False
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = self.member_name + '_size_at'
        method.decl_variable.type_ = KilTypeSize()
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        index_variable = KilVariable('_index', KilTypeSize())
        method.arg_variables = [KilDeclaration(index_variable)]
        count_variable = KilOperation(this_member.value, 'len')
        method.code_block = KilBlock([
            KilIf(expr=KilOperation(index_variable, '>=', count_variable), true_body=KilBlock([
                KilReturn(KilLiteral(0, KilTypeSize())),
            ])),
            KilReturn(KilOperation(KilOperation(this_member.value, '[]', index_variable), 'len'))
        ])
        return method

    def compile_add_method(self):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = True
        this.reference = True
        this_member = KilMemberInterface(self.root_name, self.class_name, self.member_name, this, self.ksy)
        method = KilMethod()
        method.class_type = this_member.class_type
        method.mutable = True
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = 'add_' + self.member_name
        method.decl_variable.type_ = KilTypeBoolean()
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        value_variable = KilVariable()
        value_variable.name = '_value'
        value_variable.type_ = this_member.type_
        value_variable.mutable = False
        value_variable.reference = False
        value_variable.list_ = False
        method.arg_variables = [KilDeclaration(value_variable)]
        add_member = KilOperation(this_member.value, 'add', value_variable)
        method.code_block = KilBlock([
            KilIf(expr=add_member, false_body=KilBlock([
                KilReturn(KilLiteral(False, KilTypeBoolean())),
            ])),
        ])

        # changes have been made to this member, update/set everything else as needed
        method.code_block.children.extend(self.compile_update_block().children)

        # done
        method.code_block.children.append(KilReturn(KilLiteral(True, KilTypeBoolean())))
        return method

    def compile_update_block(self):
        block = KilBlock()

        this_variable = KilVariable()
        this_variable.name = KilVariable.THIS_NAME
        this_variable.type_ = KilTypeClass(name=self.class_name)
        this_variable.mutable = True
        this_variable.reference = True
        this = KilClassInterface(self.root_name, self.class_name, this_variable, self.ksy)

        # don't recursively call this methods's set method
        class_id = self.ksy.ksy_class_id(self.root_name, self.class_name)
        class_member_id = self.ksy.ksy_class_id(self.root_name, self.class_name, self.member_name)
        class_id_list = [class_id, class_member_id]

        # disable update on this class (to avoid infinite recursion)
        update_block = KilBlock()
        update_block_error_handled = KilBlock()
        update_block_error_handled.children.extend(this.update_disable_block(class_id_list, backup=True).children)
        try_catch_block = KilTryCatchBlock(rethrow=True)
        try_catch_block.try_body = update_block
        update_block_error_handled.children.append(try_catch_block)

        # re-enable update on return OR error/exception
        try_catch_block.final_body = this.update_enable_block(class_id_list, restore=True)
        block.children.append(KilIf(expr=this.update_enabled(class_member_id), true_body=update_block_error_handled))

        # update member values (specific to when a change is made on this member)
        update_compiler = KilUpdateCompiler(self.root_name, self.class_name, self.member_name, ksy=self.ksy)
        update_block.children.extend(update_compiler.compile_update_block().children)

        # do a full update of the class (now that the member-specific updates are done)
        block.children.append(KilIf(expr=this.update(), false_body=KilBlock([
            KilReturn(KilLiteral(False, KilTypeBoolean())),
        ])))

        return block
