import os
import sys

from .kil_types import *
from .kil_ksy_library import *
from .kil_file_compiler import *


class KilCompiler(object):

    def __init__(self, file_path_key=None, import_map_key=None, compile_list_key=None):
        self.file_path_key = file_path_key
        self.import_map_key = import_map_key
        self.compile_list_key = compile_list_key

    def compile(self, ksy_dict, args):
        ksy = KilKsyLibrary(self.file_path_key, ksy_dict[self.import_map_key], ksy_dict[self.compile_list_key])
        compiler = KilFileCompiler(ksy_dict['meta']['id'], args.namespace, self.file_path_key, ksy)
        return compiler.compile_file()


class KilRecompileToReturnBuffer(object):

    @classmethod
    def recompile_method_to_return_buffer(cls, method):
        assert isinstance(method, KilMethod)
        assert isinstance(method.decl_variable.type_, KilTypeBoolean)
        method.decl_variable.variable.type_ = KilTypeBuffer()
        buffer_variables = [arg_variable for arg_variable in method.arg_variables if isinstance(arg_variable.type_, KilTypeBuffer)]
        assert len(buffer_variables) == 1
        return_varible = buffer_variables[0].variable
        method.decl_variable.variable.mutable = return_varible.mutable
        method.arg_variables = [arg_variable for arg_variable in method.arg_variables if not isinstance(arg_variable.type_, KilTypeBuffer)]
        method.code_block.children.insert(0, KilOperation(KilDeclaration(return_varible), '=', KilLiteral(type_=KilTypeNone())))
        for obj in method.code_block.walk():
            if isinstance(obj, KilReturn):
                obj.return_ = return_varible

    @classmethod
    def recompile_method_with_buffer_size_arg(cls, method):
        assert isinstance(method, KilMethod)
        found = False
        for arg_variable in method.arg_variables:
            if isinstance(arg_variable.type_, KilTypeBuffer):
                found = True
        if isinstance(method.decl_variable.type_, KilTypeBuffer):
            found = True
        if not found:
            return
        buffer_count = 0
        new_arg_variables = list()
        for arg_variable in method.arg_variables:
            new_arg_variables.append(arg_variable)
            if isinstance(arg_variable.type_, KilTypeBuffer):
                buffer_count += 1
                size_variable = KilVariable()
                size_variable.name = '_size'
                size_variable.type_ = KilTypeSize()
                size_variable.mutable = arg_variable.mutable
                size_variable.reference = arg_variable.mutable
                new_arg_variables.append(KilDeclaration(size_variable))
        if isinstance(method.decl_variable.type_, KilTypeBuffer):
            buffer_count += 1
            size_variable = KilVariable()
            size_variable.name = '_size'
            size_variable.type_ = KilTypeSize()
            size_variable.mutable = True
            size_variable.reference = True
            new_arg_variables.append(KilDeclaration(size_variable))
        method.arg_variables = new_arg_variables
        assert buffer_count <= 1

    @classmethod
    def recompile_method_call_to_return_buffer(cls, method_call):
        assert isinstance(method_call, KilMethodCall)
        assert isinstance(method_call.decl_variable.type_, KilTypeBoolean)
        method_call.decl_variable.type_ = KilTypeBuffer()
        buffer_variables = [arg_variable for arg_variable in method_call.arg_variables if isinstance(arg_variable.type_, KilTypeBuffer)]
        assert len(buffer_variables) == 1
        return_varible = buffer_variables[0].variable
        method_call.decl_variable.mutable = return_varible.mutable
        method_call.arg_variables = [arg_variable for arg_variable in method_call.arg_variables if not isinstance(arg_variable.type_, KilTypeBuffer)]

    @classmethod
    def recompile_method_call_with_buffer_size_arg(cls, method_call):
        assert isinstance(method_call, KilMethodCall)
        found = False
        for arg_variable in method_call.arg_variables:
            if isinstance(arg_variable.type_, KilTypeBuffer):
                found = True
        if isinstance(method_call.decl_variable.type_, KilTypeBuffer):
            found = True
        if not found:
            return
        buffer_count = 0
        method_call = copy.deepcopy(method_call)
        new_arg_variables = list()
        for arg_variable in method_call.arg_variables:
            new_arg_variables.append(arg_variable)
            if isinstance(arg_variable.type_, KilTypeBuffer):
                buffer_count += 1
                size_variable = KilVariable()
                size_variable.name = '_size'
                size_variable.type_ = KilTypeSize()
                size_variable.mutable = arg_variable.mutable
                size_variable.reference = arg_variable.mutable
                new_arg_variables.append(size_variable)
        if isinstance(method_call.decl_variable.type_, KilTypeBuffer):
            buffer_count += 1
            size_variable = KilVariable()
            size_variable.name = '_size'
            size_variable.type_ = KilTypeSize()
            size_variable.mutable = True
            size_variable.reference = True
            new_arg_variables.append(size_variable)
        method_call.arg_variables = new_arg_variables
        assert buffer_count <= 1

    def __init__(self, method_name=None):
        self.method_name = method_name

    def compile(self, kil_obj, args):
        for kil_sub_obj in kil_obj.walk():
            if isinstance(kil_sub_obj, KilMethod):
                if kil_sub_obj.decl_variable.variable.name == self.method_name:
                    if isinstance(kil_sub_obj.decl_variable.variable.type_, KilTypeBoolean):
                        self.recompile_method_to_return_buffer(kil_sub_obj)
            if isinstance(kil_sub_obj, KilMethodCall):
                if kil_sub_obj.decl_variable.name == self.method_name:
                    if isinstance(kil_sub_obj.decl_variable.type_, KilTypeBoolean):
                        self.recompile_method_call_to_return_buffer(kil_sub_obj)
        return kil_obj


class KilRecompileExceptionsToReturns(object):

    def __init__(self):
        pass

    @classmethod
    def find_try_catch_block(cls, kil_obj):
        if isinstance(kil_obj, KilBlock):
            kil_block = kil_obj
            for index in range(0, len(kil_block.children)):
                kil_obj = kil_block.children[index]
                if isinstance(kil_obj, KilTryCatchBlock):
                    return kil_obj, index, kil_block
        return None, None, None

    @classmethod
    def append_block_containing_return(cls, kil_block, kil_append_return_block):
        assert isinstance(kil_block, KilBlock)
        assert isinstance(kil_append_return_block, KilBlock)
        kil_block_new = KilBlock([])
        for kil_obj in kil_block.children:
            if isinstance(kil_obj, KilReturn):
                # place the code before the return statement
                kil_block_new.children.extend(kil_append_return_block.children)
                kil_block_new.children.append(kil_obj)
                # no code can execute in the block after the return statement
                break
            elif isinstance(kil_obj, KilBlock):
                # recompile this sub-block
                kil_obj = cls.append_block_containing_return(kil_obj, kil_append_return_block)
                kil_block_new.children.append(kil_obj)
            elif isinstance(kil_obj, KilTryCatchBlock):
                raise NotImplementedError("this should never happen as we recompile the deepest try-catch blocks first")
            elif isinstance(kil_obj, KilIf):
                if kil_obj.true_body:
                    kil_obj.true_body = cls.append_block_containing_return(kil_obj.true_body, kil_append_return_block)
                if kil_obj.false_body:
                    kil_obj.false_body = cls.append_block_containing_return(kil_obj.false_body, kil_append_return_block)
                kil_block_new.children.append(kil_obj)
            elif isinstance(kil_obj, KilFor):
                if kil_obj.body:
                    kil_obj.body = cls.append_block_containing_return(kil_obj.body, kil_append_return_block)
                kil_block_new.children.append(kil_obj)
            elif isinstance(kil_obj, KilWhile):
                if kil_obj.body:
                    kil_obj.body = cls.append_block_containing_return(kil_obj.body, kil_append_return_block)
                kil_block_new.children.append(kil_obj)
            elif isinstance(kil_obj, KilDoWhile):
                if kil_obj.body:
                    kil_obj.body = cls.append_block_containing_return(kil_obj.body, kil_append_return_block)
                kil_block_new.children.append(kil_obj)
            else:
                # some other operation/code that we don't care about
                kil_block_new.children.append(kil_obj)
        return kil_block_new

    @classmethod
    def recompile_exception_to_block(cls, kil_try_catch_block, index, kil_block):
        assert isinstance(kil_try_catch_block, KilTryCatchBlock)
        assert isinstance(index, int)
        assert isinstance(kil_block, KilBlock)
        if kil_try_catch_block.catch_body:
            raise NotImplementedError("don't know how to compile out (remove) a catch body")
        if not kil_try_catch_block.final_body:
            kil_try_catch_block.final_body = KilBlock([])
        assert isinstance(kil_try_catch_block.final_body, KilBlock)
        if not kil_try_catch_block.try_body:
            kil_try_catch_block.try_body = KilBlock([])
        assert isinstance(kil_try_catch_block.try_body, KilBlock)
        if len(kil_try_catch_block.try_body.children) == 0:
            # if there is nothing to try, there will be no exceptions - just a final call code
            kil_recompiled_block = KilBlock([])
            kil_recompiled_block.children.extend(kil_block.children[:index])
            kil_recompiled_block.children.extend(kil_try_catch_block.final_body.children)
            kil_recompiled_block.children.extend(kil_block.children[index + 1:])
            # put the recompiled code back into the code block
            kil_block.children = kil_recompiled_block.children
            return
        if len(kil_try_catch_block.final_body.children) == 0:
            # even if there is an error, there is nothing we need to do - just run the try code
            kil_recompiled_block = KilBlock([])
            kil_recompiled_block.children.extend(kil_block.children[:index])
            kil_recompiled_block.children.extend(kil_try_catch_block.try_body.children)
            kil_recompiled_block.children.extend(kil_block.children[index + 1:])
            # put the recompiled code back into the code block
            kil_block.children = kil_recompiled_block.children
            return
        # we have both a try-block and finally-block to handle
        kil_recompiled_block = KilBlock([])
        kil_recompiled_block.children.extend(kil_block.children[:index])
        try_block = cls.append_block_containing_return(kil_try_catch_block.try_body, kil_try_catch_block.final_body)
        kil_recompiled_block.children.extend(try_block.children)
        kil_recompiled_block.children.extend(kil_try_catch_block.final_body.children)
        kil_recompiled_block.children.extend(kil_block.children[index + 1:])
        # put the recompiled code back into the code block
        kil_block.children = kil_recompiled_block.children
        return

    def compile(self, kil_obj, args):
        kil_root_obj = kil_obj
        exception_recompiled = True
        while exception_recompiled:
            exception_recompiled = False
            for kil_obj in reversed(kil_root_obj.walk()):  # walk backwards for deepest exception handlers first
                kil_try_catch_block, index, kil_block = self.find_try_catch_block(kil_obj)
                if kil_try_catch_block:
                    self.recompile_exception_to_block(kil_try_catch_block, index, kil_block)
                    # exit walk (as object tree has changed) and start again looking for next exception block
                    exception_recompiled = True
                    break
        return kil_root_obj
