from ast import Expr
import os
import sys
import string

from .. expression_parser import ExpressionParser

from .kil_types import *
from .kil_ksy_library import *
from .kil_enum_interface import *
from .kil_member_interface import *
from .kil_instance_interface import *
from .kil_class_interface import *


class KilExprCompiler(object):

    EXPR_TOKEN_OP_ATTR_MAP = {
        'and': ['a', 'b'],
        'not': ['a'],
        'or': ['a', 'b'],
        '==': ['a', 'b'],
        '!=': ['a', 'b'],
        '<=': ['a', 'b'],
        '>=': ['a', 'b'],
        '<<': ['a', 'b'],
        '>>': ['a', 'b'],
        '[]': ['a', 'b'],
        '<': ['a', 'b'],
        '>': ['a', 'b'],
        '.': ['a', 'b'],
        '+': ['a', 'b'],
        '-': ['a', 'b'],
        '*': ['a', 'b'],
        '/': ['a', 'b'],
        '%': ['a', 'b'],
        '?': ['a', 'b', 'c'],
        '&': ['a', 'b'],
        '|': ['a', 'b'],
        '^': ['a', 'b'],
        '~': ['a'],
    }

    @classmethod
    def typecast_promote_to_integer(cls, kil_obj):
        if isinstance(kil_obj.type_, KilTypeInteger):
            if kil_obj.type_.bitfield:
                integer_bits = KilTypeInteger.promote_bitfield_to_integer_bits(kil_obj.type_.bits)
                type_ = KilTypeInteger(kil_obj.type_.signed, integer_bits, bitfield=False)
                kil_obj = KilTypecast(kil_obj, type_)
            return kil_obj
        if isinstance(kil_obj.type_, KilTypeSize):
            return KilTypecast(kil_obj, KilTypeInteger(signed=False, bits=64))
        if isinstance(kil_obj.type_, KilTypeBoolean):
            true_value = KilLiteral(1, KilTypeInteger(signed=False, bits=8))
            false_value = KilLiteral(0, KilTypeInteger(signed=False, bits=8))
            return KilOperation(kil_obj, '?', true_value, false_value)
        if isinstance(kil_obj.type_, KilTypeFloat):
            return KilTypecast(kil_obj, KilTypeInteger(signed=False, bits=64))
        if isinstance(kil_obj.type_, KilTypeChar):
            return KilTypecast(kil_obj, kil_obj.integer_type)
        raise Exception("cannot promote type '{}' to integer".format(repr(kil_obj.type_)))

    @classmethod
    def typecast_promote_to_larger_of_two_integers(cls, kil_a, kil_b):
        assert isinstance(kil_a.type_, KilTypeInteger)
        assert isinstance(kil_b.type_, KilTypeInteger)
        assert not kil_a.type_.bitfield
        assert not kil_b.type_.bitfield
        if kil_a.type_.signed == kil_b.type_.signed:
            if kil_a.type_.bits == kil_b.type_.bits:
                # nothing to do
                pass
            else:
                # choose more bits
                if kil_a.type_.bits > kil_b.type_.bits:
                    # make b as large as a
                    kil_b = KilTypecast(kil_b, kil_a.type_)
                else:
                    # make a as large as b
                    kil_a = KilTypecast(kil_a, kil_b.type_)
        else:
            if kil_a.type_.bits == kil_b.type_.bits:
                # only the signs are different, so make it a signed integer
                signed = True
                # unsigned value may not fit inside a signed integer with same number of bits
                bits = kil_a.type_.bits
                # so promote the intergers to next bit size
                more_bits = KilTypeInteger.promote_to_next_bit_size(bits)
                kil_a = KilTypecast(kil_a, KilTypeInteger(signed, more_bits))
                kil_b = KilTypecast(kil_b, KilTypeInteger(signed, more_bits))
            else:
                # signs and bits are different
                if kil_a.type_.signed:
                    # a is signed, b is unsigned
                    if kil_a.type_.bits > kil_b.type_.bits:
                        # a is a larger/signed value - just promote b to the same type
                        kil_b = KilTypecast(kil_b, kil_a.type_)
                    else:
                        # a is signed but b has more bits
                        signed = True
                        bits = kil_b.type_.bits
                        # we will need more bits than either - to fit the unsigned into a signed integer
                        more_bits = KilTypeInteger.promote_to_next_bit_size(bits)
                        kil_a = KilTypecast(kil_a, KilTypeInteger(signed, more_bits))
                        kil_b = KilTypecast(kil_b, KilTypeInteger(signed, more_bits))
                else:
                    # b is signed, a is unsigned
                    if kil_b.type_.bits > kil_a.type_.bits:
                        # b is a larger/signed value - just promote a to the same type
                        kil_a = KilTypecast(kil_a, kil_b.type_)
                    else:
                        # b is signed but a has more bits
                        signed = True
                        bits = kil_a.type_.bits
                        # we will need more bits than either - to fit the unsigned into a signed integer
                        more_bits = KilTypeInteger.promote_to_next_bit_size(bits)
                        kil_a = KilTypecast(kil_a, KilTypeInteger(signed, more_bits))
                        kil_b = KilTypecast(kil_b, KilTypeInteger(signed, more_bits))
        return kil_a, kil_b

    @classmethod
    def typecast_promote_to_next_integer_size(cls, kil_a, kil_b):
        kil_a, kil_b = cls.typecast_promote_to_larger_of_two_integers(kil_a, kil_b)
        # after the call, a and b have the same type
        signed = kil_a.type_.signed
        bits = KilTypeInteger.promote_to_next_bit_size(kil_a.type_.bits)
        type_ = KilTypeInteger(signed, bits)
        kil_a = KilTypecast(kil_a, type_)
        kil_b = KilTypecast(kil_b, type_)
        return kil_a, kil_b

    @classmethod
    def typecast_promote_to_float(cls, kil_obj):
        if isinstance(kil_obj.type_, KilTypeFloat):
            return kil_obj
        if isinstance(kil_obj.type_, KilTypeSize):
            return KilTypecast(kil_obj, KilTypeFloat(bits=64))
        if isinstance(kil_obj.type_, KilTypeBoolean):
            true_value = KilLiteral(1, KilTypeFloat(bits=32))
            false_value = KilLiteral(0, KilTypeFloat(bits=32))
            return KilOperation(kil_obj, '?', true_value, false_value)
        if isinstance(kil_obj.type_, KilTypeInteger):
            if kil_obj.type_.bits > 16:
                return KilTypecast(kil_obj, KilTypeFloat(bits=64))
            return KilTypecast(kil_obj, KilTypeFloat(bits=32))
        if isinstance(kil_obj.type_, KilTypeChar):
            return KilTypecast(KilTypecast(kil_obj, kil_obj.integer_type), KilTypeFloat(signed=False, bits=32))
        raise Exception("cannot promote type '{}' to float/double".format(repr(kil_obj.type_)))

    @classmethod
    def typecast_operation_using_floats(cls, kil_a, kil_b):
        assert isinstance(kil_a.type_, KilTypeFloat)
        assert isinstance(kil_b.type_, KilTypeFloat)
        if kil_a.type_.bits == kil_b.type_.bits:
            pass  # no typecast needed
        if kil_a.type_.bits == 32 and kil_b.type_.bits == 64:
            # add more bits to arg A
            kil_a = KilTypecast(kil_a, KilTypeFloat(bits=64))
        elif kil_a.type_.bits == 64 and kil_b.type_.bits == 32:
            # add more bits to arg B
            kil_b = KilTypecast(kil_b, KilTypeFloat(bits=64))
        else:
            raise NotImplementedError("programmer error, why do we have floats with {} and {} bits?".format(kil_a.type_.bits, kil_b.type_.bits))
        return kil_a, kil_b

    @classmethod
    def typecast_operation_comparison(cls, kil_a, kil_b):
        # prefer float compares over integer compares
        if isinstance(kil_a.type_, KilTypeFloat) or isinstance(kil_b.type_, KilTypeFloat):
            kil_a = cls.typecast_promote_to_float(kil_a)
            kil_b = cls.typecast_promote_to_float(kil_b)
            kil_a, kil_b = cls.typecast_operation_using_floats(kil_a, kil_b)
            return kil_a, kil_b, None
        # after floats, prefer integer compares
        if isinstance(kil_a.type_, KilTypeInteger) or isinstance(kil_b.type_, KilTypeInteger):
            kil_a = cls.typecast_promote_to_integer(kil_a)
            kil_b = cls.typecast_promote_to_integer(kil_b)
            kil_a, kil_b = cls.typecast_promote_to_larger_of_two_integers(kil_a, kil_b)
            return kil_a, kil_b, None
        # if both are bools, leave them as-is
        if isinstance(kil_a.type_, KilTypeBoolean) and isinstance(kil_b.type_, KilTypeBoolean):
            return kil_a, kil_b, None  # no typecast needed
        # if both are sized-integers, leave them as-is
        if isinstance(kil_a.type_, KilTypeSize) and isinstance(kil_b.type_, KilTypeSize):
            return kil_a, kil_b, None  # no typecast needed
        # if both are charaters, leave them as-is (as long as they are backed by the same integer type)
        if isinstance(kil_a.type_, KilTypeSize) and isinstance(kil_b.type_, KilTypeSize):
            type_a = kil_a.type_.integer_type
            type_b = kil_b.type_.integer_type
            if type_a.signed == type_b.signed and type_a.bits == type_b.bits:
                return kil_a, kil_b, None  # no typecast needed
        # force these type to integers and compare
        INTEGER_COMPARE_TYPE_CLASSES = [
            KilTypeSize,
            KilTypeBoolean,
            KilTypeChar
        ]
        for type_class in INTEGER_COMPARE_TYPE_CLASSES:
            if isinstance(kil_a.type_, type_class) or isinstance(kil_b.type_, type_class):
                kil_a = cls.typecast_promote_to_integer(kil_a)
                kil_b = cls.typecast_promote_to_integer(kil_b)
                kil_a, kil_b = cls.typecast_promote_to_larger_of_two_integers(kil_a, kil_b)
                return kil_a, kil_b, None
        # don't know (or haven't implemented) how to compare these types
        raise NotImplementedError("comparison of type {} and type {}".format(repr(kil_a.type_), repr(kil_b.type_)))

    @classmethod
    def typecast_operation_addition(cls, kil_a, kil_b):
        if isinstance(kil_a.type_, KilTypeSize) and isinstance(kil_b.type_, KilTypeSize):
            return kil_a, kil_b, None  # no typecast needed
        if isinstance(kil_a.type_, KilTypeFloat) or isinstance(kil_b.type_, KilTypeFloat):
            # do float operations if either arg is a float
            kil_a = cls.typecast_promote_to_float(kil_a)
            kil_b = cls.typecast_promote_to_float(kil_b)
            kil_a, kil_b = cls.typecast_operation_using_floats(kil_a, kil_b)
            return kil_a, kil_b, None
        # do integer operations by default
        kil_a = cls.typecast_promote_to_integer(kil_a)
        kil_b = cls.typecast_promote_to_integer(kil_b)
        kil_a, kil_b = cls.typecast_promote_to_next_integer_size(kil_a, kil_b)
        return kil_a, kil_b, None

    @classmethod
    def typecast_operation_subtraction(cls, kil_a, kil_b):
        if isinstance(kil_a.type_, KilTypeSize) and isinstance(kil_b.type_, KilTypeSize):
            return kil_a, kil_b, None  # no typecast needed
        if isinstance(kil_a.type_, KilTypeFloat) or isinstance(kil_b.type_, KilTypeFloat):
            # do float operations if either arg is a float
            kil_a = cls.typecast_promote_to_float(kil_a)
            kil_b = cls.typecast_promote_to_float(kil_b)
            kil_a, kil_b = cls.typecast_operation_using_floats(kil_a, kil_b)
            return kil_a, kil_b, None
        # do integer operations by default
        kil_a = cls.typecast_promote_to_integer(kil_a)
        kil_b = cls.typecast_promote_to_integer(kil_b)
        kil_a, kil_b = cls.typecast_promote_to_next_integer_size(kil_a, kil_b)
        return kil_a, kil_b, None

    @classmethod
    def typecast_operation_multiply(cls, kil_a, kil_b):
        if isinstance(kil_a.type_, KilTypeSize) and isinstance(kil_b.type_, KilTypeSize):
            return kil_a, kil_b, None  # no typecast needed
        if isinstance(kil_a.type_, KilTypeFloat) or isinstance(kil_b.type_, KilTypeFloat):
            # do float operations if either arg is a float
            kil_a = cls.typecast_promote_to_float(kil_a)
            kil_b = cls.typecast_promote_to_float(kil_b)
            kil_a, kil_b = cls.typecast_operation_using_floats(kil_a, kil_b)
            return kil_a, kil_b, None
        # do integer operations by default
        kil_a = cls.typecast_promote_to_integer(kil_a)
        kil_b = cls.typecast_promote_to_integer(kil_b)
        kil_a, kil_b = cls.typecast_promote_to_next_integer_size(kil_a, kil_b)
        return kil_a, kil_b, None

    @classmethod
    def typecast_operation_divide(cls, kil_a, kil_b):
        if isinstance(kil_a.type_, KilTypeSize) and isinstance(kil_b.type_, KilTypeSize):
            return kil_a, kil_b, None  # no typecast needed
        if isinstance(kil_a.type_, KilTypeFloat) or isinstance(kil_b.type_, KilTypeFloat):
            # do float operations if either arg is a float
            kil_a = cls.typecast_promote_to_float(kil_a)
            kil_b = cls.typecast_promote_to_float(kil_b)
            kil_a, kil_b = cls.typecast_operation_using_floats(kil_a, kil_b)
            return kil_a, kil_b, None
        # do integer operations by default
        kil_a = cls.typecast_promote_to_integer(kil_a)
        kil_b = cls.typecast_promote_to_integer(kil_b)
        if kil_a.type_.signed == kil_b.type_.signed:
            # same sign, result can fit inside a's type
            result_type = kil_a.type_
        else:
            # different signs, result must be signed
            signed = True
            # integer division means the result cannot be larger than a's type
            bits = kil_a.type_.bits
            if not kil_a.type_.signed:
                # however, if a is not signed and we are moving to a signed number - increase bits
                bits = KilTypeInteger.promote_to_next_bit_size(bits)
            result_type = KilTypeInteger(signed, bits)
        kil_a, kil_b = cls.typecast_promote_to_larger_of_two_integers(kil_a, kil_b)
        if (kil_a.type_.signed != result_type.signed) or (kil_a.type_.bits != result_type.bits):
            # a's type has been changed to perform the operation - typecast back
            return kil_a, kil_b, result_type
        return kil_a, kil_b, None

    @classmethod
    def typecast_operation_mod(cls, kil_a, kil_b):
        if isinstance(kil_a.type_, KilTypeSize) and isinstance(kil_b.type_, KilTypeSize):
            return kil_a, kil_b, None  # no typecast needed
        if isinstance(kil_a.type_, KilTypeFloat) or isinstance(kil_b.type_, KilTypeFloat):
            # do float operations if either arg is a float
            kil_a = cls.typecast_promote_to_float(kil_a)
            kil_b = cls.typecast_promote_to_float(kil_b)
            kil_a, kil_b = cls.typecast_operation_using_floats(kil_a, kil_b)
            return kil_a, kil_b, None
        # do integer operations by default
        kil_a = cls.typecast_promote_to_integer(kil_a)
        kil_b = cls.typecast_promote_to_integer(kil_b)
        if kil_a.type_.signed == kil_b.type_.signed:
            # same sign, result can fit inside b's type
            result_type = kil_b.type_
        else:
            # different signs, result must be signed
            signed = True
            # integer mod means the result cannot be larger than b's type
            bits = kil_b.type_.bits
            if not kil_b.type_.signed:
                # however, if b is not signed and we are moving to a signed number - increase bits
                bits = KilTypeInteger.promote_to_next_bit_size(bits)
            result_type = KilTypeInteger(signed, bits)
        kil_a, kil_b = cls.typecast_promote_to_larger_of_two_integers(kil_a, kil_b)
        if (kil_a.type_.signed != result_type.signed) or (kil_a.type_.bits != result_type.bits):
            # a's type has been changed to perform the operation - typecast back
            return kil_a, kil_b, result_type
        return kil_a, kil_b, None

    @classmethod
    def typecast_operation_bitshift_up(cls, kil_a, kil_b):
        if isinstance(kil_a.type_, KilTypeSize) and isinstance(kil_b.type_, KilTypeSize):
            return kil_a, kil_b, None  # no typecast needed
        # do integer operations by default
        kil_a = cls.typecast_promote_to_integer(kil_a)
        signed = kil_a.type_.signed
        kil_b = cls.typecast_promote_to_integer(kil_b)
        if kil_a.type_.signed != signed or kil_a.type_.bits < 64:
            kil_a = KilTypecast(kil_a, KilTypeInteger(signed=signed, bits=64))
        if kil_b.type_.signed != signed or kil_b.type_.bits < 64:
            kil_b = KilTypecast(kil_b, KilTypeInteger(signed=signed, bits=64))
        return kil_a, kil_b, None

    @classmethod
    def typecast_operation_bitshift_down(cls, kil_a, kil_b):
        if isinstance(kil_a.type_, KilTypeSize) and isinstance(kil_b.type_, KilTypeSize):
            return kil_a, kil_b, None  # no typecast needed
        # do integer operations by default
        kil_a = cls.typecast_promote_to_integer(kil_a)
        return_type = kil_a.type_
        kil_b = cls.typecast_promote_to_integer(kil_b)
        kil_a, kil_b = cls.typecast_promote_to_larger_of_two_integers(kil_a, kil_b)
        if return_type.signed == kil_a.type_.signed and return_type.bits == kil_a.type_.bits:
            return_type = None
        return kil_a, kil_b, return_type

    @classmethod
    def typecast_operation_binary_and(cls, kil_a, kil_b):
        if isinstance(kil_a, KilLiteral):
            if isinstance(kil_b, KilLiteral):
                hard_coded_mask_a = kil_a.value_as_integer
                hard_coded_mask_b = kil_b.value_as_integer
                hard_coded_mask = hard_coded_mask_a if hard_coded_mask_a < hard_coded_mask_b else hard_coded_mask_b
            else:
                hard_coded_mask = kil_a.value_as_integer
        else:
            if isinstance(kil_b, KilLiteral):
                hard_coded_mask = kil_b.value_as_integer
            else:
                hard_coded_mask = None
        if isinstance(kil_a.type_, KilTypeSize) and isinstance(kil_b.type_, KilTypeSize):
            return kil_a, kil_b, None  # no typecast needed
        # do integer operations by default
        kil_a = cls.typecast_promote_to_integer(kil_a)
        kil_b = cls.typecast_promote_to_integer(kil_b)
        kil_a, kil_b = cls.typecast_promote_to_larger_of_two_integers(kil_a, kil_b)
        return_type = None
        if hard_coded_mask is not None:
            return_type = KilTypeInteger.convert_value_to_smallest_type(hard_coded_mask)
        return kil_a, kil_b, return_type

    @classmethod
    def typecast_operation_binary_or(cls, kil_a, kil_b):
        if isinstance(kil_a.type_, KilTypeSize) and isinstance(kil_b.type_, KilTypeSize):
            return kil_a, kil_b, None  # no typecast needed
        # do integer operations by default
        kil_a = cls.typecast_promote_to_integer(kil_a)
        kil_b = cls.typecast_promote_to_integer(kil_b)
        kil_a, kil_b = cls.typecast_promote_to_larger_of_two_integers(kil_a, kil_b)
        return kil_a, kil_b, None

    @classmethod
    def typecast_operation_binary_xor(cls, kil_a, kil_b):
        if isinstance(kil_a.type_, KilTypeSize) and isinstance(kil_b.type_, KilTypeSize):
            return kil_a, kil_b, None  # no typecast needed
        # do integer operations by default
        kil_a = cls.typecast_promote_to_integer(kil_a)
        kil_b = cls.typecast_promote_to_integer(kil_b)
        kil_a, kil_b = cls.typecast_promote_to_larger_of_two_integers(kil_a, kil_b)
        return kil_a, kil_b, None

    @classmethod
    def typecast_operation_ternary(cls, kil_a, kil_b):
        MUST_MATCH_TYPES = [
            KilTypeClass,
            KilTypeBuffer,
            KilTypeString
        ]
        for must_match_type in MUST_MATCH_TYPES:
            if isinstance(kil_a.type_, must_match_type) and not isinstance(kil_b.type_, must_match_type):
                raise Exception("cannot use ternary on two different types '{}' and '{}'".format(repr(kil_a.type_), repr(kil_b.type_)))
            if not isinstance(kil_a.type_, must_match_type) and isinstance(kil_b.type_, must_match_type):
                raise Exception("cannot use ternary on two different types '{}' and '{}'".format(repr(kil_a.type_), repr(kil_b.type_)))
        if isinstance(kil_a.type_, KilTypeClass) and isinstance(kil_b.type_, KilTypeClass):
            if kil_a.type_.name != kil_b.type_.name:
                raise Exception("cannot use ternary on two different class types '{}' and '{}'".format(kil_a.type_.name, kil_b.type_.name))
            return kil_a, kil_b
        if isinstance(kil_a.type_, KilTypeBuffer) and isinstance(kil_b.type_, KilTypeBuffer):
            return kil_a, kil_b
        if isinstance(kil_a.type_, KilTypeString) and isinstance(kil_b.type_, KilTypeString):
            if kil_a.type_.encoding != kil_b.type_.encoding:
                raise Exception("cannot use ternary on two different string encodings '{}' and '{}'".format(kil_a.type_.ksy_encoding, kil_b.type_.ksy_encoding))
            return kil_a, kil_b
        if isinstance(kil_a.type_, KilTypeSize) and isinstance(kil_b.type_, KilTypeSize):
            return kil_a, kil_b  # no typecast needed
        if isinstance(kil_a.type_, KilTypeFloat) or isinstance(kil_b.type_, KilTypeFloat):
            # do float operations if either arg is a float
            kil_a = cls.typecast_promote_to_float(kil_a)
            kil_b = cls.typecast_promote_to_float(kil_b)
            return cls.typecast_operation_using_floats(kil_a, kil_b)
        # do integer operations by default
        kil_a = cls.typecast_promote_to_integer(kil_a)
        kil_b = cls.typecast_promote_to_integer(kil_b)
        kil_a, kil_b = cls.typecast_promote_to_larger_of_two_integers(kil_a, kil_b)
        return kil_a, kil_b

    @classmethod
    def typecast_operation(cls, kil_obj):
        assert isinstance(kil_obj, KilOperation)
        if kil_obj.op in ['.', 'and', 'or', 'not']:
            # nothigng to do, typecast not needed
            return kil_obj
        kil_a = kil_obj.a
        kil_b = kil_obj.b
        kil_c = kil_obj.c
        result_type = None
        if kil_obj.op in ['==', '!=', '<=', '>=', '<', '>']:
            kil_a, kil_b, result_type = cls.typecast_operation_comparison(kil_a, kil_b)
        elif kil_obj.op == '+':
            kil_a, kil_b, result_type = cls.typecast_operation_addition(kil_a, kil_b)
        elif kil_obj.op == '-':
            kil_a, kil_b, result_type = cls.typecast_operation_subtraction(kil_a, kil_b)
        elif kil_obj.op == '*':
            kil_a, kil_b, result_type = cls.typecast_operation_multiply(kil_a, kil_b)
        elif kil_obj.op == '/':
            kil_a, kil_b, result_type = cls.typecast_operation_divide(kil_a, kil_b)
        elif kil_obj.op == '%':
            kil_a, kil_b, result_type = cls.typecast_operation_mod(kil_a, kil_b)
        elif kil_obj.op == '<<':
            kil_a, kil_b, result_type = cls.typecast_operation_bitshift_up(kil_a, kil_b)
        elif kil_obj.op == '>>':
            kil_a, kil_b, result_type = cls.typecast_operation_bitshift_down(kil_a, kil_b)
        elif kil_obj.op == '&':
            kil_a, kil_b, result_type = cls.typecast_operation_binary_and(kil_a, kil_b)
        elif kil_obj.op == '|':
            kil_a, kil_b, result_type = cls.typecast_operation_binary_or(kil_a, kil_b)
        elif kil_obj.op == '^':
            kil_a, kil_b, result_type = cls.typecast_operation_binary_xor(kil_a, kil_b)
        elif kil_obj.op == '?':
            kil_b, kil_c = cls.typecast_operation_ternary(kil_b, kil_c)
        elif kil_obj.op == '~':
            if kil_a.type_.signed or kil_a.type_.bits < 64:
                kil_a = KilTypecast(kil_a, KilTypeInteger(signed=False, bits=64))
        else:
            raise NotImplementedError("don't know how to typecast operation '{}'".format(kil_obj.op))
        kil_obj.a = kil_a
        kil_obj.b = kil_b
        kil_obj.c = kil_c
        if result_type:
            kil_obj = KilTypecast(kil_obj, result_type)
        return kil_obj

    def __init__(self, root_name=None, class_name=None, member_name=None, class_variable=None, current_variable=None, case_variable=None, value_name=None, value_variable=None, length_is_object_size=False, ksy=None):
        self.root_name = root_name
        self.class_name = class_name
        self.member_name = member_name
        self.class_variable = class_variable
        self.current_variable = current_variable  # for '_' in expressions
        self.case_variable = case_variable  # for '_case' in expressions
        self.value_name = value_name  # for instance setting in expressions
        self.value_variable = value_variable  # for instance setting in expressions
        self.length_is_object_size = length_is_object_size
        self.pre_param_list_variable = None
        self.ksy = ksy

    def copy(self):
        expr_compiler = KilExprCompiler()
        expr_compiler.root_name = self.root_name
        expr_compiler.class_name = self.class_name
        expr_compiler.member_name = self.member_name
        expr_compiler.class_variable = self.class_variable
        expr_compiler.current_variable = self.current_variable  # for '_' in expressions
        expr_compiler.case_variable = self.case_variable  # for '_case' in expressions
        expr_compiler.value_variable = self.value_variable  # for instance setting in expressions
        expr_compiler.length_is_object_size = self.length_is_object_size
        expr_compiler.pre_param_list_variable = self.pre_param_list_variable
        expr_compiler.ksy = self.ksy
        return expr_compiler

    def compile_to_i(self, variable):
        if isinstance(variable.type_, KilTypeInteger):
            return variable
        if isinstance(variable, KilEnumValue):
            return KilLiteral(variable.value, variable.type_)
        raise NotImplementedError("don't know how to 'to_i' on object '{}' with type '{}'".format(repr(variable), repr(variable.type_)))

    def compile_size(self, variable):
        if isinstance(variable, KilVariable):
            # for 'self'
            assert isinstance(variable.type_, KilTypeClass)
            root_name = self.root_name
            class_name = variable.type_.name
            this_class_interface = KilClassInterface(root_name, class_name, variable, ksy=self.ksy)
            return this_class_interface.size()
        else:
            assert isinstance(variable, KilOperation)
            method_call = variable.b
            this_variable = variable.a
            assert isinstance(method_call, KilMethodCall)
            if method_call.decl_variable.name == 'root':
                root_name = method_call.decl_variable.type_.name
                class_name = root_name
                this_class_interface = KilClassInterface(root_name, class_name, variable, ksy=self.ksy)
                return this_class_interface.size()
            elif method_call.decl_variable.name == 'parent':
                root_name = self.root_name
                class_name = method_call.decl_variable.type_.name
                this_class_interface = KilClassInterface(root_name, class_name, variable, ksy=self.ksy)
                return this_class_interface.size()
        # for size of a member in this class
        class_name = this_variable.type_.name
        root_name = self.ksy.find_root_name_from_class_name(self.root_name, class_name)
        member_name = method_call.decl_variable.name
        ksy = self.ksy
        if root_name != self.root_name:
            ksy = ksy.copy()
            ksy.change_import_context(root_name)
        list_ = False
        index_variable = None
        if member_name.endswith('_at'):
            member_name = member_name[:-len('_at')]
            index_variable = method_call.arg_variables[0]
            list_ = True
        if ksy.is_value_instance(root_name, class_name, member_name):
            instance_name = member_name
            expr_compiler = self.copy()
            expr_compiler.root_name = root_name
            expr_compiler.class_name = class_name
            expr_compiler.member_name = member_name
            expr_compiler.class_variable = this_variable
            this_member = KilInstanceInterface(root_name, class_name, instance_name, this_variable, expr_compiler, ksy)
        else:
            this_member = KilMemberInterface(root_name, class_name, member_name, this_variable, ksy)
        if list_:
            return this_member.sizeat(index_variable)
        return this_member.size()

    def compile_count(self, variable):
        if isinstance(variable, KilVariable):
            root_name = self.root_name
            class_name = variable.type_.name
            if variable.list_ or isinstance(variable.type_, KilTypeBuffer) or isinstance(variable.type_, KilTypeString):
                # we can just do a length operation on the variable
                return KilOperation(variable, 'len')
            # otherwise, we expect it to be a class and we want the 'size' of it
            assert isinstance(variable.type_, KilTypeClass)
            return self.compile_size(variable)
        assert isinstance(variable, KilOperation)
        method_call = variable.b
        this_variable = variable.a
        assert isinstance(method_call, KilMethodCall)
        class_name = this_variable.type_.name
        root_name = self.ksy.find_root_name_from_class_name(self.root_name, class_name)
        member_name = method_call.decl_variable.name
        ksy = self.ksy
        if root_name != self.root_name:
            ksy = ksy.copy()
            ksy.change_import_context(root_name)
        if ksy.is_value_instance(root_name, class_name, member_name):
            instance_name = member_name
            expr_compiler = self.copy()
            expr_compiler.root_name = root_name
            expr_compiler.class_name = class_name
            expr_compiler.member_name = member_name
            expr_compiler.class_variable = this_variable
            this_member = KilInstanceInterface(root_name, class_name, instance_name, this_variable, expr_compiler, ksy)
        else:
            this_member = KilMemberInterface(root_name, class_name, member_name, this_variable, ksy)
        if this_member.list_:
            if self.pre_param_list_variable and (this_variable == self.pre_param_list_variable):
                # count is being used as part of a parameter arg for a new object
                # however, the count has already been incremented when the parameter argumented is determined
                # so subject 1 to adjust for this
                zero = KilLiteral(0, KilTypeSize())
                one = KilLiteral(1, KilTypeSize())
                return KilOperation(KilOperation(this_member.count(), '>', zero), '?', KilOperation(this_member.count(), '-', one), zero)
            return this_member.count()
        # trying to get a count on a non-list? just get the size
        return self.compile_size(variable)

    def compile_get(self, variable, member_name):
        if isinstance(variable.type_, KilTypeIO):
            _io = KilIoInterface(reference=True)
            if member_name == 'pos':
                return _io.pos()
            if member_name == 'size':
                return _io.size()
            if member_name == 'bit_pos':
                return _io.bit_pos()
            if member_name == 'bit_size':
                return _io.bit_size()
            raise NotImplementedError("_io.{}".format(member_name))
        this_variable = variable
        class_name = variable.type_.name
        root_name = self.ksy.find_root_name_from_class_name(self.root_name, class_name)
        ksy = self.ksy
        if root_name != self.root_name:
            ksy = ksy.copy()
            ksy.change_import_context(root_name)
        if ksy.is_value_instance(root_name, class_name, member_name):
            instance_name = member_name
            expr_compiler = self.copy()
            expr_compiler.root_name = root_name
            expr_compiler.class_name = class_name
            expr_compiler.member_name = member_name
            expr_compiler.class_variable = this_variable
            this_member = KilInstanceInterface(root_name, class_name, instance_name, this_variable, expr_compiler, ksy)
        else:
            this_member = KilMemberInterface(root_name, class_name, member_name, this_variable, ksy)
        return this_member.get()

    def compile_atindex(self, array_variable, index_variable):
        assert isinstance(array_variable, KilOperation)
        this_variable = array_variable.a
        method_call = array_variable.b
        assert isinstance(method_call, KilMethodCall)
        class_name = this_variable.type_.name
        root_name = self.ksy.find_root_name_from_class_name(self.root_name, class_name)
        member_name = method_call.decl_variable.name
        ksy = self.ksy
        if root_name != self.root_name:
            ksy = ksy.copy()
            ksy.change_import_context(root_name)
        if ksy.is_value_instance(root_name, class_name, member_name):
            instance_name = member_name
            expr_compiler = self.copy()
            expr_compiler.root_name = root_name
            expr_compiler.class_name = class_name
            expr_compiler.member_name = member_name
            expr_compiler.class_variable = this_variable
            this_member = KilInstanceInterface(root_name, class_name, instance_name, this_variable, expr_compiler, ksy)
        else:
            this_member = KilMemberInterface(root_name, class_name, member_name, this_variable, ksy)
        return this_member.atindex(index_variable)

    def deconflict_member_name(self, member_name):
        return member_name + '_'

    def compile_expr_tree(self, expr_tree):
        if isinstance(expr_tree, list) and len(expr_tree) == 1:
            expr_tree = expr_tree[0]
        if isinstance(expr_tree, bool):
            return KilLiteral(expr_tree, KilTypeBoolean())
        if isinstance(expr_tree, int):
            expr_tree = str(expr_tree)
        if isinstance(expr_tree, str):
            expr = expr_tree
            if expr.startswith('_'):
                if expr == '_self':
                    interface = KilClassInterface(self.root_name, self.class_name, self.class_variable, self.ksy)
                    return interface.this(mutable=False)
                if expr == '_root':
                    interface = KilClassInterface(self.root_name, self.class_name, self.class_variable, self.ksy)
                    return interface.root(mutable=False)
                if expr == '_parent':
                    interface = KilClassInterface(self.root_name, self.class_name, self.class_variable, self.ksy)
                    return interface.parent(mutable=False)
                if expr == '_case':
                    if not self.case_variable:
                        raise Exception("missing '_case' value - is this a switch statement?")
                    return self.case_variable
                if expr == '_io':
                    return KilIoInterface(reference=True).this
                if expr == '_':
                    if not self.current_variable:
                        raise Exception("missing '_' value - is this a repeat-until statement?")
                    return self.current_variable
            if self.value_name is not None and expr == self.value_name:
                return self.value_variable
            if expr == 'true':
                return KilLiteral(True, KilTypeBoolean())
            if expr == 'false':
                return KilLiteral(False, KilTypeBoolean())
            if expr.find('::') != -1:
                enum_name = expr.split('::')[0]
                enum_owner_class_name = self.ksy.find_enum_owner_class_name(self.root_name, self.class_name, enum_name)
                return KilTypecast(KilEnumValueInterface(self.root_name, enum_owner_class_name, expr, self.ksy).value, KilTypeInteger(signed=False, bits=32))
            value = None
            try:
                value = int(expr, 0)
            except ValueError:
                pass
            if value is not None:
                return KilLiteral(value=value, type_=KilTypeInteger.convert_value_to_smallest_type(value))
            value = None
            try:
                value = float(expr)
            except ValueError:
                pass
            if value is not None:
                return KilLiteral(value=value, type_=KilTypeFloat(bits=64))
            return self.compile_expr_tree(['.', '_self', expr])
        assert isinstance(expr_tree, list)
        assert len(expr_tree) > 0
        op = expr_tree[0]
        args = expr_tree[1:]
        if op not in ExpressionParser.EXPR_TOKENS:
            raise NotImplementedError("operation '{}'".format(op))
        kil_obj = KilOperation()
        kil_obj.op = op
        if op == '.':
            assert len(args) == 2
            this_variable = self.compile_expr_tree(args[0])
            member_name = args[1]
            if member_name == 'to_i':
                return self.compile_to_i(this_variable)
            if member_name == 'length':
                # thank you kaitai for making this so confusing that length => size
                if isinstance(this_variable.type_, KilTypeString) or isinstance(this_variable.type_, KilTypeBuffer):
                    # number of characters in string, number of bytes in buffer
                    return self.compile_size(this_variable)
                if isinstance(this_variable.type_, KilTypeClass) and self.length_is_object_size:
                    # for class instances (objects), treat 'length' as a request for the serialized size of the class
                    return self.compile_size(this_variable)
                # treat as member - but we need to deconflict the name
                if not isinstance(this_variable.type_, KilTypeIO):
                    member_name = self.deconflict_member_name(member_name)
                return self.compile_get(this_variable, member_name)
            if member_name == 'size':
                # thank you kaitai for making this so confusing that size => count
                if this_variable.list_:
                    # this is a list, 'size' will be used to get the number of elements in the list
                    return self.compile_count(this_variable)
                # treat as member - but we need to deconflict the name
                if not isinstance(this_variable.type_, KilTypeIO):
                    member_name = self.deconflict_member_name(member_name)
                return self.compile_get(this_variable, member_name)
            return self.compile_get(this_variable, member_name)
        if op == '[]':
            assert len(args) == 2
            array_variable = self.compile_expr_tree(args[0])
            index_variable = self.compile_expr_tree(args[1])
            return self.compile_atindex(array_variable, index_variable)
        arg_indx_to_op_attr = self.EXPR_TOKEN_OP_ATTR_MAP[op]
        arg_index = 0
        for arg in args:
            op_attr = arg_indx_to_op_attr[arg_index]
            setattr(kil_obj, op_attr, self.compile_expr_tree(arg))
            arg_index += 1
        kil_obj = self.typecast_operation(kil_obj)
        kil_op_obj = kil_obj
        while isinstance(kil_op_obj, KilTypecast):
            kil_op_obj = kil_op_obj.value
        assert isinstance(kil_op_obj, KilOperation)
        if kil_op_obj.op in ['/', '%']:
            # avoid divide-by-zero
            # we would rather an incorrect result for expressions than an exception
            kil_divisor = kil_op_obj.b
            if isinstance(kil_divisor.type_, KilTypeFloat):
                zero = KilLiteral(0.0, kil_divisor.type_)
            else:
                zero = KilLiteral(0, kil_divisor.type_)
            divisor_is_zero = KilOperation(kil_divisor, '==', zero)
            kil_obj = KilOperation(divisor_is_zero, '?', zero, kil_obj)
        if kil_op_obj.op in ['<<', '>>']:
            # avoid shift by negative numbers
            # we would rather an incorrect result for expressions than an exception
            kil_shift_value = kil_op_obj.b
            assert isinstance(kil_shift_value.type_, KilTypeInteger)
            if kil_shift_value.type_.signed:
                zero = KilLiteral(0, kil_shift_value.type_)
                value_is_negative = KilOperation(kil_shift_value, '<', zero)
                kil_shift_value = KilOperation(value_is_negative, '?', zero, kil_shift_value)
                kil_op_obj.b = kil_shift_value
        return kil_obj

    def compile_typecast(self, kil_obj, type_):
        if isinstance(type_, KilTypeNone):
            if not isinstance(kil_obj.type_, KilTypeNone):
                raise Exception("cannot typecast {} to none type".format(repr(kil_obj.type_)))
            # no typecast needed, same type
        elif isinstance(type_, KilTypeBoolean):
            if not isinstance(kil_obj.type_, KilTypeBoolean):
                # make it an integer
                kil_obj = self.typecast_promote_to_integer(kil_obj)
                # compare it to zero to make it a bool
                zero = KilLiteral(0, kil_obj.type_)
                kil_obj = KilOperation(zero, '!=', kil_obj)
                # now typecast to bool
                kil_obj = KilTypecast(kil_obj, KilTypeBoolean())
            # no typecast needed, same type
        elif isinstance(type_, KilTypeInteger):
            # turn value into an integer and then typecast
            kil_obj = self.typecast_promote_to_integer(kil_obj)
            if type_.signed != kil_obj.type_.signed or type_.bits != kil_obj.type_.bits:
                kil_obj = KilTypecast(kil_obj, type_)
        elif isinstance(type_, KilTypeFloat):
            # turn value into a float and then typecast
            kil_obj = self.typecast_promote_to_float(kil_obj)
            if type_.bits != kil_obj.type_.bits:
                kil_obj = KilTypecast(kil_obj, type_)
        elif isinstance(type_, KilTypeBuffer):
            if not isinstance(kil_obj.type_, KilTypeBuffer):
                raise Exception("cannot typecast {} to buffer".format(repr(kil_obj.type_)))
            # no typecast needed, same type
        elif isinstance(type_, KilTypeString):
            if not isinstance(kil_obj.type_, KilTypeString):
                raise Exception("cannot typecast {} to string".format(repr(kil_obj.type_)))
            if type_.encoding != kil_obj.type_.encoding:
                raise Exception("cannot typecast encoding {} to {}".format(repr(kil_obj.type_.ksy_encoding, type_.ksy_encoding)))
            # no typecast needed, same type
        elif isinstance(type_, KilTypeChar):
            if not isinstance(kil_obj.type_, KilTypeChar):
                # make it an integer
                kil_obj = self.typecast_promote_to_integer(kil_obj)
                # now typecast to char
                kil_obj = KilTypecast(kil_obj, type_)
            # no typecast needed, same type
        elif isinstance(type_, KilTypeSize):
            if not isinstance(kil_obj.type_, KilTypeSize):
                # make it an integer
                kil_obj = self.typecast_promote_to_integer(kil_obj)
                # now typecast to sized-type
                kil_obj = KilTypecast(kil_obj, type_)
            # no typecast needed, same type
        elif isinstance(type_, KilTypeIO):
            if not isinstance(kil_obj.type_, KilTypeIO):
                raise Exception("cannot typecast {} to IO class".format(repr(kil_obj.type_)))
            # no typecast needed, same class
        elif isinstance(type_, KilTypeClass):
            if not isinstance(kil_obj.type_, KilTypeClass):
                raise Exception("cannot typecast {} to class".format(repr(kil_obj.type_)))
            if type_.name != kil_obj.type_.name:
                raise Exception("cannot typecast class '{}' to class '{}'".format(kil_obj.type_.name, type_.name))
            # no typecast needed, same class
        else:
            raise NotImplementedError("how to typecast to {}".format(repr(type_)))
        return kil_obj

    def expr_contains_io(self, expr):
        try:
            expr_list = ExpressionParser.split_expr_list(expr)
        except:
            print("ERROR COMPILING EXPRESSION: '{}'".format(expr))
            raise
        return '_io' in expr_list

    def compile_expr(self, expr, type_=None):
        kil_obj = None
        self.constructed_class_name_list = list()
        expr_list = None
        expr_tree = None
        try:
            expr_list = ExpressionParser.split_expr_list(expr)
            expr_tree = ExpressionParser.create_expr_tree(expr_list)
            expr_tree = ExpressionParser.reduce_expr_tree(expr_tree)
            kil_obj = self.compile_expr_tree(expr_tree)
        except:
            print("ERROR COMPILING EXPRESSION: '{}'".format(expr))
            print(expr_list)
            raise
        if type_:
            kil_obj = self.compile_typecast(kil_obj, type_)
        return kil_obj

    def compile_io_expr(self, io_expr):
        io_expr = io_expr.strip()
        if io_expr == '_io':
            return KilIoInterface(reference=True).this
        if io_expr == '_root._io':
            # TODO: io.root() in my runtime libraries is not correct
            return KilIoInterface(reference=True).root()
        if io_expr == '_parent._io':
            # TODO: io.root() in my runtime libraries is not correct
            return KilIoInterface(reference=True).parent()
        # TODO: actually handle the full set of options for 'io' key in ksy files
        raise NotImplementedError("IO_EXPRESSION: '{}'".format(io_expr))
