import os
import sys

from .kil_types import *
from .kil_ksy_library import *


class KilEnumCompiler(object):

    def __init__(self, root_name=None, class_name=None, enum_name=None, ksy=None):
        self.root_name = root_name
        self.class_name = class_name
        self.enum_name = enum_name
        self.ksy = ksy

    def compile_enum(self):
        ksy_enum_dict = self.ksy.find_ksy_enum_dict_from_enum_name(self.root_name, self.class_name, self.enum_name)
        enum = KilEnum()
        enum.name = self.enum_name
        enum.class_type = KilTypeClass(name=self.class_name)
        for value in sorted(ksy_enum_dict.keys()):
            information = ksy_enum_dict[value]
            if isinstance(information, bool):
                information = 'true' if information else 'false'
            if isinstance(information, int):
                information = str(information)
            if isinstance(information, str):
                information = {'id': information}
            assert isinstance(information, dict)
            enum_value = KilEnumValue()
            enum_value.name = information['id']
            enum_value.enum = enum
            enum_value.value = value
            enum.enums.append(enum_value)
        return enum
