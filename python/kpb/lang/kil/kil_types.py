import os
import sys
import struct


class KilFile(object):

    def __init__(self):
        self.name = None
        self.input_file_path = None
        self.children = list()

    def walk(self):
        kil_obj_list = [self]
        for kil_obj in self.children:
            kil_obj_list.extend(kil_obj.walk())
        return kil_obj_list


class KilImport(object):

    def __init__(self, name=None, public=True, external=False, imports=None):
        self.name = name
        self.public = public
        self.external = external
        self.imports = imports if imports else list()

    def walk(self):
        return [self]


class KilNamespace(object):

    def __init__(self, name=None):
        self.name = name
        self.children = list()

    def walk(self):
        kil_obj_list = [self]
        for kil_obj in self.children:
            kil_obj_list.extend(kil_obj.walk())
        return kil_obj_list


class KilClass(object):

    def __init__(self):
        self.name = None
        self.enums = list()
        self.constructor = None
        self.destructor = None
        self.members = list()
        self.methods = list()

    def walk(self):
        kil_obj_list = [self]
        for kil_obj in self.enums:
            kil_obj_list.extend(kil_obj.walk())
        if self.constructor:
            kil_obj_list.extend(self.constructor.walk())
        if self.destructor:
            kil_obj_list.extend(self.destructor.walk())
        for kil_obj in self.members:
            kil_obj_list.extend(kil_obj.walk())
        for kil_obj in self.methods:
            kil_obj_list.extend(kil_obj.walk())
        return kil_obj_list

    @property
    def type_(self):
        return KilTypeClass(name=self.name)


class KilMethod(object):

    def __init__(self):
        self.class_type = None
        self.decl_variable = None
        self.arg_variables = list()
        self.mutable = False
        self.public = True
        self.class_method = False
        self.hint_treat_as_member_variable = False
        self.code_block = None

    def walk(self):
        kil_obj_list = [self]
        if self.class_type:
            kil_obj_list.extend(self.class_type.walk())
        if self.decl_variable:
            kil_obj_list.extend(self.decl_variable.walk())
        for kil_obj in self.arg_variables:
            kil_obj_list.extend(kil_obj.walk())
        if self.code_block:
            kil_obj_list.extend(self.code_block.walk())
        return kil_obj_list


class KilMethodCall(object):

    def __init__(self, class_type=None):
        self.class_type = class_type
        self.decl_variable = None
        self.arg_variables = list()
        self.mutable = False
        self.public = True
        self.class_method = False
        self.hint_treat_as_member_variable = False

    def walk(self):
        kil_obj_list = [self]
        if self.class_type:
            kil_obj_list.extend(self.class_type.walk())
        if self.decl_variable:
            kil_obj_list.extend(self.decl_variable.walk())
        for kil_obj in self.arg_variables:
            kil_obj_list.extend(kil_obj.walk())
        return kil_obj_list

    @property
    def type_(self):
        return self.decl_variable.type_

    @property
    def reference(self):
        return self.decl_variable.reference

    @property
    def list_(self):
        return self.decl_variable.list_


class KilFunctionCall(object):

    def __init__(self, class_type=None):
        self.decl_variable = None
        self.arg_variables = list()
        self.public = True

    def walk(self):
        kil_obj_list = [self]
        if self.decl_variable:
            kil_obj_list.extend(self.decl_variable.walk())
        for kil_obj in self.arg_variables:
            kil_obj_list.extend(kil_obj.walk())
        return kil_obj_list

    @property
    def type_(self):
        return self.decl_variable.type_


class KilConstructor(object):

    def __init__(self):
        self.class_type = None
        self.arg_variables = list()
        self.public = True
        self.code_block = None

    def walk(self):
        kil_obj_list = [self]
        if self.class_type:
            kil_obj_list.extend(self.class_type.walk())
        for kil_obj in self.arg_variables:
            kil_obj_list.extend(kil_obj.walk())
        if self.code_block:
            kil_obj_list.extend(self.code_block.walk())
        return kil_obj_list


class KilConstructorCall(object):

    def __init__(self, class_type=None, arg_variables=None):
        self.class_type = class_type
        self.arg_variables = arg_variables if arg_variables else list()
        self.public = True

    def walk(self):
        kil_obj_list = [self]
        if self.class_type:
            kil_obj_list.extend(self.class_type.walk())
        for kil_obj in self.arg_variables:
            kil_obj_list.extend(kil_obj.walk())
        return kil_obj_list


class KilDestructor(object):

    def __init__(self):
        self.class_type = None
        self.public = True
        self.code_block = None

    def walk(self):
        kil_obj_list = [self]
        if self.class_type:
            kil_obj_list.extend(self.class_type.walk())
        if self.code_block:
            kil_obj_list.extend(self.code_block.walk())
        return kil_obj_list


class KilDestructorCall(object):

    def __init__(self, class_type=None):
        self.class_type = class_type
        self.public = True

    def walk(self):
        kil_obj_list = [self]
        if self.class_type:
            kil_obj_list.extend(self.class_type.walk())
        return kil_obj_list


class KilTypeNone(object):

    def walk(self):
        return [self]


class KilTypeBoolean(object):

    def walk(self):
        return [self]


KIL_INTEGER_BYTE_COUNTS = [1, 2, 4, 8]
KIL_INTEGER_BIT_COUNTS = [v * 8 for v in KIL_INTEGER_BYTE_COUNTS]
KIL_INTEGER_TYPES = {}
KIL_INTEGERS_LE = []
KIL_INTEGERS_BE = []
for bits in range(1, KIL_INTEGER_BIT_COUNTS[-1] + 1):
    KIL_INTEGER_TYPES['b{}'.format(bits)] = (False, bits)
for byte_count in KIL_INTEGER_BYTE_COUNTS:
    bits = byte_count * 8
    for sign in ['s', 'u']:
        signed = sign == 's'
        KIL_INTEGERS_LE.append('{}{}{}'.format(sign, byte_count, 'le'))
        KIL_INTEGERS_BE.append('{}{}{}'.format(sign, byte_count, 'be'))
        for endian in ['', 'be', 'le']:
            KIL_INTEGER_TYPES['{}{}{}'.format(sign, byte_count, endian)] = (signed, bits)
KIL_INTEGER_RANGES = []
for byte_count in sorted(KIL_INTEGER_BYTE_COUNTS):
    bits = byte_count * 8
    for signed in [False, True]:
        if not signed:
            min_value = 0
            max_value = 0
            for bit in range(0, bits):
                max_value |= 1 << bit
        else:
            min_value = 1 << (bits - 1)
            max_value = 0
            for bit in range(0, bits - 1):
                max_value |= 1 << bit
            if byte_count == 1:
                min_value = struct.unpack('<b', struct.pack('<B', min_value))[0]
            elif byte_count == 2:
                min_value = struct.unpack('<h', struct.pack('<H', min_value))[0]
            elif byte_count == 4:
                min_value = struct.unpack('<i', struct.pack('<I', min_value))[0]
            elif byte_count == 8:
                min_value = struct.unpack('<q', struct.pack('<Q', min_value))[0]
            else:
                raise NotImplementedError
        KIL_INTEGER_RANGES.append((bits, signed, min_value, max_value))


class KilTypeInteger(object):

    @classmethod
    def convert_value_to_smallest_type(cls, value):
        assert isinstance(value, int)
        for int_bits, int_signed, int_min_value, int_max_value in KIL_INTEGER_RANGES:
            if int_min_value <= value and int_max_value >= value:
                return KilTypeInteger(signed=int_signed, bits=int_bits, bitfield=False)
        raise Exception("value {} does not fit inside any of the supported integer types".format(value))

    @classmethod
    def convert_range_to_smallest_type(cls, min_value, max_value):
        assert isinstance(min_value, int)
        assert isinstance(max_value, int)
        for int_bits, int_signed, int_min_value, int_max_value in KIL_INTEGER_RANGES:
            if int_min_value <= min_value and int_max_value >= max_value:
                return KilTypeInteger(signed=int_signed, bits=int_bits, bitfield=False)
        raise Exception("range {} to {} (inclusive) does not fit inside any of the supported integer types".format(min_value, max_value))

    @classmethod
    def convert_ksy_type_to_endian(self, ksy_type, default=None):
        if ksy_type in KIL_INTEGERS_LE:
            return 'le'
        if ksy_type in KIL_INTEGERS_BE:
            return 'be'
        if ksy_type.startswith('b'):
            return 'be'  # all bitfields are big-endian
        return default

    @classmethod
    def convert_ksy_type_to_type(self, ksy_type, default=None):
        items = KIL_INTEGER_TYPES.get(ksy_type, None)
        if items:
            type_ = KilTypeInteger()
            type_.signed = items[0]
            type_.bits = items[1]
            type_.bitfield = ksy_type.startswith('b')
            return type_
        return default

    @classmethod
    def promote_to_next_bit_size(self, bits):
        for i in range(0, len(KIL_INTEGER_BIT_COUNTS)):
            cur_bits = KIL_INTEGER_BIT_COUNTS[i]
            if (i + 1) < len(KIL_INTEGER_BIT_COUNTS):
                next_bits = KIL_INTEGER_BIT_COUNTS[i + 1]
            else:
                next_bits = KIL_INTEGER_BIT_COUNTS[-1]
            if bits == cur_bits:
                return next_bits
        raise Exception("don't know how to promote {}-bit integer".format(bits))

    @classmethod
    def promote_bitfield_to_integer_bits(self, bits):
        for max_bits in KIL_INTEGER_BIT_COUNTS:
            if bits <= max_bits:
                return max_bits
        raise Exception("don't know how to promote {}-bit bitfield".format(bits))

    def __init__(self, signed=True, bits=32, bitfield=False):
        self.signed = signed
        self.bits = bits
        self.bitfield = bitfield

    def walk(self):
        return [self]

    @property
    def bytes_(self):
        return self.bits >> 3

    @property
    def ksy_type(self):
        for ksy_type, items in KIL_INTEGER_TYPES.items():
            if ksy_type.endswith('be') or ksy_type.endswith('le'):
                continue
            signed, bits = items
            if self.signed == signed and self.bits == bits:
                return ksy_type
        raise NotImplementedError('programmer error, should never reach here')

    @property
    def ksy_integer_type(self):
        integer_bits = self.promote_bitfield_to_integer_bits(self.bits)
        for ksy_type, items in KIL_INTEGER_TYPES.items():
            if ksy_type.startswith('b'):
                continue
            if ksy_type.endswith('be') or ksy_type.endswith('le'):
                continue
            signed, bits = items
            if self.signed == signed and integer_bits == bits:
                return ksy_type
        raise NotImplementedError('programmer error, should never reach here')


class KilTypeFloat(object):

    @classmethod
    def convert_ksy_type_to_endian(self, ksy_type, default=None):
        if ksy_type in ['f4le', 'f8le']:
            return 'le'
        if ksy_type in ['f4be', 'f8be']:
            return 'be'
        return default

    def __init__(self, bits=32):
        self.bits = bits

    def walk(self):
        return [self]

    @property
    def bytes_(self):
        return self.bits >> 3

    @property
    def ksy_type(self):
        if self.bits == 32:
            return 'f4'
        if self.bits == 64:
            return 'f8'
        raise NotImplementedError('programmer error, should never reach here')


class KilTypeBuffer(object):

    def walk(self):
        return [self]


class KilTypeString(object):
    ASCII = 1
    UTF16LE = 2
    UTF16BE = 3
    UTF8 = 4
    KSY_ENCODING_MAP = {
        'ascii': ASCII,
        'utf16le': UTF16LE,
        'utf16be': UTF16BE,
        'utf8': UTF8,
    }
    KIL_ENCODING_MAP = {
        ASCII: 'ascii',
        UTF16LE: 'utf16le',
        UTF16BE: 'utf16be',
        UTF8: 'utf8',
    }
    NAME_ENCODING_MAP = {
        ASCII: 'ascii',
        UTF16LE: 'utf16le',
        UTF16BE: 'utf16be',
        UTF8: 'utf8',
    }

    @classmethod
    def convert_ksy_encoding(cls, ksy_encoding):
        assert ksy_encoding
        ksy_encoding = ksy_encoding.lower().replace('-', '').replace('_', '').strip()
        encoding = cls.KSY_ENCODING_MAP.get(ksy_encoding, None)
        if encoding:
            return encoding
        raise NotImplementedError("encoding '{}'".format(ksy_encoding))

    @classmethod
    def convert_kil_encoding(cls, encoding):
        assert encoding
        ksy_encoding = cls.KIL_ENCODING_MAP.get(encoding, None)
        if ksy_encoding:
            return ksy_encoding
        raise NotImplementedError("encoding code {}".format(encoding))

    def __init__(self, encoding=None):
        self.encoding = encoding

    def walk(self):
        return [self]

    @property
    def wide(self):
        return self.encoding in [KilTypeString.UTF16LE, KilTypeString.UTF16BE]

    @property
    def char_type(self):
        return KilTypeChar(self.encoding)

    def compile_enum_variable(self):
        enum = KilEnum(class_type=KilTypeIO(), name='encoding')
        enum_value_name = self.NAME_ENCODING_MAP.get(self.encoding, None)
        if enum_value_name:
            return KilEnumValue(enum, enum_value_name)
        raise NotImplementedError("encoding code {}".format(self.encoding))

    @property
    def ksy_encoding(self):
        return self.convert_kil_encoding(self.encoding)


class KilTypeChar(object):
    ASCII = KilTypeString.ASCII
    UTF16LE = KilTypeString.UTF16LE
    UTF16BE = KilTypeString.UTF16BE
    UTF8 = KilTypeString.UTF8

    def __init__(self, encoding=None):
        self.encoding = encoding

    def walk(self):
        return [self]

    @property
    def wide(self):
        return self.encoding in [KilTypeChar.UTF16LE, KilTypeChar.UTF16BE]

    @property
    def integer_type(self):
        bits = 16 if self.wide else 8
        return KilTypeInteger(signed=True, bits=bits)


class KilTypeSize(object):

    def walk(self):
        return [self]


class KilTypeIO(object):

    def walk(self):
        return [self]


class KilTypeClass(object):

    def __init__(self, name=None):
        self.name = name
        self.external = False  # used for external user-defined process classes

    def walk(self):
        return [self]


def kil_type_to_dict(type_):
    type_dict = dict()
    if isinstance(type_, KilTypeNone):
        type_dict['type'] = 'none'
    elif isinstance(type_, KilTypeBoolean):
        type_dict['type'] = 'boolean'
    elif isinstance(type_, KilTypeInteger):
        type_dict['type'] = 'integer'
        type_dict['signed'] = type_.signed
        type_dict['bits'] = type_.bits
        type_dict['bitfield'] = type_.bitfield
    elif isinstance(type_, KilTypeFloat):
        type_dict['type'] = 'float'
        type_dict['bits'] = type_.bits 
    elif isinstance(type_, KilTypeBuffer):
        type_dict['type'] = 'buffer'
    elif isinstance(type_, KilTypeString):
        type_dict['type'] = 'string'
        type_dict['encoding'] = type_.encoding
    elif isinstance(type_, KilTypeChar):
        type_dict['type'] = 'char'
        type_dict['encoding'] = type_.encoding
    elif isinstance(type_, KilTypeSize):
        type_dict['type'] = 'size'
    elif isinstance(type_, KilTypeIO):
        type_dict['type'] = 'io'
    elif isinstance(type_, KilTypeClass):
        type_dict['type'] = 'class'
        type_dict['name'] = type_.name
        type_dict['external'] = type_.external
    else:
        raise NotImplementedError(type_)
    return type_dict


def kil_type_from_dict(type_dict):
    type_ = None
    if type_dict['type'] == 'none':
        type_ = KilTypeNone()
    elif type_dict['type'] == 'boolean':
        type_ = KilTypeBoolean()
    elif type_dict['type'] == 'integer':
        type_ = KilTypeInteger()
        type_.signed = type_dict['signed']
        type_.bits = type_dict['bits']
        type_.bitfield = type_dict['bitfield']
    elif type_dict['type'] == 'float':
        type_ = KilTypeFloat()
        type_.bits = type_dict['bits']
    elif type_dict['type'] == 'buffer':
        type_ = KilTypeBuffer()
    elif type_dict['type'] == 'string':
        type_ = KilTypeString()
        type_.encoding = type_dict['encoding']
    elif type_dict['type'] == 'char':
        type_ = KilTypeChar()
        type_.encoding = type_dict['encoding']
    elif type_dict['type'] == 'size':
        type_ = KilTypeSize()
    elif type_dict['type'] == 'io':
        type_ = KilTypeIO()
    elif type_dict['type'] == 'class':
        type_ = KilTypeClass()
        type_.name = type_dict['name']
        type_.external = type_dict['external']
    else:
        raise NotImplementedError(type_name)
    return type_


class KilVariable(object):
    THIS_NAME = '**this**'

    def __init__(self, name=None, type_=None, mutable=True, reference=False):
        self.name = name
        self.type_ = type_ if type_ else KilTypeNone()
        self.mutable = mutable
        self.reference = reference
        self.list_ = False

    def walk(self):
        kil_obj_list = [self]
        if self.type_:
            kil_obj_list.extend(self.type_.walk())
        return kil_obj_list


class KilDeclaration(object):

    def __init__(self, variable=None):
        self.variable = variable

    def walk(self):
        kil_obj_list = [self]
        if self.variable:
            kil_obj_list.extend(self.variable.walk())
        return kil_obj_list

    @property
    def name(self):
        return self.variable.name

    @property
    def type_(self):
        return self.variable.type_

    @property
    def mutable(self):
        return self.variable.mutable

    @property
    def reference(self):
        return self.variable.reference

    @property
    def list_(self):
        return self.variable.list_


class KilLiteral(object):

    def __init__(self, value=None, type_=None):
        self.value = value
        self.type_ = type_ if type_ else KilTypeNone()

    def walk(self):
        kil_obj_list = [self]
        if self.type_:
            kil_obj_list.extend(self.type_.walk())
        return kil_obj_list

    @property
    def mutable(self):
        return False

    @property
    def reference(self):
        if isinstance(self.type_, KilTypeChar) or \
                isinstance(self.type_, KilTypeInteger) or \
                isinstance(self.type_, KilTypeBoolean) or \
                isinstance(self.type_, KilTypeSize):
            return False
        return True

    @property
    def list_(self):
        return False

    @property
    def value_as_integer(self):
        value = self.value
        if value is None:
            value = 0
        if isinstance(value, float):
            value = int(value)
        if isinstance(value, str):
            try:
                value = int(value, 0)
            except ValueError:
                pass
        if isinstance(value, bool):
            value = 1 if value else 0
        if not isinstance(value, int):
            value = 0
        return value


class KilEnum(object):

    def __init__(self, class_type=None, name=None):
        self.class_type = class_type
        self.name = name
        self.enums = list()

    def walk(self):
        kil_obj_list = [self]
        if self.class_type:
            kil_obj_list.extend(self.class_type.walk())
        for enum in self.enums:
            kil_obj_list.extend(enum.walk())
        return kil_obj_list

    @property
    def type_(self):
        if len(self.enums) == 0:
            return KilTypeInteger()
        min_value = self.enums[0].value
        max_value = self.enums[0].value
        for enum_value in self.enums[1:]:
            if enum_value.value < min_value:
                min_value = enum_value.value
            if enum_value.value > max_value:
                max_value = enum_value.value
        return KilTypeInteger.convert_range_to_smallest_type(min_value, max_value)


class KilEnumValue(object):

    def __init__(self, enum=None, name=None, value=None):
        self.enum = enum
        self.name = name
        self.value = value

    def walk(self):
        return [self]

    @property
    def type_(self):
        return self.enum.type_

    @property
    def mutable(self):
        return False

    @property
    def reference(self):
        return False

    @property
    def list_(self):
        return False


class KilMember(object):

    def __init__(self):
        self.class_type = None
        self.variable = None
        self.public = False

    def walk(self):
        kil_obj_list = [self]
        if self.class_type:
            kil_obj_list.extend(self.class_type.walk())
        if self.variable:
            kil_obj_list.extend(self.variable.walk())
        return kil_obj_list

    @property
    def name(self):
        return self.variable.name

    @property
    def type_(self):
        return self.variable.type_

    @property
    def list_(self):
        return self.variable.list_


class KilBlock(object):

    def __init__(self, children=None):
        self.children = children if children else list()

    def walk(self):
        kil_obj_list = [self]
        for kil_obj in self.children:
            kil_obj_list.extend(kil_obj.walk())
        return kil_obj_list


class KilTryCatchBlock(object):

    def __init__(self, try_body=None, catch_body=None, final_body=None, rethrow=False):
        self.try_body = try_body
        self.catch_body = catch_body
        self.final_body = final_body
        self.rethrow = rethrow

    def walk(self):
        kil_obj_list = [self]
        if self.try_body:
            kil_obj_list.extend(self.try_body.walk())
        if self.catch_body:
            kil_obj_list.extend(self.catch_body.walk())
        if self.final_body:
            kil_obj_list.extend(self.final_body.walk())
        return kil_obj_list


class KilReturn(object):

    def __init__(self, return_=None):
        self.return_ = return_

    def walk(self):
        kil_obj_list = [self]
        if self.return_:
            kil_obj_list.extend(self.return_.walk())
        return kil_obj_list

    @property
    def type_(self):
        return self.return_.type_

    @property
    def mutable(self):
        return self.return_.mutable

    @property
    def list_(self):
        return self.return_.list_


class KilIf(object):

    def __init__(self, expr, true_body=None, false_body=None):
        self.expr = expr
        self.true_body = true_body
        self.false_body = false_body

    def walk(self):
        kil_obj_list = [self]
        if self.expr:
            kil_obj_list.extend(self.expr.walk())
        if self.true_body:
            kil_obj_list.extend(self.true_body.walk())
        if self.false_body:
            kil_obj_list.extend(self.false_body.walk())
        return kil_obj_list


class KilFor(object):

    def __init__(self, start=None, condition=None, update=None, body=None):
        self.start = start
        self.condition = condition
        self.update = update
        self.body = body

    def walk(self):
        kil_obj_list = [self]
        if self.start:
            kil_obj_list.extend(self.start.walk())
        if self.condition:
            kil_obj_list.extend(self.condition.walk())
        if self.update:
            kil_obj_list.extend(self.update.walk())
        if self.body:
            kil_obj_list.extend(self.body.walk())
        return kil_obj_list


class KilWhile(object):

    def __init__(self, expr=None, body=None):
        self.expr = expr
        self.body = body

    def walk(self):
        kil_obj_list = [self]
        if self.expr:
            kil_obj_list.extend(self.expr.walk())
        if self.body:
            kil_obj_list.extend(self.body.walk())
        return kil_obj_list


class KilDoWhile(object):

    def __init__(self, expr=None, body=None):
        self.expr = expr
        self.body = body

    def walk(self):
        kil_obj_list = [self]
        if self.expr:
            kil_obj_list.extend(self.expr.walk())
        if self.body:
            kil_obj_list.extend(self.body.walk())
        return kil_obj_list


class KilBreak(object):

    def __init__(self):
        pass

    def walk(self):
        return [self]


class KilOperation(object):

    def __init__(self, a=None, op=None, b=None, c=None):
        self.a = a
        self.op = op
        self.b = b
        self.c = c

    def walk(self):
        kil_obj_list = [self]
        if self.a:
            kil_obj_list.extend(self.a.walk())
        if self.b:
            kil_obj_list.extend(self.b.walk())
        if self.c:
            kil_obj_list.extend(self.c.walk())
        return kil_obj_list

    @property
    def mutable(self):
        if self.op == '.':
            if not self.a.mutable:
                return False
            return self.b.mutable
        if self.op == '[]':
            return self.a.mutable
        return False

    @property
    def type_(self):
        if self.op == '.':
            return self.b.type_
        if self.op == '?':
            return self.b.type_
        if self.op in ['==', '!=', '<=', '>=', '<', '>', 'and', 'or', 'not']:
            return KilTypeBoolean()
        return self.a.type_

    @property
    def reference(self):
        if self.op == '.':
            return self.b.reference
        if self.op == '[]':
            if isinstance(self.op.a.type_, KilTypeClass) or \
                    isinstance(self.op.a.type_, KilTypeBuffer) or \
                    isinstance(self.op.a.type_, KilTypeString):
                return True
            return False
        return False

    @property
    def list_(self):
        if self.op == '.':
            return self.b.list_
        if self.op in ['[]', 'len', 'clear', 'add']:
            return False
        return self.a.list_


class KilTypecast(object):

    def __init__(self, value=None, type_=None):
        self.value = value
        self.type_ = type_

    def walk(self):
        kil_obj_list = [self]
        if self.value:
            kil_obj_list.extend(self.value.walk())
        if self.type_:
            kil_obj_list.extend(self.type_.walk())
        return kil_obj_list

    @property
    def mutable(self):
        return self.value.mutable

    @property
    def reference(self):
        return self.value.reference

    @property
    def list_(self):
        return self.value.list_
