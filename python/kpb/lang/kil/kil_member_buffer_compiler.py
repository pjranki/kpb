import os
import sys

from .kil_types import *
from .kil_ksy_library import *
from .kil_io_interface import *
from .kil_member_generic_compiler import *


class KilMemberBufferCompiler(KilMemberGenericCompiler):

    def compile_methods(self):
        list_ = KilMemberGenericInterface(self.root_name, self.class_name, self.member_name, None, self.ksy).list_
        methods = list()
        if not list_:
            methods.append(self.compile_getter_method())
            methods.append(self.compile_size_method())
            methods.append(self.compile_setter_method())
        else:
            methods.append(self.compile_getlist_method())
            methods.append(self.compile_count_method())
            methods.append(self.compile_atindex_method())
            methods.append(self.compile_sizeat_method())
            methods.append(self.compile_add_method())
        methods.append(self.compile_clear_method())
        methods.append(self.compile_has_method())
        methods.append(self.compile_randomize_method())
        return methods

    def compile_getter_method(self):
        method = super(KilMemberBufferCompiler, self).compile_getter_method()
        method.decl_variable.variable.reference = True
        if isinstance(method.code_block.children[-1].return_, KilVariable):
            # HACK: for tmp variable used in process-value
            method.decl_variable.variable.reference = False
            method.code_block.children[-1].return_.reference = False
        return method

    def compile_setter_method(self):
        method = super(KilMemberBufferCompiler, self).compile_setter_method()
        method.arg_variables[0].variable.reference = True
        return method

    def compile_read_call(self):
        ksy_member_dict = self.ksy.find_ksy_member_dict_from_member_name(self.root_name, self.class_name, self.member_name)
        _io = KilIoInterface(reference=True)
        if ksy_member_dict.get('size-eos', False):
            read_size_variable = KilOperation(_io.size(), '-', _io.pos())
        else:
            read_size_variable = self.expr_compiler(this_mutable=True).compile_expr(ksy_member_dict['size'], KilTypeSize())
        return _io.read_bytes(read_size_variable)

    def compile_write_call(self, value_variable):
        _io = KilIoInterface(reference=True)
        ksy_member_dict = self.ksy.find_ksy_member_dict_from_member_name(self.root_name, self.class_name, self.member_name)
        if ksy_member_dict.get('size-eos', False):
            this = KilVariable()
            this.name = KilVariable.THIS_NAME
            this.type_ = KilTypeClass(name=self.class_name)
            this.mutable = False
            this.reference = True
            this_member = KilMemberBufferInterface(self.root_name, self.class_name, self.member_name, this, ksy=self.ksy)
            write_size_variable = this_member.size()
        else:
            write_size_variable = self.expr_compiler(this_mutable=False).compile_expr(ksy_member_dict['size'], KilTypeSize())
        return _io.write_bytes(value_variable, write_size_variable)
