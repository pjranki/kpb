import os
import sys

from .kil_types import *
from .kil_ksy_library import *
from .kil_member_generic_interface import *

class KilMemberBooleanInterface(KilMemberGenericInterface):

    def constructor_default(self, expr_compiler):
        default = self.ksy_member_dict.get('-default', False)
        if isinstance(default, str):
            assert default in ['true', 'false']
            default = True if default == 'true' else False
        return KilLiteral(default, self.type_)

    def destructor_default(self):
        return KilLiteral(False, self.type_)

    def atindex(self, index_variable):
        method_call = super(KilMemberBooleanInterface, self).atindex(index_variable)
        method_call.b.decl_variable.reference = False
        return method_call
