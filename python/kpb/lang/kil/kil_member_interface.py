import os
import sys

from .kil_ksy_library import *
from .kil_member_generic_interface import *
from .kil_member_boolean_interface import *
from .kil_member_integer_interface import *
from .kil_member_float_interface import *
from .kil_member_string_interface import *
from .kil_member_buffer_interface import *
from .kil_member_class_interface import *


class KilMemberInterface(object):
    KIL_TYPE_INTERFACE_MAP = {
        KilTypeBoolean: KilMemberBooleanInterface,
        KilTypeInteger: KilMemberIntegerInterface,
        KilTypeFloat: KilMemberFloatInterface,
        KilTypeString: KilMemberStringInterface,
        KilTypeBuffer: KilMemberBufferInterface,
        KilTypeClass: KilMemberClassInterface,
    }

    @classmethod
    def __new__(cls, new_cls, root_name, class_name, member_name, class_variable, ksy):
        if new_cls == KilMemberInterface:
            generic = KilMemberGenericInterface(root_name, class_name, member_name, class_variable, ksy)
            interface = cls.KIL_TYPE_INTERFACE_MAP[type(generic.type_)](root_name, class_name, member_name, class_variable, ksy)
            return interface
        return super().__new__(new_cls)
