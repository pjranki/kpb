import os
import sys

from .kil_types import *
from .kil_ksy_library import *
from .kil_expr_compiler import *
from .kil_update_compiler import *
from .kil_process_value_interface import *


class KilInstanceCompiler(object):

    def __init__(self, root_name, class_name, instance_name, ksy):
        self.root_name = root_name
        self.class_name = class_name
        self.instance_name = instance_name
        self.ksy = ksy
        self.cache_value_expr = None

    def expr_compiler(self, this_mutable):
        this = KilVariable()
        this.name = KilVariable.THIS_NAME
        this.type_ = KilTypeClass(name=self.class_name)
        this.mutable = True
        this.reference = True
        member_name = self.instance_name
        return KilExprCompiler(self.root_name, self.class_name, member_name, class_variable=this, ksy=self.ksy)

    @property
    def value_expr(self):
        if self.cache_value_expr:
            return self.cache_value_expr
        ksy_value_instance_dict = self.ksy.find_ksy_value_instance_dict_from_name(self.root_name, self.class_name, self.instance_name)
        value_expr = self.expr_compiler(this_mutable=False).compile_expr(ksy_value_instance_dict['value'])
        self.ksy.cache_class_member_type(self.class_name, self.instance_name, value_expr.type_)
        self.ksy.cache_class_member_is_list(self.class_name, self.instance_name, value_expr.list_)
        self.cache_value_expr = value_expr
        return value_expr

    def compile_methods(self):
        methods = [self.compile_getter_method()]
        if not self.value_expr.list_ and not isinstance(self.value_expr.type_, KilTypeClass):
            # FUTURE: support classes and lists as part of instance setter method
            methods.append(self.compile_setter_method())
        return methods

    def compile_getter_method(self):
        ksy_value_instance_dict = self.ksy.find_ksy_value_instance_dict_from_name(self.root_name, self.class_name, self.instance_name)
        method = KilMethod()
        method.class_type = KilTypeClass(name=self.class_name)
        method.mutable = False
        method.public = True
        method.hint_treat_as_member_variable = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = self.instance_name
        method.decl_variable.type_ = self.value_expr.type_
        method.decl_variable.mutable = False
        method.decl_variable.reference = self.value_expr.reference
        method.decl_variable.list_ = self.value_expr.list_
        method.decl_variable = KilDeclaration(method.decl_variable)
        process_value_expr = ksy_value_instance_dict.get('-process-value', None)
        if process_value_expr:
            process_value = KilProcessValueInterface(self.instance_name, process_value_expr)
            tmp_variable = KilVariable()
            tmp_variable.name = '_process_value_tmp_{}'.format(self.instance_name)
            tmp_variable.type_ = self.value_expr.type_
            tmp_variable.mutable = False
            tmp_variable.reference = method.decl_variable.reference
            method.code_block = KilBlock([
                process_value.constructor(self.expr_compiler(this_mutable=False)),
                KilOperation(KilDeclaration(tmp_variable), '=', process_value.getter(self.value_expr)),
                process_value.destructor(),
                KilReturn(tmp_variable)
            ])
        else:
            method.code_block = KilBlock([
                KilReturn(self.value_expr)
            ])
        return method

    def compile_setter_method(self):
        ksy_value_instance_dict = self.ksy.find_ksy_value_instance_dict_from_name(self.root_name, self.class_name, self.instance_name)
        method = KilMethod()
        method.class_type = KilTypeClass(name=self.class_name)
        method.mutable = True
        method.public = True
        method.decl_variable = KilVariable()
        method.decl_variable.name = 'set_' + self.instance_name
        method.decl_variable.type_ = KilTypeBoolean()
        method.decl_variable.mutable = False
        method.decl_variable.reference = False
        method.decl_variable.list_ = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        value_variable = KilVariable()
        value_variable.name = '_value'
        value_variable.type_ = self.value_expr.type_
        value_variable.mutable = False
        value_variable.reference = self.value_expr.reference
        value_variable.list_ = self.value_expr.list_
        method.arg_variables = [KilDeclaration(value_variable)]
        process_value_expr = ksy_value_instance_dict.get('-process-value', None)
        if process_value_expr:
            process_value = KilProcessValueInterface(self.instance_name, process_value_expr)
            method.code_block = KilBlock([
                process_value.constructor(self.expr_compiler(this_mutable=True)),
                KilOperation(value_variable, '=', process_value.setter(value_variable)),
                process_value.destructor()
            ])
        else:
            method.code_block = KilBlock([
                KilTypecast(value_variable, KilTypeNone()),
            ])

        # changes have been made to this member, update/set everything else as needed
        method.code_block.children.extend(self.compile_update_block(value_variable).children)

        # done
        method.code_block.children.append(KilReturn(KilLiteral(True, KilTypeBoolean())))
        return method

    def compile_update_block(self, value_variable):
        block = KilBlock()

        this_variable = KilVariable()
        this_variable.name = KilVariable.THIS_NAME
        this_variable.type_ = KilTypeClass(name=self.class_name)
        this_variable.mutable = True
        this_variable.reference = True
        this = KilClassInterface(self.root_name, self.class_name, this_variable, self.ksy)

        # don't recursively call this instance's set method
        class_id = self.ksy.ksy_class_id(self.root_name, self.class_name)
        class_instance_id = self.ksy.ksy_class_id(self.root_name, self.class_name, self.instance_name)
        class_id_list = [class_id, class_instance_id]

        # disable update on this class (to avoid infinite recursion)
        update_block = KilBlock()
        update_block_error_handled = KilBlock()
        update_block_error_handled.children.extend(this.update_disable_block(class_id_list, backup=True).children)
        try_catch_block = KilTryCatchBlock(rethrow=True)
        try_catch_block.try_body = update_block
        update_block_error_handled.children.append(try_catch_block)

        # re-enable update on return OR error/exception
        try_catch_block.final_body = this.update_enable_block(class_id_list, restore=True)
        block.children.append(KilIf(expr=this.update_enabled(class_instance_id), true_body=update_block_error_handled))

        # update member values (specific to when a change is made on this member)
        update_compiler = KilUpdateCompiler(self.root_name, self.class_name, instance_name=self.instance_name, value_variable=value_variable, ksy=self.ksy)
        update_block.children.extend(update_compiler.compile_update_block().children)

        # do a full update of the class (now that the member-specific updates are done)
        block.children.append(KilIf(expr=this.update(), false_body=KilBlock([
            KilReturn(KilLiteral(False, KilTypeBoolean())),
        ])))

        return block
