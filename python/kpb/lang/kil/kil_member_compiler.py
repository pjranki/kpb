import os
import sys

from .kil_ksy_library import *
from .kil_member_generic_interface import *
from .kil_member_boolean_compiler import *
from .kil_member_integer_compiler import *
from .kil_member_float_compiler import *
from .kil_member_string_compiler import *
from .kil_member_buffer_compiler import *
from .kil_member_class_compiler import *


class KilMemberCompiler(object):
    KIL_TYPE_COMPILER_MAP = {
        KilTypeBoolean: KilMemberBooleanCompiler,
        KilTypeInteger: KilMemberIntegerCompiler,
        KilTypeFloat: KilMemberFloatCompiler,
        KilTypeString: KilMemberStringCompiler,
        KilTypeBuffer: KilMemberBufferCompiler,
        KilTypeClass: KilMemberClassCompiler,
    }

    @classmethod
    def __new__(cls, new_cls, root_name, class_name, member_name, ksy):
        if new_cls == KilMemberCompiler:
            class_variable = None
            generic = KilMemberGenericInterface(root_name, class_name, member_name, class_variable, ksy)
            compiler = cls.KIL_TYPE_COMPILER_MAP[type(generic.type_)](root_name, class_name, member_name, ksy)
            return compiler
        return super().__new__(new_cls)
