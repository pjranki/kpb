import os
import sys

from .kil_types import *
from .kil_ksy_library import *


class KilMemberGenericInterface(object):

    def __init__(self, root_name, class_name, member_name, class_variable, ksy):
        self.root_name = root_name
        self.class_name = class_name
        self.member_name = member_name
        self.class_variable = class_variable
        self.ksy = ksy

    @property
    def ksy_member_dict(self):
        return self.ksy.find_ksy_member_dict_from_member_name(self.root_name, self.class_name, self.member_name)

    @property
    def class_type(self):
        return KilTypeClass(name=self.class_name)

    @property
    def list_(self):
        return True if self.ksy_member_dict.get('repeat') else False

    @property
    def type_(self):
        ksy_type = self.ksy_member_dict.get('type', None)
        if ksy_type:
            ksy_type = ksy_type.split('(')[0]  # for when class is constructed with params
        if ksy_type == 'bool':
            return KilTypeBoolean()
        if ksy_type:
            type_ = KilTypeInteger.convert_ksy_type_to_type(ksy_type, default=None)
            if type_:
                return type_
            if ksy_type.lower().strip() in ['str', 'strz']:
                ksy_encoding = self.ksy.find_ksy_string_encoding(self.root_name, self.class_name, self.member_name)
                return KilTypeString(KilTypeString.convert_ksy_encoding(ksy_encoding))
            if ksy_type in ['f4', 'f4le', 'f4be']:
                return KilTypeFloat(bits=32)
            if ksy_type in ['f8', 'f8le', 'f8be']:
                return KilTypeFloat(bits=64)
            return KilTypeClass(name=ksy_type)
        ksy_size = self.ksy_member_dict.get('size', None)
        if ksy_size is not None:
            return KilTypeBuffer()
        ksy_size_eos = self.ksy_member_dict.get('size-eos', False)
        if ksy_size_eos:
            return KilTypeBuffer()
        raise NotImplementedError('type for: {}'.format(repr(self.ksy_member_dict)))

    def this(self, mutable=True):
        assert self.class_variable
        if not self.class_variable.mutable and mutable:
            raise NotImplementedError('you are trying to mutate an immutable class instance')
        return self.class_variable

    @property
    def value(self):
        variable = KilVariable()
        variable.name = self.member_name
        variable.type_ = self.type_
        variable.mutable = True
        variable.reference = False
        variable.list_ = self.list_
        return KilOperation(self.this(self.class_variable.mutable), '.', variable)

    def clear(self):
        method_call = KilMethodCall()
        method_call.class_type = self.class_type
        method_call.mutable = True
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'clear_' + self.member_name
        method_call.decl_variable.type_ = KilTypeNone()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = list()
        method_call = KilOperation(self.this(mutable=True), '.', method_call)
        return method_call

    def has(self):
        method_call = KilMethodCall()
        method_call.class_type = self.class_type
        method_call.mutable = False
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'has_' + self.member_name
        method_call.decl_variable.type_ = KilTypeBoolean()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = list()
        method_call = KilOperation(self.this(mutable=False), '.', method_call)
        return method_call

    def read_has_expr(self, expr_compiler):
        if_expr = self.ksy_member_dict.get('if', 'true')
        if expr_compiler.expr_contains_io(if_expr):
            # has method has no access to io streams (they do not existing outside of parsing/serializing)
            # so inline the has method code so that '_io' object can be used as part of has code
            return expr_compiler.compile_expr(if_expr, KilTypeBoolean())
        return self.has()

    def write_has_expr(self, expr_compiler):
        return self.has()

    def get(self):
        method_call = KilMethodCall()
        method_call.class_type = self.class_type
        method_call.mutable = False
        method_call.public = True
        method_call.hint_treat_as_member_variable = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = self.member_name
        method_call.decl_variable.type_ = self.type_
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        if self.list_:
            method_call.decl_variable.reference = True  # pass a reference rather than a copy
        method_call.decl_variable.list_ = self.list_
        method_call.arg_variables = list()
        method_call = KilOperation(self.this(mutable=False), '.', method_call)
        return method_call

    def getlist(self):
        return self.get()

    def set(self, value_variable):
        method_call = KilMethodCall()
        method_call.class_type = self.class_type
        method_call.mutable = True
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'set_' + self.member_name
        method_call.decl_variable.type_ = KilTypeBoolean()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = [value_variable]
        method_call = KilOperation(self.this(mutable=True), '.', method_call)
        return method_call

    def size(self):
        method_call = KilMethodCall()
        method_call.class_type = self.class_type
        method_call.mutable = False
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = self.member_name + '_size'
        method_call.decl_variable.type_ = KilTypeSize()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = list()
        method_call = KilOperation(self.this(mutable=False), '.', method_call)
        return method_call

    def randomize(self):
        method_call = KilMethodCall()
        method_call.class_type = self.class_type
        method_call.mutable = True
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'randomize_' + self.member_name
        method_call.decl_variable.type_ = KilTypeBoolean()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = list()
        method_call = KilOperation(self.this(mutable=False), '.', method_call)
        return method_call

    def count(self):
        return self.size()

    def atindex(self, index_variable):
        method_call = KilMethodCall()
        method_call.class_type = self.class_type
        method_call.mutable = False
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = self.member_name + '_at'
        method_call.decl_variable.type_ = self.type_
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = True
        method_call.decl_variable.list_ = False
        method_call.arg_variables = [index_variable]
        method_call = KilOperation(self.this(mutable=False), '.', method_call)
        return method_call

    def sizeat(self, index_variable):
        method_call = KilMethodCall()
        method_call.class_type = self.class_type
        method_call.mutable = False
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = self.member_name + '_size_at'
        method_call.decl_variable.type_ = KilTypeSize()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = [index_variable]
        method_call = KilOperation(self.this(mutable=False), '.', method_call)
        return method_call

    def add(self, value_variable):
        method_call = KilMethodCall()
        method_call.class_type = self.class_type
        method_call.mutable = True
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'add_' + self.member_name
        method_call.decl_variable.type_ = KilTypeBoolean()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = [value_variable]
        method_call = KilOperation(self.this(mutable=True), '.', method_call)
        return method_call
