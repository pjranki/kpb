import os
import sys

from .kil_types import *


class KilIoInterface(object):

    def __init__(self, reference=True):
        self.reference = reference

    @property
    def this(self):
        variable = KilVariable()
        variable.name = '_io'
        variable.type_ = KilTypeIO()
        variable.mutable = True
        variable.reference = self.reference
        return variable

    @property
    def immutable_this(self):
        variable = KilVariable()
        variable.name = '_io'
        variable.type_ = KilTypeIO()
        variable.mutable = False
        variable.reference = self.reference
        return variable

    def constructor(self):
        call_constructor = KilConstructorCall(KilTypeIO())
        call_constructor = KilOperation(KilDeclaration(self.this), '=', call_constructor)
        return call_constructor

    def destructor(self):
        call_destructor = KilDestructorCall(KilTypeIO())
        call_destructor = KilOperation(self.this, '.', call_destructor)
        return call_destructor

    def set_readonly(self, data_variable):
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.decl_variable = KilVariable('set_readonly', KilTypeNone(), mutable=False, reference=False)
        method_call.arg_variables = [data_variable]
        method_call.mutable = True
        method_call.public = True
        method_call = KilOperation(self.this, '.', method_call)
        return method_call

    def set_readonly_or_fill(self, data_variable, fill_variable):
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.decl_variable = KilVariable('set_readonly_or_fill', KilTypeNone(), mutable=False, reference=False)
        method_call.arg_variables = [data_variable, fill_variable]
        method_call.mutable = True
        method_call.public = True
        method_call = KilOperation(self.this, '.', method_call)
        return method_call

    def get(self):
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.decl_variable = KilVariable('get', KilTypeBuffer(), reference=True)
        method_call.arg_variables = list()
        method_call.mutable = True
        method_call.public = True
        method_call = KilOperation(self.this, '.', method_call)
        return method_call

    def get_size(self):
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.decl_variable = KilVariable('get_size', KilTypeSize())
        method_call.arg_variables = list()
        method_call.mutable = True
        method_call.public = True
        method_call = KilOperation(self.this, '.', method_call)
        return method_call

    def ok(self):
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.decl_variable = KilVariable('ok', KilTypeBoolean())
        method_call.arg_variables = list()
        method_call.mutable = False
        method_call.public = True
        method_call.hint_treat_as_member_variable = True
        method_call = KilOperation(self.immutable_this, '.', method_call)
        return method_call

    def eof(self):
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.decl_variable = KilVariable('eof', KilTypeBoolean())
        method_call.arg_variables = list()
        method_call.mutable = False
        method_call.public = True
        method_call.hint_treat_as_member_variable = True
        method_call = KilOperation(self.immutable_this, '.', method_call)
        return method_call

    def size(self):
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.decl_variable = KilVariable('size', KilTypeSize())
        method_call.arg_variables = list()
        method_call.mutable = False
        method_call.public = True
        method_call.hint_treat_as_member_variable = True
        method_call = KilOperation(self.immutable_this, '.', method_call)
        return method_call

    def pos(self):
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.decl_variable = KilVariable('pos', KilTypeSize())
        method_call.arg_variables = list()
        method_call.mutable = False
        method_call.public = True
        method_call.hint_treat_as_member_variable = True
        method_call = KilOperation(self.immutable_this, '.', method_call)
        return method_call

    def bit_size(self):
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.decl_variable = KilVariable('bit_size', KilTypeSize())
        method_call.arg_variables = list()
        method_call.mutable = False
        method_call.public = True
        method_call.hint_treat_as_member_variable = True
        method_call = KilOperation(self.immutable_this, '.', method_call)
        return method_call

    def bit_pos(self):
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.decl_variable = KilVariable('bit_pos', KilTypeSize())
        method_call.arg_variables = list()
        method_call.mutable = False
        method_call.public = True
        method_call.hint_treat_as_member_variable = True
        method_call = KilOperation(self.immutable_this, '.', method_call)
        return method_call

    def bits(self):
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.decl_variable = KilVariable('bits', KilTypeInteger(signed=False, bits=64))
        method_call.arg_variables = list()
        method_call.mutable = False
        method_call.public = True
        method_call.hint_treat_as_member_variable = True
        method_call = KilOperation(self.immutable_this, '.', method_call)
        return method_call

    def set_bits(self, bits_variable):
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.decl_variable = KilVariable('set_bits', KilTypeNone(), mutable=False, reference=False)
        method_call.arg_variables = [bits_variable]
        method_call.mutable = True
        method_call.public = True
        method_call = KilOperation(self.this, '.', method_call)
        return method_call

    def bits_left(self):
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.decl_variable = KilVariable('bits_left', KilTypeInteger(signed=False, bits=8))
        method_call.arg_variables = list()
        method_call.mutable = False
        method_call.public = True
        method_call.hint_treat_as_member_variable = True
        method_call = KilOperation(self.immutable_this, '.', method_call)
        return method_call

    def set_bits_left(self, bits_variable):
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.decl_variable = KilVariable('set_bits_left', KilTypeNone(), mutable=False, reference=False)
        method_call.arg_variables = [bits_variable]
        method_call.mutable = True
        method_call.public = True
        method_call = KilOperation(self.this, '.', method_call)
        return method_call

    def align_to_byte(self):
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.decl_variable = KilVariable('align_to_byte', KilTypeNone())
        method_call.arg_variables = list()
        method_call.mutable = True
        method_call.public = True
        method_call = KilOperation(self.this, '.', method_call)
        return method_call

    def push_substream(self, stream_variable=None, pos_variable=None, size_variable=None):
        if not stream_variable:
            stream_variable = self.this
        if pos_variable:
            set_pos = KilLiteral(True, KilTypeBoolean())
        else:
            set_pos = KilLiteral(False, KilTypeBoolean())
            pos_variable = KilLiteral(0, KilTypeSize())
        if size_variable:
            set_size = KilLiteral(True, KilTypeBoolean())
        else:
            set_size = KilLiteral(False, KilTypeBoolean())
            size_variable = KilLiteral(0, KilTypeSize())
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.decl_variable = KilVariable('push_substream', KilTypeNone())
        method_call.arg_variables = [stream_variable, set_pos, pos_variable, set_size, size_variable]
        method_call.mutable = True
        method_call.public = True
        method_call = KilOperation(self.this, '.', method_call)
        return method_call

    def pop_substream(self):
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.decl_variable = KilVariable('pop_substream', KilTypeNone())
        method_call.arg_variables = list()
        method_call.mutable = True
        method_call.public = True
        method_call = KilOperation(self.this, '.', method_call)
        return method_call

    def root(self):
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.decl_variable = KilVariable('root', KilTypeIO(), reference=True, mutable=True)
        method_call.arg_variables = list()
        method_call.mutable = True
        method_call.public = True
        method_call.hint_treat_as_member_variable = True
        method_call = KilOperation(self.this, '.', method_call)
        return method_call

    def parent(self):
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.decl_variable = KilVariable('parent', KilTypeIO(), reference=True, mutable=True)
        method_call.arg_variables = list()
        method_call.mutable = True
        method_call.public = True
        method_call.hint_treat_as_member_variable = True
        method_call = KilOperation(self.this, '.', method_call)
        return method_call

    def read_integer(self, type_, endian):
        arg_variables = list()
        typecast_required = False
        if type_.bitfield:
            method_name = 'read_bits_int'
            number_of_bits = KilLiteral(type_.bits, KilTypeInteger())
            arg_variables = [number_of_bits]
            typecast_required = True
        else:
            int_sign = 's' if type_.signed else 'u'
            int_size = '{}'.format(type_.bytes_)
            int_endian = endian if type_.bytes_ > 1 else ''
            method_name = 'read_{}{}{}'.format(int_sign, int_size, int_endian)
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.mutable = True
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = method_name
        method_call.decl_variable.type_ = type_
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = arg_variables
        method_call = KilOperation(self.this, '.', method_call)
        if typecast_required:
            method_call = KilTypecast(method_call, type_)
        return method_call

    def write_integer(self, variable, endian):
        type_ = variable.type_
        arg_variables = list()
        if type_.bitfield:
            method_name = 'write_bits_int'
            number_of_bits = KilLiteral(type_.bits, KilTypeInteger())
            arg_variables = [number_of_bits]
            arg_variables.append(KilTypecast(variable, KilTypeInteger(signed=False, bits=64)))
        else:
            int_sign = 's' if type_.signed else 'u'
            int_size = '{}'.format(type_.bytes_)
            int_endian = endian if type_.bytes_ > 1 else ''
            method_name = 'write_{}{}{}'.format(int_sign, int_size, int_endian)
            arg_variables.append(variable)
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.mutable = True
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = method_name
        method_call.decl_variable.type_ = KilTypeNone()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = arg_variables
        method_call = KilOperation(self.this, '.', method_call)
        return method_call

    def read_float(self, type_, endian):
        assert isinstance(type_, KilTypeFloat)
        assert type_.bits in [32, 64]
        arg_variables = list()
        float_size = '{}'.format(type_.bytes_)
        float_endian = endian if type_.bytes_ > 1 else ''
        method_name = 'read_f{}{}'.format(float_size, float_endian)
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.mutable = True
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = method_name
        method_call.decl_variable.type_ = type_
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = arg_variables
        method_call = KilOperation(self.this, '.', method_call)
        return method_call

    def write_float(self, variable, endian):
        type_ = variable.type_
        assert isinstance(type_, KilTypeFloat)
        assert type_.bits in [32, 64]
        arg_variables = list()
        float_size = '{}'.format(type_.bytes_)
        float_endian = endian if type_.bytes_ > 1 else ''
        method_name = 'write_f{}{}'.format(float_size, float_endian)
        arg_variables.append(variable)
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.mutable = True
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = method_name
        method_call.decl_variable.type_ = KilTypeNone()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = arg_variables
        method_call = KilOperation(self.this, '.', method_call)
        return method_call

    def read_string(self, type_, size_variable=None, term_variable=None):
        if type_.wide:
            method_name = 'read_wchar_str'
        else:
            method_name = 'read_char_str'
        consume_variable = KilLiteral(True, KilTypeBoolean())
        encoding_variable = type_.compile_enum_variable()
        if size_variable is None:
            size_variable = 0
            use_size_variable = KilLiteral(False, KilTypeBoolean())
        else:
            use_size_variable = KilLiteral(True, KilTypeBoolean())
        if isinstance(size_variable, int):
            size_variable = KilLiteral(size_variable, KilTypeSize())
        if term_variable is None:
            term_variable = 0
            include_term_variable = KilLiteral(False, KilTypeBoolean())
        else:
            include_term_variable = KilLiteral(True, KilTypeBoolean())
        if isinstance(term_variable, int):
            term_variable = KilLiteral(term_variable, type_.char_type)
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.mutable = True
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = method_name
        method_call.decl_variable.type_ = type_
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = [
            size_variable,
            term_variable,
            use_size_variable,
            include_term_variable,
            consume_variable,
            encoding_variable
        ]
        method_call = KilOperation(self.this, '.', method_call)
        return method_call

    def write_string(self, variable, size_variable=None, term_variable=None):
        type_ = variable.type_
        if type_.wide:
            method_name = 'write_wchar_str'
        else:
            method_name = 'write_char_str'
        encoding_variable = type_.compile_enum_variable()
        if size_variable is None:
            size_variable = 0
            use_size_variable = KilLiteral(False, KilTypeBoolean())
        else:
            use_size_variable = KilLiteral(True, KilTypeBoolean())
        if isinstance(size_variable, int):
            size_variable = KilLiteral(size_variable, KilTypeSize())
        if term_variable is None:
            term_variable = 0
            include_term_variable = KilLiteral(False, KilTypeBoolean())
        else:
            include_term_variable = KilLiteral(True, KilTypeBoolean())
        if isinstance(term_variable, int):
            term_variable = KilLiteral(term_variable, type_.char_type)
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.mutable = True
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = method_name
        method_call.decl_variable.type_ = KilTypeNone()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = [
            variable,
            size_variable,
            term_variable,
            use_size_variable,
            include_term_variable,
            encoding_variable
        ]
        method_call = KilOperation(self.this, '.', method_call)
        return method_call

    def read_bytes(self, read_size_variable):
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.mutable = True
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'read_bytes'
        method_call.decl_variable.type_ = KilTypeBuffer()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = [
            read_size_variable
        ]
        method_call = KilOperation(self.this, '.', method_call)
        return method_call

    def write_bytes(self, variable, write_size_variable):
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.mutable = True
        method_call.public = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'write_bytes'
        method_call.decl_variable.type_ = KilTypeNone()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = [
            variable,
            write_size_variable
        ]
        method_call = KilOperation(self.this, '.', method_call)
        return method_call

    def random_value_from_range(self, inclusive_min_variable, inclusive_max_variable):
        type_ = inclusive_min_variable.type_
        assert type(type_) == type(inclusive_max_variable.type_)
        assert not inclusive_min_variable.list_
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.mutable = True
        method_call.public = True
        method_call.class_method = True
        method_call.decl_variable = KilVariable()
        if isinstance(type_, KilTypeSize):
            method_call.decl_variable.name = 'random_size'
        elif isinstance(type_, KilTypeInteger):
            method_call.decl_variable.name = 'random_{}'.format(type_.ksy_integer_type)
        elif isinstance(type_, KilTypeFloat):
            method_call.decl_variable.name = 'random_{}'.format(type_.ksy_type)
        elif isinstance(type_, KilTypeBoolean):
            method_call.decl_variable.name = 'random_bool'
        else:
            raise NotImplementedError(type_)
        method_call.decl_variable.type_ = type_
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = [
            inclusive_min_variable,
            inclusive_max_variable
        ]
        return method_call

    def random_bytes_from_range(self, variable, inclusive_min_size, inclusive_max_size, inclusive_min_variable, inclusive_max_variable):
        size_type_ = inclusive_min_size.type_
        assert isinstance(size_type_, KilTypeSize)
        assert type(size_type_) == type(inclusive_max_size.type_)
        assert not inclusive_min_size.list_
        type_ = inclusive_min_variable.type_
        assert isinstance(type_, KilTypeInteger)
        assert type(type_) == type(inclusive_max_variable.type_)
        assert not inclusive_min_variable.list_
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.mutable = True
        method_call.public = True
        method_call.class_method = True
        method_call.decl_variable = KilVariable()
        method_call.decl_variable.name = 'random_bytes'
        method_call.decl_variable.type_ = KilTypeBoolean()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = [
            variable,
            inclusive_min_size,
            inclusive_max_size,
            inclusive_min_variable,
            inclusive_max_variable
        ]
        return method_call

    def random_string_from_range(self, variable, inclusive_min_size, inclusive_max_size, inclusive_min_variable, inclusive_max_variable):
        string_type = variable.type_
        assert isinstance(string_type, KilTypeString)
        size_type_ = inclusive_min_size.type_
        assert isinstance(size_type_, KilTypeSize)
        assert type(size_type_) == type(inclusive_max_size.type_)
        assert not inclusive_min_size.list_
        type_ = inclusive_min_variable.type_
        assert isinstance(type_, KilTypeInteger)
        assert type(type_) == type(inclusive_max_variable.type_)
        assert not inclusive_min_variable.list_
        method_call = KilMethodCall()
        method_call.class_type = KilTypeIO()
        method_call.mutable = True
        method_call.public = True
        method_call.class_method = True
        method_call.decl_variable = KilVariable()
        if string_type.wide:
            method_call.decl_variable.name = 'random_wchar_str'
        else:
            method_call.decl_variable.name = 'random_char_str'
        method_call.decl_variable.type_ = KilTypeBoolean()
        method_call.decl_variable.mutable = False
        method_call.decl_variable.reference = False
        method_call.decl_variable.list_ = False
        method_call.arg_variables = [
            variable,
            inclusive_min_size,
            inclusive_max_size,
            inclusive_min_variable,
            inclusive_max_variable,
            string_type.compile_enum_variable(),
        ]
        return method_call
