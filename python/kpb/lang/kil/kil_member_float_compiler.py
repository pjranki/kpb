import os
import sys

from .kil_types import *
from .kil_ksy_library import *
from .kil_io_interface import *
from .kil_member_float_interface import *
from .kil_member_generic_compiler import *


class KilMemberFloatCompiler(KilMemberGenericCompiler):

    def compile_methods(self):
        list_ = KilMemberGenericInterface(self.root_name, self.class_name, self.member_name, None, self.ksy).list_
        methods = list()
        if not list_:
            methods.append(self.compile_getter_method())
            methods.append(self.compile_setter_method())
        else:
            methods.append(self.compile_getlist_method())
            methods.append(self.compile_count_method())
            methods.append(self.compile_atindex_method())
            methods.append(self.compile_add_method())
        methods.append(self.compile_clear_method())
        methods.append(self.compile_has_method())
        methods.append(self.compile_randomize_method())
        return methods

    def compile_atindex_method(self):
        method = super(KilMemberFloatCompiler, self).compile_atindex_method()
        method.decl_variable.variable.reference = False
        return method

    def compile_read_call(self):
        endian = self.ksy.find_ksy_endian(self.root_name, self.class_name, self.member_name)
        type_ = KilMemberFloatInterface(self.root_name, self.class_name, self.member_name, None, self.ksy).type_
        _io = KilIoInterface(reference=True)
        return _io.read_float(type_, endian)

    def compile_write_call(self, value_variable):
        endian = self.ksy.find_ksy_endian(self.root_name, self.class_name, self.member_name)
        _io = KilIoInterface(reference=True)
        return _io.write_float(value_variable, endian)
