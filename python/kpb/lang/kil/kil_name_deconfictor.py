import os
import sys

REPLACE_PREFIX_NAMES = [
    'set_',
    'clear_',
    'has_',
    'mutable_',
    'add_',
]

REPLACE_SUFFIX_NAMES = [
    '_at',
    '_size',
    '_size_at',
    '_t',
    '_set',
]

REPLACE_NAMES = [
    'init',
    'fini',
    'update',
    'set',
    'clear',
    'copy_from',
    'has',
    'default',
    'read',
    'write',
    'mutable',
    'size',
    'length',
    'add',
    'at',
    'size_at',
    'serialize_to_string',
    'parse_from_string',
    'parse_from_string_or_fill',
    'randomize',
    'this',
    'self',
    'root',
    'parent',
    'parent_type',
    'void',
    'io',
    'true',
    'false',
    'len',
    'nullptr',
    'none',
    'to_i',
    'float',
    'double',
    'getter',
    'setter',
]

class KilNameDeconflictor(object):

    def name(self, name):
        if name.startswith('_'):
            return name
        if name in ['True', 'False']:
            name = name.lower()
        for prefix in REPLACE_PREFIX_NAMES:
            if name.startswith(prefix):
                name = '_' + name
        for suffix in REPLACE_SUFFIX_NAMES:
            if name.endswith(suffix):
                name = name + '_'
        if name in REPLACE_NAMES:
            name = name + '_'
        return name

    def import_name(self, name):
        return name

    def namespace_name(self, name):
        return name

    def class_name(self, name):
        if name.startswith('bad_class_'):
            name = name.replace('bad_class_', 'fc_')
        name = self.name(name)
        return name

    def member_name(self, name):
        if name.startswith('bad_member_'):
            name = name.replace('bad_member_', 'fm_')
        name = self.name(name)
        return name

    def enum_name(self, name):
        if name.startswith('bad_enum_'):
            name = name.replace('bad_enum_', 'fe_')
        name = self.name(name)
        return name

    def enum_value_name(self, name):
        if name.startswith('bad_value_enum_'):
            name = name.replace('bad_value_enum_', 'fve_')
        name = self.name(name)
        return name
