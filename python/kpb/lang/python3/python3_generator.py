import os
import sys

from .. text_helper import TextHelper
from . python3_compiler import *


class Python3SourceGenerator(object):

    def generate(self, obj, args=None):
        generate_methods = {
            list: self.generate_list,
            Python3File: self.generate_file,
            Python3Import: self.generate_import,
            Python3Class: self.generate_class,
            Python3Function: self.generate_function,
            Python3FunctionCall: self.generate_function_call,
            Python3Method: self.generate_method,
            Python3MethodCall: self.generate_method_call,
            Python3ConstructorCall: self.generate_constructor_call,
            Python3Variable: self.generate_variable,
            Python3Literal: self.generate_literal,
            Python3Block: self.generate_block,
            Python3TryCatchBlock: self.generate_try_catch_block,
            Python3Return: self.generate_return,
            Python3If: self.generate_if,
            Python3While: self.generate_while,
            Python3Break: self.generate_break,
            Python3Operation: self.generate_operation,
        }
        generate_method = generate_methods.get(obj.__class__, None)
        if not generate_method:
            raise NotImplementedError(repr(obj))
        return generate_method(obj)

    def generate_list(self, obj_list):
        text = TextHelper()
        first = True
        for obj in obj_list:
            text_block = self.generate(obj)
            if len(text_block.strip()) > 0:
                if not first:
                    text.nl()
                text.block(text_block)
            first = False
        return str(text)

    def generate_file(self, obj):
        text = TextHelper()
        text_block = self.generate(obj.children)
        if len(text_block.strip()) > 0:
            text.block(text_block)
        return str(text)

    def generate_import(self, obj):
        if obj.import_names and len(obj.import_names) > 0:
            return 'from {}{} import {}'.format('' if obj.external else '.', obj.name, ', '.join(obj.import_names))
        return 'import {}'.format(obj.name)

    def generate_function(self, obj):
        text = TextHelper()
        decl_text = self.generate(obj.decl_variable)
        arg_variables_text = ', '.join([self.generate(arg) for arg in obj.arg_variables])
        text.format('def {}({}):', decl_text, arg_variables_text)
        text.block(self.generate(obj.code_block))
        return str(text)

    def generate_function_call(self, obj):
        arg_variables_text = ', '.join([self.generate(arg) for arg in obj.arg_variables])
        return '{}({})'.format(obj.decl_variable.name, arg_variables_text)

    def generate_class(self, obj):
        text = TextHelper()
        text.format('class {}(object):', obj.name)
        text.tab()
        text.block(self.generate(obj.declarations))
        text.block(self.generate(obj.methods))
        text.shift_tab()
        return str(text)

    def generate_method(self, obj):
        text = TextHelper()
        decl_text = self.generate(obj.decl_variable)
        arg_variables_text = ', '.join([self.generate(arg) for arg in obj.arg_variables])
        if obj.property:
            text.line('@property')
        if obj.class_method:
            text.line('@classmethod')
            if len(obj.arg_variables) == 0:
                arg_variables_text = 'cls'
            else:
                arg_variables_text = 'cls, ' + arg_variables_text
        text.format('def {}({}):', decl_text, arg_variables_text)
        text.block(self.generate(obj.code_block))
        return str(text)

    def generate_method_call(self, obj):
        arg_variables_text = ', '.join([self.generate(arg) for arg in obj.arg_variables])
        if obj.property:
            return obj.decl_variable.name
        method_name = obj.decl_variable.name
        if obj.class_method:
            if isinstance(obj.class_type, Python3TypeIO):
                method_name = 'KpbIO.{}'.format(method_name)
            else:
                raise NotImplementedError(obj.class_type)
        return '{}({})'.format(method_name, arg_variables_text)

    def generate_constructor_call(self, obj):
        arg_variables_text = ', '.join([self.generate(arg) for arg in obj.arg_variables])
        return '{}({})'.format(obj.class_type.name, arg_variables_text)

    def generate_variable(self, obj):
        return obj.name

    def generate_literal(self, obj):
        return obj.text

    def generate_block(self, obj):
        text = TextHelper()
        text.tab()
        block_text = self.generate(obj.children)
        if block_text.strip() == '':
            block_text = 'pass'
        text.block(block_text)
        text.shift_tab()
        return str(text)

    def generate_try_catch_block(self, obj):
        text = TextHelper()
        text.line('try:')
        assert isinstance(obj.try_body, Python3Block)
        block_text = self.generate(obj.try_body)
        text.block(block_text)
        if obj.catch_body:
            text.line('except:')
            assert isinstance(obj.catch_body, Python3Block)
            block_text = self.generate(obj.catch_body)
            text.block(block_text)
        if obj.final_body:
            text.line('finally:')
            assert isinstance(obj.final_body, Python3Block)
            block_text = self.generate(obj.final_body)
            text.block(block_text)
        return str(text)

    def generate_return(self, obj):
        if not obj.return_:
            return 'return'
        return 'return {}'.format(self.generate(obj.return_))

    def generate_if(self, obj):
        text = TextHelper()
        if not obj.true_body:
            text.line('if not ({}):'.format(self.generate(obj.expr)))
        else:
            text.line('if {}:'.format(self.generate(obj.expr)))
        if obj.true_body:
            if not isinstance(obj.true_body, Python3Block):
                text.tab()
            text.block(self.generate(obj.true_body))
            if not isinstance(obj.true_body, Python3Block):
                text.shift_tab()
        if obj.true_body and obj.false_body:
            text.line('else:')
        if obj.false_body:
            if not isinstance(obj.false_body, Python3Block):
                text.tab()
            text.block(self.generate(obj.false_body))
            if not isinstance(obj.false_body, Python3Block):
                text.shift_tab()
        return str(text)

    def generate_while(self, obj):
        text = TextHelper()
        text.line('while {}:'.format(self.generate(obj.condition)))
        if not isinstance(obj.body, Python3Block):
            text.tab()
        text.block(self.generate(obj.body))
        if not isinstance(obj.body, Python3Block):
            text.shift_tab()
        return str(text)

    def generate_break(self, obj):
        return 'break'

    def generate_operation(self, obj):
        if obj.op == '.':
            return '{}.{}'.format(self.generate(obj.a), self.generate(obj.b))
        elif obj.op == '=':
            return '{} = {}'.format(self.generate(obj.a), self.generate(obj.b))
        elif obj.op in ['==', '<', '>', '<=', '>=', '!=', '+', '-', '*', '/', '%', '<<', '>>', '&', '|', '^', 'and', 'or']:
            return '({}) {} ({})'.format(self.generate(obj.a), obj.op, self.generate(obj.b))
        elif obj.op in ['++', '--']:
            a_text = self.generate(obj.a)
            op_text = obj.op[0]
            return '{} = {} {} 1'.format(a_text, a_text, op_text)
        elif obj.op == 'not':
            return 'not ({})'.format(self.generate(obj.a))
        elif obj.op == '~':
            return '~({})'.format(self.generate(obj.a))
        elif obj.op == '[]':
            return '{}[{}]'.format(self.generate(obj.a), self.generate(obj.b))
        elif obj.op == '?':
            return '({}) if ({}) else ({})'.format(self.generate(obj.b), self.generate(obj.a), self.generate(obj.c))
        raise NotImplementedError("operation '{}'".format(obj.op))
