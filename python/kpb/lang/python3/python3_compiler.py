import os
import sys

from .. text_helper import to_camel_case
from .. kil.kil_compiler import *

CAMEL_CASE_METHODS = [
    'clear',
    'copy_from',
    'parse_from_string',
    'parse_from_string_or_fill',
    'serialize_to_string',
    'read',
    'write',
    'update',
    'size',
    'randomize',
]


class Python3File(object):

    def __init__(self):
        self.name = None
        self.guard_name = None
        self.input_file_path = None
        self.children = list()


class Python3Import(object):

    def __init__(self):
        self.name = None
        self.external = True
        self.import_names = list()


class Python3Function(object):

    def __init__(self):
        self.decl_variable = None
        self.arg_variables = list()
        self.code_block = None

    @property
    def type_(self):
        return self.decl_variable.type_


class Python3FunctionCall(object):

    def __init__(self):
        self.decl_variable = None
        self.arg_variables = list()


class Python3Class(object):

    def __init__(self):
        self.name = None
        self.declarations = list()
        self.classes = list()
        self.methods = list()
        self.members = list()

    @property
    def type_(self):
        return Python3TypeClass(name=self.name)


class Python3Method(object):

    def __init__(self):
        self.class_type = None
        self.class_method = False
        self.decl_variable = None
        self.arg_variables = list()
        self.property = False
        self.code_block = None

    @property
    def type_(self):
        return self.decl_variable.type_


class Python3MethodCall(object):

    def __init__(self):
        self.class_type = None
        self.class_method = False
        self.decl_variable = None
        self.arg_variables = list()
        self.property = False

    @property
    def type_(self):
        return self.decl_variable.type_


class Python3ConstructorCall(object):

    def __init__(self):
        self.class_type = None
        self.arg_variables = list()


class Python3TypeNone(object):
    pass


class Python3TypeBoolean(object):
    pass


class Python3TypeBytes(object):
    pass


class Python3TypeSize(object):
    pass


class Python3TypeInteger(object):
    pass


class Python3TypeFloat(object):
    pass


class Python3TypeString(object):

    def __init__(self, encoding=None):
        self.encoding = encoding if encoding else 'ascii'


class Python3TypeIO(object):

    @property
    def name(self):
        return 'KpbIO'


class Python3TypeClass(object):

    def __init__(self, name=None):
        self.name = name


class Python3Variable(object):

    def __init__(self, name=None, type_=None):
        self.name = name
        self.type_ = type_ if type_ else Python3TypeNone()


class Python3Literal(object):

    def __init__(self, text):
        self.text = text


class Python3Block(object):

    def __init__(self, children=None):
        self.children = children if children else list()


class Python3TryCatchBlock(object):

    def __init__(self, try_body=None, catch_body=None, final_body=None):
        self.try_body = try_body
        self.catch_body = catch_body
        self.final_body = final_body


class Python3Return(object):

    def __init__(self, return_=None):
        self.return_ = return_

    @property
    def type_(self):
        return self.return_.type_


class Python3If(object):

    def __init__(self, expr=None, true_body=None, false_body=None):
        self.expr = expr
        self.true_body = true_body
        self.false_body = false_body


class Python3While(object):

    def __init__(self, condition=None, body=None):
        self.condition = condition
        self.body = body


class Python3Break(object):

    def __init__(self):
        pass


class Python3Operation(object):

    def __init__(self, a=None, op=None, b=None, c=None):
        self.a = a
        self.op = op
        self.b = b
        self.c = c

    @property
    def type_(self):
        if self.op == '.':
            return self.b.type_
        return self.a.type_


class Python3Compiler(object):

    def compile(self, kil_obj, args=None):
        compile_methods = {
            list: self.compile_list,
            KilFile: self.compile_file,
            KilImport: self.compile_import,
            KilNamespace: self.compile_namespace,
            KilClass: self.compile_class,
            KilEnum: self.compile_enum,
            KilMethod: self.compile_method,
            KilMethodCall: self.compile_method_call,
            KilConstructor: self.compile_constructor,
            KilConstructorCall: self.compile_constructor_call,
            KilTypeNone: self.compile_type_none,
            KilTypeBoolean: self.compile_type_boolean,
            KilTypeInteger: self.compile_type_integer,
            KilTypeFloat: self.compile_type_float,
            KilTypeString: self.compile_type_string,
            KilTypeChar: self.compile_type_char,
            KilTypeBuffer: self.compile_type_buffer,
            KilTypeSize: self.compile_type_size,
            KilTypeIO: self.compile_type_io,
            KilTypeClass: self.compile_type_class,
            KilVariable: self.compile_variable,
            KilDeclaration: self.compile_declaration,
            KilEnumValue: self.compile_enum_value,
            KilLiteral: self.compile_literal,
            KilMember: self.compile_member,
            KilBlock: self.compile_block,
            KilTryCatchBlock: self.compile_try_catch_block,
            KilReturn: self.compile_return,
            KilIf: self.compile_if,
            KilFor: self.compile_for,
            KilWhile: self.compile_while,
            KilDoWhile: self.compile_dowhile,
            KilBreak: self.compile_break,
            KilOperation: self.compile_operation,
            KilTypecast: self.compile_typecast,
        }
        compile_method = compile_methods.get(kil_obj.__class__, None)
        if not compile_method:
            raise NotImplementedError(repr(kil_obj))
        return compile_method(kil_obj)

    def compile_list(self, kil_obj_list):
        obj_list = list()
        for kil_obj in kil_obj_list:
            obj = self.compile(kil_obj)
            if not obj:
                continue
            if isinstance(obj, list):
                obj_list.extend(obj)
            else:
                obj_list.append(obj)
        return obj_list

    def compile_file(self, kil_file):
        self.import_class_to_constructor_map = dict() # reset
        obj = Python3File()
        obj.input_file_name = kil_file.input_file_name
        obj.name = kil_file.name
        import_ = Python3Import()
        import_.external = True
        import_.name = 'os'
        obj.children.append(import_)
        import_ = Python3Import()
        import_.external = True
        import_.name = 'sys'
        obj.children.append(import_)
        import_ = Python3Import()
        import_.external = True
        import_.name = 'kpb_io'
        import_.import_names = ['*']
        obj.children.append(import_)
        obj.children.extend(self.compile(kil_file.children))
        return obj

    def compile_import(self, kil_import):
        if kil_import.external:
            import_ = Python3Import()
            import_.name = kil_import.name
            import_.external = True
            import_.import_names = ['*']
            return import_
        if not kil_import.public:
            imports = list()
            for kil_class in [c for c in kil_import.imports if isinstance(c, KilTypeClass)]:
                class_type = self.compile(kil_class)
                lazy_import_decl = Python3Variable()
                lazy_import_decl.name = '_{}'.format(class_type.name)
                lazy_import_decl.type_ = class_type
                lazy_import_function = Python3Function()
                lazy_import_function.decl_variable = lazy_import_decl
                lazy_import_function.arg_variables = []
                lazy_import_function.code_block = Python3Block()
                import_class = Python3Import()
                import_class.name = kil_import.name
                import_class.external = False
                import_class.import_names = [class_type.name]
                constructor_call = Python3ConstructorCall()
                constructor_call.class_type = class_type
                constructor_call.arg_variables = list()
                lazy_import_function.code_block.children.extend([
                    import_class,
                    Python3Return(constructor_call)
                ])
                constructor_call = Python3FunctionCall()
                constructor_call.decl_variable = lazy_import_decl
                constructor_call.arg_variables = []
                imports.append(lazy_import_function)
                self.import_class_to_constructor_map[class_type.name] = constructor_call
            return imports
        import_ = Python3Import()
        import_.name = kil_import.name
        import_.external = False
        import_.import_names = ['*']
        return import_

    def compile_namespace(self, kil_namespace):
        return self.compile(kil_namespace.children)

    def compile_class(self, kil_class):
        obj = Python3Class()
        obj.name = to_camel_case(kil_class.name)
        obj.declarations.extend(self.compile(kil_class.enums))
        constructor = self.compile(kil_class.constructor)
        obj.methods.append(constructor)
        obj.methods.append(self.compile_setattr(obj))
        obj.members.extend(self.compile(kil_class.members))
        obj.methods.extend(self.compile(kil_class.methods))
        # references must be set to None in constructor (otherwise we can't check them before assignment)
        this_variable = Python3Variable()
        this_variable.name = 'self'
        this_variable.type_ = Python3TypeClass(name=obj.name)
        for kil_member in kil_class.members:
            if kil_member.variable.reference or isinstance(kil_member.variable.type_, KilTypeClass):
                member_variable = self.compile(kil_member)
                member_variable = Python3Operation(this_variable, '.', member_variable)
                set_to_none = Python3Operation(member_variable, '=', Python3Literal('None'))
                constructor.code_block.children.insert(0, set_to_none)
        return obj

    def compile_enum(self, kil_enum):
        obj = Python3Class()
        obj.name = to_camel_case(kil_enum.name)
        type_ = self.compile(kil_enum.type_)
        for kil_enum_value in kil_enum.enums:
            enum_value = Python3Variable()
            enum_value.name = '{}_{}'.format(kil_enum.name, kil_enum_value.name).upper()
            enum_value.type_ = type_
            enum_value = Python3Operation(enum_value, '=', Python3Literal(kil_enum_value.value))
            obj.declarations.append(enum_value)
        return obj

    def compile_setattr(self, class_):
        decl_setattr = Python3Variable()
        decl_setattr.name = '__setattr__'
        decl_setattr.type_ = Python3TypeNone()
        arg_self_variable = Python3Variable()
        arg_self_variable.name = 'self'
        arg_self_variable.type_ = class_.type_
        arg_name_variable = Python3Variable()
        arg_name_variable.name = 'name'
        arg_name_variable.type_ = Python3TypeString()
        arg_value_variable = Python3Variable()
        arg_value_variable.name = 'value'
        arg_value_variable.type_ = Python3TypeNone()
        setattr_method = Python3Method()
        setattr_method.class_type = class_.type_
        setattr_method.decl_variable = decl_setattr
        setattr_method.arg_variables = [arg_self_variable, arg_name_variable, arg_value_variable]
        setattr_method.code_block = Python3Block()
        set_method_variable = Python3Variable()
        set_method_variable.name = 'method'
        set_method_variable.type_ = Python3TypeNone()
        method_name = Python3Operation(Python3Literal("'set_'"), '+', arg_name_variable)
        decl_getattr = Python3Variable()
        decl_getattr.name = 'getattr'
        decl_getattr.type_ = Python3TypeNone()
        call_getattr = Python3FunctionCall()
        call_getattr.decl_variable = decl_getattr
        call_getattr.arg_variables = [arg_self_variable, method_name, Python3Literal('None')]
        call_set_method = Python3FunctionCall()
        call_set_method.decl_variable = set_method_variable
        call_set_method.arg_variables = [arg_value_variable]
        get_class = Python3Operation(arg_self_variable, '.', Python3Variable('__class__'))
        get_super = Python3FunctionCall()
        get_super.decl_variable = Python3Variable()
        get_super.decl_variable.name = 'super'
        get_super.decl_variable.type_ = Python3TypeNone()
        get_super.arg_variables = [get_class, arg_self_variable]
        setattr_super = Python3MethodCall()
        setattr_super.class_type = None
        setattr_super.decl_variable = setattr_method.decl_variable
        setattr_super.arg_variables = setattr_method.arg_variables[1:]
        call_super_setattr = Python3Operation(get_super, '.', setattr_super)
        setattr_method.code_block.children.extend([
            Python3Operation(set_method_variable, '=', call_getattr),
            Python3If(expr=set_method_variable, true_body=Python3Block([
                Python3Return(call_set_method)
            ])),
            Python3Return(call_super_setattr)
        ])
        return setattr_method

    def compile_method(self, kil_method):
        method = Python3Method()
        method.class_type = self.compile(kil_method.class_type)
        method.class_method = kil_method.class_method
        method.decl_variable = self.compile(kil_method.decl_variable)
        if method.decl_variable.name in CAMEL_CASE_METHODS and not isinstance(kil_method.class_type, KilTypeIO):
            method.decl_variable.name = to_camel_case(method.decl_variable.name)
        arg_self_variable = Python3Variable()
        arg_self_variable.name = 'self'
        arg_self_variable.type_ = method.class_type
        method.arg_variables.append(arg_self_variable)
        method.arg_variables.extend(self.compile(kil_method.arg_variables))
        method.code_block = self.compile(kil_method.code_block)
        method.property = kil_method.hint_treat_as_member_variable
        return method

    def compile_method_call(self, kil_method_call):
        if kil_method_call.decl_variable.name == 'write_wchar_str':
            kil_method_call.decl_variable.name = 'write_char_str'
        if kil_method_call.decl_variable.name == 'read_wchar_str':
            kil_method_call.decl_variable.name = 'read_char_str'
        method_call = Python3MethodCall()
        method_call.class_type = self.compile(kil_method_call.class_type)
        method_call.class_method = kil_method_call.class_method
        method_call.decl_variable = self.compile(kil_method_call.decl_variable)
        if method_call.decl_variable.name in CAMEL_CASE_METHODS and not isinstance(kil_method_call.class_type, KilTypeIO):
            method_call.decl_variable.name = to_camel_case(method_call.decl_variable.name)
        method_call.arg_variables = self.compile(kil_method_call.arg_variables)
        method_call.property = kil_method_call.hint_treat_as_member_variable
        if method_call.class_method and isinstance(kil_method_call.class_type, KilTypeIO):
            method_name = method_call.decl_variable.name
            if method_name in ['random_bytes', 'random_char_str', 'random_wchar_str']:
                assert len(method_call.arg_variables) > 0
                arg_this = method_call.arg_variables[0]
                method_call.arg_variables = method_call.arg_variables[1:]
                assert isinstance(arg_this, Python3Operation)
                if arg_this.op == '.':
                    member_name_variable = Python3Literal("'{}'".format(arg_this.b.name))
                    self_variable = arg_this.a
                    set_value = Python3FunctionCall()
                    set_value.decl_variable = Python3Variable()
                    set_value.decl_variable.name = 'setattr'
                    set_value.decl_variable.type_ = Python3TypeNone()
                    set_value.arg_variables = [self_variable, member_name_variable, method_call]
                elif arg_this.op == '[]':
                    arg_index = arg_this.b
                    arg_this = arg_this.a
                    set_value = Python3MethodCall()
                    set_value.decl_variable = Python3Variable()
                    set_value.decl_variable.name = '__setitem__'
                    set_value.decl_variable.type_ = Python3TypeNone()
                    set_value.arg_variables = [arg_index, method_call]
                    set_value = Python3Operation(arg_this, '.', set_value)
                else:
                    raise NotImplementedError(arg_this.op)
                method_call = Python3Operation(a=set_value, op='?', b=Python3Literal('True'), c=Python3Literal('True'))
        if kil_method_call.decl_variable.name == 'getter':
            if isinstance(kil_method_call.arg_variables[0], KilOperation):
                if isinstance(kil_method_call.arg_variables[0].b, KilVariable):
                    if isinstance(kil_method_call.arg_variables[0].b.type_, KilTypeClass):
                        method_call.arg_variables[0] = self.compile_lazy_member_constructor_call(kil_method_call.arg_variables[0])
        return method_call

    def compile_constructor(self, kil_constructor):
        method = KilMethod()
        method.class_type = kil_constructor.class_type
        method.decl_variable = KilVariable()
        method.decl_variable.name = '__init__'
        method.decl_variable.type_ = KilTypeNone()
        method.decl_variable.mutable = False
        method.arg_variables = kil_constructor.arg_variables
        method.mutable = True
        method.public = kil_constructor.public
        method.code_block = kil_constructor.code_block
        return self.compile_method(method)

    def compile_constructor_call(self, kil_constructor_call):
        constructor_call = Python3ConstructorCall()
        constructor_call.class_type = self.compile(kil_constructor_call.class_type)
        constructor_call.arg_variables = self.compile(kil_constructor_call.arg_variables)
        return constructor_call

    def compile_lazy_member_constructor_call(self, kil_member, set_member=False):
        member = self.compile(kil_member)
        constructor_call = Python3ConstructorCall()
        constructor_call.class_type = member.type_
        constructor_call.arg_variables = list()
        constructor_call = self.import_class_to_constructor_map.get(member.type_.name, constructor_call)
        lazy_constructor = Python3Operation(member, '?', member, constructor_call)
        if set_member:
            lazy_constructor = Python3Operation(member, '=', lazy_constructor)
        return lazy_constructor

    def compile_type_none(self, kil_type_none):
        return Python3TypeNone()

    def compile_type_boolean(self, kil_type_boolean):
        return Python3TypeBoolean()

    def compile_type_integer(self, kil_type_integer):
        return Python3TypeInteger()

    def compile_type_float(self, kil_type_integer):
        return Python3TypeFloat()

    def compile_type_string(self, kil_type_string):
        if kil_type_string.encoding == KilTypeString.ASCII:
            return Python3TypeString('ascii')
        elif kil_type_string.encoding == KilTypeString.UTF16LE:
            return Python3TypeString('utf-16le')
        elif kil_type_string.encoding == KilTypeString.UTF16BE:
            return Python3TypeString('utf-16be')
        elif kil_type_string.encoding == KilTypeString.UTF8:
            return Python3TypeString('utf-8')
        else:
            raise NotImplementedError('encoding type {}'.format(kil_type_string.encoding))

    def compile_type_char(self, kil_type_char):
        # treat it the same as a string
        return self.compile_type_string(kil_type_char)

    def compile_type_buffer(self, kil_type_buffer):
        return Python3TypeBytes()

    def compile_type_size(self, kil_type_integer):
        return Python3TypeSize()

    def compile_type_io(self, kil_type_io):
        return Python3TypeIO()

    def compile_type_class(self, kil_type):
        return Python3TypeClass(name=to_camel_case(kil_type.name))

    def compile_variable(self, kil_variable):
        variable = Python3Variable()
        variable.name = kil_variable.name
        if kil_variable.name == KilVariable.THIS_NAME:
            variable.name = 'self'
        variable.type_ = self.compile(kil_variable.type_)
        return variable

    def compile_declaration(self, kil_declaration):
        return self.compile(kil_declaration.variable)

    def compile_enum_value(self, kil_enum_value):
        class_name = self.compile(kil_enum_value.enum.class_type).name
        enum_name = to_camel_case(kil_enum_value.enum.name)
        enum_value_name = '{}_{}'.format(kil_enum_value.enum.name, kil_enum_value.name).upper()
        return Python3Literal('{}.{}.{}'.format(class_name, enum_name, enum_value_name))

    def compile_literal(self, literal):
        if isinstance(literal.type_, KilTypeNone):
            if literal.value == '[]':
                return Python3Literal('[]')
            return Python3Literal('None')
        elif isinstance(literal.type_, KilTypeBoolean):
            return Python3Literal('True' if literal.value else 'False')
        elif isinstance(literal.type_, KilTypeInteger):
            return Python3Literal('{}'.format(literal.value))
        elif isinstance(literal.type_, KilTypeFloat):
            return Python3Literal('{}'.format(literal.value))
        elif isinstance(literal.type_, KilTypeString):
            return Python3Literal("'{}'".format(literal.value))
        elif isinstance(literal.type_, KilTypeChar):
            if literal.type_.wide:
                return Python3Literal("chr(0x{:04})".format(literal.value))
            return Python3Literal("chr(0x{:02})".format(literal.value))
        elif isinstance(literal.type_, KilTypeBuffer):
            if literal.value is None:
                return Python3Literal('None')
            return Python3Literal("b'{}'".format(''.join(['\\x{:02x}'.format(v) for v in literal.value])))
        elif isinstance(literal.type_, KilTypeSize):
            return Python3Literal('{}'.format(literal.value))
        elif isinstance(literal.type_, KilTypeIO):
            raise NotImplementedError('literal type {}'.format(repr(literal.type_)))
        elif isinstance(literal.type_, KilTypeClass):
            if literal.value is None:
                return Python3Literal('None')
            constructor_call = Python3ConstructorCall()
            constructor_call.class_type = self.compile(literal.type_)
            constructor_call.arg_variables = list()
            return constructor_call
        else:
            raise NotImplementedError('literal type {}'.format(repr(literal.type_)))

    def compile_member(self, kil_member):
        obj = self.compile(kil_member.variable)
        obj.name += '_'
        return obj

    def compile_block(self, kil_block):
        block = Python3Block()
        block.children = self.compile(kil_block.children)
        return block

    def compile_try_catch_block(self, kil_try_catch_block):
        try_catch_block = Python3TryCatchBlock()
        try_catch_block.try_body = self.compile(kil_try_catch_block.try_body)
        if len(try_catch_block.try_body.children) == 0:
            try_catch_block.try_body.children.append(Python3Literal('pass'))
        if kil_try_catch_block.catch_body:
            try_catch_block.catch_body = self.compile(kil_try_catch_block.catch_body)
        else:
            try_catch_block.catch_body = Python3Block()
        if kil_try_catch_block.rethrow:
            try_catch_block.catch_body.children.append(Python3Literal('raise'))
        if len(try_catch_block.catch_body.children) == 0:
            try_catch_block.catch_body.children.append(Python3Literal('pass'))
        if kil_try_catch_block.final_body:
            try_catch_block.final_body = self.compile(kil_try_catch_block.final_body)
        else:
            try_catch_block.final_body = Python3Block()
        if len(try_catch_block.final_body.children) == 0:
            try_catch_block.final_body.children.append(Python3Literal('pass'))
        return try_catch_block

    def compile_return(self, kil_return):
        return_ = Python3Return()
        if not kil_return.return_:
            return return_
        return_.return_ = self.compile(kil_return.return_)
        returns_immutable_class = isinstance(kil_return.return_.type_, KilTypeClass) and not kil_return.return_.mutable
        returns_member = isinstance(return_.return_, Python3Operation) and return_.return_.op == '.' and isinstance(return_.return_.b, Python3Variable)
        if returns_immutable_class and returns_member:
            if not (kil_return.return_.list_ and kil_return.return_.op != '[]'):
                return [self.compile_lazy_member_constructor_call(kil_return.return_, set_member=True), return_]
        if isinstance(kil_return.return_, KilLiteral):
            if isinstance(kil_return.return_.type_, KilTypeClass):
                constructor_call = Python3ConstructorCall()
                constructor_call.class_type = self.compile(kil_return.return_.type_)
                constructor_call.arg_variables = list()
                return_.return_ = constructor_call
                return return_
        return return_

    def compile_if(self, kil_if):
        if_ = Python3If()
        if_.expr = self.compile(kil_if.expr)
        if kil_if.true_body:
            if_.true_body = self.compile(kil_if.true_body)
        if kil_if.false_body:
            if_.false_body = self.compile(kil_if.false_body)
        return if_

    def compile_for(self, kil_for):
        while_ = Python3While()
        while_.condition = self.compile(kil_for.condition)
        while_.body = self.compile(kil_for.body)
        start = self.compile(kil_for.start)
        update = self.compile(kil_for.update)
        while_.body.children.append(update)
        return [start, while_]

    def compile_while(self, kil_while):
        while_ = Python3While()
        while_.condition = self.compile(kil_while.expr)
        while_.body = self.compile(kil_while.body)
        return while_

    def compile_dowhile(self, kil_dowhile):
        dowhile_ = Python3While()
        dowhile_.condition = Python3Literal('True')
        dowhile_.body = self.compile(kil_dowhile.body)
        assert isinstance(dowhile_.body, Python3Block)
        dowhile_.body.children.append(Python3If(expr=self.compile(kil_dowhile.expr), false_body=Python3Block([
            Python3Break()
        ])))
        return dowhile_

    def compile_break(self, kil_break):
        return Python3Break()

    def compile_operation(self, kil_operation):
        if kil_operation.op == '.' and isinstance(kil_operation.b, KilDestructorCall):
            # don't call destruction in Python 3 (compiler will handle it for us)
            return None
        if kil_operation.op == 'clr':
            if kil_operation.a.list_:
                kil_operation.op = '='
                kil_operation.b = KilLiteral('[]', type_=KilTypeNone())
            else:
                raise NotImplementedError(type(kil_operation.a.type_))
        if kil_operation.op == 'len':
            function_call = Python3FunctionCall()
            function_call.decl_variable = Python3Variable()
            function_call.decl_variable.name = 'len'
            function_call.decl_variable.type_ = Python3TypeSize()
            function_call.arg_variables = [self.compile(kil_operation.a)]
            return function_call
        if kil_operation.op == '=' and isinstance(kil_operation.a.type_, KilTypeClass) and not kil_operation.a.type_.external and isinstance(kil_operation.b, KilConstructorCall):
            return self.compile_lazy_member_constructor_call(kil_operation.a, set_member=True)
        operation = Python3Operation()
        operation.a = self.compile(kil_operation.a)
        operation.op = kil_operation.op
        if kil_operation.b:
            operation.b = self.compile(kil_operation.b)
        if kil_operation.c:
            operation.c = self.compile(kil_operation.c)
        if operation.op == 'add':
            if isinstance(kil_operation.a.type_, KilTypeClass):
                constructor_call = Python3ConstructorCall()
                constructor_call.class_type = operation.a.type_
                constructor_call.arg_variables = list()
                operation.b = constructor_call
            operation.op = '.'
            method_call = Python3MethodCall()
            method_call.class_type = operation.a.type_
            method_call.decl_variable = Python3Variable()
            method_call.decl_variable.name = 'append'
            method_call.decl_variable.type_ = Python3TypeNone()
            method_call.arg_variables = [operation.b]
            operation.b = method_call
            ternary = Python3Operation()
            ternary.a = operation
            ternary.op = '?'
            ternary.b = Python3Literal('True')
            ternary.c = Python3Literal('True')
            operation = ternary
        if operation.op == '.' and isinstance(operation.a, Python3Variable):
            if operation.a.name == 'self' and isinstance(operation.b, Python3Variable):
                # member access, add a '_' suffix to the name
                operation.b.name += '_'
        if kil_operation.op == '/':
            if not isinstance(kil_operation.a.type_, KilTypeFloat) and not isinstance(kil_operation.b.type_, KilTypeFloat):
                # after division, ensure the result is still an integer
                function_call = Python3FunctionCall()
                function_call.decl_variable = Python3Variable()
                function_call.decl_variable.name = 'int'
                function_call.decl_variable.type_ = Python3TypeSize()
                function_call.arg_variables = [operation]
                operation = function_call
        return operation

    def compile_typecast(self, kil_typecast):
        if isinstance(kil_typecast.type_, KilTypeNone):
            typecast_immutable_class = isinstance(kil_typecast.value.type_, KilTypeClass) and not kil_typecast.value.mutable
            typecast_member = isinstance(kil_typecast.value, KilOperation) and kil_typecast.value.op == '.' and isinstance(kil_typecast.value.b, KilVariable)
            if typecast_immutable_class and typecast_member:
                if not (kil_typecast.value.list_ and kil_typecast.value.op != '[]'):
                    return self.compile_lazy_member_constructor_call(kil_typecast.value, set_member=False)
        return self.compile(kil_typecast.value)
