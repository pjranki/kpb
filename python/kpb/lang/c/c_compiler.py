import os
import sys
import copy

from .. kil.kil_compiler import *


class CFile(object):

    def __init__(self):
        self.name = None
        self.input_file_path = None
        self.children = list()


class CInclude(object):

    def __init__(self):
        self.name = None
        self.internal = False
        self.private = True


class CStruct(object):

    def __init__(self):
        self.name = None
        self.typedef_name = None
        self.members = list()


class CFunction(object):

    def __init__(self):
        self.decl_variable = None
        self.arg_variables = list()
        self.static = True
        self.code_block = None


class CFunctionCall(object):

    def __init__(self, decl_variable=None, arg_variables=None):
        self.decl_variable = decl_variable
        self.arg_variables = arg_variables if arg_variables else list()

    @property
    def name(self):
        return self.decl_variable.name

    @property
    def type_(self):
        return self.decl_variable.type_

    @property
    def const(self):
        return self.decl_variable.const

    @property
    def pointers(self):
        return self.decl_variable.pointers


class CMacro(object):

    def __init__(self, name=None, args=None, body=None):
        self.name = name
        self.args = args if args else list()
        self.body = body
        self.private = True


class CMacroCall(object):

    def __init__(self, name=None, *args):
        self.name = name
        self.args = list(args)

    @property
    def type_(self):
        return CTypeVoid()

    @property
    def const(self):
        for arg in self.args:
            if hasattr(arg, 'const'):
                return arg.const
        return True

    @property
    def pointers(self):
        for arg in self.args:
            if hasattr(arg, 'pointers'):
                return arg.pointers
        return 0


class CTypeVoid(object):
    pass


class CTypeBool(object):
    pass


class CTypeInteger(object):

    def __init__(self):
        self.signed = True
        self.bits = 32


class CTypeFloat(object):
    pass


class CTypeDouble(object):
    pass


class CTypeChar(object):
    pass


class CTypeWChar(object):
    pass


class CTypeSize(object):
    pass


class CTypeStruct(object):

    def __init__(self, name=None):
        self.name = name


class CVariable(object):

    def __init__(self, name=None, type_=None, const=False):
        self.name = name
        self.type_ = type_ if type_ else CTypeVoid()
        self.const = const
        self.pointers = 0


class CDeclaration(object):

    def __init__(self, variable=None):
        self.variable = variable

    @property
    def name(self):
        return self.variable.name

    @property
    def type_(self):
        return self.variable.type_

    @property
    def const(self):
        return self.variable.const

    @property
    def pointers(self):
        return self.variable.pointers


class CLiteral(object):

    def __init__(self, text):
        self.text = text

    @property
    def type_(self):
        return None

    @property
    def const(self):
        return True

    @property
    def pointers(self):
        return 0


class CBlock(object):

    def __init__(self, children=None):
        self.children = children if children else list()


class CReturn(object):

    def __init__(self, return_=None):
        self.return_ = return_

    @property
    def type_(self):
        return self.return_.type_

    @property
    def const(self):
        return self.return_.const

    @property
    def pointers(self):
        return self.return_.pointers


class CIf(object):

    def __init__(self, expr=None, true_body=None, false_body=None):
        self.expr = expr
        self.true_body = true_body
        self.false_body = false_body


class CFor(object):

    def __init__(self, start=None, condition=None, update=None, body=None):
        self.start = start
        self.condition = condition
        self.update = update
        self.body = body


class CWhile(object):

    def __init__(self, expr=None, body=None):
        self.expr = expr
        self.body = body


class CDoWhile(object):

    def __init__(self, expr=None, body=None):
        self.expr = expr
        self.body = body


class CBreak(object):

    def __init__(self):
        pass


class COperation(object):

    def __init__(self, a=None, op=None, b=None, c=None):
        self.a = a
        self.op = op
        self.b = b
        self.c = c

    @property
    def type_(self):
        if self.op == '.':
            return self.b.type_
        return self.a.type_

    @property
    def const(self):
        if self.op == '.':
            return self.b.const
        return self.a.const

    @property
    def pointers(self):
        if self.op == '.':
            return self.b.pointers
        return self.a.pointers


class CTypecast(object):

    def __init__(self, value=None, type_=None):
        self.value = value
        self.type_ = type_

    @property
    def const(self):
        return self.value.const

    @property
    def pointers(self):
        return self.value.pointers


class CCompiler(object):

    def __init__(self, header_ext='.h'):
        self.header_ext = header_ext
        self.name_prefix = ''

    def compile(self, kil_obj, args=None):
        if args and args.namespace:
            self.name_prefix = args.namespace + '_'
        compile_methods = {
            list: self.compile_list,
            KilFile: self.compile_file,
            KilImport: self.compile_import,
            KilNamespace: self.compile_namespace,
            KilClass: self.compile_class,
            KilEnum: self.compile_enum,
            KilMethod: self.compile_method,
            KilMethodCall: self.compile_method_call,
            KilConstructor: self.compile_constructor,
            KilDestructor: self.compile_destructor,
            KilFunctionCall: self.compile_function_call,
            KilTypeNone: self.compile_type_none,
            KilTypeBoolean: self.compile_type_boolean,
            KilTypeInteger: self.compile_type_integer,
            KilTypeFloat: self.compile_type_float,
            KilTypeString: self.compile_type_string,
            KilTypeChar: self.compile_type_char,
            KilTypeBuffer: self.compile_type_buffer,
            KilTypeSize: self.compile_type_size,
            KilTypeIO: self.compile_type_io,
            KilTypeClass: self.compile_type_class,
            KilVariable: self.compile_variable,
            KilDeclaration: self.compile_declaration,
            KilEnumValue: self.compile_enum_value,
            KilLiteral: self.compile_literal,
            KilMember: self.compile_member,
            KilBlock: self.compile_block,
            KilReturn: self.compile_return,
            KilIf: self.compile_if,
            KilFor: self.compile_for,
            KilWhile: self.compile_while,
            KilDoWhile: self.compile_dowhile,
            KilBreak: self.compile_break,
            KilOperation: self.compile_operation,
            KilTypecast: self.compile_typecast,
        }
        compile_method = compile_methods.get(kil_obj.__class__, None)
        if not compile_method:
            raise NotImplementedError(repr(kil_obj))
        return compile_method(kil_obj)

    def compile_list(self, kil_obj_list):
        obj_list = list()
        for kil_obj in kil_obj_list:
            obj = self.compile(kil_obj)
            if not obj:
                continue
            if isinstance(obj, list):
                obj_list.extend(obj)
            else:
                obj_list.append(obj)
        return obj_list

    def compile_file(self, kil_file):
        obj = CFile()
        obj.input_file_name = kil_file.input_file_name
        obj.name = kil_file.name
        include = CInclude()
        include.name = obj.name + self.header_ext
        include.internal = True
        include.private = True
        obj.children.append(include)
        include = CInclude()
        include.name = 'kpb_io' + self.header_ext
        include.internal = False
        include.private = False
        obj.children.append(include)
        obj.children.extend(self.compile(kil_file.children))
        return obj

    def compile_import(self, kil_import):
        include = CInclude()
        include.name = kil_import.name + self.header_ext
        include.internal = not kil_import.external
        include.private = not kil_import.public
        return include

    def compile_namespace(self, kil_namespace):
        return self.compile(kil_namespace.children)

    def compile_default_instance_global_and_function(self, struct):
        default_variable = CVariable()
        default_variable.name = '_default_{}'.format(struct.name)
        default_variable.type_ = CTypeStruct(name=struct.name)
        default_variable.const = False  # we need to be able to call init on it - for its members to be the right values
        default_variable.pointers = 0
        default_global = COperation(CDeclaration(default_variable), '=', CLiteral('{0}'))
        default_function = CFunction()
        default_function.decl_variable = CVariable()
        default_function.decl_variable.type_ = CTypeStruct(name=struct.name)
        default_function.decl_variable.const = True
        default_function.decl_variable.pointers = 1
        default_function.decl_variable.name = '{}_{}'.format(struct.name, 'default')
        default_function.decl_variable = CDeclaration(default_function.decl_variable)
        default_function.static = False
        decl_init = CVariable()
        decl_init.name = '{}_{}'.format(struct.name, 'init')
        decl_init.type_ = CTypeVoid()
        decl_init.const = False
        decl_init.pointers = 0
        init_default = CFunctionCall(decl_init, [COperation(default_variable, '&')])
        return_default = CReturn(COperation(default_variable, '&'))
        default_function.code_block = CBlock([
            init_default,
            return_default
        ])
        return [default_global, default_function]

    def compile_class(self, kil_class):
        obj = CStruct()
        obj.name = self.name_prefix + kil_class.name
        obj.typedef_name = obj.name + '_t'
        # default_func_ptr used in type checking by KPB_IO_ISINSTANCE
        default_func_ptr = CVariable(name='_default_func_ptr', type_=CTypeVoid(), const=True)
        default_func_ptr.pointers = 1
        obj.members.append(CDeclaration(default_func_ptr))
        obj.members.extend(self.compile(kil_class.members))
        struct_and_functions = list()
        struct_and_functions.append(obj)
        struct_and_functions.extend(self.compile(kil_class.enums))
        struct_and_functions.extend(self.compile_default_instance_global_and_function(obj))
        struct_and_functions.append(self.compile(kil_class.constructor))
        struct_and_functions.append(self.compile(kil_class.destructor))
        struct_and_functions.extend(self.compile(kil_class.methods))
        return struct_and_functions

    def compile_enum(self, kil_enum):
        class_type = self.compile(kil_enum.class_type)
        type_ = self.compile(kil_enum.type_)
        enums = list()
        for kil_enum_value in kil_enum.enums:
            obj = CMacro()
            obj.name = '{}_{}_{}'.format(class_type.name, kil_enum.name, kil_enum_value.name).upper()
            obj.private = False
            value = CLiteral('{}'.format(kil_enum_value.value))
            obj.body = CTypecast(value=value, type_=type_)
            enums.append(obj)
        return enums

    @classmethod
    def recompile_method_with_buffer_size_arg(cls, method):
        found = False
        for arg_variable in method.arg_variables:
            if isinstance(arg_variable.type_, KilTypeBuffer):
                found = True
        if isinstance(method.decl_variable.type_, KilTypeBuffer):
            found = True
        if not found:
            return method
        buffer_count = 0
        method = copy.deepcopy(method)
        new_arg_variables = list()
        for arg_variable in method.arg_variables:
            new_arg_variables.append(arg_variable)
            if isinstance(arg_variable.type_, KilTypeBuffer):
                buffer_count += 1
                size_variable = KilVariable()
                size_variable.name = '_size'
                size_variable.type_ = KilTypeSize()
                size_variable.mutable = arg_variable.mutable
                size_variable.reference = arg_variable.mutable
                new_arg_variables.append(KilDeclaration(size_variable))
        if isinstance(method.decl_variable.type_, KilTypeBuffer):
            buffer_count += 1
            size_variable = KilVariable()
            size_variable.name = '_size'
            size_variable.type_ = KilTypeSize()
            size_variable.mutable = True
            size_variable.reference = True
            new_arg_variables.append(KilDeclaration(size_variable))
        method.arg_variables = new_arg_variables
        assert buffer_count <= 1
        return method

    def compile_method(self, kil_method):
        if not (isinstance(kil_method.decl_variable.type_, KilTypeBuffer) and not kil_method.decl_variable.mutable):
            # FUTURE: this is a dirty hack to filter out return member buffers (don't want to have a size argument)
            kil_method = self.recompile_method_with_buffer_size_arg(kil_method)
        obj = CFunction()
        obj.decl_variable = self.compile(kil_method.decl_variable)
        obj.decl_variable.variable.name = self.name_prefix + kil_method.class_type.name + '_' + kil_method.decl_variable.name
        if not kil_method.class_method:
            arg_this_variable = CVariable()
            arg_this_variable.name = '_this'
            arg_this_variable.type_ = CTypeStruct(name=self.name_prefix + kil_method.class_type.name)
            arg_this_variable.const = not kil_method.mutable
            arg_this_variable.pointers = 1
            obj.arg_variables.append(CDeclaration(arg_this_variable))
        obj.arg_variables.extend(self.compile(kil_method.arg_variables))
        obj.static = not kil_method.public
        obj.code_block = self.compile(kil_method.code_block)
        obj = self.compile_function_null_checks(obj)
        return obj

    def compile_method_call(self, kil_method_call, kil_this_variable=None):
        assert kil_method_call.class_method or kil_this_variable
        if not (isinstance(kil_method_call.decl_variable.type_, KilTypeBuffer) and not kil_method_call.decl_variable.mutable):
            # FUTURE: this is a dirty hack to filter out return member buffers (don't want to have a size argument)
            kil_method_call = self.recompile_method_call_with_buffer_size_arg(kil_method_call)
        class_type = self.compile(kil_method_call.class_type)
        method_name = kil_method_call.decl_variable.name
        if kil_method_call.class_method and isinstance(kil_method_call.class_type, KilTypeIO):
            if isinstance(kil_method_call.class_type, KilTypeIO):
                if method_name == 'random_bytes':
                    args = self.compile(kil_method_call.arg_variables)
                    assert len(args) >= 6
                    assert isinstance(args[1], CVariable) and args[1].name == '_size'
                    args = args[0: 1] + args[2: ]  # remove added 'size' variable
                    assert len(args) == 5
                    if isinstance(args[0], COperation) and args[0].op == '[]':
                        args = [args[0].a] + [args[0].b] + args[1:]
                        assert len(args) == 6
                        return CMacroCall('KPB_RANDOM_BYTES_LIST', *args)
                    return CMacroCall('KPB_RANDOM_BYTES', *args)
                if method_name == 'random_char_str':
                    args = self.compile(kil_method_call.arg_variables)
                    assert len(args) == 6
                    if isinstance(args[0], COperation) and args[0].op == '[]':
                        args = [args[0].a] + [args[0].b] + args[1:]
                        assert len(args) == 7
                        return CMacroCall('KPB_RANDOM_CHAR_STR_LIST', *args)
                    return CMacroCall('KPB_RANDOM_CHAR_STR', *args)
                if method_name == 'random_wchar_str':
                    args = self.compile(kil_method_call.arg_variables)
                    assert len(args) == 6
                    if isinstance(args[0], COperation) and args[0].op == '[]':
                        args = [args[0].a] + [args[0].b] + args[1:]
                        assert len(args) == 7
                        return CMacroCall('KPB_RANDOM_WCHAR_STR_LIST', *args)
                    return CMacroCall('KPB_RANDOM_WCHAR_STR', *args)
            function_name = '{}_{}'.format('kpb', method_name)
        else:
            function_name = '{}_{}'.format(class_type.name, method_name)
        obj = CFunctionCall()
        obj.decl_variable = self.compile(kil_method_call.decl_variable)
        obj.decl_variable.name = function_name
        if not kil_method_call.class_method:
            obj.arg_variables.append(self.compile(kil_this_variable))
        obj.arg_variables.extend(self.compile(kil_method_call.arg_variables))
        new_arg_variables = list()
        for arg_variable in obj.arg_variables:
            if isinstance(arg_variable.type_, CTypeStruct) and arg_variable.pointers == 0:
                if isinstance(arg_variable, COperation) and arg_variable.a.name == '_this':
                    default_function_name = '{}_default'.format(arg_variable.type_.name)
                    arg_variable = CMacroCall('KPB_OBJECT_GET', arg_variable.type_, arg_variable, CLiteral(default_function_name))
                else:
                    arg_variable = COperation(arg_variable, '&')
            new_arg_variables.append(arg_variable)
        obj.arg_variables = new_arg_variables
        if obj.decl_variable.name.endswith('write_bytes'):
            # FUTURE: this adds a second argument to pass the members size
            assert isinstance(obj.arg_variables[1], CFunctionCall)
            assert obj.arg_variables[2].name == '_size'
            get_buffer = obj.arg_variables[1]
            get_buffer_size = CFunctionCall()
            get_buffer_size.decl_variable = CVariable()
            if get_buffer.decl_variable.name.endswith('_at'):
                get_buffer_size.decl_variable.name = get_buffer.decl_variable.name[:-len('_at')] + '_size_at'
            else:
                get_buffer_size.decl_variable.name = get_buffer.decl_variable.name + '_size'
            get_buffer_size.decl_variable.type_ = CTypeSize()
            get_buffer_size.decl_variable.const = False
            get_buffer_size.decl_variable.pointers = 0
            get_buffer_size.arg_variables = get_buffer.arg_variables
            obj.arg_variables[2] = get_buffer_size
        if len(obj.arg_variables) == 3 and isinstance(obj.arg_variables[1], CFunctionCall) and obj.arg_variables[1].name.endswith('read_bytes'):
            # FUTURE: this adds a second argument to get the buffer's size from IO
            assert isinstance(obj.arg_variables[1], CFunctionCall)
            assert obj.arg_variables[2].name == '_size'
            read_bytes = obj.arg_variables[1]
            size_variable = read_bytes.arg_variables[1]
            read_bytes.arg_variables = [read_bytes.arg_variables[0], size_variable]
            read_bytes_size = CFunctionCall()
            read_bytes_size.decl_variable = CVariable()
            read_bytes_size.decl_variable.name = read_bytes.decl_variable.name + '_size'
            read_bytes_size.decl_variable.type_ = CTypeSize()
            read_bytes_size.decl_variable.const = False
            read_bytes_size.decl_variable.pointers = 0
            read_bytes_size.arg_variables = [read_bytes.arg_variables[0], size_variable]
            obj.arg_variables[2] = read_bytes_size
        return obj

    def compile_default_literal_from_variable(self, variable):
        if isinstance(variable.type_, CTypeVoid):
            if variable.pointers > 0:
                if variable.const:
                    return CLiteral('""')
                return CLiteral('NULL')
            else:
                return None
        elif isinstance(variable.type_, CTypeBool):
            if variable.pointers > 0:
                return CLiteral('NULL')
            else:
                return CLiteral('false')
        elif isinstance(variable.type_, CTypeInteger):
            if variable.pointers > 0:
                return CLiteral('NULL')
            else:
                return CLiteral('0')
        elif isinstance(variable.type_, CTypeChar):
            if variable.pointers > 0:
                if variable.const:
                    return CLiteral('""')
                return CLiteral('NULL')
            else:
                return CLiteral("'\\0'")
        elif isinstance(variable.type_, CTypeSize):
            if variable.pointers > 0:
                return CLiteral('NULL')
            else:
                return CLiteral('0')
        elif isinstance(variable.type_, CTypeStruct):
            if variable.pointers > 0:
                if variable.const:
                    raise NotImplementedError('implement the "default" function to get a default (static) struct')
                return CLiteral('NULL')
            else:
                return CLiteral('{0}')
        else:
            raise NotImplementedError("default literal for '{}'".format(variable.type_))

    def compile_function_null_checks(self, obj):
        assert isinstance(obj, CFunction)
        arg_variables_reversed = obj.arg_variables[:]
        arg_variables_reversed.reverse()
        for decl_arg_variable in arg_variables_reversed:
            arg_variable = decl_arg_variable.variable
            if arg_variable.pointers == 0:
                continue
            if arg_variable.name == '_this' and arg_variable.const:
                get_default = CFunctionCall()
                get_default.decl_variable = CVariable()
                get_default.decl_variable.name = '{}_{}'.format(arg_variable.type_.name, 'default')
                get_default.decl_variable.type_ = arg_variable.type_
                get_default.decl_variable.const = arg_variable.const
                get_default.decl_variable.pointers = arg_variable.pointers
                obj.code_block.children.insert(0, CIf(expr=arg_variable, false_body=CBlock([
                    COperation(arg_variable, '=', get_default)
                ])))
            else:
                obj.code_block.children.insert(0, CIf(expr=arg_variable, false_body=CBlock([
                    CReturn(self.compile_default_literal_from_variable(obj.decl_variable))
                ])))
        return obj

    def compile_constructor(self, kil_constructor):
        method = KilMethod()
        method.class_type = kil_constructor.class_type
        method.decl_variable = KilVariable()
        method.decl_variable.name = 'init'
        method.decl_variable.type_ = KilTypeNone()
        method.decl_variable.mutable = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        method.arg_variables = kil_constructor.arg_variables
        method.mutable = True
        method.public = kil_constructor.public
        method.code_block = kil_constructor.code_block

        # add code to zero structure memory (i.e. make sizes and pointers 0 by default)
        memset_call = KilFunctionCall()
        memset_call.decl_variable = KilVariable()
        memset_call.decl_variable.name = 'kpb_io_memset'
        memset_call.decl_variable.type_ = KilTypeNone()
        memset_call.decl_variable.mutable = False
        memset_call.decl_variable.reference = False
        memset_call.decl_variable.list_ = False
        ptr_variable = KilVariable()
        ptr_variable.name = '_this'
        ptr_variable.type_ = kil_constructor.class_type
        ptr_variable.mutable = True
        ptr_variable.reference = False
        ptr_variable.list_ = False
        size_variable = KilLiteral('sizeof(*_this)', KilTypeSize())
        memset_call.arg_variables = [ptr_variable, KilLiteral(0, KilTypeInteger()), size_variable]
        method.code_block.children = method.code_block.children[:]
        method.code_block.children.insert(0, memset_call)

        # default_func_ptr used in type checking by KPB_IO_ISINSTANCE
        default_func_ptr = KilVariable(name='_default_func_ptr', type_=KilTypeNone(), mutable=False, reference=True)
        ptr_variable = KilVariable()
        ptr_variable.name = '_this'
        ptr_variable.type_ = kil_constructor.class_type
        ptr_variable.mutable = True
        ptr_variable.reference = True
        ptr_variable.list_ = False
        default_func_ptr = KilOperation(ptr_variable, '.', default_func_ptr)
        default_function_name = '{}{}{}_default'.format('(size_t)', self.name_prefix, ptr_variable.type_.name)
        default_function = KilVariable(name=default_function_name, type_=KilTypeInteger(), mutable=False, reference=True)
        set_default_fn = KilOperation(default_func_ptr, '=', KilTypecast(default_function, type_=KilTypeNone()))
        method.code_block.children = method.code_block.children[:]
        method.code_block.children.insert(1, set_default_fn)
        return self.compile_method(method)

    def compile_destructor(self, kil_destructor):
        method = KilMethod()
        method.class_type = kil_destructor.class_type
        method.decl_variable = KilVariable()
        method.decl_variable.name = 'fini'
        method.decl_variable.type_ = KilTypeNone()
        method.decl_variable.mutable = False
        method.decl_variable = KilDeclaration(method.decl_variable)
        method.arg_variables = list()
        method.mutable = True
        method.public = kil_destructor.public
        method.code_block = kil_destructor.code_block
        return self.compile_method(method)

    def compile_type_none(self, kil_type):
        return CTypeVoid()

    def compile_type_boolean(self, kil_type):
        return CTypeBool()

    def compile_type_integer(self, kil_type):
        obj = CTypeInteger()
        obj.signed = kil_type.signed
        obj.bits = kil_type.bits
        if obj.bits <= 8:
            obj.bits = 8
        elif obj.bits <= 16:
            obj.bits = 16
        elif obj.bits <= 32:
            obj.bits = 32
        elif obj.bits <= 64:
            obj.bits = 64
        else:
            raise NotImplementedError('integers with more than 64 bits ({} bits)'.format(obj.bits))
        return obj

    def compile_type_float(self, kil_type):
        if kil_type.bits == 32:
            return CTypeFloat()
        if kil_type.bits == 64:
            return CTypeDouble()
        raise NotImplementedError('floats with {} bits'.format(kil_type.bits))

    def compile_type_string(self, kil_type):
        if kil_type.wide:
            return CTypeWChar()
        return CTypeChar()

    def compile_type_char(self, kil_type):
        if kil_type.wide:
            return CTypeWChar()
        return CTypeChar()

    def compile_type_buffer(self, kil_type):
        return CTypeChar()

    def compile_type_size(self, kil_type):
        return CTypeSize()

    def compile_type_io(self, kil_type):
        return CTypeStruct(name='kpb_io')

    def compile_type_class(self, kil_type):
        if kil_type.external:
            return CTypeStruct(name=(kil_type.name))
        return CTypeStruct(name=(self.name_prefix + kil_type.name))

    def compile_variable(self, kil_variable):
        variable = CVariable()
        variable.name = kil_variable.name
        variable.type_ = self.compile(kil_variable.type_)
        variable.const = not kil_variable.mutable
        if isinstance(kil_variable.type_, KilTypeNone) or \
                isinstance(kil_variable.type_, KilTypeBoolean) or \
                isinstance(kil_variable.type_, KilTypeInteger) or \
                isinstance(kil_variable.type_, KilTypeFloat) or \
                isinstance(kil_variable.type_, KilTypeSize):
            if not kil_variable.reference:
                if not kil_variable.list_:
                    variable.pointers = 0
                else:
                    variable.pointers = 1
            else:
                variable.pointers = 1
        elif isinstance(kil_variable.type_, KilTypeString):
            if not kil_variable.list_:
                variable.pointers = 1
            else:
                variable.pointers = 2
        elif isinstance(kil_variable.type_, KilTypeBuffer):
            if not kil_variable.list_:
                variable.pointers = 1
            else:
                variable.pointers = 2
        elif isinstance(kil_variable.type_, KilTypeIO) or \
                isinstance(kil_variable.type_, KilTypeClass):
            if not kil_variable.reference:
                if not kil_variable.list_:
                    variable.pointers = 0
                else:
                    variable.pointers = 1
            else:
                if not kil_variable.list_:
                    variable.pointers = 1
                else:
                    variable.pointers = 2
        else:
            raise NotImplementedError(type(kil_variable.type_))
        if variable.name == KilVariable.THIS_NAME:
            variable.name = '_this'
            variable.pointers = 1
            variable.reference = False
        if variable.pointers == 0:
            variable.const = False  # don't bother with const when not a pointers
        return variable

    def compile_declaration(self, kil_declaration):
        declaration = CDeclaration()
        declaration.variable = self.compile(kil_declaration.variable)
        return declaration

    def compile_enum_value(self, kil_enum_value):
        type_ = self.compile(kil_enum_value.enum.class_type)
        macro_name = '{}_{}_{}'.format(type_.name, kil_enum_value.enum.name, kil_enum_value.name)
        return CMacroCall(macro_name.upper())

    def compile_literal(self, literal):
        if isinstance(literal.type_, KilTypeNone):
            return CLiteral('NULL')
        elif isinstance(literal.type_, KilTypeBoolean):
            return CLiteral('true' if literal.value else 'false')
        elif isinstance(literal.type_, KilTypeInteger):
            return CLiteral('{}'.format(literal.value))
        elif isinstance(literal.type_, KilTypeFloat):
            return CLiteral('{}'.format(literal.value))
        elif isinstance(literal.type_, KilTypeSize):
            return CLiteral('{}'.format(literal.value))
        elif isinstance(literal.type_, KilTypeBuffer):
            if literal.value is None:
                return CLiteral('NULL')
            return CLiteral('"{}"'.format(''.join(['\\x{:02x}'.format(v) for v in literal.value])))
        elif isinstance(literal.type_, KilTypeString):
            if literal.value is None:
                return CLiteral('NULL')
            if literal.type_.wide:
                return CLiteral('L"{}"'.format(literal.value))
            return CLiteral('"{}"'.format(literal.value))
        elif isinstance(literal.type_, KilTypeChar):
            if literal.type_.wide:
                return CLiteral('((wchar_t)0x{:04x})'.format(literal.value))
            return CLiteral('((char)0x{:02x})'.format(literal.value))
        elif isinstance(literal.type_, KilTypeIO):
            return CLiteral('{0}')
        elif isinstance(literal.type_, KilTypeClass):
            return CLiteral('{0}')
        else:
            raise NotImplementedError('literal type {}'.format(repr(literal.type_)))

    def compile_member(self, kil_member):
        variable = self.compile(kil_member.variable)
        if kil_member.variable.list_:
            if isinstance(kil_member.variable.type_, KilTypeString):
                return CMacroCall('KPB_STRING_LIST', variable.type_, CLiteral(variable.name))
            if isinstance(kil_member.variable.type_, KilTypeBuffer):
                return CMacroCall('KPB_BYTES_LIST', variable.type_, CLiteral(variable.name))
            if isinstance(kil_member.variable.type_, KilTypeClass):
                return CMacroCall('KPB_OBJECT_LIST', variable.type_, CLiteral(variable.name))
            return CMacroCall('KPB_LIST', variable.type_, CLiteral(variable.name))
        if isinstance(kil_member.variable.type_, KilTypeString):
            return CMacroCall('KPB_STRING', variable.type_, CLiteral(variable.name))
        if isinstance(kil_member.variable.type_, KilTypeBuffer):
            return CMacroCall('KPB_BYTES', variable.type_, CLiteral(variable.name))
        if isinstance(kil_member.variable.type_, KilTypeClass):
            return CMacroCall('KPB_OBJECT', variable.type_, CLiteral(variable.name))
        return variable

    def compile_block(self, kil_block):
        block = CBlock()
        block.children = self.compile(kil_block.children)
        return block

    def compile_return(self, kil_return):
        return_ = CReturn()
        if not kil_return.return_:
            return return_
        if kil_return.list_:
            type_ = self.compile(kil_return.type_)
            variable = self.compile(kil_return.return_)
            if isinstance(kil_return.type_, KilTypeString):
                return CReturn(CMacroCall('KPB_STRING_LIST_GET', type_, variable))
            if isinstance(kil_return.type_, KilTypeBuffer):
                return CReturn(CMacroCall('KPB_BYTES_LIST_GET', type_, variable))
            if isinstance(kil_return.type_, KilTypeClass):
                return CReturn(CMacroCall('KPB_OBJECT_LIST_GET', type_, variable))
            return CReturn(CMacroCall('KPB_LIST_GET', type_, variable))
        if isinstance(kil_return.type_, KilTypeString):
            type_ = self.compile(kil_return.type_)
            variable = self.compile(kil_return.return_)
            if isinstance(variable, COperation) and variable.op == '.':
                return CReturn(CMacroCall('KPB_STRING_GET', type_, variable))
            elif isinstance(variable, CFunctionCall) and variable.name.endswith('_getter'):
                variable.arg_variables[1] = CMacroCall('KPB_STRING_GET', type_, variable.arg_variables[1])
                return CReturn(variable)
        if isinstance(kil_return.type_, KilTypeBuffer):
            type_ = self.compile(kil_return.type_)
            variable = self.compile(kil_return.return_)
            if isinstance(variable, COperation) and variable.op == '.':
                return CReturn(CMacroCall('KPB_BYTES_GET', type_, variable))
        if isinstance(kil_return.type_, KilTypeClass):
            type_ = self.compile(kil_return.type_)
            variable = self.compile(kil_return.return_)
            if isinstance(variable, COperation) and variable.op == '.' and variable.a.const:
                default_function_name = '{}_default'.format(type_.name)
                return CReturn(CMacroCall('KPB_OBJECT_GET', type_, variable, CLiteral(default_function_name)))
            if isinstance(variable, CLiteral) and not kil_return.mutable:
                default_function_name = '{}_default'.format(type_.name)
                return CReturn(CMacroCall('KPB_OBJECT_GET', type_, CLiteral('NULL'), CLiteral(default_function_name)))
        if isinstance(kil_return.return_, KilTypecast) and isinstance(kil_return.type_, KilTypeNone):
            type_ = self.compile(kil_return.type_)
            typecast = self.compile(kil_return.return_)
            variable = typecast.value
            if isinstance(variable, COperation) and variable.op == '.' and variable.a.const:
                default_function_name = '{}_default'.format(typecast.value.type_.name)
                typecast.value = CMacroCall('KPB_OBJECT_GET', variable.type_, variable, CLiteral(default_function_name))
                return CReturn(typecast)
        return_.return_ = self.compile(kil_return.return_)
        return return_

    def compile_if(self, kil_if):
        if_ = CIf()
        if_.expr = self.compile(kil_if.expr)
        if kil_if.true_body:
            if_.true_body = self.compile(kil_if.true_body)
        if kil_if.false_body:
            if_.false_body = self.compile(kil_if.false_body)
        if isinstance(if_.expr, CFunctionCall) and len(if_.expr.arg_variables) >= 2:
            if isinstance(if_.expr.arg_variables[1], CFunctionCall) and if_.expr.arg_variables[1].decl_variable.name.endswith('read_bytes'):
                # HACK: this is an optimization to make sure we only calculate the read size once
                set_function_call = if_.expr
                read_bytes_function_call = set_function_call.arg_variables[1]
                read_bytes_size_function_call = set_function_call.arg_variables[2]
                size_variable = read_bytes_function_call.arg_variables[1]
                member_name = kil_if.expr.b.decl_variable.name
                assert member_name.startswith('set_') or member_name.startswith('add_')
                member_name = member_name[4:]
                read_size_variable = CVariable(name='_size_{}'.format(member_name), type_=CTypeSize())
                decl_read_size_variable = COperation(CDeclaration(read_size_variable), '=', size_variable)
                read_bytes_function_call.arg_variables[1] = read_size_variable
                read_bytes_size_function_call.arg_variables[1] = read_size_variable
                return [decl_read_size_variable, if_]
        return if_

    def compile_for(self, kil_for):
        for_ = CFor()
        for_.start = self.compile(kil_for.start)
        for_.condition = self.compile(kil_for.condition)
        for_.update = self.compile(kil_for.update)
        for_.body = self.compile(kil_for.body)
        return for_

    def compile_while(self, kil_while):
        while_ = CWhile()
        while_.expr = self.compile(kil_while.expr)
        while_.body = self.compile(kil_while.body)
        return while_

    def compile_dowhile(self, kil_dowhile):
        dowhile_ = CDoWhile()
        dowhile_.expr = self.compile(kil_dowhile.expr)
        dowhile_.body = self.compile(kil_dowhile.body)
        return dowhile_

    def compile_break(self, kil_break):
        return CBreak()

    def compile_function_call(self, kil_function_call):
        obj = CFunctionCall()
        obj.decl_variable = self.compile(kil_function_call.decl_variable)
        obj.arg_variables = self.compile(kil_function_call.arg_variables)
        return obj

    @classmethod
    def recompile_method_call_with_buffer_size_arg(cls, method_call):
        found = False
        for arg_variable in method_call.arg_variables:
            if isinstance(arg_variable.type_, KilTypeBuffer):
                found = True
        if isinstance(method_call.decl_variable.type_, KilTypeBuffer):
            found = True
        if not found:
            return method_call
        buffer_count = 0
        method_call = copy.deepcopy(method_call)
        new_arg_variables = list()
        for arg_variable in method_call.arg_variables:
            new_arg_variables.append(arg_variable)
            if isinstance(arg_variable.type_, KilTypeBuffer):
                is_copy = isinstance(arg_variable, KilOperation) and arg_variable.op == '.' and isinstance(arg_variable.a, KilVariable) and arg_variable.a.name == '_other'
                buffer_count += 1
                if is_copy:
                    size_variable = copy.deepcopy(arg_variable)
                    if size_variable.b.decl_variable.name.endswith('_at'):
                        size_variable.b.decl_variable.name = size_variable.b.decl_variable.name[:-len('_at')] + '_size_at'
                    else:
                        size_variable.b.decl_variable.name = size_variable.b.decl_variable.name + '_size'
                elif isinstance(arg_variable, KilLiteral):
                    size_variable = KilLiteral(len(arg_variable.value), KilTypeSize())
                else:
                    size_variable = KilVariable()
                    size_variable.name = '_size'
                    size_variable.type_ = KilTypeSize()
                    size_variable.mutable = arg_variable.mutable
                    size_variable.reference = arg_variable.mutable
                new_arg_variables.append(size_variable)
        if isinstance(method_call.decl_variable.type_, KilTypeBuffer):
            buffer_count += 1
            size_variable = KilVariable()
            size_variable.name = '_size'
            size_variable.type_ = KilTypeSize()
            size_variable.mutable = True
            size_variable.reference = True
            new_arg_variables.append(size_variable)
        if method_call.decl_variable.name == 'setter' and buffer_count == 2:
            # TODO: not entirely happy with this implementation
            new_arg_variables = new_arg_variables[:-1]
        else:
            assert buffer_count <= 1
        method_call.arg_variables = new_arg_variables
        return method_call

    def compile_operation(self, kil_operation):
        if kil_operation.op == '.' and isinstance(kil_operation.b, KilMethodCall):
            kil_this_variable = kil_operation.a
            kil_method_call = kil_operation.b
            return self.compile_method_call(kil_method_call, kil_this_variable)
        if kil_operation.op == '=' and isinstance(kil_operation.b, KilConstructorCall):
            kil_variable = kil_operation.a
            kil_constructor_call = kil_operation.b
            if kil_operation.a.mutable and not isinstance(kil_operation.a.type_, KilTypeIO):
                if not kil_constructor_call.class_type.external:
                    class_type = self.compile(kil_constructor_call.class_type)
                    type_ = self.compile(kil_variable.type_)
                    member = self.compile(kil_variable)
                    constructor_name = '{}_{}'.format(type_.name, 'init')
                    return CMacroCall('KPB_OBJECT_LAZY_CONSTRUCTOR', type_, member, CLiteral(constructor_name))
            decl_variable = self.compile(kil_variable)
            class_type = self.compile(kil_constructor_call.class_type)
            call_init = CFunctionCall()
            call_init.decl_variable = CVariable()
            call_init.decl_variable.name = '{}_{}'.format(class_type.name, 'init')
            call_init.decl_variable.type_ = CTypeVoid()
            call_init.decl_variable.const = False
            call_init.decl_variable.pointers = 0
            call_init.arg_variables = [COperation(decl_variable.variable, '&')]
            call_init.arg_variables.extend([self.compile(arg_variable) for arg_variable in kil_constructor_call.arg_variables])
            return [decl_variable, call_init]
        if kil_operation.op == '.' and isinstance(kil_operation.b, KilDestructorCall):
            kil_variable = kil_operation.a
            kil_destructor_call = kil_operation.b
            variable = self.compile(kil_variable)
            class_type = self.compile(kil_destructor_call.class_type)
            call_fini = CFunctionCall()
            call_fini.decl_variable = CVariable()
            call_fini.decl_variable.name = '{}_{}'.format(class_type.name, 'fini')
            call_fini.decl_variable.type_ = CTypeVoid()
            call_fini.decl_variable.const = False
            call_fini.decl_variable.pointers = 0
            call_fini.arg_variables = [COperation(variable, '&')]
            return call_fini
        if kil_operation.op == 'clr' and kil_operation.a.list_:
            if isinstance(kil_operation.a.type_, KilTypeString):
                return CMacroCall('KPB_STRING_LIST_CLEAR', self.compile(kil_operation.a.type_), self.compile(kil_operation.a))
            if isinstance(kil_operation.a.type_, KilTypeBuffer):
                return CMacroCall('KPB_BYTES_LIST_CLEAR', self.compile(kil_operation.a.type_), self.compile(kil_operation.a))
            if isinstance(kil_operation.a.type_, KilTypeClass):
                kil_variable = kil_operation.a
                type_ = self.compile(kil_variable.type_)
                fini_function_name = '{}_fini'.format(type_.name)
                return CMacroCall('KPB_OBJECT_LIST_CLEAR', type_, self.compile(kil_variable), CLiteral(fini_function_name))
            return CMacroCall('KPB_LIST_CLEAR', self.compile(kil_operation.a.type_), self.compile(kil_operation.a))
        if kil_operation.op == 'len':
            if isinstance(kil_operation.a, KilOperation) and kil_operation.a.op == '[]':
                if isinstance(kil_operation.a.type_, KilTypeString):
                    type_ = self.compile(kil_operation.a.a.type_)
                    variable = self.compile(kil_operation.a.a)
                    index_variable = self.compile(kil_operation.a.b)
                    return CMacroCall('KPB_STRING_SIZE_AT', type_, variable, index_variable)
                if isinstance(kil_operation.a.type_, KilTypeBuffer):
                    type_ = self.compile(kil_operation.a.a.type_)
                    variable = self.compile(kil_operation.a.a)
                    index_variable = self.compile(kil_operation.a.b)
                    return CMacroCall('KPB_BYTES_SIZE_AT', type_, variable, index_variable)
            if kil_operation.a.list_:
                if isinstance(kil_operation.a.type_, KilTypeString):
                    return CMacroCall('KPB_STRING_LIST_SIZE', self.compile(kil_operation.a.type_), self.compile(kil_operation.a))
                if isinstance(kil_operation.a.type_, KilTypeBuffer):
                    return CMacroCall('KPB_BYTES_LIST_SIZE', self.compile(kil_operation.a.type_), self.compile(kil_operation.a))
                if isinstance(kil_operation.a.type_, KilTypeClass):
                    return CMacroCall('KPB_OBJECT_LIST_SIZE', self.compile(kil_operation.a.type_), self.compile(kil_operation.a))
                return CMacroCall('KPB_LIST_SIZE', self.compile(kil_operation.a.type_), self.compile(kil_operation.a))
            if isinstance(kil_operation.a.type_, KilTypeString):
                return CMacroCall('KPB_STRING_SIZE', self.compile(kil_operation.a.type_), self.compile(kil_operation.a))
            if isinstance(kil_operation.a.type_, KilTypeBuffer):
                return CMacroCall('KPB_BYTES_SIZE', self.compile(kil_operation.a.type_), self.compile(kil_operation.a))
            raise NotImplementedError(repr(kil_operation.a.type_))
        if kil_operation.op == 'add':
            if isinstance(kil_operation.a.type_, KilTypeString):
                return CMacroCall('KPB_STRING_LIST_ADD', self.compile(kil_operation.a.type_), self.compile(kil_operation.a), self.compile(kil_operation.b))
            if isinstance(kil_operation.a.type_, KilTypeBuffer):
                size_variable = CVariable(name='_size', type_=CTypeSize(), const=False)
                return CMacroCall('KPB_BYTES_LIST_ADD', self.compile(kil_operation.a.type_), self.compile(kil_operation.a), self.compile(kil_operation.b), size_variable)
            if isinstance(kil_operation.a.type_, KilTypeClass):
                type_ = self.compile(kil_operation.a.type_)
                constructor_name = '{}_{}'.format(type_.name, 'init')
                return CMacroCall('KPB_OBJECT_LIST_ADD', self.compile(kil_operation.a.type_), self.compile(kil_operation.a), CLiteral(constructor_name))
            return CMacroCall('KPB_LIST_ADD', self.compile(kil_operation.a.type_), self.compile(kil_operation.a), self.compile(kil_operation.b))
        if kil_operation.op == '=':
            kil_variable = kil_operation.a
            kil_value = kil_operation.b
            if isinstance(kil_value.type_, KilTypeString) and isinstance(kil_value, KilLiteral) and kil_value.value == '':
                return CMacroCall('KPB_STRING_CLEAR', self.compile(kil_value.type_), self.compile(kil_variable))
            if isinstance(kil_value.type_, KilTypeBuffer) and isinstance(kil_value, KilLiteral):
                assert isinstance(kil_value.value, bytes)
                c_string = CLiteral('"{}"'.format(''.join(['\\x{:02x}'.format(v) for v in kil_value.value])))
                size = CLiteral('{}'.format(len(kil_value.value)))
                return CMacroCall('KPB_BYTES_CLEAR', self.compile(kil_value.type_), self.compile(kil_variable), c_string, size)
            if isinstance(kil_value.type_, KilTypeClass) and isinstance(kil_operation.b, KilLiteral):
                type_ = self.compile(kil_value.type_)
                fini_function_name = '{}_fini'.format(type_.name)
                default_function_name = '{}_default'.format(type_.name)
                return CMacroCall('KPB_OBJECT_CLEAR', type_, self.compile(kil_variable), CLiteral(fini_function_name), CLiteral(default_function_name))
            if isinstance(kil_value.type_, KilTypeString) and isinstance(kil_value, KilVariable):
                set_string = CMacroCall('KPB_STRING_SET', self.compile(kil_value.type_), self.compile(kil_variable), self.compile(kil_value))
                set_string = CIf(expr=set_string, false_body=CBlock([
                    CReturn(CLiteral('false')),
                ]))
                return set_string
            if isinstance(kil_value.type_, KilTypeBuffer) and isinstance(kil_value, KilVariable):
                if not (isinstance(kil_variable, KilVariable) and kil_variable.name == '_output'):
                    # HACK: filter out "_output = data" from using KPB_BYTES_SET
                    size_variable = CVariable(name='_size', type_=CTypeSize(), const=False)
                    set_buffer = CMacroCall('KPB_BYTES_SET', self.compile(kil_value.type_), self.compile(kil_variable), self.compile(kil_value), size_variable)
                    set_buffer = CIf(expr=set_buffer, false_body=CBlock([
                        CReturn(CLiteral('false')),
                    ]))
                    return set_buffer
        operation = COperation()
        operation.a = self.compile(kil_operation.a)
        operation.op = kil_operation.op
        if kil_operation.b:
            operation.b = self.compile(kil_operation.b)
        if kil_operation.c:
            operation.c = self.compile(kil_operation.c)
        if operation.op == 'and':
            operation.op = '&&'
        if operation.op == 'or':
            operation.op = '||'
        if operation.op == 'not':
            operation.op = '!'
        return operation

    def compile_typecast(self, kil_typecast):
        typecast = CTypecast()
        typecast.value = self.compile(kil_typecast.value)
        typecast.type_ = self.compile(kil_typecast.type_)
        return typecast
