import os
import sys

from .. text_helper import TextHelper
from . c_compiler import  *


class CGenerator(object):

    def __init__(self):
        self.args = None

    def generate(self, obj, args=None):
        if not self.args:
            self.args = args
        generate_methods = {
            list: self.generate_list,
            CFile: self.generate_file,
            CInclude: self.generate_include,
            CMacro: self.generate_macro,
            CStruct: self.generate_struct,
            CFunction: self.generate_function,
            CFunctionCall: self.generate_function_call,
            CMacroCall: self.generate_macro_call,
            CTypeVoid: self.generate_type_void,
            CTypeBool: self.generate_type_bool,
            CTypeChar: self.generate_type_char,
            CTypeInteger: self.generate_type_integer,
            CTypeFloat: self.generate_type_float,
            CTypeDouble: self.generate_type_double,
            CTypeChar: self.generate_type_char,
            CTypeWChar: self.generate_type_wchar,
            CTypeSize: self.generate_type_size,
            CTypeStruct: self.generate_type_struct,
            CVariable: self.generate_variable,
            CDeclaration: self.generate_declaration,
            CLiteral: self.generate_literal,
            CBlock: self.generate_block,
            CReturn: self.generate_return,
            CIf: self.generate_if,
            CFor: self.generate_for,
            CWhile: self.generate_while,
            CDoWhile: self.generate_dowhile,
            CBreak: self.generate_break,
            COperation: self.generate_operation,
            CTypecast: self.generate_typecast
        }
        generate_method = generate_methods.get(obj.__class__, None)
        if not generate_method:
            raise NotImplementedError(repr(obj))
        return generate_method(obj)

    def generate_list(self, obj_list):
        text = TextHelper()
        first = False
        for obj in obj_list:
            text_block = self.generate(obj)
            if len(text_block.strip()) > 0:
                if not first:
                    text.nl()
                text.block(text_block)
            first = True
        return str(text)

    def generate_include(self, obj):
        if obj.internal:
            return '#include "{}"'.format(obj.name)
        return '#include <{}>'.format(obj.name)

    def generate_macro(self, obj):
        text = '#define {}'.format(obj.name)
        if len(obj.args) > 0:
            text = '{}({})'.format(text, ', '.join(obj.args))
        if obj.body:
            body_text = self.generate(obj.body)
            if body_text.find('\n') != -1:
                raise NotImplementedError('multi-line macros')
            text = '{} ({})'.format(text, body_text)
        return text

    def generate_type_void(self, obj):
        return 'void'

    def generate_type_bool(self, obj):
        return 'bool'

    def generate_type_char(self, obj):
        return 'char'

    def generate_type_integer(self, obj):
        prefix = 'u' if not obj.signed else ''
        return '{}int{}_t'.format(prefix, obj.bits)

    def generate_type_float(self, obj):
        return 'float'

    def generate_type_double(self, obj):
        return 'double'

    def generate_type_char(self, obj):
        return 'char'

    def generate_type_wchar(self, obj):
        return 'wchar_t'

    def generate_type_size(self, obj):
        return 'size_t'

    def generate_type_struct(self, obj):
        return 'struct {}'.format(obj.name)

    def generate_variable(self, obj):
        return obj.name

    def generate_declaration(self, obj):
        const_text = 'const ' if obj.const else ''
        type_text = self.generate(obj.type_)
        pointer_text = '*' * obj.pointers
        name_text = self.generate(obj.variable)
        return '{}{} {}{}'.format(const_text, type_text, pointer_text, name_text)

    def generate_literal(self, obj):
        return obj.text

    def generate_block(self, obj):
        text = TextHelper()
        text.line('{')
        text.tab()
        for child in obj.children:
            sub_text = self.generate(child)
            if isinstance(child, CBlock):
                text.block(sub_text)
            elif isinstance(child, CIf):
                text.block(sub_text)
            elif isinstance(child, CFor):
                text.block(sub_text)
            elif isinstance(child, CWhile):
                text.block(sub_text)
            elif isinstance(child, CDoWhile):
                text.block(sub_text)
            else:
                text.line(sub_text + ';')
        text.shift_tab()
        text.line('}')
        return str(text)

    def generate_return(self, obj):
        if not obj.return_:
            return 'return'
        return 'return {}'.format(self.generate(obj.return_))

    def generate_if(self, obj):
        text = TextHelper()
        if not obj.true_body:
            text.line('if ( !( {} ) )'.format(self.generate(obj.expr)))
        else:
            text.line('if ( {} )'.format(self.generate(obj.expr)))
        if obj.true_body:
            if not isinstance(obj.true_body, CBlock):
                text.tab()
            text.block(self.generate(obj.true_body))
            if not isinstance(obj.true_body, CBlock):
                text.shift_tab()
        if obj.true_body and obj.false_body:
            text.line('else')
        if obj.false_body:
            if not isinstance(obj.false_body, CBlock):
                text.tab()
            text.block(self.generate(obj.false_body))
            if not isinstance(obj.false_body, CBlock):
                text.shift_tab()
        return str(text)

    def generate_for(self, obj):
        text = TextHelper()
        start_text = self.generate(obj.start)
        condition_text = self.generate(obj.condition)
        update_text = self.generate(obj.update)
        text.line('for ( {}; {}; {} )'.format(start_text, condition_text, update_text))
        if not isinstance(obj.body, CBlock):
            text.tab()
        text.block(self.generate(obj.body))
        if not isinstance(obj.body, CBlock):
            text.shift_tab()
        return str(text)

    def generate_while(self, obj):
        text = TextHelper()
        expr_text = self.generate(obj.expr)
        text.line('while ( {} )'.format(expr_text))
        if not isinstance(obj.body, CBlock):
            text.tab()
        text.block(self.generate(obj.body))
        if not isinstance(obj.body, CBlock):
            text.shift_tab()
        return str(text)

    def generate_dowhile(self, obj):
        text = TextHelper()
        expr_text = self.generate(obj.expr)
        text.line('do')
        if not isinstance(obj.body, CBlock):
            text.tab()
        text.block(self.generate(obj.body))
        if not isinstance(obj.body, CBlock):
            text.shift_tab()
        text.line('while ( {} );'.format(expr_text))
        return str(text)

    def generate_break(self, obj):
        return 'break'

    def generate_operation(self, obj):
        if obj.op == '.':
            if obj.a.pointers:
                text = '{}->{}'.format(self.generate(obj.a), self.generate(obj.b))
            else:
                text = '{}.{}'.format(self.generate(obj.a), self.generate(obj.b))
            return text
        elif obj.op == '=':
            if isinstance(obj.a, CDeclaration) and obj.a.name.startswith('_default_'):
                # FUTURE: this is a hack for the only globals I need in C
                return 'static {} = {};'.format(self.generate(obj.a), self.generate(obj.b))
            a_text = self.generate(obj.a)
            b_text = self.generate(obj.b)
            if a_text == b_text:
                return '(void){}'.format(a_text)
            return '{} = {}'.format(a_text, b_text)
        elif obj.op == '&':
            if not obj.a:
                return '&({})'.format(self.generate(obj.b))  # address of
            if not obj.b:
                return '&({})'.format(self.generate(obj.a))  # address of
            # AND operation
            return '({}) & ({})'.format(self.generate(obj.a), self.generate(obj.b))
        elif obj.op == '*':
            if not obj.a:
                return '*({})'.format(self.generate(obj.b))  # dereference pointer
            if not obj.b:
                return '*({})'.format(self.generate(obj.a))  # dereference pointer
            # multiply operation
            return '({}) * ({})'.format(self.generate(obj.a), self.generate(obj.b))
        elif obj.op in ['==', '<', '>', '<=', '>=', '!=', '+', '-', '*', '/', '%', '<<', '>>', '&', '|', '^', '&&', '||']:
            return '({}) {} ({})'.format(self.generate(obj.a), obj.op, self.generate(obj.b))
        elif obj.op in ['++', '--']:
            return '{}{}'.format(self.generate(obj.a), obj.op)
        elif obj.op in ['!', '~']:
            return '{}({})'.format(obj.op, self.generate(obj.a))
        elif obj.op == '[]':
            return '{}[{}]'.format(self.generate(obj.a), self.generate(obj.b))
        elif obj.op == '?':
            return '({}) ? ({}) : ({})'.format(self.generate(obj.a), self.generate(obj.b), self.generate(obj.c))
        raise NotImplementedError("operation '{}'".format(obj.op))

    def generate_typecast(self, obj):
        type_text = self.generate(obj.type_)
        value_text = self.generate(obj.value)
        if obj.value.pointers > 0:
            type_text += ' ' + ('*' * obj.value.pointers)
        if obj.value.const and obj.value.pointers > 0:
            type_text = 'const ' + type_text
        return '(({})({}))'.format(type_text, value_text)

    def generate_function_call(self, obj):
        decl_text = self.generate(obj.decl_variable)
        arg_variables_text = ', '.join([self.generate(arg) for arg in obj.arg_variables])
        return '{}({})'.format(decl_text, arg_variables_text)

    def generate_macro_call(self, obj):
        if len(obj.args) == 0:
            return obj.name
        args_text = ', '.join([self.generate(arg) for arg in obj.args])
        return '{}({})'.format(obj.name, args_text)


class CHeaderGenerator(CGenerator):

    def generate_file(self, obj):
        text = TextHelper()
        header_guard_name = obj.name.upper()
        if self.args.namespace:
            header_guard_name = '{}_{}'.format(self.args.namespace.upper(), header_guard_name)
        header_guard_name = '_H_KPB_{}_C_H_'.format(header_guard_name)
        text.format('#ifndef {}', header_guard_name)
        text.format('#define {}', header_guard_name)

        # filter out globals (look for '=' operation outside of a function)
        children = [c for c in obj.children if not isinstance(c, COperation)]

        text_block = self.generate(children)
        if len(text_block.strip()) > 0:
            text.nl()
            text.block(text_block)
        text.nl()
        text.format('#endif  // {}', header_guard_name)
        return str(text)

    def generate_include(self, obj):
        if obj.private:
            return ''
        return super(CHeaderGenerator, self).generate_include(obj)

    def generate_macro(self, obj):
        if obj.private:
            return ''
        return super(CHeaderGenerator, self).generate_macro(obj)

    def generate_struct(self, obj):
        text = TextHelper()
        text.format('typedef struct {} {}', obj.name, '{')
        text.tab()
        if len(obj.members) == 0:
            text.line('int empty_struct;')
        for memeber in obj.members:
            text.block(self.generate(memeber) + ';')
        text.shift_tab()
        text.format('{} {};', '}', obj.typedef_name)
        return str(text)

    def generate_function(self, obj):
        if obj.static:
            return ''
        decl_text = self.generate(obj.decl_variable)
        if len(obj.arg_variables) == 0:
            arg_variables_text = 'void'
        else:
            arg_variables_text = ', '.join([self.generate(arg) for arg in obj.arg_variables])
        return '{}({});'.format(decl_text, arg_variables_text)


class CSourceGenerator(CGenerator):

    def generate_file(self, obj):
        text = TextHelper()
        text_block = self.generate(obj.children)
        if len(text_block.strip()) > 0:
            text.nl()
            text.block(text_block)
        return str(text)

    def generate_include(self, obj):
        if not obj.private:
            return ''
        return super(CSourceGenerator, self).generate_include(obj)

    def generate_macro(self, obj):
        if not obj.private:
            return ''
        return super(CHeaderGenerator, self).generate_macro(obj)

    def generate_struct(self, obj):
        return ''

    def generate_member(self, obj):
        return self.generate(obj.variable)

    def generate_function(self, obj):
        text = TextHelper()
        static_text = 'static ' if obj.static else ''
        decl_text = self.generate(obj.decl_variable)
        if len(obj.arg_variables) == 0:
            arg_variables_text = 'void'
        else:
            arg_variables_text = ', '.join([self.generate(arg) for arg in obj.arg_variables])
        text.format('{}{}({})', static_text, decl_text, arg_variables_text)
        text.block(self.generate(obj.code_block))
        return str(text)
