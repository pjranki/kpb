import os
import sys

from .. text_helper import TextHelper
from . cpp_stl_compiler import *


class CppStlGenerator(object):

    def generate(self, obj, args=None):
        generate_methods = {
            list: self.generate_list,
            CppStlFile: self.generate_file,
            CppStlInclude: self.generate_include,
            CppStlNamespace: self.generate_namespace,
            CppStlClass: self.generate_class,
            CppStlEnum: self.generate_enum,
            CppStlMethod: self.generate_method,
            CppStlMethodCall: self.generate_method_call,
            CppStlConstructor: self.generate_constructor,
            CppStlConstructorCall: self.generate_constructor_call,
            CppStlDestructor: self.generate_destructor,
            CppStlMacroCall: self.generate_macro_call,
            CppStlTypeVoid: self.generate_type_void,
            CppStlTypeBool: self.generate_type_bool,
            CppStlTypeInteger: self.generate_type_integer,
            CppStlTypeFloat: self.generate_type_float,
            CppStlTypeDouble: self.generate_type_double,
            CppStlTypeString: self.generate_type_string,
            CppStlTypeWString: self.generate_type_wstring,
            CppStlTypeChar: self.generate_type_char,
            CppStlTypeWChar: self.generate_type_wchar,
            CppStlTypeSize: self.generate_type_size,
            CppStlTypeIO: self.generate_type_io,
            CppStlTypeClass: self.generate_type_class,
            CppStlVariable: self.generate_variable,
            CppStlDeclaration: self.generate_declaration,
            CppStlDeclarationClass: self.generate_declaration_class,
            CppStlLiteral: self.generate_literal,
            CppStlMember: self.generate_member,
            CppStlBlock: self.generate_block,
            CppStlTryCatchBlock: self.generate_try_catch_block,
            CppStlLambda: self.generate_lambda,
            CppStlReturn: self.generate_return,
            CppStlIf: self.generate_if,
            CppStlFor: self.generate_for,
            CppStlWhile: self.generate_while,
            CppStlDoWhile: self.generate_dowhile,
            CppStlBreak: self.generate_break,
            CppStlOperation: self.generate_operation,
            CppStlTypecast: self.generate_typecast
        }
        generate_method = generate_methods.get(obj.__class__, None)
        if not generate_method:
            raise NotImplementedError(repr(obj))
        return generate_method(obj)

    def generate_list(self, obj_list):
        text = TextHelper()
        first = True
        for obj in obj_list:
            text_block = self.generate(obj)
            if len(text_block.strip()) > 0:
                if not first:
                    text.nl()
                text.block(text_block)
            first = False
        return str(text)

    def generate_include(self, obj):
        if obj.internal:
            return '#include "{}"'.format(obj.name)
        return '#include <{}>'.format(obj.name)

    def generate_namespace(self, obj):
        text = TextHelper()
        text_block = self.generate(obj.children)
        text.format('namespace {} {}', obj.name, '{')
        if len(text_block.strip()) > 0:
            text.nl()
            text.block(text_block)
        text.nl()
        text.format('{}  // {}', '}', obj.name)
        return str(text)

    def generate_enum(self, obj):
        text = TextHelper()
        text.format('enum {} {}', obj.name, '{')
        text.tab()
        for enum_value in obj.enums:
            value = self.generate(enum_value.value)
            text.format('{} = {},'.format(enum_value.name, value))
        text.shift_tab()
        text.line('};')
        return str(text)

    def generate_type_void(self, obj):
        return 'void'

    def generate_type_bool(self, obj):
        return 'bool'

    def generate_type_integer(self, obj):
        prefix = 'u' if not obj.signed else ''
        return '{}int{}_t'.format(prefix, obj.bits)

    def generate_type_float(self, obj):
        return 'float'

    def generate_type_double(self, obj):
        return 'double'

    def generate_type_string(self, obj):
        return 'std::string'

    def generate_type_wstring(self, obj):
        return 'std::wstring'

    def generate_type_char(self, obj):
        return 'char'

    def generate_type_wchar(self, obj):
        return 'wchar_t'

    def generate_type_size(self, obj):
        return 'size_t'

    def generate_type_io(self, obj):
        return 'kpb::IO'

    def generate_type_class(self, obj):
        return obj.name

    def generate_variable(self, obj):
        return obj.name

    def generate_declaration(self, obj):
        const_text = 'const ' if obj.const else ''
        pointer_text = '*' * obj.pointers
        reference_text = '&' if obj.reference else ''
        name_text = self.generate(obj.variable)
        type_text = self.generate(obj.type_)
        if obj.vector:
            if isinstance(obj.type_, CppStlTypeClass):
                type_text = 'std::reference_wrapper<{}> '.format(type_text)
            type_text = 'std::vector<{}>'.format(type_text)
        return '{}{} {}{}{}'.format(const_text, type_text, reference_text, pointer_text, name_text)

    def generate_declaration_class(self, obj):
        return 'class {};'.format(self.generate(obj.type_))

    def generate_literal(self, obj):
        return obj.text

    def generate_block(self, obj):
        text = TextHelper()
        text.line('{')
        text.tab()
        for child in obj.children:
            sub_text = self.generate(child)
            if isinstance(child, CppStlBlock):
                text.block(sub_text)
            elif isinstance(child, CppStlTryCatchBlock):
                text.block(sub_text)
            elif isinstance(child, CppStlIf):
                text.block(sub_text)
            elif isinstance(child, CppStlFor):
                text.block(sub_text)
            elif isinstance(child, CppStlWhile):
                text.block(sub_text)
            elif isinstance(child, CppStlDoWhile):
                text.block(sub_text)
            else:
                text.line(sub_text + ';')
        text.shift_tab()
        text.line('}')
        return str(text)

    def generate_try_catch_block(self, obj):
        text = TextHelper()
        text.line('try')
        text.block(self.generate(obj.try_body))
        text.line('catch(...)')
        text.block(self.generate(obj.catch_body))
        return str(text)

    def generate_lambda(self, obj):
        text = self.generate(obj.code_block)
        text = '[&] {}'.format(text)
        return text

    def generate_return(self, obj):
        if not obj.return_:
            return 'return'
        return 'return {}'.format(self.generate(obj.return_))

    def generate_if(self, obj):
        text = TextHelper()
        if not obj.true_body:
            text.line('if ( !( {} ) )'.format(self.generate(obj.expr)))
        else:
            text.line('if ( {} )'.format(self.generate(obj.expr)))
        if obj.true_body:
            if not isinstance(obj.true_body, CppStlBlock):
                text.tab()
            text.block(self.generate(obj.true_body))
            if not isinstance(obj.true_body, CppStlBlock):
                text.shift_tab()
        if obj.true_body and obj.false_body:
            text.line('else')
        if obj.false_body:
            if not isinstance(obj.false_body, CppStlBlock):
                text.tab()
            text.block(self.generate(obj.false_body))
            if not isinstance(obj.false_body, CppStlBlock):
                text.shift_tab()
        return str(text)

    def generate_for(self, obj):
        text = TextHelper()
        start_text = self.generate(obj.start)
        condition_text = self.generate(obj.condition)
        update_text = self.generate(obj.update)
        text.line('for ( {}; {}; {} )'.format(start_text, condition_text, update_text))
        if not isinstance(obj.body, CppStlBlock):
            text.tab()
        text.block(self.generate(obj.body))
        if not isinstance(obj.body, CppStlBlock):
            text.shift_tab()
        return str(text)

    def generate_while(self, obj):
        text = TextHelper()
        expr_text = self.generate(obj.expr)
        text.line('while ( {} )'.format(expr_text))
        if not isinstance(obj.body, CppStlBlock):
            text.tab()
        text.block(self.generate(obj.body))
        if not isinstance(obj.body, CppStlBlock):
            text.shift_tab()
        return str(text)

    def generate_dowhile(self, obj):
        text = TextHelper()
        expr_text = self.generate(obj.expr)
        text.line('do')
        if not isinstance(obj.body, CppStlBlock):
            text.tab()
        text.block(self.generate(obj.body))
        if not isinstance(obj.body, CppStlBlock):
            text.shift_tab()
        text.line('while ( {} );'.format(expr_text))
        return str(text)

    def generate_break(self, obj):
        return 'break'

    def generate_operation(self, obj):
        if obj.op == '.':
            if obj.a.pointers:
                text = '{}->{}'.format(self.generate(obj.a), self.generate(obj.b))
            else:
                text = '{}.{}'.format(self.generate(obj.a), self.generate(obj.b))
            return text
        elif obj.op == '=':
            # FUTURE: this is a hack to avoid '=' when creating new C++ objects.
            if isinstance(obj.a, CppStlDeclaration) and isinstance(obj.b, CppStlConstructorCall):
                constructor_args_text = '(' + '('.join(self.generate(obj.b).split('(')[1:])
                if constructor_args_text == '()':
                    constructor_args_text = ''
                return self.generate(obj.a) + constructor_args_text
            a_text = self.generate(obj.a)
            b_text = self.generate(obj.b)
            if a_text == b_text:
                return '(void){}'.format(a_text)
            return '{} = {}'.format(a_text, b_text)
        elif obj.op == '&':
            if not obj.a:
                return '&({})'.format(self.generate(obj.b))  # address of
            if not obj.b:
                return '&({})'.format(self.generate(obj.a))  # address of
            # AND operation
            return '({}) & ({})'.format(self.generate(obj.a), self.generate(obj.b))
        elif obj.op == '*':
            if not obj.a:
                return '*({})'.format(self.generate(obj.b))  # dereference pointer
            if not obj.b:
                return '*({})'.format(self.generate(obj.a))  # dereference pointer
            # multiply operation
            return '({}) * ({})'.format(self.generate(obj.a), self.generate(obj.b))
        elif obj.op in ['==', '<', '>', '<=', '>=', '!=', '+', '-', '*', '/', '%', '<<', '>>', '&', '|', '^', '&&', '||']:
            return '({}) {} ({})'.format(self.generate(obj.a), obj.op, self.generate(obj.b))
        elif obj.op in ['++', '--']:
            return '{}{}'.format(self.generate(obj.a), obj.op)
        elif obj.op in ['!', '~']:
            return '{}({})'.format(obj.op, self.generate(obj.a))
        elif obj.op == '[]':
            return '{}[{}]'.format(self.generate(obj.a), self.generate(obj.b))
        elif obj.op == '?':
            return '({}) ? ({}) : ({})'.format(self.generate(obj.a), self.generate(obj.b), self.generate(obj.c))
        raise NotImplementedError("operation '{}'".format(obj.op))

    def generate_typecast(self, obj):
        type_text = self.generate(obj.type_)
        value_text = self.generate(obj.value)
        if obj.value.pointers > 0:
            type_text += ' ' + ('*' * obj.value.pointers)
        if obj.value.const and obj.value.pointers > 0:
            type_text = 'const ' + type_text
        if isinstance(obj.type_, CppStlTypeClass):
            cast_test = 'reinterpret_cast'
        elif isinstance(obj.type_, CppStlTypeVoid) and obj.value.pointers > 0:
            cast_test = 'reinterpret_cast'
        else:
            cast_test = 'static_cast'
        return '{}<{}>({})'.format(cast_test, type_text, value_text)

    def generate_method_call(self, obj):
        arg_variables_text = ', '.join([self.generate(arg) for arg in obj.arg_variables])
        method_name = obj.decl_variable.name
        if obj.static:
            if isinstance(obj.class_type, CppStlTypeIO):
                method_name = 'kpb::{}'.format(method_name)
            else:
                raise NotImplementedError(obj.class_type)
        return '{}({})'.format(method_name, arg_variables_text)

    def generate_constructor_call(self, obj):
        arg_variables_text = ', '.join([self.generate(arg) for arg in obj.arg_variables])
        return '{}({})'.format(obj.class_type.name, arg_variables_text)

    def generate_macro_call(self, obj):
        args_text = ', '.join([self.generate(arg) for arg in obj.args])
        return '{}({})'.format(obj.name, args_text)


class CppStlHeaderGenerator(CppStlGenerator):

    def generate_file(self, obj):
        text = TextHelper()
        text.line('#pragma once')
        text_block = self.generate(obj.children)
        if len(text_block.strip()) > 0:
            text.nl()
            text.block(text_block)
        return str(text)

    def generate_include(self, obj):
        if obj.private:
            return ''
        return super(CppStlHeaderGenerator, self).generate_include(obj)

    def generate_class(self, obj):
        public = list()
        private = list()
        if obj.constructor.public:
            public.append(obj.constructor)
        else:
            private.append(obj.constructor)
        if obj.destructor.public:
            public.append(obj.destructor)
        else:
            private.append(obj.destructor)
        public.extend([enum for enum in obj.enums])
        public.extend([method for method in obj.methods if method.public])
        private.extend([method for method in obj.methods if not method.public])
        public.extend([member for member in obj.members if member.public])
        private.extend([member for member in obj.members if not member.public])
        text = TextHelper()
        text.format('class {} {}', obj.name, '{')
        text.tab()
        if len(public) > 0:
            text.line('public:')
            text.tab()
            text.block(self.generate(public))
            text.shift_tab()
        if len(private) > 0:
            text.line('private:')
            text.tab()
            text.block(self.generate(private))
            text.shift_tab()
        text.shift_tab()
        text.line('};')
        return str(text)

    def generate_method(self, obj):
        decl_text = self.generate(obj.decl_variable)
        arg_variables_text = ', '.join([self.generate(arg) for arg in obj.arg_variables])
        const_text = ' const' if obj.const else ''
        static_text = 'static ' if obj.static else ''
        return '{}{}({}){};'.format(static_text, decl_text, arg_variables_text, const_text)

    def generate_constructor(self, obj):
        # delete copy constructor & assignment operator as well
        text = TextHelper()
        text.line('{}();'.format(obj.class_type.name))
        text.line('{}(const {} &) = delete;'.format(obj.class_type.name, obj.class_type.name))
        text.line('{} &operator=(const {} &) = delete;'.format(obj.class_type.name, obj.class_type.name))
        return str(text)

    def generate_destructor(self, obj):
        return '~{}();'.format(obj.class_type.name)

    def generate_member(self, obj):
        return '{};'.format(self.generate(obj.variable))


class CppStlSourceGenerator(CppStlGenerator):

    def generate_file(self, obj):
        text = TextHelper()
        text_block = self.generate(obj.children)
        if len(text_block.strip()) > 0:
            text.nl()
            text.block(text_block)
        return str(text)

    def generate_include(self, obj):
        if not obj.private:
            return ''
        return super(CppStlSourceGenerator, self).generate_include(obj)

    def generate_class(self, obj):
        methods = list()
        methods.append(obj.constructor)
        methods.append(obj.destructor)
        methods.extend(obj.methods)
        return self.generate(methods)

    def generate_method(self, obj):
        text = TextHelper()
        decl_variable = CppStlVariable()
        decl_variable.name =  '{}::{}'.format(obj.class_type.name, obj.decl_variable.name)
        decl_variable.type_ = obj.decl_variable.type_
        decl_variable.const = obj.decl_variable.const
        decl_variable.pointers = obj.decl_variable.pointers
        decl_variable.reference = obj.decl_variable.reference
        decl_variable.vector = obj.decl_variable.vector
        decl_variable = CppStlDeclaration(decl_variable)
        decl_text = self.generate(decl_variable)
        arg_variables_text = ', '.join([self.generate(arg) for arg in obj.arg_variables])
        const_text = ' const' if obj.const else ''
        static_text = 'static ' if obj.static else ''
        text.format('{}{}({}){}', static_text, decl_text, arg_variables_text, const_text)
        text.block(self.generate(obj.code_block))
        return str(text)

    def generate_constructor(self, obj):
        text = TextHelper()
        text.format('{}::{}()', obj.class_type.name, obj.class_type.name)
        text.block(self.generate(obj.code_block))
        return str(text)

    def generate_destructor(self, obj):
        text = TextHelper()
        text.format('{}::~{}()', obj.class_type.name, obj.class_type.name)
        text.block(self.generate(obj.code_block))
        return str(text)

    def generate_member(self, obj):
        return self.generate(obj.variable)
