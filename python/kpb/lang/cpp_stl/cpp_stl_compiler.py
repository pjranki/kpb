import os
import sys

from .. text_helper import to_camel_case
from .. kil.kil_compiler import *

CAMEL_CASE_METHODS = [
    'clear',
    'copy_from',
    'parse_from_string',
    'parse_from_string_or_fill',
    'serialize_to_string',
    'read',
    'write',
    'update',
    'size',
    'randomize',
]


class CppStlFile(object):

    def __init__(self):
        self.name = None
        self.input_file_path = None
        self.children = list()


class CppStlInclude(object):

    def __init__(self):
        self.name = None
        self.internal = False
        self.private = True


class CppStlNamespace(object):

    def __init__(self):
        self.name = None
        self.children = list()


class CppStlClass(object):

    def __init__(self):
        self.name = None
        self.constructor = None
        self.destructor = None
        self.members = list()
        self.methods = list()


class CppStlEnum(object):

    def __init__(self, class_type=None, name=None):
        self.class_type = class_type
        self.name = name
        self.enums = list()


class CppStlEnumValue(object):

    def __init__(self, enum=None, name=None, value=None):
        self.enum = enum
        self.name = name
        self.value = value


class CppStlMethod(object):

    def __init__(self):
        self.class_type = None
        self.decl_variable = None
        self.arg_variables = list()
        self.const = False
        self.public = True
        self.static = False
        self.code_block = None


class CppStlMethodCall(object):

    def __init__(self, decl_variable=None, arg_variables=None):
        self.class_type = None
        self.static = False
        self.decl_variable = decl_variable
        self.arg_variables = arg_variables if arg_variables else list()

    @property
    def name(self):
        return self.decl_variable.name

    @property
    def type_(self):
        return self.decl_variable.type_

    @property
    def const(self):
        return self.decl_variable.const

    @property
    def pointers(self):
        return self.decl_variable.pointers

    @property
    def reference(self):
        return self.decl_variable.reference


class CppStlConstructor(object):

    def __init__(self):
        self.class_type = None
        self.arg_variables = list()
        self.public = True
        self.code_block = None


class CppStlConstructorCall(object):

    def __init__(self, class_type=None, arg_variables=None):
        self.class_type = class_type
        self.arg_variables = arg_variables if arg_variables else list()

    @property
    def const(self):
        return True

    @property
    def pointers(self):
        return 0

    @property
    def reference(self):
        return False


class CppStlDestructor(object):

    def __init__(self):
        self.class_type = None
        self.public = True
        self.code_block = None


class CppStlMacroCall(object):

    def __init__(self, name=None, *args):
        self.name = name
        self.args = list(args)

    @property
    def const(self):
        for arg in self.args:
            if hasattr(arg, 'const'):
                return arg.const
        return True

    @property
    def pointers(self):
        if self.name.startswith('KPB_CPP_STL') and self.name.endswith('_GET'):
            return 0
        for arg in self.args:
            if hasattr(arg, 'pointers'):
                return arg.pointers
        return 0


class CppStlTypeVoid(object):
    pass


class CppStlTypeBool(object):
    pass


class CppStlTypeInteger(object):

    def __init__(self):
        self.signed = True
        self.bits = 32


class CppStlTypeFloat(object):

    def __init__(self):
        pass


class CppStlTypeDouble(object):

    def __init__(self):
        pass


class CppStlTypeString(object):

    @property
    def name(self):
        return 'std::string'


class CppStlTypeWString(object):

    @property
    def name(self):
        return 'std::wstring'


class CppStlTypeChar(object):

    @property
    def name(self):
        return 'char'


class CppStlTypeWChar(object):

    @property
    def name(self):
        return 'wchar_t'


class CppStlTypeSize(object):
    pass


class CppStlTypeIO(object):

    @property
    def name(self):
        return 'kpb::IO'


class CppStlTypeClass(object):

    def __init__(self, name=None):
        self.name = name


class CppStlVariable(object):

    def __init__(self, name=None, type_=None, const=False):
        self.name = name
        self.type_ = type_ if type_ else CppStlTypeVoid()
        self.const = const
        self.pointers = 0
        self.reference = False
        self.vector = False


class CppStlDeclaration(object):

    def __init__(self, variable=None):
        self.variable = variable

    @property
    def name(self):
        return self.variable.name

    @property
    def type_(self):
        return self.variable.type_

    @property
    def const(self):
        return self.variable.const

    @property
    def pointers(self):
        return self.variable.pointers

    @property
    def reference(self):
        return self.variable.reference

    @property
    def vector(self):
        return self.variable.vector


class CppStlDeclarationClass(object):

    def __init__(self, class_type=None):
        self.class_type = class_type

    @property
    def type_(self):
        return self.class_type

    @property
    def name(self):
        self.type_.name


class CppStlLiteral(object):

    def __init__(self, text):
        self.text = text

    @property
    def const(self):
        return True

    @property
    def pointers(self):
        return 0


class CppStlMember(object):

    def __init__(self):
        self.class_type = None
        self.variable = None
        self.public = False

    @property
    def name(self):
        return self.variable.name

    @property
    def type_(self):
        return self.variable.type_

    @property
    def const(self):
        return self.variable.const

    @property
    def pointers(self):
        return self.variable.pointers

    @property
    def reference(self):
        return self.variable.reference


class CppStlBlock(object):

    def __init__(self, children=None):
        self.children = children if children else list()


class CppStlTryCatchBlock(object):

    def __init__(self, try_body=None, catch_body=None):
        self.try_body = try_body
        self.catch_body = catch_body


class CppStlLambda(object):

    def __init__(self, code_block=None):
        self.code_block = code_block


class CppStlReturn(object):

    def __init__(self, return_=None):
        self.return_ = return_

    @property
    def type_(self):
        return self.return_.type_

    @property
    def const(self):
        return self.return_.const

    @property
    def pointers(self):
        return self.return_.pointers

    @property
    def reference(self):
        return self.return_.reference


class CppStlIf(object):

    def __init__(self, expr=None, true_body=None, false_body=None):
        self.expr = expr
        self.true_body = true_body
        self.false_body = false_body


class CppStlFor(object):

    def __init__(self, start=None, condition=None, update=None, body=None):
        self.start = start
        self.condition = condition
        self.update = update
        self.body = body


class CppStlWhile(object):

    def __init__(self, expr=None, body=None):
        self.expr = expr
        self.body = body


class CppStlDoWhile(object):

    def __init__(self, expr=None, body=None):
        self.expr = expr
        self.body = body


class CppStlBreak(object):

    def __init__(self):
        pass


class CppStlOperation(object):

    def __init__(self, a=None, op=None, b=None, c=None):
        self.a = a
        self.op = op
        self.b = b
        self.c = c

    @property
    def type_(self):
        if self.op == '.':
            return self.b.type_
        return self.a.type_

    @property
    def const(self):
        if self.op == '.':
            return self.b.const
        return self.a.const

    @property
    def pointers(self):
        if self.op == '.':
            return self.b.pointers
        if self.op == '&' and self.b is None:
            return self.a.pointers + 1
        return self.a.pointers

    @property
    def reference(self):
        if self.op == '.':
            return self.b.reference
        return self.a.reference


class CppStlTypecast(object):

    def __init__(self, value=None, type_=None):
        self.value = value
        self.type_ = type_

    @property
    def const(self):
        return self.value.const

    @property
    def pointers(self):
        return self.value.pointers


class CppStlCompiler(object):

    def __init__(self, header_ext='.hpp'):
        self.header_ext = header_ext
        self.all_class_types = list()
        self.finally_method_scope_unique = 0

    def compile(self, kil_obj, args=None):
        compile_methods = {
            list: self.compile_list,
            KilFile: self.compile_file,
            KilImport: self.compile_import,
            KilNamespace: self.compile_namespace,
            KilClass: self.compile_class,
            KilEnum: self.compile_enum,
            KilMethod: self.compile_method,
            KilMethodCall: self.compile_method_call,
            KilConstructor: self.compile_constructor,
            KilConstructorCall: self.compile_constructor_call,
            KilDestructor: self.compile_destructor,
            KilTypeNone: self.compile_type_none,
            KilTypeBoolean: self.compile_type_boolean,
            KilTypeInteger: self.compile_type_integer,
            KilTypeFloat: self.compile_type_float,
            KilTypeString: self.compile_type_string,
            KilTypeChar: self.compile_type_char,
            KilTypeBuffer: self.compile_type_buffer,
            KilTypeSize: self.compile_type_size,
            KilTypeIO: self.compile_type_io,
            KilTypeClass: self.compile_type_class,
            KilVariable: self.compile_variable,
            KilDeclaration: self.compile_declaration,
            KilEnumValue: self.compile_enum_value,
            KilLiteral: self.compile_literal,
            KilMember: self.compile_member,
            KilBlock: self.compile_block,
            KilTryCatchBlock: self.compile_try_catch_block,
            KilReturn: self.compile_return,
            KilIf: self.compile_if,
            KilFor: self.compile_for,
            KilWhile: self.compile_while,
            KilDoWhile: self.compile_dowhile,
            KilBreak: self.compile_break,
            KilOperation: self.compile_operation,
            KilTypecast: self.compile_typecast,
        }
        compile_method = compile_methods.get(kil_obj.__class__, None)
        if not compile_method:
            raise NotImplementedError(repr(kil_obj))
        return compile_method(kil_obj)

    def compile_list(self, kil_obj_list):
        obj_list = list()
        for kil_obj in kil_obj_list:
            obj = self.compile(kil_obj)
            if not obj:
                continue
            if isinstance(obj, list):
                obj_list.extend(obj)
            else:
                obj_list.append(obj)
        return obj_list

    def compile_file(self, kil_file):
        self.all_class_types = list()
        obj = CppStlFile()
        obj.input_file_name = kil_file.input_file_name
        obj.name = kil_file.name
        include = CppStlInclude()
        include.name = obj.name + self.header_ext
        include.internal = True
        include.private = True
        obj.children.append(include)
        include = CppStlInclude()
        include.name = 'kpb_cpp_stl_io' + self.header_ext
        include.internal = False
        include.private = False
        obj.children.append(include)
        obj.children.extend(self.compile(kil_file.children))
        for class_type in self.all_class_types:
            obj.children.append(CppStlDeclarationClass(class_type=class_type))
        # put includes and declarations at top of file
        includes = [include for include in obj.children if isinstance(include, CppStlInclude)]
        declarations = [declaration for declaration in obj.children if isinstance(declaration, CppStlDeclarationClass)]
        children = [child for child in obj.children if child not in includes and child not in declarations]
        obj.children = list()
        obj.children.extend(includes)
        obj.children.extend(declarations)
        obj.children.extend(children)
        self.all_class_types = list()
        return obj

    def compile_import(self, kil_import):
        include = CppStlInclude()
        include.name = kil_import.name + self.header_ext
        include.internal = not kil_import.external
        include.private = not kil_import.public
        return include

    def compile_namespace(self, kil_namespace):
        self.all_class_types = list()
        obj = CppStlNamespace()
        obj.name = kil_namespace.name
        obj.children.extend(self.compile(kil_namespace.children))
        for class_type in self.all_class_types:
            obj.children.append(CppStlDeclarationClass(class_type=class_type))
        # put declarations at top of namespace
        declarations = [declaration for declaration in obj.children if isinstance(declaration, CppStlDeclarationClass)]
        children = [child for child in obj.children if not isinstance(child, CppStlDeclarationClass)]
        obj.children = list()
        obj.children.extend(declarations)
        obj.children.extend(children)
        self.all_class_types = list()
        return obj

    def compile_class(self, kil_class):
        obj = CppStlClass()
        obj.name = to_camel_case(kil_class.name)
        obj.enums = self.compile(kil_class.enums)
        obj.constructor = self.compile(kil_class.constructor)
        obj.destructor = self.compile(kil_class.destructor)
        obj.members = self.compile(kil_class.members)
        obj.methods = self.compile(kil_class.methods)
        # pointers must be set to nullptr in constructor so macros will not cause crashes
        this_variable = CppStlVariable()
        this_variable.name = 'this'
        this_variable.type_ = CppStlTypeClass(name=obj.name)
        this_variable.const = False
        this_variable.pointers = 1
        this_variable.reference = False
        this_variable.vector = False
        for member in obj.members:
            if member.pointers > 0:
                member_variable = CppStlOperation(this_variable, '.', member.variable.variable)
                set_to_nullptr = CppStlOperation(member_variable, '=', CppStlLiteral('nullptr'))
                obj.constructor.code_block.children.insert(0, set_to_nullptr)
        return obj

    def compile_enum(self, kil_enum):
        enum = CppStlEnum()
        enum.class_type = self.compile(kil_enum.class_type)
        enum.name = to_camel_case(kil_enum.name)
        for kil_enum_value in kil_enum.enums:
            enum_value = CppStlEnumValue()
            enum_value.name = '{}_{}'.format(kil_enum.name, kil_enum_value.name).upper()
            enum_value.value = CppStlLiteral('{}'.format(kil_enum_value.value))
            enum.enums.append(enum_value)
        return enum

    def compile_method(self, kil_method):
        self.finally_method_scope_unique = 0  # reset, try-catch-finally scoped to method/function
        obj = CppStlMethod()
        obj.class_type = self.compile(kil_method.class_type)
        obj.decl_variable = self.compile(kil_method.decl_variable)
        if obj.decl_variable.variable.name in CAMEL_CASE_METHODS and not isinstance(kil_method.class_type, KilTypeIO):
            obj.decl_variable.variable.name = to_camel_case(obj.decl_variable.variable.name)
        obj.arg_variables = self.compile(kil_method.arg_variables)
        obj.const = not kil_method.mutable
        obj.public = kil_method.public
        obj.static = kil_method.class_method
        obj.code_block = self.compile(kil_method.code_block)
        if len(obj.arg_variables) == 1 and obj.arg_variables[0].variable.name == '_io':
            # _io is not always use as an arg, we need to assign it to itself to hide unused warnings
            io_variable = obj.arg_variables[0].variable
            obj.code_block.children.insert(0, CppStlLiteral('(void)(_io)'))
        if len(obj.arg_variables) == 1 and obj.arg_variables[0].variable.name == '_other':
            # _other is not always use as an arg, we need to assign it to itself to hide unused warnings
            other_variable = obj.arg_variables[0].variable
            obj.code_block.children.insert(0, CppStlLiteral('(void)(_other)'))
        obj = self.compile_method_null_checks(obj)
        if obj.decl_variable.variable.name.endswith('_size_at'):
            obj.name = obj.decl_variable.variable.name[:-len('_at')]
        if obj.decl_variable.variable.name.endswith('_at'):
            obj.decl_variable.variable.name = obj.decl_variable.variable.name[:-len('_at')]
        return obj

    def compile_method_call(self, kil_method_call):
        obj = CppStlMethodCall()
        obj.class_type = self.compile(kil_method_call.class_type)
        obj.static = kil_method_call.class_method
        obj.decl_variable = self.compile(kil_method_call.decl_variable)
        if obj.decl_variable.name in CAMEL_CASE_METHODS and not isinstance(kil_method_call.class_type, KilTypeIO):
            obj.decl_variable.name = to_camel_case(obj.decl_variable.name)
        obj.arg_variables = self.compile(kil_method_call.arg_variables)
        if obj.decl_variable.name.endswith('_size_at'):
            obj.decl_variable.name = obj.decl_variable.name[:-len('_at')]
        if obj.decl_variable.name.endswith('_at'):
            obj.decl_variable.name = obj.decl_variable.name[:-len('_at')]
        if obj.decl_variable.name == 'getter':
            if isinstance(obj.arg_variables[0], CppStlOperation):
                if isinstance(obj.arg_variables[0].b, CppStlVariable):
                    if isinstance(obj.arg_variables[0].b.type_, CppStlTypeClass):
                        obj.arg_variables[0] = CppStlMacroCall('KPB_CPP_STL_OBJECT_GET', obj.arg_variables[0].type_, obj.arg_variables[0])
        return obj

    def compile_constructor(self, kil_constructor):
        obj = CppStlConstructor()
        obj.class_type = self.compile(kil_constructor.class_type)
        obj.arg_variables = self.compile(kil_constructor.arg_variables)
        obj.public = kil_constructor.public
        obj.code_block = self.compile(kil_constructor.code_block)
        obj = self.compile_method_null_checks(obj)
        return obj

    def compile_constructor_call(self, kil_constructor_call):
        obj = CppStlConstructorCall()
        obj.class_type = self.compile(kil_constructor_call.class_type)
        obj.arg_variables = self.compile(kil_constructor_call.arg_variables)
        return obj

    def compile_destructor(self, kil_destructor):
        obj = CppStlDestructor()
        obj.class_type = self.compile(kil_destructor.class_type)
        obj.public = kil_destructor.public
        obj.code_block = self.compile(kil_destructor.code_block)
        return obj

    def compile_default_literal_from_variable(self, variable):
        if isinstance(variable.type_, CppStlTypeVoid):
            if variable.pointers > 0:
                if variable.const:
                    return CppStlLiteral('""')
                return CppStlLiteral('nullptr')
            else:
                return None
        elif isinstance(variable.type_, CppStlTypeBool):
            if variable.pointers > 0:
                return CppStlLiteral('nullptr')
            else:
                return CppStlLiteral('false')
        elif isinstance(variable.type_, CppStlTypeInteger):
            if variable.pointers > 0:
                return CppStlLiteral('nullptr')
            else:
                return CppStlLiteral('0')
        elif isinstance(variable.type_, CppStlTypeString):
            if variable.pointers > 0:
                if variable.const:
                    return CppStlLiteral('""')
                return CppStlLiteral('nullptr')
            else:
                return CppStlLiteral("'\\0'")
        elif isinstance(variable.type_, CppStlTypeClass):
            if variable.pointers > 0:
                if variable.const:
                    raise NotImplementedError('implement a default (static) instance of a class')
                return CppStlLiteral('nullptr')
            else:
                return CppStlLiteral('{0}')
        else:
            raise NotImplementedError("default literal for '{}'".format(variable.type_))

    def compile_method_null_checks(self, obj):
        assert isinstance(obj, CppStlMethod) or isinstance(obj, CppStlConstructor)
        arg_variables_reversed = obj.arg_variables[:]
        arg_variables_reversed.reverse()
        for decl_arg_variable in arg_variables_reversed:
            arg_variable = decl_arg_variable.variable
            if arg_variable.pointers == 0:
                continue
            obj.code_block.children.insert(0, CppStlIf(expr=arg_variable, false_body=CppStlBlock([
                CppStlReturn(self.compile_default_literal_from_variable(obj.decl_variable))
            ])))
        return obj

    def compile_type_none(self, kil_type_none):
        return CppStlTypeVoid()

    def compile_type_boolean(self, kil_type_boolean):
        return CppStlTypeBool()

    def compile_type_integer(self, kil_type):
        obj = CppStlTypeInteger()
        obj.signed = kil_type.signed
        obj.bits = kil_type.bits
        if obj.bits <= 8:
            obj.bits = 8
        elif obj.bits <= 16:
            obj.bits = 16
        elif obj.bits <= 32:
            obj.bits = 32
        elif obj.bits <= 64:
            obj.bits = 64
        else:
            raise NotImplementedError('integers with more than 64 bits ({} bits)'.format(obj.bits))
        return obj

    def compile_type_float(self, kil_type):
        if kil_type.bits == 32:
            return CppStlTypeFloat()
        if kil_type.bits == 64:
            return CppStlTypeDouble()
        raise NotImplementedError('floats with {} bits'.format(kil_type.bits))

    def compile_type_string(self, kil_type_string):
        if kil_type_string.wide:
            return CppStlTypeWString()
        return CppStlTypeString()

    def compile_type_char(self, kil_type_char):
        if kil_type_char.wide:
            return CppStlTypeWChar()
        return CppStlTypeChar()

    def compile_type_buffer(self, kil_type_buffer):
        return CppStlTypeString()

    def compile_type_size(self, kil_type):
        return CppStlTypeSize()

    def compile_type_io(self, kil_type_io):
        return CppStlTypeIO()

    def compile_type_class(self, kil_type):
        type_ = CppStlTypeClass(name=to_camel_case(kil_type.name))
        if kil_type.external:
            type_.name = '::{}'.format(type_.name)
        else:
            if type_.name not in [t.name for t in self.all_class_types]:
                self.all_class_types.append(type_)
        return type_

    def compile_variable(self, kil_variable):
        variable = CppStlVariable()
        variable.name = kil_variable.name
        variable.type_ = self.compile(kil_variable.type_)
        variable.reference = kil_variable.reference
        variable.vector = kil_variable.list_
        if variable.name == KilVariable.THIS_NAME:
            variable.name = 'this'
            variable.pointers = 1
            variable.reference = False
        elif variable.reference and isinstance(kil_variable.type_, KilTypeNone):
            variable.pointers = 1
            variable.reference = False
        if variable.reference and kil_variable.mutable and not isinstance(kil_variable.type_, KilTypeIO):
            variable.pointers += 1
            variable.reference = False
        if not kil_variable.reference:
            variable.const = False
        else:
            variable.const = not kil_variable.mutable
        return variable

    def compile_declaration(self, kil_declaration):
        declaration = CppStlDeclaration()
        declaration.variable = self.compile(kil_declaration.variable)
        return declaration

    def compile_enum_value(self, kil_enum_value):
        class_name = self.compile(kil_enum_value.enum.class_type).name
        enum_name = to_camel_case(kil_enum_value.enum.name)
        enum_value_name = '{}_{}'.format(kil_enum_value.enum.name, kil_enum_value.name).upper()
        return CppStlLiteral('{}::{}::{}'.format(class_name, enum_name, enum_value_name))

    def compile_literal(self, literal):
        if isinstance(literal.type_, KilTypeNone):
            return CppStlLiteral('nullptr')
        elif isinstance(literal.type_, KilTypeBoolean):
            return CppStlLiteral('true' if literal.value else 'false')
        elif isinstance(literal.type_, KilTypeInteger):
            return CppStlLiteral('{}'.format(literal.value))
        elif isinstance(literal.type_, KilTypeFloat):
            return CppStlLiteral('{}'.format(literal.value))
        elif isinstance(literal.type_, KilTypeString):
            if literal.type_.wide:
                return CppStlLiteral('L"{}"'.format(literal.value))
            return CppStlLiteral('"{}"'.format(literal.value))
        elif isinstance(literal.type_, KilTypeChar):
            if literal.type_.wide:
                return CppStlLiteral('((wchar_t)0x{:04})'.format(literal.value))
            return CppStlLiteral('((char)0x{:02})'.format(literal.value))
        elif isinstance(literal.type_, KilTypeSize):
            return CppStlLiteral('{}'.format(literal.value))
        elif isinstance(literal.type_, KilTypeBuffer):
            if literal.value is None:
                return CppStlLiteral('nullptr')
            assert isinstance(literal.value, bytes)
            return CppStlLiteral('std::string("{}", {})'.format(''.join(['\\x{:02x}'.format(v) for v in literal.value]), len(literal.value)))
        elif isinstance(literal.type_, KilTypeIO):
            return CppStlLiteral('{0}')
        elif isinstance(literal.type_, KilTypeClass):
            return CppStlLiteral('{0}')
        else:
            raise NotImplementedError('literal type {}'.format(repr(literal.type_)))

    @classmethod
    def to_member_variable(cls, variable):
        assert isinstance(variable, CppStlVariable)
        variable.name += '_'
        if not variable.vector and isinstance(variable.type_, CppStlTypeClass):
            variable.pointers = 1
        return variable

    def compile_member(self, kil_member):
        member = CppStlMember()
        member.class_type = self.compile(kil_member.class_type)
        member.public = kil_member.public
        member.variable = self.compile(kil_member.variable)
        member.variable.variable = self.to_member_variable(member.variable.variable)
        return member

    def compile_block(self, kil_block):
        block = CppStlBlock()
        block.children = self.compile(kil_block.children)
        return block

    def compile_try_catch_block(self, kil_try_catch_block):
        assert kil_try_catch_block.try_body
        try_body = self.compile(kil_try_catch_block.try_body)
        if kil_try_catch_block.catch_body:
            raise NotImplementedError("catch body")
        catch_body = CppStlBlock()
        if kil_try_catch_block.rethrow:
            # no try/catch block needed
            try_catch_block = try_body
        else:
            # need try/catch block to squash exceptions
            try_catch_block = CppStlTryCatchBlock()
            try_catch_block.try_body = try_body
            try_catch_block.catch_body = catch_body
        if kil_try_catch_block.final_body:
            new_children = list()
            # add the final/lambda (to call the final block on function return/exception)
            final = CppStlVariable()
            final.name = '_finally{}'.format(self.finally_method_scope_unique)
            self.finally_method_scope_unique += 1
            final.type_ = CppStlTypeClass(name='kpb::Finally')
            final.const = False
            final.pointers = 0
            final.reference = False
            final.vector = False
            final_constructor_call = CppStlConstructorCall()
            final_constructor_call.class_type = final.type_
            lambda_ = CppStlLambda()
            lambda_.code_block = self.compile(kil_try_catch_block.final_body)
            final_constructor_call.arg_variables = [lambda_]
            new_children.append(CppStlOperation(CppStlDeclaration(final), '=', final_constructor_call))
            # add try block code AFTER final/lambda
            new_children.extend(try_body.children)
            try_body.children = new_children
        return try_catch_block

    def compile_return(self, kil_return):
        return_ = CppStlReturn()
        if not kil_return.return_:
            return return_
        return_.return_ = self.compile(kil_return.return_)
        if kil_return.list_:
            if isinstance(kil_return.type_, KilTypeClass):
                return_.return_ = CppStlMacroCall('KPB_CPP_STL_LIST_REF_GET', return_.type_, return_.return_)
            else:
                return_.return_ = CppStlMacroCall('KPB_CPP_STL_LIST_GET', return_.type_, return_.return_)
            return return_
        if isinstance(kil_return.return_.type_, KilTypeClass) and not kil_return.return_.mutable:
            if isinstance(return_.return_, CppStlVariable) and return_.return_.name == 'this':
                return_.return_ = CppStlOperation(return_.return_, '*')
                return return_
            if not isinstance(return_.return_, CppStlMacroCall) and not isinstance(return_.return_, CppStlLiteral):
                if not isinstance(kil_return.return_, KilVariable):
                    if not (isinstance(kil_return.return_.a, KilOperation) and kil_return.return_.a.b.list_):
                        if not isinstance(return_.return_.b, CppStlMethodCall):
                            return_.return_ = CppStlMacroCall('KPB_CPP_STL_OBJECT_GET', return_.type_, return_.return_)
                            return return_
        if isinstance(kil_return.return_.type_, KilTypeNone) and not kil_return.return_.mutable and return_.return_.pointers == 1:
            if isinstance(kil_return.return_, KilTypecast):
                typecast = return_.return_
                return_.return_.value = CppStlOperation(CppStlMacroCall('KPB_CPP_STL_OBJECT_GET', typecast.value.type_, typecast.value), '&')
                return return_
        if isinstance(kil_return.return_, KilLiteral):
            if isinstance(kil_return.return_.type_, KilTypeString) or \
                    isinstance(kil_return.return_.type_, KilTypeBuffer) or \
                    isinstance(kil_return.return_.type_, KilTypeClass):
                # HACK: fix compiler error 'returning address of local variable'
                type_ = self.compile(kil_return.return_.type_)
                declare_default = CppStlLiteral('static const {} _default'.format(type_.name))
                return_.return_ = CppStlLiteral('_default')
                return [declare_default, return_]
        return return_

    def compile_if(self, kil_if):
        if_ = CppStlIf()
        if_.expr = self.compile(kil_if.expr)
        if kil_if.true_body:
            if_.true_body = self.compile(kil_if.true_body)
        if kil_if.false_body:
            if_.false_body = self.compile(kil_if.false_body)
        return if_

    def compile_for(self, kil_for):
        for_ = CppStlFor()
        for_.start = self.compile(kil_for.start)
        for_.condition = self.compile(kil_for.condition)
        for_.update = self.compile(kil_for.update)
        for_.body = self.compile(kil_for.body)
        return for_

    def compile_while(self, kil_while):
        while_ = CppStlWhile()
        while_.expr = self.compile(kil_while.expr)
        while_.body = self.compile(kil_while.body)
        return while_

    def compile_dowhile(self, kil_dowhile):
        dowhile_ = CppStlDoWhile()
        dowhile_.expr = self.compile(kil_dowhile.expr)
        dowhile_.body = self.compile(kil_dowhile.body)
        return dowhile_

    def compile_break(self, kil_break):
        return CppStlBreak()

    def compile_operation(self, kil_operation):
        if kil_operation.op == '.' and isinstance(kil_operation.b, KilDestructorCall):
            # don't call destruction in C++ (compiler will handle it for us)
            return None
        if kil_operation.op == 'clr':
            if isinstance(kil_operation.a.type_, KilTypeClass):
                return CppStlMacroCall('KPB_CPP_STL_LIST_DELETE_CLEAR', self.compile(kil_operation.a.type_), self.compile(kil_operation.a))
            return CppStlMacroCall('KPB_CPP_STL_LIST_CLEAR', self.compile(kil_operation.a.type_), self.compile(kil_operation.a))
        if kil_operation.op == 'len':
            if isinstance(kil_operation.a.type_, KilTypeString) or isinstance(kil_operation.a.type_, KilTypeBuffer):
                kil_method_call = KilMethodCall()
                kil_method_call.class_type = kil_operation.a.type_
                kil_method_call.mutable = False
                kil_method_call.public = True
                kil_method_call.decl_variable = KilVariable()
                kil_method_call.decl_variable.name = 'size'
                kil_method_call.decl_variable.type_ = KilTypeSize()
                kil_method_call.decl_variable.mutable = False
                kil_method_call.decl_variable.reference = False
                kil_method_call.decl_variable.list_ = False
                kil_method_call.arg_variables = list()
                kil_operation.op = '.'
                kil_operation.b = kil_method_call
                method_call = self.compile(kil_operation)
                method_call.b.decl_variable.name = 'size'  # must be lowercase 'size'
                return method_call
            return CppStlMacroCall('KPB_CPP_STL_LIST_SIZE', self.compile(kil_operation.a.type_), self.compile(kil_operation.a))
        if kil_operation.op == 'add':
            if isinstance(kil_operation.a.type_, KilTypeClass):
                return CppStlMacroCall('KPB_CPP_STL_LIST_ADD_NEW', self.compile(kil_operation.a.type_), self.compile(kil_operation.a))
            return CppStlMacroCall('KPB_CPP_STL_LIST_ADD', self.compile(kil_operation.a.type_), self.compile(kil_operation.a), self.compile(kil_operation.b))
        if kil_operation.op == '=' and isinstance(kil_operation.a.type_, KilTypeClass) and not kil_operation.a.type_.external and isinstance(kil_operation.b, KilConstructorCall):
            return CppStlMacroCall('KPB_CPP_STL_OBJECT_LAZY_CONSTRUCTOR', self.compile(kil_operation.a.type_), self.compile(kil_operation.a))
        if kil_operation.op == '=' and isinstance(kil_operation.a.type_, KilTypeClass) and isinstance(kil_operation.b, KilLiteral):
            return CppStlMacroCall('KPB_CPP_STL_OBJECT_CLEAR', self.compile(kil_operation.a.type_), self.compile(kil_operation.a))
        if kil_operation.op == '=' and (isinstance(kil_operation.a.type_, KilTypeString) or isinstance(kil_operation.a.type_, KilTypeBuffer)) and isinstance(kil_operation.b, KilLiteral):
            if len(kil_operation.b.value) == 0:
                kil_method_call = KilMethodCall()
                kil_method_call.class_type = kil_operation.a.type_
                kil_method_call.mutable = True
                kil_method_call.public = True
                kil_method_call.decl_variable = KilVariable()
                kil_method_call.decl_variable.name = 'clear'
                kil_method_call.decl_variable.type_ = KilTypeNone()
                kil_method_call.decl_variable.mutable = False
                kil_method_call.decl_variable.reference = False
                kil_method_call.decl_variable.list_ = False
                kil_method_call.arg_variables = list()
                kil_method_call = KilOperation(kil_operation.a, '.', kil_method_call)
                method_call = self.compile(kil_method_call)
                method_call.b.decl_variable.name = 'clear'
                return method_call
        operation = CppStlOperation()
        operation.a = self.compile(kil_operation.a)
        operation.op = kil_operation.op
        if kil_operation.b:
            operation.b = self.compile(kil_operation.b)
        if kil_operation.c:
            operation.c = self.compile(kil_operation.c)
        if operation.op == '=' and not isinstance(operation.a, CppStlDeclaration) and (operation.a.pointers - 1 == operation.b.pointers):
            operation.a = CppStlOperation(operation.a, '*')
        if operation.op == '.' and isinstance(operation.a, CppStlVariable):
            if operation.a.name == 'this' and isinstance(operation.b, CppStlVariable):
                # its a member variable
                operation.b = self.to_member_variable(operation.b)
        if operation.op == '[]' and isinstance(operation.a.type_, CppStlTypeClass):
            get_method = CppStlMethodCall()
            get_method.class_type = operation.a.type_
            get_method.decl_variable = CppStlVariable()
            get_method.decl_variable.name = 'get'
            get_method.decl_variable.type_ = operation.a.type_
            get_method.decl_variable.const = False
            get_method.decl_variable.pointers = 0
            get_method.decl_variable.reference = True
            get_method.decl_variable.vector = False
            get_method.arg_variables = list()
            operation = CppStlOperation(operation, '.', get_method)
        if operation.op == '=' and isinstance(operation.a.type_, CppStlTypeClass):
            if operation.a.pointers == operation.b.pointers + 1:
                operation.b = CppStlOperation(operation.b, '&')
        if operation.op == 'and':
            operation.op = '&&'
        if operation.op == 'or':
            operation.op = '||'
        if operation.op == 'not':
            operation.op = '!'
        return operation

    def compile_typecast(self, kil_typecast):
        typecast = CppStlTypecast()
        typecast.value = self.compile(kil_typecast.value)
        typecast.type_ = self.compile(kil_typecast.type_)
        return typecast
