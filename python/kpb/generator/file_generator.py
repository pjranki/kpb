import os
import sys


class FileGenerator(object):

    def __init__(self, ext=None):
        self.ext = ext

    def generate(self, path, args):
        assert self.ext
        path = os.path.realpath(path)
        if not os.path.exists(path):
            raise FileNotFoundError(path)
        if os.path.isdir(path):
            paths = [os.path.join(path, name) for name in os.listdir(path) if name.endswith(self.ext)]
            return paths
        assert path.endswith(self.ext)
        return path


class FileWriteTextGenerator(object):

    def __init__(self, generator=None, file_name_attr=None, ext=None):
        self.generator = generator
        self.file_name_attr = file_name_attr
        self.ext = ext

    def generate(self, obj, args):
        assert self.generator
        assert self.file_name_attr
        assert self.ext
        file_name = getattr(obj, self.file_name_attr)
        file_name = os.path.basename(file_name)
        file_name = os.path.splitext(file_name)[0]
        file_name += self.ext
        if not os.path.exists(args.outdir):
            os.makedirs(args.outdir)
        path = os.path.join(args.outdir, file_name)
        text = self.generator.generate(obj, args)
        assert isinstance(text, str)
        write_file = True
        if os.path.exists(path):
            with open(path, 'rt') as handle:
                old_text = handle.read()
            if old_text == text:
                # skip this file (don't change its timestamps, allow build tools to skip it)
                write_file = False
        if write_file:
            with open(path, 'wt') as handle:
                handle.write(text)
