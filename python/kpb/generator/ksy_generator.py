import os
import sys
import re

from lang.expression_parser import ExpressionParser


class KsyPrintFileName(object):

    def __init__(self, file_path_key=None):
        self.file_path_key = file_path_key

    def generate(self, ksy_dict, args):
        path = ksy_dict[self.file_path_key]
        file_name = os.path.basename(path)
        print(file_name)
        return ksy_dict


class KsyLoadImportsToDictGenerator(object):

    def __init__(self, yaml_file_to_dict_generator=None, file_path_key=None, ext=None, add_import_map_key=None):
        self.yaml_file_to_dict_generator = yaml_file_to_dict_generator
        self.file_path_key = file_path_key
        self.ext = ext
        self.add_import_map_key = add_import_map_key

    def load_import(self, import_name, import_paths, args):
        for import_path in import_paths:
            path = os.path.join(import_path, import_name + '.ksy')
            if os.path.exists(path):
                return self.yaml_file_to_dict_generator.generate(path, args)
        raise FileNotFoundError("ksy import '{}'".format(import_name))

    def expand_imports(self, ksy_dict, ksy_imports_dict, ksy_dict_map, args):
        import_paths = [os.path.dirname(ksy_dict[self.file_path_key])]
        import_paths.extend(args.import_path)
        for import_name in ksy_dict['meta'].get('imports', list()):
            if import_name in ksy_imports_dict.keys():
                # already done
                continue
            sub_ksy_dict = ksy_dict_map.get(import_name)
            if not sub_ksy_dict:
                # load if not already loaded
                sub_ksy_dict = self.load_import(import_name, import_paths, args)
                # keep track of it - so we don't waste time loading again
                ksy_dict_map[ksy_dict['meta']['id']] = sub_ksy_dict
            # add this import
            ksy_imports_dict[sub_ksy_dict['meta']['id']] = sub_ksy_dict
            # and add its children
            ksy_imports_dict.update(self.expand_imports(sub_ksy_dict, ksy_imports_dict, ksy_dict_map, args))
        return ksy_imports_dict

    def generate(self, ksy_dict_list, args):
        ksy_dict_map = {ksy_dict['meta']['id']: ksy_dict for ksy_dict in ksy_dict_list}
        for ksy_dict in ksy_dict_list:
            ksy_imports_dict = dict()
            ksy_imports_dict[ksy_dict['meta']['id']] = ksy_dict
            ksy_imports_dict = self.expand_imports(ksy_dict, ksy_imports_dict, ksy_dict_map, args)
            ksy_dict[self.add_import_map_key] = ksy_imports_dict
        return ksy_dict_list


class KsyCompileListToDictGenerator(object):

    def __init__(self, add_compile_list_key=None):
        self.add_compile_list_key = add_compile_list_key

    def generate(self, ksy_dict_list, args):
        for ksy_dict in ksy_dict_list:
            ksy_dict[self.add_compile_list_key] = ksy_dict_list
        return ksy_dict_list


class KsyTypeFixGenerator(object):

    def __init__(self):
        pass

    def generate(self, ksy_dict, args):
        return self.type_fix_dict(ksy_dict)

    def type_fix(self, ksy_obj):
        if isinstance(ksy_obj, dict):
            return self.type_fix_dict(ksy_obj)
        if isinstance(ksy_obj, list):
            return self.type_fix_list(ksy_obj)
        if isinstance(ksy_obj, bool):
            return 'true' if ksy_obj else 'false'
        return ksy_obj

    def type_fix_id(self, id_):
        if isinstance(id_, dict):
            raise Exception("ksy 'id' cannot be a dictionary")
        if isinstance(id_, list):
            raise Exception("ksy 'id' cannot be a list")
        if not isinstance(id_, str):
            id_ = str(id_)
        return id_

    def type_fix_dict(self, ksy_dict):
        assert isinstance(ksy_dict, dict)
        new_ksy_dict = dict()
        for key, ksy_obj in ksy_dict.items():
            key = self.type_fix_id(key)
            if key == 'id':
                ksy_obj = self.type_fix_id(ksy_obj)
            if key == 'enums':
                new_ksy_dict[key] = self.type_fix_enum_dict(ksy_obj)
            else:
                new_ksy_dict[key] = self.type_fix(ksy_obj)
        return new_ksy_dict

    def type_fix_enum_dict(self, ksy_dict):
        assert isinstance(ksy_dict, dict)
        new_ksy_dict = dict()
        for key, ksy_obj in ksy_dict.items():
            key = self.type_fix_id(key)
            new_ksy_dict[key] = self.type_fix_enum_value_dict(ksy_obj)
        return new_ksy_dict

    def type_fix_enum_value_dict(self, ksy_dict):
        assert isinstance(ksy_dict, dict)
        new_ksy_dict = dict()
        for key, ksy_obj in ksy_dict.items():
            # enum values are key'd with integers
            if not isinstance(key, int):
                try:
                    key = int(key, 0)
                except ValueError:
                    raise
            new_ksy_dict[key] = self.type_fix(ksy_obj)
        return new_ksy_dict

    def type_fix_list(self, ksy_list):
        assert isinstance(ksy_list, list)
        new_ksy_list = list()
        for ksy_obj in ksy_list:
            new_ksy_list.append(self.type_fix(ksy_obj))
        return new_ksy_list


KSY_INTEGER_BYTE_COUNTS = [1, 2, 4, 8]
KSY_INTEGER_BIT_COUNTS = [v * 8 for v in KSY_INTEGER_BYTE_COUNTS]
KSY_INTEGER_TYPES = []
for bits in range(1, KSY_INTEGER_BIT_COUNTS[-1] + 1):
    KSY_INTEGER_TYPES.append('b{}'.format(bits))
for byte_count in KSY_INTEGER_BYTE_COUNTS:
    bits = byte_count * 8
    for sign in ['s', 'u']:
        signed = sign == 's'
        for endian in ['', 'be', 'le']:
            KSY_INTEGER_TYPES.append('{}{}{}'.format(sign, byte_count, endian))
KSY_FLOAT_TYPES = ['f4', 'f8']
KSY_STRING_TYPES = ['str', 'strz']

KSY_BUILTIN_TYPES = list()
KSY_BUILTIN_TYPES.extend(KSY_INTEGER_TYPES)
KSY_BUILTIN_TYPES.extend(KSY_FLOAT_TYPES)
KSY_BUILTIN_TYPES.extend(KSY_STRING_TYPES)

class KsyNameDeconfictGenerator(object):

    VALID_EXPR_CONSTANTS = ['true', 'false', 'to_i', 'length', 'size', 'bit_size']

    def __init__(self, name_deconfilctor=None):
        self.name_deconfilctor = name_deconfilctor

    def generate(self, ksy_dict, args):
        args.namespace = self.name_deconfilctor.namespace_name(args.namespace)
        return self.deconflict_file(ksy_dict)

    def deconflict_file(self, ksy_file_dict):
        if ksy_file_dict.get('meta', None):
            ksy_file_dict['meta'] = self.deconflict_meta(ksy_file_dict['meta'])
        if ksy_file_dict.get('types', None):
            ksy_file_dict['types'] = self.deconflict_types(ksy_file_dict['types'])
        # the file can also be treated as its own type (root class for the file)
        ksy_file_dict = self.deconflict_type(ksy_file_dict)
        return ksy_file_dict

    def deconflict_types(self, ksy_types_dict):
        new_ksy_types_dict = dict()
        for type_name, ksy_type_dict in ksy_types_dict.items():
            type_name = self.name_deconfilctor.class_name(type_name)
            ksy_type_dict = self.deconflict_type(ksy_type_dict)
            new_ksy_types_dict[type_name] = ksy_type_dict
        ksy_types_dict = new_ksy_types_dict
        return ksy_types_dict

    def deconflict_type(self, ksy_type_dict):
        if ksy_type_dict.get('id', None):
            ksy_type_dict['id'] = self.name_deconfilctor.class_name(ksy_type_dict['id'])
        if ksy_type_dict.get('enums', None):
            ksy_type_dict['enums'] = self.deconflict_enums(ksy_type_dict['enums'])
        if ksy_type_dict.get('params', None):
            ksy_type_dict['params'] = self.deconflict_member_list(ksy_type_dict['params'])
        if ksy_type_dict.get('seq', None):
            ksy_type_dict['seq'] = self.deconflict_member_list(ksy_type_dict['seq'])
        if ksy_type_dict.get('instances', None):
            ksy_type_dict['instances'] = self.deconflict_member_dict(ksy_type_dict['instances'])
        if ksy_type_dict.get('-instances', None):
            ksy_type_dict['instances'] = ksy_type_dict.get('instances', dict())
            for key, value in self.deconflict_member_dict(ksy_type_dict['-instances']).items():
                ksy_type_dict['instances'][key] = value
            ksy_type_dict['-instances'] = dict()
        if ksy_type_dict.get('-update', None):
            ksy_type_dict['-update'] = self.deconflict_member_list(ksy_type_dict['-update'])
        return ksy_type_dict

    def deconflict_meta(self, ksy_meta_dict):
        if ksy_meta_dict.get('id', None):
            ksy_meta_dict['id'] = self.name_deconfilctor.class_name(ksy_meta_dict['id'])
        if ksy_meta_dict.get('imports', None):
            new_imports = list()
            for import_name in ksy_meta_dict.get('imports', list()):
                import_name = self.name_deconfilctor.import_name(import_name)
                new_imports.append(import_name)
            ksy_meta_dict['imports'] = new_imports
        return ksy_meta_dict

    def deconflict_enums(self, ksy_enums_dict):
        new_ksy_enums_dict = dict()
        for enum_name, ksy_enum_dict in ksy_enums_dict.items():
            enum_name = self.name_deconfilctor.enum_name(enum_name)
            ksy_enum_dict = self.deconflict_enum(ksy_enum_dict)
            new_ksy_enums_dict[enum_name] = ksy_enum_dict
        ksy_enums_dict = new_ksy_enums_dict
        return ksy_enums_dict

    def deconflict_enum(self, ksy_enum_dict):
        new_ksy_enum_dict = dict()
        for value, obj in ksy_enum_dict.items():
            if isinstance(obj, dict):
                if obj.get('id', None):
                    obj['id'] = self.name_deconfilctor.enum_value_name(obj['id'])
            elif isinstance(obj, str):
                obj = self.name_deconfilctor.enum_value_name(obj)
            else:
                raise NotImplementedError("enum value body of {}".format(repr(obj)))
            new_ksy_enum_dict[value] = obj
        ksy_enum_dict = new_ksy_enum_dict
        return ksy_enum_dict

    def deconflict_member_list(self, ksy_members_list):
        new_ksy_members_list = list()
        for ksy_member_dict in ksy_members_list:
            ksy_member_dict = self.deconflict_member(ksy_member_dict)
            new_ksy_members_list.append(ksy_member_dict)
        ksy_members_list = new_ksy_members_list
        return ksy_members_list

    def deconflict_member_dict(self, ksy_members_dict):
        new_ksy_members_dict = dict()
        for member_name, ksy_member_dict in ksy_members_dict.items():
            member_name = self.name_deconfilctor.member_name(member_name)
            ksy_member_dict = self.deconflict_member(ksy_member_dict)
            new_ksy_members_dict[member_name] = ksy_member_dict
        ksy_members_dict = new_ksy_members_dict
        return ksy_members_dict

    def deconflict_member(self, ksy_member_dict):
        if ksy_member_dict.get('id', None):
            ksy_member_dict['id'] = self.name_deconfilctor.member_name(ksy_member_dict['id'])
        if ksy_member_dict.get('if', None):
            ksy_member_dict['if'] = self.deconflict_expr(ksy_member_dict['if'])
        if ksy_member_dict.get('size', None):
            ksy_member_dict['size'] = self.deconflict_expr(ksy_member_dict['size'])
        if ksy_member_dict.get('value', None):
            ksy_member_dict['value'] = self.deconflict_expr(ksy_member_dict['value'])
        if ksy_member_dict.get('repeat-expr', None):
            ksy_member_dict['repeat-expr'] = self.deconflict_expr(ksy_member_dict['repeat-expr'])
        if ksy_member_dict.get('type', None):
            ksy_member_dict['type'] = self.deconflict_member_type(ksy_member_dict['type'])
        if ksy_member_dict.get('-type', None):
            ksy_member_dict['type'] = self.deconflict_member_type(ksy_member_dict['-type'])
            ksy_member_dict['-type'] = None
        if ksy_member_dict.get('-set', None):
            ksy_member_dict['-set'] = self.deconflict_member_list(ksy_member_dict['-set'])
        return ksy_member_dict

    def deconflict_member_type(self, ksy_member_type):
        if isinstance(ksy_member_type, dict):
            # handle switch-on type
            ksy_member_type_dict = ksy_member_type
            if ksy_member_type_dict.get('switch-on', None):
                ksy_member_type_dict['switch-on'] = self.deconflict_expr(ksy_member_type_dict['switch-on'])
            if ksy_member_type_dict.get('cases', None):
                new_ksy_case_dict = dict()
                for case, ksy_member_type in ksy_member_type_dict['cases'].items():
                    if isinstance(case, str):
                        case = self.deconflict_expr(case)
                    new_ksy_case_dict[case] = self.deconflict_member_type(ksy_member_type)
                ksy_member_type_dict['cases'] = new_ksy_case_dict
            return ksy_member_type_dict
        if ksy_member_type.find('(') != -1:
            # handle a type taking parameters
            items = ksy_member_type.split('(')
            ksy_member_type = items[0]
            ksy_args = '('.join(items[1:])
            ksy_args = ksy_args.strip()
            assert ksy_args.endswith(')')
            ksy_args = ksy_args[:-1]
            ksy_args_list = ksy_args.split(',')
            ksy_member_type = self.deconflict_member_type(ksy_member_type)
            new_ksy_args_list = list()
            for ksy_arg in ksy_args_list:
                ksy_arg = self.deconflict_expr(ksy_arg)
                new_ksy_args_list.append(ksy_arg)
            ksy_args_list = new_ksy_args_list
            return '{}({})'.format(ksy_member_type, ', '.join(ksy_args_list))
        # handle a type name string
        if ksy_member_type in KSY_BUILTIN_TYPES:
            # filter out built-in types
            return ksy_member_type
        ksy_member_type = self.name_deconfilctor.class_name(ksy_member_type)
        return ksy_member_type

    def deconflict_expr(self, ksy_expr):
        try:
            expr_list = ExpressionParser.split_expr_list(ksy_expr)
            expr_tree = ExpressionParser.create_expr_tree(expr_list)
            expr_tree = ExpressionParser.reduce_expr_tree(expr_tree)
            expr_tree = self.deconflict_expr_tree(expr_tree)
            ksy_expr = ExpressionParser.flatten_expr_tree(expr_tree)
        except:
            print("ERROR PARSING EXPRESSION: '{}'".format(ksy_expr))
            raise
        return ksy_expr

    def deconflict_expr_tree(self, expr_tree):
        if isinstance(expr_tree, str):
            if expr_tree.find('::') != -1:
                enum_name, enum_value_name = expr_tree.split('::')
                enum_name = self.name_deconfilctor.enum_name(enum_name)
                enum_value_name = self.name_deconfilctor.enum_value_name(enum_value_name)
                return '{}::{}'.format(enum_name, enum_value_name)
            if expr_tree in self.VALID_EXPR_CONSTANTS:
                # treat these as constants (not members)
                return expr_tree
            return self.name_deconfilctor.member_name(expr_tree)
        if not isinstance(expr_tree, list):
            return expr_tree
        if len(expr_tree) == 0:
            return expr_tree
        if len(expr_tree) == 1:
            return [self.deconflict_expr_tree(expr_tree[0])]
        op = expr_tree[0]
        new_expr_tree = [op]  # first arg is the operation
        for expr in expr_tree[1:]:
            new_expr_tree.append(self.deconflict_expr_tree(expr))
        return new_expr_tree


class KsyParseException(Exception):

    @classmethod
    def to_name(cls, obj):
        if isinstance(obj, bool):
            return str(obj).lower()
        if isinstance(obj, str):
            return obj
        if isinstance(obj, type):
            name = obj.__name__
        else:
            name = obj.__class__.__name__
        if name == 'NoneType':
            return 'null'
        if name == 'dict':
            return 'map'
        if name == 'list':
            return 'array'
        if name == 'str':
            return 'string'
        if name == 'bool':
            return 'boolean'
        return name

    def __init__(self, file_path, yaml_path, message):
        super(KsyParseException, self).__init__('{}:{}: {}'.format(file_path, yaml_path, message))



class KsyParseVersionException(KsyParseException):

    def __init__(self, file_path, yaml_path, required_minimum_version, version):
        message = 'this ksy requires compiler version at least {}, but you have {}'.format(required_minimum_version, version)
        super(KsyParseVersionException, self).__init__(file_path, yaml_path, message)


class KsyParseWrongTypeException(KsyParseException):

    def __init__(self, file_path, yaml_path, valid_types, got_type):
        assert len(valid_types) > 0
        if len(valid_types) == 1:
            expected_type = self.to_name(valid_types[0])
        else:
            expected_type = ', '.join([self.to_name(t) for t in valid_types[:-1]]) + ' or ' + self.to_name(valid_types[-1])
        message = 'expected {}, got {}'.format(expected_type, self.to_name(got_type))
        super(KsyParseWrongTypeException, self).__init__(file_path, yaml_path, message)


class KsyParseKeyNotFoundException(KsyParseException):

    def __init__(self, file_path, yaml_path, key):
        if not yaml_path.endswith('/'):
            yaml_path += '/'
        yaml_path += key
        if yaml_path.find('meta') != -1:
            path = yaml_path.lstrip('/')
            message = 'no `{}` encountered in top-level class spec'.format(path)
        else:
            message = 'missing mandatory argument `{}`'.format(key)
        super(KsyParseKeyNotFoundException, self).__init__(file_path, yaml_path, message)


class KsyParseUnknownKeyFoundException(KsyParseException):

    def __init__(self, file_path, yaml_path, key, valid_keys):
        if not yaml_path.endswith('/'):
            yaml_path += '/'
        yaml_path += key
        message = 'unknown key found, expected: {}'.format(', '.join(valid_keys))
        super(KsyParseUnknownKeyFoundException, self).__init__(file_path, yaml_path, message)


class KsyParseInvalidValueKeyFoundException(KsyParseException):

    def __init__(self, file_path, yaml_path, key, valid_keys):
        if not yaml_path.endswith('/'):
            yaml_path += '/'
        yaml_path += key
        message = 'invalid key found in value instance, allowed: {}'.format(', '.join(valid_keys))
        super(KsyParseInvalidValueKeyFoundException, self).__init__(file_path, yaml_path, message)


class KsyFileToDictGenerator(object):
    KS_VERSION = 0.8
    ID_RE = '^[a-z][a-z0-9_]*$'
    VALID_SUB_KEYS = {
        'meta': ['application', 'encoding', 'endian', 'file-extension', 'id', 'imports', 'ks-debug', 'ks-opaque-types', 'ks-version', 'license', 'title', 'xref', '-bitalign'],
        'types': None,  # can be any string
        'type-instance': ['doc', 'doc-ref', 'enums', 'instances', '-instances', 'meta', 'params', 'seq', 'types', '-update'],
        'enums': None,  # can be any string
        'enum-instance': None,  # can be any string
        'enum-value-instance': ['doc', 'doc-ref', 'id'],
        'instances': None,  # can be any string
        '-instances': None,  # kpb-specific - can be any string
        'value-instance': ['doc', 'doc-ref', 'enum', 'if', 'value', '-process-value'],
        'pos-instance': ['consume', 'doc', 'doc-ref', 'eos-error', 'if', 'include', 'repeat', 'terminator', 'type', 'pos', 'io', '-set'],
        'seq-instance': ['consume', 'doc', 'doc-ref', 'eos-error', 'id', 'if', 'include', 'repeat', 'terminator', 'type', '-set'],
        'param-instance': ['doc', 'doc-ref', 'id', 'type', '-type', '-set'],
        'set-instance': ['doc', 'doc-ref', 'id', 'value', 'if'],  # kpb-specific
        'type': ['switch-on', 'cases', '-name'],
        'cases': None,
    }
    INSTANCE_TYPES = {
        'types': 'type-instance',
        'enums': 'enum-instance',
        'enum-instance': 'enum-value-instance',
        'instances': None,  # special case, can be either a value-instance or pos-instance
        '-instances': None, # kpb-specific
        'seq': 'seq-instance',
        'params': 'param-instance',
        '-update': 'set-instance', # kpb-specific
        '-set': 'set-instance', # kpb-specific
        'imports': 'import',
        'cases': 'case-instance',
    }
    VALID_TYPES = {
        'doc': [str, int, bool],
        'doc-ref': str,
        'types': dict,
        'type-instance': dict,
        'enums': dict,
        'enum-instance': dict,
        'enum-value-instance': [dict, str, bool],
        'instances': dict,
        '-instances': dict,
        'value-instance': dict,
        'pos-instance': dict,
        'meta': dict,
        'params': list,
        'param-instance': dict,
        'seq': list,
        'seq-instance': dict,
        '-update': list,
        '-set': list,
        'set-instance': dict,
        'application': None,
        'encoding': str,
        'endian': str,
        '-bitalign': int,
        'file-extension': None,
        'id': [str, bool],
        'imports': list,
        'import': str,
        'ks-debug': bool,
        'ks-opaque-types': bool,
        'ks-version': [float, int],
        'license': None,
        'title': None,
        'xref': None,
        'enum': str,
        'if': [str, bool],
        'value': [str, int, bool],
        'consume': bool,
        'eos-error': bool,
        'include': bool,
        'repeat': str,
        'repeat-expr': [str, int],
        'repeat-until': [str, bool],
        'terminator': int,
        'type': [str, dict],
        '-type': str,  # kpb-specific
        'case-instance': str,
        'pos': [str, int],
        'size': [str, int],
        'size-eos': bool,
        'contents': [str, list],
        '-default': [str, list, int, bytes],
        'pad-right': [str, int],
        'parent': str,
        'process': str,
        '-process-value': str,
        'io': str,
        'switch-on': [str, int, bool],
        'cases': dict,
        '-name': str,  # kpb-specific for choosing how to name generated code in switch-on
    }
    REQUIRED_KEYS = {
        '': ['meta'],
        'meta': ['id'],
        'seq-instance': ['id'],
        'param-instance': ['id'],
        'set-instance': ['id'],  # kpb-specific
        'type': ['switch-on', 'cases'],
    }
    KEY_ERROR_NAMES = {
        'types': 'type ID',
        'enums': 'enum ID',
        'enum-value-instance': 'enum value spec id ID',
    }
    DONT_ITERATE_KEYS = ['contents', '-default']  # these can be lists, but do not iterate them
    COMPILE_EXPR_KEYS = [
        'if',
        'value',
        'repeat-expr',
        'repeat-until',
        'pos',
        'size',
        'switch-on',
    ]

    def __init__(self, yaml_to_dict_generator=None, ext=None):
        self.yaml_to_dict_generator = yaml_to_dict_generator
        self.ext = ext
        self.args = None
        self.file_cache = dict()

    def validate_keys_and_types(self, file_path, obj, key='type-instance', yaml_path='/'):
        # validate my type
        if key not in self.VALID_TYPES:
            if key.startswith('-'):
                # ignore
                return obj
            raise KsyParseException(file_path, yaml_path, "no known type for {}".format(key))
        expected_types = self.VALID_TYPES[key]
        if expected_types is not None:
            if not isinstance(expected_types, list):
                expected_types = [expected_types]
            if type(obj) not in expected_types:
                raise KsyParseWrongTypeException(file_path, yaml_path, expected_types, obj)

        # ks-version validate
        if key == 'ks-version':
            required_minimum_version = obj
            if required_minimum_version > self.KS_VERSION:
                raise KsyParseVersionException(file_path, yaml_path, required_minimum_version, self.KS_VERSION)

        # endian validate
        if key == 'endian':
            if obj not in ['le', 'be']:
                raise KsyParseException(file_path, yaml_path, 'unable to parse endianness: `le`, `be` or calculated endianness map is expected')

        # bitalign validate
        if key == '-bitalign':
            if obj not in [1, 8]:
                raise KsyParseException(file_path, yaml_path, 'bit alignment of type to {} bit is current not supported'.format(obj))

        # valid enum IDs which are not part of a dictionary
        if key == 'enum-value-instance' and not isinstance(obj, dict):
            if isinstance(obj, bool):
                id_ = 'true' if obj else 'false'
            else:
                id_ = str(obj)
            if not re.match(self.ID_RE, id_):
                name = 'enum member ID'
                raise KsyParseException(file_path, yaml_path, "invalid {}: '{}', expected /{}/".format(name, id_, self.ID_RE))

        # valid repeat
        if key == 'repeat':
            if obj not in ['eos', 'expr', 'until']:
                raise KsyParseException(file_path, yaml_path, "expected eos / expr / until, got '{}'".format(obj))

        # valid name (switch-on, kpb-specific)
        if key == '-name':
            if obj not in ['default', 'enum', 'type']:
                raise KsyParseException(file_path, yaml_path, "expected default / enum / type, got '{}'".format(obj))

        # additional validation for lists
        if isinstance(obj, list) and key not in self.DONT_ITERATE_KEYS:
            # find the key type that represents an instance in this dictionary
            if key not in self.INSTANCE_TYPES:
                raise KsyParseException(file_path, yaml_path, "no known instance type for {}".format(key))
            instance_key = self.INSTANCE_TYPES[key]

            # validate each object in the list
            index = 0
            for sub_obj in obj:
                sub_yaml_path = '{}/{}'.format(yaml_path, index)

                # validate sub-key
                self.validate_keys_and_types(file_path, sub_obj, instance_key, sub_yaml_path)

                # next index
                index += 1

        # additional validation for dictionaries
        if isinstance(obj, dict) and key not in self.DONT_ITERATE_KEYS:
            # validate required keys
            required_sub_keys = self.REQUIRED_KEYS.get(key, list())
            for required_sub_key in required_sub_keys:
                if required_sub_key not in obj.keys():
                    raise KsyParseKeyNotFoundException(file_path, yaml_path, required_sub_key)

            # validate expected sub-keys
            if key not in self.VALID_SUB_KEYS:
                raise KsyParseException(file_path, yaml_path, "no known sub-keys for {}".format(key))
            valid_sub_keys = self.VALID_SUB_KEYS[key]
            if valid_sub_keys is not None:
                # there is a specific set of valid sub-keys

                # extend valid sub-keys
                valid_sub_keys = valid_sub_keys[:]
                if 'repeat' in obj:
                    sub_key = 'repeat-{}'.format(obj['repeat'])
                    if sub_key == 'repeat-until':
                        valid_sub_keys.extend([sub_key])
                    if sub_key == 'repeat-expr':
                        valid_sub_keys.extend([sub_key])
                if key in ['pos-instance', 'seq-instance', 'param-instance']:
                    type_ = obj.get('-type', obj.get('type', None))
                    if type_ is None:
                        valid_sub_keys.extend(['size', 'size-eos'])
                    if isinstance(type_, dict):
                        # switch-on, could be many different things
                        valid_sub_keys.extend(['enum', 'encoding', 'pad-right', 'parent', 'process', 'size', 'size-eos', '-default'])
                    else:
                        type_ = str(type_)
                        int_type = True if re.match('^(([us][1248]|b(([1-9])|([1-5][0-9])|(6[0-4]))))(|be|le)$', type_) else False
                        bool_type = type_ == 'bool'
                        str_type = type_ in ['str', 'strz']
                        if int_type or bool_type:
                            # int or bool
                            valid_sub_keys.extend(['enum', '-default'])
                        elif str_type:
                            # string
                            valid_sub_keys.extend(['encoding', 'pad-right', 'size', 'size-eos', '-default'])
                            pass
                        else:
                            # sub-type
                            valid_sub_keys.extend(['contents', 'pad-right', 'parent', 'process', 'size', 'size-eos'])
                    if key == 'param-instance':
                        remove_list = ['contents', 'pad-right', 'parent', 'process', 'size-eos']
                        for remove_ in remove_list:
                            if remove_ in valid_sub_keys:
                                valid_sub_keys.remove(remove_)
                    if 'size' in valid_sub_keys and 'size-eos' in valid_sub_keys:
                        if 'size' in obj and 'size-eos' in obj:
                            raise KsyParseException(file_path, yaml_path, "only one of 'size' or 'size-eos' must be specified")
                    if key in ['pos-instance', 'seq-instance', 'param-instance', 'value-instance']:
                        valid_sub_keys.extend(['-process-value'])

                # all keys must be valid sub-keys
                for sub_key in obj.keys():
                    sub_key = str(sub_key)  # e.g. enums can have integers instead of strings as keys
                    if sub_key.startswith('-'):
                        continue
                    if sub_key not in valid_sub_keys:
                        if key == 'value-instance':
                            raise KsyParseInvalidValueKeyFoundException(file_path, yaml_path, sub_key, valid_sub_keys)
                        else:
                            raise KsyParseUnknownKeyFoundException(file_path, yaml_path, sub_key, valid_sub_keys)

                # validate each sub-key
                for sub_key, sub_obj in obj.items():
                    sub_key = str(sub_key)  # e.g. enums can have integers instead of strings as keys
                    sub_yaml_path = yaml_path
                    if not sub_yaml_path.endswith('/'):
                        sub_yaml_path += '/'
                    sub_yaml_path += sub_key
                    self.validate_keys_and_types(file_path, sub_obj, sub_key, sub_yaml_path)

                    # id validate
                    if sub_key == 'id' and isinstance(sub_obj, str):
                        id_ = str(sub_obj)
                        if not re.match(self.ID_RE, id_) and id_ not in ['_case', '_value']:
                            name = self.KEY_ERROR_NAMES.get(key, key)
                            raise KsyParseException(file_path, yaml_path, "invalid {}: '{}', expected /{}/".format(name, id_, self.ID_RE))

                # validate repeat
                repeat_expr_key = None
                for sub_key, sub_obj in obj.items():
                    if sub_key == 'repeat':
                        if str(sub_obj) != 'eos':
                            repeat_expr_key = 'repeat-{}'.format(str(sub_obj))
                            break
                if repeat_expr_key and repeat_expr_key not in obj:
                    message = "`{}` requires a `{}` expression".format(repeat_expr_key.replace('-', ': '), repeat_expr_key)
                    raise KsyParseException(file_path, yaml_path + '/repeat', message)
            else:
                # sub-keys can be called anything
                # but the keys within those sub-keys (sub-sub-keys) might be restricted

                # all keys must be valid sub-keys
                for sub_key in obj.keys():
                    sub_key = str(sub_key)
                    if key == 'enum-instance':
                        valid = False
                        try:
                            sub_key = int(sub_key)
                            valid = True
                        except:
                            pass
                        if not valid:
                            raise KsyParseException(file_path, yaml_path, 'unable to parse `{}` as int'.format(sub_key))
                    elif key == 'cases':
                        # all keys valid, they can integers, enums or some basic expression.
                        pass
                    else:
                        if not re.match(self.ID_RE, sub_key):
                            name = self.KEY_ERROR_NAMES.get(key, key)
                            raise KsyParseException(file_path, yaml_path, "invalid {}: '{}', expected /{}/".format(name, sub_key, self.ID_RE))

                # find the key type that represents an instance in this dictionary
                if key not in self.INSTANCE_TYPES:
                    raise KsyParseException(file_path, yaml_path, "no known instance type for {}".format(key))
                instance_key = self.INSTANCE_TYPES[key]

                # validate each sub-key
                for sub_key, sub_obj in obj.items():
                    sub_key = str(sub_key)  # e.g. enums can have integers instead of strings as keys
                    sub_yaml_path = yaml_path
                    if not sub_yaml_path.endswith('/'):
                        sub_yaml_path += '/'
                    sub_yaml_path += sub_key

                    # special case - instances can switch on two types, value or position instances
                    if key == 'instances' or key == '-instances':
                        if isinstance(sub_obj, dict) and 'value' in sub_obj:
                            instance_key = 'value-instance'
                        else:
                            instance_key = 'pos-instance'

                    # validate sub-key
                    self.validate_keys_and_types(file_path, sub_obj, instance_key, sub_yaml_path)

    @classmethod
    def to_string(self, expr):
        if expr is None:
            expr = 'null'
        if isinstance(expr, bool):
            expr = 'true' if expr else 'false'
        return str(expr)

    def force_standard_types(self, file_path, obj, yaml_path='/'):
        key = yaml_path.split('/')[-1]
        yaml_path = '' if yaml_path == '/' else yaml_path

        # iterate list
        if isinstance(obj, list) and key not in self.DONT_ITERATE_KEYS:
            index = 0
            for index in range(0, len(obj)):
                obj[index] = self.force_standard_types(file_path, obj[index], yaml_path='{}/{}'.format(yaml_path, index))
                index += 1
            return obj

        # iterate dictionary
        if isinstance(obj, dict) and key not in self.DONT_ITERATE_KEYS:
            has_known_keys = isinstance(self.VALID_SUB_KEYS.get(key), list)
            for sub_key in list(obj.keys()):
                if key == 'enums':
                    # force enums to be dictionaries with integer keys
                    sub_obj = obj[sub_key]
                    for sub_sub_key in list(sub_obj.keys()):
                        enum_ = sub_obj[sub_sub_key]
                        # force enum to be dictionary
                        if not isinstance(enum_, dict):
                            enum_ = {'id': enum_}
                        # force enum key to be integer
                        if not isinstance(sub_sub_key, int):
                            value = int(sub_sub_key, 0)
                        else:
                            value = sub_sub_key
                        enum_ = self.force_standard_types(file_path, enum_, yaml_path='{}/{}/{}'.format(yaml_path, sub_key, value))
                        del sub_obj[sub_sub_key]
                        sub_obj[value] = enum_
                    del obj[sub_key]
                    obj[self.to_string(sub_key)] = sub_obj
                elif has_known_keys:
                    # force the types for these keys
                    if sub_key in self.VALID_TYPES:
                        valid_types = self.VALID_TYPES[sub_key]
                        valid_type = None
                        if not isinstance(valid_types, list):
                            valid_type = valid_types
                        elif len(valid_types) == 0:
                            valid_type = None
                        elif len(valid_types) == 1:
                            valid_type = valid_types[0]
                        else:
                            valid_type = None
                            if str in valid_types:
                                pass #print(sub_key)
                    # all other keys are expected to be strings
                    sub_obj = obj[sub_key]
                    sub_obj = self.force_standard_types(file_path, sub_obj, yaml_path='{}/{}'.format(yaml_path, sub_key))
                    del obj[sub_key]
                    obj[self.to_string(sub_key)] = sub_obj
                else:
                    # TODO: continue implementation here
                    #if sub_key == 'size':
                    #    print(sub_key)
                    # all other keys are expected to be strings
                    sub_obj = obj[sub_key]
                    sub_obj = self.force_standard_types(file_path, sub_obj, yaml_path='{}/{}'.format(yaml_path, sub_key))
                    del obj[sub_key]
                    obj[self.to_string(sub_key)] = sub_obj
            return obj

        # nothing to do
        return obj

    def pre_parse_expr(self, file_path, obj, yaml_path='/'):
        key = yaml_path.split('/')[-1]
        yaml_path = '' if yaml_path == '/' else yaml_path

        # pre-parse list
        if isinstance(obj, list) and key not in self.DONT_ITERATE_KEYS:
            index = 0
            for index in range(0, len(obj)):
                obj[index] = self.pre_parse_expr(file_path, obj[index], yaml_path='{}/{}'.format(yaml_path, index))
                index += 1
            return obj

        # pre-parse dictionary
        if isinstance(obj, dict) and key not in self.DONT_ITERATE_KEYS:
            has_known_keys = isinstance(self.VALID_SUB_KEYS.get(key), list)
            for sub_key in list(obj.keys()):
                if has_known_keys and sub_key in self.COMPILE_EXPR_KEYS:
                    expr = self.to_string(obj[sub_key])
                    try:
                        expr_list = ExpressionParser.split_expr_list(expr)
                        expr_tree = ExpressionParser.create_expr_tree(expr_list)
                        expr_tree = ExpressionParser.reduce_expr_tree(expr_tree)
                    except:
                        raise KsyParseException(file_path, yaml_path, "invalid expression '{}'".format(expr))
                    obj[sub_key] = expr
                    obj['#{}-expr-list'.format(sub_key)] = expr_list
                    obj['#{}-expr-tree'.format(sub_key)] = expr_tree
                else:
                    obj[sub_key] = self.pre_parse_expr(file_path, obj[sub_key], yaml_path='{}/{}'.format(yaml_path, sub_key))
            return obj

        # nothing to do
        return obj

    @classmethod
    def generate_switchon_member_name(cls, name_generator, member_name, case_expr, type_expr):
        if name_generator == 'default':
            # focus on being unique - use both member name and enumerator to make it unqiue
            if case_expr == '_':
                # for the default case, use the member name
                name = member_name
            elif case_expr.count('::') != 0:
                # use the enumerator name
                enum_value_name = case_expr.split('::')[1]
                enum_value_name = enum_value_name.split('.')[0]
                name = '{}_{}'.format(member_name, enum_value_name)
            else:
                # case is a number, use the member name appended with the case number
                name = '{}_{}'.format(member_name, case_expr)
        elif name_generator == 'enum':
            # force the use of enumerators for names if present
            if case_expr == '_':
                # for the default case, use the member name
                name = member_name
            elif case_expr.count('::') != 0:
                # use the enumerator name
                enum_value_name = case_expr.split('::')[1]
                enum_value_name = enum_value_name.split('.')[0]
                name = enum_value_name
            else:
                # case is a number, use the member name appended with the case number
                name = '{}_{}'.format(member_name, case_expr)
        elif name_generator == 'type':
            # force the use of type names
            name = type_expr.split('(')[0].strip()
        else:
            raise Exception("member '{}' has an unknown switch-on name generator of '{}' [-name: {}]".format(member_name, name_generator, name_generator))
        return name

    def replace_case_expr(self, file_path, obj, case_expr, yaml_path='/'):
        key = yaml_path.split('/')[-1]
        yaml_path = '' if yaml_path == '/' else yaml_path

        # pre-parse list
        if isinstance(obj, list) and key not in self.DONT_ITERATE_KEYS:
            index = 0
            for index in range(0, len(obj)):
                obj[index] = self.replace_case_expr(file_path, obj[index], case_expr, yaml_path='{}/{}'.format(yaml_path, index))
                index += 1
            return obj

        # pre-parse dictionary
        if isinstance(obj, dict) and key not in self.DONT_ITERATE_KEYS:
            has_known_keys = isinstance(self.VALID_SUB_KEYS.get(key), list)
            for sub_key in list(obj.keys()):
                if sub_key in self.COMPILE_EXPR_KEYS and has_known_keys:
                    expr = self.to_string(obj[sub_key])
                    expr_list = None
                    try:
                        expr_list = ExpressionParser.split_expr_list(expr)
                    except:
                        raise KsyParseException(file_path, '{}/{}'.format(yaml_path, sub_key), "invalid expression '{}'".format(expr))
                    if '_case' in expr_list:
                        if case_expr == '_':
                            raise KsyParseException(file_path, '{}/{}'.format(yaml_path, sub_key), "cannot use `_case` when there is a default switch of `_`".format(expr))
                        for i in range(0, len(expr_list)):
                            if expr_list[i] == '_case':
                                expr_list[i] = case_expr
                        expr = ' '.join(expr_list)
                        obj[sub_key] = expr
                else:
                    obj[sub_key] = self.replace_case_expr(file_path, obj[sub_key], case_expr, yaml_path='{}/{}'.format(yaml_path, sub_key))
            return obj

        # nothing to do
        return obj

    def expand_switchon_instance(self, file_path, obj, container, yaml_path):
        member_name = obj['id']
        if_expr = self.to_string(obj.get('if', 'true'))
        switch_on_expr = self.to_string(obj['type']['switch-on'])
        name_generator = obj['type'].get('-name', 'default')
        new_obj_list = list()
        new_obj_dict = dict()
        for case_expr, type_expr in obj['type']['cases'].items():
            case_expr = self.to_string(case_expr)
            id_ = self.generate_switchon_member_name(name_generator, member_name, case_expr, type_expr)
            type_ = type_expr
            if case_expr == '_':
                if_ = ' and '.join(['(({}) != ({}))'.format(self.to_string(c), switch_on_expr) for c in obj['type']['cases'].keys() if c != '_'])
            else:
                if_ = '(({}) == ({}))'.format(case_expr, switch_on_expr)
            if if_expr == 'true':
                if_ = if_
            elif if_expr == 'false':
                if_ = 'false'
            else:
                if_ = '({}) and ({})'.format(if_expr, if_)
            new_obj = dict()
            new_obj.update(obj)
            new_obj['if'] = if_
            new_obj['type'] = type_
            new_obj = self.replace_case_expr(file_path, new_obj, case_expr, yaml_path)
            if container == dict:
                new_obj_dict[id_] = new_obj
            else:
                new_obj['id'] = id_
                new_obj_list.append(new_obj)
        if container == dict:
            return new_obj_dict
        return new_obj_list

    def expand_switchon(self, file_path, obj, yaml_path='/'):
        key = yaml_path.split('/')[-1]
        yaml_path = '' if yaml_path == '/' else yaml_path

        # pre-parse list
        if isinstance(obj, list) and key not in self.DONT_ITERATE_KEYS:
            index = 0
            new_obj = list()
            index = 0
            for sub_obj in obj[:]:
                if isinstance(sub_obj, dict) and isinstance(sub_obj.get('type'), dict):
                    new_obj_list = self.expand_switchon_instance(file_path, sub_obj, container=list, yaml_path='{}/{}'.format(yaml_path, index))
                    current_ids = [n['id'] for n in obj if n['id'] != sub_obj['id']]
                    for id_ in [n['id'] for n in new_obj_list]:
                        if id_ in current_ids and id_ != sub_obj['id']:
                            raise KsyParseException(file_path, '{}/{}'.format(yaml_path, index), 'switch-on expansion has generated duplicate ID `{}`'.format(id_))
                    new_obj.extend(new_obj_list)
                else:
                    new_obj.append(self.expand_switchon(file_path, obj[index], yaml_path='{}/{}'.format(yaml_path, index)))
                index += 1
            obj = new_obj
            return obj

        # pre-parse dictionary
        if isinstance(obj, dict) and key not in self.DONT_ITERATE_KEYS:
            has_known_keys = isinstance(self.VALID_SUB_KEYS.get(key), list)
            for sub_key in list(obj.keys()):
                sub_obj = obj[sub_key]
                if isinstance(sub_obj, dict) and isinstance(sub_obj.get('type'), dict) and has_known_keys:
                    new_obj_dict = self.expand_switchon_instance(file_path, sub_obj, container=dict, yaml_path='{}/{}'.format(yaml_path, sub_key))
                    del obj[sub_key]
                    for id_ in new_obj_dict.keys():
                        if id_ in obj and id_ != sub_obj['id']:
                            raise KsyParseException(file_path, '{}/{}'.format(yaml_path, sub_key), 'switch-on expansion has generated duplicate ID `{}`'.format(id_))
                    obj.update(new_obj_dict)
                else:
                    obj[sub_key] = self.expand_switchon(file_path, obj[sub_key], yaml_path='{}/{}'.format(yaml_path, sub_key))

        # nothing to do
        return obj

    def load_import(self, file_path, import_name):
        import_paths = [os.path.dirname(file_path)]
        import_paths.extend(self.args.import_path)
        for import_path in import_paths:
            path = os.path.join(import_path, import_name + self.ext)
            if os.path.exists(path):
                return self.load_yaml_file(path)
        raise KsyParseException("ksy import '{}'".format(import_name))

    def load_file(self, file_path):
        # try and use existing file from cache
        file_ = self.file_cache.get(file_path)
        if file_:
            return file_

        # load new file
        file_ = self.yaml_to_dict_generator.generate(file_path, self.args)
        if not isinstance(file_, dict):
            raise KsyParseWrongTypeException(file_path, '/', dict, file_)

        # validate structure and types within yaml
        self.validate_keys_and_types(file_path, file_)

        # force all known keys to use a single type e.g. all expression will be a string
        file_ = self.force_standard_types(file_path, file_)

        # place a copy of meta/id in the root dictionary (for the root class)
        file_['id'] = file_['meta']['id']

        # expand switch-on statements
        file_ = self.expand_switchon(file_path, file_)

        # pre-parse expressions into expression lists and expression trees
        file_ = self.pre_parse_expr(file_path, file_)

        # TODO: validate switch-on ints VS enums
        # TODO: validate cases type names
        # TODO: validate endian for integers (not bitfield)
        # TODO: validate encoding for strings
        # TODO: validate expressions

        # place the file path in the root dictionary (helps with source code generation later on)
        file_['#file'] = file_path

        # cache and return loaded file
        self.file_cache[file_path] = file_
        return file_

    def generate(self, file_list, args):
        self.args = args
        for file_path in file_list:
            file_ = self.load_file(file_path)
        return file_list  # TODO:
