import os
import sys
try:
    import yaml
except ModuleNotFoundError:
    raise ModuleNotFoundError("No module named '{}', please run 'pip install {}'".format('yaml', 'pyyaml'))


class YamlFileToDictGenerator(object):

    def __init__(self, add_file_path_key=None):
        self.add_file_path_key = add_file_path_key

    def generate(self, yaml_file, args):
        with open(yaml_file, 'rt') as handle:
            dict_ = yaml.safe_load(handle.read())
        if self.add_file_path_key:
            dict_[self.add_file_path_key] = yaml_file
        return dict_
