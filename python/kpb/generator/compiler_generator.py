import os
import sys


class CompilerGenerator(object):

    def __init__(self, compiler=None):
        self.compiler = compiler

    def generate(self, obj, args):
        assert self.compiler
        return self.compiler.compile(obj, args)
