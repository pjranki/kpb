import os
import sys
import copy


class ListGenerator(object):

    def __init__(self, generator_list=None):
        self.generator_list = generator_list if generator_list else list()

    def generate(self, obj, args):
        for generator in self.generator_list:
            obj = generator.generate(obj, args)
        return obj


class IterateListGenerator(object):

    def __init__(self, generator=None):
        self.generator = generator

    def generate(self, obj, args):
        assert self.generator
        if isinstance(obj, list):
            obj_list = obj
        else:
            obj_list = [obj]
        new_obj_list = list()
        for obj in obj_list:
            if isinstance(obj, list):
                new_obj = self.generate(obj, args)
            else:
                new_obj = self.generator.generate(obj, args)
            if isinstance(new_obj, list):
                new_obj_list.extend(new_obj)
            else:
                new_obj_list.append(new_obj)
        return new_obj_list


class IterateAndIncludeListGenerator(object):

    def __init__(self, generator=None):
        self.generator = generator

    def generate(self, obj, args):
        assert self.generator
        if isinstance(obj, list):
            obj_list = obj
        else:
            obj_list = [obj]
        new_obj_list = list()
        for obj in obj_list:
            if isinstance(obj, list):
                new_obj = self.generate(obj, obj_list, args)
            else:
                new_obj = self.generator.generate(obj, args)
            if isinstance(new_obj, list):
                new_obj_list.extend(new_obj)
            else:
                new_obj_list.append(new_obj)
        return new_obj_list


class ParallelListGenerator(object):

    def __init__(self, generator_list=None):
        self.generator_list = generator_list if generator_list else list()

    def generate(self, obj, args):
        new_obj_list = list()
        for generator in self.generator_list:
            obj_copy = copy.deepcopy(obj)
            new_obj = generator.generate(obj_copy, args)
            new_obj_list.append(new_obj)
        return new_obj_list
