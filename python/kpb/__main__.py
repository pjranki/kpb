import os
import sys
import argparse

from generator.list_generator import ListGenerator, IterateListGenerator, ParallelListGenerator
from generator.file_generator import FileGenerator, FileWriteTextGenerator
from generator.yaml_generator import YamlFileToDictGenerator
from generator.ksy_generator import KsyPrintFileName, KsyLoadImportsToDictGenerator, KsyCompileListToDictGenerator, KsyTypeFixGenerator, KsyFileToDictGenerator
from generator.compiler_generator import CompilerGenerator
from generator.ksy_generator import KsyNameDeconfictGenerator

from lang.kil.kil_compiler import KilCompiler, KilRecompileToReturnBuffer, KilRecompileExceptionsToReturns
from lang.kil.kil_name_deconfictor import KilNameDeconflictor
from lang.c.c_compiler import CCompiler
from lang.c.c_generator import CHeaderGenerator, CSourceGenerator
from lang.cpp_stl.cpp_stl_compiler import CppStlCompiler
from lang.cpp_stl.cpp_stl_generator import CppStlHeaderGenerator, CppStlSourceGenerator
from lang.python3.python3_compiler import Python3Compiler
from lang.python3.python3_generator import Python3SourceGenerator


KIL_GENERATOR = ListGenerator([
    IterateListGenerator(FileGenerator(ext='.ksy')),
    KsyFileToDictGenerator(
        yaml_to_dict_generator=YamlFileToDictGenerator(),
        ext='.ksy',
    ),
    IterateListGenerator(YamlFileToDictGenerator(add_file_path_key='-file')),
    IterateListGenerator(KsyTypeFixGenerator()),
    IterateListGenerator(KsyNameDeconfictGenerator(KilNameDeconflictor())),
    KsyLoadImportsToDictGenerator(
        ListGenerator([
            YamlFileToDictGenerator(add_file_path_key='-file'),
            KsyTypeFixGenerator(),
            KsyNameDeconfictGenerator(KilNameDeconflictor())
        ]),
        file_path_key='-file',
        ext='.ksy',
        add_import_map_key='-import-map'),
    KsyCompileListToDictGenerator(add_compile_list_key='-compile-list'),
    IterateListGenerator(CompilerGenerator(KilCompiler(file_path_key='-file', import_map_key='-import-map', compile_list_key='-compile-list'))),
])

C_GENERATOR = ListGenerator([
    KIL_GENERATOR,
    IterateListGenerator(CompilerGenerator(KilRecompileExceptionsToReturns())),
    IterateListGenerator(CompilerGenerator(KilRecompileToReturnBuffer(method_name='serialize_to_string'))),
    IterateListGenerator(CompilerGenerator(CCompiler(header_ext='.h'))),
    IterateListGenerator(ParallelListGenerator([
        FileWriteTextGenerator(CHeaderGenerator(), file_name_attr='name', ext='.h'),
        FileWriteTextGenerator(CSourceGenerator(), file_name_attr='name', ext='.c'),
    ]))
])

CPP_STL_GENERATOR = ListGenerator([
    KIL_GENERATOR,
    IterateListGenerator(CompilerGenerator(CppStlCompiler(header_ext='.hpp'))),
    IterateListGenerator(ParallelListGenerator([
        FileWriteTextGenerator(CppStlHeaderGenerator(), file_name_attr='name', ext='.hpp'),
        FileWriteTextGenerator(CppStlSourceGenerator(), file_name_attr='name', ext='.cpp'),
    ]))
])

PYTHON3_GENERATOR = ListGenerator([
    KIL_GENERATOR,
    IterateListGenerator(CompilerGenerator(KilRecompileToReturnBuffer(method_name='serialize_to_string'))),
    IterateListGenerator(CompilerGenerator(Python3Compiler())),
    IterateListGenerator(FileWriteTextGenerator(Python3SourceGenerator(), file_name_attr='name', ext='.py')),
])

ALL_GENERATOR = ParallelListGenerator([
    C_GENERATOR,
    CPP_STL_GENERATOR,
    PYTHON3_GENERATOR,
])

GENERATORS = {
    'c': C_GENERATOR,
    'cpp_stl': CPP_STL_GENERATOR,
    'python': PYTHON3_GENERATOR,
    'all': ALL_GENERATOR,
}

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--target', required=True, choices=GENERATORS.keys())
    parser.add_argument('--outdir', default=os.getcwd())
    parser.add_argument('--namespace', default=None)
    parser.add_argument('--import-path', default='', type=str)
    parser.add_argument('file', nargs='+')
    args = parser.parse_args()
    args.import_path = [path.strip() for path in args.import_path.split(';') if path.strip() != '']
    GENERATORS[args.target].generate(args.file, args)


if __name__ == '__main__':
    main()
