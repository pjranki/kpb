import os
import sys
import struct


KPB_IO_RANDOM_FILL = None


class KpbIOError(Exception):
    pass


class KpbIO(object):

    class Encoding(object):
        ENCODING_ASCII = 'LATIN1'  # allows all chars in range 0x00 -> 0xff ('ASCII' in python does not)
        ENCODING_UTF16LE = 'UTF-16LE'
        ENCODING_UTF16BE = 'UTF-16BE'
        ENCODING_UTF8= 'UTF-8'

    @classmethod
    def get_mask_ones(cls, n):
        if n == 64:
            return 0xFFFFFFFFFFFFFFFF
        return (1 << n) - 1

    @classmethod
    def get_unsigned(cls, _value, n):
        if _value < 0:
            _value = cls.get_mask_ones(n) + 1 + _value
        return _value & cls.get_mask_ones(n)

    @classmethod
    def get_signed(cls, _value, n):
        max_signed_value = cls.get_mask_ones(n - 1)
        if _value > max_signed_value:
            _value = ((~(_value) & cls.get_mask_ones(n)) + 1) * -1
        return _value

    def __init__(self):
        # owner
        self.stream_owner = self
        self.write_mode = True

        # root
        self.stream_root = None

        # current stream
        self.stream_current = self
        self.stream_next = None
        self.stream_prev = None
        self.consume_on_pop = False

        # current stream data
        self.stream_data = self
        self.buffer = b''

        # current stream view
        self.stream_view = self
        self.view_size_fixed = False
        self.view_offset = 0
        self.view_size = 0
        self.pos_ = 0
        self.bits_left = 0
        self.bits = 0

        # fill with 0s or 1s
        self.fill = False
        self.fill_byte = None
        self.fill_eof = False

    @property
    def writable(self):
        return self.stream_owner.write_mode

    @property
    def readonly(self):
        return not self.stream_owner.write_mode

    def set_readonly(self, data):
        assert isinstance(data, bytes)

        # cannot call set readonly from any other stream accept the owner
        if self.stream_owner != self:
            raise KpbIOError("Can only call set_readonly on the stream's owner")

        # change data and view settings to the current stream (rather than referencing other streams)
        self.stream_data = self.stream_current
        self.stream_view = self.stream_current

        # disable writes to steam (and sub-streams)
        self.stream_owner.write_mode = False

        # assign & position the fixed data to the stream
        self.stream_data.buffer = data[:]
        self.stream_view.view_offset = 0
        self.stream_view.view_size_fixed = True
        self.stream_view.view_size = len(data)
        self.stream_view.pos_ = 0
        self.stream_view.bits_left = 0
        self.stream_view.bits = 0

    def set_readonly_or_fill(self, data, fill):
        self.set_readonly(data)
        self.fill = True
        self.fill_byte = b'\xff' if fill > 0 else b'\x00'
        self.fill_eof = False

    def set(self, data):
        assert isinstance(data, bytes)

        # cannot call set from any other stream accept the owner
        if self.stream_owner != self:
            raise KpbIOError("Can only call set on the stream's owner")

        # keep the fixed/unfixed stream setting
        view_size_fixed = self.stream_view.view_size_fixed

        # change data and view settings to the current stream (rather than referencing other streams)
        self.stream_current.stream_data = self.stream_current # input data is stored in current stream object
        self.stream_current.stream_view = self.stream_current # changes to view are made to current stream object
        self.stream_data = self.stream_current.stream_data
        self.stream_view = self.stream_current.stream_view

        # write no-longer changes the parent stream, so we need to write the buffer on pop
        parent = self.stream_current.stream_prev
        if parent and self.writable:
            self.stream_current.consume_on_pop = True

        # assign & position the fixed data to the stream
        self.stream_data.buffer = data[:]
        self.stream_view.view_offset = 0
        self.stream_view.view_size_fixed = view_size_fixed
        self.stream_view.view_size = len(data) if view_size_fixed else 0
        self.stream_view.pos_ = 0
        self.stream_view.bits_left = 0
        self.stream_view.bits = 0

        # and position the buffer accordingly
        if self.writable:
            # buffer has been written, and position is at the end
            self.stream_view.pos_ = len(data)
        else:
            # reading of this new buffer is at the start
            self.stream_view.pos_ = 0

    def get(self):
        self.align_to_byte()
        start = self.stream_view.view_offset
        end = start + self.size
        return self.stream_data.buffer[start: end]

    def get_size(self):
        self.align_to_byte()
        return self.size

    @property
    def ok(self):
        # Not needed in python, we use exceptions
        # But is still called in generated code
        return True

    @property
    def root(self):
        if self.stream_owner.stream_root:
            return self.stream_owner.stream_root

        # create a new stream to represent the root (as the owner's data/view context can change)
        root = KpbIO()
        root.stream_owner = self.stream_owner
        if self.stream_owner.stream_next:
            root.stream_prev = self.stream_owner.stream_next.stream_prev
        root.stream_next = self.stream_owner.stream_next
        if root.stream_next:
            root.stream_next.stream_prev = root
        self.stream_owner.stream_next = root

        # root stream is just an alias to the original stream
        root.stream_data = self.stream_owner
        root.stream_view = self.stream_owner

        # use this new stream as the root in future
        self.stream_owner.stream_root = root

        # move all sub-streams to use root
        stream = self.stream_owner
        while stream:
            # don't adjust the root stream (just all the other streams)
            if stream != root:
                if stream.stream_current == self.stream_owner:
                    stream.stream_current = root
            stream = stream.stream_next
        return self.stream_owner.stream_root

    @property
    def parent(self):
        parent = None
        if self == self.stream_owner:
            parent = self.stream_owner.stream_current.stream_prev
        else:
            parent = self.stream_prev
        if not parent:
            parent = self.root
        return parent

    def push_substream(self, substream, set_pos, pos, set_size, size):
        assert isinstance(substream, KpbIO)

        # cannot call push from any other stream accept the owner
        if self.stream_owner != self:
            raise KpbIOError("Can only call push_substream on the stream's owner")

        # current active stream will become the parent
        parent = self.stream_current
        if parent == self.stream_owner:
            # if parent points to the owner, treat it as the root
            parent = self.root

        # if substream is the owner, we are being asked to use the current (active) substream
        target = substream
        if target == self.stream_owner:
            if self.stream_current == self.stream_owner:
                target = self.root
            else:
                target = self.stream_current

        # flush bits & make sure parent stream is byte-aligned
        self.align_to_byte()

        # creating & link stream into the list of streams
        new_stream = KpbIO()
        new_stream.stream_owner = self.stream_owner
        new_stream.stream_next = None
        new_stream.stream_prev = parent
        parent.stream_next = new_stream

        # additional checks when working with a fixed read-only buffer (i.e. parsing)
        if self.readonly:
            start = pos if set_pos else target.pos
            end = start + (size if set_size else 0)

            # in read-only, cannot set a position & size outside the stream
            if end > target.size:
                if not self.fill:
                    raise KpbIOError("Setting position/size outside read-only stream")
                else:
                    end = target.size

        # this is the offset of the view we want (it must begin at the start of the target stream)
        view_offset = target.stream_view.view_offset
        if set_pos:
            # place the view at this offset into the target's stream
            view_offset += pos
        else:
            # we are reading/writing after the current position of the target stream
            # place the view at the current position of the target stream
            view_offset += target.stream_view.pos_

        # this is the size of the view we want
        view_size_fixed = False    # keep writing until the end of the stream
        view_size = 0
        if set_size:
            view_size_fixed = True
            view_size = size

        # further restrict the view if the target's stream is also restricted
        if target.stream_view.view_size_fixed:
            # maximum allowed offset of a new view inside the target
            view_offset_max = target.stream_view.view_offset + target.stream_view.view_size
            if view_offset > view_offset_max:
                view_offset = view_offset_max

            # maximum allowed size of a new view inside the target stream
            view_size_max = view_offset_max - view_offset
            if not view_size_fixed:
                # no restriction was asked for, make all target data available
                view_size_fixed = True
                view_size = view_size_max
            if view_size > view_size_max:
                view_size = view_size_max

        # determine the required size of the backing buffer (and fill with zeros if needed)
        required_buffer_size = view_offset + view_size
        if len(target.stream_data.buffer) < required_buffer_size:
            if self.writable:
                # resize if writing
                target.stream_data.buffer += b'\x00' * (required_buffer_size - len(target.stream_data.buffer))
            else:
                # error if reading (cannot invent bytes out of nowhere)
                raise KpbIOError("Position/size is passed the end of the stream")

        # data will be read/written directly to/from target stream
        new_stream.stream_data = target.stream_data

        # view of the new stream's data which will be restricted
        new_stream.stream_view = new_stream
        new_stream.stream_view.view_offset = view_offset
        new_stream.stream_view.view_size_fixed = view_size_fixed
        new_stream.stream_view.view_size = view_size

        # only consume data if we are reading/writing sequentially (not at a new position)
        new_stream.consume_on_pop = not set_pos

        # make this stream active
        self.stream_current = new_stream
        self.stream_data = self.stream_current.stream_data
        self.stream_view = self.stream_current.stream_view

    def pop_substream(self):
        # cannot call pop from any other stream accept the owner
        # cannot call push from any other stream accept the owner
        if self.stream_owner != self:
            raise KpbIOError("Can only call pop_substream on the stream's owner")

        # must have a parent to pop
        if not self.stream_current.stream_prev:
            raise KpbIOError("There is no stream to pop")

        # flush bits & and byte align before writing parent stream or re-positioning it
        self.align_to_byte()

        # parent stream
        parent = self.stream_current.stream_prev

        # consume data??
        if self.stream_current.consume_on_pop:
            if self.writable and (parent.stream_data != self.stream_data):
                # write changes into parent
                if self.stream_view.view_size_fixed:
                    # size: <value>
                    parent.write_bytes(self.stream_data.buffer, self.stream_view.view_size)
                else:
                    # size-eos: true
                    parent.write_bytes(self.stream_data.buffer, len(self.stream_data.buffer))
            else:
                # move position forward in the parent stream
                if self.stream_view.view_size_fixed:
                    # size: <value>
                    parent.stream_view.pos_ += self.stream_view.view_size
                else:
                    # size-eos: true
                    parent.stream_view.pos_ += self.stream_view.pos_

        # move back to previously active stream
        unused_stream = self.stream_current
        self.stream_current = self.stream_current.stream_prev
        self.stream_data = self.stream_current.stream_data
        self.stream_view = self.stream_current.stream_view
        self.stream_current.stream_next = None

        # delete the unused stream
        # (nothing to do)

    @property
    def size(self):
        # if the offset into the buffer for the view is beyound the buffer, the view has no space
        if self.stream_view.view_offset > len(self.stream_data.buffer):
            return 0

        # size can be determined by the size of the buffer (starting from where the view starts)
        size = len(self.stream_data.buffer) - self.stream_view.view_offset

        # if the size of the view of the buffer is fixed - we know the size
        if self.stream_view.view_size_fixed:
            # for safety, only use the view size if the stream has enough data in it
            if size > self.stream_view.view_size:
                size = self.stream_view.view_size

        # done
        return size

    @property
    def pos(self):
        # if position is outside, cap at very end of stream
        if self.stream_view.pos_ > self.size:
            return self.size
        return self.stream_view.pos_

    @property
    def bit_size(self):
        if self.writable:
            return (self.size * 8) + self.stream_view.bits_left
        else:
            # FUTURE: update this when we support bit streams of bit-size
            return self.size * 8

    @property
    def bit_pos(self):
        bits_left = self.stream_view.bits_left
        if self.writable:
            return (self.pos * 8) + bits_left
        if bits_left == 0:
            return (self.pos * 8)
        assert bits_left <= 8
        # in read-only, bits_left is how many bits we can use (not where we are)
        bits_left = 8 - bits_left
        # -1 because we moved forward 8-bits when we filled the bit register
        return ((self.pos - 1) * 8) + bits_left

    @property
    def eof(self):
        return self.stream_view.fill_eof or ((self.pos >= self.size) and (self.stream_view.bits_left == 0))

    @property
    def remaining(self):
        if self.size > self.pos:
            return self.size - self.pos
        return 0

    def align_to_byte(self):
        if 0 == self.stream_view.bits_left:
            self.stream_view.bits_left = 0
            self.stream_view.bits = 0
            return
        if self.writable:
            if self.stream_view.bits_left >= 8:
                raise NotImplementedError('More than 8 bits left, this should never happen')
            left = self.stream_view.bits
            left <<= (8 - self.stream_view.bits_left)
            left &= 0xff
            self.write(struct.pack('B', left))
        self.stream_view.bits_left = 0
        self.stream_view.bits = 0

    def read_s1(self):
        self.align_to_byte()
        return struct.unpack('b', self.read(1))[0]

    def read_s2be(self):
        self.align_to_byte()
        return struct.unpack('>h', self.read(2))[0]

    def read_s4be(self):
        self.align_to_byte()
        return struct.unpack('>i', self.read(4))[0]

    def read_s8be(self):
        self.align_to_byte()
        return struct.unpack('>q', self.read(8))[0]

    def read_s2le(self):
        self.align_to_byte()
        return struct.unpack('<h', self.read(2))[0]

    def read_s4le(self):
        return struct.unpack('<i', self.read(4))[0]

    def read_s8le(self):
        self.align_to_byte()
        return struct.unpack('<q', self.read(8))[0]

    def read_u1(self):
        self.align_to_byte()
        return struct.unpack('B', self.read(1))[0]

    def read_u2be(self):
        self.align_to_byte()
        return struct.unpack('>H', self.read(2))[0]

    def read_u4be(self):
        self.align_to_byte()
        return struct.unpack('>I', self.read(4))[0]

    def read_u8be(self):
        self.align_to_byte()
        return struct.unpack('>Q', self.read(8))[0]

    def read_u2le(self):
        self.align_to_byte()
        return struct.unpack('<H', self.read(2))[0]

    def read_u4le(self):
        self.align_to_byte()
        return struct.unpack('<I', self.read(4))[0]

    def read_u8le(self):
        self.align_to_byte()
        return struct.unpack('<Q', self.read(8))[0]

    def read_f4le(self):
        self.align_to_byte()
        return struct.unpack('<f', self.read(4))[0]

    def read_f8le(self):
        self.align_to_byte()
        return struct.unpack('<d', self.read(8))[0]

    def read_f4be(self):
        self.align_to_byte()
        return struct.unpack('>f', self.read(4))[0]

    def read_f8be(self):
        self.align_to_byte()
        return struct.unpack('>d', self.read(8))[0]

    def read_bits_int_be(self, n):
        value = 0
        while n > 0:
            if self.stream_view.bits_left == 0:
                value_byte = struct.unpack('B', self.read(1))[0]
                self.stream_view.bits = value_byte & 0xff
                self.stream_view.bits_left = 8
            n -= 1
            self.stream_view.bits_left -= 1
            value <<= 1
            value &=~ 1
            value |= (self.stream_view.bits >> self.stream_view.bits_left) & 0x1
        return value

    def read_bits_int(self, n):
        return self.read_bits_int_be(n)

    def read_bits_int_le(self, n):
        # TODO: implemented bits le
        raise NotImplementedError

    def read_char_str(self, size, term, use_size, include_term, consume, src_enc):
        if not consume:
            raise NotImplementedError
        self.align_to_byte()
        if isinstance(term, int):
            term = chr(term)
        term_data = term.encode(src_enc)
        if not use_size:
            orig_pos = self.stream_view.pos_
            size = 0
            while True:
                current_data = self.read(len(term_data))
                size += len(current_data)
                if current_data == term_data:
                    break
            self.stream_view.pos_ = orig_pos
        str_data = self.read(size)
        # TODO: handle include_term
        nul_data = b'\x00' * len(term_data)
        nul_pos = -1
        for i in range(0, len(str_data), len(nul_data)):
            if str_data[i: i + len(nul_data)] == nul_data:
                nul_pos = i
                break
        if nul_pos != -1:
            str_data = str_data[:nul_pos]
        str_ = str_data.decode(src_enc)
        return str_

    def read_bytes(self, size):
        self.align_to_byte()
        return self.read(size)

    def write_s1(self, _value):
        self.align_to_byte()
        self.write(struct.pack('B', self.get_unsigned(_value, 8)))

    def write_s2be(self, _value):
        self.align_to_byte()
        self.write(struct.pack('>H', self.get_unsigned(_value, 16)))

    def write_s4be(self, _value):
        self.align_to_byte()
        self.write(struct.pack('>I', self.get_unsigned(_value, 32)))

    def write_s8be(self, _value):
        self.align_to_byte()
        self.write(struct.pack('>Q', self.get_unsigned(_value, 64)))

    def write_s2le(self, _value):
        self.align_to_byte()
        self.write(struct.pack('<H', self.get_unsigned(_value, 16)))

    def write_s4le(self, _value):
        self.align_to_byte()
        self.write(struct.pack('<I', self.get_unsigned(_value, 32)))

    def write_s8le(self, _value):
        self.align_to_byte()
        self.write(struct.pack('<Q', self.get_unsigned(_value, 64)))

    def write_u1(self, _value):
        self.align_to_byte()
        self.write(struct.pack('B', self.get_unsigned(_value, 8)))

    def write_u2be(self, _value):
        self.align_to_byte()
        self.write(struct.pack('>H', self.get_unsigned(_value, 16)))

    def write_u4be(self, _value):
        self.align_to_byte()
        self.write(struct.pack('>I', self.get_unsigned(_value, 32)))

    def write_u8be(self, _value):
        self.align_to_byte()
        self.write(struct.pack('>Q', self.get_unsigned(_value, 64)))

    def write_u2le(self, _value):
        self.align_to_byte()
        self.write(struct.pack('<H', self.get_unsigned(_value, 16)))

    def write_u4le(self, _value):
        self.align_to_byte()
        self.write(struct.pack('<I', self.get_unsigned(_value, 32)))

    def write_u8le(self, _value):
        self.align_to_byte()
        self.write(struct.pack('<Q', self.get_unsigned(_value, 64)))

    def write_f4le(self, _value):
        self.align_to_byte()
        self.write(struct.pack('<f', _value))

    def write_f8le(self, _value):
        self.align_to_byte()
        self.write(struct.pack('<d', _value))

    def write_f4be(self, _value):
        self.align_to_byte()
        self.write(struct.pack('>f', _value))

    def write_f8be(self, _value):
        self.align_to_byte()
        self.write(struct.pack('>d', _value))

    def write_bits_int_be(self, n, _bits):
        while n > 0:
            while self.stream_view.bits_left >= 8:
                value = (self.stream_view.bits >> (self.stream_view.bits_left - 8)) & 0xff
                self.write(struct.pack('B', self.get_unsigned(value, 8)))
                # no need to zero top bits
                self.stream_view.bits_left -= 8
            n -= 1
            self.stream_view.bits_left += 1  # this might take us back up to 8-bits
            self.stream_view.bits <<= 1
            self.stream_view.bits &=~ 1
            self.stream_view.bits |= (_bits >> n) & 1
        if self.stream_view.bits_left >= 8:  # if back up to 8-bits - flush
            value = (self.stream_view.bits >> (self.stream_view.bits_left - 8)) & 0xff
            self.write(struct.pack('B', self.get_unsigned(value, 8)))
            # no need to zero top bits
            self.stream_view.bits_left -= 8

    def write_bits_int(self, n, _bits):
        self.write_bits_int_be(n, _bits)

    def write_bits_int_le(self, n, _bits):
        # TODO: implemented bits le
        raise NotImplementedError

    def write_char_str(self, str_, size, term, use_size, include_term, src_enc):
        self.align_to_byte()
        if use_size and 0 == size:
            return
        if isinstance(term, int):
            term = chr(term)
        term_data = term.encode(src_enc)
        term_pos = str_.find(term)
        if term_pos != -1:
            str_ = str_[:term_pos]
        str_data = str_.encode(src_enc)
        if not str_data.endswith(term_data) and include_term:
            if use_size:
                if size < len(term_data):
                    raise KpbIOError("strz too small to store terminator")
                str_data = str_data[:size - len(term_data)]
            str_data += term_data
        if use_size and len(str_data) > size:
            str_data = str_data[:size]
        while use_size and len(str_data) < size:
            if size - len(str_data) == 1:
                str_data += b'\x00'
            else:
                str_data += term_data
        self.write(str_data)

    def write_bytes(self, data, write_size):
        self.align_to_byte()
        if len(data) < write_size:
            data += b'\x00' * (write_size - len(data))
        self.write(data[:write_size])

    def read(self, size):
        # cannot read from a writable stream
        if self.writable:
            raise KpbIOError("Can't read from a write IO stream")

        # how to handle not enough data for read
        padding = b''
        if size > self.remaining:
            if not self.fill:
                raise KpbIOError("Not enought bytes in stream")
            else:
                self.stream_view.fill_eof = True
                padding = self.fill_byte * (size - self.remaining)
                size = self.remaining

        # read & move stream
        start = self.stream_view.view_offset + self.stream_view.pos_
        end = start + size
        data = self.stream_data.buffer[start: end]
        assert len(data) == size
        data = data + padding
        assert len(data) == size + len(padding)
        self.stream_view.pos_ += size  # TODO: need to move through padding if fill=true
        return data


    def write(self, data):
        assert isinstance(data, bytes)
        size = len(data)

        # cannot write a read-only stream
        if self.readonly:
            raise KpbIOError("Can't write to a read IO stream")

        # resize buffer OR cap the size of the write
        if self.stream_view.view_size_fixed:
            # cap the write for fixed-sized buffers
            size = size if size < self.remaining else self.remaining
        else:
            # fill with zeros until the buffer reaches the desired write position
            if self.stream_view.pos_ > self.size:
                self.stream_data.buffer += b'\x00' * (self.stream_view.pos_ - self.size)

        # stop here if there is nothing to write
        if size == 0:
            return

        # place data within existing buffer
        view_start_offset = self.stream_view.view_offset
        view_end_offset = view_start_offset + self.size
        view_prefix = self.stream_data.buffer[: view_start_offset]
        view_suffix = b''
        if view_end_offset < len(self.stream_data.buffer):
            view_suffix = self.stream_data.buffer[view_end_offset:]
        start_offset = self.pos
        end_offset = start_offset + size
        prefix = self.stream_data.buffer[view_start_offset: view_start_offset + start_offset]
        suffix = b''
        if end_offset < self.size:
            suffix = self.stream_data.buffer[view_start_offset + end_offset: view_end_offset]
        self.stream_data.buffer = view_prefix + prefix + data[:size] + suffix + view_suffix
        self.stream_view.pos_ += size

    @classmethod
    def random_api(cls, api=None):
        global KPB_IO_RANDOM_FILL
        KPB_IO_RANDOM_FILL = api

    @classmethod
    def random_fill(cls, size):
        global KPB_IO_RANDOM_FILL
        if not KPB_IO_RANDOM_FILL:
            raise Exception("random api not set")
        return KPB_IO_RANDOM_FILL(size)

    @classmethod
    def order_min_max(cls, a, b):
        if a > b:
            tmp = b
            b = a
            a = tmp
        return a, b

    @classmethod
    def random(cls, fmt, a, b):
        if a == b:
            return a
        assert fmt.lower() in ['b', 'h', 'i', 'q']
        unsigned = fmt in ['B', 'H', 'I', 'Q']
        fmt = '>{}'.format(fmt)
        a, b = cls.order_min_max(a, b)
        value = struct.unpack(fmt.upper(), cls.random_fill(struct.calcsize(fmt)))[0]
        if unsigned:
            minv = 1<<((struct.calcsize(fmt)*8)-1)
            maxv = (1<<((struct.calcsize(fmt)*8)-1))-1
            maxv = minv | maxv
            minv = 0
            assert minv == 0
            assert maxv > 0
        else:
            minv = struct.unpack(fmt, struct.pack(fmt.upper(), 1<<((struct.calcsize(fmt)*8)-1)))[0]
            maxv = (1<<((struct.calcsize(fmt)*8)-1))-1
            assert minv < 0
            assert maxv > 0
        if (a == minv) and (b == maxv):
            return value + a
        range_ = b - a + 1
        value = (value % range_) + a
        assert value >= a
        assert value <= b
        return value

    @classmethod
    def random_s1(cls, min_inclusive, max_inclusive):
        return cls.random('b', min_inclusive, max_inclusive)

    @classmethod
    def random_s2(cls, min_inclusive, max_inclusive):
        return cls.random('h', min_inclusive, max_inclusive)

    @classmethod
    def random_s4(cls, min_inclusive, max_inclusive):
        return cls.random('i', min_inclusive, max_inclusive)

    @classmethod
    def random_s8(cls, min_inclusive, max_inclusive):
        return cls.random('q', min_inclusive, max_inclusive)

    @classmethod
    def random_u1(cls, min_inclusive, max_inclusive):
        return cls.random('B', min_inclusive, max_inclusive)

    @classmethod
    def random_u2(cls, min_inclusive, max_inclusive):
        return cls.random('H', min_inclusive, max_inclusive)

    @classmethod
    def random_u4(cls, min_inclusive, max_inclusive):
        return cls.random('I', min_inclusive, max_inclusive)

    @classmethod
    def random_u8(cls, min_inclusive, max_inclusive):
        return cls.random('Q', min_inclusive, max_inclusive)

    @classmethod
    def random_f4(cls, min_inclusive, max_inclusive):
        min_inclusive, max_inclusive = cls.order_min_max(min_inclusive, max_inclusive)
        range_ = max_inclusive - min_inclusive
        random_range = 0xffffffffffffffff
        random_value = cls.random_u8(0, random_range)
        if random_value == 0:
            return min_inclusive
        if random_value == random_range:
            return max_inclusive
        offset = (float(random_value) * range_) / float(random_range)
        value = min_inclusive + offset
        if value < min_inclusive:
            return min_inclusive
        if value > max_inclusive:
            return max_inclusive
        return value

    @classmethod
    def random_f8(cls, min_inclusive, max_inclusive):
        min_inclusive, max_inclusive = cls.order_min_max(min_inclusive, max_inclusive)
        range_ = max_inclusive - min_inclusive
        random_range = 0xffffffffffffffff
        random_value = cls.random_u8(0, random_range)
        if random_value == 0:
            return min_inclusive
        if random_value == random_range:
            return max_inclusive
        offset = (float(random_value) * range_) / float(random_range)
        value = min_inclusive + offset
        if value < min_inclusive:
            return min_inclusive
        if value > max_inclusive:
            return max_inclusive
        return value

    @classmethod
    def random_bool(cls, min_inclusive, max_inclusive):
        if min_inclusive == max_inclusive:
            return min_inclusive
        return (cls.random_u1(0x00, 0xff) & 0x80) != 0

    @classmethod
    def random_size(cls, min_inclusive, max_inclusive):
        if min_inclusive > 0xffffffff:
            min_inclusive = 0xffffffff
        if max_inclusive > 0xffffffff:
            max_inclusive = 0xffffffff
        return cls.random_u4(min_inclusive, max_inclusive)

    @classmethod
    def random_bytes(cls, min_size, max_size, min_byte, max_byte):
        size = cls.random_size(min_size, max_size)
        data_list = list()
        for _ in range(0, size):
            value = struct.pack('<B', cls.random_u1(min_byte, max_byte))
            data_list.append(value)
        data = b''.join(data_list)
        assert len(data) == size
        return data

    @classmethod
    def random_char(cls, min_char, max_char, max_size_in_bytes, enc):
        if enc == cls.Encoding.ENCODING_ASCII:
            value = cls.random_u1(min_char, max_char)
            data = struct.pack('B', value)
            return data.decode(enc)
        if enc == cls.Encoding.ENCODING_UTF16LE or enc == cls.Encoding.ENCODING_UTF16BE:
            value = cls.random_u2(min_char, max_char)
            fmt = '<H' if enc == cls.Encoding.ENCODING_UTF16LE else '>H'
            data = struct.pack(fmt, value)
            if (value >= 0xdc00) and (value <= 0xdfff):
                value -= 0x400  # these are illegal, but I will make it legal
                data = struct.pack(fmt, value)
            if (value >= 0xd800) and (value <= 0xdbff):
                if max_size_in_bytes >= 4:
                    value2 = cls.random_u2(min_char, max_char)
                    if not ((value2 >= 0xdc00) and (value2 <= 0xdfff)):
                        value2 = (value2 & 0x7ff) | 0xdc00  # these are illegal, but I will make them legal
                    data += struct.pack(fmt, value2)
                else:
                    value = (value & 0x7ff) | 0xd600  # these are illegal, but I will make them legal
                    data = struct.pack(fmt, value)
            return data.decode(enc)
        if enc == cls.Encoding.ENCODING_UTF8:
            data = b''
            value = cls.random_u1(min_char, max_char)  # TODO: not correct when min/max is a uint32
            data = struct.pack('B', value)
            if (value & 0x80) != 0x80:
                # single byte utf-8
                return data.decode(enc)
            if max_size_in_bytes <= 1:
                # force 1-byte utf-8
                value = value &~ 0x80
                data = struct.pack('B', value)
                return data.decode(enc)
            if (value & 0xc0) == 0x80:
                # these are illegal, but I will make them legal
                value = (value & 0x1f) | 0xc0  # make it a 2-byte utf-8 character
                data = struct.pack('B', value)
            if (value & 0xf8) == 0xf8:
                # these are illegal, but I will make them legal
                value = (value & 0x7) | 0xf0  # make it a 4-byte utf-8 character
                data = struct.pack('B', value)
            if (value & 0xe0) == 0xc0:
                size_in_bytes = 2
            elif (value & 0xf0) == 0xe0:
                size_in_bytes = 3
            elif (value & 0xf8) == 0xf0:
                size_in_bytes = 4
            else:
                raise NotImplementedError('this should never happen')
            if size_in_bytes > max_size_in_bytes:
                if max_size_in_bytes <= 2:
                    # force 2-byte utf-8
                    value = (value & 0x1f) | 0xc0
                    data = struct.pack('B', value)
                    size_in_bytes = 2
                elif max_size_in_bytes <= 3:
                    # force 3-byte utf-8
                    value = (value & 0x0f) | 0xe0
                    data = struct.pack('B', value)
                    size_in_bytes = 3
            if size_in_bytes == 2:
                if value == 0xc0 or value == 0xc1:
                    # these are illegal, but I will make them legal
                    value = 0xc2
                    data = struct.pack('B', value)
            if size_in_bytes == 3:
                if value == 0xe0:
                    # these are illegal, but I will make them legal
                    value = 0xe1
                    data = struct.pack('B', value)
                if value == 0xed:
                    # these are illegal, but I will make them legal
                    value = 0xee
                    data = struct.pack('B', value)
            if size_in_bytes == 4:
                if value == 0xf0:
                    # these are illegal, but I will make them legal
                    value = 0xf1
                    data = struct.pack('B', value)
            for i in range(1, size_in_bytes):
                value_next = (cls.random_u1(min_char, max_char) & 0x3f) | 0x80  # TODO: not correct when min/max is a uint32
                if i == 1 and ((value == 0xf4 and value_next >= 0x90) or (value > 0xf4)):
                    # these are illegal, but I will make them legal
                    value = 0xf3
                    data = struct.pack('B', value)
                data += struct.pack('B', value_next)
            assert len(data) == size_in_bytes
            return data.decode(enc)
        raise NotImplementedError(enc)

    @classmethod
    def random_char_str(cls, min_size, max_size, min_char, max_char, enc):
        assert max_char <= 0xffffffff
        size = cls.random_size(min_size, max_size)
        data = b''
        while len(data) < size:
            value = cls.random_char(min_char, max_char, size - len(data), enc).encode(enc)
            data += value
        assert len(data) == size
        data = data.decode(enc)
        data = data.split('\x00')[0]
        return data

    @classmethod
    def random_wchar_str(cls, min_size, max_size, min_char, max_char, enc):
        assert max_char <= 0xffffffff
        size = cls.random_size(min_size, max_size)
        size &=~ 0x1  # even byte count
        data = b''
        while len(data) < size:
            assert len(data) % 2 == 0
            value = cls.random_char(min_char, max_char, size - len(data), enc).encode(enc)
            assert len(value) == 2 or len(value) == 4
            data += value
        assert len(data) == size
        data = data.decode(enc)
        data = data.split('\x00')[0]
        return data
