import os
import sys
import subprocess

root = os.path.abspath(os.path.dirname('__file__'))
script = os.path.join(root, 'tutorial', 'build_tutorial.py')
assert os.path.exists(script)
p = subprocess.Popen([sys.executable, script], shell=(os.name=='nt'))
p.communicate()
