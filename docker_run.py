import os
import sys
import platform

if __name__ == "__main__":
    if platform.processor() == "arm":
        platform_directive = "--platform=linux/amd64"
    else:
        platform_directive = ""

    cmd = f"docker run {platform_directive} -i -t -u user -v ${{PWD}}:/home/user/kpb -w /home/user/kpb kpb"
    if os.name == "nt":
        cmd = f'powershell -Command "{cmd}"'
    sys.exit(os.system(cmd))
