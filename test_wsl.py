import os
import sys

root = os.path.abspath(os.path.dirname('__file__'))
assert os.path.exists(root)
os.chdir(root)
sys.exit(os.system('bash -c "python3 test.py"'))
