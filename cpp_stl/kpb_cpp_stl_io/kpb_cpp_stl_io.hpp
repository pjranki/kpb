#ifndef _H_KPB_IO_CPP_STL_H_
#define _H_KPB_IO_CPP_STL_H_

#include <cstdint>
#include <vector>
#include <string>
#include <functional>

namespace kpb {

class IO {

    public:

        enum Encoding
        {
            ENCODING_ASCII = 0,
            ENCODING_UTF16LE = 1,
            ENCODING_UTF16BE = 2,
            ENCODING_UTF8 = 3,
        };

        typedef int16_t utf16_t;
        static utf16_t utf16_wchar(wchar_t chr, Encoding enc);
        static wchar_t wchar_utf16(utf16_t chr, Encoding enc);
        static std::basic_string<utf16_t> utf16_dupwchar(const std::wstring &str, Encoding enc);
        static size_t utf16_len(const utf16_t *str);
        static const utf16_t *utf16_chr(const utf16_t *str, utf16_t chr);

        IO();
        IO(const IO &) = delete;
        IO &operator=(const IO &) = delete;
        ~IO();

        void set_error();
        bool ok() const;

        bool writable() const;
        bool readonly() const;

        void set_readonly(const std::string &data);
        void set_readonly_or_fill(const std::string &data, uint8_t fill);
        void set(const std::string &data);
        std::string get();
        size_t get_size();

        IO &root();
        IO &parent();

        void push_substream(IO &substream, bool set_pos, size_t pos, bool set_size, size_t size);
        void pop_substream();

        size_t size() const;
        size_t pos() const;
        size_t bit_size() const;
        size_t bit_pos() const;
        bool eof() const;

        size_t remaining() const;

        void align_to_byte();

        int8_t read_s1();
        int16_t read_s2be();
        int32_t read_s4be();
        int64_t read_s8be();
        int16_t read_s2le();
        int32_t read_s4le();
        int64_t read_s8le();
        uint8_t read_u1();
        uint16_t read_u2be();
        uint32_t read_u4be();
        uint64_t read_u8be();
        uint16_t read_u2le();
        uint32_t read_u4le();
        uint64_t read_u8le();
        float read_f4le();
        double read_f8le();
        float read_f4be();
        double read_f8be();
        uint64_t read_bits_int_be(int n);
        uint64_t read_bits_int(int n);
        uint64_t read_bits_int_le(int n);

        std::string read_char_str(size_t size, char term, bool use_size, bool include_term, bool consume, Encoding src_enc);
        std::wstring read_wchar_str(size_t size, wchar_t term, bool use_size, bool include_term, bool consume, Encoding src_enc);

        std::string read_bytes(size_t size);

        void write_s1(int8_t value);
        void write_s2be(int16_t value);
        void write_s4be(int32_t value);
        void write_s8be(int64_t value);
        void write_s2le(int16_t value);
        void write_s4le(int32_t value);
        void write_s8le(int64_t value);
        void write_u1(uint8_t value);
        void write_u2be(uint16_t value);
        void write_u4be(uint32_t value);
        void write_u8be(uint64_t value);
        void write_u2le(uint16_t value);
        void write_u4le(uint32_t value);
        void write_u8le(uint64_t value);
        void write_f4le(float value);
        void write_f8le(double value);
        void write_f4be(float value);
        void write_f8be(double value);
        void write_bits_int_be(int n, uint64_t _bits);
        void write_bits_int(int n, uint64_t _bits);
        void write_bits_int_le(int n, uint64_t _bits);

        void write_char_str(const std::string &str, size_t size, char term, bool use_size, bool include_term, Encoding src_enc);
        void write_wchar_str(const std::wstring &str, size_t size, wchar_t term, bool use_size, bool include_term, Encoding src_enc);

        void write_bytes(const std::string &data, size_t write_size);

    protected:
        bool read(void *data, size_t size);
        bool write(const void *data, size_t size);

    private:
        // owner
        IO *stream_owner_;
        bool write_mode_;
        bool ok_;

        // root
        IO *stream_root_;

        // current stream
        IO *stream_current_;
        IO *stream_next_;
        IO *stream_prev_;
        bool consume_on_pop_;

        // current stream data
        IO *stream_data_;
        std::string buffer_;

        // current stream view
        IO *stream_view_;
        bool view_size_fixed_;
        size_t view_offset_;
        size_t view_size_;
        size_t pos_;
        int bits_left_;
        uint64_t bits_;

        // fill with 0s or 1s
        bool fill_;
        uint8_t fill_byte_;
        bool fill_eof_;
};

class Finally
{
    public:
        Finally(std::function<void(void)> func) : func_(func) {};
        ~Finally() {func_();}
    private:
        std::function<void(void)> func_;
};

template <typename T>
bool vector_add(std::vector<T> &list, const T &value)
{
    list.push_back(value);
    return true;
}

template <typename T>
T *vector_ref_add(std::vector<std::reference_wrapper<T> > &list)
{
    T *obj = new T();
    try
    {
        list.push_back((*obj));
    }
    catch(...)
    {
        delete obj;
        throw;
    }
    return &(list.at(list.size() - 1).get());
}

template <typename T>
void vector_ref_clear(std::vector<std::reference_wrapper<T> > &list)
{
    for (size_t i = 0; i < list.size(); i++)
    {
        T *obj = &(list.at(i).get());
        delete obj;
    }
    list.clear();
}

template <typename T>
const T &object_empty(const T& unused_)
{
    const T *unused = &unused_;
    (void)unused;
    static T default_;
    return default_;
}

template <typename T>
const T *object_passthrough(const T& obj)
{
    return &obj;
}

template <typename T>
const T *object_passthrough_ptr(const T* obj)
{
    return obj;
}

typedef bool(*random_bytes_fn_t)(void *ctx, char *data, size_t size);
void random_api(random_bytes_fn_t func, void *ctx);  // NOT THREAD SAFE

int8_t random_s1(int8_t min_inclusive, int8_t max_inclusive);
int16_t random_s2(int16_t min_inclusive, int16_t max_inclusive);
int32_t random_s4(int32_t min_inclusive, int32_t max_inclusive);
int64_t random_s8(int64_t min_inclusive, int64_t max_inclusive);
uint8_t random_u1(uint8_t min_inclusive, uint8_t max_inclusive);
uint16_t random_u2(uint16_t min_inclusive, uint16_t max_inclusive);
uint32_t random_u4(uint32_t min_inclusive, uint32_t max_inclusive);
uint64_t random_u8(uint64_t min_inclusive, uint64_t max_inclusive);
float random_f4(float min_inclusive, float max_inclusive);
double random_f8(double min_inclusive, double max_inclusive);
bool random_bool(bool min_inclusive, bool max_inclusive);
size_t random_size(size_t min_inclusive, size_t max_inclusive);
bool random_bytes(std::string &bytes, size_t min_size, size_t max_size, uint8_t min_byte, uint8_t max_byte);
bool random_char_str(std::string &str, size_t min_size, size_t max_size, uint32_t min_char, uint32_t max_char, IO::Encoding enc);
bool random_wchar_str(std::wstring &str, size_t min_size, size_t max_size, uint32_t min_char, uint32_t max_char, IO::Encoding enc);

}  // kpb

#define KPB_CPP_STL_LIST_CLEAR(typename, var)           ((var).clear())
#define KPB_CPP_STL_LIST_DELETE_CLEAR(typename, var)    (kpb::vector_ref_clear(var))
#define KPB_CPP_STL_LIST_GET(typename, var)             (((var).size() > 0) ? (*kpb::object_passthrough(var)) : kpb::object_empty(std::vector<typename>()))
#define KPB_CPP_STL_LIST_REF_GET(typename, var)         (((var).size() > 0) ? (*kpb::object_passthrough(var)) : kpb::object_empty(std::vector<std::reference_wrapper<typename> >()))
#define KPB_CPP_STL_LIST_SIZE(typename, var)            ((var).size())
#define KPB_CPP_STL_LIST_ADD(typename, var, v)          kpb::vector_add((var), v)
#define KPB_CPP_STL_LIST_ADD_NEW(typename, var)         kpb::vector_ref_add((var))

#define KPB_CPP_STL_OBJECT_CLEAR(typename, var)   \
    if ((var)) { \
        delete (var); \
        (var) = nullptr; \
    }
#define KPB_CPP_STL_OBJECT_LAZY_CONSTRUCTOR(typename, var)   ( !(var) ? (var) = new typename() : (var) )
#define KPB_CPP_STL_OBJECT_GET(typename, var)     ((var) ? (*kpb::object_passthrough_ptr((typename*)(var))) : kpb::object_empty(typename()))

#endif  // _H_KPB_IO_CPP_STL_H_
