#include "kpb_cpp_stl_io.hpp"

#include <string.h>

namespace kpb {

enum Endian {
    kBE = 0,
    kLE = 1,
};

static Endian byte_order()
{
    if (*((uint16_t*)"\x01\x23") != 0x0123)
    {
        return Endian::kLE;
    }
    return Endian::kBE;
}

static int8_t byte_swap_s1(int8_t value)
{
    return value;
}

static int16_t byte_swap_s2(int16_t value)
{
    int16_t swapped = 0;
    swapped |= (value >> 8) & 0x00ff;
    swapped |= (value << 8) & 0xff00;
    return swapped;
}

static int32_t byte_swap_s4(int32_t value)
{
    int32_t swapped = 0;
    swapped |= (value >> 24) & 0x000000ff;
    swapped |= (value >> 8)  & 0x0000ff00;
    swapped |= (value << 8)  & 0x00ff0000;
    swapped |= (value << 24) & 0xff000000;
    return swapped;
}

static int64_t byte_swap_s8(int64_t value)
{
    int64_t swapped = 0;
    swapped |= (value >> 56) & 0x00000000000000ff;
    swapped |= (value >> 40) & 0x000000000000ff00;
    swapped |= (value >> 24) & 0x0000000000ff0000;
    swapped |= (value >> 8)  & 0x00000000ff000000;
    swapped |= (value << 8)  & 0x000000ff00000000;
    swapped |= (value << 24) & 0x0000ff0000000000;
    swapped |= (value << 40) & 0x00ff000000000000;
    swapped |= (value << 56) & 0xff00000000000000;
    return swapped;
}

static uint8_t byte_swap_u1(uint8_t value)
{
    return value;
}

static uint16_t byte_swap_u2(uint16_t value)
{
    uint16_t swapped = 0;
    swapped |= (value >> 8) & 0x00ff;
    swapped |= (value << 8) & 0xff00;
    return swapped;
}

static uint32_t byte_swap_u4(uint32_t value)
{
    uint32_t swapped = 0;
    swapped |= (value >> 24) & 0x000000ff;
    swapped |= (value >> 8)  & 0x0000ff00;
    swapped |= (value << 8)  & 0x00ff0000;
    swapped |= (value << 24) & 0xff000000;
    return swapped;
}

static uint64_t byte_swap_u8(uint64_t value)
{
    uint64_t swapped = 0;
    swapped |= (value >> 56) & 0x00000000000000ff;
    swapped |= (value >> 40) & 0x000000000000ff00;
    swapped |= (value >> 24) & 0x0000000000ff0000;
    swapped |= (value >> 8)  & 0x00000000ff000000;
    swapped |= (value << 8)  & 0x000000ff00000000;
    swapped |= (value << 24) & 0x0000ff0000000000;
    swapped |= (value << 40) & 0x00ff000000000000;
    swapped |= (value << 56) & 0xff00000000000000;
    return swapped;
}

static float byte_swap_f4(float value)
{
    uint32_t out_value = byte_swap_u4(*reinterpret_cast<uint32_t*>(&value));
    return *reinterpret_cast<float*>(&out_value);
}

static double byte_swap_f8(double value)
{
    uint64_t out_value = byte_swap_u8(*reinterpret_cast<uint64_t*>(&value));
    return *reinterpret_cast<double*>(&out_value);
}

IO::utf16_t IO::utf16_wchar(wchar_t chr, Encoding enc)
{
    // TODO: this is not entirely correct on linux systems (where wchar_t is UTF-32)
    if ( (byte_order() == kpb::Endian::kLE) && (enc == kpb::IO::Encoding::ENCODING_UTF16LE) )
    {
        return (IO::utf16_t)chr;
    }
    if ( (byte_order() == kpb::Endian::kBE) && (enc == kpb::IO::Encoding::ENCODING_UTF16BE) )
    {
        return (IO::utf16_t)chr;
    }
    return (IO::utf16_t)byte_swap_s2((int16_t)chr);
}

wchar_t IO::wchar_utf16(IO::utf16_t chr, Encoding enc)
{
    // TODO: this is not entirely correct on linux systems (where wchar_t is UTF-32)
    if ( (byte_order() == kpb::Endian::kLE) && (enc == kpb::IO::Encoding::ENCODING_UTF16LE) )
    {
        return ((wchar_t)chr) & 0xffff;
    }
    if ( (byte_order() == kpb::Endian::kBE) && (enc == kpb::IO::Encoding::ENCODING_UTF16BE) )
    {
        return ((wchar_t)chr) & 0xffff;
    }
    return ((wchar_t)byte_swap_s2((int16_t)chr)) & 0xffff;
}

std::basic_string<IO::utf16_t> IO::utf16_dupwchar(const std::wstring &str, Encoding enc)
{
    // TODO: this is not entirely correct on linux systems (where wchar_t is UTF-32)
    std::basic_string<IO::utf16_t> utf16_str;
    for ( size_t i = 0; i < str.size(); i++ )
    {
        utf16_str.push_back(IO::utf16_wchar(str[i], enc));
    }
    return utf16_str;
}

size_t IO::utf16_len(const IO::utf16_t *str)
{
    size_t len = 0;
    while ( *str != 0 )
    {
        str++;
        len++;
    }
    return len;
}

const IO::utf16_t *IO::utf16_chr(const IO::utf16_t *str, IO::utf16_t chr)
{
    while ( *str != 0 )
    {
        if ( *str == chr )
        {
            return str;
        }
        str++;
    }
    return NULL;
}

IO::IO() : buffer_()
{
    // owner
    stream_owner_ = this;
    write_mode_ = true;
    ok_ = true;

    // root
    stream_root_ = nullptr;

    // current stream
    stream_current_ = this;
    stream_next_ = nullptr;
    stream_prev_ = nullptr;
    consume_on_pop_ = false;

    // current stream data
    stream_data_ = this;
    //buffer_;

    // current stream view
    stream_view_ = this;
    view_size_fixed_ = false;
    view_offset_ = 0;
    view_size_ = 0;
    pos_ = 0;
    bits_left_ = 0;
    bits_ = 0;

    // fill with 0s or 1s
    fill_ = false;
    fill_byte_ = 0x00;
    fill_eof_ = false;
}

IO::~IO()
{
    delete stream_next_;
}

void IO::set_error()
{
    stream_owner_->ok_ = false;
}

bool IO::ok() const
{
    return stream_owner_->ok_;
}

bool IO::writable() const
{
    return stream_owner_->write_mode_;
}

bool IO::readonly() const
{
    return !(stream_owner_->write_mode_);
}

void IO::set_readonly(const std::string &data)
{
    if ( !ok() ) return;

    // cannot call push from any other stream accept the owner
    if ( stream_owner_ != this )
    {
        set_error();
        return;
    }

    // change data and view settings to the current stream (rather than referencing other streams)
    stream_data_ = stream_current_;
    stream_view_ = stream_current_;

    // disable writes to steam (and sub-streams)
    stream_owner_->write_mode_ = false;

    // assign & position the fixed data to the stream
    stream_data_->buffer_ = data;
    stream_view_->view_offset_ = 0;
    stream_view_->view_size_fixed_ = true;
    stream_view_->view_size_ = data.size();
    stream_view_->pos_ = 0;
    stream_view_->bits_left_ = 0;
    stream_view_->bits_ = 0;
}

void IO::set_readonly_or_fill(const std::string &data, uint8_t fill)
{
    set_readonly(data);
    fill_ = true;
    fill_byte_ = fill != 0 ? 0xff : 0x00;
    fill_eof_ = false;
}

void IO::set(const std::string &data)
{
    if ( !ok() ) return;

    // cannot call set from any other stream accept the owner
    if ( stream_owner_ != this )
    {
        set_error();
        return;
    }

    // keep the fixed/unfixed stream setting
    bool view_size_fixed = stream_view_->view_size_fixed_;

    // change data and view settings to the current stream (rather than referencing other streams)
    stream_current_->stream_data_ = stream_current_; // input data is stored in current stream object
    stream_current_->stream_view_ = stream_current_; // changes to view are made to current stream object
    stream_data_ = stream_current_->stream_data_;
    stream_view_ = stream_current_->stream_view_;

    // write no-longer changes the parent stream, so we need to read/write the buffer on pop
    IO *parent = stream_current_->stream_prev_;
    if ( parent && writable() )
    {
        stream_current_->consume_on_pop_ = true;
    }

    // assign & position the fixed data to the stream
    stream_data_->buffer_ = data;
    stream_view_->view_offset_ = 0;
    stream_view_->view_size_fixed_ = view_size_fixed;
    stream_view_->view_size_ = view_size_fixed ? data.size() : 0;
    stream_view_->pos_ = 0;
    stream_view_->bits_left_ = 0;
    stream_view_->bits_ = 0;

    // and position the buffer accordingly
    if ( writable() )
    {
        // buffer has been written, and position is at the end
        stream_view_->pos_ = data.size();
    }
    else
    {
        // reading of this new buffer is at the start
        stream_view_->pos_ = 0;
    }
}

std::string IO::get()
{
    if ( !ok() ) return std::string("", 0);
    align_to_byte();  // make sure remaining bitfields are added to the buffer
    if ( !ok() ) return std::string("", 0);
    return std::string(stream_data_->buffer_.c_str() + stream_view_->view_offset_, size());
}

size_t IO::get_size()
{
    align_to_byte();  // make sure remaining bitfields are added to the buffer
    return size();
}

IO &IO::root()
{
    if ( stream_owner_->stream_root_ )
    {
        return *(stream_owner_->stream_root_);
    }

    // create a new stream to represent the root (as the owner's data/view context can change)
    IO *root = new IO();
    root->stream_owner_ = stream_owner_;
    if ( stream_owner_->stream_next_ )
    {
        root->stream_prev_ = stream_owner_->stream_next_->stream_prev_;
    }
    root->stream_next_ = stream_owner_->stream_next_;
    if ( root->stream_next_ )
    {
        root->stream_next_->stream_prev_ = root;
    }
    stream_owner_->stream_next_ = root;

    // root stream is just an alias to the original stream
    root->stream_data_ = stream_owner_;
    root->stream_view_ = stream_owner_;

    // use this new stream as the root in future
    stream_owner_->stream_root_ = root;

    // move all sub-streams to use root
    IO *stream = stream_owner_;
    while ( stream )
    {
        // don't adjust the root stream (just all the other streams)
        if ( stream != root )
        {
            if ( stream->stream_current_ == stream_owner_ )
            {
                stream->stream_current_ = root;
            }
        }
        stream = stream->stream_next_;
    }
    return *(stream_owner_->stream_root_);
}

IO &IO::parent()
{
    IO *parent = nullptr;
    if ( this == stream_owner_ )
    {
        parent = stream_owner_->stream_current_->stream_prev_;
    }
    else
    {
        parent = stream_prev_;
    }
    if ( !parent )
    {
        parent = &root();
    }
    return *parent;
}

void IO::push_substream(IO &substream, bool set_pos, size_t pos, bool set_size, size_t size)
{
    if ( !ok() ) return;

    // cannot call push from any other stream accept the owner
    if ( stream_owner_ != this )
    {
        set_error();
        return;
    }

    // current active stream will become the parent
    IO *parent = stream_current_;
    if ( parent == stream_owner_ )
    {
        // if parent points to the owner, treat it as the root
        parent = &root();
    }

    // if substream is the owner, we are being asked to use the current (active) substream
    IO *target = &substream;
    if ( target == stream_owner_ )
    {
        if ( stream_current_ == stream_owner_ )
        {
            target = &root();
        }
        else
        {
            target = stream_current_;
        }
    }

    // flush bits & make sure parent stream is byte-aligned
    align_to_byte();
    if ( !ok() ) return;

    // creating & link stream into the list of streams
    IO *new_stream = new IO();
    new_stream->stream_owner_ = stream_owner_;
    new_stream->stream_next_ = nullptr;
    new_stream->stream_prev_ = parent;
    parent->stream_next_ = new_stream;

    // overflow check
    if ( ( pos + size ) < size )
    {
        set_error();
        return;
    }

    // additional checks when working with a fixed read-only buffer (i.e. parsing)
    if ( readonly() )
    {
        size_t start = set_pos ? pos : target->pos();
        size_t end = start + (set_size ? size : 0);

        // in read-only, cannot set a position & size outside the stream
        if ( end > target->size() )
        {
            if ( !fill_ )
            {
                set_error();
                return;
            }
            else
            {
                end = target->size();
            }
        }
    }

    // this is the offset of the view we want (it must begin at the start of the target stream)
    size_t view_offset = target->stream_view_->view_offset_;
    if ( set_pos )
    {
        // place the view at this offset into the target's stream
        view_offset += pos;
    }
    else
    {
        // we are reading/writing after the current position of the target stream
        // place the view at the current position of the target stream
        view_offset += target->stream_view_->pos_;
    }

    // this is the size of the view we want
    bool view_size_fixed = false;  // keep writing until the end of the stream
    size_t view_size = 0;
    if ( set_size )
    {
        view_size_fixed = true;
        view_size = size;
    }

    // further restrict the view if the target's stream is also restricted
    if ( target->stream_view_->view_size_fixed_ )
    {
        // maximum allowed offset of a new view inside the target
        size_t view_offset_max = target->stream_view_->view_offset_ + target->stream_view_->view_size_;
        if ( view_offset > view_offset_max )
        {
            view_offset = view_offset_max;
        }

        // maximum allowed size of a new view inside the target stream
        size_t view_size_max = view_offset_max - view_offset;
        if ( !view_size_fixed )
        {
            // no restriction was asked for, make all target data available
            view_size_fixed = true;
            view_size = view_size_max;
        }
        if ( view_size > view_size_max )
        {
            view_size = view_size_max;
        }
    }

    // determine the required size of the backing buffer (and fill with zeros if needed)
    size_t required_buffer_size = view_offset + view_size;
    if ( target->stream_data_->buffer_.size() < required_buffer_size )
    {
        if ( writable() )
        {
            // resize if writing
            target->stream_data_->buffer_.resize(required_buffer_size, '\x00');
        }
        else
        {
            // error if reading (cannot invent bytes out of nowhere)
            set_error();
            return;
        }
    }

    // data will be read/written directly to/from target stream
    new_stream->stream_data_ = target->stream_data_;

    // view of the new stream's data which will be restricted
    new_stream->stream_view_ = new_stream;
    new_stream->stream_view_->view_offset_ = view_offset;
    new_stream->stream_view_->view_size_fixed_ = view_size_fixed;
    new_stream->stream_view_->view_size_ = view_size;

    // only consume data if we are reading/writing sequentially (not at a new position)
    new_stream->consume_on_pop_ = !set_pos;

    // make this stream active
    stream_current_ = new_stream;
    stream_data_ = stream_current_->stream_data_;
    stream_view_ = stream_current_->stream_view_;
}

void IO::pop_substream()
{
    if ( !ok() ) return;

    // cannot call pop from any other stream accept the owner
    if ( stream_owner_ != this )
    {
        set_error();
        return;
    }

    // must have a parent to pop
    if ( !stream_current_->stream_prev_ )
    {
        set_error();
        return;
    }

    // flush bits & and byte align before writing parent stream or re-positioning it
    align_to_byte();
    if ( !ok() ) return;

    // parent stream
    IO *parent = stream_current_->stream_prev_;

    // consume data??
    if ( stream_current_->consume_on_pop_ )
    {
        if ( writable() && ( parent->stream_data_ != stream_data_ ) )
        {
            // write changes into parent
            if ( stream_view_->view_size_fixed_ )
            {
                // size: <value>
                parent->write_bytes(stream_data_->buffer_, stream_view_->view_size_);
            }
            else
            {
                // size-eos: true
                parent->write_bytes(stream_data_->buffer_, stream_data_->buffer_.size());
            }
        }
        else
        {
            // move position forward in the parent stream
            if ( stream_view_->view_size_fixed_ )
            {
                // size: <value>
                parent->stream_view_->pos_ += stream_view_->view_size_;
            }
            else
            {
                // size-eos: true
                parent->stream_view_->pos_ += stream_view_->pos_;
            }
        }

        // check reads/writes ok
        if ( !ok() ) return;
    }

    // move back to previously active stream
    IO *unused_stream = stream_current_;
    stream_current_ = stream_current_->stream_prev_;
    stream_data_ = stream_current_->stream_data_;
    stream_view_ = stream_current_->stream_view_;
    stream_current_->stream_next_ = nullptr;

    // delete the unused stream
    delete unused_stream;
}

size_t IO::size() const
{
    // if the offset into the buffer for the view is beyound the buffer, the view has no space
    if ( stream_view_->view_offset_ > stream_data_->buffer_.size() )
    {
        return 0;
    }

    // size can be determined by the size of the buffer (starting from where the view starts)
    size_t size = stream_data_->buffer_.size() - stream_view_->view_offset_;

    // if the size of the view of the buffer is fixed - we know the size
    if ( stream_view_->view_size_fixed_ )
    {
        // for safety, only use the view size if the stream has enough data in it
        if ( size > stream_view_->view_size_ )
        {
            size = stream_view_->view_size_;
        }
    }

    // done
    return size;
}

size_t IO::pos() const
{
    // if position is outside, cap at very end of stream
    if ( stream_view_->pos_ > size() )
    {
        return size();
    }
    return stream_view_->pos_;
}

size_t IO::bit_size() const
{
    if ( writable() )
    {
        return (size() * 8) + stream_view_->bits_left_;
    }
    else
    {
        // FUTURE: update this when we support bit streams of bit-size
        return size() * 8;
    }
}

size_t IO::bit_pos() const
{
    size_t bits_left = stream_view_->bits_left_;
    if ( writable() )
    {
        return (pos() * 8) + bits_left;
    }
    if (bits_left == 0)
    {
        return (pos() * 8);
    }
    if ( bits_left > 8 )
    {
        // error state, just return byte position as bit position
        return pos() * 8;
    }
    // in read-only, bits_left is how many bits we can use (not where we are)
    bits_left = 8 - bits_left;
    // -1 because we moved forward 8-bits when we filled the bit register
    return ((pos() - 1) * 8) + bits_left;
}

bool IO::eof() const
{
    return stream_view_->fill_eof_ || ((pos() >= size()) && (stream_view_->bits_left_ == 0));
}

size_t IO::remaining() const
{
    if ( size() > pos() )
    {
        return size() - pos();
    }
    return 0;
}

void IO::align_to_byte()
{
    if ( !ok() ) return;
    if ( 0 == stream_view_->bits_left_ )
    {
        stream_view_->bits_left_ = 0;
        stream_view_->bits_ = 0;
        return;
    }
    if ( writable() )
    {
        if ( stream_view_->bits_left_ >= 8 )
        {
            set_error();
            return;
        }
        uint8_t left = (uint8_t)stream_view_->bits_;
        left <<= (8 - stream_view_->bits_left_);
        write(&left, 1);
    }
    stream_view_->bits_left_ = 0;
    stream_view_->bits_ = 0;
}

int8_t IO::read_s1()
{
    if ( !ok() ) return 0;
    align_to_byte();
    int8_t value = 0;
    if ( !read(&value, sizeof(value)) ) return 0;
    return value;
}

int16_t IO::read_s2be()
{
    if ( !ok() ) return 0;
    align_to_byte();
    int16_t value = 0;
    if ( !read(&value, sizeof(value)) ) return 0;
    if ( byte_order() != Endian::kBE ) return byte_swap_s2(value);
    return value;
}

int32_t IO::read_s4be()
{
    if ( !ok() ) return 0;
    align_to_byte();
    int32_t value = 0;
    if ( !read(&value, sizeof(value)) ) return 0;
    if ( byte_order() != Endian::kBE ) return byte_swap_s4(value);
    return value;
}

int64_t IO::read_s8be()
{
    if ( !ok() ) return 0;
    align_to_byte();
    int64_t value = 0;
    if ( !read(&value, sizeof(value)) ) return 0;
    if ( byte_order() != Endian::kBE ) return byte_swap_s8(value);
    return value;
}

int16_t IO::read_s2le()
{
    if ( !ok() ) return 0;
    align_to_byte();
    int16_t value = 0;
    if ( !read(&value, sizeof(value)) ) return 0;
    if ( byte_order() != Endian::kLE ) return byte_swap_s2(value);
    return value;
}

int32_t IO::read_s4le()
{
    if ( !ok() ) return 0;
    align_to_byte();
    int32_t value = 0;
    if ( !read(&value, sizeof(value)) ) return 0;
    if ( byte_order() != Endian::kLE ) return byte_swap_s4(value);
    return value;
}

int64_t IO::read_s8le()
{
    if ( !ok() ) return 0;
    align_to_byte();
    int64_t value = 0;
    if ( !read(&value, sizeof(value)) ) return 0;
    if ( byte_order() != Endian::kLE ) return byte_swap_s8(value);
    return value;
}

uint8_t IO::read_u1()
{
    if ( !ok() ) return 0;
    align_to_byte();
    uint8_t value = 0;
    if ( !read(&value, sizeof(value)) ) return 0;
    return value;
}

uint16_t IO::read_u2be()
{
    if ( !ok() ) return 0;
    align_to_byte();
    uint16_t value = 0;
    if ( !read(&value, sizeof(value)) ) return 0;
    if ( byte_order() != Endian::kBE ) return byte_swap_u2(value);
    return value;
}

uint32_t IO::read_u4be()
{
    if ( !ok() ) return 0;
    align_to_byte();
    uint32_t value = 0;
    if ( !read(&value, sizeof(value)) ) return 0;
    if ( byte_order() != Endian::kBE ) return byte_swap_u4(value);
    return value;
}

uint64_t IO::read_u8be()
{
    if ( !ok() ) return 0;
    align_to_byte();
    uint64_t value = 0;
    if ( !read(&value, sizeof(value)) ) return 0;
    if ( byte_order() != Endian::kBE ) return byte_swap_u8(value);
    return value;
}

uint16_t IO::read_u2le()
{
    if ( !ok() ) return 0;
    align_to_byte();
    uint16_t value = 0;
    if ( !read(&value, sizeof(value)) ) return 0;
    if ( byte_order() != Endian::kLE ) return byte_swap_u2(value);
    return value;
}

uint32_t IO::read_u4le()
{
    if ( !ok() ) return 0;
    align_to_byte();
    uint32_t value = 0;
    if ( !read(&value, sizeof(value)) ) return 0;
    if ( byte_order() != Endian::kLE ) return byte_swap_u4(value);
    return value;
}

uint64_t IO::read_u8le()
{
    if ( !ok() ) return 0;
    align_to_byte();
    uint64_t value = 0;
    if ( !read(&value, sizeof(value)) ) return 0;
    if ( byte_order() != Endian::kLE ) return byte_swap_u8(value);
    return value;
}

float IO::read_f4le() {
    if ( !ok() ) return 0.0;
    align_to_byte();
    float value = 0.0;
    if ( !read(&value, sizeof(value)) ) return 0.0;
    if ( byte_order() != Endian::kLE ) return byte_swap_f4(value);
    return value;
}

double IO::read_f8le() {
    if ( !ok() ) return 0.0;
    align_to_byte();
    double value = 0.0;
    if ( !read(&value, sizeof(value)) ) return 0.0;
    if ( byte_order() != Endian::kLE ) return byte_swap_f8(value);
    return value;
}

float IO::read_f4be() {
    if ( !ok() ) return 0.0;
    align_to_byte();
    float value = 0.0;
    if ( !read(&value, sizeof(value)) ) return 0.0;
    if ( byte_order() != Endian::kBE ) return byte_swap_f4(value);
    return value;
}

double IO::read_f8be() {
    if ( !ok() ) return 0.0;
    align_to_byte();
    double value = 0.0;
    if ( !read(&value, sizeof(value)) ) return 0.0;
    if ( byte_order() != Endian::kBE ) return byte_swap_f8(value);
    return value;
}

uint64_t IO::read_bits_int_be(int n)
{
    if ( !ok() ) return 0;
    uint64_t value = 0;
    while ( n > 0 )
    {
        if ( stream_view_->bits_left_ == 0 )
        {
            uint8_t value_byte = 0;
            if ( !read(&value_byte, sizeof(value_byte)) )
            {
                return 0;
            }
            stream_view_->bits_ = ((uint64_t)value_byte) & 0xff;
            stream_view_->bits_left_ = 8;
        }
        n--;
        stream_view_->bits_left_--;
        value <<= 1;
        value &=~ 1;
        value |= (stream_view_->bits_ >> stream_view_->bits_left_) & 0x1;
    }
    return value;
}

uint64_t IO::read_bits_int(int n)
{
    return read_bits_int_be(n);
}

uint64_t IO::read_bits_int_le(int n)
{
    (void)n;
    // TODO: implemented bits le
    set_error();
    return 0;
}

std::string IO::read_char_str(size_t size, char term, bool use_size, bool include_term, bool consume, IO::Encoding src_enc)
{
    (void)include_term;

    // stop if stream is already broken
    if ( !ok() ) return "";

    // not implemented yet
    if ( !consume )
    {
        set_error();
        return "";
    }

    // only ASCII and UTF8 is supported here.
    if ( (kpb::IO::Encoding::ENCODING_ASCII != src_enc) && (kpb::IO::Encoding::ENCODING_UTF8 != src_enc) )
    {
        set_error();
        return "";
    }

    // strings must be byte aligned
    align_to_byte();
    if ( !ok() ) return "";

    // calculate size if not provided (scaning forward to find the terminator)
    if ( !use_size )
    {
        size_t orig_pos = stream_view_->pos_;
        size = 0;
        while ( true )
        {
            char current = 0;
            if ( remaining() < sizeof(term) )
            {
                set_error();
                return "";
            }
            if ( !read(&current, sizeof(current)) ) return "";
            size += sizeof(current);
            if ( current == term )
            {
                break;
            }
        }
        stream_view_->pos_ = orig_pos;
    }

    // easy
    if ( size == 0 )
    {
        return "";
    }

    // TODO: handle include_term

    // read string
    std::string data;
    data.resize(size, 0x00);
    if ( !read((char*)data.c_str(), size) ) return "";

    // shorten to first nul
    data.resize(strlen(data.c_str()));
    return data;
}

std::wstring IO::read_wchar_str(size_t size, wchar_t term, bool use_size, bool include_term, bool consume, IO::Encoding src_enc)
{
    (void)include_term;

    // stop if stream is already broken
    if ( !ok() ) return L"";

    // not implemented yet
    if ( !consume )
    {
        set_error();
        return L"";
    }

    // only UTF16 is supported here.
    if ( (kpb::IO::Encoding::ENCODING_UTF16LE != src_enc) && (kpb::IO::Encoding::ENCODING_UTF16BE != src_enc) )
    {
        set_error();
        return L"";
    }

    // strings must be byte aligned
    align_to_byte();
    if ( !ok() ) return L"";

    // calculate size if not provided (scaning forward to find the terminator)
    IO::utf16_t utf16_term = IO::utf16_wchar(term, src_enc);
    if ( !use_size )
    {
        size_t orig_pos = stream_view_->pos_;
        size = 0;
        while ( true )
        {
            IO::utf16_t utf16_current = 0;
            if ( remaining() < sizeof(utf16_term) )
            {
                set_error();
                return L"";
            }
            if ( !read(&utf16_current, sizeof(utf16_current)) ) return L"";
            size += sizeof(utf16_current);
            if ( utf16_current == utf16_term )
            {
                break;
            }
        }
        stream_view_->pos_ = orig_pos;
    }

    // easy
    if ( size == 0 )
    {
        return L"";
    }

    // TODO: handle include_term

    // read string
    std::basic_string<IO::utf16_t> utf16_data;
    if ( size > (size + 1) )
    {
        set_error();
        return L"";
    }
    utf16_data.resize((size + 1) / sizeof(IO::utf16_t), 0x0000);
    if ( !read((char*)utf16_data.c_str(), size) ) return L"";

    // shorten to first nul
    utf16_data.resize(IO::utf16_len(utf16_data.c_str()));

    // utf16 string to wide character string
    std::wstring data;
    for ( size_t i = 0; i < utf16_data.size(); i++ )
    {
        data.push_back(IO::wchar_utf16(utf16_data[i], src_enc));
    }

    // done
    return data;
}

std::string IO::read_bytes(size_t size)
{
    if ( !ok() ) return std::string("", 0);
    align_to_byte();
    if ( !ok() ) return std::string("", 0);
    std::string data;
    data.resize(size, '\x00');
    if ( !read((void*)data.c_str(), size) ) return std::string("", 0);
    return data;
}

void IO::write_s1(int8_t value)
{
    if ( !ok() ) return;
    align_to_byte();
    if ( !ok() ) return;
    write(&value, sizeof(value));
}

void IO::write_s2be(int16_t value)
{
    if ( !ok() ) return;
    align_to_byte();
    if ( !ok() ) return;
    if ( byte_order() != Endian::kBE ) value = byte_swap_s2(value);
    write(&value, sizeof(value));
}

void IO::write_s4be(int32_t value)
{
    if ( !ok() ) return;
    align_to_byte();
    if ( !ok() ) return;
    if ( byte_order() != Endian::kBE ) value = byte_swap_s4(value);
    write(&value, sizeof(value));
}

void IO::write_s8be(int64_t value)
{
    if ( !ok() ) return;
    align_to_byte();
    if ( !ok() ) return;
    if ( byte_order() != Endian::kBE ) value = byte_swap_s8(value);
    write(&value, sizeof(value));
}

void IO::write_s2le(int16_t value)
{
    if ( !ok() ) return;
    align_to_byte();
    if ( !ok() ) return;
    if ( byte_order() != Endian::kLE ) value = byte_swap_s2(value);
    write(&value, sizeof(value));
}

void IO::write_s4le(int32_t value)
{
    if ( !ok() ) return;
    align_to_byte();
    if ( !ok() ) return;
    if ( byte_order() != Endian::kLE ) value = byte_swap_s4(value);
    write(&value, sizeof(value));
}

void IO::write_s8le(int64_t value)
{
    if ( !ok() ) return;
    align_to_byte();
    if ( !ok() ) return;
    if ( byte_order() != Endian::kLE ) value = byte_swap_s8(value);
    write(&value, sizeof(value));
}

void IO::write_u1(uint8_t value)
{
    if ( !ok() ) return;
    align_to_byte();
    if ( !ok() ) return;
    write(&value, sizeof(value));
}

void IO::write_u2be(uint16_t value)
{
    if ( !ok() ) return;
    align_to_byte();
    if ( !ok() ) return;
    if ( byte_order() != Endian::kBE ) value = byte_swap_u2(value);
    write(&value, sizeof(value));
}

void IO::write_u4be(uint32_t value)
{
    if ( !ok() ) return;
    align_to_byte();
    if ( !ok() ) return;
    if ( byte_order() != Endian::kBE ) value = byte_swap_u4(value);
    write(&value, sizeof(value));
}

void IO::write_u8be(uint64_t value)
{
    if ( !ok() ) return;
    align_to_byte();
    if ( !ok() ) return;
    if ( byte_order() != Endian::kBE ) value = byte_swap_u8(value);
    write(&value, sizeof(value));
}

void IO::write_u2le(uint16_t value)
{
    if ( !ok() ) return;
    align_to_byte();
    if ( !ok() ) return;
    if ( byte_order() != Endian::kLE ) value = byte_swap_u2(value);
    write(&value, sizeof(value));
}

void IO::write_u4le(uint32_t value)
{
    if ( !ok() ) return;
    align_to_byte();
    if ( !ok() ) return;
    if ( byte_order() != Endian::kLE ) value = byte_swap_u4(value);
    write(&value, sizeof(value));
}

void IO::write_u8le(uint64_t value)
{
    if ( !ok() ) return;
    align_to_byte();
    if ( !ok() ) return;
    if ( byte_order() != Endian::kLE ) value = byte_swap_u8(value);
    write(&value, sizeof(value));
}

void IO::write_f4le(float value) {
    if ( !ok() ) return;
    align_to_byte();
    if ( !ok() ) return;
    if ( byte_order() != Endian::kLE ) value = byte_swap_f4(value);
    write(&value, sizeof(value));
}

void IO::write_f8le(double value) {
    if ( !ok() ) return;
    align_to_byte();
    if ( !ok() ) return;
    if ( byte_order() != Endian::kLE ) value = byte_swap_f8(value);
    write(&value, sizeof(value));
}

void IO::write_f4be(float value) {
    if ( !ok() ) return;
    align_to_byte();
    if ( !ok() ) return;
    if ( byte_order() != Endian::kBE ) value = byte_swap_f4(value);
    write(&value, sizeof(value));
}

void IO::write_f8be(double value) {
    if ( !ok() ) return;
    align_to_byte();
    if ( !ok() ) return;
    if ( byte_order() != Endian::kBE ) value = byte_swap_f8(value);
    write(&value, sizeof(value));
}

void IO::write_bits_int_be(int n, uint64_t _bits)
{
    if ( !ok() ) return;
    while ( n > 0 )
    {
        while ( stream_view_->bits_left_ >= 8 )
        {
            uint8_t value = (uint8_t)((stream_view_->bits_ >> (stream_view_->bits_left_- 8)) & 0xff);
            if ( !write(&value, sizeof(value)) )
            {
                return;
            }
            // no need to zero top bits
            stream_view_->bits_left_ -= 8;
        }
        n--;
        stream_view_->bits_left_++;  // this might take us back up to 8-bits
        stream_view_->bits_ <<= 1;
        stream_view_->bits_ &=~ 1;
        stream_view_->bits_ |= (_bits >> n) & 1;
    }
    if ( stream_view_->bits_left_ >= 8 ) // if back up to 8-bits - flush
    {
        uint8_t value = (uint8_t)((stream_view_->bits_ >> (stream_view_->bits_left_- 8)) & 0xff);
        if ( !write(&value, sizeof(value)) )
        {
            return;
        }
        // no need to zero top bits
        stream_view_->bits_left_ -= 8;
    }
}

void IO::write_bits_int(int n, uint64_t _bits)
{
    write_bits_int_be(n, _bits);
}

void IO::write_bits_int_le(int n, uint64_t _bits)
{
    (void)n;
    (void)_bits;
    // TODO: implemented bits le
    set_error();
}

void IO::write_char_str(const std::string &str, size_t size, char term, bool use_size, bool include_term, IO::Encoding src_enc)
{
    // stop if stream is already broken
    if ( !ok() ) return;

    // only ASCII and UTF8 is supported here.
    if ( (kpb::IO::Encoding::ENCODING_ASCII != src_enc) && (kpb::IO::Encoding::ENCODING_UTF8 != src_enc) )
    {
        set_error();
        return;
    }

    // strings must be byte aligned
    align_to_byte();
    if ( !ok() ) return;

    // stop right now if not writing anything
    if ( use_size && (0 == size) )
    {
        return;
    }

    // calculate length
    size_t length = 0;
    const char *term_pos = strchr(str.c_str(), term);
    if ( term_pos )
    {
        // use the position of the terminator to determine string length
        length = (size_t)(term_pos - str.c_str());
    }
    else
    {
        // use the nul to determine the string length
        length = strlen(str.c_str());
    }

    // calculate size if not provided (scaning forward to find the terminator)
    if ( use_size )
    {
        if ( include_term )
        {
            if ( size < sizeof(char) )
            {
                // not enough space to include the terminator
                set_error();
                return;
            }

            // leave space to include the terminator
            size -= sizeof(char);
        }

        // clip the string so it fits.
        if ( length > size )
        {
            length = size;
        }
    }
    else
    {
        // we are using as much as we need
        size = length;
    }

    // write characters in string
    if ( !write(str.c_str(), length) ) return;

    // fill with terminators
    while ( length < size )
    {
        if ( !write(&term, sizeof(term)) ) return;
        length += sizeof(term);
    }

    // terminate
    if ( include_term )
    {
        if ( !write(&term, sizeof(term)) ) return;
    }

    // done
    return;
}

void IO::write_wchar_str(const std::wstring &str, size_t size, wchar_t term, bool use_size, bool include_term, IO::Encoding src_enc)
{
    // stop if stream is already broken
    if ( !ok() ) return;

    // only ASCII is supported here.
    if ( (kpb::IO::Encoding::ENCODING_UTF16LE != src_enc) && (kpb::IO::Encoding::ENCODING_UTF16BE != src_enc) )
    {
        set_error();
        return;
    }

    // strings must be byte aligned
    align_to_byte();
    if ( !ok() ) return;

    // stop right now if not writing anything
    if ( use_size && (0 == size) )
    {
        return;
    }

    // convert
    IO::utf16_t utf16_term = IO::utf16_wchar(term, src_enc);
    std::basic_string<IO::utf16_t> utf16_str = IO::utf16_dupwchar(str, src_enc);

    // calculate length
    size_t length = 0;
    const IO::utf16_t *term_pos = IO::utf16_chr(utf16_str.c_str(), utf16_term);
    if ( term_pos )
    {
        // use the position of the terminator to determine string length
        length = ((size_t)(term_pos - utf16_str.c_str()));
    }
    else
    {
        // use the nul to determine the string length
        length = IO::utf16_len(utf16_str.c_str());
    }

    // calculate size if not provided (scaning forward to find the terminator)
    if ( use_size )
    {
        if ( include_term )
        {
            if ( size < sizeof(IO::utf16_t) )
            {
                // not enough space to include the terminator
                set_error();
                return;
            }

            // leave space to include the terminator
            size -= sizeof(IO::utf16_t);
        }

        // clip the string so it fits.
        if ( (length * sizeof(IO::utf16_t)) > size )
        {
            length = size / sizeof(IO::utf16_t);
        }
    }
    else
    {
        // we are using as much as we need
        size = length * sizeof(IO::utf16_t);
    }

    // write characters in string
    if ( !write(utf16_str.c_str(), length * sizeof(IO::utf16_t)) ) return;

    // fill with terminators
    while ( length < (size / sizeof(IO::utf16_t)) )
    {
        if ( !write(&utf16_term, sizeof(utf16_term)) ) return;
        length += 1;
    }

    // terminate
    if ( include_term )
    {
        if ( !write(&utf16_term, sizeof(utf16_term)) ) return;
    }

    // add zeros for odd sizes
    if ( (size % sizeof(IO::utf16_t)) != 0 )
    {
        char pad = 0x00;
        if ( !write(&pad, sizeof(pad)) ) return;
    }

    // done
    return;
}

void IO::write_bytes(const std::string &data, size_t write_size)
{
    if ( !ok() ) return;
    align_to_byte();
    if ( !ok() ) return;
    if ( data.size() >= write_size )
    {
        write(data.c_str(), write_size);
        return;
    }
    if ( !write(data.c_str(), data.size()) ) return;
    write_size -= data.size();
    while (write_size > 0)
    {
        if ( !write("\x00", 1) ) return;
        write_size--;
    }
}

bool IO::read(void *data, size_t size)
{
    size_t padding_size = 0;

    if ( !ok() ) return false;

    // cannot read from a writable stream
    if ( writable() )
    {
        set_error();
        return false;
    }

    // how to handle not enough data for read
    padding_size = 0;
    if ( size > remaining() )
    {
        if ( !fill_ )
        {
            set_error();
            return false;
        }
        else
        {
            stream_view_->fill_eof_ = true;
            padding_size = size - remaining();
            size = remaining();
        }
    }

    // read & move stream
    memcpy(data, stream_data_->buffer_.c_str() + stream_view_->view_offset_ + pos(), size);
    memset(((char*)data) + size, fill_byte_, padding_size);
    stream_view_->pos_ += size; // TODO: need to move through padding if fill=true
    return true;
}

bool IO::write(const void *data, size_t size)
{
    if ( !ok() ) return false;

    // cannot write a read-only stream
    if ( readonly() )
    {
        set_error();
        return false;
    }

    // resize buffer OR cap the size of the write
    if ( stream_view_->view_size_fixed_ )
    {
        // cap the write for fixed-sized buffers
        size = (size < remaining() ) ? size : remaining();
    }
    else
    {
        // fill with zeros until the buffer reaches the desired write position
        while ( stream_view_->pos_ > this->size() )
        {
            stream_data_->buffer_ += std::string("\x00", 1);
        }
    }

    // stop here if there is nothing to write
    if ( size == 0 )
    {
        return true;
    }

    // place data within existing buffer
    size_t view_end_offset = stream_view_->view_offset_ + this->size();
    std::string view_prefix = std::string(stream_data_->buffer_.c_str(), stream_view_->view_offset_);
    std::string view_suffix = std::string("", 0);
    if ( view_end_offset < stream_data_->buffer_.size() )
    {
        view_suffix = std::string(stream_data_->buffer_.c_str() + view_end_offset, stream_data_->buffer_.size() - view_end_offset);
    }
    size_t end_offset = pos() + size;
    std::string prefix = std::string(stream_data_->buffer_.c_str() + stream_view_->view_offset_, pos());
    std::string suffix = std::string("", 0);
    if ( end_offset < this->size() )
    {
        suffix = std::string(stream_data_->buffer_.c_str() + stream_view_->view_offset_ + end_offset, this->size() - end_offset);
    }
    stream_data_->buffer_ = view_prefix + prefix + std::string((const char *)data, size) + suffix + view_suffix;
    stream_view_->pos_ += size;
    return true;
}

static random_bytes_fn_t g_random_bytes_fn = nullptr;
static void *g_random_bytes_ctx = nullptr;

void random_api(random_bytes_fn_t func, void *ctx)
{
    g_random_bytes_fn = func;
    g_random_bytes_ctx = ctx;
}

static bool random_fill(void *data, size_t size)
{
    memset(data, 0, size);
    if ( !g_random_bytes_fn )
    {
        return false;
    }
    return g_random_bytes_fn(g_random_bytes_ctx, (char*)data, size);
}

#define KPB_SWAP_MINMAX_OR_RETURN_IF_EQUAL(t,a,b) \
    if (a == b) \
    { \
        return a; \
    } \
    if (a > b) \
    { \
        t tmp = b; \
        b = a; \
        a = tmp; \
    }

#define KPB_RETURN_RANDOM_UNSIGNED_INT(t,kt,a,b) \
    KPB_SWAP_MINMAX_OR_RETURN_IF_EQUAL(t,a,b) \
    t value = 0; \
    (void)random_fill(&value, sizeof(value)); \
    if ( byte_order() != Endian::kBE ) value = byte_swap_##kt(value); \
    t maxv = 1; \
    maxv <<= ((sizeof(t)*8)-1); \
    maxv |= ~maxv; \
    if ((a == 0) && (b == maxv)) \
    { \
        return value; \
    } \
    value = (value % (b - a + 1)) + a; \
    return value;

#define KPB_RETURN_RANDOM_SIGNED_INT(t,kt,a,b) \
    KPB_SWAP_MINMAX_OR_RETURN_IF_EQUAL(t,a,b) \
    t value = 0; \
    (void)random_fill(&value, sizeof(value)); \
    if ( byte_order() != Endian::kBE ) value = byte_swap_##kt(value); \
    t minv = 1; \
    minv <<= ((sizeof(t)*8)-1); \
    t maxv = minv - 1; \
    if ( (a == minv) && (b == maxv) ) \
    { \
        return value + a; \
    } \
    u##t range = b - a + 1; \
    value = (t)(((*((u##t*)(&value))) % range) + (*((u##t*)(&a)))); \
    return value;

int8_t random_s1(int8_t min_inclusive, int8_t max_inclusive)
{
    KPB_RETURN_RANDOM_SIGNED_INT(int8_t, s1, min_inclusive, max_inclusive);
}

int16_t random_s2(int16_t min_inclusive, int16_t max_inclusive)
{
    KPB_RETURN_RANDOM_SIGNED_INT(int16_t, s2, min_inclusive, max_inclusive);
}

int32_t random_s4(int32_t min_inclusive, int32_t max_inclusive)
{
    KPB_RETURN_RANDOM_SIGNED_INT(int32_t, s4, min_inclusive, max_inclusive);
}

int64_t random_s8(int64_t min_inclusive, int64_t max_inclusive)
{
    KPB_RETURN_RANDOM_SIGNED_INT(int64_t, s8, min_inclusive, max_inclusive);
}

uint8_t random_u1(uint8_t min_inclusive, uint8_t max_inclusive)
{
    KPB_RETURN_RANDOM_UNSIGNED_INT(uint8_t, u1, min_inclusive, max_inclusive);
}

uint16_t random_u2(uint16_t min_inclusive, uint16_t max_inclusive)
{
    KPB_RETURN_RANDOM_UNSIGNED_INT(uint16_t, u2, min_inclusive, max_inclusive);
}

uint32_t random_u4(uint32_t min_inclusive, uint32_t max_inclusive)
{
    KPB_RETURN_RANDOM_UNSIGNED_INT(uint32_t, u4, min_inclusive, max_inclusive);
}

uint64_t random_u8(uint64_t min_inclusive, uint64_t max_inclusive)
{
    KPB_RETURN_RANDOM_UNSIGNED_INT(uint64_t, u8, min_inclusive, max_inclusive);
}

float random_f4(float min_inclusive, float max_inclusive)
{
    KPB_SWAP_MINMAX_OR_RETURN_IF_EQUAL(float, min_inclusive, max_inclusive);
    float range = max_inclusive - min_inclusive;
    uint64_t random_range = 0xffffffffffffffff;
    uint64_t random_value = random_u8(0, random_range);
    if ( random_value == 0 )
    {
        return min_inclusive;
    }
    if ( random_value == random_range )
    {
        return max_inclusive;
    }
    float offset = (float)((((double)random_value) * ((double)range)) / ((double)random_range));
    float value = min_inclusive + offset;
    if ( value < min_inclusive )
    {
        return min_inclusive;
    }
    if ( value > max_inclusive )
    {
        return max_inclusive;
    }
    return value;
}

double random_f8(double min_inclusive, double max_inclusive)
{
    KPB_SWAP_MINMAX_OR_RETURN_IF_EQUAL(double, min_inclusive, max_inclusive);
    double range = max_inclusive - min_inclusive;
    uint64_t random_range = 0xffffffffffffffff;
    uint64_t random_value = random_u8(0, random_range);
    if ( random_value == 0 )
    {
        return min_inclusive;
    }
    if ( random_value == random_range )
    {
        return max_inclusive;
    }
    double offset = (((double)random_value) * range) / ((double)random_range);
    double value = min_inclusive + offset;
    if ( value < min_inclusive )
    {
        return min_inclusive;
    }
    if ( value > max_inclusive )
    {
        return max_inclusive;
    }
    return value;
}

bool random_bool(bool min_inclusive, bool max_inclusive)
{
    if ( min_inclusive == max_inclusive )
    {
        return min_inclusive;
    }
    return (random_u1(0x00, 0xff) & 0x80) != 0;
}

size_t random_size(size_t min_inclusive, size_t max_inclusive)
{
    if (min_inclusive > 0xffffffff)
    {
        min_inclusive = 0xffffffff;
    }
    if (max_inclusive > 0xffffffff)
    {
        max_inclusive = 0xffffffff;
    }
    return (size_t)random_u4((uint32_t)min_inclusive, (uint32_t)max_inclusive);
}

bool random_bytes(std::string &bytes, size_t min_size, size_t max_size, uint8_t min_byte, uint8_t max_byte)
{
    size_t size = random_size(min_size, max_size);
    bytes.resize(size, '\x00');
    for ( size_t i = 0; i < size; i++ )
    {
        bytes[i] = (char)random_u1(min_byte, max_byte);
    }
    return true;
}

bool random_char_str(std::string &str, size_t min_size, size_t max_size, uint32_t min_char, uint32_t max_char, IO::Encoding enc)
{
    size_t size = random_size(min_size, max_size);
    str.resize(size, '\x00');
    // TODO: this may not be correct
    (void)enc;
    if (min_char > 0xff)
    {
        min_char = 0xff;
    }
    if (max_char > 0xff)
    {
        max_char = 0xff;
    }
    for ( size_t i = 0; i < size; i++ )
    {
        str[i] = (char)random_u1((uint8_t)min_char, (uint8_t)max_char);
    }
    return true;
}

bool random_wchar_str(std::wstring &str, size_t min_size, size_t max_size, uint32_t min_char, uint32_t max_char, IO::Encoding enc)
{
    size_t size = random_size(min_size, max_size);
    str.resize(size / sizeof(IO::utf16_t), L'\0');
    // TODO: this may not be correct
    (void)enc;
    if (min_char > 0xffff)
    {
        min_char = 0xffff;
    }
    if (max_char > 0xffff)
    {
        max_char = 0xffff;
    }
    for ( size_t i = 0; i < (size / sizeof(IO::utf16_t)); i++ )
    {
        str[i] = (wchar_t)random_u2((uint16_t)min_char, (uint16_t)max_char);
    }
    return true;
}

}  // kpb
