import os
import sys
import platform

if __name__ == "__main__":
    if platform.processor() == "arm":
        platform_directive = "--platform=linux/amd64"
    else:
        platform_directive = ""

    docker_cmd = f"docker build {platform_directive} -t kpb:latest ."
    print(f"Building with Docker: {docker_cmd}")
    sys.exit(os.system(docker_cmd))
