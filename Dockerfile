FROM ubuntu:latest

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get --yes --quiet update && \
    apt-get --yes --quiet upgrade && \
    apt-get --yes install python3 python3-pip python3-yaml cmake gcc g++ gnupg2

#RUN apt-key adv --keyserver hkp://pool.sks-keyservers.net --recv 379CE192D401AB61 && \
#    echo "deb https://dl.bintray.com/kaitai-io/debian jessie main" | tee /etc/apt/sources.list.d/kaitai.list && \
#    apt-get --yes --quiet update && \
#    apt-get --yes install kaitai-struct-compiler

RUN groupadd -r user && \
    useradd -r -g user user && \
    mkdir -p /home/user && \
    chown -R user:user /home/user

USER user

# RUN python3 -m pip install PyYAML

WORKDIR /home/user

ENTRYPOINT [ "/bin/bash" ]