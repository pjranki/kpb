# KPB

Kaitai Protobuf

## What

Define protocols in kaitai yaml files, generate protobuf-style code to parse/serialize binary messages.

## Docker 

You can build a KPB docker container from this repo:

```bash
# windows
py -3 docker_build.py

# linux
python3 docker_build.py
```

To enter the docker container and run commands:

```bash
# windows
py -3 docker_run.py

# linux
python3 docker_run.py

# inside container (to test it works)
python3 test.py
```

## Requirements

 - C/C++ compiler (gcc, g++ & make for Linux, Visual Studios for Windows)
 - cmake (3.19 or higher, older versions "may" work but not recommended)
 - Python 3
 - PyYAML for Python 3 (can be installed using python pip)
 - kaitai-struct-compiler (see https://kaitai.io/#download)

To check your setup, run:

```bash
# windows
py -3 test.py

# linux
python3 test.py
```

## Why

Because I couldn't find a solution (I liked) to parse/serialize binary messages.

Kaitai yaml files are awesome for defining protocols, but only supports parsing (no serializing support).

Generated protobuf code is nice to work with, but only supports protobuf binary messages.

So, take the things I like from both and make a hybrid solution.

## How

Use the ```kpb``` python library to generate code (its a command line tool), Add generated code to your project.

You will also need some runtimes (included in this repo) that provide the common parsing/serializing stuff.

## When

When will it be finished? Maybe never. If you want to take this idea and make it better - go for it.

And let me know when you do.

## Languages

Generates code for c, c++ and python 3.

And, yes, c is still a useful language - even if its not safe.

Where is my rust? Well, I'm only one person - I had to make some hard choices.

## Credits

Huge thanks to those who worked on kaitai/protobuf - they have been a lifeline to my current work with binary protocols.

You may also find that you don't need my repo, checkout these projects for more detail.

https://kaitai.io/

https://developers.google.com/protocol-buffers/


## Future
Dumping spot for my random ideas:
 - Dependency of pos/seq.
 - Pre-parse ksy files with better errors and performance.
