import os
import sys
import shutil
import subprocess

KSC = 'kaitai-struct-compiler'

TARGETS = ['python', 'cpp_stl']

def main():
    root = os.path.dirname(os.path.realpath(__file__))
    root = os.path.realpath(os.path.join(root, '..'))
    out = os.path.realpath(os.path.join(root, 'test', 'out_kaitai'))
    ksy_folder = os.path.realpath(os.path.join(root, 'test', 'ksy'))
    assert os.path.exists(ksy_folder)
    if os.path.exists(out):
        shutil.rmtree(out)
    os.makedirs(out)
    ksy_files = list()
    for ksy_file_name in os.listdir(ksy_folder):
        if not ksy_file_name.endswith('.ksy'):
            continue
        ksy_files.append(os.path.join('..\\ksy\\{}'.format(ksy_file_name)))
    for target in TARGETS:
        args = [KSC, '--target', target]
        args.extend(ksy_files)
        p = subprocess.Popen(args, shell=(os.name=='nt'), cwd=out)
        p.communicate()
        if p.returncode != 0:
            return p.returncode
    return 0

if __name__ == '__main__':
    sys.exit(main())
