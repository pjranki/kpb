import os
import sys
import unittest

from kpb_test.io_expr import IoExpr, IoExprBytes, IoExprEos, IoExprUntil, \
    IoExprUntilClassList, IoExprUntilClassItem, IoExprUntilClassInfo
from kpb_io import KpbIO

class TestValueInstances(unittest.TestCase):

    def test_value_instances(self):
        io_expr = IoExpr()

        # _io.pos
        io_expr.ParseFromString(b'\xa1\xa2\xa3\xa4\xa5\xc1\xc2\xc3')
        self.assertEqual(b'\xa1\xa2\xa3\xa4\xa5', io_expr.data)
        self.assertEqual(b'\xc1\xc2\xc3', io_expr.align)
        io_expr.ParseFromString(b'\xa1\xa2\xa3\xa4\xa5\xc1\xc2\xc3\xff\xff')
        self.assertEqual(b'\xa1\xa2\xa3\xa4\xa5', io_expr.data)
        self.assertEqual(b'\xc1\xc2\xc3', io_expr.align)
        io_expr.Clear()
        io_expr.set_data(b'\xaa')
        io_expr.set_align(b'\xe1\xe2')
        result = io_expr.SerializeToString()
        self.assertEqual(b'\xaa\x00\x00\x00\x00\xe1\xe2\x00', result)
        io_expr.set_data(b'\xa1\xa2\xa3\xa4\xa5')
        io_expr.set_align(b'\xc1\xc2\xc3')
        result = io_expr.SerializeToString()
        self.assertEqual(b'\xa1\xa2\xa3\xa4\xa5\xc1\xc2\xc3', result)
        io_expr.set_data(b'\xa1\xa2\xa3\xa4\xa5\xff')
        io_expr.set_align(b'\xc1\xc2\xc3\xff')
        result = io_expr.SerializeToString()
        self.assertEqual(b'\xa1\xa2\xa3\xa4\xa5\xc1\xc2\xc3', result)

        # _io.size and size-eos: true
        obj_bytes = IoExprBytes()
        result = obj_bytes.SerializeToString()
        self.assertEqual(b'\x00', result)
        self.assertTrue(obj_bytes.set_fixed(0xcc))
        result = obj_bytes.SerializeToString()
        self.assertEqual(b'\xcc', result)
        self.assertTrue(obj_bytes.set_variable(b'\x12\x34\x56\x78'))
        result = obj_bytes.SerializeToString()
        self.assertEqual(b'\xcc\x12\x34\x56\x78', result)
        obj_bytes.ParseFromString(b'\xdd')
        self.assertEqual(0xdd, obj_bytes.fixed)
        self.assertEqual(b'', obj_bytes.variable)
        obj_bytes.ParseFromString(b'\xee\xfe\xab')
        self.assertEqual(0xee, obj_bytes.fixed)
        self.assertEqual(b'\xfe\xab', obj_bytes.variable)

        # _io.eof and repeat: eos
        obj_eos = IoExprEos()
        result = obj_eos.SerializeToString()
        self.assertEqual(b'\x00', result)
        self.assertTrue(obj_eos.set_fixed(0xcc))
        result = obj_eos.SerializeToString()
        self.assertEqual(b'\xcc', result)
        self.assertTrue(obj_eos.add_variable(0x3412))
        self.assertTrue(obj_eos.add_variable(0x7856))
        result = obj_eos.SerializeToString()
        self.assertEqual(b'\xcc\x12\x34\x56\x78', result)
        obj_eos.ParseFromString(b'\xdd')
        self.assertEqual(0xdd, obj_eos.fixed)
        self.assertEqual(0, len(obj_eos.variable))
        obj_eos.ParseFromString(b'\xee\xab\xcd\x67\x89')
        self.assertEqual(0xee, obj_eos.fixed)
        self.assertEqual(2, len(obj_eos.variable))
        self.assertEqual(0xcdab, obj_eos.variable[0])
        self.assertEqual(0x8967, obj_eos.variable[1])

        # _ and repeat-until
        obj_until = IoExprUntil()
        result = obj_until.SerializeToString()
        self.assertEqual(b'\x00', result)
        self.assertTrue(obj_until.set_fixed(0xcc))
        result = obj_until.SerializeToString()
        self.assertEqual(b'\xcc', result)
        self.assertTrue(obj_until.add_variable(0x3412))
        self.assertTrue(obj_until.add_variable(0x7856))
        result = obj_until.SerializeToString()
        self.assertEqual(b'\xcc\x12\x34\x56\x78', result)
        obj_until.ParseFromString(b'\xdd\xad\x0b\xab\xcd')
        self.assertEqual(0xdd, obj_until.fixed)
        self.assertEqual(1, len(obj_until.variable))
        self.assertEqual(0xbad, obj_until.variable[0])
        obj_until.ParseFromString(b'\xee\xab\xcd\xad\x0b')
        self.assertEqual(0xee, obj_until.fixed)
        self.assertEqual(2, len(obj_until.variable))
        self.assertEqual(0xcdab, obj_until.variable[0])
        self.assertEqual(0xbad, obj_until.variable[1])

        # regression test for list of classes when using a class to check its value
        class_list = IoExprUntilClassList()
        self.assertEqual(0, len(class_list.items))
        class_list.ParseFromString(b'\x05')
        self.assertEqual(1, len(class_list.items))
        self.assertEqual(0x05, class_list.items[0].info.value)
        class_list.ParseFromString(b'\x06\x07')
        self.assertEqual(2, len(class_list.items))
        self.assertEqual(0x06, class_list.items[0].info.value)
        self.assertEqual(0x07, class_list.items[1].info.value)
        class_list.ParseFromString(b'\x07\x08\x03')
        self.assertEqual(3, len(class_list.items))
        self.assertEqual(0x07, class_list.items[0].info.value)
        self.assertEqual(0x08, class_list.items[1].info.value)
        self.assertEqual(0x03, class_list.items[2].info.value)
        class_list.ParseFromString(b'\x09\x0a\x03\x0f')
        self.assertEqual(3, len(class_list.items))
        self.assertEqual(0x09, class_list.items[0].info.value)
        self.assertEqual(0x0a, class_list.items[1].info.value)
        self.assertEqual(0x03, class_list.items[2].info.value)
        class_list.Clear()
        result = class_list.SerializeToString()
        self.assertEqual(b'', result)
        class_list.add_items().mutable_info().value = 0x01
        result = class_list.SerializeToString()
        self.assertEqual(b'\x01', result)
        class_list.add_items().mutable_info().value = 0x02
        result = class_list.SerializeToString()
        self.assertEqual(b'\x01\x02', result)
        class_list.add_items().mutable_info().value = 0x03
        result = class_list.SerializeToString()
        self.assertEqual(b'\x01\x02\x03', result)
        class_list.add_items().mutable_info().value = 0x04
        result = class_list.SerializeToString()
        self.assertEqual(b'\x01\x02\x03', result)
