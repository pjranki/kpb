import os
import sys
import unittest

from kpb_test.pos_root_parent import PosRootParent, PosChild, PosGrandchild
from kpb_io import KpbIO, KpbIOError

class TestPosRootParent(unittest.TestCase):

    def test_pos_root_parent(self):

        # check default state (after constructor, after a clear)
        root = PosRootParent()
        self.assertEqual(28, root.Size())
        root.Clear()
        self.assertEqual(28, root.Size())

        # parse at correct positions in correct substreams
        root.ParseFromString(b"\x11\x11\x11\x11\x22\x22\x22\x22\x33\x33\x33\x33\x44\x44\x44\x44\x55\x55\x55\x55\x66\x66\x66\x66\x77\x77\x77\x77")
        self.assertEqual(0x11111111, root.value)
        self.assertEqual(0x22222222, root.child.value_parent)
        self.assertEqual(0x33333333, root.child.value_root)
        self.assertEqual(0x44444444, root.child.grandchild.value_root)
        self.assertEqual(0x55555555, root.child.value)
        self.assertEqual(0x66666666, root.child.grandchild.value_parent)
        self.assertEqual(0x77777777, root.child.grandchild.value)

        # serialize at correct positions in correct substreams
        root.Clear()
        child = root.mutable_child()
        grandchild = child.mutable_grandchild()
        self.assertTrue(root.set_value(0x77777777))
        self.assertTrue(child.set_value_parent(0x66666666))
        self.assertTrue(child.set_value_root(0x55555555))
        self.assertTrue(grandchild.set_value_root(0x41414141))
        self.assertTrue(child.set_value(0x33333333))
        self.assertTrue(grandchild.set_value_parent(0x22222222))
        self.assertTrue(grandchild.set_value(0x11111111))
        result = root.SerializeToString()
        self.assertEqual(
            b"\x77\x77\x77\x77\x66\x66\x66\x66\x55\x55\x55\x55\x41\x41\x41\x41\x33\x33\x33\x33\x22\x22\x22\x22\x11\x11\x11\x11",
            result
        )
