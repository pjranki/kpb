import os
import sys
import unittest

from kpb_test.parent_child import ParentChild


class TestParentChild(unittest.TestCase):

    def test_parent_child(self):
        child = ParentChild()
        self.assertEqual(42, child.parent_id)
