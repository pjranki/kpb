import os
import sys
import unittest

from kpb_test.object_single import ObjectSingle
from kpb_io import KpbIO

class TestObjectSingle(unittest.TestCase):

    def test_object_single(self):
        object_single = ObjectSingle()
        self.assertEqual(0x00000000, object_single.other.value)
        self.assertEqual(b"\x00\x00\x00\x00", object_single.SerializeToString())
        sub_object = object_single.mutable_other()
        sub_object.set_value(0x12345678)
        self.assertEqual(0x12345678, sub_object.value)
        self.assertEqual(0x12345678, object_single.other.value)
        self.assertEqual(b"\x78\x56\x34\x12", object_single.SerializeToString())
        object_single.Clear()
        self.assertEqual(0x00000000, object_single.other.value)
        self.assertEqual(b"\x00\x00\x00\x00", object_single.SerializeToString())

        object_single.ParseFromString(b"\xab\xcd\xef\x89")
        sub_object = object_single.mutable_other()
        self.assertEqual(0x89efcdab, sub_object.value)
        self.assertEqual(0x89efcdab, object_single.other.value)
        object_single.ParseFromString(b"\x00\x00\x00\x00")
        sub_object = object_single.mutable_other()
        self.assertEqual(0x00000000, sub_object.value)
        self.assertEqual(0x00000000, object_single.other.value)
