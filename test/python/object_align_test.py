import os
import sys
import unittest

from kpb_test.object_align import ObjectAlign
from kpb_io import KpbIO

class TestObjectAlign(unittest.TestCase):

    def test_object_align(self):
        obj = ObjectAlign()

        obj.value_1 = 0xa
        obj.mutable_object_b4().value = 0xc
        obj.value_2 = 0x5
        obj.add_value_list(0x1)
        obj.add_value_list(0x2)
        obj.add_value_list(0x3)
        obj.add_value_list(0x4)

        self.assertEqual(b"\xa0\xc0\x51\x23\x40", obj.SerializeToString())

        obj.Clear()
        obj.ParseFromString(b"\x50\x30\xa4\x32\x10")
        self.assertEqual(0x5, obj.value_1)
        self.assertEqual(0x3, obj.object_b4.value)
        self.assertEqual(0xa, obj.value_2)
        self.assertEqual(0x4, obj.value_list[0])
        self.assertEqual(0x3, obj.value_list[1])
        self.assertEqual(0x2, obj.value_list[2])
        self.assertEqual(0x1, obj.value_list[3])
