class ProcessValueB3(object):

    def __init__(self, root):
        self.root = root

    def getter(self, value):
        return value + self.root.memset_value

    def setter(self, value):
        return value - self.root.memset_value
