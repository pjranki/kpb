class ProcessValueF4(object):

    def __init__(self, multiple_float_by_4):
        self.multiple_float_by_4 = multiple_float_by_4

    def getter(self, value):
        return (value * 4.0) if self.multiple_float_by_4 else value

    def setter(self, value):
        return (value * 4.0) if self.multiple_float_by_4 else value
