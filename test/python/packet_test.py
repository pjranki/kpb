import os
import sys
import unittest

from kpb_test.packet import Packet, PacketHeader, PacketBody
from kpb_io import KpbIO

class TestPacket(unittest.TestCase):
    TEST_EMPTY_PACKET = b"\x0a\x00\x00\x00" + b"\x02\x00\x00\x00" + b"\x00\x00"
    TEST_DATA = b"\x41\x42\xff\x00\xcc"
    TEST_DATA_PACKET = b"\x0f\x00\x00\x00" + b"\x07\x00\x00\x00" + b"\x05\x00" + TEST_DATA

    def test_packet(self):
        packet = Packet()

        # -default is used to create the default sizes for the packet when empty
        self.assertEqual(8, packet.header.header_size_)
        self.assertEqual(2, packet.header.body_size_)
        self.assertEqual(10, packet.header.total_size_)

        # serialize as expected
        result = packet.SerializeToString()
        self.assertEqual(self.TEST_EMPTY_PACKET, result)

        # calling Clear() calls Update() -> and should calculate the same values as -default
        packet.Clear()
        self.assertEqual(8, packet.header.header_size_)
        self.assertEqual(2, packet.header.body_size_)
        self.assertEqual(10, packet.header.total_size_)

        # serialize as expected
        result = packet.SerializeToString()
        self.assertEqual(self.TEST_EMPTY_PACKET, result)

        # adding some data will update all size fields
        body = packet.mutable_body()
        body.data = self.TEST_DATA
        self.assertEqual(8, packet.header.header_size_)
        self.assertEqual(7, packet.header.body_size_)
        self.assertEqual(15, packet.header.total_size_)

        # serialize as expected
        result = packet.SerializeToString()
        self.assertEqual(self.TEST_DATA_PACKET, result)

        # clear
        packet.Clear()

        # serialize as expected
        result = packet.SerializeToString()
        self.assertEqual(self.TEST_EMPTY_PACKET, result)

        # parse as expected
        packet.ParseFromString(self.TEST_DATA_PACKET)
        self.assertEqual(8, packet.header.header_size_)
        self.assertEqual(7, packet.header.body_size_)
        self.assertEqual(15, packet.header.total_size_)
        self.assertEqual(self.TEST_DATA, packet.body.data)
