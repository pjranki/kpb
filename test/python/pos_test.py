import os
import sys
import unittest

from kpb_test.pos import Pos, SubPos
from kpb_io import KpbIO, KpbIOError

class TestPos(unittest.TestCase):

    def test_pos(self):

        # check default state (after constructor, after a clear)
        sub_pos = SubPos()
        self.assertEqual(11, sub_pos.Size())
        sub_pos.Clear()
        self.assertEqual(11, sub_pos.Size())

        # test that the size includes position instances
        sub_pos.Clear()
        sub_pos.value1 = 0xdeadbeef
        sub_pos.value2 = 0xcafebabe
        self.assertEqual(11, sub_pos.Size())
        result = sub_pos.SerializeToString()
        self.assertEqual(b"\xef\xbe\xad\xde\x00\x00\x00\xbe\xba\xfe\xca", result)

        # test that reads can't work out of bounds
        sub_pos.Clear()
        try:
            sub_pos.ParseFromString(b"\xef\xbe\xad\x12\x00\x00\x00\x34\xba")
            raise AssertionError("This should cause 'not enough memory' exception")
        except KpbIOError:
            pass

        # successful read
        sub_pos.Clear()
        self.assertTrue(sub_pos.ParseFromString(b"\xef\xbe\xad\x12\x00\x00\x00\x34\xba\xfe\xca"))
        self.assertEqual(0x12adbeef, sub_pos.value1)
        self.assertEqual(0xcafeba34, sub_pos.value2)

        # check default state (after constructor, after a clear)
        obj = Pos() 
        self.assertEqual(19, obj.Size())
        obj.Clear()
        self.assertEqual(19, obj.Size())

        # read and stop at error code
        obj.Clear()
        self.assertTrue(obj.ParseFromString(b"\x01\x00\x00\xc0"))
        self.assertEqual(0xc0000001, obj.error_code)
        self.assertFalse(obj.has_sub_pos_offset())
        self.assertFalse(obj.has_sub_pos_size_())
        self.assertFalse(obj.has_sub_pos())
        self.assertEqual(4, obj.Size())

        # fail to read, not enough data
        obj.Clear()
        try:
            obj.ParseFromString(b"\x09\x00\x00\x00\x0b\x00\x00\x00\x00\xef\xbe\xad\xde\x00\x00\x00\xbe\xba\xfe")
            raise AssertionError("This should cause 'not enough memory' exception")
        except KpbIOError:
            pass

        # read all
        obj.Clear()
        self.assertTrue(obj.ParseFromString(b"\x09\x00\x00\x00\x0b\x00\x00\x00\x00\xef\xbe\xad\xde\x00\x00\x00\xbe\xba\xfe\xca"))
        self.assertEqual(0, obj.error_code)
        self.assertTrue(obj.has_sub_pos_offset())
        self.assertTrue(obj.has_sub_pos_size_())
        self.assertTrue(obj.has_sub_pos())
        self.assertEqual(9, obj.sub_pos_offset)
        self.assertEqual(11, obj.sub_pos_size_)
        self.assertEqual(0xdeadbeef, obj.sub_pos.value1)
        self.assertEqual(0xcafebabe, obj.sub_pos.value2)
        self.assertEqual(20, obj.Size())

        # write error
        obj.Clear()
        obj.first_u4 = 0xc0001234
        result = obj.SerializeToString()
        self.assertEqual(b"\x34\x12\x00\xc0", result)

        # write all
        obj.Clear()
        obj.sub_pos_offset = 9
        obj.sub_pos.value1 = 0xdeadbeef
        obj.sub_pos.value2 = 0xcafebabe
        result = obj.SerializeToString()
        self.assertEqual(b"\x09\x00\x00\x00\x0b\x00\x00\x00\x00\xef\xbe\xad\xde\x00\x00\x00\xbe\xba\xfe\xca", result)
