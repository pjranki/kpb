import os
import sys
import unittest

from kpb_test.process_value import ProcessValue, SubProcessValue

class TestProcessValue(unittest.TestCase):

    def test_process_value(self):
        obj = ProcessValue()

        # value1 tests
        self.assertEqual(10, obj.value1)
        obj.value1 = 20
        self.assertEqual(25, obj.value1)

        # value2 tests
        self.assertEqual(False, obj.value2)
        obj.value2 = True
        self.assertEqual(True, obj.value2)
        obj.value2 = False
        self.assertEqual(False, obj.value2)
        obj.invert_logic = True
        obj.value2 = True
        obj.invert_logic = False
        self.assertEqual(False, obj.value2)
        obj.invert_logic = True
        obj.value2 = False
        obj.invert_logic = False
        self.assertEqual(True, obj.value2)
        obj.value2 = True
        obj.invert_logic = True
        self.assertEqual(False, obj.value2)
        obj.invert_logic = False
        obj.value2 = False
        obj.invert_logic = True
        self.assertEqual(True, obj.value2)
        obj.invert_logic = False

        # value3 tests
        self.assertEqual(10, obj.value3)
        obj.value3 = 20
        self.assertEqual(25, obj.value3)

        # value4 tests
        self.assertEqual(0, obj.value4)
        obj.memset_value = 10
        self.assertEqual(10, obj.value4)
        obj.memset_value = 4
        obj.value4 = 12
        obj.memset_value = 1
        self.assertEqual(9, obj.value4)
        obj.memset_value = 0

        # value5 tests
        self.assertEqual("", obj.value5)
        obj.value5 = "abc"
        self.assertEqual("\x00\x00\x00", obj.value5)
        obj.memset_value = ord('z')
        obj.value5 = "abcd"
        self.assertEqual("zzzz", obj.value5)
        obj.memset_value = ord('y')
        self.assertEqual("yyyy", obj.value5)
        obj.memset_value = 0

        # value6 tests
        self.assertEqual(b"", obj.value6)
        obj.value6 = b"abc"
        self.assertEqual(b"\x00\x00\x00", obj.value6)
        obj.memset_value = ord('z')
        obj.value6 = b"abcd"
        self.assertEqual(b"zzzz", obj.value6)
        obj.memset_value = ord('y')
        self.assertEqual(b"yyyy", obj.value6)
        obj.memset_value = 0

        # value7 tests
        self.assertAlmostEqual(0.0, obj.value7)
        obj.value7 = 1.0
        self.assertAlmostEqual(1.0, obj.value7)
        obj.multiple_float_by_4 = True
        self.assertAlmostEqual(4.0, obj.value7)
        obj.value7 = 2.0
        obj.multiple_float_by_4 = False
        self.assertAlmostEqual(8.0, obj.value7)

        # value8 tests
        obj.getter_count = 0
        obj.setter_count = 0
        self.assertEqual(0xff, obj.value8.input_value)
        self.assertEqual(1, obj.getter_count)
        self.assertEqual(0, obj.setter_count)
        obj.mutable_value8().input_value = 0xaa
        self.assertEqual(0xaa, obj.value8.input_value)
        self.assertEqual(2, obj.getter_count)
        self.assertEqual(1, obj.setter_count)

        # value9 tests
        obj.getter_count = 0
        obj.setter_count = 0
        self.assertEqual(0x00, obj.value9.memset_value)
        self.assertEqual(1, obj.getter_count)
        self.assertEqual(0, obj.setter_count)
        self.assertEqual(0x00, obj.value9.memset_value)
        self.assertEqual(2, obj.getter_count)
        self.assertEqual(0, obj.setter_count)

        # value10 tests
        self.assertEqual(False, obj.value10)
        obj.invert_logic = True
        self.assertEqual(True, obj.value10)
        obj.invert_logic = False
        self.assertEqual(False, obj.value10)
