import os
import sys
import unittest

from kpb_test.bytes_list import BytesList
from kpb_io import KpbIO

class TestBytesList(unittest.TestCase):

    def test_bytes_list(self):
        bytes_list = BytesList()
        self.assertEqual(0, bytes_list.data2_list_size())
        self.assertEqual(0, bytes_list.data4_list_size())
        self.assertEqual(0, len(bytes_list.data2_list))
        self.assertEqual(0, len(bytes_list.data4_list))
        result = bytes_list.SerializeToString()
        self.assertEqual(b"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", result)
        bytes_list.add_data2_list(b"A")
        bytes_list.add_data4_list(b"a")
        self.assertEqual(1, bytes_list.data2_list_size())
        self.assertEqual(1, bytes_list.data4_list_size())
        self.assertEqual(1, len(bytes_list.data2_list))
        self.assertEqual(1, len(bytes_list.data4_list))
        self.assertEqual(b"A", bytes_list.data2_list[0])
        self.assertEqual(b"a", bytes_list.data4_list[0])
        result = bytes_list.SerializeToString()
        self.assertEqual(b"\x41\x00\x00\x00\x00\x00\x00\x00\x00\x00\x61\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", result)
        bytes_list.add_data2_list(b"BC")
        bytes_list.add_data4_list(b"bc")
        self.assertEqual(2, bytes_list.data2_list_size())
        self.assertEqual(2, bytes_list.data4_list_size())
        self.assertEqual(2, len(bytes_list.data2_list))
        self.assertEqual(2, len(bytes_list.data4_list))
        self.assertEqual(b"A", bytes_list.data2_list[0])
        self.assertEqual(b"BC", bytes_list.data2_list[1])
        self.assertEqual(b"a", bytes_list.data4_list[0])
        self.assertEqual(b"bc", bytes_list.data4_list[1])
        result = bytes_list.SerializeToString()
        self.assertEqual(b"\x41\x00\x42\x43\x00\x00\x00\x00\x00\x00\x61\x00\x00\x00\x62\x63\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", result)
        bytes_list.add_data2_list(b"DEF")
        bytes_list.add_data2_list(b"DEF")
        bytes_list.add_data2_list(b"DEF")
        bytes_list.add_data4_list(b"def")
        bytes_list.add_data4_list(b"def")
        bytes_list.add_data4_list(b"def")
        self.assertEqual(5, bytes_list.data2_list_size())
        self.assertEqual(5, bytes_list.data4_list_size())
        self.assertEqual(5, len(bytes_list.data2_list))
        self.assertEqual(5, len(bytes_list.data4_list))
        self.assertEqual(b"A", bytes_list.data2_list[0])
        self.assertEqual(b"BC", bytes_list.data2_list[1])
        self.assertEqual(b"DEF", bytes_list.data2_list[2])
        self.assertEqual(b"DEF", bytes_list.data2_list[3])
        self.assertEqual(b"DEF", bytes_list.data2_list[4])
        self.assertEqual(b"a", bytes_list.data4_list[0])
        self.assertEqual(b"bc", bytes_list.data4_list[1])
        self.assertEqual(b"def", bytes_list.data4_list[2])
        self.assertEqual(b"def", bytes_list.data4_list[3])
        self.assertEqual(b"def", bytes_list.data4_list[4])
        result = bytes_list.SerializeToString()
        self.assertEqual(b"\x41\x00\x42\x43\x44\x45\x44\x45\x44\x45\x61\x00\x00\x00\x62\x63\x00\x00\x64\x65\x66\x00\x64\x65\x66\x00\x64\x65\x66\x00", result)
        bytes_list.add_data2_list(b"G")
        bytes_list.add_data4_list(b"g")
        self.assertEqual(6, bytes_list.data2_list_size())
        self.assertEqual(6, bytes_list.data4_list_size())
        self.assertEqual(6, len(bytes_list.data2_list))
        self.assertEqual(6, len(bytes_list.data4_list))
        self.assertEqual(b"A", bytes_list.data2_list[0])
        self.assertEqual(b"BC", bytes_list.data2_list[1])
        self.assertEqual(b"DEF", bytes_list.data2_list[2])
        self.assertEqual(b"DEF", bytes_list.data2_list[3])
        self.assertEqual(b"DEF", bytes_list.data2_list[4])
        self.assertEqual(b"G", bytes_list.data2_list[5])
        self.assertEqual(b"a", bytes_list.data4_list[0])
        self.assertEqual(b"bc", bytes_list.data4_list[1])
        self.assertEqual(b"def", bytes_list.data4_list[2])
        self.assertEqual(b"def", bytes_list.data4_list[3])
        self.assertEqual(b"def", bytes_list.data4_list[4])
        self.assertEqual(b"g", bytes_list.data4_list[5])
        result = bytes_list.SerializeToString()
        self.assertEqual(b"\x41\x00\x42\x43\x44\x45\x44\x45\x44\x45\x61\x00\x00\x00\x62\x63\x00\x00\x64\x65\x66\x00\x64\x65\x66\x00\x64\x65\x66\x00", result)

        bytes_list.Clear()
        bytes_list.ParseFromString(b"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00")
        self.assertEqual(5, bytes_list.data2_list_size())
        self.assertEqual(5, bytes_list.data4_list_size())
        self.assertEqual(5, len(bytes_list.data2_list))
        self.assertEqual(5, len(bytes_list.data4_list))
        self.assertEqual(b"\x00\x00", bytes_list.data2_list[0])
        self.assertEqual(b"\x00\x00", bytes_list.data2_list[1])
        self.assertEqual(b"\x00\x00", bytes_list.data2_list[2])
        self.assertEqual(b"\x00\x00", bytes_list.data2_list[3])
        self.assertEqual(b"\x00\x00", bytes_list.data2_list[4])
        self.assertEqual(b"\x00\x00\x00\x00", bytes_list.data4_list[0])
        self.assertEqual(b"\x00\x00\x00\x00", bytes_list.data4_list[1])
        self.assertEqual(b"\x00\x00\x00\x00", bytes_list.data4_list[2])
        self.assertEqual(b"\x00\x00\x00\x00", bytes_list.data4_list[3])
        self.assertEqual(b"\x00\x00\x00\x00", bytes_list.data4_list[4])
        bytes_list.Clear()
        bytes_list.ParseFromString(b"\x41\x00\x00\x00\x00\x00\x00\x00\x00\x00\x61\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00")
        self.assertEqual(5, bytes_list.data2_list_size())
        self.assertEqual(5, bytes_list.data4_list_size())
        self.assertEqual(5, len(bytes_list.data2_list))
        self.assertEqual(5, len(bytes_list.data4_list))
        self.assertEqual(b"A\x00", bytes_list.data2_list[0])
        self.assertEqual(b"\x00\x00", bytes_list.data2_list[1])
        self.assertEqual(b"\x00\x00", bytes_list.data2_list[2])
        self.assertEqual(b"\x00\x00", bytes_list.data2_list[3])
        self.assertEqual(b"\x00\x00", bytes_list.data2_list[4])
        self.assertEqual(b"a\x00\x00\x00", bytes_list.data4_list[0])
        self.assertEqual(b"\x00\x00\x00\x00", bytes_list.data4_list[1])
        self.assertEqual(b"\x00\x00\x00\x00", bytes_list.data4_list[2])
        self.assertEqual(b"\x00\x00\x00\x00", bytes_list.data4_list[3])
        self.assertEqual(b"\x00\x00\x00\x00", bytes_list.data4_list[4])
        bytes_list.Clear()
        bytes_list.ParseFromString(b"\x41\x00\x42\x43\x00\x00\x00\x00\x00\x00\x61\x00\x00\x00\x62\x63\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00")
        self.assertEqual(5, bytes_list.data2_list_size())
        self.assertEqual(5, bytes_list.data4_list_size())
        self.assertEqual(5, len(bytes_list.data2_list))
        self.assertEqual(5, len(bytes_list.data4_list))
        self.assertEqual(b"A\x00", bytes_list.data2_list[0])
        self.assertEqual(b"BC", bytes_list.data2_list[1])
        self.assertEqual(b"\x00\x00", bytes_list.data2_list[2])
        self.assertEqual(b"\x00\x00", bytes_list.data2_list[3])
        self.assertEqual(b"\x00\x00", bytes_list.data2_list[4])
        self.assertEqual(b"a\x00\x00\x00", bytes_list.data4_list[0])
        self.assertEqual(b"bc\x00\x00", bytes_list.data4_list[1])
        self.assertEqual(b"\x00\x00\x00\x00", bytes_list.data4_list[2])
        self.assertEqual(b"\x00\x00\x00\x00", bytes_list.data4_list[3])
        self.assertEqual(b"\x00\x00\x00\x00", bytes_list.data4_list[4])
        bytes_list.Clear()
        bytes_list.ParseFromString(b"\x41\x00\x42\x43\x44\x45\x44\x45\x44\x45\x61\x00\x00\x00\x62\x63\x00\x00\x64\x65\x66\x00\x64\x65\x66\x00\x64\x65\x66\x00")
        self.assertEqual(5, bytes_list.data2_list_size())
        self.assertEqual(5, bytes_list.data4_list_size())
        self.assertEqual(5, len(bytes_list.data2_list))
        self.assertEqual(5, len(bytes_list.data4_list))
        self.assertEqual(b"A\x00", bytes_list.data2_list[0])
        self.assertEqual(b"BC", bytes_list.data2_list[1])
        self.assertEqual(b"DE", bytes_list.data2_list[2])
        self.assertEqual(b"DE", bytes_list.data2_list[3])
        self.assertEqual(b"DE", bytes_list.data2_list[4])
        self.assertEqual(b"a\x00\x00\x00", bytes_list.data4_list[0])
        self.assertEqual(b"bc\x00\x00", bytes_list.data4_list[1])
        self.assertEqual(b"def\x00", bytes_list.data4_list[2])
        self.assertEqual(b"def\x00", bytes_list.data4_list[3])
        self.assertEqual(b"def\x00", bytes_list.data4_list[4])
