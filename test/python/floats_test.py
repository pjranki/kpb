import os
import sys
import unittest

from kpb_test.floats import Floats
from kpb_io import KpbIO

FLOAT_0_0_LE            = b"\x00\x00\x00\x00"
DOUBLE_0_0_LE           = b"\x00\x00\x00\x00\x00\x00\x00\x00"
FLOAT_0_0_BE            = b"\x00\x00\x00\x00"
DOUBLE_0_0_BE           = b"\x00\x00\x00\x00\x00\x00\x00\x00"
FLOAT_1_1_LE            = b"\xcd\xcc\x8c\x3f"
DOUBLE_1_1_LE           = b"\x9a\x99\x99\x99\x99\x99\xf1\x3f"
FLOAT_1_1_BE            = b"\x3f\x8c\xcc\xcd"
DOUBLE_1_1_BE           = b"\x3f\xf1\x99\x99\x99\x99\x99\x9a"
FLOAT_NEG_2_2_LE        = b"\xcd\xcc\x0c\xc0"
DOUBLE_NEG_2_2_LE       = b"\x9a\x99\x99\x99\x99\x99\x01\xc0"
FLOAT_NEG_2_2_BE        = b"\xc0\x0c\xcc\xcd"
DOUBLE_NEG_2_2_BE       = b"\xc0\x01\x99\x99\x99\x99\x99\x9a"

EXPECTED_0_0            = FLOAT_0_0_LE + DOUBLE_0_0_LE + FLOAT_0_0_LE + DOUBLE_0_0_LE + FLOAT_0_0_BE + DOUBLE_0_0_BE
EXPECTED_1_1            = FLOAT_1_1_LE + DOUBLE_1_1_LE + FLOAT_1_1_LE + DOUBLE_1_1_LE + FLOAT_1_1_BE + DOUBLE_1_1_BE
EXPECTED_NEG_2_2        = FLOAT_NEG_2_2_LE + DOUBLE_NEG_2_2_LE + FLOAT_NEG_2_2_LE + DOUBLE_NEG_2_2_LE + FLOAT_NEG_2_2_BE + DOUBLE_NEG_2_2_BE


class TestFloats(unittest.TestCase):

    def test_floats(self):
        floats = Floats()
        result = floats.SerializeToString()
        self.assertEqual(EXPECTED_0_0, result)
        self.assertAlmostEqual(0.0, floats.float_)
        self.assertAlmostEqual(0.0, floats.double_)
        self.assertAlmostEqual(0.0, floats.floatle)
        self.assertAlmostEqual(0.0, floats.doublele)
        self.assertAlmostEqual(0.0, floats.floatbe)
        self.assertAlmostEqual(0.0, floats.doublebe)

        floats.float_ = 1.1
        floats.double_ = 1.1
        floats.floatle = 1.1
        floats.doublele = 1.1
        floats.floatbe = 1.1
        floats.doublebe = 1.1
        self.assertAlmostEqual(1.1, floats.float_)
        self.assertAlmostEqual(1.1, floats.double_)
        self.assertAlmostEqual(1.1, floats.floatle)
        self.assertAlmostEqual(1.1, floats.doublele)
        self.assertAlmostEqual(1.1, floats.floatbe)
        self.assertAlmostEqual(1.1, floats.doublebe)
        result = floats.SerializeToString()
        self.assertEqual(EXPECTED_1_1, result)

        floats.float_ = -2.2
        floats.double_ = -2.2
        floats.floatle = -2.2
        floats.doublele = -2.2
        floats.floatbe = -2.2
        floats.doublebe = -2.2
        self.assertAlmostEqual(-2.2, floats.float_)
        self.assertAlmostEqual(-2.2, floats.double_)
        self.assertAlmostEqual(-2.2, floats.floatle)
        self.assertAlmostEqual(-2.2, floats.doublele)
        self.assertAlmostEqual(-2.2, floats.floatbe)
        self.assertAlmostEqual(-2.2, floats.doublebe)
        result = floats.SerializeToString()
        self.assertEqual(EXPECTED_NEG_2_2, result)

        floats.Clear()
        self.assertAlmostEqual(0.0, floats.float_)
        self.assertAlmostEqual(0.0, floats.double_)
        self.assertAlmostEqual(0.0, floats.floatle)
        self.assertAlmostEqual(0.0, floats.doublele)
        self.assertAlmostEqual(0.0, floats.floatbe)
        self.assertAlmostEqual(0.0, floats.doublebe)
        result = floats.SerializeToString()
        self.assertEqual(EXPECTED_0_0, result)

        floats.ParseFromString(EXPECTED_1_1)
        self.assertAlmostEqual(1.1, floats.float_)
        self.assertAlmostEqual(1.1, floats.double_)
        self.assertAlmostEqual(1.1, floats.floatle)
        self.assertAlmostEqual(1.1, floats.doublele)
        self.assertAlmostEqual(1.1, floats.floatbe)
        self.assertAlmostEqual(1.1, floats.doublebe)

        floats.ParseFromString(EXPECTED_NEG_2_2)
        self.assertAlmostEqual(-2.2, floats.float_)
        self.assertAlmostEqual(-2.2, floats.double_)
        self.assertAlmostEqual(-2.2, floats.floatle)
        self.assertAlmostEqual(-2.2, floats.doublele)
        self.assertAlmostEqual(-2.2, floats.floatbe)
        self.assertAlmostEqual(-2.2, floats.doublebe)

        floats.ParseFromString(EXPECTED_0_0)
        self.assertAlmostEqual(0.0, floats.float_)
        self.assertAlmostEqual(0.0, floats.double_)
        self.assertAlmostEqual(0.0, floats.floatle)
        self.assertAlmostEqual(0.0, floats.doublele)
        self.assertAlmostEqual(0.0, floats.floatbe)
        self.assertAlmostEqual(0.0, floats.doublebe)
