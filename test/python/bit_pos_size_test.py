import os
import sys
import unittest

from kpb_test.bit_pos_size import BitPosSize_
from kpb_io import KpbIO

class TestBitPosSize(unittest.TestCase):

    def test_bit_pos_size(self):
        obj = BitPosSize_()

        obj.Clear()
        obj.bit_count = 6
        obj.bit_align = 7
        obj.expected_pos_in_bits = 8
        obj.expected_size_in_bits = 8
        for _ in range(obj.bit_count):
            obj.add_bits(1)
        obj.end = 1
        obj.add_ok(0x55)
        result = obj.SerializeToString()
        self.assertEqual(b'\xfd\x55', result)

        obj.Clear()
        obj.bit_count = 7
        obj.bit_align = 7
        obj.expected_pos_in_bits = 8
        obj.expected_size_in_bits = 8
        for _ in range(obj.bit_count):
            obj.add_bits(1)
        obj.end = 1
        obj.add_ok(0x55)
        result = obj.SerializeToString()
        self.assertEqual(b'\xff\x55', result)

        obj.Clear()
        obj.bit_count = 8
        obj.bit_align = 7
        obj.expected_pos_in_bits = 15
        obj.expected_size_in_bits = 15
        for _ in range(obj.bit_count):
            obj.add_bits(1)
        obj.end = 1
        obj.add_ok(0x55)
        result = obj.SerializeToString()
        self.assertEqual(b'\xff\x02\xaa', result)

        obj.Clear()
        obj.bit_count = 6
        obj.bit_align = 7
        obj.expected_pos_in_bits = 8
        obj.expected_size_in_bits = 16
        obj.ParseFromString(b'\xfd\x55')
        for i in range(obj.bit_count):
            self.assertEqual(1, obj.bits_at(i))
        self.assertEqual(1, obj.padding_size())
        self.assertEqual(1, obj.end)
        self.assertEqual(0x55, obj.ok_at(0))

        obj.Clear()
        obj.bit_count = 7
        obj.bit_align = 7
        obj.expected_pos_in_bits = 8
        obj.expected_size_in_bits = 16
        obj.ParseFromString(b'\xff\x55')
        for i in range(obj.bit_count):
            self.assertEqual(1, obj.bits_at(i))
        self.assertEqual(0, obj.padding_size())
        self.assertEqual(1, obj.end)
        self.assertEqual(0x55, obj.ok_at(0))

        obj.Clear()
        obj.bit_count = 8
        obj.bit_align = 7
        obj.expected_pos_in_bits = 15
        obj.expected_size_in_bits = 24
        obj.ParseFromString(b'\xff\x02\xaa')
        for i in range(obj.bit_count):
            self.assertEqual(1, obj.bits_at(i))
        self.assertEqual(6, obj.padding_size())
        self.assertEqual(1, obj.end)
        self.assertEqual(0x55, obj.ok_at(0))
