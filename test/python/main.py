import os
import sys
import unittest

out = os.environ['KPB_OUT_PATH']  # see test.py script that provides this environment variable
assert os.path.exists(out)

sys.path.insert(0, os.path.join(out, 'test', 'python'))
sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..', '..', 'python'))

from integers_test import *
from bitfield_test import *
from basic_list_test import *
from strings_single_test import *
from strings_list_test import *
from bytes_single_test import *
from bytes_list_test import *
from object_single_test import *
from object_list_test import *
from imports_test import *
from enums_test import *
from switchon_test import *
from params_test import *
from value_instances_test import *
from expressions_test import *
from update_test import *
from update_member_test import *
from typecast_test import *
from name_deconflict_test import *
from io_expr_test import *
from boolean_test import *
from object_size_test import *
from object_align_test import *
from self_root_parent_test import *
from floats_test import *
from packet_test import *
from contents_test import *
from randomize_test import *
from process_test import *
from constructor_recursion_test import *
from pos_test import *
from pos_root_parent_test import *
from copy_from_test import *
from parse_or_fill_test import *
from bitalign_test import *
from process_value_test import *
from parent_child_test import *
from set_instance_value_test import *
from bit_pos_size_test import *

def main():
    print('Start python test')
    unittest.main()
    print('Done python test')


if __name__ == '__main__':
    main()
