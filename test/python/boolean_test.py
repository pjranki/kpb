import os
import sys
import unittest

from kpb_test.boolean import Boolean
from kpb_io import KpbIO

class TestBoolean(unittest.TestCase):

    def test_value_instances(self):
        value = Boolean()
        value.set_value1(True)
        self.assertTrue(value.value1)
        value.set_value1(False)
        self.assertFalse(value.value1)
