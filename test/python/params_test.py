import os
import sys
import unittest

from kpb_test.params import Params
from kpb_io import KpbIO

class TestParams(unittest.TestCase):

    def test_params(self):
        params = Params()
        result = params.SerializeToString()
        self.assertEqual(b"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", result)
        obj2 = params.mutable_data2()
        self.assertEqual(0xcafe, obj2.arg1)
        obj3 = params.add_data3()
        self.assertEqual(42, obj3.arg1)
        self.assertEqual(0xbeef, obj3.arg2)
        obj3 = params.add_data3()
        self.assertEqual(42, obj3.arg1)
        self.assertEqual(0xbeef, obj3.arg2)
        params.ParseFromString(b"\x01\x00\x00\x00\x02\x00\x00\x00\x03\x00\x00\x00\x04\x00\x00\x00")
        self.assertEqual(1, params.data1.value1)
        self.assertEqual(2, params.data2.value1)
        self.assertEqual(0xcafe, params.data2.arg1)
        self.assertEqual(2, len(params.data3))
        self.assertEqual(3, params.data3[0].value1)
        self.assertEqual(42, params.data3[0].arg1)
        self.assertEqual(0xbeef, params.data3[0].arg2)
        self.assertEqual(4, params.data3[1].value1)
        self.assertEqual(42, params.data3[1].arg1)
        self.assertEqual(0xbeef, params.data3[1].arg2)

        # test that list size changes AFTER parameter values are set
        params.Clear()
        obj3 = params.add_data3()
        self.assertEqual(0, obj3.index)
        self.assertEqual(1, obj3.current_count)
        obj3 = params.add_data3()
        self.assertEqual(1, obj3.index)
        self.assertEqual(2, obj3.current_count)
