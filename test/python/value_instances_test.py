import os
import sys
import unittest

from kpb_test.value_instances import ValueInstances
from kpb_io import KpbIO

class TestValueInstances(unittest.TestCase):

    def test_value_instances(self):
        value_instances = ValueInstances()
        value_instances.SerializeToString()
        value_instances.set_value1(0xdeadbeef)
        self.assertEqual(b"\xef\xbe\xad\xde", value_instances.SerializeToString())
        self.assertEqual(0xdeadbeef, value_instances.value1)
        self.assertEqual(-1234, value_instances.instance1)
        self.assertEqual(5678, value_instances.instance2)
        value_instances.ParseFromString(b"\xbe\xba\xfe\xca")
        self.assertEqual(0xcafebabe, value_instances.value1)
        self.assertEqual(-1234, value_instances.instance1)
        self.assertEqual(5678, value_instances.instance2)
