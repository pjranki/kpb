import os
import sys
import unittest

from kpb_test.integers_le import IntegersLe
from kpb_test.integers_be import IntegersBe
from kpb_test.integers import Integers
from kpb_io import KpbIO

BYTES_ZERO_30 = b"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
BYTES_ONES_30 = b"\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff"

class TestIntegers(unittest.TestCase):

    def test_integers(self):
        integers = IntegersLe()
        self.assertEqual(integers.integer_s1, 0)
        self.assertEqual(integers.integer_u1, 0)
        self.assertEqual(integers.integer_s2, 0)
        self.assertEqual(integers.integer_u2, 0)
        self.assertEqual(integers.integer_s4, 0)
        self.assertEqual(integers.integer_u4, 0)
        self.assertEqual(integers.integer_s8, 0)
        self.assertEqual(integers.integer_u8, 0)

        integers_zeros = IntegersLe()
        expected_zeros = integers_zeros.SerializeToString()
        self.assertEqual(expected_zeros, BYTES_ZERO_30)

        integers_ones = IntegersLe()
        integers_ones.set_integer_s1(-1)
        integers_ones.set_integer_u1(0xff)
        integers_ones.set_integer_s2(-1)
        integers_ones.set_integer_u2(0xffff)
        integers_ones.set_integer_s4(-1)
        integers_ones.set_integer_u4(0xffffffff)
        integers_ones.set_integer_s8(-1)
        integers_ones.set_integer_u8(0xffffffffffffffff)
        self.assertEqual(integers_ones.integer_s1, -1)
        self.assertEqual(integers_ones.integer_u1, 0xff)
        self.assertEqual(integers_ones.integer_s2, -1)
        self.assertEqual(integers_ones.integer_u2, 0xffff)
        self.assertEqual(integers_ones.integer_s4, -1)
        self.assertEqual(integers_ones.integer_u4, 0xffffffff)
        self.assertEqual(integers_ones.integer_s8, -1)
        self.assertEqual(integers_ones.integer_u8, 0xffffffffffffffff)
        expected_ones = integers_ones.SerializeToString()
        self.assertEqual(expected_ones, BYTES_ONES_30)

        integers.Clear()
        self.assertEqual(integers.integer_s1, 0)
        self.assertEqual(integers.integer_u1, 0)
        self.assertEqual(integers.integer_s2, 0)
        self.assertEqual(integers.integer_u2, 0)
        self.assertEqual(integers.integer_s4, 0)
        self.assertEqual(integers.integer_u4, 0)
        self.assertEqual(integers.integer_s8, 0)
        self.assertEqual(integers.integer_u8, 0)
        integers.ParseFromString(BYTES_ONES_30)
        self.assertEqual(integers.integer_s1, -1)
        self.assertEqual(integers.integer_u1, 0xff)
        self.assertEqual(integers.integer_s2, -1)
        self.assertEqual(integers.integer_u2, 0xffff)
        self.assertEqual(integers.integer_s4, -1)
        self.assertEqual(integers.integer_u4, 0xffffffff)
        self.assertEqual(integers.integer_s8, -1)
        self.assertEqual(integers.integer_u8, 0xffffffffffffffff)
        integers.ParseFromString(BYTES_ZERO_30)
        self.assertEqual(integers.integer_s1, 0)
        self.assertEqual(integers.integer_u1, 0)
        self.assertEqual(integers.integer_s2, 0)
        self.assertEqual(integers.integer_u2, 0)
        self.assertEqual(integers.integer_s4, 0)
        self.assertEqual(integers.integer_u4, 0)
        self.assertEqual(integers.integer_s8, 0)
        self.assertEqual(integers.integer_u8, 0)

        be = IntegersBe()
        le = IntegersLe()
        expected_be = b''
        expected_le = b''
        be.integer_s1 = 0xca
        le.integer_s1 = 0xca
        expected_be += b"\xca"
        expected_le += b"\xca"
        be.integer_u1 = 0xfe
        le.integer_u1 = 0xfe
        expected_be += b"\xfe"
        expected_le += b"\xfe"
        be.integer_s2 = 0xfedb
        le.integer_s2 = 0xfedb
        expected_be += b"\xfe\xdb"
        expected_le += b"\xdb\xfe"
        be.integer_u2 = 0xc234
        le.integer_u2 = 0xc234
        expected_be += b"\xc2\x34"
        expected_le += b"\x34\xc2"
        be.integer_s4 = 0x87654321
        le.integer_s4 = 0x87654321
        expected_be += b"\x87\x65\x43\x21"
        expected_le += b"\x21\x43\x65\x87"
        be.integer_u4 = 0x98765432
        le.integer_u4 = 0x98765432
        expected_be += b"\x98\x76\x54\x32"
        expected_le += b"\x32\x54\x76\x98"
        be.integer_s8 = 0x8765432101abcdef
        le.integer_s8 = 0x8765432101abcdef
        expected_be += b"\x87\x65\x43\x21\x01\xab\xcd\xef"
        expected_le += b"\xef\xcd\xab\x01\x21\x43\x65\x87"
        be.integer_u8 = 0xfedcba9876543210
        le.integer_u8 = 0xfedcba9876543210
        expected_be += b"\xfe\xdc\xba\x98\x76\x54\x32\x10"
        expected_le += b"\x10\x32\x54\x76\x98\xba\xdc\xfe"
        self.assertEqual(len(expected_be), 30)
        self.assertEqual(len(expected_le), 30)

        result_be = be.SerializeToString()
        result_le = le.SerializeToString()
        self.assertEqual(result_be, expected_be)
        self.assertEqual(result_le, expected_le)
        be.Clear()
        le.Clear()
        be.ParseFromString(result_be)
        self.assertEqual(be.integer_s1, KpbIO.get_signed(0xca, 8))
        self.assertEqual(be.integer_u1, 0xfe)
        self.assertEqual(be.integer_s2, KpbIO.get_signed(0xfedb, 16))
        self.assertEqual(be.integer_u2, 0xc234)
        self.assertEqual(be.integer_s4, KpbIO.get_signed(0x87654321, 32))
        self.assertEqual(be.integer_u4, 0x98765432)
        self.assertEqual(be.integer_s8, KpbIO.get_signed(0x8765432101abcdef, 64))
        self.assertEqual(be.integer_u8, 0xfedcba9876543210)
        le.ParseFromString(result_le)
        self.assertEqual(le.integer_s1, KpbIO.get_signed(0xca, 8))
        self.assertEqual(le.integer_u1, 0xfe)
        self.assertEqual(le.integer_s2, KpbIO.get_signed(0xfedb, 16))
        self.assertEqual(le.integer_u2, 0xc234)
        self.assertEqual(le.integer_s4, KpbIO.get_signed(0x87654321, 32))
        self.assertEqual(le.integer_u4, 0x98765432)
        self.assertEqual(le.integer_s8, KpbIO.get_signed(0x8765432101abcdef, 64))
        self.assertEqual(le.integer_u8, 0xfedcba9876543210)

        intergers_mixed = Integers()
        intergers_mixed.integer_s2le = 0x1234
        intergers_mixed.integer_u2le = 0x1234
        intergers_mixed.integer_s4le = 0x12345678
        intergers_mixed.integer_u4le = 0x12345678
        intergers_mixed.integer_s8le = 0x1234567890abcdef
        intergers_mixed.integer_u8le = 0x1234567890abcdef
        intergers_mixed.integer_s2be = 0x1234
        intergers_mixed.integer_u2be = 0x1234
        intergers_mixed.integer_s4be = 0x12345678
        intergers_mixed.integer_u4be = 0x12345678
        intergers_mixed.integer_s8be = 0x1234567890abcdef
        intergers_mixed.integer_u8be = 0x1234567890abcdef
        result = intergers_mixed.SerializeToString()
        self.assertEqual(result, \
            b"\x34\x12" + b"\x34\x12" + \
            b"\x78\x56\x34\x12" + b"\x78\x56\x34\x12" + \
            b"\xef\xcd\xab\x90\x78\x56\x34\x12" + b"\xef\xcd\xab\x90\x78\x56\x34\x12" + \
            b"\x12\x34" + b"\x12\x34" \
            b"\x12\x34\x56\x78" + b"\x12\x34\x56\x78" + \
            b"\x12\x34\x56\x78\x90\xab\xcd\xef" + b"\x12\x34\x56\x78\x90\xab\xcd\xef")
        intergers_mixed.Clear()
        intergers_mixed.ParseFromString( \
            b"\x34\x12" + b"\x34\x12" + \
            b"\x78\x56\x34\x12" + b"\x78\x56\x34\x12" + \
            b"\xef\xcd\xab\x90\x78\x56\x34\x12" + b"\xef\xcd\xab\x90\x78\x56\x34\x12" + \
            b"\x12\x34" + b"\x12\x34" \
            b"\x12\x34\x56\x78" + b"\x12\x34\x56\x78" + \
            b"\x12\x34\x56\x78\x90\xab\xcd\xef" + b"\x12\x34\x56\x78\x90\xab\xcd\xef")
        self.assertEqual(0x1234, intergers_mixed.integer_s2le)
        self.assertEqual(0x1234, intergers_mixed.integer_u2le)
        self.assertEqual(0x12345678, intergers_mixed.integer_s4le)
        self.assertEqual(0x12345678, intergers_mixed.integer_u4le)
        self.assertEqual(0x1234567890abcdef, intergers_mixed.integer_s8le)
        self.assertEqual(0x1234567890abcdef, intergers_mixed.integer_u8le)
        self.assertEqual(0x1234, intergers_mixed.integer_s2be)
        self.assertEqual(0x1234, intergers_mixed.integer_u2be)
        self.assertEqual(0x12345678, intergers_mixed.integer_s4be)
        self.assertEqual(0x12345678, intergers_mixed.integer_u4be)
        self.assertEqual(0x1234567890abcdef, intergers_mixed.integer_s8be)
        self.assertEqual(0x1234567890abcdef, intergers_mixed.integer_u8be)
