class ProcessValueSubType(object):

    def __init__(self, root):
        self.root = root

    def getter(self, value):
        self.root.getter_count = self.root.getter_count + 1
        return value

    def setter(self, value):
        self.root.setter_count = self.root.setter_count + 1
        return value
