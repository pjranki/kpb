import os
import sys
import unittest

from kpb_test.name_deconflict import FcObj, CorrectUseOfSize_, CorrectUseOfLength
from kpb_io import KpbIO

class TestUpdate(unittest.TestCase):

    def test_update(self):
        obj = FcObj()

        # call update
        obj.Clear()
        self.assertEqual(
            b"\x20\x00\x00\x00" + b"\x00\x00\x00\x00" + b"\x00\x00\x00\x00" + \
            b"\x00\x00" + b"" + \
            b"\x00\x00" + \
            b"\x00" + b"" + \
            b"\x01" + b"\x00" + \
            b"\x00\x00" + \
            b"\x00" + b"" + \
            b"\x02" + b"\x00\x00" + \
            b"\x00\x00" + \
            b"\x00" + b"" + \
            b"\x00" + \
            b"\x00" + b"" + \
            b"\x00" + b"" + \
            b"\x00",
            obj.SerializeToString()
        )
        self.assertEqual(32, obj.fm_total_size1)
        self.assertEqual(1, obj.fm_asciiz_size1)
        self.assertEqual(2, obj.fm_widez_size1)

        # change some values - others are (automatically) updated
        self.assertTrue(obj.set_fm_total_size1(0xdeadbeef))
        self.assertTrue(obj.set_fm_flags(0xdeadbeef))
        self.assertTrue(obj.set_fm_unused(0xcafebabe))
        self.assertTrue(obj.set_fm_buffer(b"\xab\x00\xcd"))
        self.assertTrue(obj.add_fm_buffer_list(b"\x12\x34\x56\x78\x90"))
        self.assertTrue(obj.set_fm_ascii("A"))
        self.assertTrue(obj.set_fm_asciiz("BC"))
        self.assertTrue(obj.add_fm_ascii_list("DEF"))
        self.assertTrue(obj.set_fm_wide("a"))
        self.assertTrue(obj.set_fm_widez("bc"))
        self.assertTrue(obj.add_fm_wide_list("def"))

        single = obj.add_fm_single_data_list()
        self.assertTrue(single.set_fm_data(b"\x33\x33\x33"))

        multi = obj.add_fm_multi_data_list()
        self.assertTrue(multi.set_fm_data(b"\x55\x55\x55\x55\x55"))

        multi = obj.mutable_fm_multi_data()
        self.assertTrue(multi.set_fm_data(b"\x44\x44\x44\x44"))
        single = obj.mutable_fm_single_data()
        self.assertTrue(single.set_fm_data(b"\x11"))
        multi = single.mutable_fm_multi_data()
        self.assertTrue(multi.set_fm_data(b"\x22\x22"))

        self.assertEqual(74, obj.fm_total_size1)
        self.assertEqual(0xdeadbeef, obj.fm_flags)
        self.assertEqual(0, obj.fm_unused)
        self.assertEqual(3, obj.fm_buffer_size1)
        self.assertEqual(1, obj.fm_buffer_list_count)
        self.assertEqual(1, obj.fm_ascii_size1)
        self.assertEqual(3, obj.fm_asciiz_size1)
        self.assertEqual(1, obj.fm_ascii_list_count)
        self.assertEqual(2, obj.fm_wide_size1)
        self.assertEqual(6, obj.fm_widez_size1)
        self.assertEqual(1, obj.fm_wide_list_count)

        self.assertEqual(
            b"\x4a\x00\x00\x00" + b"\xef\xbe\xad\xde" + b"\x00\x00\x00\x00" + \
            b"\x03\x00" + b"\xab\x00\xcd" + \
            b"\x01\x00" + b"\x12\x34\x56\x78"\
            b"\x01" + b"A" + \
            b"\x03" + b"BC\x00" + \
            b"\x01\x00" + b"DEF\x00" + \
            b"\x02" + b"a\x00" + \
            b"\x06" + b"b\x00" + b"c\x00" + b"\x00\x00" + \
            b"\x01\x00" + b"d\x00" + b"e\x00" + \
            b"\x01" + b"\x11" + b"\x02" + b"\x22\x22" + \
            b"\x01" + b"\x03" + b"\x33\x33\x33" + b"\x00" + \
            b"\x04" + b"\x44\x44\x44\x44" + \
            b"\x01" + b"\x05" + b"\x55\x55\x55\x55\x55",
            obj.SerializeToString()
        )

        # changes in parent are reflected in children via instances
        obj.set_fm_flags(0xdeadbeef)
        self.assertEqual(0xdeadbeef, obj.fm_single_data.fm_parent_flags)
        self.assertEqual(0xdeadbeef, obj.fm_single_data.fm_multi_data.fm_flags)
        obj.set_fm_flags(0xcafebabe)
        self.assertEqual(0xcafebabe, obj.fm_single_data.fm_parent_flags)
        self.assertEqual(0xcafebabe, obj.fm_single_data.fm_multi_data.fm_flags)

        # clear
        obj.Clear()
        self.assertEqual(
            b"\x20\x00\x00\x00" + b"\x00\x00\x00\x00" + b"\x00\x00\x00\x00" + \
            b"\x00\x00" + b"" + \
            b"\x00\x00" + \
            b"\x00" + b"" + \
            b"\x01" + b"\x00" + \
            b"\x00\x00" + \
            b"\x00" + b"" + \
            b"\x02" + b"\x00\x00" + \
            b"\x00\x00" + \
            b"\x00" + b"" + \
            b"\x00" + \
            b"\x00" + b"" + \
            b"\x00" + b"" + \
            b"\x00",
            obj.SerializeToString()
        )

        # parse from string
        obj.ParseFromString(
            b"\x4a\x00\x00\x00" + b"\xef\xbe\xad\xde" + b"\xa5\xa5\xa5\xa5" + \
            b"\x03\x00" + b"\xab\x00\xcd" + \
            b"\x01\x00" + b"\x12\x34\x56\x78"\
            b"\x01" + b"A" + \
            b"\x03" + b"BC\x00" + \
            b"\x01\x00" + b"DEF\x00" + \
            b"\x02" + b"a\x00" + \
            b"\x06" + b"b\x00" + b"c\x00" + b"\x00\x00" + \
            b"\x01\x00" + b"d\x00" + b"e\x00" + \
            b"\x01" + b"\x11" + b"\x02" + b"\x22\x22" + \
            b"\x01" + b"\x03" + b"\x33\x33\x33" + b"\x00" + \
            b"\x04" + b"\x44\x44\x44\x44" + \
            b"\x01" + b"\x05" + b"\x55\x55\x55\x55\x55",
        )
        self.assertEqual(74, obj.fm_total_size1)
        self.assertEqual(0xdeadbeef, obj.fm_flags)
        self.assertEqual(0xa5a5a5a5, obj.fm_unused)  # this is correct, parse/read should NOT call update
        self.assertEqual(3, obj.fm_buffer_size1)
        self.assertEqual(b"\xab\x00\xcd", obj.fm_buffer)
        self.assertEqual(1, obj.fm_buffer_list_count)
        self.assertEqual(1, len(obj.fm_buffer_list))
        self.assertEqual(b"\x12\x34\x56\x78", obj.fm_buffer_list[0])
        self.assertEqual(1, obj.fm_ascii_size1)
        self.assertEqual("A", obj.fm_ascii)
        self.assertEqual(3, obj.fm_asciiz_size1)
        self.assertEqual("BC", obj.fm_asciiz)
        self.assertEqual(1, obj.fm_ascii_list_count)
        self.assertEqual(1, len(obj.fm_ascii_list))
        self.assertEqual("DEF", obj.fm_ascii_list[0])
        self.assertEqual(2, obj.fm_wide_size1)
        self.assertEqual("a", obj.fm_wide)
        self.assertEqual(6, obj.fm_widez_size1)
        self.assertEqual("bc", obj.fm_widez)
        self.assertEqual(1, obj.fm_wide_list_count)
        self.assertEqual(1, len(obj.fm_wide_list))
        self.assertEqual("de", obj.fm_wide_list[0])
        self.assertEqual(1, obj.fm_single_data.fm_data_size1)
        self.assertEqual(b"\x11", obj.fm_single_data.fm_data)
        self.assertEqual(2, obj.fm_single_data.fm_multi_data.fm_data_size1)
        self.assertEqual(b"\x22\x22", obj.fm_single_data.fm_multi_data.fm_data)
        self.assertEqual(1, obj.fm_single_data_list_count)
        self.assertEqual(1, len(obj.fm_single_data_list))
        self.assertEqual(3, obj.fm_single_data_list[0].fm_data_size1)
        self.assertEqual(b"\x33\x33\x33", obj.fm_single_data_list[0].fm_data)
        self.assertEqual(0, obj.fm_single_data_list[0].fm_multi_data.fm_data_size1)
        self.assertEqual(b"", obj.fm_single_data_list[0].fm_multi_data.fm_data)
        self.assertEqual(4, obj.fm_multi_data.fm_data_size1)
        self.assertEqual(b"\x44\x44\x44\x44", obj.fm_multi_data.fm_data)
        self.assertEqual(1, obj.fm_multi_data_list_count)
        self.assertEqual(1, len(obj.fm_multi_data_list))
        self.assertEqual(5, obj.fm_multi_data_list[0].fm_data_size1)
        self.assertEqual(b"\x55\x55\x55\x55\x55", obj.fm_multi_data_list[0].fm_data)

        # enum
        self.assertEqual(0x1234, FcObj.FeFoobar.FE_FOOBAR_FVE_V1)

        # instances
        self.assertEqual(1, obj.fm_0)
        self.assertEqual(True, obj.fm_1)
        self.assertEqual(0, obj.fm_2.fm_data_size1)
        self.assertEqual(0, obj.fm_3)
        self.assertEqual(0, obj.fm_4)

        # correct use of size
        correct_size = CorrectUseOfSize_()
        correct_size.set_size_(0xdeadbeef)
        self.assertEqual(0xdeadbeef, correct_size.size_)
        self.assertEqual(0xdeadbeef, correct_size.value1)
        self.assertEqual(0xdeadbeef, correct_size.value2)
        correct_size.set_size_(0xcafebabe)
        self.assertEqual(0xcafebabe, correct_size.size_)
        self.assertEqual(0xcafebabe, correct_size.value1)
        self.assertEqual(0xcafebabe, correct_size.value2)

        # correct use of length
        correct_length = CorrectUseOfLength()
        correct_length.set_length_(0xdeadbeef)
        self.assertEqual(0xdeadbeef, correct_length.length_)
        # update/set will use length as the size of the object (not the member's value)
        self.assertEqual(8, correct_length.value1)
        self.assertEqual(0xdeadbeef, correct_length.value2)
        correct_length.set_length_(0xcafebabe)
        self.assertEqual(0xcafebabe, correct_length.length_)
        # update/set will use length as the size of the object (not the member's value)
        self.assertEqual(8, correct_length.value1)
        self.assertEqual(0xcafebabe, correct_length.value2)
