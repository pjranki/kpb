class ProcessValueBool(object):

    def __init__(self, invert_logic):
        self.invert_logic = invert_logic

    def getter(self, value):
        return not value if self.invert_logic else value

    def setter(self, value):
        return not value if self.invert_logic else value
