import os
import sys
import unittest

from kpb_test.update import ObjUpdate
from kpb_io import KpbIO

class TestUpdate(unittest.TestCase):

    def test_update(self):
        update = ObjUpdate()

        # constructor should not call update (need to use '-default' on values)
        self.assertEqual(
            b"\x20\x00\x00\x00" + b"\x00\x00\x00\x00" + b"\x00\x00\x00\x00" + \
            b"\x00\x00" + b"" + \
            b"\x00\x00" + \
            b"\x00" + b"" + \
            b"\x01" + b"\x00" + \
            b"\x00\x00" + \
            b"\x00" + b"" + \
            b"\x02" + b"\x00\x00" + \
            b"\x00\x00" + \
            b"\x00" + b"" + \
            b"\x00" + \
            b"\x00" + b"" + \
            b"\x00" + b"" + \
            b"\x00",
            update.SerializeToString()
        )
        self.assertEqual(32, update.total_size1)
        self.assertEqual(1, update.asciiz_size1)
        self.assertEqual(2, update.widez_size1)

        # clear calls update
        update.Clear()
        self.assertEqual(
            b"\x20\x00\x00\x00" + b"\x00\x00\x00\x00" + b"\x00\x00\x00\x00" + \
            b"\x00\x00" + b"" + \
            b"\x00\x00" + \
            b"\x00" + b"" + \
            b"\x01" + b"\x00" + \
            b"\x00\x00" + \
            b"\x00" + b"" + \
            b"\x02" + b"\x00\x00" + \
            b"\x00\x00" + \
            b"\x00" + b"" + \
            b"\x00" + \
            b"\x00" + b"" + \
            b"\x00" + b"" + \
            b"\x00",
            update.SerializeToString()
        )
        self.assertEqual(32, update.total_size1)
        self.assertEqual(1, update.asciiz_size1)
        self.assertEqual(2, update.widez_size1)

        # change some values - others are (automatically) updated
        self.assertTrue(update.set_total_size1(0xdeadbeef))
        self.assertTrue(update.set_flags(0xdeadbeef))
        self.assertTrue(update.set_unused(0xcafebabe))
        self.assertTrue(update.set_buffer(b"\xab\x00\xcd"))
        self.assertTrue(update.add_buffer_list(b"\x12\x34\x56\x78\x90"))
        self.assertTrue(update.set_ascii("A"))
        self.assertTrue(update.set_asciiz("BC"))
        self.assertTrue(update.add_ascii_list("DEF"))
        self.assertTrue(update.set_wide("a"))
        self.assertTrue(update.set_widez("bc"))
        self.assertTrue(update.add_wide_list("def"))

        single = update.add_single_data_list()
        self.assertTrue(single.set_data(b"\x33\x33\x33"))

        multi = update.add_multi_data_list()
        self.assertTrue(multi.set_data(b"\x55\x55\x55\x55\x55"))

        multi = update.mutable_multi_data()
        self.assertTrue(multi.set_data(b"\x44\x44\x44\x44"))
        single = update.mutable_single_data()
        self.assertTrue(single.set_data(b"\x11"))
        multi = single.mutable_multi_data()
        self.assertTrue(multi.set_data(b"\x22\x22"))

        self.assertEqual(74, update.total_size1)
        self.assertEqual(0xdeadbeef, update.flags)
        self.assertEqual(0, update.unused)
        self.assertEqual(3, update.buffer_size1)
        self.assertEqual(1, update.buffer_list_count)
        self.assertEqual(1, update.ascii_size1)
        self.assertEqual(3, update.asciiz_size1)
        self.assertEqual(1, update.ascii_list_count)
        self.assertEqual(2, update.wide_size1)
        self.assertEqual(6, update.widez_size1)
        self.assertEqual(1, update.wide_list_count)

        self.assertEqual(
            b"\x4a\x00\x00\x00" + b"\xef\xbe\xad\xde" + b"\x00\x00\x00\x00" + \
            b"\x03\x00" + b"\xab\x00\xcd" + \
            b"\x01\x00" + b"\x12\x34\x56\x78"\
            b"\x01" + b"A" + \
            b"\x03" + b"BC\x00" + \
            b"\x01\x00" + b"DEF\x00" + \
            b"\x02" + b"a\x00" + \
            b"\x06" + b"b\x00" + b"c\x00" + b"\x00\x00" + \
            b"\x01\x00" + b"d\x00" + b"e\x00" + \
            b"\x01" + b"\x11" + b"\x02" + b"\x22\x22" + \
            b"\x01" + b"\x03" + b"\x33\x33\x33" + b"\x00" + \
            b"\x04" + b"\x44\x44\x44\x44" + \
            b"\x01" + b"\x05" + b"\x55\x55\x55\x55\x55",
            update.SerializeToString()
        )

        # changes in parent are reflected in children via instances
        update.set_flags(0xdeadbeef)
        self.assertEqual(0xdeadbeef, update.single_data.parent_flags)
        self.assertEqual(0xdeadbeef, update.single_data.multi_data.flags)
        update.set_flags(0xcafebabe)
        self.assertEqual(0xcafebabe, update.single_data.parent_flags)
        self.assertEqual(0xcafebabe, update.single_data.multi_data.flags)

        # clear
        update.Clear()
        self.assertEqual(
            b"\x20\x00\x00\x00" + b"\x00\x00\x00\x00" + b"\x00\x00\x00\x00" + \
            b"\x00\x00" + b"" + \
            b"\x00\x00" + \
            b"\x00" + b"" + \
            b"\x01" + b"\x00" + \
            b"\x00\x00" + \
            b"\x00" + b"" + \
            b"\x02" + b"\x00\x00" + \
            b"\x00\x00" + \
            b"\x00" + b"" + \
            b"\x00" + \
            b"\x00" + b"" + \
            b"\x00" + b"" + \
            b"\x00",
            update.SerializeToString()
        )

        # parse from string
        update.ParseFromString(
            b"\x4a\x00\x00\x00" + b"\xef\xbe\xad\xde" + b"\xa5\xa5\xa5\xa5" + \
            b"\x03\x00" + b"\xab\x00\xcd" + \
            b"\x01\x00" + b"\x12\x34\x56\x78"\
            b"\x01" + b"A" + \
            b"\x03" + b"BC\x00" + \
            b"\x01\x00" + b"DEF\x00" + \
            b"\x02" + b"a\x00" + \
            b"\x06" + b"b\x00" + b"c\x00" + b"\x00\x00" + \
            b"\x01\x00" + b"d\x00" + b"e\x00" + \
            b"\x01" + b"\x11" + b"\x02" + b"\x22\x22" + \
            b"\x01" + b"\x03" + b"\x33\x33\x33" + b"\x00" + \
            b"\x04" + b"\x44\x44\x44\x44" + \
            b"\x01" + b"\x05" + b"\x55\x55\x55\x55\x55",
        )
        self.assertEqual(74, update.total_size1)
        self.assertEqual(0xdeadbeef, update.flags)
        self.assertEqual(0xa5a5a5a5, update.unused)  # this is correct, parse/read should NOT call update
        self.assertEqual(3, update.buffer_size1)
        self.assertEqual(b"\xab\x00\xcd", update.buffer)
        self.assertEqual(1, update.buffer_list_count)
        self.assertEqual(1, len(update.buffer_list))
        self.assertEqual(b"\x12\x34\x56\x78", update.buffer_list[0])
        self.assertEqual(1, update.ascii_size1)
        self.assertEqual("A", update.ascii)
        self.assertEqual(3, update.asciiz_size1)
        self.assertEqual("BC", update.asciiz)
        self.assertEqual(1, update.ascii_list_count)
        self.assertEqual(1, len(update.ascii_list))
        self.assertEqual("DEF", update.ascii_list[0])
        self.assertEqual(2, update.wide_size1)
        self.assertEqual("a", update.wide)
        self.assertEqual(6, update.widez_size1)
        self.assertEqual("bc", update.widez)
        self.assertEqual(1, update.wide_list_count)
        self.assertEqual(1, len(update.wide_list))
        self.assertEqual("de", update.wide_list[0])
        self.assertEqual(1, update.single_data.data_size1)
        self.assertEqual(b"\x11", update.single_data.data)
        self.assertEqual(2, update.single_data.multi_data.data_size1)
        self.assertEqual(b"\x22\x22", update.single_data.multi_data.data)
        self.assertEqual(1, update.single_data_list_count)
        self.assertEqual(1, len(update.single_data_list))
        self.assertEqual(3, update.single_data_list[0].data_size1)
        self.assertEqual(b"\x33\x33\x33", update.single_data_list[0].data)
        self.assertEqual(0, update.single_data_list[0].multi_data.data_size1)
        self.assertEqual(b"", update.single_data_list[0].multi_data.data)
        self.assertEqual(4, update.multi_data.data_size1)
        self.assertEqual(b"\x44\x44\x44\x44", update.multi_data.data)
        self.assertEqual(1, update.multi_data_list_count)
        self.assertEqual(1, len(update.multi_data_list))
        self.assertEqual(5, update.multi_data_list[0].data_size1)
        self.assertEqual(b"\x55\x55\x55\x55\x55", update.multi_data_list[0].data)
