import os
import sys
import unittest

from kpb_test.process import ProcessBodySize_, ProcessBodySizeEos
from kpb_io import KpbIO

class TestProcess(unittest.TestCase):
    HELLOWORLD_ALIGN32_BUFFER = b"\x20\x00\x00\x00\x20\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xe2\xcf\xc6\xc6\xc5\xfd\xc5\xd8\xc6\xce\x8b\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
    HELLOWORLD_ALIGN8_BUFFER = b"\x08\x00\x00\x00\x00\x00\x00\x00\xe2\xcf\xc6\xc6\xc5\xfd\xc5\xd8\xc6\xce\x8b\xaa\xaa\xaa\xaa\xaa"

    def test_process(self):
        process = ProcessBodySize_()
        config = process.mutable_config()
        config.alignment = 32
        body = process.mutable_body()
        body.message = "HelloWorld!"
        self.assertEqual(32, process.body_size_)
        result = process.SerializeToString()
        self.assertEqual(self.HELLOWORLD_ALIGN32_BUFFER, result)

        process.Clear()
        self.assertEqual(0, process.config.alignment)
        self.assertEqual("", process.body.message)
        self.assertEqual(1, process.body_size_)
        self.assertTrue(process.ParseFromString(self.HELLOWORLD_ALIGN32_BUFFER))
        self.assertEqual(32, process.config.alignment)
        self.assertEqual("HelloWorld!", process.body.message)
        self.assertEqual(32, process.body_size_)

        process_eos = ProcessBodySizeEos()
        config = process_eos.mutable_config()
        config.alignment = 8
        body = process_eos.mutable_body()
        body.message = "HelloWorld!"
        result = process_eos.SerializeToString()
        self.assertEqual(self.HELLOWORLD_ALIGN8_BUFFER, result)

        process_eos.Clear()
        self.assertEqual(0, process_eos.config.alignment)
        self.assertEqual("", process_eos.body.message)
        self.assertTrue(process_eos.ParseFromString(self.HELLOWORLD_ALIGN8_BUFFER))
        self.assertEqual(8, process_eos.config.alignment)
        self.assertEqual("HelloWorld!", process_eos.body.message)
