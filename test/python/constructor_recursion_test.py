import os
import sys
import unittest

from kpb_test.constructor_recursion import ConstructorRecursion
from kpb_io import KpbIO

class TestConstructorRecursion(unittest.TestCase):

    def test_constructor_recursion(self):
        obj = ConstructorRecursion()
        obj.Clear()
