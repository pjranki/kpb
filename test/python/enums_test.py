import os
import sys
import unittest

from kpb_test.enums import Enums, SubEnums
from kpb_io import KpbIO

class TestEnums(unittest.TestCase):

    def test_enums(self):
        enums = Enums()
        enums.SerializeToString()
        self.assertEqual(1, Enums.TestEnum.TEST_ENUM_FOOBAR)
        self.assertEqual(2, Enums.TestEnum.TEST_ENUM_CAFEBABE)
        sub_enums = SubEnums()
        sub_enums.SerializeToString()
        self.assertEqual(0xffff, SubEnums.TestEnum.TEST_ENUM_FOOBAR)
        self.assertEqual(0x75a5, SubEnums.TestEnum.TEST_ENUM_CAFEBABE)
