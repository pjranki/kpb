import struct

class ProcessValueBuf5(object):

    def __init__(self, memset_value):
        self.memset_value = memset_value

    def getter(self, value):
        return struct.pack('<B', self.memset_value) * len(value)

    def setter(self, value):
        return struct.pack('<B', self.memset_value) * len(value)
