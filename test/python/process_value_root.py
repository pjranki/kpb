class ProcessValueRoot(object):

    def __init__(self, root, arg):
        self.root = root
        self.arg = arg

    def getter(self, value):
        if self.arg == 0xaa:
            self.root.getter_count = self.root.getter_count + 1
        return value

    def setter(self, value):
        if self.arg == 0xaa:
            self.root.setter_count = self.root.setter_count + 1
        return value
