import os
import sys
import unittest

from kpb_test.parse_or_fill import ParseOrFill, ParseOrFillEof
from kpb_io import KpbIO, KpbIOError

class TestParseOrFill(unittest.TestCase):

    def test_parse_or_fill(self):
        obj = ParseOrFill()

        # test a standard read
        obj.ParseFromString(b"\x12\x34\x56\x78\x90\xab\xcd\xef")
        self.assertEqual(0x12345678, obj.value1)
        self.assertEqual(0x90abcdef, obj.other.value2)

        # test that reads can't work out of bounds
        obj.Clear()
        try:
            obj.ParseFromString(b"\x12\x34\x56\x78")
            raise AssertionError("This should cause 'not enough memory' exception")
        except KpbIOError:
            pass

        # test a complete fill read with 0s
        obj.ParseFromStringOrFill(b"\x12\x34\x56\x78\x90\xab\xcd\xef", 0)
        self.assertEqual(0x12345678, obj.value1)
        self.assertEqual(0x90abcdef, obj.other.value2)

        # test a complete fill read with 1s
        obj.ParseFromStringOrFill(b"\x12\x34\x56\x78\x90\xab\xcd\xef", 1)
        self.assertEqual(0x12345678, obj.value1)
        self.assertEqual(0x90abcdef, obj.other.value2)

        # test a fill read with 0s
        obj.ParseFromStringOrFill(b"\x12\x34\x56", 0)
        self.assertEqual(0x12345600, obj.value1)
        self.assertEqual(0x00000000, obj.other.value2)

        # test a fill read with 1s
        obj.ParseFromStringOrFill(b"\x12\x34\x56", 1)
        self.assertEqual(0x123456ff, obj.value1)
        self.assertEqual(0xffffffff, obj.other.value2)

        # test a sub type fill read with 0s
        obj.ParseFromStringOrFill(b"\x12\x34\x56\x78\x89", 0)
        self.assertEqual(0x12345678, obj.value1)
        self.assertEqual(0x89000000, obj.other.value2)

        # test a sub type fill read with 1s
        obj.ParseFromStringOrFill(b"\x12\x34\x56\x78\x89", 1)
        self.assertEqual(0x12345678, obj.value1)
        self.assertEqual(0x89ffffff, obj.other.value2)

        # make sure eof is hit (even if filling)
        obj2 = ParseOrFillEof()
        self.assertEqual(0, obj2.prefix)
        self.assertEqual(0, len(obj2.value))
        obj2.ParseFromStringOrFill(b"\xa5\x5f\xf1", 0)
        self.assertEqual(0xa, obj2.prefix)
        self.assertEqual(3, len(obj2.value))
        self.assertEqual(0x55, obj2.value[0])
        self.assertEqual(0xff, obj2.value[1])
        self.assertEqual(0x10, obj2.value[2])
