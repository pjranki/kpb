import os
import sys
import unittest

from kpb_test.self_root_parent import Root_, Child, GrandChild
from kpb_io import KpbIO

class TestEnums(unittest.TestCase):

    def test_enums(self):
        root_obj = Root_()
        child_obj = root_obj.mutable_child()
        grand_child_obj = child_obj.mutable_child()

        root_obj.value = 0xdeadbeef
        child_obj.value = 0xcafebabe
        grand_child_obj.value = 0xf00ba123

        self.assertEqual(0xdeadbeef, root_obj.value)
        self.assertEqual(0xcafebabe, child_obj.value)
        self.assertEqual(0xf00ba123, grand_child_obj.value)

        self.assertEqual(0xdeadbeef, root_obj.root_value)
        self.assertEqual(0xdeadbeef, root_obj.self_value)

        self.assertEqual(0xdeadbeef, child_obj.root_value)
        self.assertEqual(0xdeadbeef, child_obj.parent_value)
        self.assertEqual(0xcafebabe, child_obj.self_value)

        self.assertEqual(0xdeadbeef, grand_child_obj.root_value)
        self.assertEqual(0xcafebabe, grand_child_obj.parent_value)
        self.assertEqual(0xf00ba123, grand_child_obj.self_value)

        root_obj.Clear()

        child_obj = root_obj.add_child_list()
        grand_child_obj = child_obj.add_grand_child_list()

        root_obj.value = 0xdeadbeef
        child_obj.value = 0xcafebabe
        grand_child_obj.value = 0xf00ba123

        self.assertEqual(0xdeadbeef, root_obj.value)
        self.assertEqual(0xcafebabe, child_obj.value)
        self.assertEqual(0xf00ba123, grand_child_obj.value)

        self.assertEqual(0xdeadbeef, root_obj.root_value)
        self.assertEqual(0xdeadbeef, root_obj.self_value)

        self.assertEqual(0xdeadbeef, child_obj.root_value)
        self.assertEqual(0xdeadbeef, child_obj.parent_value)
        self.assertEqual(0xcafebabe, child_obj.self_value)

        self.assertEqual(0xdeadbeef, grand_child_obj.root_value)
        self.assertEqual(0xcafebabe, grand_child_obj.parent_value)
        self.assertEqual(0xf00ba123, grand_child_obj.self_value)

        child_obj = root_obj.mutable_child_list(0)
        grand_child_obj = child_obj.mutable_grand_child_list(0)

        root_obj.value = 0xdeadbeef
        child_obj.value = 0xcafebabe
        grand_child_obj.value = 0xf00ba123

        self.assertEqual(0xdeadbeef, root_obj.value)
        self.assertEqual(0xcafebabe, child_obj.value)
        self.assertEqual(0xf00ba123, grand_child_obj.value)

        self.assertEqual(0xdeadbeef, root_obj.root_value)
        self.assertEqual(0xdeadbeef, root_obj.self_value)

        self.assertEqual(0xdeadbeef, child_obj.root_value)
        self.assertEqual(0xdeadbeef, child_obj.parent_value)
        self.assertEqual(0xcafebabe, child_obj.self_value)

        self.assertEqual(0xdeadbeef, grand_child_obj.root_value)
        self.assertEqual(0xcafebabe, grand_child_obj.parent_value)
        self.assertEqual(0xf00ba123, grand_child_obj.self_value)

        orphan = Child()

        self.assertEqual(0, orphan.root_value)
        self.assertEqual(0, orphan.self_value)
        self.assertEqual(0, orphan.parent_value)

        orphan.Clear()

        self.assertEqual(0, orphan.root_value)
        self.assertEqual(0, orphan.self_value)
        self.assertEqual(0, orphan.parent_value)
