class ProcessValueStrz(object):

    def __init__(self, memset_value):
        self.memset_value = memset_value

    def getter(self, value):
        return chr(self.memset_value) * len(value)

    def setter(self, value):
        return chr(self.memset_value) * len(value)
