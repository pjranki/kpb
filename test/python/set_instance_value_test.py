import os
import sys
import unittest

from kpb_test.set_instance_value import _SetInstanceValue
from kpb_io import KpbIO

class TestSetInstanceValue(unittest.TestCase):

    def test_set_instance_value(self):
        obj = _SetInstanceValue()

        # bool
        obj.input_bool = True
        self.assertEqual(True, obj.storage_bool)
        obj.input_bool = False
        self.assertEqual(False, obj.storage_bool)

        # bitfield
        obj.input_b4 = 0xa
        self.assertEqual(0xa, obj.storage_b4)
        obj.input_b4 = 0x5
        self.assertEqual(0x5, obj.storage_b4)

        # unsigned integer
        obj.input_u4 = 123
        self.assertEqual(123, obj.storage_u4)
        obj.input_u4 = 456
        self.assertEqual(456, obj.storage_u4)

        # signed integer
        obj.input_s4 = -12
        self.assertEqual(-12, obj.storage_s4)
        obj.input_s4 = 45
        self.assertEqual(45, obj.storage_s4)

        # signed integer
        obj.input_s4 = -12
        self.assertEqual(-12, obj.storage_s4)
        obj.input_s4 = 45
        self.assertEqual(45, obj.storage_s4)

        # float
        obj.input_f4 = 1.1
        self.assertAlmostEqual(1.1, obj.storage_f4)
        obj.input_f4 = -2.3
        self.assertAlmostEqual(-2.3, obj.storage_f4)

        # calculate and set _value
        obj.calc_value_u4 = 2
        self.assertEqual(9, obj.storage_u4)

        # check nested sets
        obj.nested_1 = 1
        self.assertEqual(1 * 2 * 3 * 4, obj.nested_4)
        self.assertEqual(1 * 2 * 3 * 4 * 5, obj.nested_3)
