import os
import sys
import unittest

from kpb_test.bitfield import Bitfield
from kpb_io import KpbIO

BYTES_ZERO_16 = b"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
BYTES_ONES_16 = b"\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xfe"

class TestBitfield(unittest.TestCase):

    def test_bitfield_single(self):
        bitfield = Bitfield()
        self.assertEqual(BYTES_ZERO_16, bitfield.SerializeToString())
        bitfield.set_bitfield_b1(0x1)
        bitfield.set_bitfield_b2(0x3)
        bitfield.set_bitfield_b4(0xf)
        bitfield.set_bitfield_b8(0xff)
        bitfield.set_bitfield_b16(0xffff)
        bitfield.set_bitfield_b32(0xffffffff)
        bitfield.set_bitfield_b64(0xffffffffffffffff)
        self.assertEqual(BYTES_ONES_16, bitfield.SerializeToString())
        bitfield.Clear()
        self.assertEqual(BYTES_ZERO_16, bitfield.SerializeToString())
        bitfield.set_bitfield_b1(0x1)
        bitfield.set_bitfield_b2(0x2)
        bitfield.set_bitfield_b4(0x5)
        bitfield.set_bitfield_b8(0xaa)
        bitfield.set_bitfield_b16(0x5555)
        bitfield.set_bitfield_b32(0xaaaaaaaa)
        bitfield.set_bitfield_b64(0x5555555555555555)
        self.assertEqual(b"\xcb\x54\xaa\xab\x55\x55\x55\x54\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa", bitfield.SerializeToString())
        bitfield.set_bitfield_b1(0x0)
        bitfield.set_bitfield_b2(0x1)
        bitfield.set_bitfield_b4(0xa)
        bitfield.set_bitfield_b8(0x55)
        bitfield.set_bitfield_b16(0xaaaa)
        bitfield.set_bitfield_b32(0x55555555)
        bitfield.set_bitfield_b64(0xaaaaaaaaaaaaaaaa)
        self.assertEqual(b"\x34\xab\x55\x54\xaa\xaa\xaa\xab\x55\x55\x55\x55\x55\x55\x55\x54", bitfield.SerializeToString())

        bitfield.Clear()
        self.assertEqual(bitfield.bitfield_b1, 0)
        self.assertEqual(bitfield.bitfield_b2, 0)
        self.assertEqual(bitfield.bitfield_b4, 0)
        self.assertEqual(bitfield.bitfield_b8, 0)
        self.assertEqual(bitfield.bitfield_b16, 0)
        self.assertEqual(bitfield.bitfield_b32, 0)
        self.assertEqual(bitfield.bitfield_b64, 0)
        bitfield.ParseFromString(b"\xcb\x54\xaa\xab\x55\x55\x55\x54\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa")
        self.assertEqual(bitfield.bitfield_b1, 0x1)
        self.assertEqual(bitfield.bitfield_b2, 0x2)
        self.assertEqual(bitfield.bitfield_b4, 0x5)
        self.assertEqual(bitfield.bitfield_b8, 0xaa)
        self.assertEqual(bitfield.bitfield_b16, 0x5555)
        self.assertEqual(bitfield.bitfield_b32, 0xaaaaaaaa)
        self.assertEqual(bitfield.bitfield_b64, 0x5555555555555555)
        bitfield.ParseFromString(b"\x34\xab\x55\x54\xaa\xaa\xaa\xab\x55\x55\x55\x55\x55\x55\x55\x55")
        self.assertEqual(bitfield.bitfield_b1, 0x0)
        self.assertEqual(bitfield.bitfield_b2, 0x1)
        self.assertEqual(bitfield.bitfield_b4, 0xa)
        self.assertEqual(bitfield.bitfield_b8, 0x55)
        self.assertEqual(bitfield.bitfield_b16, 0xaaaa)
        self.assertEqual(bitfield.bitfield_b32, 0x55555555)
        self.assertEqual(bitfield.bitfield_b64, 0xaaaaaaaaaaaaaaaa)
