class XorEncoding(object):

    def __init__(self, xor_key, config):
        self.xor_key = xor_key
        self.config = config

    def encode_size(self, _io):
        self.encode(_io)
        return _io.size

    def encode(self, _io):
        alignment = self.config.alignment
        if alignment == 0:
            # no divide by zero please
            alignment = 1

        # get a copy of the buffer in the stream
        buffer = _io.get()

        # create a buffer that will be big enough to hold the encoded output
        buffer_size = len(buffer)
        if ( buffer_size % alignment ) != 0:
            buffer_size += ( alignment - ( buffer_size % alignment ) )
        buffer += b'\x00' * (buffer_size - len(buffer))

        # xor encode
        buffer = bytes([v ^ self.xor_key for v in buffer])

        # set the encode buffer
        _io.set(buffer)

        # done
        return True

    def decode(self, _io):
        # check that buffer is correctly aligned
        alignment = self.config.alignment
        if alignment == 0:
            # no divide by zero please
            alignment = 1

        # get a copy of the input buffer
        buffer = _io.get()

        # check buffer is correctly aligned
        if ( len(buffer) % alignment) != 0:
            return False

        # xor decode
        buffer = bytes([v ^ self.xor_key for v in buffer])

        # set the decoded buffer
        _io.set(buffer)

        # done
        return True
