import os
import sys
import unittest

from kpb_test.contents import Contents
from kpb_io import KpbIO

class TestContents(unittest.TestCase):

    def test_contents(self):
        contents = Contents()

        self.assertEqual(b"\xCA\xFE\xBA\xBE", contents.magic)
        self.assertEqual(b"\x4A\x46\x49\x46", contents.magic1)
        self.assertEqual(b"\xCA\xFE\xBA\xBE", contents.magic2)
        self.assertEqual(b"\x43\x41\x46\x45\x00\x42\x41\x42\x45", contents.magic3)
        self.assertEqual(b"\x66\x6F\x6F\x00\x41\x0A\x2A", contents.magic4)
        self.assertEqual(b"\x01\x55\x41\x2C\x33\x03", contents.magic5)

        contents.magic = b""
        contents.magic1 = b"\x11"
        contents.magic2 = b"\x11\x22"
        contents.magic3 = b"\x11\x22\x33"
        contents.magic4 = b"\x11\x22\x33\x44"
        contents.magic5 = b"\x11\x22\x33\x44\x55"

        self.assertEqual(b"", contents.magic)
        self.assertEqual(b"\x11", contents.magic1)
        self.assertEqual(b"\x11\x22", contents.magic2)
        self.assertEqual(b"\x11\x22\x33", contents.magic3)
        self.assertEqual(b"\x11\x22\x33\x44", contents.magic4)
        self.assertEqual(b"\x11\x22\x33\x44\x55", contents.magic5)

        result = contents.SerializeToString()
        contents.ParseFromString(result)

        self.assertEqual(b"\x00\x00\x00\x00", contents.magic)
        self.assertEqual(b"\x11\x00\x00\x00", contents.magic1)
        self.assertEqual(b"\x11\x22\x00\x00", contents.magic2)
        self.assertEqual(b"\x11\x22\x33\x00\x00\x00\x00\x00\x00", contents.magic3)
        self.assertEqual(b"\x11\x22\x33\x44\x00\x00\x00", contents.magic4)
        self.assertEqual(b"\x11\x22\x33\x44\x55\x00", contents.magic5)

        contents.Clear()

        self.assertEqual(b"\xCA\xFE\xBA\xBE", contents.magic)
        self.assertEqual(b"\x4A\x46\x49\x46", contents.magic1)
        self.assertEqual(b"\xCA\xFE\xBA\xBE", contents.magic2)
        self.assertEqual(b"\x43\x41\x46\x45\x00\x42\x41\x42\x45", contents.magic3)
        self.assertEqual(b"\x66\x6F\x6F\x00\x41\x0A\x2A", contents.magic4)
        self.assertEqual(b"\x01\x55\x41\x2C\x33\x03", contents.magic5)
