import os
import sys
import unittest

from kpb_test.bitalign import BitalignDefault, Bitalign1, Bitalign8
from kpb_io import KpbIO

class TestBitAlign(unittest.TestCase):

    def test_bitalign(self):
        # default bit alignment
        obj = BitalignDefault()

        # serialize obj with default bit alignment
        obj.v1 = 1
        obj.mutable_v2().v = 0x7
        obj.mutable_v3().v = 0x7
        obj.v4 = 0xfff
        result = obj.SerializeToString()
        self.assertEqual(result, b'\x80\xe0\xe0\xff\xf0')

        # parse obj with default bit alignment
        obj.Clear()
        obj.ParseFromString(b'\x80\xe0\xe0\xff\xf0')
        self.assertEqual(obj.v1, 1)
        self.assertEqual(obj.v2.v, 0x7)
        self.assertEqual(obj.v3.v, 0x7)
        self.assertEqual(obj.v4, 0xfff)

        # 8 bit alignment
        obj = Bitalign8()

        # serialize obj with 8 bit alignment
        obj.v1 = 1
        obj.mutable_v2().v = 0x7
        obj.mutable_v3().v = 0x7
        obj.v4 = 0xfff
        result = obj.SerializeToString()
        self.assertEqual(result, b'\x80\xe0\xe0\xff\xf0')

        # parse obj with 8 bit alignment
        obj.Clear()
        obj.ParseFromString(b'\x80\xe0\xe0\xff\xf0')
        self.assertEqual(obj.v1, 1)
        self.assertEqual(obj.v2.v, 0x7)
        self.assertEqual(obj.v3.v, 0x7)
        self.assertEqual(obj.v4, 0xfff)

        # 1 bit alignment
        obj = Bitalign1()

        # serialize obj with 1 bit alignment
        obj.v1 = 1
        obj.mutable_v2().v = 0x7
        obj.mutable_v3().v = 0x7
        obj.v4 = 0xfff
        result = obj.SerializeToString()
        self.assertEqual(result, b'\xff\xff\xe0')

        # parse obj with 1 bit alignment
        obj.Clear()
        obj.ParseFromString(b'\xff\xff\xe0')
        self.assertEqual(obj.v1, 1)
        self.assertEqual(obj.v2.v, 0x7)
        self.assertEqual(obj.v3.v, 0x7)
        self.assertEqual(obj.v4, 0xfff)
