import os
import sys
import unittest

from kpb_test.object_size import ObjectSize_
from kpb_io import KpbIO, KpbIOError

class TestObjectSize(unittest.TestCase):

    def test_object_size(self):
        obj = ObjectSize_()

        self.assertEqual(b"\x00\x00\x00\x00\x00\x00\x00\x00", obj.SerializeToString())
        try:
            obj.ParseFromString(b"\x00\x00\x00\x00\x00\x00\x00\x00")
            raise AssertionError("This should cause 'not enough memory' exception")
        except KpbIOError:
            pass
        obj.ParseFromString(b"\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00")

        sub_obj = obj.mutable_object_var()
        self.assertTrue(sub_obj.set_value(0x12345678))
        sub_obj = obj.mutable_object_fix()
        self.assertTrue(sub_obj.set_value(0x09abcdef))

        obj.set_object_size_(0)
        self.assertEqual(b"\x00\x00\x00\x00\xef\xcd\xab\x09", obj.SerializeToString())

        obj.set_object_size_(1)
        self.assertEqual(b"\x01\x00\x00\x00\x78\xef\xcd\xab\x09", obj.SerializeToString())

        obj.set_object_size_(2)
        self.assertEqual(b"\x02\x00\x00\x00\x78\x56\xef\xcd\xab\x09", obj.SerializeToString())

        obj.set_object_size_(3)
        self.assertEqual(b"\x03\x00\x00\x00\x78\x56\x34\xef\xcd\xab\x09", obj.SerializeToString())

        obj.set_object_size_(4)
        self.assertEqual(b"\x04\x00\x00\x00\x78\x56\x34\x12\xef\xcd\xab\x09", obj.SerializeToString())

        obj.set_object_size_(5)
        self.assertEqual(b"\x05\x00\x00\x00\x78\x56\x34\x12\x00\xef\xcd\xab\x09", obj.SerializeToString())

        obj.set_object_size_(6)
        self.assertEqual(b"\x06\x00\x00\x00\x78\x56\x34\x12\x00\x00\xef\xcd\xab\x09", obj.SerializeToString())

        obj.Clear()
        obj.ParseFromString(b"\x04\x00\x00\x00\x78\x56\x34\x12\xef\xcd\xab\x09")
        self.assertEqual(4, obj.object_size_)
        self.assertEqual(0x12345678, obj.object_var.value)
        self.assertEqual(0x09abcdef, obj.object_fix.value)

        obj.Clear()
        obj.ParseFromString(b"\x05\x00\x00\x00\x78\x56\x34\x12\xff\xef\xcd\xab\x09")
        self.assertEqual(5, obj.object_size_)
        self.assertEqual(0x12345678, obj.object_var.value)
        self.assertEqual(0x09abcdef, obj.object_fix.value)

        obj.Clear()
        obj.ParseFromString(b"\x06\x00\x00\x00\x78\x56\x34\x12\xff\xff\xef\xcd\xab\x09")
        self.assertEqual(6, obj.object_size_)
        self.assertEqual(0x12345678, obj.object_var.value)
        self.assertEqual(0x09abcdef, obj.object_fix.value)
