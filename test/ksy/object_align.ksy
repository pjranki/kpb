meta:
  id: object_align
  endian: le

seq:
  - id: value_1
    type: b4
  - id: object_b4
    type: object_b4
  - id: value_2
    type: b4
  - id: value_list
    type: b4
    repeat: expr
    repeat-expr: 4

types:
  object_b4:
    seq:
      - id: value
        type: b4
