meta:
  id: object_single
  endian: le

seq:
  - id: other
    type: sub_object

types:
  sub_object:
    seq:
      - id: value
        type: u4
