meta:
  id: strings_list

seq:
  - id: char_list
    type: str
    size: 2
    encoding: ASCII
    repeat: expr
    repeat-expr: 5
  - id: wchar_list
    type: str
    size: 4
    encoding: UTF_16LE
    repeat: expr
    repeat-expr: 5
