meta:
  id: params
  endian: le

seq:
  - id: data1
    type: param_object1
  - id: data2
    type: param_object2(0xcafe)
  - id: data3
    type: param_object3(42, 0xbeef, data3.size, current_count)
    repeat: expr
    repeat-expr: 2

instances:
  current_count:
    value: data3.size & 0xffffffff

types:
  param_object1:
    seq:
      - id: value1
        type: u4

  param_object2:
    params:
      - id: arg1
        type: u2
    seq:
      - id: value1
        type: u4

  param_object3:
    params:
      - id: arg1
        type: u1
      - id: arg2
        type: u2
      - id: index
        type: u4
      - id: current_count
        type: u4
    seq:
      - id: value1
        type: u4
