meta:
  id: pos_root_parent
  endian: le

seq:
  - id: value
    type: u4
# 4 : child.value_parent
# 8 : child.value_root
# 12: child.grandchild.value_root
# 16: 0 : child.value
# 20: 4 : child.grandchild.value_parent
# 24: 8 : child.grandchild.value
# 28 (size)

instances:
  child:
    type: pos_child
    pos: 16
    io: _io

types:
  pos_child:
    seq:
      - id: value
        type: u4
    instances:
      value_parent:
        type: u4
        pos: 4
        io: _parent._io
      value_root:
        type: u4
        pos: 8
        io: _root._io
      grandchild:
        type: pos_grandchild
        pos: 8
        io: _io

  pos_grandchild:
    seq:
      - id: value
        type: u4
    instances:
      value_parent:
        type: u4
        pos: 4
        io: _parent._io
      value_root:
        type: u4
        pos: 12
        io: _root._io
