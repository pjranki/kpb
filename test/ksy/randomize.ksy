meta:
  id: randomize
  endian: le

params:
  - id: count_min
    type: u1
  - id: count_max
    type: u1
  - id: s1_min
    type: s1
  - id: s1_max
    type: s1
  - id: u1_min
    type: u1
  - id: u1_max
    type: u1
  - id: s2_min
    type: s2
  - id: s2_max
    type: s2
  - id: u2_min
    type: u2
  - id: u2_max
    type: u2
  - id: s4_min
    type: s4
  - id: s4_max
    type: s4
  - id: u4_min
    type: u4
  - id: u4_max
    type: u4
  - id: s8_min
    type: s8
  - id: s8_max
    type: s8
  - id: u8_min
    type: u8
  - id: u8_max
    type: u8
  - id: bool_min
    type: bool
  - id: bool_max
    type: bool
  - id: bool_
    type: bool
    -randomize-content: range
    -randomize-content-range-min: bool_min
    -randomize-content-range-max: bool_max

seq:
  - id: s1
    type: s1
    -randomize-content: range
    -randomize-content-range-min: s1_min
    -randomize-content-range-max: s1_max
  - id: s1_list
    type: s1
    repeat: expr
    repeat-expr: 3
    -randomize-count: range
    -randomize-count-range-min: count_min
    -randomize-count-range-max: count_max
    -randomize-content: range
    -randomize-content-range-min: s1_min
    -randomize-content-range-max: s1_max
  - id: s2
    type: s2
    -randomize-content: range
    -randomize-content-range-min: s2_min
    -randomize-content-range-max: s2_max
  - id: s2_list
    type: s2
    repeat: expr
    repeat-expr: 3
    -randomize-count: range
    -randomize-count-range-min: count_min
    -randomize-count-range-max: count_max
    -randomize-content: range
    -randomize-content-range-min: s2_min
    -randomize-content-range-max: s2_max
  - id: s4
    type: s4
    -randomize-content: range
    -randomize-content-range-min: s4_min
    -randomize-content-range-max: s4_max
  - id: s4_list
    type: s4
    repeat: expr
    repeat-expr: 3
    -randomize-count: range
    -randomize-count-range-min: count_min
    -randomize-count-range-max: count_max
    -randomize-content: range
    -randomize-content-range-min: s4_min
    -randomize-content-range-max: s4_max
  - id: s8
    type: s8
    -randomize-content: range
    -randomize-content-range-min: s8_min
    -randomize-content-range-max: s8_max
  - id: s8_list
    type: s8
    repeat: expr
    repeat-expr: 3
    -randomize-count: range
    -randomize-count-range-min: count_min
    -randomize-count-range-max: count_max
    -randomize-content: range
    -randomize-content-range-min: s8_min
    -randomize-content-range-max: s8_max
  - id: u1
    type: u1
    -randomize-content: range
    -randomize-content-range-min: u1_min
    -randomize-content-range-max: u1_max
  - id: u1_list
    type: u1
    repeat: expr
    repeat-expr: 3
    -randomize-count: range
    -randomize-count-range-min: count_min
    -randomize-count-range-max: count_max
    -randomize-content: range
    -randomize-content-range-min: u1_min
    -randomize-content-range-max: u1_max
  - id: u2
    type: u2
    -randomize-content: range
    -randomize-content-range-min: u2_min
    -randomize-content-range-max: u2_max
  - id: u2_list
    type: u2
    repeat: expr
    repeat-expr: 3
    -randomize-count: range
    -randomize-count-range-min: count_min
    -randomize-count-range-max: count_max
    -randomize-content: range
    -randomize-content-range-min: u2_min
    -randomize-content-range-max: u2_max
  - id: u4
    type: u4
    -randomize-content: range
    -randomize-content-range-min: u4_min
    -randomize-content-range-max: u4_max
  - id: u4_list
    type: u4
    repeat: expr
    repeat-expr: 3
    -randomize-count: range
    -randomize-count-range-min: count_min
    -randomize-count-range-max: count_max
    -randomize-content: range
    -randomize-content-range-min: u4_min
    -randomize-content-range-max: u4_max
  - id: u8
    type: u8
    -randomize-content: range
    -randomize-content-range-min: u8_min
    -randomize-content-range-max: u8_max
  - id: u8_list
    type: u8
    repeat: expr
    repeat-expr: 3
    -randomize-count: range
    -randomize-count-range-min: count_min
    -randomize-count-range-max: count_max
    -randomize-content: range
    -randomize-content-range-min: u8_min
    -randomize-content-range-max: u8_max
  - id: f4
    type: f4
    -randomize-content: range
    -randomize-content-range-min: -11.1
    -randomize-content-range-max: 22.3
  - id: f4_list
    type: f4
    repeat: expr
    repeat-expr: 3
    -randomize-count: range
    -randomize-count-range-min: count_min
    -randomize-count-range-max: count_max
    -randomize-content: range
    -randomize-content-range-min: -11.1
    -randomize-content-range-max: 22.3
  - id: f8
    type: f8
    -randomize-content: range
    -randomize-content-range-min: -11.1
    -randomize-content-range-max: 22.3
  - id: f8_list
    type: f8
    repeat: expr
    repeat-expr: 3
    -randomize-count: range
    -randomize-count-range-min: count_min
    -randomize-count-range-max: count_max
    -randomize-content: range
    -randomize-content-range-min: -11.1
    -randomize-content-range-max: 22.3
  - id: data
    size: 4
    -randomize-size: range
    -randomize-size-range-min: 0
    -randomize-size-range-max: 4
    -randomize-content: range
    -randomize-content-range-min: 0x00
    -randomize-content-range-max: 0xff
  - id: data_list
    size: 4
    repeat: expr
    repeat-expr: 3
    -randomize-count: range
    -randomize-count-range-min: count_min
    -randomize-count-range-max: count_max
    -randomize-size: range
    -randomize-size-range-min: 0
    -randomize-size-range-max: 4
    -randomize-content: range
    -randomize-content-range-min: 0x00
    -randomize-content-range-max: 0xff
  - id: ascii
    size: 4
    type: str
    encoding: ASCII
    -randomize-size: range
    -randomize-size-range-min: 0
    -randomize-size-range-max: 4
    -randomize-content: range
    -randomize-content-range-min: 0x00
    -randomize-content-range-max: 0xff
  - id: ascii_list
    size: 4
    type: str
    encoding: ASCII
    repeat: expr
    repeat-expr: 3
    -randomize-count: range
    -randomize-count-range-min: count_min
    -randomize-count-range-max: count_max
    -randomize-size: range
    -randomize-size-range-min: 0
    -randomize-size-range-max: 4
    -randomize-content: range
    -randomize-content-range-min: 0x00
    -randomize-content-range-max: 0xff
  - id: utf16le
    size: 8
    type: str
    encoding: UTF-16LE
    -randomize-size: range
    -randomize-size-range-min: 0
    -randomize-size-range-max: 8
    -randomize-content: range
    -randomize-content-range-min: 0x0000
    -randomize-content-range-max: 0xffff
  - id: utf16le_list
    size: 8
    type: str
    encoding: UTF-16LE
    repeat: expr
    repeat-expr: 3
    -randomize-count: range
    -randomize-count-range-min: count_min
    -randomize-count-range-max: count_max
    -randomize-size: range
    -randomize-size-range-min: 0
    -randomize-size-range-max: 8
    -randomize-content: range
    -randomize-content-range-min: 0x0000
    -randomize-content-range-max: 0xffff
  - id: utf16be
    size: 8
    type: str
    encoding: UTF-16BE
    -randomize-size: range
    -randomize-size-range-min: 0
    -randomize-size-range-max: 8
    -randomize-content: range
    -randomize-content-range-min: 0x0000
    -randomize-content-range-max: 0xffff
  - id: utf16be_list
    size: 8
    type: str
    encoding: UTF-16BE
    repeat: expr
    repeat-expr: 3
    -randomize-count: range
    -randomize-count-range-min: count_min
    -randomize-count-range-max: count_max
    -randomize-size: range
    -randomize-size-range-min: 0
    -randomize-size-range-max: 8
    -randomize-content: range
    -randomize-content-range-min: 0x0000
    -randomize-content-range-max: 0xffff
  - id: utf8
    size: 4
    type: str
    encoding: UTF-8
    -randomize-size: range
    -randomize-size-range-min: 0
    -randomize-size-range-max: 4
    -randomize-content: range
    -randomize-content-range-min: 0x00000000
    -randomize-content-range-max: 0xffffffff
  - id: utf8_list
    size: 4
    type: str
    encoding: UTF-8
    repeat: expr
    repeat-expr: 3
    -randomize-count: range
    -randomize-count-range-min: count_min
    -randomize-count-range-max: count_max
    -randomize-size: range
    -randomize-size-range-min: 0
    -randomize-size-range-max: 4
    -randomize-content: range
    -randomize-content-range-min: 0x00000000
    -randomize-content-range-max: 0xffffffff
  - id: object
    type: random_object
  - id: object_list
    type: random_object
    repeat: expr
    repeat-expr: 3
    -randomize-count: range
    -randomize-count-range-min: count_min
    -randomize-count-range-max: count_max
  - id: u2_eos
    type: u2
    repeat: eos
    -randomize-count: range
    -randomize-count-range-min: count_min
    -randomize-count-range-max: count_max

types:
  random_object:
    seq:
      - id: data_size
        type: u4
        -randomize-content: range
        -randomize-content-range-min: 1
        -randomize-content-range-max: 8
      - id: data
        size: data_size
        -randomize-size: range
        -randomize-size-range-min: 1
        -randomize-size-range-max: 8
        -randomize-content: range
        -randomize-content-range-min: 0x00
        -randomize-content-range-max: 0xff
    -update:
      - id: data_size
        value: data.length
