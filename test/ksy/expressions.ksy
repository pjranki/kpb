meta:
  id: expressions
  endian: le

seq:
  - id: list_numbers
    type: u4
    repeat: expr
    repeat-expr: 3
  - id: list_objs
    type: expr_obj
    repeat: expr
    repeat-expr: 3

instances:
  op_addition:
    value: 2 + 3
  op_subtract:
    value: 7 - 4
  op_multiplication:
    value: 4 * 5
  op_division:
    value: 16 / 3
  op_modulo:
    value: 16 % 3
  op_relational_lt_true:
    value: 1 < 2
  op_relational_lt_false:
    value: 2 < 1
  op_relational_gt_true:
    value: 2 > 1
  op_relational_gt_false:
    value: 1 > 2
  op_relational_lte_true:
    value: 1 <= 1
  op_relational_lte_false:
    value: 2 <= 1
  op_relational_gte_true:
    value: 1 >= 1
  op_relational_gte_false:
    value: 1 >= 2
  op_relational_eq_true:
    value: 2 == 2
  op_relational_eq_false:
    value: 1 == 2
  op_relational_ne_true:
    value: 1 != 2
  op_relational_ne_false:
    value: 2 != 2
  op_bitwise_lsh:
    value: 1 << 2
  op_bitwise_rsh:
    value: 0x10 >> 2
  op_bitwise_and:
    value: 0xf0 & 0x1f
  op_bitwise_or:
    value: 0x8 | 0x4
  op_bitwise_xor:
    value: 0xaa ^ 0xff
  op_bitwise_inv:
    value: 0xff & ~0xf0
  op_logical_and_true:
    value: true and true
  op_logical_and_false:
    value: true and false
  op_logical_or_true:
    value: true or false
  op_logical_or_false:
    value: false or false
  op_logical_not_true:
    value: not false
  op_logical_not_false:
    value: not true
  op_enum_eq_true:
    value: 'foobar::v1 == foobar::v1'
  op_ternary_choose_1:
    value: 'true ? true ? 1 : 2 : false ? 3 : 4'
  op_ternary_choose_2:
    value: 'true ? false ? 1 : 2 : true ? 3 : 4'
  op_ternary_choose_3:
    value: 'false ? false ? 1 : 2 : true ? 3 : 4'
  op_ternary_choose_4:
    value: 'false ? true ? 1 : 2 : false ? 3 : 4'
  op_list_index:
    value: 1
  op_list_number:
    value: list_numbers[_root.op_list_index]
  op_list_obj:
    value: _root.list_objs[op_list_index * 2].value1
  op_list_nested:
    value: _root.list_objs[op_list_index].list_numbers[1]
  op_order_47:
    value: 7 * 6 + 5
  op_order_37:
    value: 7 + 6 * 5
  op_order_65:
    value: (7 + 6) * 5
  op_order_77:
    value: 7 * (6 + 5)
  op_same_op_24:
    value: 2 * 3 * 4
  op_logic_order_true:
    value: true and true or not false and false

enums:
  foobar:
    1: v1

types:
  expr_obj:
    seq:
      - id: value1
        type: u4
      - id: list_numbers
        type: u4
        repeat: expr
        repeat-expr: 3
