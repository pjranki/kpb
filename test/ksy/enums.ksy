meta:
  id: enums
  endian: le

seq:
  - id: value1
    type: u1
    enum: test_enum

enums:
  test_enum:
    1:
      id: foobar
      doc: blah
    2: cafebabe

types:
  sub_enums:
    seq:
      - id: value2
        type: u4
        enum: test_enum
    enums:
      test_enum:
        0xffff:
          id: foobar
          doc: blah
        0x75a5: cafebabe
