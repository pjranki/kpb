meta:
  id: bad_class_obj
  endian: le

seq:
  - id: bad_member_total_size1
    type: u4

  - id: bad_member_flags
    type: u4

  - id: bad_member_unused
    type: u4

  - id: bad_member_buffer_size1
    type: u2
  - id: bad_member_buffer
    size: bad_member_buffer_size1
  - id: bad_member_buffer_list_count
    type: u2
  - id: bad_member_buffer_list
    size: 4
    repeat: expr
    repeat-expr: bad_member_buffer_list_count

  - id: bad_member_ascii_size1
    type: u1
  - id: bad_member_ascii
    type: str
    encoding: ascii
    size: bad_member_ascii_size1
  - id: bad_member_asciiz_size1
    type: u1
  - id: bad_member_asciiz
    type: strz
    encoding: ascii
    size: bad_member_asciiz_size1
  - id: bad_member_ascii_list_count
    type: u2
  - id: bad_member_ascii_list
    type: str
    encoding: ascii
    size: 4
    repeat: expr
    repeat-expr: bad_member_ascii_list_count

  - id: bad_member_wide_size1
    type: u1
  - id: bad_member_wide
    type: str
    encoding: utf-16le
    size: bad_member_wide_size1
  - id: bad_member_widez_size1
    type: u1
  - id: bad_member_widez
    type: strz
    encoding: utf-16le
    size: bad_member_widez_size1
  - id: bad_member_wide_list_count
    type: u2
  - id: bad_member_wide_list
    type: str
    encoding: utf-16le
    size: 4
    repeat: expr
    repeat-expr: bad_member_wide_list_count

  - id: bad_member_single_data
    type: bad_class_single_parent(bad_member_flags)
  - id: bad_member_single_data_list_count
    type: u1
  - id: bad_member_single_data_list
    type: bad_class_single_parent(bad_member_flags)
    repeat: expr
    repeat-expr: bad_member_single_data_list_count
  - id: bad_member_multi_data
    type: bad_class_multi_parent(bad_member_flags)
  - id: bad_member_multi_data_list_count
    type: u1
  - id: bad_member_multi_data_list
    type: bad_class_multi_parent(bad_member_flags)
    repeat: expr
    repeat-expr: bad_member_multi_data_list_count

-update:
  - id: bad_member_buffer_size1
    value: bad_member_buffer.length
  - id: bad_member_buffer_list_count
    value: bad_member_buffer_list.size

  - id: bad_member_ascii_size1
    value: bad_member_ascii.length
  - id: bad_member_asciiz_size1
    value: bad_member_asciiz.length + 1
  - id: bad_member_ascii_list_count
    value: bad_member_ascii_list.size

  - id: bad_member_wide_size1
    value: bad_member_wide.length * 2
  - id: bad_member_widez_size1
    value: (bad_member_widez.length + 1) * 2
  - id: bad_member_wide_list_count
    value: bad_member_wide_list.size

  - id: bad_member_single_data_list_count
    value: bad_member_single_data_list.size
  - id: bad_member_multi_data_list_count
    value: bad_member_multi_data_list.size

  - id: bad_member_single_data
  - id: bad_member_single_data
  - id: bad_member_single_data_list
  - id: bad_member_single_data_list
  - id: bad_member_multi_data
  - id: bad_member_multi_data
  - id: bad_member_multi_data_list
  - id: bad_member_multi_data_list

  - id: bad_member_unused
    value: bad_member_buffer_list[0].length
  - id: bad_member_unused
    value: bad_member_ascii_list[0].length
  - id: bad_member_unused
    value: bad_member_wide_list[0].length
  - id: bad_member_unused
    value: bad_member_single_data_list[0].length
  - id: bad_member_unused
    value: bad_member_single_data.length
  - id: bad_member_unused
    value: _root.bad_member_single_data.length
  - id: bad_member_unused
    value: 0

  - id: bad_member_total_size1
    value: _root.length

types:
  bad_class_single_parent:
    params:
      - id: bad_member_flags
        type: u4
    seq:
      - id: bad_member_data_size1
        type: u1
      - id: bad_member_data
        size: bad_member_data_size1
      - id: bad_member_multi_data
        type: bad_class_multi_parent(bad_member_flags)
    -update:
      - id: bad_member_data_size1
        value: bad_member_data.length
      - id: bad_member_multi_data
    instances:
      bad_member_parent_flags:
        value: _parent.bad_member_flags

  bad_class_multi_parent:
    params:
      - id: bad_member_flags
        type: u4
    seq:
      - id: bad_member_data_size1
        type: u1
      - id: bad_member_data
        size: bad_member_data_size1
    -update:
      - id: bad_member_data_size1
        value: bad_member_data.length

  bad_members:
    params:
      - id: default
        type: u1
      - id: init
        type: u1
      - id: fini
        type: u1
      - id: set
        type: u1
      - id: set_value
        type: u1
      - id: default_value
        type: u1
      - id: update
        type: u1
      - id: clear
        type: u1
      - id: value
        type: u1
      - id: index
        type: u1
      - id: count
        type: u1
      - id: size
        type: u1
      - id: length
        type: u1
      - id: value_size
        type: u1
      - id: value_count
        type: u1
      - id: value_length
        type: u1
      - id: serialize_to_string
        type: u1
      - id: parse_from_string
        type: u1
      - id: this
        type: u1
      - id: self
        type: u1
      - id: read
        type: u1
      - id: write
        type: u1
      - id: clear_value
        type: u1
      - id: has
        type: u1
      - id: has_value
        type: u1
      - id: mutable_value
        type: u1
      - id: add_value
        type: u1
      - id: at
        type: u1
      - id: size_at
        type: u1
      - id: value_at
        type: u1
      - id: value_size_at
        type: u1
      - id: void
        type: u1
      - id: io
        type: u1
      - id: class_t
        type: u1
      - id: true
        type: u1
      - id: false
        type: u1
      - id: len
        type: u1
      - id: mutable
        type: u1
      - id: nullptr
        type: u1
      - id: none
        type: u1
      - id: value_set
        type: u1
      - id: parent
        type: u1

  bad_class_set:
    params:
      - id: parent
        type: u1
    seq:
      - id: value1
        type: u1

  bad_cpp:
    seq:
      - id: bad_cpp
        type: u1

  bad_c_class:
    seq:
      - id: prefix_bad_c_class
        type: prefix_bad_c_class
      - id: prefix_prefix_bad_c_class
        type: prefix_prefix_bad_c_class

  prefix_bad_c_class:
    seq:
      - id: value1
        type: u1

  prefix_prefix_bad_c_class:
    seq:
      - id: value1
        type: u1

  correct_use_of_size:
    seq:
      - id: size
        type: u4
      - id: value1
        type: u4
    -update:
      - id: value1
        value: size
    instances:
      value2:
        value: size

  correct_use_of_length:
    seq:
      - id: length
        type: u4
      - id: value1
        type: u4
    -update:
      - id: value1
        value: length
        doc: |
          update/set will use length as the size of the object (not the member's value)
    instances:
      value2:
        value: length
        doc: |
          everything else will use length as the member's value


instances:
  bad_member_0:
    value: 1
  bad_member_1:
    value: 'bad_enum_foobar::bad_value_enum_v1 == bad_enum_foobar::bad_value_enum_v1'
  bad_member_2:
    value: bad_member_single_data_list[_root.bad_member_0]
  bad_member_3:
    value: _root.bad_member_single_data_list[bad_member_0 * 2].bad_member_flags
  bad_member_4:
    value: _root.bad_member_single_data_list[bad_member_0].bad_member_data_size1

enums:
  bad_enum_foobar:
    0x1234: bad_value_enum_v1
