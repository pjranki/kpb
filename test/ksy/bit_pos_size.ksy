meta:
  id: bit_pos_size
  -bitalign: 1
  endian: be

params:
  - id: bit_count
    type: u4
  - id: bit_align
    type: u4
  - id: expected_pos_in_bits
    type: u4
  - id: expected_size_in_bits
    type: u4

seq:
  - id: bits
    type: b1
    repeat: expr
    repeat-expr: bit_count
  - id: padding
    type: b1
    repeat: expr
    repeat-expr: '((_io.bit_pos % bit_align) == 0) ? 0 : (bit_align - (_io.bit_pos % bit_align))'
  - id: end
    type: b1
  - id: ok
    type: b8
    repeat: expr
    repeat-expr: '((_io.bit_size == expected_size_in_bits) and (_io.bit_pos == expected_pos_in_bits)) ? 1 : 0'
