meta:
  id: io_expr
  endian: le

seq:
  - id: data
    size: 5
  - id: align
    size: (4 - _io.pos) % 4

types:
  io_expr_bytes:
    seq:
      - id: fixed
        type: u1
      - id: variable
        size-eos: true

  io_expr_eos:
    seq:
      - id: fixed
        type: u1
      - id: variable
        type: u2
        repeat: eos

  io_expr_until:
    seq:
      - id: fixed
        type: u1
      - id: variable
        type: u2
        repeat: until
        repeat-until: _ == 0xbad

  io_expr_until_class_list:
    seq:
      - id: items
        type: io_expr_until_class_item
        repeat: until
        repeat-until: _.end

  io_expr_until_class_item:
    seq:
      - id: info
        type: io_expr_until_class_info
    instances:
      end:
        value: info.value == 3

  io_expr_until_class_info:
    seq:
      - id: value
        type: u1
