meta:
  id: process
  endian: le

types:
  process_config:
    seq:
      - id: alignment
        type: u4

  process_body:
    seq:
      - id: message
        type: strz
        encoding: ascii

  process_body_size:
    seq:
      - id: config
        type: process_config
      - id: body_size
        type: u4
      - id: padding
        size: '((_io.pos % config.alignment) != 0) ? (config.alignment - (_io.pos % config.alignment)) : 0'
      - id: body
        type: process_body
        size: body_size
        process: xor_encoding(0xaa, config)
    -update:
      - id: body_size
        value: body.length

  process_body_size_eos:
    seq:
      - id: config
        type: process_config
      - id: padding
        size: '((_io.pos % config.alignment) != 0) ? (config.alignment - (_io.pos % config.alignment)) : 0'
      - id: body
        type: process_body
        size-eos: true
        process: xor_encoding(0xaa, config)
