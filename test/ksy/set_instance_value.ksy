meta:
  id: set_instance_value
  endian: le

params:
  - id: storage_bool
    type: bool

seq:
  - id: storage_b4
    type: b4
  - id: storage_u4
    type: u4
  - id: storage_s4
    type: s4
  - id: storage_f4
    type: f4
  - id: nested_4
    type: u1
    -set:
      - id: nested_3
        value: nested_4 * 5
  - id: nested_3
    type: u1
    -set:
      - id: nested_4
        value: nested_3 * 4

instances:
  input_bool:
    value: storage_bool
    -set:
      - id: storage_bool
        value: input_bool
  input_b4:
    value: storage_b4
    -set:
      - id: storage_b4
        value: input_b4
  input_u4:
    value: storage_u4
    -set:
      - id: storage_u4
        value: input_u4
  input_s4:
    value: storage_s4
    -set:
      - id: storage_s4
        value: input_s4
  input_f4:
    value: storage_f4
    -set:
      - id: storage_f4
        value: input_f4
  calc_value_u4:
    value: storage_u4
    -set:
      - id: calc_value_u4
        value: calc_value_u4 * 4
      - id: storage_u4
        value: calc_value_u4 + 1
  nested_2:
    value: nested_3 / 3
    -set:
      - id: nested_3
        value: nested_2 * 3
  nested_1:
    value: nested_2 / 2
    -set:
      - id: nested_2
        value: nested_1 * 2
