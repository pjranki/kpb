meta:
  id: integers

seq:
  - id: integer_s2le
    type: s2le
  - id: integer_u2le
    type: u2le
  - id: integer_s4le
    type: s4le
  - id: integer_u4le
    type: u4le
  - id: integer_s8le
    type: s8le
  - id: integer_u8le
    type: u8le
  - id: integer_s2be
    type: s2be
  - id: integer_u2be
    type: u2be
  - id: integer_s4be
    type: s4be
  - id: integer_u4be
    type: u4be
  - id: integer_s8be
    type: s8be
  - id: integer_u8be
    type: u8be
