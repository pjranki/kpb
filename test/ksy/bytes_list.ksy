meta:
  id: bytes_list

seq:
  - id: data2_list
    size: 2
    repeat: expr
    repeat-expr: 5
  - id: data4_list
    size: 4
    repeat: expr
    repeat-expr: 5
