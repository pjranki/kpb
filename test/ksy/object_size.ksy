meta:
  id: object_size
  endian: le

seq:
  - id: object_size
    type: u4
  - id: object_var
    type: object_size_4
    size: object_size
  - id: object_fix
    type: object_size_4

types:
  object_size_4:
    seq:
      - id: value
        type: u4
