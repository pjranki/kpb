meta:
  id: object_list
  endian: le

seq:
  - id: list
    type: object_item
    repeat: expr
    repeat-expr: 3

types:
  object_item:
    seq:
      - id: value
        type: u4
