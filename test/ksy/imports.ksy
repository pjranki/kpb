meta:
  id: imports
  imports:
    - integers_le
    - bitfield
    - object_single

seq:
  - id: value1
    type: bitfield
  - id: value2
    type: integers_le
  - id: value3
    type: object_single
