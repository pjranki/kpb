meta:
  id: bitalign
  endian: be

types:
  v3bd:
    seq:
     - id: v
       type: b3

  bitalign_default:
    seq:
      - id: v1
        type: b1
      - id: v2
        type: v3bd
      - id: v3
        type: v3bd
      - id: v4
        type: b12

  v3b8:
    -bitalign: 8
    seq:
     - id: v
       type: b3

  bitalign_8:
    -bitalign: 8
    seq:
      - id: v1
        type: b1
      - id: v2
        type: v3b8
      - id: v3
        type: v3b8
      - id: v4
        type: b12

  v3b1:
    -bitalign: 1
    seq:
     - id: v
       type: b3

  bitalign_1:
    -bitalign: 1
    seq:
      - id: v1
        type: b1
      - id: v2
        type: v3b1
      - id: v3
        type: v3b1
      - id: v4
        type: b12
