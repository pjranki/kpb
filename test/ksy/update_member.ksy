meta:
  id: member_update
  endian: le

seq:
  - id: key
    type: u2
  - id: obj
    type: member_update_obj

-update:
  - id: key
    value: obj.key

enums:
  key:
    1: u1
    2: u2
    4: u4
    8: u8
    9: number
    10: obj
    11: obj_list

types:
  member_update_obj:
    seq:
      - id: key
        type: u4
      - id: number
        type: u4
        if: 'key == key::number.to_i'
        -set:
          - id: key
            value: 'key::number.to_i'
      - id: obj
        type: member_update_sub_obj
        if: 'key == key::obj.to_i'
        -set:
          - id: key
            value: 'key::obj.to_i'
      - id: obj_list
        type: member_update_sub_obj
        repeat: expr
        repeat-expr: 2
        if: 'key == key::obj_list.to_i'
        -set:
          - id: key
            value: 'key::obj_list.to_i'
      - id: switch
        -set:
          - id: key
            value: _case & 0xf
        type:
          switch-on: key
          -name: type
          cases:
            1: u1
            'key::u2.to_i': u2
            4: u4
            8: u8

  member_update_sub_obj:
    seq:
      - id: value
        type: u1
