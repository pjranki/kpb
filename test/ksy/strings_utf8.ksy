meta:
  id: strings_utf8

seq:
  - id: text
    type: str
    size: 2
    encoding: UTF_8
  - id: textz
    type: strz
    encoding: UTF_8
  - id: textz_sized
    type: strz
    encoding: UTF_8
    size: 3
