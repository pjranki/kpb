meta:
  id: bitfield

seq:
  - id: bitfield_b1
    type: b1
  - id: bitfield_b2
    type: b2
  - id: bitfield_b4
    type: b4
  - id: bitfield_b8
    type: b8
  - id: bitfield_b16
    type: b16
  - id: bitfield_b32
    type: b32
  - id: bitfield_b64
    type: b64
