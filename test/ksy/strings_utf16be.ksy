meta:
  id: strings_utf16be

seq:
  - id: text
    type: str
    size: 4
    encoding: UTF_16BE
  - id: textz
    type: strz
    encoding: UTF_16BE
  - id: textz_sized
    type: strz
    encoding: UTF_16BE
    size: 6
