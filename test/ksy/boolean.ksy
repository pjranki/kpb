meta:
  id: boolean
  endian: le

params:
  - id: value1
    type: bool

seq:
  - id: value2
    type: b1
