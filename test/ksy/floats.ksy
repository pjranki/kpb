meta:
  id: floats
  endian: le

seq:
  - id: float
    type: f4
  - id: double
    type: f8
  - id: floatle
    type: f4le
  - id: doublele
    type: f8le
  - id: floatbe
    type: f4be
  - id: doublebe
    type: f8be
