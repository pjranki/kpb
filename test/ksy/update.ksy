meta:
  id: obj_update
  endian: le

seq:
  - id: total_size1
    type: u4
    -default: 32

  - id: flags
    type: u4

  - id: unused
    type: u4

  - id: buffer_size1
    type: u2
  - id: buffer
    size: buffer_size1
  - id: buffer_list_count
    type: u2
  - id: buffer_list
    size: 4
    repeat: expr
    repeat-expr: buffer_list_count

  - id: ascii_size1
    type: u1
  - id: ascii
    type: str
    encoding: ascii
    size: ascii_size1
  - id: asciiz_size1
    type: u1
    -default: 1  # when empty, will have a nul-terminator
  - id: asciiz
    type: strz
    encoding: ascii
    size: asciiz_size1
  - id: ascii_list_count
    type: u2
  - id: ascii_list
    type: str
    encoding: ascii
    size: 4
    repeat: expr
    repeat-expr: ascii_list_count

  - id: wide_size1
    type: u1
  - id: wide
    type: str
    encoding: utf-16le
    size: wide_size1
  - id: widez_size1
    type: u1
    -default: 2  # when empty, will have a nul-terminator
  - id: widez
    type: strz
    encoding: utf-16le
    size: widez_size1
  - id: wide_list_count
    type: u2
  - id: wide_list
    type: str
    encoding: utf-16le
    size: 4
    repeat: expr
    repeat-expr: wide_list_count

  - id: single_data
    type: single_parent(flags)
  - id: single_data_list_count
    type: u1
  - id: single_data_list
    type: single_parent(flags)
    repeat: expr
    repeat-expr: single_data_list_count
  - id: multi_data
    type: multi_parent(flags)
  - id: multi_data_list_count
    type: u1
  - id: multi_data_list
    type: multi_parent(flags)
    repeat: expr
    repeat-expr: multi_data_list_count

-update:
  - id: buffer_size1
    value: buffer.length
  - id: buffer_list_count
    value: buffer_list.size

  - id: ascii_size1
    value: ascii.length
  - id: asciiz_size1
    value: asciiz.length + 1
  - id: ascii_list_count
    value: ascii_list.size

  - id: wide_size1
    value: wide.length * 2
  - id: widez_size1
    value: (widez.length + 1) * 2
  - id: wide_list_count
    value: wide_list.size

  - id: single_data_list_count
    value: single_data_list.size
  - id: multi_data_list_count
    value: multi_data_list.size

  - id: single_data
  - id: single_data
  - id: single_data_list
  - id: single_data_list
  - id: multi_data
  - id: multi_data
  - id: multi_data_list
  - id: multi_data_list

  - id: unused
    value: buffer_list[0].length
  - id: unused
    value: ascii_list[0].length
  - id: unused
    value: wide_list[0].length
  - id: unused
    value: single_data_list[0].length
  - id: unused
    value: single_data.length
  - id: unused
    value: _root.single_data.length
  - id: unused
    value: 0

  - id: total_size1
    value: _root.length

types:
  single_parent:
    params:
      - id: flags
        type: u4
    seq:
      - id: data_size1
        type: u1
      - id: data
        size: data_size1
      - id: multi_data
        type: multi_parent(flags)
    -update:
      - id: data_size1
        value: data.length
      - id: multi_data
    instances:
      parent_flags:
        value: _parent.flags

  multi_parent:
    params:
      - id: flags
        type: u4
    seq:
      - id: data_size1
        type: u1
      - id: data
        size: data_size1
    -update:
      - id: data_size1
        value: data.length
