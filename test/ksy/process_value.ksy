meta:
  id: process_value
  endian: le
  encoding: ascii

params:
  - id: getter_count
    type: u1
  - id: setter_count
    type: u1
  - id: memset_value
    type: u1
  - id: invert_logic
    type: bool
  - id: multiple_float_by_4
    type: bool
  - id: value1
    type: u4
    -process-value: process_value_u4
  - id: value2
    type: bool
    -process-value: process_value_bool(invert_logic)

seq:
  - id: value3
    type: s2
    -process-value: process_value_s2
  - id: value4
    type: b3
    -process-value: process_value_b3(_root)
  - id: value5
    type: strz
    -process-value: process_value_strz(_root.memset_value)
  - id: value6
    size: 5
    -process-value: process_value_buf5(_root.memset_value)
  - id: value7
    type: f4
    -process-value: process_value_f4(multiple_float_by_4)
  - id: value8
    type: sub_process_value(0xff)
    -process-value: process_value_sub_type(_root)

instances:
  value9:
    value: _root
    -process-value: process_value_root(_root, 0xaa)
  value10:
    value: false
    -process-value: process_value_bool(invert_logic)

types:
  sub_process_value:
    params:
      - id: input_value
        type: u1
    seq:
      - id: value
        type: u1
