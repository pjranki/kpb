meta:
  id: packet
  endian: le

seq:
  - id: header
    type: packet_header
  - id: body
    type: packet_body
    size: header.body_size

-update:
  - id: header

types:
  packet_header:
    seq:
      - id: total_size
        type: u4
        -default: 10
      - id: body_size
        type: u4
        -default: 2
    instances:
      header_size:
        value: 4 + 4
    -update:
      - id: body_size
        value: _parent.body.length
      - id: total_size
        value: header_size + body_size
  packet_body:
    seq:
      - id: data_size
        type: u2
      - id: data
        size: data_size
    -update:
      - id: data_size
        value: data.length
