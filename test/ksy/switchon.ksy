meta:
  id: switchon
  endian: le
  encoding: ascii

seq:
  - id: data_type
    type: u1
    enum: oneof_type
  - id: data
    type:
      switch-on: data_type
      -name: enum
      cases:
         'oneof_type::object': object1
         'oneof_type::number': object2
  - id: value_type
    type: u2
  - id: value1
    type:
      switch-on: value_type
      -name: type
      cases:
         2: u2
         4: u4
         _: u8
  - id: data2
    type:
      switch-on: data_type.to_i
      -name: type
      cases:
         'oneof_type::number.to_i': object3

types:
  object1:
    seq:
      - id: value1
        type: u4
  object2:
    seq:
      - id: value2
        type: u2
  object3:
    seq:
      - id: empty
        size: 0

  object_bool:
    params:
      - id: arg
        type: bool
    seq:
      - id: data
        type: u1

  dup_enum_number:
    seq:
      - id: data_type
        type: u1
      - id: data
        type:
          switch-on: data_type
          cases:
            1: object_bool(true)
            2: object_bool(false)

  dup_enum_name:
    seq:
      - id: data_type
        type: u1
        enum: boolean
      - id: data
        type:
          switch-on: data_type
          cases:
            'boolean::true': object_bool(true)
            'boolean::false': object_bool(false)
    enums:
      boolean:
        10: true
        20: false

enums:
  oneof_type:
    1: object
    2: string
    3: number
