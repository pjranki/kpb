meta:
  id: basic_list
  endian: le

seq:
  - id: bitfield
    type: b3
    repeat: expr
    repeat-expr: 4
  - id: numbers
    type: u2
    repeat: expr
    repeat-expr: 5
