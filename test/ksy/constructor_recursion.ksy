meta:
  id: constructor_recursion
  endian: le

seq:
  - id: size
    type: u4
  - id: child_value_count
    type: u4
  - id: child
    type: constructor_recursion_child

-update:
  - id: size
    value: length
  - id: child_value_count
    value: child.value.size

types:
  constructor_recursion_child:
    seq:
      - id: value
        type: u4
        repeat: expr
        repeat-expr: _parent.child_value_count
