meta:
  id: integers_be
  endian: be

seq:
  - id: integer_s1
    type: s1
  - id: integer_u1
    type: u1
  - id: integer_s2
    type: s2
  - id: integer_u2
    type: u2
  - id: integer_s4
    type: s4
  - id: integer_u4
    type: u4
  - id: integer_s8
    type: s8
  - id: integer_u8
    type: u8
