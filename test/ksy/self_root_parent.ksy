meta:
  id: root
  endian: le

seq:
  - id: value
    type: u4
  - id: child
    type: child
  - id: child_list
    type: child
    repeat: expr
    repeat-expr: 1

instances:
  root_value:
    value: _root.value
  self_value:
    value: value

types:
  child:
    seq:
      - id: value
        type: u4
      - id: child
        type: grand_child
      - id: grand_child_list
        type: grand_child
        repeat: expr
        repeat-expr: 1
    instances:
      root_value:
        value: _root.value
      self_value:
        value: value
      parent_value:
        value: _parent.value

  grand_child:
    seq:
      - id: value
        type: u4
    instances:
      root_value:
        value: _root.value
      self_value:
        value: value
      parent_value:
        value: _parent.value
