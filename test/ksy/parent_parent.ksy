meta:
  id: parent_parent
  endian: le
  imports:
    - parent_child

params:
  - id: id
    type: u8
    -default: 42
  - id: child
    type: parent_child
