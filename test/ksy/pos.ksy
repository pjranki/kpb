meta:
  id: pos
  endian: le

seq:
  - id: sub_pos_offset
    type: u4
    if: error_code == 0
    -default: 8
  - id: sub_pos_size
    type: u4
    if: error_code == 0
    -default: 11

instances:
  first_u4:
    type: u4
    pos: 0
    if: _io.size >= 4
    -default: 0
  error_code:
    value: '((first_u4 & 0x80000000) != 0 ? first_u4 : 0) & 0xffffffff'
  sub_pos:
    type: sub_pos
    pos: sub_pos_offset
    size: sub_pos_size
    if: error_code == 0

-update:
  - id: sub_pos_size
    value: sub_pos.length

types:
  sub_pos:
    seq:
      - id: value1
        type: u4
    instances:
      value2:
        pos: 7
        type: u4
