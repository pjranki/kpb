meta:
  id: parse_or_fill

seq:
  - id: value1
    type: u4be
  - id: other
    type: other_parse_or_fill
    # TODO: size: 4

types:
  other_parse_or_fill:
    seq:
      - id: value2
        type: u4be

  parse_or_fill_eof:
    -bitalign: 1
    seq:
      - id: prefix
        type: b4
      - id: value
        type: b8
        repeat: eos
