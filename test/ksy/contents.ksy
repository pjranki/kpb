meta:
  id: contents

seq:
  - id: magic
    contents: [0xca, 0xfe, 0xba, 0xbe]
    doc: |
      EXPECTED: "\xCA\xFE\xBA\xBE", 4
  - id: magic1
    contents: JFIF
    doc: |
      EXPECTED: "\x4A\x46\x49\x46", 4
  - id: magic2
    contents:
      - 0xca
      - 0xfe
      - 0xba
      - 0xbe
    doc: |
      EXPECTED: "\xCA\xFE\xBA\xBE", 4
  - id: magic3
    contents: [CAFE, 0, BABE]
    doc: |
      EXPECTED: "\x43\x41\x46\x45\x00\x42\x41\x42\x45", 9
  - id: magic4
    contents: [foo, 0, A, 0xa, 42]
    doc: |
      EXPECTED: "\x66\x6F\x6F\x00\x41\x0A\x2A", 7
  - id: magic5
    contents: [1, 0x55, 'A,3', 3]
    doc: |
      EXPECTED: "\x01\x55\x41\x2C\x33\x03", 6
