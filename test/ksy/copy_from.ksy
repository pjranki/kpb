meta:
  id: copy_from
  endian: le

params:
  - id: copy_source
    -type: copy_type

seq:
  - id: copy_obj
    type: copy_sub_type(copy_source)

types:
  copy_type:
    seq:
      - id: value1
        type: u4

  copy_sub_type:
    params:
      - id: copy_destination
        -type: copy_type
    seq:
      - id: value
        type: u4
      - id: value_array
        type: u4
        repeat: expr
        repeat-expr: 4
      - id: float
        type: f4
      - id: float_array
        type: f4
        repeat: expr
        repeat-expr: 4
      - id: double
        type: f8
      - id: double_array
        type: f8
        repeat: expr
        repeat-expr: 4
      - id: string
        type: strz
        encoding: ascii
      - id: string_array
        type: strz
        encoding: ascii
        repeat: expr
        repeat-expr: 4
      - id: buffer
        size: 8
      - id: buffer_array
        size: 8
        repeat: expr
        repeat-expr: 4
      - id: obj
        type: copy_type
      - id: obj_array
        type: copy_type
        repeat: expr
        repeat-expr: 4
