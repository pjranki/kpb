meta:
  id: value_instances
  endian: le

seq:
  - id: value1
    type: u4

instances:
  instance1:
    value: -1234
  instance2:
    value: 5678
