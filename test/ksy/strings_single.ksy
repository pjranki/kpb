meta:
  id: strings_single
  encoding: UTF_16LE

seq:
  - id: str_ascii
    type: str
    size: 2
    encoding: ASCII
  - id: str_wchar
    type: str
    size: 4
    encoding: UTF_16LE
  - id: strz_ascii
    type: strz
    encoding: ASCII
  - id: strz_wchar
    type: strz
  - id: strz_ascii_sized
    type: strz
    size: 3
    encoding: ASCII
  - id: strz_wchar_sized
    type: strz
    size: 6
