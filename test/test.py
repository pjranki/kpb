import os
import sys
import shutil
import subprocess

def cmake_generator():
    if os.name != 'nt':
        return 'Unix Makefiles'
    for line in subprocess.Popen(['cmake', '--help'], stdout=subprocess.PIPE).communicate()[0].split(b'\n'):
        line = line.decode()
        if 'Visual Studio' not in line:
            continue
        line = line.replace('\n', '').replace('\r', '').strip()
        if not line.startswith('*'):
            continue
        generator = line[1:].split('=')[0].split('[')[0].strip()
        return generator
    raise NotImplementedError("default cmake generator for visual studio not found, run cmake --help to see why")

def cmake(path, out, generator, arch):
    if not os.path.exists(out):
        os.makedirs(out)
    assert os.path.exists(out)
    tmp = os.getcwd()
    try:
        os.chdir(out)
        assert out == os.getcwd()
        if os.name == 'nt':
            arch = {'x86': 'Win32', 'amd64': 'x64'}[arch]
            p = subprocess.Popen(
                ['cmake', '-G', generator, '-A', arch, '-D', 'CMAKE_KPB_TEST=kpb_test', path],
                shell=(os.name == 'nt'))
        else:
            p = subprocess.Popen(
                ['cmake', '-G', generator, '-D', 'CMAKE_KPB_TEST=kpb_test', path],
                shell=(os.name == 'nt'))
        p.communicate()
        assert 0 == p.returncode
    finally:
        os.chdir(tmp)

def cmake_build(out, configuration, jobs):
    configuration = {'debug': 'Debug', 'release': 'Release'}[configuration]
    assert os.path.exists(out)
    tmp = os.getcwd()
    try:
        os.chdir(out)
        assert out == os.getcwd()
        p = subprocess.Popen(
            ['cmake', '--build', '.', '--config', configuration, '--parallel', str(jobs)],
            shell=(os.name == 'nt'),
            cwd=out)
        p.communicate()
        assert 0 == p.returncode
    finally:
        os.chdir(tmp)

def run_executable_target(out, configuration, target_name):
    configuration = {'debug': 'Debug', 'release': 'Release'}[configuration]
    if os.name == 'nt':
        path = os.path.join(out, 'test', configuration, target_name + '.exe')
    else:
        path = os.path.join(out, 'test', target_name)
    assert os.path.exists(path)
    p = subprocess.Popen([path], shell=(os.name=='nt'))
    p.communicate()
    assert 0 == p.returncode

def run_python(out):
    root = os.path.dirname(os.path.realpath(__file__))
    root = os.path.realpath(os.path.join(root, '..'))
    path = os.path.join(root, 'test', 'python', 'main.py')
    assert os.path.exists(path)
    env = dict()
    env.update(os.environ)
    env['KPB_OUT_PATH'] = out
    p = subprocess.Popen([sys.executable, path], env=env, shell=(os.name=='nt'))
    p.communicate()
    assert 0 == p.returncode

def main():
    if os.name == 'nt':
        build_arch = ['x86', 'amd64']
        build_config = ['debug', 'release']
    else:
        # TODO: arch and config do nothing, add in future
        build_arch = ['amd64']
        build_config = ['release']
    root = os.path.dirname(os.path.realpath(__file__))
    root = os.path.realpath(os.path.join(root, '..'))
    out = os.path.realpath(os.path.join(root, 'test', 'out_{}'.format(os.name)))
    for arch in build_arch:
        for config in build_config:
            out_arch_config = os.path.join('{}_{}_{}'.format(out, arch, config))
            cmake(root, out_arch_config, cmake_generator(), arch)
    for arch in build_arch:
        for config in build_config:
            out_arch_config = os.path.join('{}_{}_{}'.format(out, arch, config))
            cmake_build(out_arch_config, config, 4)
    for arch in build_arch:
        for config in build_config:
            out_arch_config = os.path.join('{}_{}_{}'.format(out, arch, config))
            run_executable_target(out_arch_config, config, 'kpb_test_c')
    for arch in build_arch:
        for config in build_config:
            out_arch_config = os.path.join('{}_{}_{}'.format(out, arch, config))
            run_executable_target(out_arch_config, config, 'kpb_test_cpp_stl')
    for arch in build_arch:
        for config in build_config:
            out_arch_config = os.path.join('{}_{}_{}'.format(out, arch, config))
            run_python(out_arch_config)

if __name__ == '__main__':
    main()
