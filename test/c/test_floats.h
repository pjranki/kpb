#ifndef _H_KPB_TEST_FLOATS_H_
#define _H_KPB_TEST_FLOATS_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_floats(void);

#ifdef __cplusplus
};
#endif

#endif
