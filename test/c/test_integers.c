#include "test_integers.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/integers_be.h"
#include "c/integers_le.h"
#include "c/integers.h"

#define BYTES_ZERO_30 "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
#define BYTES_ZERO_30_SIZE (sizeof(BYTES_ZERO_30)-1)
#define BYTES_ONES_30 "\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff"
#define BYTES_ONES_30_SIZE (sizeof(BYTES_ONES_30)-1)

int kpb_test_integers(void)
{
    // defaults
    kpb_test_integers_le_t integers;
    kpb_test_integers_le_init(&integers);
    EXPECT_EQUAL(kpb_test_integers_le_integer_s1(&integers), 0);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u1(&integers), 0);
    EXPECT_EQUAL(kpb_test_integers_le_integer_s2(&integers), 0);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u2(&integers), 0);
    EXPECT_EQUAL(kpb_test_integers_le_integer_s4(&integers), 0);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u4(&integers), 0);
    EXPECT_EQUAL(kpb_test_integers_le_integer_s8(&integers), 0);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u8(&integers), 0);

    kpb_test_integers_le_t integers_zeros;
    kpb_test_integers_le_init(&integers_zeros);
    size_t expected_zeros_size = 0;
    char *expected_zeros = kpb_test_integers_le_serialize_to_string(&integers_zeros, &expected_zeros_size);
    EXPECT_NOT_NULL(expected_zeros);
    EXPECT_BYTES_EQUAL(expected_zeros, expected_zeros_size, BYTES_ZERO_30, BYTES_ZERO_30_SIZE);
    kpb_io_free(expected_zeros);

    kpb_test_integers_le_t integers_ones;
    kpb_test_integers_le_init(&integers_ones);
    kpb_test_integers_le_set_integer_s1(&integers_ones, -1);
    kpb_test_integers_le_set_integer_u1(&integers_ones, 0xff);
    kpb_test_integers_le_set_integer_s2(&integers_ones, -1);
    kpb_test_integers_le_set_integer_u2(&integers_ones, 0xffff);
    kpb_test_integers_le_set_integer_s4(&integers_ones, -1);
    kpb_test_integers_le_set_integer_u4(&integers_ones, 0xffffffff);
    kpb_test_integers_le_set_integer_s8(&integers_ones, -1);
    kpb_test_integers_le_set_integer_u8(&integers_ones, 0xffffffffffffffff);
    EXPECT_EQUAL(kpb_test_integers_le_integer_s1(&integers_ones), -1);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u1(&integers_ones), 0xff);
    EXPECT_EQUAL(kpb_test_integers_le_integer_s2(&integers_ones), -1);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u2(&integers_ones), 0xffff);
    EXPECT_EQUAL(kpb_test_integers_le_integer_s4(&integers_ones), -1);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u4(&integers_ones), 0xffffffff);
    EXPECT_EQUAL(kpb_test_integers_le_integer_s8(&integers_ones), -1);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u8(&integers_ones), 0xffffffffffffffff);
    size_t expected_ones_size = 0;
    char *expected_ones = kpb_test_integers_le_serialize_to_string(&integers_ones, &expected_ones_size);
    EXPECT_NOT_NULL(expected_ones);
    EXPECT_BYTES_EQUAL(expected_ones, expected_ones_size, BYTES_ONES_30, BYTES_ONES_30_SIZE);
    kpb_io_free(expected_ones);

    kpb_test_integers_le_clear(&integers);
    EXPECT_EQUAL(kpb_test_integers_le_integer_s1(&integers), 0);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u1(&integers), 0);
    EXPECT_EQUAL(kpb_test_integers_le_integer_s2(&integers), 0);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u2(&integers), 0);
    EXPECT_EQUAL(kpb_test_integers_le_integer_s4(&integers), 0);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u4(&integers), 0);
    EXPECT_EQUAL(kpb_test_integers_le_integer_s8(&integers), 0);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u8(&integers), 0);
    EXPECT_TRUE(kpb_test_integers_le_parse_from_string(&integers, BYTES_ONES_30, BYTES_ONES_30_SIZE));
    EXPECT_EQUAL(kpb_test_integers_le_integer_s1(&integers), -1);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u1(&integers), 0xff);
    EXPECT_EQUAL(kpb_test_integers_le_integer_s2(&integers), -1);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u2(&integers), 0xffff);
    EXPECT_EQUAL(kpb_test_integers_le_integer_s4(&integers), -1);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u4(&integers), 0xffffffff);
    EXPECT_EQUAL(kpb_test_integers_le_integer_s8(&integers), -1);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u8(&integers), 0xffffffffffffffff);
    EXPECT_TRUE(kpb_test_integers_le_parse_from_string(&integers, BYTES_ZERO_30, BYTES_ZERO_30_SIZE));
    EXPECT_EQUAL(kpb_test_integers_le_integer_s1(&integers), 0);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u1(&integers), 0);
    EXPECT_EQUAL(kpb_test_integers_le_integer_s2(&integers), 0);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u2(&integers), 0);
    EXPECT_EQUAL(kpb_test_integers_le_integer_s4(&integers), 0);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u4(&integers), 0);
    EXPECT_EQUAL(kpb_test_integers_le_integer_s8(&integers), 0);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u8(&integers), 0);

    kpb_test_integers_be_t be;
    kpb_test_integers_be_init(&be);
    kpb_test_integers_le_t le;
    kpb_test_integers_le_init(&le);
    STACK_BUFFER(expected_be, 512);
    STACK_BUFFER(expected_le, 512);
    kpb_test_integers_be_set_integer_s1(&be, -54);
    kpb_test_integers_le_set_integer_s1(&le, -54);
    STACK_BUFFER_APPEND(expected_be, "\xca", 1);
    STACK_BUFFER_APPEND(expected_le, "\xca", 1);
    kpb_test_integers_be_set_integer_u1(&be, 0xfe);
    kpb_test_integers_le_set_integer_u1(&le, 0xfe);
    STACK_BUFFER_APPEND(expected_be, "\xfe", 1);
    STACK_BUFFER_APPEND(expected_le, "\xfe", 1);
    kpb_test_integers_be_set_integer_s2(&be, -293);
    kpb_test_integers_le_set_integer_s2(&le, -293);
    STACK_BUFFER_APPEND(expected_be, "\xfe\xdb", 2);
    STACK_BUFFER_APPEND(expected_le, "\xdb\xfe", 2);
    kpb_test_integers_be_set_integer_u2(&be, 0xc234);
    kpb_test_integers_le_set_integer_u2(&le, 0xc234);
    STACK_BUFFER_APPEND(expected_be, "\xc2\x34", 2);
    STACK_BUFFER_APPEND(expected_le, "\x34\xc2", 2);
    kpb_test_integers_be_set_integer_s4(&be, -2023406815);
    kpb_test_integers_le_set_integer_s4(&le, -2023406815);
    STACK_BUFFER_APPEND(expected_be, "\x87\x65\x43\x21", 4);
    STACK_BUFFER_APPEND(expected_le, "\x21\x43\x65\x87", 4);
    kpb_test_integers_be_set_integer_u4(&be, 0x98765432);
    kpb_test_integers_le_set_integer_u4(&le, 0x98765432);
    STACK_BUFFER_APPEND(expected_be, "\x98\x76\x54\x32", 4);
    STACK_BUFFER_APPEND(expected_le, "\x32\x54\x76\x98", 4);
    kpb_test_integers_be_set_integer_s8(&be, -8690466096900485649);
    kpb_test_integers_le_set_integer_s8(&le, -8690466096900485649);
    STACK_BUFFER_APPEND(expected_be, "\x87\x65\x43\x21\x01\xab\xcd\xef", 8);
    STACK_BUFFER_APPEND(expected_le, "\xef\xcd\xab\x01\x21\x43\x65\x87", 8);
    kpb_test_integers_be_set_integer_u8(&be, 0xfedcba9876543210);
    kpb_test_integers_le_set_integer_u8(&le, 0xfedcba9876543210);
    STACK_BUFFER_APPEND(expected_be, "\xfe\xdc\xba\x98\x76\x54\x32\x10", 8);
    STACK_BUFFER_APPEND(expected_le, "\x10\x32\x54\x76\x98\xba\xdc\xfe", 8);
    EXPECT_EQUAL(STACK_BUFFER_SIZE(expected_be), 30);
    EXPECT_EQUAL(STACK_BUFFER_SIZE(expected_le), 30);

    size_t result_be_size = 0;
    char *result_be = kpb_test_integers_be_serialize_to_string(&be, &result_be_size);
    size_t result_le_size = 0;
    char *result_le = kpb_test_integers_le_serialize_to_string(&le, &result_le_size);
    EXPECT_NOT_NULL(result_be);
    EXPECT_NOT_NULL(result_le);
    EXPECT_BYTES_EQUAL(result_be, result_be_size, STACK_BUFFER_GET(expected_be), STACK_BUFFER_SIZE(expected_be));
    EXPECT_BYTES_EQUAL(result_le, result_le_size, STACK_BUFFER_GET(expected_le), STACK_BUFFER_SIZE(expected_le));
    kpb_test_integers_be_clear(&be);
    kpb_test_integers_le_clear(&le);
    EXPECT_TRUE(kpb_test_integers_be_parse_from_string(&be, result_be, result_be_size));
    EXPECT_EQUAL(kpb_test_integers_be_integer_s1(&be), -54);
    EXPECT_EQUAL(kpb_test_integers_be_integer_u1(&be), 0xfe);
    EXPECT_EQUAL(kpb_test_integers_be_integer_s2(&be), -293);
    EXPECT_EQUAL(kpb_test_integers_be_integer_u2(&be), 0xc234);
    EXPECT_EQUAL(kpb_test_integers_be_integer_s4(&be), -2023406815);
    EXPECT_EQUAL(kpb_test_integers_be_integer_u4(&be), 0x98765432);
    EXPECT_EQUAL(kpb_test_integers_be_integer_s8(&be), -8690466096900485649);
    EXPECT_EQUAL(kpb_test_integers_be_integer_u8(&be), 0xfedcba9876543210);
    EXPECT_TRUE(kpb_test_integers_le_parse_from_string(&le, result_le, result_le_size));
    EXPECT_EQUAL(kpb_test_integers_le_integer_s1(&le), -54);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u1(&le), 0xfe);
    EXPECT_EQUAL(kpb_test_integers_le_integer_s2(&le), -293);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u2(&le), 0xc234);
    EXPECT_EQUAL(kpb_test_integers_le_integer_s4(&le), -2023406815);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u4(&le), 0x98765432);
    EXPECT_EQUAL(kpb_test_integers_le_integer_s8(&le), -8690466096900485649);
    EXPECT_EQUAL(kpb_test_integers_le_integer_u8(&le), 0xfedcba9876543210);
    kpb_io_free(result_be);
    kpb_io_free(result_le);

    kpb_test_integers_le_fini(&integers);
    kpb_test_integers_le_fini(&integers_zeros);
    kpb_test_integers_le_fini(&integers_ones);
    kpb_test_integers_be_fini(&be);
    kpb_test_integers_le_fini(&le);

    kpb_test_integers_t intergers_mixed;
    kpb_test_integers_init(&intergers_mixed);
    kpb_test_integers_set_integer_s2le(&intergers_mixed, 0x1234);
    kpb_test_integers_set_integer_u2le(&intergers_mixed, 0x1234);
    kpb_test_integers_set_integer_s4le(&intergers_mixed, 0x12345678);
    kpb_test_integers_set_integer_u4le(&intergers_mixed, 0x12345678);
    kpb_test_integers_set_integer_s8le(&intergers_mixed, 0x1234567890abcdef);
    kpb_test_integers_set_integer_u8le(&intergers_mixed, 0x1234567890abcdef);
    kpb_test_integers_set_integer_s2be(&intergers_mixed, 0x1234);
    kpb_test_integers_set_integer_u2be(&intergers_mixed, 0x1234);
    kpb_test_integers_set_integer_s4be(&intergers_mixed, 0x12345678);
    kpb_test_integers_set_integer_u4be(&intergers_mixed, 0x12345678);
    kpb_test_integers_set_integer_s8be(&intergers_mixed, 0x1234567890abcdef);
    kpb_test_integers_set_integer_u8be(&intergers_mixed, 0x1234567890abcdef);
    size_t result_size = 0;
    char *result = kpb_test_integers_serialize_to_string(&intergers_mixed, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(result, result_size,
        "\x34\x12" "\x34\x12" \
        "\x78\x56\x34\x12" "\x78\x56\x34\x12" \
        "\xef\xcd\xab\x90\x78\x56\x34\x12" "\xef\xcd\xab\x90\x78\x56\x34\x12" \
        "\x12\x34" "\x12\x34" \
        "\x12\x34\x56\x78" "\x12\x34\x56\x78" \
        "\x12\x34\x56\x78\x90\xab\xcd\xef" "\x12\x34\x56\x78\x90\xab\xcd\xef",
        (2 + 4 + 8) * 4);
    kpb_io_free(result);
    kpb_test_integers_clear(&intergers_mixed);
    EXPECT_TRUE(kpb_test_integers_parse_from_string(&intergers_mixed,
        "\x34\x12" "\x34\x12" \
        "\x78\x56\x34\x12" "\x78\x56\x34\x12" \
        "\xef\xcd\xab\x90\x78\x56\x34\x12" "\xef\xcd\xab\x90\x78\x56\x34\x12" \
        "\x12\x34" "\x12\x34" \
        "\x12\x34\x56\x78" "\x12\x34\x56\x78" \
        "\x12\x34\x56\x78\x90\xab\xcd\xef" "\x12\x34\x56\x78\x90\xab\xcd\xef",
        (2 + 4 + 8) * 4));
    EXPECT_EQUAL(0x1234, kpb_test_integers_integer_s2le(&intergers_mixed));
    EXPECT_EQUAL(0x1234, kpb_test_integers_integer_u2le(&intergers_mixed));
    EXPECT_EQUAL(0x12345678, kpb_test_integers_integer_s4le(&intergers_mixed));
    EXPECT_EQUAL(0x12345678, kpb_test_integers_integer_u4le(&intergers_mixed));
    EXPECT_EQUAL(0x1234567890abcdef, kpb_test_integers_integer_s8le(&intergers_mixed));
    EXPECT_EQUAL(0x1234567890abcdef, kpb_test_integers_integer_u8le(&intergers_mixed));
    EXPECT_EQUAL(0x1234, kpb_test_integers_integer_s2be(&intergers_mixed));
    EXPECT_EQUAL(0x1234, kpb_test_integers_integer_u2be(&intergers_mixed));
    EXPECT_EQUAL(0x12345678, kpb_test_integers_integer_s4be(&intergers_mixed));
    EXPECT_EQUAL(0x12345678, kpb_test_integers_integer_u4be(&intergers_mixed));
    EXPECT_EQUAL(0x1234567890abcdef, kpb_test_integers_integer_s8be(&intergers_mixed));
    EXPECT_EQUAL(0x1234567890abcdef, kpb_test_integers_integer_u8be(&intergers_mixed));
    kpb_test_integers_fini(&intergers_mixed);
    return 0;
}
