#include "process_value_f4.h"

#include <kpb_io.h>

void process_value_f4_init(struct process_value_f4 *_this, bool multiple_float_by_4)
{
    memset(_this, 0, sizeof(*_this));
    _this->multiple_float_by_4_ = multiple_float_by_4;
}

float process_value_f4_getter(struct process_value_f4 *_this, float value)
{
    return _this->multiple_float_by_4_ ? (value * 4.0f) : value;
}

float process_value_f4_setter(struct process_value_f4 *_this, float value)
{
    return _this->multiple_float_by_4_ ? (value * 4.0f) : value;
}

void process_value_f4_fini(struct process_value_f4 *_this)
{
    memset(_this, 0, sizeof(*_this));
}
