#include "xor_encoding.h"

#include <kpb_io.h>

void xor_encoding_init(struct xor_encoding *_this, uint8_t xor_key, const kpb_test_process_config_t *config)
{
    memset(_this, 0, sizeof(*_this));
    _this->xor_key = xor_key;
    _this->config = config;
}

size_t xor_encoding_encode_size(struct xor_encoding *_this, struct kpb_io *_io)
{
    // to calculate the encoded size, we will just encode the buffer and then get the size
    if ( !xor_encoding_encode(_this, _io) )
    {
        return 0;
    }
    return kpb_io_size(_io);
}

bool xor_encoding_encode(struct xor_encoding *_this, struct kpb_io *_io)
{
    size_t alignment = kpb_test_process_config_alignment(_this->config);
    if ( alignment == 0 )
    {
        // no divide by zero please
        alignment = 1;
    }

    // get a copy of the buffer in the stream
    size_t in_buffer_size = 0;
    char *in_buffer = kpb_io_get(_io, &in_buffer_size);  // this will return a new allocation
    if ( !in_buffer )
    {
        return false;
    }

    // create a buffer that will be big enough to hold the encoded output
    size_t out_buffer_size = in_buffer_size;
    if ( ( out_buffer_size % alignment ) != 0 )
    {
        out_buffer_size += ( alignment - ( out_buffer_size % alignment ) );
    }
    char *out_buffer = kpb_io_malloc(out_buffer_size);
    if ( !out_buffer )
    {
        kpb_io_free(in_buffer);
        return false;
    }

    // place the input buffer into a larger buffer (with an aligned size)
    memcpy(out_buffer, in_buffer, in_buffer_size);
    memset(out_buffer + in_buffer_size, 0, out_buffer_size - in_buffer_size);
    kpb_io_free(in_buffer);
    in_buffer = NULL;
    in_buffer_size = 0;

    // xor encode
    for ( size_t i = 0; i < out_buffer_size; i++ )
    {
        ((uint8_t*)out_buffer)[i] ^= _this->xor_key;
    }

    // set the encode buffer
    if ( !kpb_io_set(_io, out_buffer, out_buffer_size) )
    {
        kpb_io_free(out_buffer);
        return false;
    }

    // free output buffer
    kpb_io_free(out_buffer);
    out_buffer = NULL;
    out_buffer_size = 0;

    // done
    return true;
}

bool xor_encoding_decode(struct xor_encoding *_this, struct kpb_io *_io)
{
    // check that buffer is correctly aligned
    size_t alignment = kpb_test_process_config_alignment(_this->config);
    if ( alignment == 0 )
    {
        // no divide by zero please
        alignment = 1;
    }

    // get a copy of the input buffer
    size_t buffer_size = 0;
    char * buffer = kpb_io_get(_io, &buffer_size);
    if ( !buffer )
    {
        return false;
    }

    // check buffer is correctly aligned
    if ( ( buffer_size % alignment) != 0 )
    {
        kpb_io_free(buffer);
        return false;
    }

    // xor decode
    for ( size_t i = 0; i < buffer_size; i++ )
    {
        ((uint8_t*)buffer)[i] ^= _this->xor_key;
    }

    // set the decoded buffer
    if ( !kpb_io_set(_io, buffer, buffer_size) )
    {
        kpb_io_free(buffer);
        return false;
    }

    // done
    kpb_io_free(buffer);
    return true;
}

void xor_encoding_fini(struct xor_encoding *_this)
{
    memset(_this, 0, sizeof(*_this));
}
