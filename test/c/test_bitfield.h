#ifndef _H_KPB_TEST_BITFIELD_H_
#define _H_KPB_TEST_BITFIELD_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_bitfield(void);

#ifdef __cplusplus
};
#endif

#endif
