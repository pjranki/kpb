#include "test_process.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/process.h"

#define HELLOWORLD_ALIGN32_BUFFER \
    "\x20\x00\x00\x00\x20\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00" \
    "\xe2\xcf\xc6\xc6\xc5\xfd\xc5\xd8\xc6\xce\x8b\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
#define HELLOWORLD_ALIGN32_BUFFER_SIZE (sizeof(HELLOWORLD_ALIGN32_BUFFER)-1)

#define HELLOWORLD_ALIGN8_BUFFER \
    "\x08\x00\x00\x00\x00\x00\x00\x00" \
    "\xe2\xcf\xc6\xc6\xc5\xfd\xc5\xd8\xc6\xce\x8b\xaa\xaa\xaa\xaa\xaa"
#define HELLOWORLD_ALIGN8_BUFFER_SIZE (sizeof(HELLOWORLD_ALIGN8_BUFFER)-1)


int kpb_test_process(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_process_body_size__t process;
    kpb_test_process_config_t *config = NULL;
    kpb_test_process_body_t *body = NULL;
    kpb_test_process_body_size__init(&process);
    config = kpb_test_process_body_size__mutable_config(&process);
    EXPECT_NOT_NULL(config);
    EXPECT_TRUE(kpb_test_process_config_set_alignment(config, 32));
    body = kpb_test_process_body_size__mutable_body(&process);
    EXPECT_NOT_NULL(body);
    EXPECT_TRUE(kpb_test_process_body_set_message(body, "HelloWorld!"));
    EXPECT_EQUAL(32, kpb_test_process_body_size__body_size_(&process));
    result_size = 0;
    result = kpb_test_process_body_size__serialize_to_string(&process, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(
        HELLOWORLD_ALIGN32_BUFFER,
        HELLOWORLD_ALIGN32_BUFFER_SIZE,
        result,
        result_size
    );
    kpb_io_free(result);
    kpb_test_process_body_size__fini(&process);

    kpb_test_process_body_size__init(&process);
    kpb_test_process_body_size__clear(&process);
    config = kpb_test_process_body_size__mutable_config(&process);
    EXPECT_EQUAL(0, kpb_test_process_config_alignment(config));
    body = kpb_test_process_body_size__mutable_body(&process);
    EXPECT_STRING_EQUAL("", kpb_test_process_body_message(body));
    EXPECT_EQUAL(1, kpb_test_process_body_size__body_size_(&process));
    EXPECT_TRUE(kpb_test_process_body_size__parse_from_string(&process, HELLOWORLD_ALIGN32_BUFFER, HELLOWORLD_ALIGN32_BUFFER_SIZE));
    config = kpb_test_process_body_size__mutable_config(&process);
    EXPECT_EQUAL(32, kpb_test_process_config_alignment(config));
    body = kpb_test_process_body_size__mutable_body(&process);
    EXPECT_STRING_EQUAL("HelloWorld!", kpb_test_process_body_message(body));
    EXPECT_EQUAL(32, kpb_test_process_body_size__body_size_(&process));
    kpb_test_process_body_size__fini(&process);

    kpb_test_process_body_size_eos_t process_eos;
    kpb_test_process_body_size_eos_init(&process_eos);
    config = kpb_test_process_body_size_eos_mutable_config(&process_eos);
    EXPECT_NOT_NULL(config);
    EXPECT_TRUE(kpb_test_process_config_set_alignment(config, 8));
    body = kpb_test_process_body_size_eos_mutable_body(&process_eos);
    EXPECT_NOT_NULL(body);
    EXPECT_TRUE(kpb_test_process_body_set_message(body, "HelloWorld!"));
    result_size = 0;
    result = kpb_test_process_body_size_eos_serialize_to_string(&process_eos, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(
        HELLOWORLD_ALIGN8_BUFFER,
        HELLOWORLD_ALIGN8_BUFFER_SIZE,
        result,
        result_size
    );
    kpb_io_free(result);
    kpb_test_process_body_size_eos_fini(&process_eos);

    kpb_test_process_body_size_eos_init(&process_eos);
    kpb_test_process_body_size_eos_clear(&process_eos);
    config = kpb_test_process_body_size_eos_mutable_config(&process_eos);
    EXPECT_EQUAL(0, kpb_test_process_config_alignment(config));
    body = kpb_test_process_body_size_eos_mutable_body(&process_eos);
    EXPECT_STRING_EQUAL("", kpb_test_process_body_message(body));
    EXPECT_TRUE(kpb_test_process_body_size_eos_parse_from_string(&process_eos, HELLOWORLD_ALIGN8_BUFFER, HELLOWORLD_ALIGN8_BUFFER_SIZE));
    config = kpb_test_process_body_size_eos_mutable_config(&process_eos);
    EXPECT_EQUAL(8, kpb_test_process_config_alignment(config));
    body = kpb_test_process_body_size_eos_mutable_body(&process_eos);
    EXPECT_STRING_EQUAL("HelloWorld!", kpb_test_process_body_message(body));
    kpb_test_process_body_size_eos_fini(&process_eos);

    return 0;
}
