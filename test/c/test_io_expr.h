#ifndef _H_KPB_TEST_IO_EXPR_H_
#define _H_KPB_TEST_IO_EXPR_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_io_expr(void);

#ifdef __cplusplus
};
#endif

#endif
