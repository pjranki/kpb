#include "test_params.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/params.h"

int kpb_test_params(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_params_t params;
    kpb_test_param_object2_t *obj2 = NULL;
    kpb_test_param_object3_t *obj3 = NULL;
    kpb_test_params_init(&params);
    result_size = 0;
    result = kpb_test_params_serialize_to_string(&params, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 16, result, result_size);
    kpb_io_free(result);
    obj2 = kpb_test_params_mutable_data2(&params);
    EXPECT_NOT_NULL(obj2);
    EXPECT_EQUAL(0xcafe, obj2->arg1);
    obj3 = kpb_test_params_add_data3(&params);
    EXPECT_NOT_NULL(obj3);
    EXPECT_EQUAL(42, obj3->arg1);
    EXPECT_EQUAL(0xbeef, obj3->arg2);
    obj3 = kpb_test_params_add_data3(&params);
    EXPECT_NOT_NULL(obj3);
    EXPECT_EQUAL(42, obj3->arg1);
    EXPECT_EQUAL(0xbeef, obj3->arg2);
    EXPECT_TRUE(kpb_test_params_parse_from_string(&params, "\x01\x00\x00\x00\x02\x00\x00\x00\x03\x00\x00\x00\x04\x00\x00\x00", 16));
    EXPECT_EQUAL(1, params.data1->value1);
    EXPECT_EQUAL(2, params.data2->value1);
    EXPECT_EQUAL(0xcafe, params.data2->arg1);
    EXPECT_EQUAL(2, kpb_test_params_data3_size(&params));
    EXPECT_EQUAL(3, params.data3[0]->value1);
    EXPECT_EQUAL(42, params.data3[0]->arg1);
    EXPECT_EQUAL(0xbeef, params.data3[0]->arg2);
    EXPECT_EQUAL(4, params.data3[1]->value1);
    EXPECT_EQUAL(42, params.data3[1]->arg1);
    EXPECT_EQUAL(0xbeef, params.data3[1]->arg2);

    // test that list size changes AFTER parameter values are set
    kpb_test_params_clear(&params);
    obj3 = kpb_test_params_add_data3(&params);
    EXPECT_NOT_NULL(obj3);
    EXPECT_EQUAL(0, kpb_test_param_object3_index(obj3));
    EXPECT_EQUAL(1, kpb_test_param_object3_current_count(obj3));
    obj3 = kpb_test_params_add_data3(&params);
    EXPECT_NOT_NULL(obj3);
    EXPECT_EQUAL(1, kpb_test_param_object3_index(obj3));
    EXPECT_EQUAL(2, kpb_test_param_object3_current_count(obj3));

    kpb_test_params_fini(&params);
    return 0;
}
