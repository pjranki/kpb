#include "test_object_align.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/object_align.h"

int kpb_test_object_align(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_object_align_t obj;
    kpb_test_object_b4_t *sub_obj = NULL;
    kpb_test_object_align_init(&obj);

    EXPECT_TRUE(kpb_test_object_align_set_value_1(&obj, 0xa));
    sub_obj = kpb_test_object_align_mutable_object_b4(&obj);
    EXPECT_NOT_NULL(sub_obj);
    EXPECT_TRUE(kpb_test_object_b4_set_value(sub_obj, 0xc));
    EXPECT_TRUE(kpb_test_object_align_set_value_2(&obj, 0x5));
    EXPECT_TRUE(kpb_test_object_align_add_value_list(&obj, 0x1));
    EXPECT_TRUE(kpb_test_object_align_add_value_list(&obj, 0x2));
    EXPECT_TRUE(kpb_test_object_align_add_value_list(&obj, 0x3));
    EXPECT_TRUE(kpb_test_object_align_add_value_list(&obj, 0x4));

    result_size = 0;
    result = kpb_test_object_align_serialize_to_string(&obj, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xa0\xc0\x51\x23\x40", 5, result, result_size);
    kpb_io_free(result);

    kpb_test_object_align_clear(&obj);
    EXPECT_TRUE(kpb_test_object_align_parse_from_string(&obj, "\x50\x30\xa4\x32\x10", 5));
    EXPECT_EQUAL(0x5, obj.value_1);
    EXPECT_EQUAL(0x3, obj.object_b4->value);
    EXPECT_EQUAL(0xa, obj.value_2);
    EXPECT_EQUAL(0x4, obj.value_list[0]);
    EXPECT_EQUAL(0x3, obj.value_list[1]);
    EXPECT_EQUAL(0x2, obj.value_list[2]);
    EXPECT_EQUAL(0x1, obj.value_list[3]);

    kpb_test_object_align_fini(&obj);
    return 0;
}
