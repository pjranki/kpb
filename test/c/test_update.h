#ifndef _H_KPB_TEST_UPDATE_H_
#define _H_KPB_TEST_UPDATE_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_update(void);

#ifdef __cplusplus
};
#endif

#endif
