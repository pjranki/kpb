#include "process_value_u4.h"

#include <kpb_io.h>

void process_value_u4_init(struct process_value_u4 *_this)
{
    memset(_this, 0, sizeof(*_this));
}

uint32_t process_value_u4_getter(struct process_value_u4 *_this, uint32_t value)
{
    (void)_this;
    return value + 10;
}

uint32_t process_value_u4_setter(struct process_value_u4 *_this, uint32_t value)
{
    (void)_this;
    return value - 5;
}

void process_value_u4_fini(struct process_value_u4 *_this)
{
    memset(_this, 0, sizeof(*_this));
}
