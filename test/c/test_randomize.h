#ifndef _H_KPB_TEST_RANDOMIZE_H_
#define _H_KPB_TEST_RANDOMIZE_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_randomize(void);

#ifdef __cplusplus
};
#endif

#endif
