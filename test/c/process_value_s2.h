#ifndef _H_PROCESS_VALUE_S2_H_
#define _H_PROCESS_VALUE_S2_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <string.h>

#include <kpb_io.h>

#include "c/process_value.h"

struct process_value_s2 {
    int unused;
};

void process_value_s2_init(struct process_value_s2 *_this);
int16_t process_value_s2_getter(struct process_value_s2 *_this, int16_t value);
int16_t process_value_s2_setter(struct process_value_s2 *_this, int16_t value);
void process_value_s2_fini(struct process_value_s2 *_this);

#ifdef __cplusplus
};
#endif

#endif
