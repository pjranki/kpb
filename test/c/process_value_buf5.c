#include "process_value_buf5.h"

#include <kpb_io.h>

void process_value_buf5_init(struct process_value_buf5 *_this, uint8_t memset_value)
{
    memset(_this, 0, sizeof(*_this));
    _this->memset_value_ = memset_value;
}

const char *process_value_buf5_getter(struct process_value_buf5 *_this, const char *value /* TODO: this should have size */)
{
    // TODO
    (void)_this;
    return value;
}

char *process_value_buf5_setter(struct process_value_buf5 *_this, char *value, size_t size)
{
    // TODO
    (void)_this;
    (void)size;
    return value;
}

void process_value_buf5_fini(struct process_value_buf5 *_this)
{
    memset(_this, 0, sizeof(*_this));
}
