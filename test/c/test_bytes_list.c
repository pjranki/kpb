#include "test_bytes_list.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/bytes_list.h"


int kpb_test_bytes_list(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_bytes_list_t bytes_list;
    kpb_test_bytes_list_init(&bytes_list);
    EXPECT_EQUAL(0, kpb_test_bytes_list_data2_list_size(&bytes_list));
    EXPECT_EQUAL(0, kpb_test_bytes_list_data4_list_size(&bytes_list));
    EXPECT_EQUAL(0, bytes_list._data2_list_size);
    EXPECT_EQUAL(0, bytes_list._data4_list_size);
    result = kpb_test_bytes_list_serialize_to_string(&bytes_list, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 30, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_bytes_list_add_data2_list(&bytes_list, "A", 1));
    EXPECT_TRUE(kpb_test_bytes_list_add_data4_list(&bytes_list, "a", 1));
    EXPECT_EQUAL(1, kpb_test_bytes_list_data2_list_size(&bytes_list));
    EXPECT_EQUAL(1, kpb_test_bytes_list_data4_list_size(&bytes_list));
    EXPECT_EQUAL(1, bytes_list._data2_list_size);
    EXPECT_EQUAL(1, bytes_list._data4_list_size);
    EXPECT_BYTES_EQUAL("A", 1, bytes_list.data2_list[0], bytes_list._data2_list_sizeof[0]);
    EXPECT_BYTES_EQUAL("A", 1, kpb_test_bytes_list_data2_list_at(&bytes_list, 0), kpb_test_bytes_list_data2_list_size_at(&bytes_list, 0));
    EXPECT_BYTES_EQUAL("a", 1, bytes_list.data4_list[0], bytes_list._data4_list_sizeof[0]);
    EXPECT_BYTES_EQUAL("a", 1, kpb_test_bytes_list_data4_list_at(&bytes_list, 0), kpb_test_bytes_list_data4_list_size_at(&bytes_list, 0));
    result = kpb_test_bytes_list_serialize_to_string(&bytes_list, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x41\x00\x00\x00\x00\x00\x00\x00\x00\x00\x61\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 30, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_bytes_list_add_data2_list(&bytes_list, "BC", 2));
    EXPECT_TRUE(kpb_test_bytes_list_add_data4_list(&bytes_list, "bc", 2));
    EXPECT_EQUAL(2, kpb_test_bytes_list_data2_list_size(&bytes_list));
    EXPECT_EQUAL(2, kpb_test_bytes_list_data4_list_size(&bytes_list));
    EXPECT_EQUAL(2, bytes_list._data2_list_size);
    EXPECT_EQUAL(2, bytes_list._data4_list_size);
    EXPECT_BYTES_EQUAL("A", 1, bytes_list.data2_list[0], bytes_list._data2_list_sizeof[0]);
    EXPECT_BYTES_EQUAL("A", 1, kpb_test_bytes_list_data2_list_at(&bytes_list, 0), kpb_test_bytes_list_data2_list_size_at(&bytes_list, 0));
    EXPECT_BYTES_EQUAL("BC", 2, bytes_list.data2_list[1], bytes_list._data2_list_sizeof[1]);
    EXPECT_BYTES_EQUAL("BC", 2, kpb_test_bytes_list_data2_list_at(&bytes_list, 1), kpb_test_bytes_list_data2_list_size_at(&bytes_list, 1));
    EXPECT_BYTES_EQUAL("a", 1, bytes_list.data4_list[0], bytes_list._data4_list_sizeof[0]);
    EXPECT_BYTES_EQUAL("a", 1, kpb_test_bytes_list_data4_list_at(&bytes_list, 0), kpb_test_bytes_list_data4_list_size_at(&bytes_list, 0));
    EXPECT_BYTES_EQUAL("bc", 2, bytes_list.data4_list[1], bytes_list._data4_list_sizeof[1]);
    EXPECT_BYTES_EQUAL("bc", 2, kpb_test_bytes_list_data4_list_at(&bytes_list, 1), kpb_test_bytes_list_data4_list_size_at(&bytes_list, 1));
    result = kpb_test_bytes_list_serialize_to_string(&bytes_list, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x41\x00\x42\x43\x00\x00\x00\x00\x00\x00\x61\x00\x00\x00\x62\x63\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 30, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_bytes_list_add_data2_list(&bytes_list, "DEF", 3));
    EXPECT_TRUE(kpb_test_bytes_list_add_data2_list(&bytes_list, "DEF", 3));
    EXPECT_TRUE(kpb_test_bytes_list_add_data2_list(&bytes_list, "DEF", 3));
    EXPECT_TRUE(kpb_test_bytes_list_add_data4_list(&bytes_list, "def", 3));
    EXPECT_TRUE(kpb_test_bytes_list_add_data4_list(&bytes_list, "def", 3));
    EXPECT_TRUE(kpb_test_bytes_list_add_data4_list(&bytes_list, "def", 3));
    EXPECT_EQUAL(5, kpb_test_bytes_list_data2_list_size(&bytes_list));
    EXPECT_EQUAL(5, kpb_test_bytes_list_data4_list_size(&bytes_list));
    EXPECT_EQUAL(5, bytes_list._data2_list_size);
    EXPECT_EQUAL(5, bytes_list._data4_list_size);
    EXPECT_BYTES_EQUAL("A", 1, bytes_list.data2_list[0], bytes_list._data2_list_sizeof[0]);
    EXPECT_BYTES_EQUAL("BC", 2, bytes_list.data2_list[1], bytes_list._data2_list_sizeof[1]);
    EXPECT_BYTES_EQUAL("DEF", 3, bytes_list.data2_list[2], bytes_list._data2_list_sizeof[2]);
    EXPECT_BYTES_EQUAL("DEF", 3, bytes_list.data2_list[3], bytes_list._data2_list_sizeof[3]);
    EXPECT_BYTES_EQUAL("DEF", 3, bytes_list.data2_list[4], bytes_list._data2_list_sizeof[4]);
    EXPECT_BYTES_EQUAL("a", 1, bytes_list.data4_list[0], bytes_list._data4_list_sizeof[0]);
    EXPECT_BYTES_EQUAL("bc", 2, bytes_list.data4_list[1], bytes_list._data4_list_sizeof[1]);
    EXPECT_BYTES_EQUAL("def", 3, bytes_list.data4_list[2], bytes_list._data4_list_sizeof[2]);
    EXPECT_BYTES_EQUAL("def", 3, bytes_list.data4_list[3], bytes_list._data4_list_sizeof[3]);
    EXPECT_BYTES_EQUAL("def", 3, bytes_list.data4_list[4], bytes_list._data4_list_sizeof[4]);
    result = kpb_test_bytes_list_serialize_to_string(&bytes_list, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x41\x00\x42\x43\x44\x45\x44\x45\x44\x45\x61\x00\x00\x00\x62\x63\x00\x00\x64\x65\x66\x00\x64\x65\x66\x00\x64\x65\x66\x00", 30, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_bytes_list_add_data2_list(&bytes_list, "G", 1));
    EXPECT_TRUE(kpb_test_bytes_list_add_data4_list(&bytes_list, "g", 1));
    EXPECT_EQUAL(6, kpb_test_bytes_list_data2_list_size(&bytes_list));
    EXPECT_EQUAL(6, kpb_test_bytes_list_data4_list_size(&bytes_list));
    EXPECT_EQUAL(6, bytes_list._data2_list_size);
    EXPECT_EQUAL(6, bytes_list._data4_list_size);
    EXPECT_BYTES_EQUAL("A", 1, bytes_list.data2_list[0], bytes_list._data2_list_sizeof[0]);
    EXPECT_BYTES_EQUAL("BC", 2, bytes_list.data2_list[1], bytes_list._data2_list_sizeof[1]);
    EXPECT_BYTES_EQUAL("DEF", 3, bytes_list.data2_list[2], bytes_list._data2_list_sizeof[2]);
    EXPECT_BYTES_EQUAL("DEF", 3, bytes_list.data2_list[3], bytes_list._data2_list_sizeof[3]);
    EXPECT_BYTES_EQUAL("DEF", 3, bytes_list.data2_list[4], bytes_list._data2_list_sizeof[4]);
    EXPECT_BYTES_EQUAL("G", 1, bytes_list.data2_list[5], bytes_list._data2_list_sizeof[5]);
    EXPECT_BYTES_EQUAL("a", 1, bytes_list.data4_list[0], bytes_list._data4_list_sizeof[0]);
    EXPECT_BYTES_EQUAL("bc", 2, bytes_list.data4_list[1], bytes_list._data4_list_sizeof[1]);
    EXPECT_BYTES_EQUAL("def", 3, bytes_list.data4_list[2], bytes_list._data4_list_sizeof[2]);
    EXPECT_BYTES_EQUAL("def", 3, bytes_list.data4_list[3], bytes_list._data4_list_sizeof[3]);
    EXPECT_BYTES_EQUAL("def", 3, bytes_list.data4_list[4], bytes_list._data4_list_sizeof[4]);
    EXPECT_BYTES_EQUAL("g", 1, bytes_list.data4_list[5], bytes_list._data4_list_sizeof[5]);
    result = kpb_test_bytes_list_serialize_to_string(&bytes_list, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x41\x00\x42\x43\x44\x45\x44\x45\x44\x45\x61\x00\x00\x00\x62\x63\x00\x00\x64\x65\x66\x00\x64\x65\x66\x00\x64\x65\x66\x00", 30, result, result_size);
    kpb_io_free(result);

    kpb_test_bytes_list_clear(&bytes_list);
    EXPECT_TRUE(kpb_test_bytes_list_parse_from_string(&bytes_list, "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 30));
    EXPECT_EQUAL(5, kpb_test_bytes_list_data2_list_size(&bytes_list));
    EXPECT_EQUAL(5, kpb_test_bytes_list_data4_list_size(&bytes_list));
    EXPECT_EQUAL(5, bytes_list._data2_list_size);
    EXPECT_EQUAL(5, bytes_list._data4_list_size);
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list[0], bytes_list._data2_list_sizeof[0]);
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list[1], bytes_list._data2_list_sizeof[1]);
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list[2], bytes_list._data2_list_sizeof[2]);
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list[3], bytes_list._data2_list_sizeof[3]);
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list[4], bytes_list._data2_list_sizeof[4]);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list[0], bytes_list._data4_list_sizeof[0]);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list[1], bytes_list._data4_list_sizeof[1]);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list[2], bytes_list._data4_list_sizeof[2]);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list[3], bytes_list._data4_list_sizeof[3]);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list[4], bytes_list._data4_list_sizeof[4]);
    kpb_test_bytes_list_clear(&bytes_list);
    EXPECT_TRUE(kpb_test_bytes_list_parse_from_string(&bytes_list, "\x41\x00\x00\x00\x00\x00\x00\x00\x00\x00\x61\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 30));
    EXPECT_EQUAL(5, kpb_test_bytes_list_data2_list_size(&bytes_list));
    EXPECT_EQUAL(5, kpb_test_bytes_list_data4_list_size(&bytes_list));
    EXPECT_EQUAL(5, bytes_list._data2_list_size);
    EXPECT_EQUAL(5, bytes_list._data4_list_size);
    EXPECT_BYTES_EQUAL("A\x00", 2, bytes_list.data2_list[0], bytes_list._data2_list_sizeof[0]);
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list[1], bytes_list._data2_list_sizeof[1]);
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list[2], bytes_list._data2_list_sizeof[2]);
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list[3], bytes_list._data2_list_sizeof[3]);
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list[4], bytes_list._data2_list_sizeof[4]);
    EXPECT_BYTES_EQUAL("a\x00\x00\x00", 4, bytes_list.data4_list[0], bytes_list._data4_list_sizeof[0]);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list[1], bytes_list._data4_list_sizeof[1]);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list[2], bytes_list._data4_list_sizeof[2]);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list[3], bytes_list._data4_list_sizeof[3]);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list[4], bytes_list._data4_list_sizeof[4]);
    kpb_test_bytes_list_clear(&bytes_list);
    EXPECT_TRUE(kpb_test_bytes_list_parse_from_string(&bytes_list, "\x41\x00\x42\x43\x00\x00\x00\x00\x00\x00\x61\x00\x00\x00\x62\x63\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 30));
    EXPECT_EQUAL(5, kpb_test_bytes_list_data2_list_size(&bytes_list));
    EXPECT_EQUAL(5, kpb_test_bytes_list_data4_list_size(&bytes_list));
    EXPECT_EQUAL(5, bytes_list._data2_list_size);
    EXPECT_EQUAL(5, bytes_list._data4_list_size);
    EXPECT_BYTES_EQUAL("A\x00", 2, bytes_list.data2_list[0], bytes_list._data2_list_sizeof[0]);
    EXPECT_BYTES_EQUAL("BC", 2, bytes_list.data2_list[1], bytes_list._data2_list_sizeof[1]);
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list[2], bytes_list._data2_list_sizeof[2]);
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list[3], bytes_list._data2_list_sizeof[3]);
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list[4], bytes_list._data2_list_sizeof[4]);
    EXPECT_BYTES_EQUAL("a\x00\x00\x00", 4, bytes_list.data4_list[0], bytes_list._data4_list_sizeof[0]);
    EXPECT_BYTES_EQUAL("bc\x00\x00", 4, bytes_list.data4_list[1], bytes_list._data4_list_sizeof[1]);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list[2], bytes_list._data4_list_sizeof[2]);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list[3], bytes_list._data4_list_sizeof[3]);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list[4], bytes_list._data4_list_sizeof[4]);
    kpb_test_bytes_list_clear(&bytes_list);
    EXPECT_TRUE(kpb_test_bytes_list_parse_from_string(&bytes_list, "\x41\x00\x42\x43\x44\x45\x44\x45\x44\x45\x61\x00\x00\x00\x62\x63\x00\x00\x64\x65\x66\x00\x64\x65\x66\x00\x64\x65\x66\x00", 30));
    EXPECT_EQUAL(5, kpb_test_bytes_list_data2_list_size(&bytes_list));
    EXPECT_EQUAL(5, kpb_test_bytes_list_data4_list_size(&bytes_list));
    EXPECT_EQUAL(5, bytes_list._data2_list_size);
    EXPECT_EQUAL(5, bytes_list._data4_list_size);
    EXPECT_BYTES_EQUAL("A\x00", 2, bytes_list.data2_list[0], bytes_list._data2_list_sizeof[0]);
    EXPECT_BYTES_EQUAL("BC", 2, bytes_list.data2_list[1], bytes_list._data2_list_sizeof[1]);
    EXPECT_BYTES_EQUAL("DE", 2, bytes_list.data2_list[2], bytes_list._data2_list_sizeof[2]);
    EXPECT_BYTES_EQUAL("DE", 2, bytes_list.data2_list[3], bytes_list._data2_list_sizeof[3]);
    EXPECT_BYTES_EQUAL("DE", 2, bytes_list.data2_list[4], bytes_list._data2_list_sizeof[4]);
    EXPECT_BYTES_EQUAL("a\x00\x00\x00", 4, bytes_list.data4_list[0], bytes_list._data4_list_sizeof[0]);
    EXPECT_BYTES_EQUAL("bc\x00\x00", 4, bytes_list.data4_list[1], bytes_list._data4_list_sizeof[1]);
    EXPECT_BYTES_EQUAL("def\x00", 4, bytes_list.data4_list[2], bytes_list._data4_list_sizeof[2]);
    EXPECT_BYTES_EQUAL("def\x00", 4, bytes_list.data4_list[3], bytes_list._data4_list_sizeof[3]);
    EXPECT_BYTES_EQUAL("def\x00", 4, bytes_list.data4_list[4], bytes_list._data4_list_sizeof[4]);

    kpb_test_bytes_list_fini(&bytes_list);
    return 0;
}
