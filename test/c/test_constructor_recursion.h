#ifndef _H_KPB_TEST_CONSTRUCTOR_RECURSION_H_
#define _H_KPB_TEST_CONSTRUCTOR_RECURSION_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_constructor_recursion(void);

#ifdef __cplusplus
};
#endif

#endif
