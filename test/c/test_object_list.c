#include "test_object_list.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/object_list.h"


int kpb_test_object_list(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_object_list_t object_list;
    kpb_test_object_item_t *object_item;
    kpb_test_object_list_init(&object_list);
    EXPECT_EQUAL(0, kpb_test_object_list_list_size(&object_list));
    EXPECT_EQUAL(0, object_list._list_size);
    result = kpb_test_object_list_serialize_to_string(&object_list, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 12, result, result_size);
    kpb_io_free(result);
    object_item = kpb_test_object_list_add_list(&object_list);
    EXPECT_NOT_NULL(object_item);
    EXPECT_TRUE(kpb_test_object_item_set_value(object_item, 0x12345678));
    EXPECT_EQUAL(1, kpb_test_object_list_list_size(&object_list));
    EXPECT_EQUAL(1, object_list._list_size);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 0));
    EXPECT_EQUAL(0x12345678, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 0)));
    EXPECT_NOT_NULL(object_list.list[0]);
    EXPECT_EQUAL(0x12345678, object_list.list[0]->value);
    result = kpb_test_object_list_serialize_to_string(&object_list, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x78\x56\x34\x12\x00\x00\x00\x00\x00\x00\x00\x00", 12, result, result_size);
    kpb_io_free(result);
    object_item = kpb_test_object_list_add_list(&object_list);
    EXPECT_NOT_NULL(object_item);
    EXPECT_TRUE(kpb_test_object_item_set_value(object_item, 0xabcdef89));
    EXPECT_EQUAL(2, kpb_test_object_list_list_size(&object_list));
    EXPECT_EQUAL(2, object_list._list_size);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 0));
    EXPECT_EQUAL(0x12345678, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 0)));
    EXPECT_NOT_NULL(object_list.list[0]);
    EXPECT_EQUAL(0x12345678, object_list.list[0]->value);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 1));
    EXPECT_EQUAL(0xabcdef89, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 1)));
    EXPECT_NOT_NULL(object_list.list[1]);
    EXPECT_EQUAL(0xabcdef89, object_list.list[1]->value);
    result = kpb_test_object_list_serialize_to_string(&object_list, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x78\x56\x34\x12\x89\xef\xcd\xab\x00\x00\x00\x00", 12, result, result_size);
    kpb_io_free(result);
    object_item = kpb_test_object_list_add_list(&object_list);
    EXPECT_NOT_NULL(object_item);
    EXPECT_TRUE(kpb_test_object_item_set_value(object_item, 0xa1a2a3a4));
    EXPECT_EQUAL(3, kpb_test_object_list_list_size(&object_list));
    EXPECT_EQUAL(3, object_list._list_size);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 0));
    EXPECT_EQUAL(0x12345678, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 0)));
    EXPECT_NOT_NULL(object_list.list[0]);
    EXPECT_EQUAL(0x12345678, object_list.list[0]->value);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 1));
    EXPECT_EQUAL(0xabcdef89, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 1)));
    EXPECT_NOT_NULL(object_list.list[1]);
    EXPECT_EQUAL(0xabcdef89, object_list.list[1]->value);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 2));
    EXPECT_EQUAL(0xa1a2a3a4, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 2)));
    EXPECT_NOT_NULL(object_list.list[2]);
    EXPECT_EQUAL(0xa1a2a3a4, object_list.list[2]->value);
    result = kpb_test_object_list_serialize_to_string(&object_list, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x78\x56\x34\x12\x89\xef\xcd\xab\xa4\xa3\xa2\xa1", 12, result, result_size);
    kpb_io_free(result);
    object_item = kpb_test_object_list_add_list(&object_list);
    EXPECT_NOT_NULL(object_item);
    EXPECT_TRUE(kpb_test_object_item_set_value(object_item, 0x52535455));
    EXPECT_EQUAL(4, kpb_test_object_list_list_size(&object_list));
    EXPECT_EQUAL(4, object_list._list_size);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 0));
    EXPECT_EQUAL(0x12345678, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 0)));
    EXPECT_NOT_NULL(object_list.list[0]);
    EXPECT_EQUAL(0x12345678, object_list.list[0]->value);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 1));
    EXPECT_EQUAL(0xabcdef89, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 1)));
    EXPECT_NOT_NULL(object_list.list[1]);
    EXPECT_EQUAL(0xabcdef89, object_list.list[1]->value);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 2));
    EXPECT_EQUAL(0xa1a2a3a4, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 2)));
    EXPECT_NOT_NULL(object_list.list[2]);
    EXPECT_EQUAL(0xa1a2a3a4, object_list.list[2]->value);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 3));
    EXPECT_EQUAL(0x52535455, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 3)));
    EXPECT_NOT_NULL(object_list.list[3]);
    EXPECT_EQUAL(0x52535455, object_list.list[3]->value);
    result = kpb_test_object_list_serialize_to_string(&object_list, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x78\x56\x34\x12\x89\xef\xcd\xab\xa4\xa3\xa2\xa1", 12, result, result_size);
    kpb_io_free(result);

    kpb_test_object_list_clear(&object_list);
    EXPECT_TRUE(kpb_test_object_list_parse_from_string(&object_list, "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 12));
    EXPECT_EQUAL(3, kpb_test_object_list_list_size(&object_list));
    EXPECT_EQUAL(3, object_list._list_size);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 0));
    EXPECT_EQUAL(0x00000000, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 0)));
    EXPECT_NOT_NULL(object_list.list[0]);
    EXPECT_EQUAL(0x00000000, object_list.list[0]->value);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 1));
    EXPECT_EQUAL(0x00000000, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 1)));
    EXPECT_NOT_NULL(object_list.list[1]);
    EXPECT_EQUAL(0x00000000, object_list.list[1]->value);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 2));
    EXPECT_EQUAL(0x00000000, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 2)));
    EXPECT_NOT_NULL(object_list.list[2]);
    EXPECT_EQUAL(0x00000000, object_list.list[2]->value);
    EXPECT_TRUE(kpb_test_object_list_parse_from_string(&object_list, "\x78\x56\x34\x12\x00\x00\x00\x00\x00\x00\x00\x00", 12));
    EXPECT_EQUAL(3, kpb_test_object_list_list_size(&object_list));
    EXPECT_EQUAL(3, object_list._list_size);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 0));
    EXPECT_EQUAL(0x12345678, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 0)));
    EXPECT_NOT_NULL(object_list.list[0]);
    EXPECT_EQUAL(0x12345678, object_list.list[0]->value);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 1));
    EXPECT_EQUAL(0x00000000, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 1)));
    EXPECT_NOT_NULL(object_list.list[1]);
    EXPECT_EQUAL(0x00000000, object_list.list[1]->value);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 2));
    EXPECT_EQUAL(0x00000000, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 2)));
    EXPECT_NOT_NULL(object_list.list[2]);
    EXPECT_EQUAL(0x00000000, object_list.list[2]->value);
    EXPECT_TRUE(kpb_test_object_list_parse_from_string(&object_list, "\x78\x56\x34\x12\x89\xef\xcd\xab\x00\x00\x00\x00", 12));
    EXPECT_EQUAL(3, kpb_test_object_list_list_size(&object_list));
    EXPECT_EQUAL(3, object_list._list_size);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 0));
    EXPECT_EQUAL(0x12345678, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 0)));
    EXPECT_NOT_NULL(object_list.list[0]);
    EXPECT_EQUAL(0x12345678, object_list.list[0]->value);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 1));
    EXPECT_EQUAL(0xabcdef89, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 1)));
    EXPECT_NOT_NULL(object_list.list[1]);
    EXPECT_EQUAL(0xabcdef89, object_list.list[1]->value);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 2));
    EXPECT_EQUAL(0x00000000, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 2)));
    EXPECT_NOT_NULL(object_list.list[2]);
    EXPECT_EQUAL(0x00000000, object_list.list[2]->value);
    EXPECT_TRUE(kpb_test_object_list_parse_from_string(&object_list, "\x78\x56\x34\x12\x89\xef\xcd\xab\xa4\xa3\xa2\xa1", 12));
    EXPECT_EQUAL(3, kpb_test_object_list_list_size(&object_list));
    EXPECT_EQUAL(3, object_list._list_size);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 0));
    EXPECT_EQUAL(0x12345678, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 0)));
    EXPECT_NOT_NULL(object_list.list[0]);
    EXPECT_EQUAL(0x12345678, object_list.list[0]->value);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 1));
    EXPECT_EQUAL(0xabcdef89, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 1)));
    EXPECT_NOT_NULL(object_list.list[1]);
    EXPECT_EQUAL(0xabcdef89, object_list.list[1]->value);
    EXPECT_NOT_NULL(kpb_test_object_list_list_at(&object_list, 2));
    EXPECT_EQUAL(0xa1a2a3a4, kpb_test_object_item_value(kpb_test_object_list_list_at(&object_list, 2)));
    EXPECT_NOT_NULL(object_list.list[2]);
    EXPECT_EQUAL(0xa1a2a3a4, object_list.list[2]->value);

    kpb_test_object_list_fini(&object_list);
    return 0;
}
