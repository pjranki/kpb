#include "test_copy_from.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/copy_from.h"

int kpb_test_copy_from(void)
{
    // TODO: write this test
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_copy_from__t obj;
    kpb_test_copy_from__init(&obj);
    result_size = 0;
    result = kpb_test_copy_from__serialize_to_string(&obj, &result_size);
    EXPECT_NOT_NULL(result);
    kpb_io_free(result);
    kpb_test_copy_from__fini(&obj);
    return 0;
}
