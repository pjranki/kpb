#include "test_bit_pos_size.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/bit_pos_size.h"

int kpb_test_bit_pos_size(void)
{
    size_t result_size = 0;
    char *result = NULL;
    size_t i = 0;
    kpb_test_bit_pos_size__t obj;
    kpb_test_bit_pos_size__init(&obj);

    kpb_test_bit_pos_size__clear(&obj);
    EXPECT_TRUE(kpb_test_bit_pos_size__set_bit_count(&obj, 6));
    EXPECT_TRUE(kpb_test_bit_pos_size__set_bit_align(&obj, 7));
    EXPECT_TRUE(kpb_test_bit_pos_size__set_expected_pos_in_bits(&obj, 8));
    EXPECT_TRUE(kpb_test_bit_pos_size__set_expected_size_in_bits(&obj, 8));
    for ( i = 0; i < kpb_test_bit_pos_size__bit_count(&obj); i++ )
    {
        EXPECT_TRUE(kpb_test_bit_pos_size__add_bits(&obj, 1));
    }
    EXPECT_TRUE(kpb_test_bit_pos_size__set_end(&obj, 1));
    EXPECT_TRUE(kpb_test_bit_pos_size__add_ok(&obj, 0x55));
    result_size = 0;
    result = kpb_test_bit_pos_size__serialize_to_string(&obj, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xfd\x55", 2, result, result_size);
    kpb_io_free(result);

    kpb_test_bit_pos_size__clear(&obj);
    EXPECT_TRUE(kpb_test_bit_pos_size__set_bit_count(&obj, 7));
    EXPECT_TRUE(kpb_test_bit_pos_size__set_bit_align(&obj, 7));
    EXPECT_TRUE(kpb_test_bit_pos_size__set_expected_pos_in_bits(&obj, 8));
    EXPECT_TRUE(kpb_test_bit_pos_size__set_expected_size_in_bits(&obj, 8));
    for ( i = 0; i < kpb_test_bit_pos_size__bit_count(&obj); i++ )
    {
        EXPECT_TRUE(kpb_test_bit_pos_size__add_bits(&obj, 1));
    }
    EXPECT_TRUE(kpb_test_bit_pos_size__set_end(&obj, 1));
    EXPECT_TRUE(kpb_test_bit_pos_size__add_ok(&obj, 0x55));
    result_size = 0;
    result = kpb_test_bit_pos_size__serialize_to_string(&obj, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xff\x55", 2, result, result_size);
    kpb_io_free(result);

    kpb_test_bit_pos_size__clear(&obj);
    EXPECT_TRUE(kpb_test_bit_pos_size__set_bit_count(&obj, 8));
    EXPECT_TRUE(kpb_test_bit_pos_size__set_bit_align(&obj, 7));
    EXPECT_TRUE(kpb_test_bit_pos_size__set_expected_pos_in_bits(&obj, 15));
    EXPECT_TRUE(kpb_test_bit_pos_size__set_expected_size_in_bits(&obj, 15));
    for ( i = 0; i < kpb_test_bit_pos_size__bit_count(&obj); i++ )
    {
        EXPECT_TRUE(kpb_test_bit_pos_size__add_bits(&obj, 1));
    }
    EXPECT_TRUE(kpb_test_bit_pos_size__set_end(&obj, 1));
    EXPECT_TRUE(kpb_test_bit_pos_size__add_ok(&obj, 0x55));
    result_size = 0;
    result = kpb_test_bit_pos_size__serialize_to_string(&obj, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xff\x02\xaa", 3, result, result_size);
    kpb_io_free(result);

    kpb_test_bit_pos_size__clear(&obj);
    EXPECT_TRUE(kpb_test_bit_pos_size__set_bit_count(&obj, 6));
    EXPECT_TRUE(kpb_test_bit_pos_size__set_bit_align(&obj, 7));
    EXPECT_TRUE(kpb_test_bit_pos_size__set_expected_pos_in_bits(&obj, 8));
    EXPECT_TRUE(kpb_test_bit_pos_size__set_expected_size_in_bits(&obj, 16));
    EXPECT_TRUE(kpb_test_bit_pos_size__parse_from_string(&obj, "\xfd\x55", 2));
    for ( i = 0; i < kpb_test_bit_pos_size__bit_count(&obj); i++ )
    {
        EXPECT_EQUAL(1, kpb_test_bit_pos_size__bits_at(&obj, i));
    }
    EXPECT_EQUAL(1, kpb_test_bit_pos_size__padding_size(&obj));
    EXPECT_EQUAL(1, kpb_test_bit_pos_size__end(&obj));
    EXPECT_EQUAL(0x55, kpb_test_bit_pos_size__ok_at(&obj, 0));

    kpb_test_bit_pos_size__clear(&obj);
    EXPECT_TRUE(kpb_test_bit_pos_size__set_bit_count(&obj, 7));
    EXPECT_TRUE(kpb_test_bit_pos_size__set_bit_align(&obj, 7));
    EXPECT_TRUE(kpb_test_bit_pos_size__set_expected_pos_in_bits(&obj, 8));
    EXPECT_TRUE(kpb_test_bit_pos_size__set_expected_size_in_bits(&obj, 16));
    EXPECT_TRUE(kpb_test_bit_pos_size__parse_from_string(&obj, "\xff\x55", 2));
    for ( i = 0; i < kpb_test_bit_pos_size__bit_count(&obj); i++ )
    {
        EXPECT_EQUAL(1, kpb_test_bit_pos_size__bits_at(&obj, i));
    }
    EXPECT_EQUAL(0, kpb_test_bit_pos_size__padding_size(&obj));
    EXPECT_EQUAL(1, kpb_test_bit_pos_size__end(&obj));
    EXPECT_EQUAL(0x55, kpb_test_bit_pos_size__ok_at(&obj, 0));

    kpb_test_bit_pos_size__clear(&obj);
    EXPECT_TRUE(kpb_test_bit_pos_size__set_bit_count(&obj, 8));
    EXPECT_TRUE(kpb_test_bit_pos_size__set_bit_align(&obj, 7));
    EXPECT_TRUE(kpb_test_bit_pos_size__set_expected_pos_in_bits(&obj, 15));
    EXPECT_TRUE(kpb_test_bit_pos_size__set_expected_size_in_bits(&obj, 24));
    EXPECT_TRUE(kpb_test_bit_pos_size__parse_from_string(&obj, "\xff\x02\xaa", 3));
    for ( i = 0; i < kpb_test_bit_pos_size__bit_count(&obj); i++ )
    {
        EXPECT_EQUAL(1, kpb_test_bit_pos_size__bits_at(&obj, i));
    }
    EXPECT_EQUAL(6, kpb_test_bit_pos_size__padding_size(&obj));
    EXPECT_EQUAL(1, kpb_test_bit_pos_size__end(&obj));
    EXPECT_EQUAL(0x55, kpb_test_bit_pos_size__ok_at(&obj, 0));

    kpb_test_bit_pos_size__fini(&obj);
    return 0;
}
