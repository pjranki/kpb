#include <stdio.h>

#include "test_integers.h"
#include "test_bitfield.h"
#include "test_basic_list.h"
#include "test_strings_single.h"
#include "test_strings_list.h"
#include "test_bytes_single.h"
#include "test_bytes_list.h"
#include "test_object_single.h"
#include "test_object_list.h"
#include "test_imports.h"
#include "test_enums.h"
#include "test_switchon.h"
#include "test_params.h"
#include "test_value_instances.h"
#include "test_expressions.h"
#include "test_update.h"
#include "test_update_member.h"
#include "test_typecast.h"
#include "test_name_deconflict.h"
#include "test_io_expr.h"
#include "test_boolean.h"
#include "test_object_size.h"
#include "test_object_align.h"
#include "test_self_root_parent.h"
#include "test_floats.h"
#include "test_packet.h"
#include "test_contents.h"
#include "test_randomize.h"
#include "test_process.h"
#include "test_constructor_recursion.h"
#include "test_pos.h"
#include "test_pos_root_parent.h"
#include "test_copy_from.h"
#include "test_parse_or_fill.h"
#include "test_bitalign.h"
#include "test_process_value.h"
#include "test_parent_child.h"
#include "test_set_instance_value.h"
#include "test_bit_pos_size.h"

int main(int argc, char *argv[])
{
    (void)argv;
    (void)argc;

    int err = 0;

    printf("Start C test\n");

    err = kpb_test_integers();
    if ( err )
    {
        return err;
    }

    err = kpb_test_bitfield();
    if ( err )
    {
        return err;
    }

    err = kpb_test_basic_list();
    if ( err )
    {
        return err;
    }

    err = kpb_test_strings_single();
    if ( err )
    {
        return err;
    }

    err = kpb_test_strings_list();
    if ( err )
    {
        return err;
    }

    err = kpb_test_bytes_single();
    if ( err )
    {
        return err;
    }

    err = kpb_test_bytes_list();
    if ( err )
    {
        return err;
    }

    err = kpb_test_object_single();
    if ( err )
    {
        return err;
    }

    err = kpb_test_object_list();
    if ( err )
    {
        return err;
    }

    err = kpb_test_imports();
    if ( err )
    {
        return err;
    }

    err = kpb_test_enums();
    if ( err )
    {
        return err;
    }

    err = kpb_test_switchon();
    if ( err )
    {
        return err;
    }

    err = kpb_test_params();
    if ( err )
    {
        return err;
    }

    err = kpb_test_value_instances();
    if ( err )
    {
        return err;
    }

    err = kpb_test_expressions();
    if ( err )
    {
        return err;
    }

    err = kpb_test_update();
    if ( err )
    {
        return err;
    }

    err = kpb_test_update_member();
    if ( err )
    {
        return err;
    }

    err = kpb_test_typecast();
    if ( err )
    {
        return err;
    }

    err = kpb_test_name_deconflict();
    if ( err )
    {
        return err;
    }

    err = kpb_test_io_expr();
    if ( err )
    {
        return err;
    }

    err = kpb_test_boolean();
    if ( err )
    {
        return err;
    }

    err = kpb_test_object_size();
    if ( err )
    {
        return err;
    }

    err = kpb_test_object_align();
    if ( err )
    {
        return err;
    }

    err = kpb_test_self_root_parent();
    if ( err )
    {
        return err;
    }

    err = kpb_test_floats();
    if ( err )
    {
        return err;
    }

    err = kpb_test_packet();
    if ( err )
    {
        return err;
    }

    err = kpb_test_contents();
    if ( err )
    {
        return err;
    }

    err = kpb_test_randomize();
    if ( err )
    {
        return err;
    }

    err = kpb_test_process();
    if ( err )
    {
        return err;
    }

    err = kpb_test_constructor_recursion();
    if ( err )
    {
        return err;
    }

    err = kpb_test_pos();
    if ( err )
    {
        return err;
    }

    err = kpb_test_pos_root_parent();
    if ( err )
    {
        return err;
    }

    err = kpb_test_copy_from();
    if ( err )
    {
        return err;
    }

    err = kpb_test_parse_or_fill();
    if ( err )
    {
        return err;
    }

    err = kpb_test_bitalign();
    if ( err )
    {
        return err;
    }

    err = kpb_test_process_value();
    if ( err )
    {
        return err;
    }

    err = kpb_test_parent_child();
    if ( err )
    {
        return err;
    }

    err = kpb_test_set_instance_value();
    if ( err )
    {
        return err;
    }

    err = kpb_test_bit_pos_size();
    if ( err )
    {
        return err;
    }

    printf("Done C test\n");
    return 0;
}
