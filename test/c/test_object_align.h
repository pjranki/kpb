#ifndef _H_KPB_TEST_OBJECT_ALIGN_H_
#define _H_KPB_TEST_OBJECT_ALIGN_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_object_align(void);

#ifdef __cplusplus
};
#endif

#endif
