#ifndef _H_KPB_TEST_VALUE_INSTANCES_H_
#define _H_KPB_TEST_VALUE_INSTANCES_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_value_instances(void);

#ifdef __cplusplus
};
#endif

#endif
