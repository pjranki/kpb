#include "test_name_deconflict.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/name_deconflict.h"

int kpb_test_name_deconflict(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_fc_obj_t obj;
    kpb_test_fc_single_parent_t *single = NULL;
    kpb_test_fc_multi_parent_t *multi = NULL;

    kpb_test_fc_obj_init(&obj);
    kpb_test_fc_obj_clear(&obj);

    // call update
    result_size = 0;
    result = kpb_test_fc_obj_serialize_to_string(&obj, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(
        "\x20\x00\x00\x00" "\x00\x00\x00\x00" "\x00\x00\x00\x00" \
        "\x00\x00" "" \
        "\x00\x00" \
        "\x00" "" \
        "\x01" "\x00" \
        "\x00\x00" \
        "\x00" "" \
        "\x02" "\x00\x00" \
        "\x00\x00" \
        "\x00" "" \
        "\x00" \
        "\x00" "" \
        "\x00" "" \
        "\x00",
        32,
        result,
        result_size
    );
    kpb_io_free(result);
    EXPECT_EQUAL(32, kpb_test_fc_obj_fm_total_size1(&obj));
    EXPECT_EQUAL(1, kpb_test_fc_obj_fm_asciiz_size1(&obj));
    EXPECT_EQUAL(2, kpb_test_fc_obj_fm_widez_size1(&obj));

    // change some values - others are (automatically) updated
    EXPECT_TRUE(kpb_test_fc_obj_set_fm_total_size1(&obj, 0xdeadbeef));
    EXPECT_TRUE(kpb_test_fc_obj_set_fm_flags(&obj, 0xdeadbeef));
    EXPECT_TRUE(kpb_test_fc_obj_set_fm_unused(&obj, 0xcafebabe));
    EXPECT_TRUE(kpb_test_fc_obj_set_fm_buffer(&obj, "\xab\x00\xcd", 3));
    EXPECT_TRUE(kpb_test_fc_obj_add_fm_buffer_list(&obj, "\x12\x34\x56\x78\x90", 5));
    EXPECT_TRUE(kpb_test_fc_obj_set_fm_ascii(&obj, "A"));
    EXPECT_TRUE(kpb_test_fc_obj_set_fm_asciiz(&obj, "BC"));
    EXPECT_TRUE(kpb_test_fc_obj_add_fm_ascii_list(&obj, "DEF"));
    EXPECT_TRUE(kpb_test_fc_obj_set_fm_wide(&obj, L"a"));
    EXPECT_TRUE(kpb_test_fc_obj_set_fm_widez(&obj, L"bc"));
    EXPECT_TRUE(kpb_test_fc_obj_add_fm_wide_list(&obj, L"def"));

    single = kpb_test_fc_obj_add_fm_single_data_list(&obj);
    EXPECT_NOT_NULL(single);
    EXPECT_TRUE(kpb_test_fc_single_parent_set_fm_data(single, "\x33\x33\x33", 3));

    multi = kpb_test_fc_obj_add_fm_multi_data_list(&obj);
    EXPECT_NOT_NULL(multi);
    EXPECT_TRUE(kpb_test_fc_multi_parent_set_fm_data(multi, "\x55\x55\x55\x55\x55", 5));

    multi = kpb_test_fc_obj_mutable_fm_multi_data(&obj);
    EXPECT_NOT_NULL(multi);
    EXPECT_TRUE(kpb_test_fc_multi_parent_set_fm_data(multi, "\x44\x44\x44\x44", 4));
    single = kpb_test_fc_obj_mutable_fm_single_data(&obj);
    EXPECT_NOT_NULL(single);
    EXPECT_TRUE(kpb_test_fc_single_parent_set_fm_data(single, "\x11", 1));
    multi = kpb_test_fc_single_parent_mutable_fm_multi_data(single);
    EXPECT_NOT_NULL(multi);
    EXPECT_TRUE(kpb_test_fc_multi_parent_set_fm_data(multi, "\x22\x22", 2));

    EXPECT_EQUAL(74, kpb_test_fc_obj_fm_total_size1(&obj));
    EXPECT_EQUAL(0xdeadbeef, kpb_test_fc_obj_fm_flags(&obj));
    EXPECT_EQUAL(0, kpb_test_fc_obj_fm_unused(&obj));
    EXPECT_EQUAL(3, kpb_test_fc_obj_fm_buffer_size1(&obj));
    EXPECT_EQUAL(1, kpb_test_fc_obj_fm_buffer_list_count(&obj));
    EXPECT_EQUAL(1, kpb_test_fc_obj_fm_ascii_size1(&obj));
    EXPECT_EQUAL(3, kpb_test_fc_obj_fm_asciiz_size1(&obj));
    EXPECT_EQUAL(1, kpb_test_fc_obj_fm_ascii_list_count(&obj));
    EXPECT_EQUAL(2, kpb_test_fc_obj_fm_wide_size1(&obj));
    EXPECT_EQUAL(6, kpb_test_fc_obj_fm_widez_size1(&obj));
    EXPECT_EQUAL(1, kpb_test_fc_obj_fm_wide_list_count(&obj));

    result_size = 0;
    result = kpb_test_fc_obj_serialize_to_string(&obj, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(
        "\x4a\x00\x00\x00" "\xef\xbe\xad\xde" "\x00\x00\x00\x00" \
        "\x03\x00" "\xab\x00\xcd" \
        "\x01\x00" "\x12\x34\x56\x78"\
        "\x01" "A" \
        "\x03" "BC\x00" \
        "\x01\x00" "DEF\x00" \
        "\x02" "a\x00" \
        "\x06" "b\x00" "c\x00" "\x00\x00" \
        "\x01\x00" "d\x00" "e\x00" \
        "\x01" "\x11" "\x02" "\x22\x22" \
        "\x01" "\x03" "\x33\x33\x33" "\x00" \
        "\x04" "\x44\x44\x44\x44" \
        "\x01" "\x05" "\x55\x55\x55\x55\x55",
        74,
        result, result_size
    );
    kpb_io_free(result);

    // changes in parent are reflected in children via instances
    kpb_test_fc_obj_set_fm_flags(&obj, 0xdeadbeef);
    EXPECT_EQUAL(0xdeadbeef, kpb_test_fc_single_parent_fm_parent_flags(kpb_test_fc_obj_fm_single_data(&obj)));
    EXPECT_EQUAL(0xdeadbeef, kpb_test_fc_multi_parent_fm_flags(kpb_test_fc_single_parent_fm_multi_data(kpb_test_fc_obj_fm_single_data(&obj))));
    kpb_test_fc_obj_set_fm_flags(&obj, 0xcafebabe);
    EXPECT_EQUAL(0xcafebabe, kpb_test_fc_single_parent_fm_parent_flags(kpb_test_fc_obj_fm_single_data(&obj)));
    EXPECT_EQUAL(0xcafebabe, kpb_test_fc_multi_parent_fm_flags(kpb_test_fc_single_parent_fm_multi_data(kpb_test_fc_obj_fm_single_data(&obj))));

    // clear
    kpb_test_fc_obj_clear(&obj);
    result_size = 0;
    result = kpb_test_fc_obj_serialize_to_string(&obj, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(
        "\x20\x00\x00\x00" "\x00\x00\x00\x00" "\x00\x00\x00\x00" \
        "\x00\x00" "" \
        "\x00\x00" \
        "\x00" "" \
        "\x01" "\x00" \
        "\x00\x00" \
        "\x00" "" \
        "\x02" "\x00\x00" \
        "\x00\x00" \
        "\x00" "" \
        "\x00" \
        "\x00" "" \
        "\x00" "" \
        "\x00",
        32,
        result, result_size
    );
    kpb_io_free(result);

    // parse from string
    EXPECT_TRUE(kpb_test_fc_obj_parse_from_string(&obj,
        "\x4a\x00\x00\x00" "\xef\xbe\xad\xde" "\xa5\xa5\xa5\xa5" \
        "\x03\x00" "\xab\x00\xcd" \
        "\x01\x00" "\x12\x34\x56\x78"\
        "\x01" "A" \
        "\x03" "BC\x00" \
        "\x01\x00" "DEF\x00" \
        "\x02" "a\x00" \
        "\x06" "b\x00" "c\x00" "\x00\x00" \
        "\x01\x00" "d\x00" "e\x00" \
        "\x01" "\x11" "\x02" "\x22\x22" \
        "\x01" "\x03" "\x33\x33\x33" "\x00" \
        "\x04" "\x44\x44\x44\x44" \
        "\x01" "\x05" "\x55\x55\x55\x55\x55",
        74
    ));
    EXPECT_EQUAL(74, kpb_test_fc_obj_fm_total_size1(&obj));
    EXPECT_EQUAL(0xdeadbeef, kpb_test_fc_obj_fm_flags(&obj));
    EXPECT_EQUAL(0xa5a5a5a5, kpb_test_fc_obj_fm_unused(&obj));  // this is correct, parse/read should NOT call update
    EXPECT_EQUAL(3, kpb_test_fc_obj_fm_buffer_size1(&obj));
    EXPECT_BYTES_EQUAL("\xab\x00\xcd", 3, kpb_test_fc_obj_fm_buffer(&obj), kpb_test_fc_obj_fm_buffer_size(&obj));
    EXPECT_EQUAL(1, kpb_test_fc_obj_fm_buffer_list_count(&obj));
    EXPECT_EQUAL(1, kpb_test_fc_obj_fm_buffer_list_size(&obj));
    EXPECT_BYTES_EQUAL("\x12\x34\x56\x78", 4, kpb_test_fc_obj_fm_buffer_list_at(&obj, 0), kpb_test_fc_obj_fm_buffer_list_size_at(&obj, 0));
    EXPECT_EQUAL(1, kpb_test_fc_obj_fm_ascii_size1(&obj));
    EXPECT_STRING_EQUAL("A", kpb_test_fc_obj_fm_ascii(&obj));
    EXPECT_EQUAL(3, kpb_test_fc_obj_fm_asciiz_size1(&obj));
    EXPECT_STRING_EQUAL("BC", kpb_test_fc_obj_fm_asciiz(&obj));
    EXPECT_EQUAL(1, kpb_test_fc_obj_fm_ascii_list_count(&obj));
    EXPECT_EQUAL(1, kpb_test_fc_obj_fm_ascii_list_size(&obj));
    EXPECT_STRING_EQUAL("DEF", kpb_test_fc_obj_fm_ascii_list_at(&obj, 0));
    EXPECT_EQUAL(2, kpb_test_fc_obj_fm_wide_size1(&obj));
    EXPECT_STRING_EQUAL(L"a", kpb_test_fc_obj_fm_wide(&obj));
    EXPECT_EQUAL(6, kpb_test_fc_obj_fm_widez_size1(&obj));
    EXPECT_STRING_EQUAL(L"bc", kpb_test_fc_obj_fm_widez(&obj));
    EXPECT_EQUAL(1, kpb_test_fc_obj_fm_wide_list_count(&obj));
    EXPECT_EQUAL(1, kpb_test_fc_obj_fm_wide_list_size(&obj));
    EXPECT_STRING_EQUAL(L"de", kpb_test_fc_obj_fm_wide_list_at(&obj, 0));
    EXPECT_EQUAL(1, kpb_test_fc_single_parent_fm_data_size1(kpb_test_fc_obj_fm_single_data(&obj)));
    EXPECT_BYTES_EQUAL("\x11", 1, kpb_test_fc_single_parent_fm_data(kpb_test_fc_obj_fm_single_data(&obj)), kpb_test_fc_single_parent_fm_data_size(kpb_test_fc_obj_fm_single_data(&obj)));
    EXPECT_EQUAL(2, kpb_test_fc_multi_parent_fm_data_size1(kpb_test_fc_single_parent_fm_multi_data(kpb_test_fc_obj_fm_single_data(&obj))));
    EXPECT_BYTES_EQUAL("\x22\x22", 2,
        kpb_test_fc_multi_parent_fm_data(kpb_test_fc_single_parent_fm_multi_data(kpb_test_fc_obj_fm_single_data(&obj))),
        kpb_test_fc_multi_parent_fm_data_size(kpb_test_fc_single_parent_fm_multi_data(kpb_test_fc_obj_fm_single_data(&obj)))
    );
    EXPECT_EQUAL(1, kpb_test_fc_obj_fm_single_data_list_count(&obj));
    EXPECT_EQUAL(1, kpb_test_fc_obj_fm_single_data_list_size(&obj));
    EXPECT_EQUAL(3, kpb_test_fc_single_parent_fm_data_size1(kpb_test_fc_obj_fm_single_data_list_at(&obj, 0)));
    EXPECT_BYTES_EQUAL("\x33\x33\x33", 3,
        kpb_test_fc_single_parent_fm_data(kpb_test_fc_obj_fm_single_data_list_at(&obj, 0)),
        kpb_test_fc_single_parent_fm_data_size(kpb_test_fc_obj_fm_single_data_list_at(&obj, 0))
    );
    EXPECT_EQUAL(0, kpb_test_fc_multi_parent_fm_data_size1(kpb_test_fc_single_parent_fm_multi_data(kpb_test_fc_obj_fm_single_data_list_at(&obj, 0))));
    EXPECT_EQUAL(0,
        kpb_test_fc_multi_parent_fm_data_size(kpb_test_fc_single_parent_fm_multi_data(kpb_test_fc_obj_fm_single_data_list_at(&obj, 0)))
    );
    EXPECT_EQUAL(4, kpb_test_fc_multi_parent_fm_data_size1(kpb_test_fc_obj_fm_multi_data(&obj)));
    EXPECT_BYTES_EQUAL("\x44\x44\x44\x44", 4,
        kpb_test_fc_multi_parent_fm_data(kpb_test_fc_obj_fm_multi_data(&obj)),
        kpb_test_fc_multi_parent_fm_data_size(kpb_test_fc_obj_fm_multi_data(&obj))
    );
    EXPECT_EQUAL(1, kpb_test_fc_obj_fm_multi_data_list_count(&obj));
    EXPECT_EQUAL(1, kpb_test_fc_obj_fm_multi_data_list_size(&obj));
    EXPECT_EQUAL(5, kpb_test_fc_multi_parent_fm_data_size1(kpb_test_fc_obj_fm_multi_data_list_at(&obj, 0)));
    EXPECT_BYTES_EQUAL("\x55\x55\x55\x55\x55", 5,
        kpb_test_fc_multi_parent_fm_data(kpb_test_fc_obj_fm_multi_data_list_at(&obj, 0)),
        kpb_test_fc_multi_parent_fm_data_size(kpb_test_fc_obj_fm_multi_data_list_at(&obj, 0))
    );

    // enum
    uint16_t value16 = KPB_TEST_FC_OBJ_FE_FOOBAR_FVE_V1;
    EXPECT_EQUAL(0x1234, value16);

    // instances
    EXPECT_EQUAL(1, kpb_test_fc_obj_fm_0(&obj));
    EXPECT_TRUE(kpb_test_fc_obj_fm_1(&obj));
    EXPECT_EQUAL(0, kpb_test_fc_single_parent_fm_data_size1(kpb_test_fc_obj_fm_2(&obj)));
    EXPECT_EQUAL(0, kpb_test_fc_obj_fm_3(&obj));
    EXPECT_EQUAL(0, kpb_test_fc_obj_fm_4(&obj));

    kpb_test_fc_obj_fini(&obj);

    // correct use of size
    kpb_test_correct_use_of_size__t correct_size;
    kpb_test_correct_use_of_size__init(&correct_size);
    EXPECT_TRUE(kpb_test_correct_use_of_size__set_size_(&correct_size, 0xdeadbeef));
    EXPECT_EQUAL(0xdeadbeef, correct_size.size_);
    EXPECT_EQUAL(0xdeadbeef, correct_size.value1);
    EXPECT_EQUAL(0xdeadbeef, kpb_test_correct_use_of_size__value2(&correct_size));
    EXPECT_TRUE(kpb_test_correct_use_of_size__set_size_(&correct_size, 0xcafebabe));
    EXPECT_EQUAL(0xcafebabe, correct_size.size_);
    EXPECT_EQUAL(0xcafebabe, correct_size.value1);
    EXPECT_EQUAL(0xcafebabe, kpb_test_correct_use_of_size__value2(&correct_size));
    kpb_test_correct_use_of_size__fini(&correct_size);

    // correct use of length
    kpb_test_correct_use_of_length_t correct_length;
    kpb_test_correct_use_of_length_init(&correct_length);
    EXPECT_TRUE(kpb_test_correct_use_of_length_set_length_(&correct_length, 0xdeadbeef));
    EXPECT_EQUAL(0xdeadbeef, correct_length.length_);
    // update/set will use length as the size of the object (not the member's value)
    EXPECT_EQUAL(8, correct_length.value1);
    EXPECT_EQUAL(0xdeadbeef, kpb_test_correct_use_of_length_value2(&correct_length));
    EXPECT_TRUE(kpb_test_correct_use_of_length_set_length_(&correct_length, 0xcafebabe));
    EXPECT_EQUAL(0xcafebabe, correct_length.length_);
    // update/set will use length as the size of the object (not the member's value)
    EXPECT_EQUAL(8, correct_length.value1);
    EXPECT_EQUAL(0xcafebabe, kpb_test_correct_use_of_length_value2(&correct_length));
    kpb_test_correct_use_of_length_fini(&correct_length);
    return 0;
}
