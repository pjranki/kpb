#include "test_object_single.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/object_single.h"


int kpb_test_object_single(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_object_single_t object_single;
    kpb_test_sub_object_t *sub_object = NULL;
    const kpb_test_sub_object_t *sub_object_const = NULL;
    kpb_test_object_single_init(&object_single);
    sub_object_const = kpb_test_object_single_other(&object_single);
    EXPECT_NULL(sub_object);
    sub_object = kpb_test_object_single_mutable_other(&object_single);
    EXPECT_NOT_NULL(sub_object);
    EXPECT_EQUAL(0x00000000, kpb_test_sub_object_value(sub_object));
    result = kpb_test_object_single_serialize_to_string(&object_single, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, result, result_size);
    kpb_io_free(result);
    sub_object = kpb_test_object_single_mutable_other(&object_single);
    EXPECT_NOT_NULL(sub_object);
    kpb_test_sub_object_set_value(sub_object, 0x12345678);
    EXPECT_EQUAL(0x12345678, kpb_test_sub_object_value(sub_object));
    result = kpb_test_object_single_serialize_to_string(&object_single, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x78\x56\x34\x12", 4, result, result_size);
    kpb_io_free(result);
    kpb_test_object_single_clear(&object_single);
    sub_object_const = kpb_test_object_single_other(&object_single);
    EXPECT_NOT_NULL(sub_object_const);
    result = kpb_test_object_single_serialize_to_string(&object_single, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, result, result_size);
    kpb_io_free(result);

    EXPECT_TRUE(kpb_test_object_single_parse_from_string(&object_single, "\xab\xcd\xef\x89", 4));
    sub_object_const = kpb_test_object_single_other(&object_single);
    EXPECT_NOT_NULL(sub_object_const);
    EXPECT_EQUAL(0x89efcdab, kpb_test_sub_object_value(sub_object_const));
    sub_object = kpb_test_object_single_mutable_other(&object_single);
    EXPECT_EQUAL(0x89efcdab, kpb_test_sub_object_value(sub_object));
    EXPECT_TRUE(kpb_test_object_single_parse_from_string(&object_single, "\x00\x00\x00\x00", 4));
    sub_object_const = kpb_test_object_single_other(&object_single);
    EXPECT_NOT_NULL(sub_object_const);
    EXPECT_EQUAL(0x00000000, kpb_test_sub_object_value(sub_object_const));
    sub_object = kpb_test_object_single_mutable_other(&object_single);
    EXPECT_EQUAL(0x00000000, kpb_test_sub_object_value(sub_object));

    kpb_test_object_single_fini(&object_single);
    return 0;
}
