#include "process_value_sub_type.h"

#include <kpb_io.h>

void process_value_sub_type_init(struct process_value_sub_type *_this, const kpb_test_process_value_t *_root)
{
    memset(_this, 0, sizeof(*_this));
    _this->root_ = (kpb_test_process_value_t *)_root;
}

const kpb_test_sub_process_value_t *process_value_sub_type_getter(struct process_value_sub_type *_this, const kpb_test_sub_process_value_t *value)
{
    kpb_test_process_value_set_getter_count(_this->root_, kpb_test_process_value_getter_count(_this->root_) + 1);
    return value;
}

kpb_test_sub_process_value_t *process_value_sub_type_setter(struct process_value_sub_type *_this, kpb_test_sub_process_value_t *value)
{
    kpb_test_process_value_set_setter_count(_this->root_, kpb_test_process_value_setter_count(_this->root_) + 1);
    return value;
}

void process_value_sub_type_fini(struct process_value_sub_type *_this)
{
    memset(_this, 0, sizeof(*_this));
}
