#ifndef _H_KPB_TEST_PARAMS_H_
#define _H_KPB_TEST_PARAMS_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_params(void);

#ifdef __cplusplus
};
#endif

#endif
