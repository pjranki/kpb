#ifndef _H_PROCESS_VALUE_SUB_TYPE_H_
#define _H_PROCESS_VALUE_SUB_TYPE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <string.h>

#include <kpb_io.h>

#include "c/process_value.h"

struct process_value_sub_type {
    kpb_test_process_value_t *root_;
};

void process_value_sub_type_init(struct process_value_sub_type *_this, const kpb_test_process_value_t *_root);
const kpb_test_sub_process_value_t *process_value_sub_type_getter(struct process_value_sub_type *_this, const kpb_test_sub_process_value_t *value);
kpb_test_sub_process_value_t *process_value_sub_type_setter(struct process_value_sub_type *_this, kpb_test_sub_process_value_t *value);
void process_value_sub_type_fini(struct process_value_sub_type *_this);

#ifdef __cplusplus
};
#endif

#endif
