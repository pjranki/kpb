#include "test_object_size.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/object_size.h"

int kpb_test_object_size(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_object_size__t obj;
    kpb_test_object_size_4_t *sub_obj = NULL;
    kpb_test_object_size__init(&obj);

    result_size = 0;
    result = kpb_test_object_size__serialize_to_string(&obj, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00\x00\x00", 8, result, result_size);
    kpb_io_free(result);
    EXPECT_FALSE(kpb_test_object_size__parse_from_string(&obj, "\x00\x00\x00\x00\x00\x00\x00\x00", 8));
    EXPECT_TRUE(kpb_test_object_size__parse_from_string(&obj, "\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 16));

    sub_obj = kpb_test_object_size__mutable_object_var(&obj);
    EXPECT_NOT_NULL(sub_obj);
    EXPECT_TRUE(kpb_test_object_size_4_set_value(sub_obj, 0x12345678));
    sub_obj = kpb_test_object_size__mutable_object_fix(&obj);
    EXPECT_NOT_NULL(sub_obj);
    EXPECT_TRUE(kpb_test_object_size_4_set_value(sub_obj, 0x09abcdef));

    EXPECT_TRUE(kpb_test_object_size__set_object_size_(&obj, 0));
    result_size = 0;
    result = kpb_test_object_size__serialize_to_string(&obj, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\xef\xcd\xab\x09", 8, result, result_size);
    kpb_io_free(result);

    EXPECT_TRUE(kpb_test_object_size__set_object_size_(&obj, 1));
    result_size = 0;
    result = kpb_test_object_size__serialize_to_string(&obj, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x01\x00\x00\x00\x78\xef\xcd\xab\x09", 9, result, result_size);
    kpb_io_free(result);

    EXPECT_TRUE(kpb_test_object_size__set_object_size_(&obj, 2));
    result_size = 0;
    result = kpb_test_object_size__serialize_to_string(&obj, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x02\x00\x00\x00\x78\x56\xef\xcd\xab\x09", 10, result, result_size);
    kpb_io_free(result);

    EXPECT_TRUE(kpb_test_object_size__set_object_size_(&obj, 3));
    result_size = 0;
    result = kpb_test_object_size__serialize_to_string(&obj, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x03\x00\x00\x00\x78\x56\x34\xef\xcd\xab\x09", 11, result, result_size);
    kpb_io_free(result);

    EXPECT_TRUE(kpb_test_object_size__set_object_size_(&obj, 4));
    result_size = 0;
    result = kpb_test_object_size__serialize_to_string(&obj, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x04\x00\x00\x00\x78\x56\x34\x12\xef\xcd\xab\x09", 12, result, result_size);
    kpb_io_free(result);

    EXPECT_TRUE(kpb_test_object_size__set_object_size_(&obj, 5));
    result_size = 0;
    result = kpb_test_object_size__serialize_to_string(&obj, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x05\x00\x00\x00\x78\x56\x34\x12\x00\xef\xcd\xab\x09", 13, result, result_size);
    kpb_io_free(result);

    EXPECT_TRUE(kpb_test_object_size__set_object_size_(&obj, 6));
    result_size = 0;
    result = kpb_test_object_size__serialize_to_string(&obj, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x06\x00\x00\x00\x78\x56\x34\x12\x00\x00\xef\xcd\xab\x09", 14, result, result_size);
    kpb_io_free(result);

    kpb_test_object_size__clear(&obj);
    EXPECT_TRUE(kpb_test_object_size__parse_from_string(&obj, "\x04\x00\x00\x00\x78\x56\x34\x12\xef\xcd\xab\x09", 12));
    EXPECT_EQUAL(4, obj.object_size_);
    EXPECT_EQUAL(0x12345678, obj.object_var->value);
    EXPECT_EQUAL(0x09abcdef, obj.object_fix->value);

    kpb_test_object_size__clear(&obj);
    EXPECT_TRUE(kpb_test_object_size__parse_from_string(&obj, "\x05\x00\x00\x00\x78\x56\x34\x12\xff\xef\xcd\xab\x09", 13));
    EXPECT_EQUAL(5, obj.object_size_);
    EXPECT_EQUAL(0x12345678, obj.object_var->value);
    EXPECT_EQUAL(0x09abcdef, obj.object_fix->value);

    kpb_test_object_size__clear(&obj);
    EXPECT_TRUE(kpb_test_object_size__parse_from_string(&obj, "\x06\x00\x00\x00\x78\x56\x34\x12\xff\xff\xef\xcd\xab\x09", 14));
    EXPECT_EQUAL(6, obj.object_size_);
    EXPECT_EQUAL(0x12345678, obj.object_var->value);
    EXPECT_EQUAL(0x09abcdef, obj.object_fix->value);

    kpb_test_object_size__fini(&obj);
    return 0;
}
