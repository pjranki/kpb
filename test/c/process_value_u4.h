#ifndef _H_PROCESS_VALUE_U4_H_
#define _H_PROCESS_VALUE_U4_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <string.h>

#include <kpb_io.h>

#include "c/process_value.h"

struct process_value_u4 {
    int unused;
};

void process_value_u4_init(struct process_value_u4 *_this);
uint32_t process_value_u4_getter(struct process_value_u4 *_this, uint32_t value);
uint32_t process_value_u4_setter(struct process_value_u4 *_this, uint32_t value);
void process_value_u4_fini(struct process_value_u4 *_this);

#ifdef __cplusplus
};
#endif

#endif
