#ifndef _H_KPB_TEST_POS_ROOT_PARENT_H_
#define _H_KPB_TEST_POS_ROOT_PARENT_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_pos_root_parent(void);

#ifdef __cplusplus
};
#endif

#endif
