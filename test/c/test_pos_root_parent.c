#include "test_pos_root_parent.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/pos_root_parent.h"

int kpb_test_pos_root_parent(void)
{
    size_t result_size = 0;
    char *result = NULL;

    // check default state (after constructor, after a clear)
    kpb_test_pos_root_parent_t root;
    kpb_test_pos_child_t *child = NULL;
    kpb_test_pos_grandchild_t *grandchild = NULL;
    kpb_test_pos_root_parent_init(&root);
    EXPECT_EQUAL(28, kpb_test_pos_root_parent_size(&root));
    kpb_test_pos_root_parent_clear(&root);
    EXPECT_EQUAL(28, kpb_test_pos_root_parent_size(&root));

    // parse at correct positions in correct substreams
    EXPECT_TRUE(kpb_test_pos_root_parent_parse_from_string(&root, "\x11\x11\x11\x11\x22\x22\x22\x22\x33\x33\x33\x33\x44\x44\x44\x44\x55\x55\x55\x55\x66\x66\x66\x66\x77\x77\x77\x77", 28));
    EXPECT_EQUAL(0x11111111, kpb_test_pos_root_parent_value(&root));
    EXPECT_EQUAL(0x22222222, kpb_test_pos_child_value_parent(kpb_test_pos_root_parent_child(&root)));
    EXPECT_EQUAL(0x33333333, kpb_test_pos_child_value_root(kpb_test_pos_root_parent_child(&root)));
    EXPECT_EQUAL(0x44444444, kpb_test_pos_grandchild_value_root(kpb_test_pos_child_grandchild(kpb_test_pos_root_parent_child(&root))));
    EXPECT_EQUAL(0x55555555, kpb_test_pos_child_value(kpb_test_pos_root_parent_child(&root)));
    EXPECT_EQUAL(0x66666666, kpb_test_pos_grandchild_value_parent(kpb_test_pos_child_grandchild(kpb_test_pos_root_parent_child(&root))));
    EXPECT_EQUAL(0x77777777, kpb_test_pos_grandchild_value(kpb_test_pos_child_grandchild(kpb_test_pos_root_parent_child(&root))));

    // serialize at correct positions in correct substreams
    kpb_test_pos_root_parent_clear(&root);
    child = kpb_test_pos_root_parent_mutable_child(&root);
    EXPECT_NOT_NULL(child);
    grandchild = kpb_test_pos_child_mutable_grandchild(child);
    EXPECT_NOT_NULL(grandchild);
    EXPECT_TRUE(kpb_test_pos_root_parent_set_value(&root, 0x77777777));
    EXPECT_TRUE(kpb_test_pos_child_set_value_parent(child, 0x66666666));
    EXPECT_TRUE(kpb_test_pos_child_set_value_root(child, 0x55555555));
    EXPECT_TRUE(kpb_test_pos_grandchild_set_value_root(grandchild, 0x41414141));
    EXPECT_TRUE(kpb_test_pos_child_set_value(child, 0x33333333));
    EXPECT_TRUE(kpb_test_pos_grandchild_set_value_parent(grandchild, 0x22222222));
    EXPECT_TRUE(kpb_test_pos_grandchild_set_value(grandchild, 0x11111111));
    result = kpb_test_pos_root_parent_serialize_to_string(&root, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(
        "\x77\x77\x77\x77\x66\x66\x66\x66\x55\x55\x55\x55\x41\x41\x41\x41\x33\x33\x33\x33\x22\x22\x22\x22\x11\x11\x11\x11", 28,
        result, result_size);
    kpb_io_free(result);

    kpb_test_pos_root_parent_fini(&root);
    return 0;
}
