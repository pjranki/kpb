#include "test_contents.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/contents.h"

int kpb_test_contents(void)
{
    kpb_test_contents_t contents;
    kpb_test_contents_init(&contents);

    EXPECT_BYTES_EQUAL("\xCA\xFE\xBA\xBE", 4, kpb_test_contents_magic(&contents), kpb_test_contents_magic_size(&contents));
    EXPECT_BYTES_EQUAL("\x4A\x46\x49\x46", 4, kpb_test_contents_magic1(&contents), kpb_test_contents_magic1_size(&contents));
    EXPECT_BYTES_EQUAL("\xCA\xFE\xBA\xBE", 4, kpb_test_contents_magic2(&contents), kpb_test_contents_magic2_size(&contents));
    EXPECT_BYTES_EQUAL("\x43\x41\x46\x45\x00\x42\x41\x42\x45", 9, kpb_test_contents_magic3(&contents), kpb_test_contents_magic3_size(&contents));
    EXPECT_BYTES_EQUAL("\x66\x6F\x6F\x00\x41\x0A\x2A", 7, kpb_test_contents_magic4(&contents), kpb_test_contents_magic4_size(&contents));
    EXPECT_BYTES_EQUAL("\x01\x55\x41\x2C\x33\x03", 6, kpb_test_contents_magic5(&contents), kpb_test_contents_magic5_size(&contents));

    EXPECT_TRUE(kpb_test_contents_set_magic(&contents, "", 0));
    EXPECT_TRUE(kpb_test_contents_set_magic1(&contents, "\x11", 1));
    EXPECT_TRUE(kpb_test_contents_set_magic2(&contents, "\x11\x22", 2));
    EXPECT_TRUE(kpb_test_contents_set_magic3(&contents, "\x11\x22\x33", 3));
    EXPECT_TRUE(kpb_test_contents_set_magic4(&contents, "\x11\x22\x33\x44", 4));
    EXPECT_TRUE(kpb_test_contents_set_magic5(&contents, "\x11\x22\x33\x44\x55", 5));

    EXPECT_EQUAL(0, kpb_test_contents_magic_size(&contents));
    EXPECT_BYTES_EQUAL("\x11", 1, kpb_test_contents_magic1(&contents), kpb_test_contents_magic1_size(&contents));
    EXPECT_BYTES_EQUAL("\x11\x22", 2, kpb_test_contents_magic2(&contents), kpb_test_contents_magic2_size(&contents));
    EXPECT_BYTES_EQUAL("\x11\x22\x33", 3, kpb_test_contents_magic3(&contents), kpb_test_contents_magic3_size(&contents));
    EXPECT_BYTES_EQUAL("\x11\x22\x33\x44", 4, kpb_test_contents_magic4(&contents), kpb_test_contents_magic4_size(&contents));
    EXPECT_BYTES_EQUAL("\x11\x22\x33\x44\x55", 5, kpb_test_contents_magic5(&contents), kpb_test_contents_magic5_size(&contents));

    size_t result_size = 0;
    char *result = kpb_test_contents_serialize_to_string(&contents, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_TRUE(kpb_test_contents_parse_from_string(&contents, result, result_size));
    kpb_io_free(result);

    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, kpb_test_contents_magic(&contents), kpb_test_contents_magic_size(&contents));
    EXPECT_BYTES_EQUAL("\x11\x00\x00\x00", 4, kpb_test_contents_magic1(&contents), kpb_test_contents_magic1_size(&contents));
    EXPECT_BYTES_EQUAL("\x11\x22\x00\x00", 4, kpb_test_contents_magic2(&contents), kpb_test_contents_magic2_size(&contents));
    EXPECT_BYTES_EQUAL("\x11\x22\x33\x00\x00\x00\x00\x00\x00", 9, kpb_test_contents_magic3(&contents), kpb_test_contents_magic3_size(&contents));
    EXPECT_BYTES_EQUAL("\x11\x22\x33\x44\x00\x00\x00", 7, kpb_test_contents_magic4(&contents), kpb_test_contents_magic4_size(&contents));
    EXPECT_BYTES_EQUAL("\x11\x22\x33\x44\x55\x00", 6, kpb_test_contents_magic5(&contents), kpb_test_contents_magic5_size(&contents));

    kpb_test_contents_clear(&contents);

    EXPECT_BYTES_EQUAL("\xCA\xFE\xBA\xBE", 4, kpb_test_contents_magic(&contents), kpb_test_contents_magic_size(&contents));
    EXPECT_BYTES_EQUAL("\x4A\x46\x49\x46", 4, kpb_test_contents_magic1(&contents), kpb_test_contents_magic1_size(&contents));
    EXPECT_BYTES_EQUAL("\xCA\xFE\xBA\xBE", 4, kpb_test_contents_magic2(&contents), kpb_test_contents_magic2_size(&contents));
    EXPECT_BYTES_EQUAL("\x43\x41\x46\x45\x00\x42\x41\x42\x45", 9, kpb_test_contents_magic3(&contents), kpb_test_contents_magic3_size(&contents));
    EXPECT_BYTES_EQUAL("\x66\x6F\x6F\x00\x41\x0A\x2A", 7, kpb_test_contents_magic4(&contents), kpb_test_contents_magic4_size(&contents));
    EXPECT_BYTES_EQUAL("\x01\x55\x41\x2C\x33\x03", 6, kpb_test_contents_magic5(&contents), kpb_test_contents_magic5_size(&contents));

    kpb_test_contents_fini(&contents);
    return 0;
}
