#ifndef _H_KPB_TEST_COPY_FROM_H_
#define _H_KPB_TEST_COPY_FROM_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_copy_from(void);

#ifdef __cplusplus
};
#endif

#endif
