#include "test_bitalign.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/bitalign.h"

int kpb_test_bitalign(void)
{
    kpb_test_bitalign_default_t objd;
    kpb_test_bitalign_8_t obj8;
    kpb_test_bitalign_1_t obj1;
    size_t result_size = 0;
    char *result = NULL;

    kpb_test_bitalign_default_init(&objd);
    kpb_test_bitalign_8_init(&obj8);
    kpb_test_bitalign_1_init(&obj1);

    // serialize objd with default bit alignment
    EXPECT_TRUE(kpb_test_bitalign_default_set_v1(&objd, 1));
    EXPECT_TRUE(kpb_test_v3bd_set_v(kpb_test_bitalign_default_mutable_v2(&objd), 0x7));
    EXPECT_TRUE(kpb_test_v3bd_set_v(kpb_test_bitalign_default_mutable_v3(&objd), 0x7));
    EXPECT_TRUE(kpb_test_bitalign_default_set_v4(&objd, 0xfff));
    result_size = 0;
    result = kpb_test_bitalign_default_serialize_to_string(&objd, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(result, result_size, "\x80\xe0\xe0\xff\xf0", 5);
    kpb_io_free(result);

    // parse objd with default bit alignment
    kpb_test_bitalign_default_clear(&objd);
    EXPECT_TRUE(kpb_test_bitalign_default_parse_from_string(&objd, "\x80\xe0\xe0\xff\xf0", 5));
    EXPECT_EQUAL(kpb_test_bitalign_default_v1(&objd), 1);
    EXPECT_EQUAL(kpb_test_v3bd_v(kpb_test_bitalign_default_v2(&objd)), 0x7);
    EXPECT_EQUAL(kpb_test_v3bd_v(kpb_test_bitalign_default_v3(&objd)), 0x7);
    EXPECT_EQUAL(kpb_test_bitalign_default_v4(&objd), 0xfff);

    // serialize obj8 with 8 bit alignment
    EXPECT_TRUE(kpb_test_bitalign_8_set_v1(&obj8, 1));
    EXPECT_TRUE(kpb_test_v3b8_set_v(kpb_test_bitalign_8_mutable_v2(&obj8), 0x7));
    EXPECT_TRUE(kpb_test_v3b8_set_v(kpb_test_bitalign_8_mutable_v3(&obj8), 0x7));
    EXPECT_TRUE(kpb_test_bitalign_8_set_v4(&obj8, 0xfff));
    result_size = 0;
    result = kpb_test_bitalign_8_serialize_to_string(&obj8, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(result, result_size, "\x80\xe0\xe0\xff\xf0", 5);
    kpb_io_free(result);

    // parse obj8 with 8 bit alignment
    kpb_test_bitalign_8_clear(&obj8);
    EXPECT_TRUE(kpb_test_bitalign_8_parse_from_string(&obj8, "\x80\xe0\xe0\xff\xf0", 5));
    EXPECT_EQUAL(kpb_test_bitalign_8_v1(&obj8), 1);
    EXPECT_EQUAL(kpb_test_v3b8_v(kpb_test_bitalign_8_v2(&obj8)), 0x7);
    EXPECT_EQUAL(kpb_test_v3b8_v(kpb_test_bitalign_8_v3(&obj8)), 0x7);
    EXPECT_EQUAL(kpb_test_bitalign_8_v4(&obj8), 0xfff);

    // serialize obj1 with 1 bit alignment
    EXPECT_TRUE(kpb_test_bitalign_1_set_v1(&obj1, 1));
    EXPECT_TRUE(kpb_test_v3b1_set_v(kpb_test_bitalign_1_mutable_v2(&obj1), 0x7));
    EXPECT_TRUE(kpb_test_v3b1_set_v(kpb_test_bitalign_1_mutable_v3(&obj1), 0x7));
    EXPECT_TRUE(kpb_test_bitalign_1_set_v4(&obj1, 0xfff));
    result_size = 0;
    result = kpb_test_bitalign_1_serialize_to_string(&obj1, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(result, result_size, "\xff\xff\xe0", 3);
    kpb_io_free(result);

    // parse obj1 with 1 bit alignment
    kpb_test_bitalign_1_clear(&obj1);
    EXPECT_TRUE(kpb_test_bitalign_1_parse_from_string(&obj1, "\xff\xff\xe0", 3));
    EXPECT_EQUAL(kpb_test_bitalign_1_v1(&obj1), 1);
    EXPECT_EQUAL(kpb_test_v3b1_v(kpb_test_bitalign_1_v2(&obj1)), 0x7);
    EXPECT_EQUAL(kpb_test_v3b1_v(kpb_test_bitalign_1_v3(&obj1)), 0x7);
    EXPECT_EQUAL(kpb_test_bitalign_1_v4(&obj1), 0xfff);

    kpb_test_bitalign_default_fini(&objd);
    kpb_test_bitalign_8_fini(&obj8);
    kpb_test_bitalign_1_fini(&obj1);
    return 0;
}
