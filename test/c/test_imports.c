#include "test_imports.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/imports.h"

int kpb_test_imports(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_imports_t imports;
    kpb_test_imports_init(&imports);
    result_size = 0;
    result = kpb_test_imports_serialize_to_string(&imports, &result_size);
    EXPECT_NOT_NULL(result);
    kpb_io_free(result);
    kpb_test_imports_fini(&imports);
    return 0;
}
