#include "test_parse_or_fill.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/parse_or_fill.h"

int kpb_test_parse_or_fill(void)
{
    kpb_test_parse_or_fill_t obj;
    kpb_test_parse_or_fill_eof_t obj2;
    kpb_test_parse_or_fill_init(&obj);
    kpb_test_parse_or_fill_eof_init(&obj2);

    // test a standard read
    EXPECT_TRUE(kpb_test_parse_or_fill_parse_from_string(&obj, "\x12\x34\x56\x78\x90\xab\xcd\xef", 8));
    EXPECT_EQUAL(0x12345678, kpb_test_parse_or_fill_value1(&obj));
    EXPECT_EQUAL(0x90abcdef, kpb_test_other_parse_or_fill_value2(kpb_test_parse_or_fill_other(&obj)));

    // test that reads can't work out of bounds
    kpb_test_parse_or_fill_clear(&obj);
    EXPECT_FALSE(kpb_test_parse_or_fill_parse_from_string(&obj, "\x12\x34\x56\x78", 4));

    // test a complete fill read with 0s
    EXPECT_TRUE(kpb_test_parse_or_fill_parse_from_string_or_fill(&obj, "\x12\x34\x56\x78\x90\xab\xcd\xef", 8, 0));
    EXPECT_EQUAL(0x12345678, kpb_test_parse_or_fill_value1(&obj));
    EXPECT_EQUAL(0x90abcdef, kpb_test_other_parse_or_fill_value2(kpb_test_parse_or_fill_other(&obj)));

    // test a complete fill read with 1s
    EXPECT_TRUE(kpb_test_parse_or_fill_parse_from_string_or_fill(&obj, "\x12\x34\x56\x78\x90\xab\xcd\xef", 8, 1));
    EXPECT_EQUAL(0x12345678, kpb_test_parse_or_fill_value1(&obj));
    EXPECT_EQUAL(0x90abcdef, kpb_test_other_parse_or_fill_value2(kpb_test_parse_or_fill_other(&obj)));

    // test a fill read with 0s
    EXPECT_TRUE(kpb_test_parse_or_fill_parse_from_string_or_fill(&obj, "\x12\x34\x56", 3, 0));
    EXPECT_EQUAL(0x12345600, kpb_test_parse_or_fill_value1(&obj));
    EXPECT_EQUAL(0x00000000, kpb_test_other_parse_or_fill_value2(kpb_test_parse_or_fill_other(&obj)));

    // test a fill read with 1s
    EXPECT_TRUE(kpb_test_parse_or_fill_parse_from_string_or_fill(&obj, "\x12\x34\x56", 3, 1));
    EXPECT_EQUAL(0x123456ff, kpb_test_parse_or_fill_value1(&obj));
    EXPECT_EQUAL(0xffffffff, kpb_test_other_parse_or_fill_value2(kpb_test_parse_or_fill_other(&obj)));

    // test a sub type fill read with 0s
    EXPECT_TRUE(kpb_test_parse_or_fill_parse_from_string_or_fill(&obj, "\x12\x34\x56\x78\x89", 5, 0));
    EXPECT_EQUAL(0x12345678, kpb_test_parse_or_fill_value1(&obj));
    EXPECT_EQUAL(0x89000000, kpb_test_other_parse_or_fill_value2(kpb_test_parse_or_fill_other(&obj)));

    // test a sub type fill read with 1s
    EXPECT_TRUE(kpb_test_parse_or_fill_parse_from_string_or_fill(&obj, "\x12\x34\x56\x78\x89", 5, 1));
    EXPECT_EQUAL(0x12345678, kpb_test_parse_or_fill_value1(&obj));
    EXPECT_EQUAL(0x89ffffff, kpb_test_other_parse_or_fill_value2(kpb_test_parse_or_fill_other(&obj)));

    // make sure eof is hit (even if filling)
    EXPECT_EQUAL(0, kpb_test_parse_or_fill_eof_prefix(&obj2));
    EXPECT_EQUAL(0, kpb_test_parse_or_fill_eof_value_size(&obj2));
    EXPECT_TRUE(kpb_test_parse_or_fill_eof_parse_from_string_or_fill(&obj2, "\xa5\x5f\xf1", 3, 0));
    EXPECT_EQUAL(0xa, kpb_test_parse_or_fill_eof_prefix(&obj2));
    EXPECT_EQUAL(3, kpb_test_parse_or_fill_eof_value_size(&obj2));
    EXPECT_EQUAL(0x55, kpb_test_parse_or_fill_eof_value_at(&obj2, 0));
    EXPECT_EQUAL(0xff, kpb_test_parse_or_fill_eof_value_at(&obj2, 1));
    EXPECT_EQUAL(0x10, kpb_test_parse_or_fill_eof_value_at(&obj2, 2));

    kpb_test_parse_or_fill_eof_fini(&obj2);
    kpb_test_parse_or_fill_fini(&obj);
    return 0;
}
