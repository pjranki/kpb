#include "test_expressions.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/expressions.h"

int kpb_test_expressions(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_expressions_t expressions;
    kpb_test_expr_obj_t *obj = NULL;
    kpb_test_expressions_init(&expressions);

    EXPECT_TRUE(kpb_test_expressions_add_list_numbers(&expressions, 0xdeadbeef));
    EXPECT_TRUE(kpb_test_expressions_add_list_numbers(&expressions, 0xcafebabe));
    obj = kpb_test_expressions_add_list_objs(&expressions);
    EXPECT_NOT_NULL(obj);
    obj->value1 = 0xdead0001;
    obj = kpb_test_expressions_add_list_objs(&expressions);
    EXPECT_NOT_NULL(obj);
    obj->value1 = 0xdead0002;
    EXPECT_TRUE(kpb_test_expr_obj_add_list_numbers(obj, 0xdeadbeef));
    EXPECT_TRUE(kpb_test_expr_obj_add_list_numbers(obj, 0xbabebeef));
    obj = kpb_test_expressions_add_list_objs(&expressions);
    EXPECT_NOT_NULL(obj);
    obj->value1 = 0xdeadcafe;

    result_size = 0;
    result = kpb_test_expressions_serialize_to_string(&expressions, &result_size);
    EXPECT_NOT_NULL(result);
    kpb_io_free(result);

    EXPECT_EQUAL(5, kpb_test_expressions_op_addition(&expressions));
    EXPECT_EQUAL(3, kpb_test_expressions_op_subtract(&expressions));
    EXPECT_EQUAL(20, kpb_test_expressions_op_multiplication(&expressions));
    EXPECT_EQUAL(5, kpb_test_expressions_op_division(&expressions));
    EXPECT_EQUAL(1, kpb_test_expressions_op_modulo(&expressions));
    EXPECT_EQUAL(true, kpb_test_expressions_op_relational_lt_true(&expressions));
    EXPECT_EQUAL(false, kpb_test_expressions_op_relational_lt_false(&expressions));
    EXPECT_EQUAL(true, kpb_test_expressions_op_relational_gt_true(&expressions));
    EXPECT_EQUAL(false, kpb_test_expressions_op_relational_gt_false(&expressions));
    EXPECT_EQUAL(true, kpb_test_expressions_op_relational_lte_true(&expressions));
    EXPECT_EQUAL(false, kpb_test_expressions_op_relational_lte_false(&expressions));
    EXPECT_EQUAL(true, kpb_test_expressions_op_relational_gte_true(&expressions));
    EXPECT_EQUAL(false, kpb_test_expressions_op_relational_gte_false(&expressions));
    EXPECT_EQUAL(true, kpb_test_expressions_op_relational_eq_true(&expressions));
    EXPECT_EQUAL(false, kpb_test_expressions_op_relational_eq_false(&expressions));
    EXPECT_EQUAL(true, kpb_test_expressions_op_relational_ne_true(&expressions));
    EXPECT_EQUAL(false, kpb_test_expressions_op_relational_ne_false(&expressions));
    EXPECT_EQUAL(4, kpb_test_expressions_op_bitwise_lsh(&expressions));
    EXPECT_EQUAL(4, kpb_test_expressions_op_bitwise_rsh(&expressions));
    EXPECT_EQUAL(0x10, kpb_test_expressions_op_bitwise_and(&expressions));
    EXPECT_EQUAL(0x0c, kpb_test_expressions_op_bitwise_or(&expressions));
    EXPECT_EQUAL(0x55, kpb_test_expressions_op_bitwise_xor(&expressions));
    EXPECT_EQUAL(0x0f, kpb_test_expressions_op_bitwise_inv(&expressions));
    EXPECT_EQUAL(true, kpb_test_expressions_op_logical_and_true(&expressions));
    EXPECT_EQUAL(false, kpb_test_expressions_op_logical_and_false(&expressions));
    EXPECT_EQUAL(true, kpb_test_expressions_op_logical_or_true(&expressions));
    EXPECT_EQUAL(false, kpb_test_expressions_op_logical_or_false(&expressions));
    EXPECT_EQUAL(true, kpb_test_expressions_op_logical_not_true(&expressions));
    EXPECT_EQUAL(false, kpb_test_expressions_op_logical_not_false(&expressions));
    EXPECT_EQUAL(true, kpb_test_expressions_op_enum_eq_true(&expressions));
    EXPECT_EQUAL(1, kpb_test_expressions_op_ternary_choose_1(&expressions));
    EXPECT_EQUAL(2, kpb_test_expressions_op_ternary_choose_2(&expressions));
    EXPECT_EQUAL(3, kpb_test_expressions_op_ternary_choose_3(&expressions));
    EXPECT_EQUAL(4, kpb_test_expressions_op_ternary_choose_4(&expressions));
    EXPECT_EQUAL(1, kpb_test_expressions_op_list_index(&expressions));
    EXPECT_EQUAL(0xcafebabe, kpb_test_expressions_op_list_number(&expressions));
    EXPECT_EQUAL(0xdeadcafe, kpb_test_expressions_op_list_obj(&expressions));
    EXPECT_EQUAL(0xbabebeef, kpb_test_expressions_op_list_nested(&expressions));
    EXPECT_EQUAL(47, kpb_test_expressions_op_order_47(&expressions));
    EXPECT_EQUAL(37, kpb_test_expressions_op_order_37(&expressions));
    EXPECT_EQUAL(65, kpb_test_expressions_op_order_65(&expressions));
    EXPECT_EQUAL(77, kpb_test_expressions_op_order_77(&expressions));
    EXPECT_EQUAL(24, kpb_test_expressions_op_same_op_24(&expressions));
    EXPECT_EQUAL(true, kpb_test_expressions_op_logic_order_true(&expressions));

    kpb_test_expressions_fini(&expressions);
    return 0;
}
