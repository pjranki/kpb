#ifndef _H_KPB_TEST_IMPORTS_H_
#define _H_KPB_TEST_IMPORTS_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_imports(void);

#ifdef __cplusplus
};
#endif

#endif
