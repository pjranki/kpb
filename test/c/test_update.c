#include "test_update.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/update.h"

int kpb_test_update(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_obj_update_t update;
    kpb_test_single_parent_t *single = NULL;
    kpb_test_multi_parent_t *multi = NULL;

    kpb_test_obj_update_init(&update);

    // init should not call update (need to use '-default' on values)
    result_size = 0;
    result = kpb_test_obj_update_serialize_to_string(&update, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(
        // total_size1, flags, unused
        "\x20\x00\x00\x00" "\x00\x00\x00\x00" "\x00\x00\x00\x00" \
        // 
        "\x00\x00" "" \
        "\x00\x00" \
        "\x00" "" \
        "\x01" "\x00" \
        "\x00\x00" \
        "\x00" "" \
        "\x02" "\x00\x00" \
        "\x00\x00" \
        "\x00" "" \
        "\x00" \
        "\x00" "" \
        "\x00" "" \
        "\x00",
        32,
        result,
        result_size
    );
    kpb_io_free(result);
    EXPECT_EQUAL(32, kpb_test_obj_update_total_size1(&update));
    EXPECT_EQUAL(1, kpb_test_obj_update_asciiz_size1(&update));
    EXPECT_EQUAL(2, kpb_test_obj_update_widez_size1(&update));

    // clear calls update
    kpb_test_obj_update_clear(&update);
    result_size = 0;
    result = kpb_test_obj_update_serialize_to_string(&update, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(
        "\x20\x00\x00\x00" "\x00\x00\x00\x00" "\x00\x00\x00\x00" \
        "\x00\x00" "" \
        "\x00\x00" \
        "\x00" "" \
        "\x01" "\x00" \
        "\x00\x00" \
        "\x00" "" \
        "\x02" "\x00\x00" \
        "\x00\x00" \
        "\x00" "" \
        "\x00" \
        "\x00" "" \
        "\x00" "" \
        "\x00",
        32,
        result,
        result_size
    );
    kpb_io_free(result);
    EXPECT_EQUAL(32, kpb_test_obj_update_total_size1(&update));
    EXPECT_EQUAL(1, kpb_test_obj_update_asciiz_size1(&update));
    EXPECT_EQUAL(2, kpb_test_obj_update_widez_size1(&update));

    // change some values - others are (automatically) updated
    EXPECT_TRUE(kpb_test_obj_update_set_total_size1(&update, 0xdeadbeef));
    EXPECT_TRUE(kpb_test_obj_update_set_flags(&update, 0xdeadbeef));
    EXPECT_TRUE(kpb_test_obj_update_set_unused(&update, 0xcafebabe));
    EXPECT_TRUE(kpb_test_obj_update_set_buffer(&update, "\xab\x00\xcd", 3));
    EXPECT_TRUE(kpb_test_obj_update_add_buffer_list(&update, "\x12\x34\x56\x78\x90", 5));
    EXPECT_TRUE(kpb_test_obj_update_set_ascii(&update, "A"));
    EXPECT_TRUE(kpb_test_obj_update_set_asciiz(&update, "BC"));
    EXPECT_TRUE(kpb_test_obj_update_add_ascii_list(&update, "DEF"));
    EXPECT_TRUE(kpb_test_obj_update_set_wide(&update, L"a"));
    EXPECT_TRUE(kpb_test_obj_update_set_widez(&update, L"bc"));
    EXPECT_TRUE(kpb_test_obj_update_add_wide_list(&update, L"def"));

    single = kpb_test_obj_update_add_single_data_list(&update);
    EXPECT_NOT_NULL(single);
    EXPECT_TRUE(kpb_test_single_parent_set_data(single, "\x33\x33\x33", 3));

    multi = kpb_test_obj_update_add_multi_data_list(&update);
    EXPECT_NOT_NULL(multi);
    EXPECT_TRUE(kpb_test_multi_parent_set_data(multi, "\x55\x55\x55\x55\x55", 5));

    multi = kpb_test_obj_update_mutable_multi_data(&update);
    EXPECT_NOT_NULL(multi);
    EXPECT_TRUE(kpb_test_multi_parent_set_data(multi, "\x44\x44\x44\x44", 4));
    single = kpb_test_obj_update_mutable_single_data(&update);
    EXPECT_NOT_NULL(single);
    EXPECT_TRUE(kpb_test_single_parent_set_data(single, "\x11", 1));
    multi = kpb_test_single_parent_mutable_multi_data(single);
    EXPECT_NOT_NULL(multi);
    EXPECT_TRUE(kpb_test_multi_parent_set_data(multi, "\x22\x22", 2));

    EXPECT_EQUAL(74, kpb_test_obj_update_total_size1(&update));
    EXPECT_EQUAL(0xdeadbeef, kpb_test_obj_update_flags(&update));
    EXPECT_EQUAL(0, kpb_test_obj_update_unused(&update));
    EXPECT_EQUAL(3, kpb_test_obj_update_buffer_size1(&update));
    EXPECT_EQUAL(1, kpb_test_obj_update_buffer_list_count(&update));
    EXPECT_EQUAL(1, kpb_test_obj_update_ascii_size1(&update));
    EXPECT_EQUAL(3, kpb_test_obj_update_asciiz_size1(&update));
    EXPECT_EQUAL(1, kpb_test_obj_update_ascii_list_count(&update));
    EXPECT_EQUAL(2, kpb_test_obj_update_wide_size1(&update));
    EXPECT_EQUAL(6, kpb_test_obj_update_widez_size1(&update));
    EXPECT_EQUAL(1, kpb_test_obj_update_wide_list_count(&update));

    result_size = 0;
    result = kpb_test_obj_update_serialize_to_string(&update, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(
        "\x4a\x00\x00\x00" "\xef\xbe\xad\xde" "\x00\x00\x00\x00" \
        "\x03\x00" "\xab\x00\xcd" \
        "\x01\x00" "\x12\x34\x56\x78"\
        "\x01" "A" \
        "\x03" "BC\x00" \
        "\x01\x00" "DEF\x00" \
        "\x02" "a\x00" \
        "\x06" "b\x00" "c\x00" "\x00\x00" \
        "\x01\x00" "d\x00" "e\x00" \
        "\x01" "\x11" "\x02" "\x22\x22" \
        "\x01" "\x03" "\x33\x33\x33" "\x00" \
        "\x04" "\x44\x44\x44\x44" \
        "\x01" "\x05" "\x55\x55\x55\x55\x55",
        74,
        result, result_size
    );
    kpb_io_free(result);

    // changes in parent are reflected in children via instances
    kpb_test_obj_update_set_flags(&update, 0xdeadbeef);
    EXPECT_EQUAL(0xdeadbeef, kpb_test_single_parent_parent_flags(kpb_test_obj_update_single_data(&update)));
    EXPECT_EQUAL(0xdeadbeef, kpb_test_multi_parent_flags(kpb_test_single_parent_multi_data(kpb_test_obj_update_single_data(&update))));
    kpb_test_obj_update_set_flags(&update, 0xcafebabe);
    EXPECT_EQUAL(0xcafebabe, kpb_test_single_parent_parent_flags(kpb_test_obj_update_single_data(&update)));
    EXPECT_EQUAL(0xcafebabe, kpb_test_multi_parent_flags(kpb_test_single_parent_multi_data(kpb_test_obj_update_single_data(&update))));

    // clear
    kpb_test_obj_update_clear(&update);
    result_size = 0;
    result = kpb_test_obj_update_serialize_to_string(&update, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(
        "\x20\x00\x00\x00" "\x00\x00\x00\x00" "\x00\x00\x00\x00" \
        "\x00\x00" "" \
        "\x00\x00" \
        "\x00" "" \
        "\x01" "\x00" \
        "\x00\x00" \
        "\x00" "" \
        "\x02" "\x00\x00" \
        "\x00\x00" \
        "\x00" "" \
        "\x00" \
        "\x00" "" \
        "\x00" "" \
        "\x00",
        32,
        result, result_size
    );
    kpb_io_free(result);

    // parse from string
    EXPECT_TRUE(kpb_test_obj_update_parse_from_string(&update,
        "\x4a\x00\x00\x00" "\xef\xbe\xad\xde" "\xa5\xa5\xa5\xa5" \
        "\x03\x00" "\xab\x00\xcd" \
        "\x01\x00" "\x12\x34\x56\x78"\
        "\x01" "A" \
        "\x03" "BC\x00" \
        "\x01\x00" "DEF\x00" \
        "\x02" "a\x00" \
        "\x06" "b\x00" "c\x00" "\x00\x00" \
        "\x01\x00" "d\x00" "e\x00" \
        "\x01" "\x11" "\x02" "\x22\x22" \
        "\x01" "\x03" "\x33\x33\x33" "\x00" \
        "\x04" "\x44\x44\x44\x44" \
        "\x01" "\x05" "\x55\x55\x55\x55\x55",
        74
    ));
    EXPECT_EQUAL(74, kpb_test_obj_update_total_size1(&update));
    EXPECT_EQUAL(0xdeadbeef, kpb_test_obj_update_flags(&update));
    EXPECT_EQUAL(0xa5a5a5a5, kpb_test_obj_update_unused(&update));  // this is correct, parse/read should NOT call update
    EXPECT_EQUAL(3, kpb_test_obj_update_buffer_size1(&update));
    EXPECT_BYTES_EQUAL("\xab\x00\xcd", 3, kpb_test_obj_update_buffer(&update), kpb_test_obj_update_buffer_size(&update));
    EXPECT_EQUAL(1, kpb_test_obj_update_buffer_list_count(&update));
    EXPECT_EQUAL(1, kpb_test_obj_update_buffer_list_size(&update));
    EXPECT_BYTES_EQUAL("\x12\x34\x56\x78", 4, kpb_test_obj_update_buffer_list_at(&update, 0), kpb_test_obj_update_buffer_list_size_at(&update, 0));
    EXPECT_EQUAL(1, kpb_test_obj_update_ascii_size1(&update));
    EXPECT_STRING_EQUAL("A", kpb_test_obj_update_ascii(&update));
    EXPECT_EQUAL(3, kpb_test_obj_update_asciiz_size1(&update));
    EXPECT_STRING_EQUAL("BC", kpb_test_obj_update_asciiz(&update));
    EXPECT_EQUAL(1, kpb_test_obj_update_ascii_list_count(&update));
    EXPECT_EQUAL(1, kpb_test_obj_update_ascii_list_size(&update));
    EXPECT_STRING_EQUAL("DEF", kpb_test_obj_update_ascii_list_at(&update, 0));
    EXPECT_EQUAL(2, kpb_test_obj_update_wide_size1(&update));
    EXPECT_STRING_EQUAL(L"a", kpb_test_obj_update_wide(&update));
    EXPECT_EQUAL(6, kpb_test_obj_update_widez_size1(&update));
    EXPECT_STRING_EQUAL(L"bc", kpb_test_obj_update_widez(&update));
    EXPECT_EQUAL(1, kpb_test_obj_update_wide_list_count(&update));
    EXPECT_EQUAL(1, kpb_test_obj_update_wide_list_size(&update));
    EXPECT_STRING_EQUAL(L"de", kpb_test_obj_update_wide_list_at(&update, 0));
    EXPECT_EQUAL(1, kpb_test_single_parent_data_size1(kpb_test_obj_update_single_data(&update)));
    EXPECT_BYTES_EQUAL("\x11", 1, kpb_test_single_parent_data(kpb_test_obj_update_single_data(&update)), kpb_test_single_parent_data_size(kpb_test_obj_update_single_data(&update)));
    EXPECT_EQUAL(2, kpb_test_multi_parent_data_size1(kpb_test_single_parent_multi_data(kpb_test_obj_update_single_data(&update))));
    EXPECT_BYTES_EQUAL("\x22\x22", 2,
        kpb_test_multi_parent_data(kpb_test_single_parent_multi_data(kpb_test_obj_update_single_data(&update))),
        kpb_test_multi_parent_data_size(kpb_test_single_parent_multi_data(kpb_test_obj_update_single_data(&update)))
    );
    EXPECT_EQUAL(1, kpb_test_obj_update_single_data_list_count(&update));
    EXPECT_EQUAL(1, kpb_test_obj_update_single_data_list_size(&update));
    EXPECT_EQUAL(3, kpb_test_single_parent_data_size1(kpb_test_obj_update_single_data_list_at(&update, 0)));
    EXPECT_BYTES_EQUAL("\x33\x33\x33", 3,
        kpb_test_single_parent_data(kpb_test_obj_update_single_data_list_at(&update, 0)),
        kpb_test_single_parent_data_size(kpb_test_obj_update_single_data_list_at(&update, 0))
    );
    EXPECT_EQUAL(0, kpb_test_multi_parent_data_size1(kpb_test_single_parent_multi_data(kpb_test_obj_update_single_data_list_at(&update, 0))));
    EXPECT_EQUAL(0,
        kpb_test_multi_parent_data_size(kpb_test_single_parent_multi_data(kpb_test_obj_update_single_data_list_at(&update, 0)))
    );
    EXPECT_EQUAL(4, kpb_test_multi_parent_data_size1(kpb_test_obj_update_multi_data(&update)));
    EXPECT_BYTES_EQUAL("\x44\x44\x44\x44", 4,
        kpb_test_multi_parent_data(kpb_test_obj_update_multi_data(&update)),
        kpb_test_multi_parent_data_size(kpb_test_obj_update_multi_data(&update))
    );
    EXPECT_EQUAL(1, kpb_test_obj_update_multi_data_list_count(&update));
    EXPECT_EQUAL(1, kpb_test_obj_update_multi_data_list_size(&update));
    EXPECT_EQUAL(5, kpb_test_multi_parent_data_size1(kpb_test_obj_update_multi_data_list_at(&update, 0)));
    EXPECT_BYTES_EQUAL("\x55\x55\x55\x55\x55", 5,
        kpb_test_multi_parent_data(kpb_test_obj_update_multi_data_list_at(&update, 0)),
        kpb_test_multi_parent_data_size(kpb_test_obj_update_multi_data_list_at(&update, 0))
    );

    kpb_test_obj_update_fini(&update);
    return 0;
}
