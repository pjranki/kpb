#ifndef _H_PROCESS_VALUE_BOOL_H_
#define _H_PROCESS_VALUE_BOOL_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <string.h>

#include <kpb_io.h>

#include "c/process_value.h"

struct process_value_bool {
    bool invert_logic_;
};

void process_value_bool_init(struct process_value_bool *_this, bool invert_logic);
bool process_value_bool_getter(struct process_value_bool *_this, bool value);
bool process_value_bool_setter(struct process_value_bool *_this, bool value);
void process_value_bool_fini(struct process_value_bool *_this);

#ifdef __cplusplus
};
#endif

#endif
