#include "test_constructor_recursion.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/constructor_recursion.h"

int kpb_test_constructor_recursion(void)
{
    kpb_test_constructor_recursion_t obj;
    kpb_test_constructor_recursion_init(&obj);
    kpb_test_constructor_recursion_clear(&obj);
    kpb_test_constructor_recursion_fini(&obj);
    return 0;
}
