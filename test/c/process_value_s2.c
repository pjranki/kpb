#include "process_value_s2.h"

#include <kpb_io.h>

void process_value_s2_init(struct process_value_s2 *_this)
{
    memset(_this, 0, sizeof(*_this));
}

int16_t process_value_s2_getter(struct process_value_s2 *_this, int16_t value)
{
    (void)_this;
    return value + 10;
}

int16_t process_value_s2_setter(struct process_value_s2 *_this, int16_t value)
{
    (void)_this;
    return value - 5;
}

void process_value_s2_fini(struct process_value_s2 *_this)
{
    memset(_this, 0, sizeof(*_this));
}
