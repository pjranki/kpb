#include "process_value_root.h"

#include <kpb_io.h>

void process_value_root_init(struct process_value_root *_this, const kpb_test_process_value_t *_root, uint8_t arg2)
{
    memset(_this, 0, sizeof(*_this));
    _this->root_ = (kpb_test_process_value_t *)_root;
    _this->arg2_ = arg2;
}

const kpb_test_process_value_t *process_value_root_getter(struct process_value_root *_this, const kpb_test_process_value_t *value)
{
    if ( _this->arg2_ == 0xaa )
    {
        kpb_test_process_value_set_getter_count(_this->root_, kpb_test_process_value_getter_count(_this->root_) + 1);
    }
    return value;
}

kpb_test_process_value_t *process_value_root_setter(struct process_value_root *_this, kpb_test_process_value_t *value)
{
    if ( _this->arg2_ == 0xaa )
    {
        kpb_test_process_value_set_setter_count(_this->root_, kpb_test_process_value_setter_count(_this->root_) + 1);
    }
    return value;
}

void process_value_root_fini(struct process_value_root *_this)
{
    memset(_this, 0, sizeof(*_this));
}
