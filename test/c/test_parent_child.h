#ifndef _H_KPB_TEST_PARENT_CHILD_H_
#define _H_KPB_TEST_PARENT_CHILD_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_parent_child(void);

#ifdef __cplusplus
};
#endif

#endif
