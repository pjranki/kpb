#include "process_value_bool.h"

#include <kpb_io.h>

void process_value_bool_init(struct process_value_bool *_this, bool invert_logic)
{
    memset(_this, 0, sizeof(*_this));
    _this->invert_logic_ = invert_logic;
}

bool process_value_bool_getter(struct process_value_bool *_this, bool value)
{
    return _this->invert_logic_ ? !value : value;
}

bool process_value_bool_setter(struct process_value_bool *_this, bool value)
{
    return _this->invert_logic_ ? !value : value;
}

void process_value_bool_fini(struct process_value_bool *_this)
{
    memset(_this, 0, sizeof(*_this));
}
