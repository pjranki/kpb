#ifndef _H_PROCESS_VALUE_B3_H_
#define _H_PROCESS_VALUE_B3_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <string.h>

#include <kpb_io.h>

#include "c/process_value.h"

struct process_value_b3 {
    const kpb_test_process_value_t *root_;
};

void process_value_b3_init(struct process_value_b3 *_this, const kpb_test_process_value_t *_root);
uint8_t process_value_b3_getter(struct process_value_b3 *_this, uint8_t value);
uint8_t process_value_b3_setter(struct process_value_b3 *_this, uint8_t value);
void process_value_b3_fini(struct process_value_b3 *_this);

#ifdef __cplusplus
};
#endif

#endif
