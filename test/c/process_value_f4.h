#ifndef _H_PROCESS_VALUE_F4_H_
#define _H_PROCESS_VALUE_F4_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <string.h>

#include <kpb_io.h>

#include "c/process_value.h"

struct process_value_f4 {
    bool multiple_float_by_4_;
};

void process_value_f4_init(struct process_value_f4 *_this, bool multiple_float_by_4);
float process_value_f4_getter(struct process_value_f4 *_this, float value);
float process_value_f4_setter(struct process_value_f4 *_this, float value);
void process_value_f4_fini(struct process_value_f4 *_this);

#ifdef __cplusplus
};
#endif

#endif
