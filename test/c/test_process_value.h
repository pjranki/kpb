#ifndef _H_KPB_TEST_PROCESS_VALUE_H_
#define _H_KPB_TEST_PROCESS_VALUE_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_process_value(void);

#ifdef __cplusplus
};
#endif

#endif
