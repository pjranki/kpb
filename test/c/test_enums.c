#include "test_enums.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/enums.h"

int kpb_test_enums(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_enums_t enums;
    kpb_test_enums_init(&enums);
    result_size = 0;
    result = kpb_test_enums_serialize_to_string(&enums, &result_size);
    EXPECT_NOT_NULL(result);
    kpb_io_free(result);
    kpb_test_enums_fini(&enums);

    kpb_test_sub_enums_t sub_enums;
    kpb_test_sub_enums_init(&sub_enums);
    result_size = 0;
    result = kpb_test_sub_enums_serialize_to_string(&sub_enums, &result_size);
    EXPECT_NOT_NULL(result);
    kpb_io_free(result);
    kpb_test_sub_enums_fini(&sub_enums);

    uint8_t enum_value8 = KPB_TEST_ENUMS_TEST_ENUM_FOOBAR;
    EXPECT_EQUAL(1, enum_value8);
    enum_value8 = KPB_TEST_ENUMS_TEST_ENUM_CAFEBABE;
    EXPECT_EQUAL(2, enum_value8);
    uint16_t enum_value16 = KPB_TEST_SUB_ENUMS_TEST_ENUM_FOOBAR;
    EXPECT_EQUAL(0xffff, enum_value16);
    enum_value16 = KPB_TEST_SUB_ENUMS_TEST_ENUM_CAFEBABE;
    EXPECT_EQUAL(0x75a5, enum_value16);
    return 0;
}
