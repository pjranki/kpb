#include "test_packet.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/packet.h"

#define TEST_EMPTY_PACKET           "\x0a\x00\x00\x00" "\x02\x00\x00\x00" "\x00\x00"
#define TEST_EMPTY_PACKET_SIZE      (sizeof(TEST_EMPTY_PACKET)-1)

#define TEST_DATA                   "\x41\x42\xff\x00\xcc"
#define TEST_DATA_SIZE              (sizeof(TEST_DATA)-1)

#define TEST_DATA_PACKET            "\x0f\x00\x00\x00" "\x07\x00\x00\x00" "\x05\x00" TEST_DATA
#define TEST_DATA_PACKET_SIZE       (sizeof(TEST_DATA_PACKET)-1)

int kpb_test_packet(void)
{
    char *result = NULL;
    size_t result_size = 0;
    kpb_test_packet_t packet;
    kpb_test_packet_init(&packet);

    // -default is used to create the default sizes for the packet when empty
    EXPECT_EQUAL(8, kpb_test_packet_header_header_size_(kpb_test_packet_header(&packet)));
    EXPECT_EQUAL(2, kpb_test_packet_header_body_size_(kpb_test_packet_header(&packet)));
    EXPECT_EQUAL(10, kpb_test_packet_header_total_size_(kpb_test_packet_header(&packet)));

    // serialize as expected
    result = kpb_test_packet_serialize_to_string(&packet, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(TEST_EMPTY_PACKET, TEST_EMPTY_PACKET_SIZE, result, result_size);
    kpb_io_free(result);

    // calling Clear() calls Update() -> and should calculate the same values as -default
    kpb_test_packet_clear(&packet);
    EXPECT_EQUAL(8, kpb_test_packet_header_header_size_(kpb_test_packet_header(&packet)));
    EXPECT_EQUAL(2, kpb_test_packet_header_body_size_(kpb_test_packet_header(&packet)));
    EXPECT_EQUAL(10, kpb_test_packet_header_total_size_(kpb_test_packet_header(&packet)));

    // serialize as expected
    result = kpb_test_packet_serialize_to_string(&packet, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(TEST_EMPTY_PACKET, TEST_EMPTY_PACKET_SIZE, result, result_size);
    kpb_io_free(result);

    // adding some data will update all size fields
    kpb_test_packet_body_t *body = kpb_test_packet_mutable_body(&packet);
    EXPECT_NOT_NULL(body);
    EXPECT_TRUE(kpb_test_packet_body_set_data(body, TEST_DATA, TEST_DATA_SIZE));
    EXPECT_EQUAL(8, kpb_test_packet_header_header_size_(kpb_test_packet_header(&packet)));
    EXPECT_EQUAL(7, kpb_test_packet_header_body_size_(kpb_test_packet_header(&packet)));
    EXPECT_EQUAL(15, kpb_test_packet_header_total_size_(kpb_test_packet_header(&packet)));

    // serialize as expected
    result = kpb_test_packet_serialize_to_string(&packet, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(TEST_DATA_PACKET, TEST_DATA_PACKET_SIZE, result, result_size);
    kpb_io_free(result);

    // clear
    kpb_test_packet_clear(&packet);

    // serialize as expected
    result = kpb_test_packet_serialize_to_string(&packet, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(TEST_EMPTY_PACKET, TEST_EMPTY_PACKET_SIZE, result, result_size);
    kpb_io_free(result);

    // parse as expected
    EXPECT_TRUE(kpb_test_packet_parse_from_string(&packet, TEST_DATA_PACKET, TEST_DATA_PACKET_SIZE));
    EXPECT_EQUAL(8, kpb_test_packet_header_header_size_(kpb_test_packet_header(&packet)));
    EXPECT_EQUAL(7, kpb_test_packet_header_body_size_(kpb_test_packet_header(&packet)));
    EXPECT_EQUAL(15, kpb_test_packet_header_total_size_(kpb_test_packet_header(&packet)));
    const char *data = kpb_test_packet_body_data(kpb_test_packet_body(&packet));
    size_t data_size = kpb_test_packet_body_data_size(kpb_test_packet_body(&packet));
    EXPECT_BYTES_EQUAL(TEST_DATA, TEST_DATA_SIZE, data, data_size);

    kpb_test_packet_fini(&packet);
    return 0;
}
