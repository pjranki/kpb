#include "test_parent_child.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/parent_child.h"

int kpb_test_parent_child(void)
{
    kpb_test_parent_child_t child;
    kpb_test_parent_child_init(&child);
    EXPECT_EQUAL(42, kpb_test_parent_child_parent_id(&child));
    kpb_test_parent_child_fini(&child);
    return 0;
}
