#include "test_update_member.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/update_member.h"

int kpb_test_update_member(void)
{
    kpb_test_member_update_t root;
    kpb_test_member_update_obj_t *obj = NULL;
    kpb_test_member_update_sub_obj_t *sub = NULL;
    kpb_test_member_update_init(&root);

    EXPECT_EQUAL(0, kpb_test_member_update_key(&root));
    obj = kpb_test_member_update_mutable_obj(&root);
    EXPECT_NOT_NULL(obj);

    EXPECT_EQUAL(0, kpb_test_member_update_key(&root));
    EXPECT_EQUAL(0, kpb_test_member_update_obj_key(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u1(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u2(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u4(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u8(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_number(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_obj(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_obj_list(obj));

    EXPECT_TRUE(kpb_test_member_update_obj_set_u1(obj, 1));
    EXPECT_EQUAL(KPB_TEST_MEMBER_UPDATE_KEY_U1, kpb_test_member_update_key(&root));
    EXPECT_EQUAL(KPB_TEST_MEMBER_UPDATE_KEY_U1, kpb_test_member_update_obj_key(obj));
    EXPECT_EQUAL(true, kpb_test_member_update_obj_has_u1(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u2(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u4(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u8(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_number(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_obj(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_obj_list(obj));

    EXPECT_TRUE(kpb_test_member_update_obj_set_u2(obj, 1));
    EXPECT_EQUAL(KPB_TEST_MEMBER_UPDATE_KEY_U2, kpb_test_member_update_key(&root));
    EXPECT_EQUAL(KPB_TEST_MEMBER_UPDATE_KEY_U2, kpb_test_member_update_obj_key(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u1(obj));
    EXPECT_EQUAL(true, kpb_test_member_update_obj_has_u2(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u4(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u8(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_number(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_obj(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_obj_list(obj));

    EXPECT_TRUE(kpb_test_member_update_obj_set_u4(obj, 1));
    EXPECT_EQUAL(KPB_TEST_MEMBER_UPDATE_KEY_U4, kpb_test_member_update_key(&root));
    EXPECT_EQUAL(KPB_TEST_MEMBER_UPDATE_KEY_U4, kpb_test_member_update_obj_key(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u1(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u2(obj));
    EXPECT_EQUAL(true, kpb_test_member_update_obj_has_u4(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u8(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_number(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_obj(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_obj_list(obj));

    EXPECT_TRUE(kpb_test_member_update_obj_set_u8(obj, 1));
    EXPECT_EQUAL(KPB_TEST_MEMBER_UPDATE_KEY_U8, kpb_test_member_update_key(&root));
    EXPECT_EQUAL(KPB_TEST_MEMBER_UPDATE_KEY_U8, kpb_test_member_update_obj_key(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u1(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u2(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u4(obj));
    EXPECT_EQUAL(true, kpb_test_member_update_obj_has_u8(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_number(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_obj(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_obj_list(obj));

    EXPECT_TRUE(kpb_test_member_update_obj_set_number(obj, 1));
    EXPECT_EQUAL(KPB_TEST_MEMBER_UPDATE_KEY_NUMBER, kpb_test_member_update_key(&root));
    EXPECT_EQUAL(KPB_TEST_MEMBER_UPDATE_KEY_NUMBER, kpb_test_member_update_obj_key(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u1(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u2(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u4(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u8(obj));
    EXPECT_EQUAL(true, kpb_test_member_update_obj_has_number(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_obj(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_obj_list(obj));

    sub = kpb_test_member_update_obj_mutable_obj(obj);
    EXPECT_NOT_NULL(sub);
    EXPECT_EQUAL(KPB_TEST_MEMBER_UPDATE_KEY_OBJ, kpb_test_member_update_key(&root));
    EXPECT_EQUAL(KPB_TEST_MEMBER_UPDATE_KEY_OBJ, kpb_test_member_update_obj_key(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u1(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u2(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u4(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u8(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_number(obj));
    EXPECT_EQUAL(true, kpb_test_member_update_obj_has_obj(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_obj_list(obj));

    sub = kpb_test_member_update_obj_add_obj_list(obj);
    EXPECT_NOT_NULL(sub);
    EXPECT_EQUAL(KPB_TEST_MEMBER_UPDATE_KEY_OBJ_LIST, kpb_test_member_update_key(&root));
    EXPECT_EQUAL(KPB_TEST_MEMBER_UPDATE_KEY_OBJ_LIST, kpb_test_member_update_obj_key(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u1(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u2(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u4(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u8(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_number(obj));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_obj(obj));
    EXPECT_EQUAL(true, kpb_test_member_update_obj_has_obj_list(obj));

    kpb_test_member_update_clear(&root);
    EXPECT_EQUAL(0, kpb_test_member_update_key(&root));
    EXPECT_EQUAL(0, kpb_test_member_update_obj_key(kpb_test_member_update_obj(&root)));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u1(kpb_test_member_update_obj(&root)));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u2(kpb_test_member_update_obj(&root)));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u4(kpb_test_member_update_obj(&root)));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_u8(kpb_test_member_update_obj(&root)));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_number(kpb_test_member_update_obj(&root)));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_obj(kpb_test_member_update_obj(&root)));
    EXPECT_EQUAL(false, kpb_test_member_update_obj_has_obj_list(kpb_test_member_update_obj(&root)));

    kpb_test_member_update_fini(&root);
    return 0;
}
