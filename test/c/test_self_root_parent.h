#ifndef _H_KPB_TEST_SELF_ROOT_PARENT_H_
#define _H_KPB_TEST_SELF_ROOT_PARENT_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_self_root_parent(void);

#ifdef __cplusplus
};
#endif

#endif
