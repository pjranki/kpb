#ifndef _H_PROCESS_VALUE_ROOT_H_
#define _H_PROCESS_VALUE_ROOT_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <string.h>

#include <kpb_io.h>

#include "c/process_value.h"

struct process_value_root {
    kpb_test_process_value_t *root_;
    uint8_t arg2_;
};

void process_value_root_init(struct process_value_root *_this, const kpb_test_process_value_t *_root, uint8_t arg2);
const kpb_test_process_value_t *process_value_root_getter(struct process_value_root *_this, const kpb_test_process_value_t *value);
kpb_test_process_value_t *process_value_root_setter(struct process_value_root *_this, kpb_test_process_value_t *value);
void process_value_root_fini(struct process_value_root *_this);

#ifdef __cplusplus
};
#endif

#endif
