#include "process_value_b3.h"

#include <kpb_io.h>

void process_value_b3_init(struct process_value_b3 *_this, const kpb_test_process_value_t *_root)
{
    memset(_this, 0, sizeof(*_this));
    _this->root_ = _root;
}

uint8_t process_value_b3_getter(struct process_value_b3 *_this, uint8_t value)
{
    return value + kpb_test_process_value_memset_value(_this->root_);
}

uint8_t process_value_b3_setter(struct process_value_b3 *_this, uint8_t value)
{
    return value - kpb_test_process_value_memset_value(_this->root_);
}

void process_value_b3_fini(struct process_value_b3 *_this)
{
    memset(_this, 0, sizeof(*_this));
}
