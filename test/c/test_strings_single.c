#include "test_strings_single.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/strings_single.h"
#include "c/strings_utf16be.h"
#include "c/strings_utf8.h"

int kpb_test_strings_single(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_strings_single_t strings;
    kpb_test_strings_single_init(&strings);
    result = kpb_test_strings_single_serialize_to_string(&strings, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 18, result, result_size);
    kpb_io_free(result);
    kpb_test_strings_single_set_str_ascii(&strings, "\xfe");
    kpb_test_strings_single_set_str_wchar(&strings, L"\u8cfe");
    kpb_test_strings_single_set_strz_ascii(&strings, "\xfe");
    kpb_test_strings_single_set_strz_wchar(&strings, L"\u8cfe");
    kpb_test_strings_single_set_strz_ascii_sized(&strings, "\xfe");
    kpb_test_strings_single_set_strz_wchar_sized(&strings, L"\u8cfe");
    result = kpb_test_strings_single_serialize_to_string(&strings, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xfe\x00\xfe\x8c\x00\x00\xfe\x00\xfe\x8c\x00\x00\xfe\x00\x00\xfe\x8c\x00\x00\x00\x00", 21, result, result_size);
    kpb_io_free(result);
    kpb_test_strings_single_clear(&strings);
    result = kpb_test_strings_single_serialize_to_string(&strings, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 18, result, result_size);
    kpb_io_free(result);
    kpb_test_strings_single_set_str_ascii(&strings, "\xfe\xba");
    kpb_test_strings_single_set_str_wchar(&strings, L"\u8cfe\u09ba");
    kpb_test_strings_single_set_strz_ascii(&strings, "\xfe\xba");
    kpb_test_strings_single_set_strz_wchar(&strings, L"\u8cfe\u09ba");
    kpb_test_strings_single_set_strz_ascii_sized(&strings, "\xfe\xba");
    kpb_test_strings_single_set_strz_wchar_sized(&strings, L"\u8cfe\u09ba");
    result = kpb_test_strings_single_serialize_to_string(&strings, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xfe\xba\xfe\x8c\xba\x09\xfe\xba\x00\xfe\x8c\xba\x09\x00\x00\xfe\xba\x00\xfe\x8c\xba\x09\x00\x00", 24, result, result_size);
    kpb_io_free(result);
    kpb_test_strings_single_set_str_ascii(&strings, "\xfe\xba\x09");
    kpb_test_strings_single_set_str_wchar(&strings, L"\u8cfe\u09ba\u6587");
    kpb_test_strings_single_set_strz_ascii(&strings, "\xfe\xba\x09");
    kpb_test_strings_single_set_strz_wchar(&strings, L"\u8cfe\u09ba\u6587");
    kpb_test_strings_single_set_strz_ascii_sized(&strings, "\xfe\xba\x09");
    kpb_test_strings_single_set_strz_wchar_sized(&strings, L"\u8cfe\u09ba\u6587");
    result = kpb_test_strings_single_serialize_to_string(&strings, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xfe\xba\xfe\x8c\xba\x09\xfe\xba\x09\x00\xfe\x8c\xba\x09\x87\x65\x00\x00\xfe\xba\x00\xfe\x8c\xba\x09\x00\x00", 27, result, result_size);
    kpb_io_free(result);

    kpb_test_strings_single_clear(&strings);
    EXPECT_TRUE(kpb_test_strings_single_parse_from_string(&strings, "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 18));
    EXPECT_STRING_EQUAL("", kpb_test_strings_single_str_ascii(&strings));
    EXPECT_STRING_EQUAL(L"", kpb_test_strings_single_str_wchar(&strings));
    EXPECT_STRING_EQUAL("", kpb_test_strings_single_strz_ascii(&strings));
    EXPECT_STRING_EQUAL(L"", kpb_test_strings_single_strz_wchar(&strings));
    EXPECT_STRING_EQUAL("", kpb_test_strings_single_strz_ascii_sized(&strings));
    EXPECT_STRING_EQUAL(L"", kpb_test_strings_single_strz_wchar_sized(&strings));
    EXPECT_TRUE(kpb_test_strings_single_parse_from_string(&strings, "\xfe\x00\xfe\x8c\x00\x00\xfe\x00\xfe\x8c\x00\x00\xfe\x00\x00\xfe\x8c\x00\x00\x00\x00", 21));
    EXPECT_STRING_EQUAL("\xfe", kpb_test_strings_single_str_ascii(&strings));
    EXPECT_STRING_EQUAL(L"\u8cfe", kpb_test_strings_single_str_wchar(&strings));
    EXPECT_STRING_EQUAL("\xfe", kpb_test_strings_single_strz_ascii(&strings));
    EXPECT_STRING_EQUAL(L"\u8cfe", kpb_test_strings_single_strz_wchar(&strings));
    EXPECT_STRING_EQUAL("\xfe", kpb_test_strings_single_strz_ascii_sized(&strings));
    EXPECT_STRING_EQUAL(L"\u8cfe", kpb_test_strings_single_strz_wchar_sized(&strings));
    EXPECT_TRUE(kpb_test_strings_single_parse_from_string(&strings, "\xfe\xba\xfe\x8c\xba\x09\xfe\xba\x00\xfe\x8c\xba\x09\x00\x00\xfe\xba\x00\xfe\x8c\xba\x09\x00\x00", 24));
    EXPECT_STRING_EQUAL("\xfe\xba", kpb_test_strings_single_str_ascii(&strings));
    EXPECT_STRING_EQUAL(L"\u8cfe\u09ba", kpb_test_strings_single_str_wchar(&strings));
    EXPECT_STRING_EQUAL("\xfe\xba", kpb_test_strings_single_strz_ascii(&strings));
    EXPECT_STRING_EQUAL(L"\u8cfe\u09ba", kpb_test_strings_single_strz_wchar(&strings));
    EXPECT_STRING_EQUAL("\xfe\xba", kpb_test_strings_single_strz_ascii_sized(&strings));
    EXPECT_STRING_EQUAL(L"\u8cfe\u09ba", kpb_test_strings_single_strz_wchar_sized(&strings));
    EXPECT_TRUE(kpb_test_strings_single_parse_from_string(&strings, "\xfe\xba\xfe\x8c\xba\x09\xfe\xba\x09\x00\xfe\x8c\xba\x09\x87\x65\x00\x00\xfe\xba\x00\xfe\x8c\xba\x09\x00\x00", 27));
    EXPECT_STRING_EQUAL("\xfe\xba", kpb_test_strings_single_str_ascii(&strings));
    EXPECT_STRING_EQUAL(L"\u8cfe\u09ba", kpb_test_strings_single_str_wchar(&strings));
    EXPECT_STRING_EQUAL("\xfe\xba\x09", kpb_test_strings_single_strz_ascii(&strings));
    EXPECT_STRING_EQUAL(L"\u8cfe\u09ba\u6587", kpb_test_strings_single_strz_wchar(&strings));
    EXPECT_STRING_EQUAL("\xfe\xba", kpb_test_strings_single_strz_ascii_sized(&strings));
    EXPECT_STRING_EQUAL(L"\u8cfe\u09ba", kpb_test_strings_single_strz_wchar_sized(&strings));

    kpb_test_strings_single_fini(&strings);

    kpb_test_strings_utf16be_t utf16be;
    kpb_test_strings_utf16be_init(&utf16be);

    result = kpb_test_strings_utf16be_serialize_to_string(&utf16be, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00" "\x00\x00" "\x00\x00\x00\x00\x00\x00", 4 + 2 + 6, result, result_size);
    kpb_io_free(result);
    kpb_test_strings_utf16be_set_text(&utf16be, L"\u8cfe");
    kpb_test_strings_utf16be_set_textz(&utf16be, L"\u8cfe");
    kpb_test_strings_utf16be_set_textz_sized(&utf16be, L"\u8cfe");
    result = kpb_test_strings_utf16be_serialize_to_string(&utf16be, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x8c\xfe\x00\x00" "\x8c\xfe\x00\x00" "\x8c\xfe\x00\x00\x00\x00", 4 + 4 + 6, result, result_size);
    kpb_io_free(result);
    kpb_test_strings_utf16be_clear(&utf16be);
    result = kpb_test_strings_utf16be_serialize_to_string(&utf16be, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00" "\x00\x00" "\x00\x00\x00\x00\x00\x00", 4 + 2 + 6, result, result_size);
    kpb_io_free(result);
    kpb_test_strings_utf16be_set_text(&utf16be, L"\u8cfe\u09ba");
    kpb_test_strings_utf16be_set_textz(&utf16be, L"\u8cfe\u09ba");
    kpb_test_strings_utf16be_set_textz_sized(&utf16be, L"\u8cfe\u09ba");
    result = kpb_test_strings_utf16be_serialize_to_string(&utf16be, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x8c\xfe\x09\xba" "\x8c\xfe\x09\xba\x00\x00" "\x8c\xfe\x09\xba\x00\x00", 4 + 6 + 6, result, result_size);
    kpb_io_free(result);
    kpb_test_strings_utf16be_set_text(&utf16be, L"\u8cfe\u09ba\u6587");
    kpb_test_strings_utf16be_set_textz(&utf16be, L"\u8cfe\u09ba\u6587");
    kpb_test_strings_utf16be_set_textz_sized(&utf16be, L"\u8cfe\u09ba\u6587");
    result = kpb_test_strings_utf16be_serialize_to_string(&utf16be, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x8c\xfe\x09\xba" "\x8c\xfe\x09\xba\x65\x87\x00\x00" "\x8c\xfe\x09\xba\x00\x00", 4 + 8 + 6, result, result_size);
    kpb_io_free(result);

    kpb_test_strings_utf16be_clear(&utf16be);
    EXPECT_TRUE(kpb_test_strings_utf16be_parse_from_string(&utf16be, "\x00\x00\x00\x00" "\x00\x00" "\x00\x00\x00\x00\x00\x00", 4 + 2 + 6));
    EXPECT_STRING_EQUAL(L"", kpb_test_strings_utf16be_text(&utf16be));
    EXPECT_STRING_EQUAL(L"", kpb_test_strings_utf16be_textz(&utf16be));
    EXPECT_STRING_EQUAL(L"", kpb_test_strings_utf16be_textz_sized(&utf16be));
    EXPECT_TRUE(kpb_test_strings_utf16be_parse_from_string(&utf16be, "\x8c\xfe\x00\x00" "\x8c\xfe\x00\x00" "\x8c\xfe\x00\x00\x00\x00", 4 + 4 + 6));
    EXPECT_STRING_EQUAL(L"\u8cfe", kpb_test_strings_utf16be_text(&utf16be));
    EXPECT_STRING_EQUAL(L"\u8cfe", kpb_test_strings_utf16be_textz(&utf16be));
    EXPECT_STRING_EQUAL(L"\u8cfe", kpb_test_strings_utf16be_textz_sized(&utf16be));
    EXPECT_TRUE(kpb_test_strings_utf16be_parse_from_string(&utf16be, "\x8c\xfe\x09\xba" "\x8c\xfe\x09\xba\x00\x00" "\x8c\xfe\x09\xba\x00\x00", 4 + 6 + 6));
    EXPECT_STRING_EQUAL(L"\u8cfe\u09ba", kpb_test_strings_utf16be_text(&utf16be));
    EXPECT_STRING_EQUAL(L"\u8cfe\u09ba", kpb_test_strings_utf16be_textz(&utf16be));
    EXPECT_STRING_EQUAL(L"\u8cfe\u09ba", kpb_test_strings_utf16be_textz_sized(&utf16be));
    EXPECT_TRUE(kpb_test_strings_utf16be_parse_from_string(&utf16be, "\x8c\xfe\x09\xba" "\x8c\xfe\x09\xba\x65\x87\x00\x00" "\x8c\xfe\x09\xba\x00\x00", 4 + 8 + 6));
    EXPECT_STRING_EQUAL(L"\u8cfe\u09ba", kpb_test_strings_utf16be_text(&utf16be));
    EXPECT_STRING_EQUAL(L"\u8cfe\u09ba\u6587", kpb_test_strings_utf16be_textz(&utf16be));
    EXPECT_STRING_EQUAL(L"\u8cfe\u09ba", kpb_test_strings_utf16be_textz_sized(&utf16be));

    kpb_test_strings_utf16be_fini(&utf16be);

    kpb_test_strings_utf8_t utf8;
    kpb_test_strings_utf8_init(&utf8);

    result = kpb_test_strings_utf8_serialize_to_string(&utf8, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x00\x00" "\x00" "\x00\x00\x00", 2 + 1 + 3, result, result_size);
    kpb_io_free(result);
    kpb_test_strings_utf8_set_text(&utf8, "\xfe");
    kpb_test_strings_utf8_set_textz(&utf8, "\xfe");
    kpb_test_strings_utf8_set_textz_sized(&utf8, "\xfe");
    result = kpb_test_strings_utf8_serialize_to_string(&utf8, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xfe\x00" "\xfe\x00" "\xfe\x00\x00", 2 + 2 + 3, result, result_size);
    kpb_io_free(result);
    kpb_test_strings_utf8_clear(&utf8);
    result = kpb_test_strings_utf8_serialize_to_string(&utf8, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x00\x00" "\x00" "\x00\x00\x00", 2 + 1 + 3, result, result_size);
    kpb_io_free(result);
    kpb_test_strings_utf8_set_text(&utf8, "\xfe\xba");
    kpb_test_strings_utf8_set_textz(&utf8, "\xfe\xba");
    kpb_test_strings_utf8_set_textz_sized(&utf8, "\xfe\xba");
    result = kpb_test_strings_utf8_serialize_to_string(&utf8, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xfe\xba" "\xfe\xba\x00" "\xfe\xba\x00", 2 + 3 + 3, result, result_size);
    kpb_io_free(result);
    kpb_test_strings_utf8_set_text(&utf8, "\xfe\xba\x09");
    kpb_test_strings_utf8_set_textz(&utf8, "\xfe\xba\x09");
    kpb_test_strings_utf8_set_textz_sized(&utf8, "\xfe\xba\x09");
    result = kpb_test_strings_utf8_serialize_to_string(&utf8, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xfe\xba" "\xfe\xba\x09\x00" "\xfe\xba\x00", 2 + 4 + 3, result, result_size);
    kpb_io_free(result);

    kpb_test_strings_utf8_clear(&utf8);
    EXPECT_TRUE(kpb_test_strings_utf8_parse_from_string(&utf8, "\x00\x00" "\x00" "\x00\x00\x00", 2 + 1 + 3));
    EXPECT_STRING_EQUAL("", kpb_test_strings_utf8_text(&utf8));
    EXPECT_STRING_EQUAL("", kpb_test_strings_utf8_textz(&utf8));
    EXPECT_STRING_EQUAL("", kpb_test_strings_utf8_textz_sized(&utf8));
    EXPECT_TRUE(kpb_test_strings_utf8_parse_from_string(&utf8, "\xfe\x00" "\xfe\x00" "\xfe\x00\x00", 2 + 2 + 3));
    EXPECT_STRING_EQUAL("\xfe", kpb_test_strings_utf8_text(&utf8));
    EXPECT_STRING_EQUAL("\xfe", kpb_test_strings_utf8_textz(&utf8));
    EXPECT_STRING_EQUAL("\xfe", kpb_test_strings_utf8_textz_sized(&utf8));
    EXPECT_TRUE(kpb_test_strings_utf8_parse_from_string(&utf8, "\xfe\xba" "\xfe\xba\x00" "\xfe\xba\x00", 2 + 3 + 3));
    EXPECT_STRING_EQUAL("\xfe\xba", kpb_test_strings_utf8_text(&utf8));
    EXPECT_STRING_EQUAL("\xfe\xba", kpb_test_strings_utf8_textz(&utf8));
    EXPECT_STRING_EQUAL("\xfe\xba", kpb_test_strings_utf8_textz_sized(&utf8));
    EXPECT_TRUE(kpb_test_strings_utf8_parse_from_string(&utf8, "\xfe\xba" "\xfe\xba\x09\x00" "\xfe\xba\x00", 2 + 4 + 3));
    EXPECT_STRING_EQUAL("\xfe\xba", kpb_test_strings_utf8_text(&utf8));
    EXPECT_STRING_EQUAL("\xfe\xba\x09", kpb_test_strings_utf8_textz(&utf8));
    EXPECT_STRING_EQUAL("\xfe\xba", kpb_test_strings_utf8_textz_sized(&utf8));

    kpb_test_strings_utf8_fini(&utf8);
    return 0;
}
