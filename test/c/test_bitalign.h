#ifndef _H_KPB_TEST_BITALIGN_H_
#define _H_KPB_TEST_BITALIGN_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_bitalign(void);

#ifdef __cplusplus
};
#endif

#endif
