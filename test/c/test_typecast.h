#ifndef _H_KPB_TEST_TYPECAST_H_
#define _H_KPB_TEST_TYPECAST_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_typecast(void);

#ifdef __cplusplus
};
#endif

#endif
