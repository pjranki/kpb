#ifndef _H_KPB_TEST_POS_H_
#define _H_KPB_TEST_POS_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_pos(void);

#ifdef __cplusplus
};
#endif

#endif
