#include "test_io_expr.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/io_expr.h"

int kpb_test_io_expr(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_io_expr_t io_expr;
    kpb_test_io_expr_init(&io_expr);

    // _io.pos
    EXPECT_TRUE(kpb_test_io_expr_parse_from_string(&io_expr, "\xa1\xa2\xa3\xa4\xa5\xc1\xc2\xc3", 8));
    EXPECT_BYTES_EQUAL("\xa1\xa2\xa3\xa4\xa5", 5, kpb_test_io_expr_data(&io_expr), kpb_test_io_expr_data_size(&io_expr));
    EXPECT_BYTES_EQUAL("\xc1\xc2\xc3", 3, kpb_test_io_expr_align(&io_expr), kpb_test_io_expr_align_size(&io_expr));
    EXPECT_TRUE(kpb_test_io_expr_parse_from_string(&io_expr, "\xa1\xa2\xa3\xa4\xa5\xc1\xc2\xc3\xff\xff", 10));
    EXPECT_BYTES_EQUAL("\xa1\xa2\xa3\xa4\xa5", 5, kpb_test_io_expr_data(&io_expr), kpb_test_io_expr_data_size(&io_expr));
    EXPECT_BYTES_EQUAL("\xc1\xc2\xc3", 3, kpb_test_io_expr_align(&io_expr), kpb_test_io_expr_align_size(&io_expr));
    kpb_test_io_expr_clear(&io_expr);
    EXPECT_TRUE(kpb_test_io_expr_set_data(&io_expr, "\xaa", 1));
    EXPECT_TRUE(kpb_test_io_expr_set_align(&io_expr, "\xe1\xe2", 2));
    result = kpb_test_io_expr_serialize_to_string(&io_expr, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xaa\x00\x00\x00\x00\xe1\xe2\x00", 8, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_io_expr_set_data(&io_expr, "\xa1\xa2\xa3\xa4\xa5", 5));
    EXPECT_TRUE(kpb_test_io_expr_set_align(&io_expr, "\xc1\xc2\xc3", 3));
    result = kpb_test_io_expr_serialize_to_string(&io_expr, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xa1\xa2\xa3\xa4\xa5\xc1\xc2\xc3", 8, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_io_expr_set_data(&io_expr, "\xa1\xa2\xa3\xa4\xa5\xff", 6));
    EXPECT_TRUE(kpb_test_io_expr_set_align(&io_expr, "\xc1\xc2\xc3\xff", 4));
    result = kpb_test_io_expr_serialize_to_string(&io_expr, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xa1\xa2\xa3\xa4\xa5\xc1\xc2\xc3", 8, result, result_size);
    kpb_io_free(result);
    kpb_test_io_expr_fini(&io_expr);

    // _io.size and size-eos: true
    kpb_test_io_expr_bytes_t obj_bytes;
    kpb_test_io_expr_bytes_init(&obj_bytes);
    result = kpb_test_io_expr_bytes_serialize_to_string(&obj_bytes, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x00", 1, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_io_expr_bytes_set_fixed(&obj_bytes, 0xcc));
    result = kpb_test_io_expr_bytes_serialize_to_string(&obj_bytes, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xcc", 1, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_io_expr_bytes_set_variable(&obj_bytes, "\x12\x34\x56\x78", 4));
    result = kpb_test_io_expr_bytes_serialize_to_string(&obj_bytes, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xcc\x12\x34\x56\x78", 5, result, result_size);
    kpb_io_free(result);
    EXPECT_FALSE(kpb_test_io_expr_bytes_parse_from_string(&obj_bytes, "", 0));
    EXPECT_TRUE(kpb_test_io_expr_bytes_parse_from_string(&obj_bytes, "\xdd", 1));
    EXPECT_EQUAL(0xdd, kpb_test_io_expr_bytes_fixed(&obj_bytes));
    EXPECT_EQUAL(0, kpb_test_io_expr_bytes_variable_size(&obj_bytes));
    EXPECT_TRUE(kpb_test_io_expr_bytes_parse_from_string(&obj_bytes, "\xee\xfe\xab", 3));
    EXPECT_EQUAL(0xee, kpb_test_io_expr_bytes_fixed(&obj_bytes));
    EXPECT_BYTES_EQUAL("\xfe\xab", 2, kpb_test_io_expr_bytes_variable(&obj_bytes), kpb_test_io_expr_bytes_variable_size(&obj_bytes));
    kpb_test_io_expr_bytes_fini(&obj_bytes);

    // _io.eof and repeat: eos
    kpb_test_io_expr_eos_t obj_eos;
    kpb_test_io_expr_eos_init(&obj_eos);
    result = kpb_test_io_expr_eos_serialize_to_string(&obj_eos, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x00", 1, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_io_expr_eos_set_fixed(&obj_eos, 0xcc));
    result = kpb_test_io_expr_eos_serialize_to_string(&obj_eos, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xcc", 1, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_io_expr_eos_add_variable(&obj_eos, 0x3412));
    EXPECT_TRUE(kpb_test_io_expr_eos_add_variable(&obj_eos, 0x7856));
    result = kpb_test_io_expr_eos_serialize_to_string(&obj_eos, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xcc\x12\x34\x56\x78", 5, result, result_size);
    kpb_io_free(result);
    EXPECT_FALSE(kpb_test_io_expr_eos_parse_from_string(&obj_eos, "", 0));
    EXPECT_FALSE(kpb_test_io_expr_eos_parse_from_string(&obj_eos, "\xdd\xdd", 2));
    EXPECT_TRUE(kpb_test_io_expr_eos_parse_from_string(&obj_eos, "\xdd", 1));
    EXPECT_EQUAL(0xdd, kpb_test_io_expr_eos_fixed(&obj_eos));
    EXPECT_EQUAL(0, kpb_test_io_expr_eos_variable_size(&obj_eos));
    EXPECT_TRUE(kpb_test_io_expr_eos_parse_from_string(&obj_eos, "\xee\xab\xcd\x67\x89", 5));
    EXPECT_EQUAL(0xee, kpb_test_io_expr_eos_fixed(&obj_eos));
    EXPECT_EQUAL(2, kpb_test_io_expr_eos_variable_size(&obj_eos));
    EXPECT_EQUAL(0xcdab, kpb_test_io_expr_eos_variable_at(&obj_eos, 0));
    EXPECT_EQUAL(0x8967, kpb_test_io_expr_eos_variable_at(&obj_eos, 1));
    kpb_test_io_expr_eos_fini(&obj_eos);

    // _ and repeat-until
    kpb_test_io_expr_until_t obj_until;
    kpb_test_io_expr_until_init(&obj_until);
    result = kpb_test_io_expr_until_serialize_to_string(&obj_until, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x00", 1, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_io_expr_until_set_fixed(&obj_until, 0xcc));
    result = kpb_test_io_expr_until_serialize_to_string(&obj_until, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xcc", 1, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_io_expr_until_add_variable(&obj_until, 0x3412));
    EXPECT_TRUE(kpb_test_io_expr_until_add_variable(&obj_until, 0x7856));
    result = kpb_test_io_expr_until_serialize_to_string(&obj_until, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xcc\x12\x34\x56\x78", 5, result, result_size);
    kpb_io_free(result);
    EXPECT_FALSE(kpb_test_io_expr_until_parse_from_string(&obj_until, "", 0));
    EXPECT_FALSE(kpb_test_io_expr_until_parse_from_string(&obj_until, "\xdd\xdd", 2));
    EXPECT_FALSE(kpb_test_io_expr_until_parse_from_string(&obj_until, "\xdd", 1));
    EXPECT_TRUE(kpb_test_io_expr_until_parse_from_string(&obj_until, "\xdd\xad\x0b\xab\xcd", 5));
    EXPECT_EQUAL(0xdd, kpb_test_io_expr_until_fixed(&obj_until));
    EXPECT_EQUAL(1, kpb_test_io_expr_until_variable_size(&obj_until));
    EXPECT_EQUAL(0xbad, kpb_test_io_expr_until_variable_at(&obj_until, 0));
    EXPECT_TRUE(kpb_test_io_expr_until_parse_from_string(&obj_until, "\xee\xab\xcd\xad\x0b", 5));
    EXPECT_EQUAL(0xee, kpb_test_io_expr_until_fixed(&obj_until));
    EXPECT_EQUAL(2, kpb_test_io_expr_until_variable_size(&obj_until));
    EXPECT_EQUAL(0xcdab, kpb_test_io_expr_until_variable_at(&obj_until, 0));
    EXPECT_EQUAL(0xbad, kpb_test_io_expr_until_variable_at(&obj_until, 1));
    kpb_test_io_expr_until_fini(&obj_until);

    // regression test for list of classes when using a class to check its value
    kpb_test_io_expr_until_class_list_t class_list;
    kpb_test_io_expr_until_class_list_init(&class_list);
    EXPECT_EQUAL(0, kpb_test_io_expr_until_class_list_items_size(&class_list));
    EXPECT_TRUE(kpb_test_io_expr_until_class_list_parse_from_string(&class_list, "\x05", 1));
    EXPECT_EQUAL(1, kpb_test_io_expr_until_class_list_items_size(&class_list));
    EXPECT_EQUAL(0x05, kpb_test_io_expr_until_class_info_value(kpb_test_io_expr_until_class_item_info(kpb_test_io_expr_until_class_list_items_at(&class_list, 0))));
    EXPECT_TRUE(kpb_test_io_expr_until_class_list_parse_from_string(&class_list, "\x06\x07", 2));
    EXPECT_EQUAL(2, kpb_test_io_expr_until_class_list_items_size(&class_list));
    EXPECT_EQUAL(0x06, kpb_test_io_expr_until_class_info_value(kpb_test_io_expr_until_class_item_info(kpb_test_io_expr_until_class_list_items_at(&class_list, 0))));
    EXPECT_EQUAL(0x07, kpb_test_io_expr_until_class_info_value(kpb_test_io_expr_until_class_item_info(kpb_test_io_expr_until_class_list_items_at(&class_list, 1))));
    EXPECT_TRUE(kpb_test_io_expr_until_class_list_parse_from_string(&class_list, "\x07\x08\x03", 3));
    EXPECT_EQUAL(3, kpb_test_io_expr_until_class_list_items_size(&class_list));
    EXPECT_EQUAL(0x07, kpb_test_io_expr_until_class_info_value(kpb_test_io_expr_until_class_item_info(kpb_test_io_expr_until_class_list_items_at(&class_list, 0))));
    EXPECT_EQUAL(0x08, kpb_test_io_expr_until_class_info_value(kpb_test_io_expr_until_class_item_info(kpb_test_io_expr_until_class_list_items_at(&class_list, 1))));
    EXPECT_EQUAL(0x03, kpb_test_io_expr_until_class_info_value(kpb_test_io_expr_until_class_item_info(kpb_test_io_expr_until_class_list_items_at(&class_list, 2))));
    EXPECT_TRUE(kpb_test_io_expr_until_class_list_parse_from_string(&class_list, "\x09\x0a\x03\x0f", 4));
    EXPECT_EQUAL(3, kpb_test_io_expr_until_class_list_items_size(&class_list));
    EXPECT_EQUAL(0x09, kpb_test_io_expr_until_class_info_value(kpb_test_io_expr_until_class_item_info(kpb_test_io_expr_until_class_list_items_at(&class_list, 0))));
    EXPECT_EQUAL(0x0a, kpb_test_io_expr_until_class_info_value(kpb_test_io_expr_until_class_item_info(kpb_test_io_expr_until_class_list_items_at(&class_list, 1))));
    EXPECT_EQUAL(0x03, kpb_test_io_expr_until_class_info_value(kpb_test_io_expr_until_class_item_info(kpb_test_io_expr_until_class_list_items_at(&class_list, 2))));
    kpb_test_io_expr_until_class_list_clear(&class_list);
    result = kpb_test_io_expr_until_class_list_serialize_to_string(&class_list, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("", 0, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_io_expr_until_class_info_set_value(kpb_test_io_expr_until_class_item_mutable_info(kpb_test_io_expr_until_class_list_add_items(&class_list)), 0x01));
    result = kpb_test_io_expr_until_class_list_serialize_to_string(&class_list, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x01", 1, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_io_expr_until_class_info_set_value(kpb_test_io_expr_until_class_item_mutable_info(kpb_test_io_expr_until_class_list_add_items(&class_list)), 0x02));
    result = kpb_test_io_expr_until_class_list_serialize_to_string(&class_list, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x01\x02", 2, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_io_expr_until_class_info_set_value(kpb_test_io_expr_until_class_item_mutable_info(kpb_test_io_expr_until_class_list_add_items(&class_list)), 0x03));
    result = kpb_test_io_expr_until_class_list_serialize_to_string(&class_list, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x01\x02\x03", 3, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_io_expr_until_class_info_set_value(kpb_test_io_expr_until_class_item_mutable_info(kpb_test_io_expr_until_class_list_add_items(&class_list)), 0x04));
    result = kpb_test_io_expr_until_class_list_serialize_to_string(&class_list, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x01\x02\x03", 3, result, result_size);
    kpb_io_free(result);
    kpb_test_io_expr_until_class_list_fini(&class_list);

    return 0;
}
