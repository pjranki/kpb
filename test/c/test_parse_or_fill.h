#ifndef _H_KPB_TEST_PARSE_OR_FILL_H_
#define _H_KPB_TEST_PARSE_OR_FILL_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_parse_or_fill(void);

#ifdef __cplusplus
};
#endif

#endif
