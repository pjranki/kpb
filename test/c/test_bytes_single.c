#include "test_bytes_single.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/bytes_single.h"


int kpb_test_bytes_single(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_bytes_single_t bytes_single;
    kpb_test_bytes_single_init(&bytes_single);
    EXPECT_EQUAL(0, kpb_test_bytes_single_data2_size(&bytes_single));
    EXPECT_EQUAL(0, bytes_single._data2_size);
    EXPECT_EQUAL(0, kpb_test_bytes_single_data4_size(&bytes_single));
    EXPECT_EQUAL(0, bytes_single._data4_size);
    result = kpb_test_bytes_single_serialize_to_string(&bytes_single, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00", 6, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_bytes_single_set_data2(&bytes_single, "\xfe", 1));
    EXPECT_TRUE(kpb_test_bytes_single_set_data4(&bytes_single, "\x12", 1));
    EXPECT_BYTES_EQUAL("\xfe", 1, kpb_test_bytes_single_data2(&bytes_single), kpb_test_bytes_single_data2_size(&bytes_single));
    EXPECT_BYTES_EQUAL("\xfe", 1, bytes_single.data2, bytes_single._data2_size);
    EXPECT_BYTES_EQUAL("\x12", 1, kpb_test_bytes_single_data4(&bytes_single), kpb_test_bytes_single_data4_size(&bytes_single));
    EXPECT_BYTES_EQUAL("\x12", 1, bytes_single.data4, bytes_single._data4_size);
    result = kpb_test_bytes_single_serialize_to_string(&bytes_single, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xfe\x00\x12\x00\x00\x00", 6, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_bytes_single_set_data2(&bytes_single, "\xfe\xdc", 2));
    EXPECT_TRUE(kpb_test_bytes_single_set_data4(&bytes_single, "\x12\x34", 2));
    EXPECT_BYTES_EQUAL("\xfe\xdc", 2, kpb_test_bytes_single_data2(&bytes_single), kpb_test_bytes_single_data2_size(&bytes_single));
    EXPECT_BYTES_EQUAL("\xfe\xdc", 2, bytes_single.data2, bytes_single._data2_size);
    EXPECT_BYTES_EQUAL("\x12\x34", 2, kpb_test_bytes_single_data4(&bytes_single), kpb_test_bytes_single_data4_size(&bytes_single));
    EXPECT_BYTES_EQUAL("\x12\x34", 2, bytes_single.data4, bytes_single._data4_size);
    result = kpb_test_bytes_single_serialize_to_string(&bytes_single, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xfe\xdc\x12\x34\x00\x00", 6, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_bytes_single_set_data2(&bytes_single, "\xfe\xdc\xab", 3));
    EXPECT_TRUE(kpb_test_bytes_single_set_data4(&bytes_single, "\x12\x34\x45\x67", 4));
    EXPECT_BYTES_EQUAL("\xfe\xdc\xab", 3, kpb_test_bytes_single_data2(&bytes_single), kpb_test_bytes_single_data2_size(&bytes_single));
    EXPECT_BYTES_EQUAL("\xfe\xdc\xab", 3, bytes_single.data2, bytes_single._data2_size);
    EXPECT_BYTES_EQUAL("\x12\x34\x45\x67", 4, kpb_test_bytes_single_data4(&bytes_single), kpb_test_bytes_single_data4_size(&bytes_single));
    EXPECT_BYTES_EQUAL("\x12\x34\x45\x67", 4, bytes_single.data4, bytes_single._data4_size);
    result = kpb_test_bytes_single_serialize_to_string(&bytes_single, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xfe\xdc\x12\x34\x45\x67", 6, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_bytes_single_set_data2(&bytes_single, "\xfe\xdc\xab\x90", 4));
    EXPECT_TRUE(kpb_test_bytes_single_set_data4(&bytes_single, "\x12\x34\x45\x67\x89", 5));
    EXPECT_BYTES_EQUAL("\xfe\xdc\xab\x90", 4, kpb_test_bytes_single_data2(&bytes_single), kpb_test_bytes_single_data2_size(&bytes_single));
    EXPECT_BYTES_EQUAL("\xfe\xdc\xab\x90", 4, bytes_single.data2, bytes_single._data2_size);
    EXPECT_BYTES_EQUAL("\x12\x34\x45\x67\x89", 5, kpb_test_bytes_single_data4(&bytes_single), kpb_test_bytes_single_data4_size(&bytes_single));
    EXPECT_BYTES_EQUAL("\x12\x34\x45\x67\x89", 5, bytes_single.data4, bytes_single._data4_size);
    result = kpb_test_bytes_single_serialize_to_string(&bytes_single, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xfe\xdc\x12\x34\x45\x67", 6, result, result_size);
    kpb_io_free(result);
    kpb_test_bytes_single_clear(&bytes_single);
    EXPECT_EQUAL(0, kpb_test_bytes_single_data2_size(&bytes_single));
    EXPECT_EQUAL(0, bytes_single._data2_size);
    EXPECT_EQUAL(0, kpb_test_bytes_single_data4_size(&bytes_single));
    EXPECT_EQUAL(0, bytes_single._data4_size);
    result = kpb_test_bytes_single_serialize_to_string(&bytes_single, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00", 6, result, result_size);
    kpb_io_free(result);

    kpb_test_bytes_single_clear(&bytes_single);
    EXPECT_EQUAL(0, kpb_test_bytes_single_data2_size(&bytes_single));
    EXPECT_EQUAL(0, bytes_single._data2_size);
    EXPECT_EQUAL(0, kpb_test_bytes_single_data4_size(&bytes_single));
    EXPECT_EQUAL(0, bytes_single._data4_size);
    EXPECT_TRUE(kpb_test_bytes_single_parse_from_string(&bytes_single, "\x00\x00\x00\x00\x00\x00", 6));
    EXPECT_BYTES_EQUAL("\x00\x00", 2, kpb_test_bytes_single_data2(&bytes_single), kpb_test_bytes_single_data2_size(&bytes_single));
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_single.data2, bytes_single._data2_size);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, kpb_test_bytes_single_data4(&bytes_single), kpb_test_bytes_single_data4_size(&bytes_single));
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_single.data4, bytes_single._data4_size);
    EXPECT_TRUE(kpb_test_bytes_single_parse_from_string(&bytes_single, "\xfe\x00\x12\x00\x00\x00", 6));
    EXPECT_BYTES_EQUAL("\xfe\x00", 2, kpb_test_bytes_single_data2(&bytes_single), kpb_test_bytes_single_data2_size(&bytes_single));
    EXPECT_BYTES_EQUAL("\xfe\x00", 2, bytes_single.data2, bytes_single._data2_size);
    EXPECT_BYTES_EQUAL("\x12\x00\x00\x00", 4, kpb_test_bytes_single_data4(&bytes_single), kpb_test_bytes_single_data4_size(&bytes_single));
    EXPECT_BYTES_EQUAL("\x12\x00\x00\x00", 4, bytes_single.data4, bytes_single._data4_size);
    EXPECT_TRUE(kpb_test_bytes_single_parse_from_string(&bytes_single, "\xfe\xdc\x12\x34\x00\x00", 6));
    EXPECT_BYTES_EQUAL("\xfe\xdc", 2, kpb_test_bytes_single_data2(&bytes_single), kpb_test_bytes_single_data2_size(&bytes_single));
    EXPECT_BYTES_EQUAL("\xfe\xdc", 2, bytes_single.data2, bytes_single._data2_size);
    EXPECT_BYTES_EQUAL("\x12\x34\x00\x00", 4, kpb_test_bytes_single_data4(&bytes_single), kpb_test_bytes_single_data4_size(&bytes_single));
    EXPECT_BYTES_EQUAL("\x12\x34\x00\x00", 4, bytes_single.data4, bytes_single._data4_size);
    EXPECT_TRUE(kpb_test_bytes_single_parse_from_string(&bytes_single, "\xfe\xdc\x12\x34\x56\x78", 6));
    EXPECT_BYTES_EQUAL("\xfe\xdc", 2, kpb_test_bytes_single_data2(&bytes_single), kpb_test_bytes_single_data2_size(&bytes_single));
    EXPECT_BYTES_EQUAL("\xfe\xdc", 2, bytes_single.data2, bytes_single._data2_size);
    EXPECT_BYTES_EQUAL("\x12\x34\x56\x78", 4, kpb_test_bytes_single_data4(&bytes_single), kpb_test_bytes_single_data4_size(&bytes_single));
    EXPECT_BYTES_EQUAL("\x12\x34\x56\x78", 4, bytes_single.data4, bytes_single._data4_size);

    kpb_test_bytes_single_fini(&bytes_single);
    return 0;
}
