#ifndef _H_KPB_TEST_EXPRESSIONS_H_
#define _H_KPB_TEST_EXPRESSIONS_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_expressions(void);

#ifdef __cplusplus
};
#endif

#endif
