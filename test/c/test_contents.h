#ifndef _H_KPB_TEST_CONTENTS_H_
#define _H_KPB_TEST_CONTENTS_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_contents(void);

#ifdef __cplusplus
};
#endif

#endif
