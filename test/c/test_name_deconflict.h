#ifndef _H_KPB_TEST_NAME_DECONFLICT_H_
#define _H_KPB_TEST_NAME_DECONFLICT_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_name_deconflict(void);

#ifdef __cplusplus
};
#endif

#endif
