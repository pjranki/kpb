#ifndef _H_KPB_TEST_BASIC_LIST_H_
#define _H_KPB_TEST_BASIC_LIST_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_basic_list(void);

#ifdef __cplusplus
};
#endif

#endif
