#ifndef _H_PROCESS_VALUE_BUF5_H_
#define _H_PROCESS_VALUE_BUF5_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <string.h>

#include <kpb_io.h>

#include "c/process_value.h"

struct process_value_buf5 {
    uint8_t memset_value_;
};

void process_value_buf5_init(struct process_value_buf5 *_this, uint8_t memset_value);
const char *process_value_buf5_getter(struct process_value_buf5 *_this, const char *value /* TODO: this should have size */);
char *process_value_buf5_setter(struct process_value_buf5 *_this, char *value, size_t size);
void process_value_buf5_fini(struct process_value_buf5 *_this);

#ifdef __cplusplus
};
#endif

#endif
