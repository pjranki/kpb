#include "test_switchon.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/switchon.h"

int kpb_test_switchon(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_switchon_t switchon;
    kpb_test_object1_t *object1 = NULL;
    kpb_test_object2_t *object2 = NULL;
    kpb_test_switchon_init(&switchon);

    result_size = 0;
    result = kpb_test_switchon_serialize_to_string(&switchon, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 11, result, result_size);
    kpb_io_free(result);
    object1 = kpb_test_switchon_mutable_object(&switchon);
    EXPECT_NOT_NULL(object1);
    EXPECT_TRUE(kpb_test_object1_set_value1(object1, 0xdeadbeef));
    EXPECT_TRUE(kpb_test_switchon_set_data_type(&switchon, KPB_TEST_SWITCHON_ONEOF_TYPE_OBJECT));
    EXPECT_TRUE(kpb_test_switchon_set_u2(&switchon, 0xcafe));
    EXPECT_TRUE(kpb_test_switchon_set_value_type(&switchon, 2));
    result_size = 0;
    result = kpb_test_switchon_serialize_to_string(&switchon, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x01\xef\xbe\xad\xde\x02\x00\xfe\xca", 9, result, result_size);
    kpb_io_free(result);
    object2 = kpb_test_switchon_mutable_number(&switchon);
    EXPECT_NOT_NULL(object2);
    EXPECT_TRUE(kpb_test_object2_set_value2(object2, 0xa537));
    EXPECT_TRUE(kpb_test_switchon_set_data_type(&switchon, KPB_TEST_SWITCHON_ONEOF_TYPE_NUMBER));
    EXPECT_TRUE(kpb_test_switchon_set_u4(&switchon, 0x12345678));
    EXPECT_TRUE(kpb_test_switchon_set_value_type(&switchon, 4));
    result_size = 0;
    result = kpb_test_switchon_serialize_to_string(&switchon, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x03\x37\xa5\x04\x00\x78\x56\x34\x12", 9, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_switchon_set_data_type(&switchon, 0xff));
    EXPECT_TRUE(kpb_test_switchon_set_u8(&switchon, 0xa1a2a3a4a5a6a7a8));
    EXPECT_TRUE(kpb_test_switchon_set_value_type(&switchon, 0xeecc));
    result_size = 0;
    result = kpb_test_switchon_serialize_to_string(&switchon, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xff\xcc\xee\xa8\xa7\xa6\xa5\xa4\xa3\xa2\xa1", 11, result, result_size);
    kpb_io_free(result);
    kpb_test_switchon_clear(&switchon);
    result_size = 0;
    result = kpb_test_switchon_serialize_to_string(&switchon, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 11, result, result_size);
    kpb_io_free(result);

    EXPECT_TRUE(kpb_test_switchon_parse_from_string(&switchon, "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 11));
    EXPECT_EQUAL(0, kpb_test_switchon_data_type(&switchon));
    EXPECT_FALSE(kpb_test_switchon_has_object(&switchon));
    EXPECT_FALSE(kpb_test_switchon_has_number(&switchon));
    EXPECT_EQUAL(0, kpb_test_switchon_value_type(&switchon));
    EXPECT_FALSE(kpb_test_switchon_has_u2(&switchon));
    EXPECT_FALSE(kpb_test_switchon_has_u4(&switchon));
    EXPECT_TRUE(kpb_test_switchon_has_u8(&switchon));
    EXPECT_EQUAL(0, kpb_test_switchon_u8(&switchon));
    EXPECT_TRUE(kpb_test_switchon_parse_from_string(&switchon, "\x01\xef\xbe\xad\xde\x02\x00\xfe\xca", 9));
    EXPECT_EQUAL(1, kpb_test_switchon_data_type(&switchon));
    EXPECT_TRUE(kpb_test_switchon_has_object(&switchon));
    EXPECT_EQUAL(0xdeadbeef, kpb_test_object1_value1(kpb_test_switchon_object(&switchon)));
    EXPECT_FALSE(kpb_test_switchon_has_number(&switchon));
    EXPECT_EQUAL(2, kpb_test_switchon_value_type(&switchon));
    EXPECT_TRUE(kpb_test_switchon_has_u2(&switchon));
    EXPECT_EQUAL(0xcafe, kpb_test_switchon_u2(&switchon));
    EXPECT_FALSE(kpb_test_switchon_has_u4(&switchon));
    EXPECT_FALSE(kpb_test_switchon_has_u8(&switchon));
    EXPECT_TRUE(kpb_test_switchon_parse_from_string(&switchon, "\x03\x37\xa5\x04\x00\x78\x56\x34\x12", 9));
    EXPECT_EQUAL(3, kpb_test_switchon_data_type(&switchon));
    EXPECT_FALSE(kpb_test_switchon_has_object(&switchon));
    EXPECT_TRUE(kpb_test_switchon_has_number(&switchon));
    EXPECT_EQUAL(0xa537, kpb_test_object2_value2(kpb_test_switchon_number(&switchon)));
    EXPECT_EQUAL(4, kpb_test_switchon_value_type(&switchon));
    EXPECT_FALSE(kpb_test_switchon_has_u2(&switchon));
    EXPECT_TRUE(kpb_test_switchon_has_u4(&switchon));
    EXPECT_EQUAL(0x12345678, kpb_test_switchon_u4(&switchon));
    EXPECT_FALSE(kpb_test_switchon_has_u8(&switchon));
    EXPECT_TRUE(kpb_test_switchon_parse_from_string(&switchon, "\xff\xcc\xee\xa8\xa7\xa6\xa5\xa4\xa3\xa2\xa1", 11));
    EXPECT_EQUAL(0xff, kpb_test_switchon_data_type(&switchon));
    EXPECT_FALSE(kpb_test_switchon_has_object(&switchon));
    EXPECT_FALSE(kpb_test_switchon_has_number(&switchon));
    EXPECT_EQUAL(0xeecc, kpb_test_switchon_value_type(&switchon));
    EXPECT_FALSE(kpb_test_switchon_has_u2(&switchon));
    EXPECT_FALSE(kpb_test_switchon_has_u4(&switchon));
    EXPECT_TRUE(kpb_test_switchon_has_u8(&switchon));
    EXPECT_EQUAL(0xa1a2a3a4a5a6a7a8, kpb_test_switchon_u8(&switchon));

    kpb_test_switchon_fini(&switchon);

    kpb_test_dup_enum_number_t dup_number;
    kpb_test_dup_enum_number_init(&dup_number);
    kpb_test_dup_enum_number_mutable_data_1(&dup_number);
    kpb_test_dup_enum_number_mutable_data_2(&dup_number);
    kpb_test_dup_enum_number_fini(&dup_number);

    kpb_test_dup_enum_name_t dup_name;
    kpb_test_dup_enum_name_init(&dup_name);
    kpb_test_dup_enum_name_mutable_data_true_(&dup_name);
    kpb_test_dup_enum_name_mutable_data_false_(&dup_name);
    kpb_test_dup_enum_name_fini(&dup_name);
    return 0;
}
