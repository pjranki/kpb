#include "test_self_root_parent.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/self_root_parent.h"

int kpb_test_self_root_parent(void)
{
    const void *null_obj = NULL;
    kpb_test_root__t root_obj;
    kpb_test_child_t *child_obj = NULL;
    kpb_test_grand_child_t *grand_child_obj = NULL;
    kpb_test_root__init(&root_obj);

    child_obj = kpb_test_root__mutable_child(&root_obj);
    EXPECT_NOT_NULL(child_obj);
    EXPECT_FALSE(KPB_IO_ISINSTANCE(null_obj, kpb_test_child));
    EXPECT_TRUE(KPB_IO_ISINSTANCE(child_obj, kpb_test_child));
    EXPECT_FALSE(KPB_IO_ISINSTANCE(child_obj, kpb_test_grand_child));
    grand_child_obj = kpb_test_child_mutable_child(child_obj);
    EXPECT_FALSE(KPB_IO_ISINSTANCE(null_obj, kpb_test_child));
    EXPECT_FALSE(KPB_IO_ISINSTANCE(grand_child_obj, kpb_test_child));
    EXPECT_TRUE(KPB_IO_ISINSTANCE(grand_child_obj, kpb_test_grand_child));
    EXPECT_NOT_NULL(grand_child_obj);

    EXPECT_EQUAL(&root_obj, child_obj->_root);
    EXPECT_EQUAL(&root_obj, grand_child_obj->_root);

    EXPECT_TRUE(kpb_test_root__set_value(&root_obj, 0xdeadbeef));
    EXPECT_TRUE(kpb_test_child_set_value(child_obj, 0xcafebabe));
    EXPECT_TRUE(kpb_test_grand_child_set_value(grand_child_obj, 0xf00ba123));

    EXPECT_EQUAL(0xdeadbeef, kpb_test_root__value(&root_obj));
    EXPECT_EQUAL(0xcafebabe, kpb_test_child_value(child_obj));
    EXPECT_EQUAL(0xf00ba123, kpb_test_grand_child_value(grand_child_obj));

    EXPECT_EQUAL(0xdeadbeef, kpb_test_root__root_value(&root_obj));
    EXPECT_EQUAL(0xdeadbeef, kpb_test_root__self_value(&root_obj));

    EXPECT_EQUAL(0xdeadbeef, kpb_test_child_root_value(child_obj));
    EXPECT_EQUAL(0xdeadbeef, kpb_test_child_parent_value(child_obj));
    EXPECT_EQUAL(0xcafebabe, kpb_test_child_self_value(child_obj));

    EXPECT_EQUAL(0xdeadbeef, kpb_test_grand_child_root_value(grand_child_obj));
    EXPECT_EQUAL(0xcafebabe, kpb_test_grand_child_parent_value(grand_child_obj));
    EXPECT_EQUAL(0xf00ba123, kpb_test_grand_child_self_value(grand_child_obj));

    kpb_test_root__clear(&root_obj);

    child_obj = kpb_test_root__add_child_list(&root_obj);
    EXPECT_NOT_NULL(child_obj);
    grand_child_obj = kpb_test_child_add_grand_child_list(child_obj);
    EXPECT_NOT_NULL(grand_child_obj);

    EXPECT_EQUAL(&root_obj, child_obj->_root);
    EXPECT_EQUAL(&root_obj, grand_child_obj->_root);

    EXPECT_TRUE(kpb_test_root__set_value(&root_obj, 0xdeadbeef));
    EXPECT_TRUE(kpb_test_child_set_value(child_obj, 0xcafebabe));
    EXPECT_TRUE(kpb_test_grand_child_set_value(grand_child_obj, 0xf00ba123));

    EXPECT_EQUAL(0xdeadbeef, kpb_test_root__value(&root_obj));
    EXPECT_EQUAL(0xcafebabe, kpb_test_child_value(child_obj));
    EXPECT_EQUAL(0xf00ba123, kpb_test_grand_child_value(grand_child_obj));

    EXPECT_EQUAL(0xdeadbeef, kpb_test_root__root_value(&root_obj));
    EXPECT_EQUAL(0xdeadbeef, kpb_test_root__self_value(&root_obj));

    EXPECT_EQUAL(0xdeadbeef, kpb_test_child_root_value(child_obj));
    EXPECT_EQUAL(0xdeadbeef, kpb_test_child_parent_value(child_obj));
    EXPECT_EQUAL(0xcafebabe, kpb_test_child_self_value(child_obj));

    EXPECT_EQUAL(0xdeadbeef, kpb_test_grand_child_root_value(grand_child_obj));
    EXPECT_EQUAL(0xcafebabe, kpb_test_grand_child_parent_value(grand_child_obj));
    EXPECT_EQUAL(0xf00ba123, kpb_test_grand_child_self_value(grand_child_obj));

    child_obj = kpb_test_root__mutable_child_list(&root_obj, 0);
    EXPECT_NOT_NULL(child_obj);
    grand_child_obj = kpb_test_child_mutable_grand_child_list(child_obj, 0);
    EXPECT_NOT_NULL(grand_child_obj);

    EXPECT_EQUAL(&root_obj, child_obj->_root);
    EXPECT_EQUAL(&root_obj, grand_child_obj->_root);

    EXPECT_TRUE(kpb_test_root__set_value(&root_obj, 0xdeadbeef));
    EXPECT_TRUE(kpb_test_child_set_value(child_obj, 0xcafebabe));
    EXPECT_TRUE(kpb_test_grand_child_set_value(grand_child_obj, 0xf00ba123));

    EXPECT_EQUAL(0xdeadbeef, kpb_test_root__value(&root_obj));
    EXPECT_EQUAL(0xcafebabe, kpb_test_child_value(child_obj));
    EXPECT_EQUAL(0xf00ba123, kpb_test_grand_child_value(grand_child_obj));

    EXPECT_EQUAL(0xdeadbeef, kpb_test_root__root_value(&root_obj));
    EXPECT_EQUAL(0xdeadbeef, kpb_test_root__self_value(&root_obj));

    EXPECT_EQUAL(0xdeadbeef, kpb_test_child_root_value(child_obj));
    EXPECT_EQUAL(0xdeadbeef, kpb_test_child_parent_value(child_obj));
    EXPECT_EQUAL(0xcafebabe, kpb_test_child_self_value(child_obj));

    EXPECT_EQUAL(0xdeadbeef, kpb_test_grand_child_root_value(grand_child_obj));
    EXPECT_EQUAL(0xcafebabe, kpb_test_grand_child_parent_value(grand_child_obj));
    EXPECT_EQUAL(0xf00ba123, kpb_test_grand_child_self_value(grand_child_obj));

    kpb_test_root__fini(&root_obj);

    kpb_test_child_t orphan;
    kpb_test_child_init(&orphan);

    EXPECT_EQUAL(0, kpb_test_child_root_value(&orphan));
    EXPECT_EQUAL(0, kpb_test_child_self_value(&orphan));
    EXPECT_EQUAL(0, kpb_test_child_parent_value(&orphan));

    kpb_test_child_clear(&orphan);

    EXPECT_EQUAL(0, kpb_test_child_root_value(&orphan));
    EXPECT_EQUAL(0, kpb_test_child_self_value(&orphan));
    EXPECT_EQUAL(0, kpb_test_child_parent_value(&orphan));

    kpb_test_child_fini(&orphan);
    return 0;
}
