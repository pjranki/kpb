#include "test_basic_list.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/basic_list.h"

int kpb_test_basic_list(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_basic_list_t basic_list;
    kpb_test_basic_list_init(&basic_list);
    result_size = 0;
    result = kpb_test_basic_list_serialize_to_string(&basic_list, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 12, result, result_size);
    kpb_io_free(result);
    EXPECT_EQUAL(kpb_test_basic_list_bitfield_size(&basic_list), 0);
    EXPECT_EQUAL(kpb_test_basic_list_numbers_size(&basic_list), 0);

    EXPECT_TRUE(kpb_test_basic_list_add_bitfield(&basic_list, 0x7));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0xffff));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0xffff));
    result_size = 0;
    result = kpb_test_basic_list_serialize_to_string(&basic_list, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xe0\x00\xff\xff\xff\xff\x00\x00\x00\x00\x00\x00", 12, result, result_size);
    kpb_io_free(result);
    EXPECT_EQUAL(kpb_test_basic_list_bitfield_size(&basic_list), 1);
    EXPECT_EQUAL(kpb_test_basic_list_numbers_size(&basic_list), 2);

    kpb_test_basic_list_clear(&basic_list);
    EXPECT_EQUAL(kpb_test_basic_list_bitfield_size(&basic_list), 0);
    EXPECT_EQUAL(kpb_test_basic_list_numbers_size(&basic_list), 0);

    kpb_test_basic_list_clear(&basic_list);
    EXPECT_TRUE(kpb_test_basic_list_add_bitfield(&basic_list, 0x7));
    EXPECT_TRUE(kpb_test_basic_list_add_bitfield(&basic_list, 0x7));
    EXPECT_TRUE(kpb_test_basic_list_add_bitfield(&basic_list, 0x7));
    EXPECT_TRUE(kpb_test_basic_list_add_bitfield(&basic_list, 0x7));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0xffff));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0xffff));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0xffff));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0xffff));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0xffff));
    result_size = 0;
    result = kpb_test_basic_list_serialize_to_string(&basic_list, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xff\xf0\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff", 12, result, result_size);
    kpb_io_free(result);
    EXPECT_EQUAL(kpb_test_basic_list_bitfield_size(&basic_list), 4);
    EXPECT_EQUAL(kpb_test_basic_list_numbers_size(&basic_list), 5);

    kpb_test_basic_list_clear(&basic_list);
    EXPECT_TRUE(kpb_test_basic_list_add_bitfield(&basic_list, 0x7));
    EXPECT_TRUE(kpb_test_basic_list_add_bitfield(&basic_list, 0x7));
    EXPECT_TRUE(kpb_test_basic_list_add_bitfield(&basic_list, 0x7));
    EXPECT_TRUE(kpb_test_basic_list_add_bitfield(&basic_list, 0x7));
    EXPECT_TRUE(kpb_test_basic_list_add_bitfield(&basic_list, 0x7));
    EXPECT_TRUE(kpb_test_basic_list_add_bitfield(&basic_list, 0x7));
    EXPECT_TRUE(kpb_test_basic_list_add_bitfield(&basic_list, 0x7));
    EXPECT_TRUE(kpb_test_basic_list_add_bitfield(&basic_list, 0x7));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0xffff));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0xffff));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0xffff));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0xffff));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0xffff));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0xffff));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0xffff));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0xffff));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0xffff));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0xffff));
    result_size = 0;
    result = kpb_test_basic_list_serialize_to_string(&basic_list, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xff\xf0\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff", 12, result, result_size);
    kpb_io_free(result);
    EXPECT_EQUAL(kpb_test_basic_list_bitfield_size(&basic_list), 8);
    EXPECT_EQUAL(kpb_test_basic_list_numbers_size(&basic_list), 10);

    kpb_test_basic_list_clear(&basic_list);
    EXPECT_TRUE(kpb_test_basic_list_add_bitfield(&basic_list, 0x5));
    EXPECT_TRUE(kpb_test_basic_list_add_bitfield(&basic_list, 0x2));
    EXPECT_TRUE(kpb_test_basic_list_add_bitfield(&basic_list, 0x5));
    EXPECT_TRUE(kpb_test_basic_list_add_bitfield(&basic_list, 0x2));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0xabcd));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0xef01));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0x2345));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0x6789));
    EXPECT_TRUE(kpb_test_basic_list_add_numbers(&basic_list, 0xfeba));
    result_size = 0;
    result = kpb_test_basic_list_serialize_to_string(&basic_list, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xaa\xa0\xcd\xab\x01\xef\x45\x23\x89\x67\xba\xfe", 12, result, result_size);
    kpb_io_free(result);
    EXPECT_EQUAL(kpb_test_basic_list_bitfield_size(&basic_list), 4);
    EXPECT_EQUAL(kpb_test_basic_list_numbers_size(&basic_list), 5);

    kpb_test_basic_list_clear(&basic_list);
    EXPECT_TRUE(kpb_test_basic_list_parse_from_string(&basic_list, "\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff", 12));
    EXPECT_EQUAL(kpb_test_basic_list_bitfield_size(&basic_list), 4);
    EXPECT_EQUAL(kpb_test_basic_list_numbers_size(&basic_list), 5);
    EXPECT_EQUAL(kpb_test_basic_list_bitfield(&basic_list)[0], 0x7);
    EXPECT_EQUAL(kpb_test_basic_list_bitfield(&basic_list)[1], 0x7);
    EXPECT_EQUAL(kpb_test_basic_list_bitfield(&basic_list)[2], 0x7);
    EXPECT_EQUAL(kpb_test_basic_list_bitfield(&basic_list)[3], 0x7);
    EXPECT_EQUAL(kpb_test_basic_list_numbers(&basic_list)[0], 0xffff);
    EXPECT_EQUAL(kpb_test_basic_list_numbers(&basic_list)[1], 0xffff);
    EXPECT_EQUAL(kpb_test_basic_list_numbers(&basic_list)[2], 0xffff);
    EXPECT_EQUAL(kpb_test_basic_list_numbers(&basic_list)[3], 0xffff);
    EXPECT_EQUAL(kpb_test_basic_list_numbers(&basic_list)[4], 0xffff);
    EXPECT_TRUE(kpb_test_basic_list_parse_from_string(&basic_list, "\x00\x0f\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 12));
    EXPECT_EQUAL(kpb_test_basic_list_bitfield_size(&basic_list), 4);
    EXPECT_EQUAL(kpb_test_basic_list_numbers_size(&basic_list), 5);
    EXPECT_EQUAL(kpb_test_basic_list_bitfield(&basic_list)[0], 0);
    EXPECT_EQUAL(kpb_test_basic_list_bitfield(&basic_list)[1], 0);
    EXPECT_EQUAL(kpb_test_basic_list_bitfield(&basic_list)[2], 0);
    EXPECT_EQUAL(kpb_test_basic_list_bitfield(&basic_list)[3], 0);
    EXPECT_EQUAL(kpb_test_basic_list_numbers(&basic_list)[0], 0);
    EXPECT_EQUAL(kpb_test_basic_list_numbers(&basic_list)[1], 0);
    EXPECT_EQUAL(kpb_test_basic_list_numbers(&basic_list)[2], 0);
    EXPECT_EQUAL(kpb_test_basic_list_numbers(&basic_list)[3], 0);
    EXPECT_EQUAL(kpb_test_basic_list_numbers(&basic_list)[4], 0);
    EXPECT_TRUE(kpb_test_basic_list_parse_from_string(&basic_list, "\xaa\xaf\xcd\xab\x01\xef\x45\x23\x89\x67\xba\xfe", 12));
    EXPECT_EQUAL(kpb_test_basic_list_bitfield_size(&basic_list), 4);
    EXPECT_EQUAL(kpb_test_basic_list_numbers_size(&basic_list), 5);
    EXPECT_EQUAL(kpb_test_basic_list_bitfield(&basic_list)[0], 0x5);
    EXPECT_EQUAL(kpb_test_basic_list_bitfield(&basic_list)[1], 0x2);
    EXPECT_EQUAL(kpb_test_basic_list_bitfield(&basic_list)[2], 0x5);
    EXPECT_EQUAL(kpb_test_basic_list_bitfield(&basic_list)[3], 0x2);
    EXPECT_EQUAL(kpb_test_basic_list_numbers(&basic_list)[0], 0xabcd);
    EXPECT_EQUAL(kpb_test_basic_list_numbers(&basic_list)[1], 0xef01);
    EXPECT_EQUAL(kpb_test_basic_list_numbers(&basic_list)[2], 0x2345);
    EXPECT_EQUAL(kpb_test_basic_list_numbers(&basic_list)[3], 0x6789);
    EXPECT_EQUAL(kpb_test_basic_list_numbers(&basic_list)[4], 0xfeba);

    kpb_test_basic_list_fini(&basic_list);
    return 0;
}
