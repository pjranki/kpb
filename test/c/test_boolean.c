#include "test_boolean.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/boolean.h"

int kpb_test_boolean(void)
{
    kpb_test_boolean_t boolean;
    kpb_test_boolean_init(&boolean);
    EXPECT_TRUE(kpb_test_boolean_set_value1(&boolean, true));
    EXPECT_TRUE(kpb_test_boolean_value1(&boolean));
    EXPECT_TRUE(kpb_test_boolean_set_value1(&boolean, false));
    EXPECT_FALSE(kpb_test_boolean_value1(&boolean));
    kpb_test_boolean_fini(&boolean);
    return 0;
}
