#ifndef _H_XOR_ENCODING_H_
#define _H_XOR_ENCODING_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <string.h>

#include <kpb_io.h>

#include "c/process.h"

struct xor_encoding {
    uint8_t xor_key;
    const kpb_test_process_config_t *config;
};

void xor_encoding_init(struct xor_encoding *_this, uint8_t xor_key, const kpb_test_process_config_t *config);
size_t xor_encoding_encode_size(struct xor_encoding *_this, struct kpb_io *_io);
bool xor_encoding_encode(struct xor_encoding *_this, struct kpb_io *_io);
bool xor_encoding_decode(struct xor_encoding *_this, struct kpb_io *_io);
void xor_encoding_fini(struct xor_encoding *_this);

#ifdef __cplusplus
};
#endif

#endif
