#include "test_value_instances.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/value_instances.h"

int kpb_test_value_instances(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_value_instances_t value_instances;
    kpb_test_value_instances_init(&value_instances);
    EXPECT_TRUE(kpb_test_value_instances_set_value1(&value_instances, 0xdeadbeef));
    result_size = 0;
    result = kpb_test_value_instances_serialize_to_string(&value_instances, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xef\xbe\xad\xde", 4, result, result_size);
    kpb_io_free(result);
    EXPECT_EQUAL(0xdeadbeef, value_instances.value1);
    EXPECT_EQUAL(-1234, kpb_test_value_instances_instance1(&value_instances));
    EXPECT_EQUAL(5678, kpb_test_value_instances_instance2(&value_instances));
    EXPECT_TRUE(kpb_test_value_instances_parse_from_string(&value_instances, "\xbe\xba\xfe\xca", 4));
    EXPECT_EQUAL(0xcafebabe, value_instances.value1);
    EXPECT_EQUAL(-1234, kpb_test_value_instances_instance1(&value_instances));
    EXPECT_EQUAL(5678, kpb_test_value_instances_instance2(&value_instances));
    kpb_test_value_instances_fini(&value_instances);
    return 0;
}
