#include "test_process_value.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/process_value.h"

int kpb_test_process_value(void)
{
    kpb_test_process_value_t obj;
    kpb_test_process_value_init(&obj);

    // value1 tests
    EXPECT_EQUAL(10, kpb_test_process_value_value1(&obj));
    EXPECT_TRUE(kpb_test_process_value_set_value1(&obj, 20));
    EXPECT_EQUAL(25, kpb_test_process_value_value1(&obj));

    // value2 tests
    EXPECT_EQUAL(false, kpb_test_process_value_value2(&obj));
    EXPECT_TRUE(kpb_test_process_value_set_value2(&obj, true));
    EXPECT_EQUAL(true, kpb_test_process_value_value2(&obj));
    EXPECT_TRUE(kpb_test_process_value_set_value2(&obj, false));
    EXPECT_EQUAL(false, kpb_test_process_value_value2(&obj));
    EXPECT_TRUE(kpb_test_process_value_set_invert_logic(&obj, true));
    EXPECT_TRUE(kpb_test_process_value_set_value2(&obj, true));
    EXPECT_TRUE(kpb_test_process_value_set_invert_logic(&obj, false));
    EXPECT_EQUAL(false, kpb_test_process_value_value2(&obj));
    EXPECT_TRUE(kpb_test_process_value_set_invert_logic(&obj, true));
    EXPECT_TRUE(kpb_test_process_value_set_value2(&obj, false));
    EXPECT_TRUE(kpb_test_process_value_set_invert_logic(&obj, false));
    EXPECT_EQUAL(true, kpb_test_process_value_value2(&obj));
    EXPECT_TRUE(kpb_test_process_value_set_value2(&obj, true));
    EXPECT_TRUE(kpb_test_process_value_set_invert_logic(&obj, true));
    EXPECT_EQUAL(false, kpb_test_process_value_value2(&obj));
    EXPECT_TRUE(kpb_test_process_value_set_invert_logic(&obj, false));
    EXPECT_TRUE(kpb_test_process_value_set_value2(&obj, false));
    EXPECT_TRUE(kpb_test_process_value_set_invert_logic(&obj, true));
    EXPECT_EQUAL(true, kpb_test_process_value_value2(&obj));
    EXPECT_TRUE(kpb_test_process_value_set_invert_logic(&obj, false));

    // value3 tests
    EXPECT_EQUAL(10, kpb_test_process_value_value3(&obj));
    EXPECT_TRUE(kpb_test_process_value_set_value3(&obj, 20));
    EXPECT_EQUAL(25, kpb_test_process_value_value3(&obj));

    // value4 tests
    EXPECT_EQUAL(0, kpb_test_process_value_value4(&obj));
    EXPECT_TRUE(kpb_test_process_value_set_memset_value(&obj, 10));
    EXPECT_EQUAL(10, kpb_test_process_value_value4(&obj));
    EXPECT_TRUE(kpb_test_process_value_set_memset_value(&obj, 4));
    EXPECT_TRUE(kpb_test_process_value_set_value4(&obj, 12));
    EXPECT_TRUE(kpb_test_process_value_set_memset_value(&obj, 1));
    EXPECT_EQUAL(9, kpb_test_process_value_value4(&obj));
    EXPECT_TRUE(kpb_test_process_value_set_memset_value(&obj, 0));

    // value5 tests
    // TODO - these were copied for C++ tests
    //EXPECT_STD_STRING_EQUAL(std::string(""), obj.value5());
    //EXPECT_TRUE(obj.set_value5("abc"));
    //EXPECT_STD_STRING_EQUAL(std::string("\x00\x00\x00", 3), obj.value5());
    //EXPECT_TRUE(kpb_test_process_value_set_memset_value(&obj, 'z'));
    //EXPECT_TRUE(obj.set_value5("abcd"));
    //EXPECT_STD_STRING_EQUAL(std::string("zzzz"), obj.value5());
    //EXPECT_TRUE(kpb_test_process_value_set_memset_value(&obj, 'y'));
    //EXPECT_STD_STRING_EQUAL(std::string("yyyy"), obj.value5());
    //EXPECT_TRUE(kpb_test_process_value_set_memset_value(&obj, 0));

    // value6 tests
    // TODO - these were copied for C++ tests
    //EXPECT_STD_STRING_EQUAL(std::string("", 0), obj.value6());
    //EXPECT_TRUE(obj.set_value6(std::string("abc", 3)));
    //EXPECT_STD_STRING_EQUAL(std::string("\x00\x00\x00", 3), obj.value6());
    //EXPECT_TRUE(kpb_test_process_value_set_memset_value(&obj, 'z'));
    //EXPECT_TRUE(obj.set_value6(std::string("abcd", 4)));
    //EXPECT_STD_STRING_EQUAL(std::string("zzzz", 4), obj.value6());
    //EXPECT_TRUE(kpb_test_process_value_set_memset_value(&obj, 'y'));
    //EXPECT_STD_STRING_EQUAL(std::string("yyyy", 4), obj.value6());
    //EXPECT_TRUE(kpb_test_process_value_set_memset_value(&obj, 0));

    // value7 tests
    EXPECT_EQUAL(0.0f, kpb_test_process_value_value7(&obj));
    EXPECT_TRUE(kpb_test_process_value_set_value7(&obj, 1.0f));
    EXPECT_EQUAL(1.0f, kpb_test_process_value_value7(&obj));
    EXPECT_TRUE(kpb_test_process_value_set_multiple_float_by_4(&obj, true));
    EXPECT_EQUAL(4.0f, kpb_test_process_value_value7(&obj));
    EXPECT_TRUE(kpb_test_process_value_set_value7(&obj, 2.0f));
    EXPECT_TRUE(kpb_test_process_value_set_multiple_float_by_4(&obj, false));
    EXPECT_EQUAL(8.0f, kpb_test_process_value_value7(&obj));

    // value8 tests
    EXPECT_TRUE(kpb_test_process_value_set_getter_count(&obj, 0));
    EXPECT_TRUE(kpb_test_process_value_set_setter_count(&obj, 0));
    EXPECT_EQUAL(0xff, kpb_test_sub_process_value_input_value(kpb_test_process_value_value8(&obj)));
    EXPECT_EQUAL(1, kpb_test_process_value_getter_count(&obj));
    EXPECT_EQUAL(0, kpb_test_process_value_setter_count(&obj));
    EXPECT_TRUE(kpb_test_sub_process_value_set_input_value(kpb_test_process_value_mutable_value8(&obj), 0xaa));
    EXPECT_EQUAL(0xaa, kpb_test_sub_process_value_input_value(kpb_test_process_value_value8(&obj)));
    EXPECT_EQUAL(2, kpb_test_process_value_getter_count(&obj));
    EXPECT_EQUAL(1, kpb_test_process_value_setter_count(&obj));

    // value9 tests
    EXPECT_TRUE(kpb_test_process_value_set_getter_count(&obj, 0));
    EXPECT_TRUE(kpb_test_process_value_set_setter_count(&obj, 0));
    EXPECT_EQUAL(0x00, kpb_test_process_value_memset_value(kpb_test_process_value_value9(&obj)));
    EXPECT_EQUAL(1, kpb_test_process_value_getter_count(&obj));
    EXPECT_EQUAL(0, kpb_test_process_value_setter_count(&obj));
    EXPECT_EQUAL(0x00, kpb_test_process_value_memset_value(kpb_test_process_value_value9(&obj)));
    EXPECT_EQUAL(2, kpb_test_process_value_getter_count(&obj));
    EXPECT_EQUAL(0, kpb_test_process_value_setter_count(&obj));

    // value10 tests
    EXPECT_EQUAL(false, kpb_test_process_value_value10(&obj));
    EXPECT_TRUE(kpb_test_process_value_set_invert_logic(&obj, true));
    EXPECT_EQUAL(true, kpb_test_process_value_value10(&obj));
    EXPECT_TRUE(kpb_test_process_value_set_invert_logic(&obj, false));
    EXPECT_EQUAL(false, kpb_test_process_value_value10(&obj));

    kpb_test_process_value_fini(&obj);
    return 0;
}
