#include "test_typecast.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/typecast.h"

int kpb_test_typecast(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_typecast_t typecast;
    kpb_test_typecast_init(&typecast);
    result_size = 0;
    result = kpb_test_typecast_serialize_to_string(&typecast, &result_size);
    EXPECT_NOT_NULL(result);
    kpb_io_free(result);
    kpb_test_typecast_fini(&typecast);
    return 0;
}
