#ifndef _H_KPB_TEST_SET_INSTANCE_VALUE_H_
#define _H_KPB_TEST_SET_INSTANCE_VALUE_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_set_instance_value(void);

#ifdef __cplusplus
};
#endif

#endif
