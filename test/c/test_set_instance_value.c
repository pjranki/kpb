#include "test_set_instance_value.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/set_instance_value.h"

int kpb_test_set_instance_value(void)
{
    kpb_test__set_instance_value_t obj;
    kpb_test__set_instance_value_init(&obj);

    // bool
    EXPECT_TRUE(kpb_test__set_instance_value_set_input_bool(&obj, true));
    EXPECT_EQUAL(true, kpb_test__set_instance_value_storage_bool(&obj));
    EXPECT_TRUE(kpb_test__set_instance_value_set_input_bool(&obj, false));
    EXPECT_EQUAL(false, kpb_test__set_instance_value_storage_bool(&obj));

    // bitfield
    EXPECT_TRUE(kpb_test__set_instance_value_set_input_b4(&obj, 0xa));
    EXPECT_EQUAL(0xa, kpb_test__set_instance_value_storage_b4(&obj));
    EXPECT_TRUE(kpb_test__set_instance_value_set_input_b4(&obj, 0x5));
    EXPECT_EQUAL(0x5, kpb_test__set_instance_value_storage_b4(&obj));

    // unsigned integer
    EXPECT_TRUE(kpb_test__set_instance_value_set_input_u4(&obj, 123));
    EXPECT_EQUAL(123, kpb_test__set_instance_value_storage_u4(&obj));
    EXPECT_TRUE(kpb_test__set_instance_value_set_input_u4(&obj, 456));
    EXPECT_EQUAL(456, kpb_test__set_instance_value_storage_u4(&obj));

    // signed integer
    EXPECT_TRUE(kpb_test__set_instance_value_set_input_s4(&obj, -12));
    EXPECT_EQUAL(-12, kpb_test__set_instance_value_storage_s4(&obj));
    EXPECT_TRUE(kpb_test__set_instance_value_set_input_s4(&obj, 45));
    EXPECT_EQUAL(45, kpb_test__set_instance_value_storage_s4(&obj));

    // signed integer
    EXPECT_TRUE(kpb_test__set_instance_value_set_input_s4(&obj, -12));
    EXPECT_EQUAL(-12, kpb_test__set_instance_value_storage_s4(&obj));
    EXPECT_TRUE(kpb_test__set_instance_value_set_input_s4(&obj, 45));
    EXPECT_EQUAL(45, kpb_test__set_instance_value_storage_s4(&obj));

    // float
    EXPECT_TRUE(kpb_test__set_instance_value_set_input_f4(&obj, 1.1f));
    EXPECT_EQUAL(1.1f, kpb_test__set_instance_value_storage_f4(&obj));
    EXPECT_TRUE(kpb_test__set_instance_value_set_input_f4(&obj, -2.3f));
    EXPECT_EQUAL(-2.3f, kpb_test__set_instance_value_storage_f4(&obj));

    // calculate and set _value
    EXPECT_TRUE(kpb_test__set_instance_value_set_calc_value_u4(&obj, 2));
    EXPECT_EQUAL(9, kpb_test__set_instance_value_storage_u4(&obj));

    // check nested sets
    EXPECT_TRUE(kpb_test__set_instance_value_set_nested_1(&obj, 1));
    EXPECT_EQUAL(1 * 2 * 3 * 4, kpb_test__set_instance_value_nested_4(&obj));
    EXPECT_EQUAL(1 * 2 * 3 * 4 * 5, kpb_test__set_instance_value_nested_3(&obj));

    kpb_test__set_instance_value_fini(&obj);
    return 0;
}
