#ifndef _H_KPB_TEST_PROCESS_H_
#define _H_KPB_TEST_PROCESS_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_process(void);

#ifdef __cplusplus
};
#endif

#endif
