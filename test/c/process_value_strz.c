#include "process_value_strz.h"

#include <kpb_io.h>

void process_value_strz_init(struct process_value_strz *_this, uint8_t memset_value)
{
    memset(_this, 0, sizeof(*_this));
    _this->memset_value_ = memset_value;
}

const char *process_value_strz_getter(struct process_value_strz *_this, const char *value)
{
    // TODO
    (void)_this;
    return value;
}

char *process_value_strz_setter(struct process_value_strz *_this, char *value)
{
    // TODO
    (void)_this;
    return value;
}

void process_value_strz_fini(struct process_value_strz *_this)
{
    memset(_this, 0, sizeof(*_this));
}
