#include "test_floats.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/floats.h"

#define FLOAT_0_0_LE            "\x00\x00\x00\x00"
#define DOUBLE_0_0_LE           "\x00\x00\x00\x00\x00\x00\x00\x00"
#define FLOAT_0_0_BE            "\x00\x00\x00\x00"
#define DOUBLE_0_0_BE           "\x00\x00\x00\x00\x00\x00\x00\x00"
#define FLOAT_1_1_LE            "\xcd\xcc\x8c\x3f"
#define DOUBLE_1_1_LE           "\x9a\x99\x99\x99\x99\x99\xf1\x3f"
#define FLOAT_1_1_BE            "\x3f\x8c\xcc\xcd"
#define DOUBLE_1_1_BE           "\x3f\xf1\x99\x99\x99\x99\x99\x9a"
#define FLOAT_NEG_2_2_LE        "\xcd\xcc\x0c\xc0"
#define DOUBLE_NEG_2_2_LE       "\x9a\x99\x99\x99\x99\x99\x01\xc0"
#define FLOAT_NEG_2_2_BE        "\xc0\x0c\xcc\xcd"
#define DOUBLE_NEG_2_2_BE       "\xc0\x01\x99\x99\x99\x99\x99\x9a"

#define EXPECTED_0_0            (FLOAT_0_0_LE DOUBLE_0_0_LE FLOAT_0_0_LE DOUBLE_0_0_LE FLOAT_0_0_BE DOUBLE_0_0_BE)
#define EXPECTED_0_0_SIZE       (sizeof(EXPECTED_0_0)-1)
#define EXPECTED_1_1            (FLOAT_1_1_LE DOUBLE_1_1_LE FLOAT_1_1_LE DOUBLE_1_1_LE FLOAT_1_1_BE DOUBLE_1_1_BE)
#define EXPECTED_1_1_SIZE       (sizeof(EXPECTED_1_1)-1)
#define EXPECTED_NEG_2_2        (FLOAT_NEG_2_2_LE DOUBLE_NEG_2_2_LE FLOAT_NEG_2_2_LE DOUBLE_NEG_2_2_LE FLOAT_NEG_2_2_BE DOUBLE_NEG_2_2_BE)
#define EXPECTED_NEG_2_2_SIZE   (sizeof(EXPECTED_NEG_2_2)-1)

int kpb_test_floats(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_floats_t floats;
    kpb_test_floats_init(&floats);
    result = kpb_test_floats_serialize_to_string(&floats, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(EXPECTED_0_0, EXPECTED_0_0_SIZE, result, result_size);
    kpb_io_free(result);
    EXPECT_EQUAL(0.0, kpb_test_floats_float_(&floats));
    EXPECT_EQUAL(0.0, kpb_test_floats_double_(&floats));
    EXPECT_EQUAL(0.0, kpb_test_floats_floatle(&floats));
    EXPECT_EQUAL(0.0, kpb_test_floats_doublele(&floats));
    EXPECT_EQUAL(0.0, kpb_test_floats_floatbe(&floats));
    EXPECT_EQUAL(0.0, kpb_test_floats_doublebe(&floats));

    EXPECT_TRUE(kpb_test_floats_set_float_(&floats, 1.1f));
    EXPECT_TRUE(kpb_test_floats_set_double_(&floats, 1.1));
    EXPECT_TRUE(kpb_test_floats_set_floatle(&floats, 1.1f));
    EXPECT_TRUE(kpb_test_floats_set_doublele(&floats, 1.1));
    EXPECT_TRUE(kpb_test_floats_set_floatbe(&floats, 1.1f));
    EXPECT_TRUE(kpb_test_floats_set_doublebe(&floats, 1.1));
    EXPECT_EQUAL(1.1f, kpb_test_floats_float_(&floats));
    EXPECT_EQUAL(1.1, kpb_test_floats_double_(&floats));
    EXPECT_EQUAL(1.1f, kpb_test_floats_floatle(&floats));
    EXPECT_EQUAL(1.1, kpb_test_floats_doublele(&floats));
    EXPECT_EQUAL(1.1f, kpb_test_floats_floatbe(&floats));
    EXPECT_EQUAL(1.1, kpb_test_floats_doublebe(&floats));
    result = kpb_test_floats_serialize_to_string(&floats, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(EXPECTED_1_1, EXPECTED_1_1_SIZE, result, result_size);
    kpb_io_free(result);

    EXPECT_TRUE(kpb_test_floats_set_float_(&floats, -2.2f));
    EXPECT_TRUE(kpb_test_floats_set_double_(&floats, -2.2));
    EXPECT_TRUE(kpb_test_floats_set_floatle(&floats, -2.2f));
    EXPECT_TRUE(kpb_test_floats_set_doublele(&floats, -2.2));
    EXPECT_TRUE(kpb_test_floats_set_floatbe(&floats, -2.2f));
    EXPECT_TRUE(kpb_test_floats_set_doublebe(&floats, -2.2));
    EXPECT_EQUAL(-2.2f, kpb_test_floats_float_(&floats));
    EXPECT_EQUAL(-2.2, kpb_test_floats_double_(&floats));
    EXPECT_EQUAL(-2.2f, kpb_test_floats_floatle(&floats));
    EXPECT_EQUAL(-2.2, kpb_test_floats_doublele(&floats));
    EXPECT_EQUAL(-2.2f, kpb_test_floats_floatbe(&floats));
    EXPECT_EQUAL(-2.2, kpb_test_floats_doublebe(&floats));
    result = kpb_test_floats_serialize_to_string(&floats, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(EXPECTED_NEG_2_2, EXPECTED_NEG_2_2_SIZE, result, result_size);
    kpb_io_free(result);

    kpb_test_floats_clear(&floats);
    EXPECT_EQUAL(0.0, kpb_test_floats_float_(&floats));
    EXPECT_EQUAL(0.0, kpb_test_floats_double_(&floats));
    EXPECT_EQUAL(0.0, kpb_test_floats_floatle(&floats));
    EXPECT_EQUAL(0.0, kpb_test_floats_doublele(&floats));
    EXPECT_EQUAL(0.0, kpb_test_floats_floatbe(&floats));
    EXPECT_EQUAL(0.0, kpb_test_floats_doublebe(&floats));
    result = kpb_test_floats_serialize_to_string(&floats, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(EXPECTED_0_0, EXPECTED_0_0_SIZE, result, result_size);
    kpb_io_free(result);

    EXPECT_TRUE(kpb_test_floats_parse_from_string(&floats, EXPECTED_1_1, EXPECTED_1_1_SIZE));
    EXPECT_EQUAL(1.1f, kpb_test_floats_float_(&floats));
    EXPECT_EQUAL(1.1, kpb_test_floats_double_(&floats));
    EXPECT_EQUAL(1.1f, kpb_test_floats_floatle(&floats));
    EXPECT_EQUAL(1.1, kpb_test_floats_doublele(&floats));
    EXPECT_EQUAL(1.1f, kpb_test_floats_floatbe(&floats));
    EXPECT_EQUAL(1.1, kpb_test_floats_doublebe(&floats));

    EXPECT_TRUE(kpb_test_floats_parse_from_string(&floats, EXPECTED_NEG_2_2, EXPECTED_NEG_2_2_SIZE));
    EXPECT_EQUAL(-2.2f, kpb_test_floats_float_(&floats));
    EXPECT_EQUAL(-2.2, kpb_test_floats_double_(&floats));
    EXPECT_EQUAL(-2.2f, kpb_test_floats_floatle(&floats));
    EXPECT_EQUAL(-2.2, kpb_test_floats_doublele(&floats));
    EXPECT_EQUAL(-2.2f, kpb_test_floats_floatbe(&floats));
    EXPECT_EQUAL(-2.2, kpb_test_floats_doublebe(&floats));

    EXPECT_TRUE(kpb_test_floats_parse_from_string(&floats, EXPECTED_0_0, EXPECTED_0_0_SIZE));
    EXPECT_EQUAL(0.0, kpb_test_floats_float_(&floats));
    EXPECT_EQUAL(0.0, kpb_test_floats_double_(&floats));
    EXPECT_EQUAL(0.0, kpb_test_floats_floatle(&floats));
    EXPECT_EQUAL(0.0, kpb_test_floats_doublele(&floats));
    EXPECT_EQUAL(0.0, kpb_test_floats_floatbe(&floats));
    EXPECT_EQUAL(0.0, kpb_test_floats_doublebe(&floats));

    kpb_test_floats_fini(&floats);
    return 0;
}
