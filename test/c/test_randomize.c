#include "test_randomize.h"

#include <stdio.h>

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#include <Windows.h>
#include <Wincrypt.h>
#else
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#endif

#include "../common/macro.h"

#include "c/randomize.h"

static bool custom_random_bytes(void *ctx, char *data, size_t size)
{
#ifdef WIN32
    (void)ctx;
    HCRYPTPROV hCryptProv;
    if ( !CryptAcquireContext(&hCryptProv, NULL, NULL, PROV_RSA_FULL, 0) )
    {
        return false;
    }
    if ( !CryptGenRandom(hCryptProv, (DWORD)size, (BYTE *)data) )
    {
        CryptReleaseContext(hCryptProv, 0);
        return false;
    }
    CryptReleaseContext(hCryptProv, 0);
    return true;
#else
    (void)ctx;
    int fd = open("/dev/urandom", O_RDONLY);
    if ( fd < 0 )
    {
        return false;
    }
    ssize_t recd = read(fd, data, (unsigned int)size);
    close(fd);
    if ( recd < 0 )
    {
        return false;
    }
    if ((size_t)recd != size)
    {
        return false;
    }
    return true;
#endif
}

static char g_random[50];
static size_t g_random_index = 0;
static size_t g_random_size = 0;

static bool custom_fixed_bytes(void *ctx, char *data, size_t size)
{
    (void)ctx;
    for ( size_t i = 0; i < size; i++ )
    {
        if ( g_random_index < g_random_size )
        {
            data[i] = g_random[g_random_index];
            g_random_index++;
        }
        else
        {
            data[i] = 0;
        }
    }
    return true;
}

static void kpb_random_from_buffer(const char *data, size_t size)
{
    // deterministic data
    memcpy(g_random, data, size);
    g_random_index = 0;
    g_random_size = size;
    kpb_random_api(custom_fixed_bytes, NULL);
}

static void kpb_random_os(void)
{
    // to make things "random", we need a way to get random bytes
    kpb_random_api(custom_random_bytes, NULL);
}

int kpb_test_randomize(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_randomize__t randomize;
    kpb_test_randomize__init(&randomize);

    EXPECT_TRUE(kpb_test_randomize__set_s1_min(&randomize, -128));
    EXPECT_TRUE(kpb_test_randomize__set_s1_max(&randomize, 127));
    kpb_random_from_buffer("\x00", 1);
    EXPECT_TRUE(kpb_test_randomize__randomize_s1(&randomize));
    EXPECT_EQUAL(-128, kpb_test_randomize__s1(&randomize));
    kpb_random_from_buffer("\xff", 1);
    EXPECT_TRUE(kpb_test_randomize__randomize_s1(&randomize));
    EXPECT_EQUAL(127, kpb_test_randomize__s1(&randomize));
    EXPECT_TRUE(kpb_test_randomize__set_s1_min(&randomize, -127));
    EXPECT_TRUE(kpb_test_randomize__set_s1_max(&randomize, 126));
    kpb_random_from_buffer("\x00", 1);
    EXPECT_TRUE(kpb_test_randomize__randomize_s1(&randomize));
    EXPECT_EQUAL(-127, kpb_test_randomize__s1(&randomize));
    kpb_random_from_buffer("\xfd", 1);
    EXPECT_TRUE(kpb_test_randomize__randomize_s1(&randomize));
    EXPECT_EQUAL(126, kpb_test_randomize__s1(&randomize));
    EXPECT_TRUE(kpb_test_randomize__set_s1_min(&randomize, 0x42));
    EXPECT_TRUE(kpb_test_randomize__set_s1_max(&randomize, 0x42));
    kpb_random_from_buffer("\x12", 1);
    EXPECT_TRUE(kpb_test_randomize__randomize_s1(&randomize));
    EXPECT_EQUAL(0x42, kpb_test_randomize__s1(&randomize));

    EXPECT_TRUE(kpb_test_randomize__set_count_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_count_max(&randomize, 3));
    EXPECT_TRUE(kpb_test_randomize__set_s1_min(&randomize, -128));
    EXPECT_TRUE(kpb_test_randomize__set_s1_max(&randomize, 127));
    kpb_random_from_buffer("\x00\x00\x00\x02\x00\x00\x00", 7);
    EXPECT_TRUE(kpb_test_randomize__randomize_s1_list(&randomize));
    EXPECT_EQUAL(-128, kpb_test_randomize__s1_list_at(&randomize, 0));
    EXPECT_EQUAL(-128, kpb_test_randomize__s1_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__s1_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02\xff\xff\xff", 7);
    EXPECT_TRUE(kpb_test_randomize__randomize_s1_list(&randomize));
    EXPECT_EQUAL(127, kpb_test_randomize__s1_list_at(&randomize, 0));
    EXPECT_EQUAL(127, kpb_test_randomize__s1_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__s1_list_at(&randomize, 2));
    EXPECT_TRUE(kpb_test_randomize__set_s1_min(&randomize, -127));
    EXPECT_TRUE(kpb_test_randomize__set_s1_max(&randomize, 126));
    kpb_random_from_buffer("\x00\x00\x00\x02\x00\x00\x00", 7);
    EXPECT_TRUE(kpb_test_randomize__randomize_s1_list(&randomize));
    EXPECT_EQUAL(-127, kpb_test_randomize__s1_list_at(&randomize, 0));
    EXPECT_EQUAL(-127, kpb_test_randomize__s1_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__s1_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02\xfd\xfd\xfd", 7);
    EXPECT_TRUE(kpb_test_randomize__randomize_s1_list(&randomize));
    EXPECT_EQUAL(126, kpb_test_randomize__s1_list_at(&randomize, 0));
    EXPECT_EQUAL(126, kpb_test_randomize__s1_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__s1_list_at(&randomize, 2));
    EXPECT_TRUE(kpb_test_randomize__set_s1_min(&randomize, 0x42));
    EXPECT_TRUE(kpb_test_randomize__set_s1_max(&randomize, 0x42));
    kpb_random_from_buffer("\x00\x00\x00\x02\x12\x34\x56", 7);
    EXPECT_TRUE(kpb_test_randomize__randomize_s1_list(&randomize));
    EXPECT_EQUAL(0x42, kpb_test_randomize__s1_list_at(&randomize, 0));
    EXPECT_EQUAL(0x42, kpb_test_randomize__s1_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__s1_list_at(&randomize, 2));

    EXPECT_TRUE(kpb_test_randomize__set_s2_min(&randomize, ~((int16_t)0x7fff)));
    EXPECT_TRUE(kpb_test_randomize__set_s2_max(&randomize, 0x7fff));
    kpb_random_from_buffer("\x00\x00", 2);
    EXPECT_TRUE(kpb_test_randomize__randomize_s2(&randomize));
    EXPECT_EQUAL(~((int16_t)0x7fff), kpb_test_randomize__s2(&randomize));
    kpb_random_from_buffer("\xff\xff", 2);
    EXPECT_TRUE(kpb_test_randomize__randomize_s2(&randomize));
    EXPECT_EQUAL(0x7fff, kpb_test_randomize__s2(&randomize));
    EXPECT_TRUE(kpb_test_randomize__set_s2_min(&randomize, ~((int16_t)0x7ffe)));
    EXPECT_TRUE(kpb_test_randomize__set_s2_max(&randomize, 0x7ffe));
    kpb_random_from_buffer("\x00\x00", 2);
    EXPECT_TRUE(kpb_test_randomize__randomize_s2(&randomize));
    EXPECT_EQUAL(~((int16_t)0x7ffe), kpb_test_randomize__s2(&randomize));
    kpb_random_from_buffer("\xff\xfd", 2);
    EXPECT_TRUE(kpb_test_randomize__randomize_s2(&randomize));
    EXPECT_EQUAL(0x7ffe, kpb_test_randomize__s2(&randomize));
    EXPECT_TRUE(kpb_test_randomize__set_s2_min(&randomize, 0x7bcd));
    EXPECT_TRUE(kpb_test_randomize__set_s2_max(&randomize, 0x7bcd));
    kpb_random_from_buffer("\x12\x34", 2);
    EXPECT_TRUE(kpb_test_randomize__randomize_s2(&randomize));
    EXPECT_EQUAL(0x7bcd, kpb_test_randomize__s2(&randomize));

    EXPECT_TRUE(kpb_test_randomize__set_count_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_count_max(&randomize, 3));
    EXPECT_TRUE(kpb_test_randomize__set_s2_min(&randomize, ~((int16_t)0x7fff)));
    EXPECT_TRUE(kpb_test_randomize__set_s2_max(&randomize, 0x7fff));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00" "\x00\x00" "\x00\x00", 4 + (2 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_s2_list(&randomize));
    EXPECT_EQUAL(~((int16_t)0x7fff), kpb_test_randomize__s2_list_at(&randomize, 0));
    EXPECT_EQUAL(~((int16_t)0x7fff), kpb_test_randomize__s2_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__s2_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff" "\xff\xff" "\xff\xff", 4 + (2 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_s2_list(&randomize));
    EXPECT_EQUAL(0x7fff, kpb_test_randomize__s2_list_at(&randomize, 0));
    EXPECT_EQUAL(0x7fff, kpb_test_randomize__s2_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__s2_list_at(&randomize, 2));
    EXPECT_TRUE(kpb_test_randomize__set_s2_min(&randomize, ~((int16_t)0x7ffe)));
    EXPECT_TRUE(kpb_test_randomize__set_s2_max(&randomize, 0x7ffe));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00" "\x00\x00" "\x00\x00", 4 + (2 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_s2_list(&randomize));
    EXPECT_EQUAL(~((int16_t)0x7ffe), kpb_test_randomize__s2_list_at(&randomize, 0));
    EXPECT_EQUAL(~((int16_t)0x7ffe), kpb_test_randomize__s2_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__s2_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xfd" "\xff\xfd" "\xff\xfd", 4 + (2 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_s2_list(&randomize));
    EXPECT_EQUAL(0x7ffe, kpb_test_randomize__s2_list_at(&randomize, 0));
    EXPECT_EQUAL(0x7ffe, kpb_test_randomize__s2_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__s2_list_at(&randomize, 2));
    EXPECT_TRUE(kpb_test_randomize__set_s2_min(&randomize, 0x7bcd));
    EXPECT_TRUE(kpb_test_randomize__set_s2_max(&randomize, 0x7bcd));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x12\x34" "\x56\x78" "\x90\xab", 4 + (2 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_s2_list(&randomize));
    EXPECT_EQUAL(0x7bcd, kpb_test_randomize__s2_list_at(&randomize, 0));
    EXPECT_EQUAL(0x7bcd, kpb_test_randomize__s2_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__s2_list_at(&randomize, 2));

    EXPECT_TRUE(kpb_test_randomize__set_s4_min(&randomize, ~((int32_t)0x7fffffff)));
    EXPECT_TRUE(kpb_test_randomize__set_s4_max(&randomize, 0x7fffffff));
    kpb_random_from_buffer("\x00\x00\x00\x00", 4);
    EXPECT_TRUE(kpb_test_randomize__randomize_s4(&randomize));
    EXPECT_EQUAL(~((int32_t)0x7fffffff), kpb_test_randomize__s4(&randomize));
    kpb_random_from_buffer("\xff\xff\xff\xff", 4);
    EXPECT_TRUE(kpb_test_randomize__randomize_s4(&randomize));
    EXPECT_EQUAL(0x7fffffff, kpb_test_randomize__s4(&randomize));
    EXPECT_TRUE(kpb_test_randomize__set_s4_min(&randomize, ~((int32_t)0x7ffffffe)));
    EXPECT_TRUE(kpb_test_randomize__set_s4_max(&randomize, 0x7ffffffe));
    kpb_random_from_buffer("\x00\x00\x00\x00", 4);
    EXPECT_TRUE(kpb_test_randomize__randomize_s4(&randomize));
    EXPECT_EQUAL(~((int32_t)0x7ffffffe), kpb_test_randomize__s4(&randomize));
    kpb_random_from_buffer("\xff\xff\xff\xfd", 4);
    EXPECT_TRUE(kpb_test_randomize__randomize_s4(&randomize));
    EXPECT_EQUAL(0x7ffffffe, kpb_test_randomize__s4(&randomize));
    EXPECT_TRUE(kpb_test_randomize__set_s4_min(&randomize, 0x7bcd1234));
    EXPECT_TRUE(kpb_test_randomize__set_s4_max(&randomize, 0x7bcd1234));
    kpb_random_from_buffer("\x12\x34\x56\x78", 4);
    EXPECT_TRUE(kpb_test_randomize__randomize_s4(&randomize));
    EXPECT_EQUAL(0x7bcd1234, kpb_test_randomize__s4(&randomize));

    EXPECT_TRUE(kpb_test_randomize__set_count_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_count_max(&randomize, 3));
    EXPECT_TRUE(kpb_test_randomize__set_s4_min(&randomize, ~((int32_t)0x7fffffff)));
    EXPECT_TRUE(kpb_test_randomize__set_s4_max(&randomize, 0x7fffffff));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "\x00\x00\x00\x00" "\x00\x00\x00\x00", 4 + (4 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_s4_list(&randomize));
    EXPECT_EQUAL(~((int32_t)0x7fffffff), kpb_test_randomize__s4_list_at(&randomize, 0));
    EXPECT_EQUAL(~((int32_t)0x7fffffff), kpb_test_randomize__s4_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__s4_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff\xff\xff" "\xff\xff\xff\xff" "\xff\xff\xff\xff", 4 + (4 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_s4_list(&randomize));
    EXPECT_EQUAL(0x7fffffff, kpb_test_randomize__s4_list_at(&randomize, 0));
    EXPECT_EQUAL(0x7fffffff, kpb_test_randomize__s4_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__s4_list_at(&randomize, 2));
    EXPECT_TRUE(kpb_test_randomize__set_s4_min(&randomize, ~((int32_t)0x7ffffffe)));
    EXPECT_TRUE(kpb_test_randomize__set_s4_max(&randomize, 0x7ffffffe));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "\x00\x00\x00\x00" "\x00\x00\x00\x00", 4 + (4 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_s4_list(&randomize));
    EXPECT_EQUAL(~((int32_t)0x7ffffffe), kpb_test_randomize__s4_list_at(&randomize, 0));
    EXPECT_EQUAL(~((int32_t)0x7ffffffe), kpb_test_randomize__s4_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__s4_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff\xff\xfd" "\xff\xff\xff\xfd" "\xff\xff\xff\xfd", 4 + (4 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_s4_list(&randomize));
    EXPECT_EQUAL(0x7ffffffe, kpb_test_randomize__s4_list_at(&randomize, 0));
    EXPECT_EQUAL(0x7ffffffe, kpb_test_randomize__s4_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__s4_list_at(&randomize, 2));
    EXPECT_TRUE(kpb_test_randomize__set_s4_min(&randomize, 0x7bcd2345));
    EXPECT_TRUE(kpb_test_randomize__set_s4_max(&randomize, 0x7bcd2345));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x12\x34\x12\x34" "\x56\x78\x56\x78" "\x90\xab\x90\xab", 4 + (4 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_s4_list(&randomize));
    EXPECT_EQUAL(0x7bcd2345, kpb_test_randomize__s4_list_at(&randomize, 0));
    EXPECT_EQUAL(0x7bcd2345, kpb_test_randomize__s4_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__s4_list_at(&randomize, 2));

    EXPECT_TRUE(kpb_test_randomize__set_s8_min(&randomize, ~((int64_t)0x7fffffffffffffff)));
    EXPECT_TRUE(kpb_test_randomize__set_s8_max(&randomize, 0x7fffffffffffffff));
    kpb_random_from_buffer("\x00\x00\x00\x00\x00\x00\x00\x00", 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_s8(&randomize));
    EXPECT_EQUAL(~((int64_t)0x7fffffffffffffff), kpb_test_randomize__s8(&randomize));
    kpb_random_from_buffer("\xff\xff\xff\xff\xff\xff\xff\xff", 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_s8(&randomize));
    EXPECT_EQUAL(0x7fffffffffffffff, kpb_test_randomize__s8(&randomize));
    EXPECT_TRUE(kpb_test_randomize__set_s8_min(&randomize, ~((int64_t)0x7ffffffffffffffe)));
    EXPECT_TRUE(kpb_test_randomize__set_s8_max(&randomize, 0x7ffffffffffffffe));
    kpb_random_from_buffer("\x00\x00\x00\x00\x00\x00\x00\x00", 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_s8(&randomize));
    EXPECT_EQUAL(~((int64_t)0x7ffffffffffffffe), kpb_test_randomize__s8(&randomize));
    kpb_random_from_buffer("\xff\xff\xff\xff\xff\xff\xff\xfd", 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_s8(&randomize));
    EXPECT_EQUAL(0x7ffffffffffffffe, kpb_test_randomize__s8(&randomize));
    EXPECT_TRUE(kpb_test_randomize__set_s8_min(&randomize, 0x7bcd2345abcd5678));
    EXPECT_TRUE(kpb_test_randomize__set_s8_max(&randomize, 0x7bcd2345abcd5678));
    kpb_random_from_buffer("\x12\x34\x56\x78\x12\x34\x56\x78", 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_s8(&randomize));
    EXPECT_EQUAL(0x7bcd2345abcd5678, kpb_test_randomize__s8(&randomize));

    EXPECT_TRUE(kpb_test_randomize__set_count_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_count_max(&randomize, 3));
    EXPECT_TRUE(kpb_test_randomize__set_s8_min(&randomize, ~((int64_t)0x7fffffffffffffff)));
    EXPECT_TRUE(kpb_test_randomize__set_s8_max(&randomize, 0x7fffffffffffffff));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00", 4 + (8 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_s8_list(&randomize));
    EXPECT_EQUAL(~((int64_t)0x7fffffffffffffff), kpb_test_randomize__s8_list_at(&randomize, 0));
    EXPECT_EQUAL(~((int64_t)0x7fffffffffffffff), kpb_test_randomize__s8_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__s8_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff\xff\xff\xff\xff\xff\xff" "\xff\xff\xff\xff\xff\xff\xff\xff" "\xff\xff\xff\xff\xff\xff\xff\xff", 4 + (8 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_s8_list(&randomize));
    EXPECT_EQUAL(0x7fffffffffffffff, kpb_test_randomize__s8_list_at(&randomize, 0));
    EXPECT_EQUAL(0x7fffffffffffffff, kpb_test_randomize__s8_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__s8_list_at(&randomize, 2));
    EXPECT_TRUE(kpb_test_randomize__set_s8_min(&randomize, ~((int64_t)0x7ffffffffffffffe)));
    EXPECT_TRUE(kpb_test_randomize__set_s8_max(&randomize, 0x7ffffffffffffffe));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00", 4 + (8 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_s8_list(&randomize));
    EXPECT_EQUAL(~((int64_t)0x7ffffffffffffffe), kpb_test_randomize__s8_list_at(&randomize, 0));
    EXPECT_EQUAL(~((int64_t)0x7ffffffffffffffe), kpb_test_randomize__s8_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__s8_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff\xff\xff\xff\xff\xff\xfd" "\xff\xff\xff\xff\xff\xff\xff\xfd" "\xff\xff\xff\xff\xff\xff\xff\xfd", 4 + (8 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_s8_list(&randomize));
    EXPECT_EQUAL(0x7ffffffffffffffe, kpb_test_randomize__s8_list_at(&randomize, 0));
    EXPECT_EQUAL(0x7ffffffffffffffe, kpb_test_randomize__s8_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__s8_list_at(&randomize, 2));
    EXPECT_TRUE(kpb_test_randomize__set_s8_min(&randomize, 0x7bcd2345abcd5678));
    EXPECT_TRUE(kpb_test_randomize__set_s8_max(&randomize, 0x7bcd2345abcd5678));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x12\x34\x12\x34\x12\x34\x12\x34" "\x56\x78\x56\x78\x56\x78\x56\x78" "\x90\xab\x90\xab\x90\xab\x90\xab", 4 + (8 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_s8_list(&randomize));
    EXPECT_EQUAL(0x7bcd2345abcd5678, kpb_test_randomize__s8_list_at(&randomize, 0));
    EXPECT_EQUAL(0x7bcd2345abcd5678, kpb_test_randomize__s8_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__s8_list_at(&randomize, 2));

    EXPECT_TRUE(kpb_test_randomize__set_u1_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_u1_max(&randomize, 0xff));
    kpb_random_from_buffer("\x00", 1);
    EXPECT_TRUE(kpb_test_randomize__randomize_u1(&randomize));
    EXPECT_EQUAL(0, kpb_test_randomize__u1(&randomize));
    kpb_random_from_buffer("\xff", 1);
    EXPECT_TRUE(kpb_test_randomize__randomize_u1(&randomize));
    EXPECT_EQUAL(0xff, kpb_test_randomize__u1(&randomize));
    EXPECT_TRUE(kpb_test_randomize__set_u1_min(&randomize, 1));
    EXPECT_TRUE(kpb_test_randomize__set_u1_max(&randomize, 0xfe));
    kpb_random_from_buffer("\x00", 1);
    EXPECT_TRUE(kpb_test_randomize__randomize_u1(&randomize));
    EXPECT_EQUAL(1, kpb_test_randomize__u1(&randomize));
    kpb_random_from_buffer("\xfd", 1);
    EXPECT_TRUE(kpb_test_randomize__randomize_u1(&randomize));
    EXPECT_EQUAL(0xfe, kpb_test_randomize__u1(&randomize));
    EXPECT_TRUE(kpb_test_randomize__set_u1_min(&randomize, 0x42));
    EXPECT_TRUE(kpb_test_randomize__set_u1_max(&randomize, 0x42));
    kpb_random_from_buffer("\x12", 1);
    EXPECT_TRUE(kpb_test_randomize__randomize_u1(&randomize));
    EXPECT_EQUAL(0x42, kpb_test_randomize__u1(&randomize));

    EXPECT_TRUE(kpb_test_randomize__set_count_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_count_max(&randomize, 3));
    EXPECT_TRUE(kpb_test_randomize__set_u1_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_u1_max(&randomize, 0xff));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00" "\x00" "\x00", 4 + (1 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_u1_list(&randomize));
    EXPECT_EQUAL(0, kpb_test_randomize__u1_list_at(&randomize, 0));
    EXPECT_EQUAL(0, kpb_test_randomize__u1_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__u1_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff" "\xff" "\xff", 4 + (1 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_u1_list(&randomize));
    EXPECT_EQUAL(0xff, kpb_test_randomize__u1_list_at(&randomize, 0));
    EXPECT_EQUAL(0xff, kpb_test_randomize__u1_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__u1_list_at(&randomize, 2));
    EXPECT_TRUE(kpb_test_randomize__set_u1_min(&randomize, 1));
    EXPECT_TRUE(kpb_test_randomize__set_u1_max(&randomize, 0xfe));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00" "\x00" "\x00", 4 + (1 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_u1_list(&randomize));
    EXPECT_EQUAL(1, kpb_test_randomize__u1_list_at(&randomize, 0));
    EXPECT_EQUAL(1, kpb_test_randomize__u1_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__u1_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xfd" "\xfd" "\xfd", 4 + (1 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_u1_list(&randomize));
    EXPECT_EQUAL(0xfe, kpb_test_randomize__u1_list_at(&randomize, 0));
    EXPECT_EQUAL(0xfe, kpb_test_randomize__u1_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__u1_list_at(&randomize, 2));
    EXPECT_TRUE(kpb_test_randomize__set_u1_min(&randomize, 0x42));
    EXPECT_TRUE(kpb_test_randomize__set_u1_max(&randomize, 0x42));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x12" "\x34" "\x56", 4 + (1 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_u1_list(&randomize));
    EXPECT_EQUAL(0x42, kpb_test_randomize__u1_list_at(&randomize, 0));
    EXPECT_EQUAL(0x42, kpb_test_randomize__u1_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__u1_list_at(&randomize, 2));

    EXPECT_TRUE(kpb_test_randomize__set_u2_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_u2_max(&randomize, 0xffff));
    kpb_random_from_buffer("\x00\x00", 2);
    EXPECT_TRUE(kpb_test_randomize__randomize_u2(&randomize));
    EXPECT_EQUAL(0, kpb_test_randomize__u2(&randomize));
    kpb_random_from_buffer("\xff\xff", 2);
    EXPECT_TRUE(kpb_test_randomize__randomize_u2(&randomize));
    EXPECT_EQUAL(0xffff, kpb_test_randomize__u2(&randomize));
    EXPECT_TRUE(kpb_test_randomize__set_u2_min(&randomize, 1));
    EXPECT_TRUE(kpb_test_randomize__set_u2_max(&randomize, 0xfffe));
    kpb_random_from_buffer("\x00\x00", 2);
    EXPECT_TRUE(kpb_test_randomize__randomize_u2(&randomize));
    EXPECT_EQUAL(1, kpb_test_randomize__u2(&randomize));
    kpb_random_from_buffer("\xff\xfd", 2);
    EXPECT_TRUE(kpb_test_randomize__randomize_u2(&randomize));
    EXPECT_EQUAL(0xfffe, kpb_test_randomize__u2(&randomize));
    EXPECT_TRUE(kpb_test_randomize__set_u2_min(&randomize, 0x8442));
    EXPECT_TRUE(kpb_test_randomize__set_u2_max(&randomize, 0x8442));
    kpb_random_from_buffer("\x12\x34", 2);
    EXPECT_TRUE(kpb_test_randomize__randomize_u2(&randomize));
    EXPECT_EQUAL(0x8442, kpb_test_randomize__u2(&randomize));

    EXPECT_TRUE(kpb_test_randomize__set_count_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_count_max(&randomize, 3));
    EXPECT_TRUE(kpb_test_randomize__set_u2_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_u2_max(&randomize, 0xffff));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00" "\x00\x00" "\x00\x00", 4 + (2 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_u2_list(&randomize));
    EXPECT_EQUAL(0, kpb_test_randomize__u2_list_at(&randomize, 0));
    EXPECT_EQUAL(0, kpb_test_randomize__u2_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__u2_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff" "\xff\xff" "\xff\xff", 4 + (2 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_u2_list(&randomize));
    EXPECT_EQUAL(0xffff, kpb_test_randomize__u2_list_at(&randomize, 0));
    EXPECT_EQUAL(0xffff, kpb_test_randomize__u2_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__u2_list_at(&randomize, 2));
    EXPECT_TRUE(kpb_test_randomize__set_u2_min(&randomize, 1));
    EXPECT_TRUE(kpb_test_randomize__set_u2_max(&randomize, 0xfffe));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00" "\x00\x00" "\x00\x00", 4 + (2 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_u2_list(&randomize));
    EXPECT_EQUAL(1, kpb_test_randomize__u2_list_at(&randomize, 0));
    EXPECT_EQUAL(1, kpb_test_randomize__u2_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__u2_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xfd" "\xff\xfd" "\xff\xfd", 4 + (2 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_u2_list(&randomize));
    EXPECT_EQUAL(0xfffe, kpb_test_randomize__u2_list_at(&randomize, 0));
    EXPECT_EQUAL(0xfffe, kpb_test_randomize__u2_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__u2_list_at(&randomize, 2));
    EXPECT_TRUE(kpb_test_randomize__set_u2_min(&randomize, 0x8442));
    EXPECT_TRUE(kpb_test_randomize__set_u2_max(&randomize, 0x8442));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x12\x34" "\x34\x56" "\x56\x78", 4 + (2 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_u2_list(&randomize));
    EXPECT_EQUAL(0x8442, kpb_test_randomize__u2_list_at(&randomize, 0));
    EXPECT_EQUAL(0x8442, kpb_test_randomize__u2_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__u2_list_at(&randomize, 2));

    EXPECT_TRUE(kpb_test_randomize__set_u4_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_u4_max(&randomize, 0xffffffff));
    kpb_random_from_buffer("\x00\x00\x00\x00", 4);
    EXPECT_TRUE(kpb_test_randomize__randomize_u4(&randomize));
    EXPECT_EQUAL(0, kpb_test_randomize__u4(&randomize));
    kpb_random_from_buffer("\xff\xff\xff\xff", 4);
    EXPECT_TRUE(kpb_test_randomize__randomize_u4(&randomize));
    EXPECT_EQUAL(0xffffffff, kpb_test_randomize__u4(&randomize));
    EXPECT_TRUE(kpb_test_randomize__set_u4_min(&randomize, 1));
    EXPECT_TRUE(kpb_test_randomize__set_u4_max(&randomize, 0xfffffffe));
    kpb_random_from_buffer("\x00\x00\x00\x00", 4);
    EXPECT_TRUE(kpb_test_randomize__randomize_u4(&randomize));
    EXPECT_EQUAL(1, kpb_test_randomize__u4(&randomize));
    kpb_random_from_buffer("\xff\xff\xff\xfd", 4);
    EXPECT_TRUE(kpb_test_randomize__randomize_u4(&randomize));
    EXPECT_EQUAL(0xfffffffe, kpb_test_randomize__u4(&randomize));
    EXPECT_TRUE(kpb_test_randomize__set_u4_min(&randomize, 0x8442a1b2));
    EXPECT_TRUE(kpb_test_randomize__set_u4_max(&randomize, 0x8442a1b2));
    kpb_random_from_buffer("\x12\x34\x56\x78", 4);
    EXPECT_TRUE(kpb_test_randomize__randomize_u4(&randomize));
    EXPECT_EQUAL(0x8442a1b2, kpb_test_randomize__u4(&randomize));

    EXPECT_TRUE(kpb_test_randomize__set_count_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_count_max(&randomize, 3));
    EXPECT_TRUE(kpb_test_randomize__set_u4_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_u4_max(&randomize, 0xffffffff));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "\x00\x00\x00\x00" "\x00\x00\x00\x00", 4 + (4 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_u4_list(&randomize));
    EXPECT_EQUAL(0, kpb_test_randomize__u4_list_at(&randomize, 0));
    EXPECT_EQUAL(0, kpb_test_randomize__u4_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__u4_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff\xff\xff" "\xff\xff\xff\xff" "\xff\xff\xff\xff", 4 + (4 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_u4_list(&randomize));
    EXPECT_EQUAL(0xffffffff, kpb_test_randomize__u4_list_at(&randomize, 0));
    EXPECT_EQUAL(0xffffffff, kpb_test_randomize__u4_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__u4_list_at(&randomize, 2));
    EXPECT_TRUE(kpb_test_randomize__set_u4_min(&randomize, 1));
    EXPECT_TRUE(kpb_test_randomize__set_u4_max(&randomize, 0xfffffffe));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "\x00\x00\x00\x00" "\x00\x00\x00\x00", 4 + (4 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_u4_list(&randomize));
    EXPECT_EQUAL(1, kpb_test_randomize__u4_list_at(&randomize, 0));
    EXPECT_EQUAL(1, kpb_test_randomize__u4_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__u4_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff\xff\xfd" "\xff\xff\xff\xfd" "\xff\xff\xff\xfd", 4 + (4 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_u4_list(&randomize));
    EXPECT_EQUAL(0xfffffffe, kpb_test_randomize__u4_list_at(&randomize, 0));
    EXPECT_EQUAL(0xfffffffe, kpb_test_randomize__u4_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__u4_list_at(&randomize, 2));
    EXPECT_TRUE(kpb_test_randomize__set_u4_min(&randomize, 0x8442a1b2));
    EXPECT_TRUE(kpb_test_randomize__set_u4_max(&randomize, 0x8442a1b2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x12\x34\x56\x78" "\x34\x56\x78\x90" "\x56\x78\xab\xcd", 4 + (4 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_u4_list(&randomize));
    EXPECT_EQUAL(0x8442a1b2, kpb_test_randomize__u4_list_at(&randomize, 0));
    EXPECT_EQUAL(0x8442a1b2, kpb_test_randomize__u4_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__u4_list_at(&randomize, 2));

    EXPECT_TRUE(kpb_test_randomize__set_u8_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_u8_max(&randomize, 0xffffffffffffffff));
    kpb_random_from_buffer("\x00\x00\x00\x00\x00\x00\x00\x00", 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_u8(&randomize));
    EXPECT_EQUAL(0, kpb_test_randomize__u8(&randomize));
    kpb_random_from_buffer("\xff\xff\xff\xff\xff\xff\xff\xff", 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_u8(&randomize));
    EXPECT_EQUAL(0xffffffffffffffff, kpb_test_randomize__u8(&randomize));
    EXPECT_TRUE(kpb_test_randomize__set_u8_min(&randomize, 1));
    EXPECT_TRUE(kpb_test_randomize__set_u8_max(&randomize, 0xfffffffffffffffe));
    kpb_random_from_buffer("\x00\x00\x00\x00\x00\x00\x00\x00", 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_u8(&randomize));
    EXPECT_EQUAL(1, kpb_test_randomize__u8(&randomize));
    kpb_random_from_buffer("\xff\xff\xff\xff\xff\xff\xff\xfd", 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_u8(&randomize));
    EXPECT_EQUAL(0xfffffffffffffffe, kpb_test_randomize__u8(&randomize));
    EXPECT_TRUE(kpb_test_randomize__set_u8_min(&randomize, 0x8442a1b28442a1b2));
    EXPECT_TRUE(kpb_test_randomize__set_u8_max(&randomize, 0x8442a1b28442a1b2));
    kpb_random_from_buffer("\x12\x34\x56\x78\x12\x34\x56\x78", 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_u8(&randomize));
    EXPECT_EQUAL(0x8442a1b28442a1b2, kpb_test_randomize__u8(&randomize));

    EXPECT_TRUE(kpb_test_randomize__set_count_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_count_max(&randomize, 3));
    EXPECT_TRUE(kpb_test_randomize__set_u8_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_u8_max(&randomize, 0xffffffffffffffff));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00", 4 + (8 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_u8_list(&randomize));
    EXPECT_EQUAL(0, kpb_test_randomize__u8_list_at(&randomize, 0));
    EXPECT_EQUAL(0, kpb_test_randomize__u8_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__u8_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff\xff\xff\xff\xff\xff\xff" "\xff\xff\xff\xff\xff\xff\xff\xff" "\xff\xff\xff\xff\xff\xff\xff\xff", 4 + (8 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_u8_list(&randomize));
    EXPECT_EQUAL(0xffffffffffffffff, kpb_test_randomize__u8_list_at(&randomize, 0));
    EXPECT_EQUAL(0xffffffffffffffff, kpb_test_randomize__u8_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__u8_list_at(&randomize, 2));
    EXPECT_TRUE(kpb_test_randomize__set_u8_min(&randomize, 1));
    EXPECT_TRUE(kpb_test_randomize__set_u8_max(&randomize, 0xfffffffffffffffe));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00", 4 + (8 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_u8_list(&randomize));
    EXPECT_EQUAL(1, kpb_test_randomize__u8_list_at(&randomize, 0));
    EXPECT_EQUAL(1, kpb_test_randomize__u8_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__u8_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff\xff\xff\xff\xff\xff\xfd" "\xff\xff\xff\xff\xff\xff\xff\xfd" "\xff\xff\xff\xff\xff\xff\xff\xfd", 4 + (8 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_u8_list(&randomize));
    EXPECT_EQUAL(0xfffffffffffffffe, kpb_test_randomize__u8_list_at(&randomize, 0));
    EXPECT_EQUAL(0xfffffffffffffffe, kpb_test_randomize__u8_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__u8_list_at(&randomize, 2));
    EXPECT_TRUE(kpb_test_randomize__set_u8_min(&randomize, 0x8442a1b28442a1b2));
    EXPECT_TRUE(kpb_test_randomize__set_u8_max(&randomize, 0x8442a1b28442a1b2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x12\x34\x56\x78\x12\x34\x56\x78" "\x34\x56\x78\x90\x34\x56\x78\x90" "\x56\x78\xab\xcd\x56\x78\xab\xcd", 4 + (8 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_u8_list(&randomize));
    EXPECT_EQUAL(0x8442a1b28442a1b2, kpb_test_randomize__u8_list_at(&randomize, 0));
    EXPECT_EQUAL(0x8442a1b28442a1b2, kpb_test_randomize__u8_list_at(&randomize, 1));
    EXPECT_EQUAL(0, kpb_test_randomize__u8_list_at(&randomize, 2));

    EXPECT_TRUE(kpb_test_randomize__set_bool_min(&randomize, false));
    EXPECT_TRUE(kpb_test_randomize__set_bool_max(&randomize, false));
    kpb_random_from_buffer("\xff", 1);
    EXPECT_TRUE(kpb_test_randomize__randomize_bool_(&randomize));
    EXPECT_EQUAL(false, kpb_test_randomize__bool_(&randomize));
    kpb_random_from_buffer("\x00", 1);
    EXPECT_TRUE(kpb_test_randomize__randomize_bool_(&randomize));
    EXPECT_EQUAL(false, kpb_test_randomize__bool_(&randomize));
    EXPECT_TRUE(kpb_test_randomize__set_bool_min(&randomize, true));
    EXPECT_TRUE(kpb_test_randomize__set_bool_max(&randomize, true));
    kpb_random_from_buffer("\xff", 1);
    EXPECT_TRUE(kpb_test_randomize__randomize_bool_(&randomize));
    EXPECT_EQUAL(true, kpb_test_randomize__bool_(&randomize));
    kpb_random_from_buffer("\x00", 1);
    EXPECT_TRUE(kpb_test_randomize__randomize_bool_(&randomize));
    EXPECT_EQUAL(true, kpb_test_randomize__bool_(&randomize));
    EXPECT_TRUE(kpb_test_randomize__set_bool_min(&randomize, false));
    EXPECT_TRUE(kpb_test_randomize__set_bool_max(&randomize, true));
    kpb_random_from_buffer("\xff", 1);
    EXPECT_TRUE(kpb_test_randomize__randomize_bool_(&randomize));
    EXPECT_EQUAL(true, kpb_test_randomize__bool_(&randomize));
    kpb_random_from_buffer("\x00", 1);
    EXPECT_TRUE(kpb_test_randomize__randomize_bool_(&randomize));
    EXPECT_EQUAL(false, kpb_test_randomize__bool_(&randomize));

    kpb_random_from_buffer("\x00\x00\x00\x00\x00\x00\x00\x00", 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_f4(&randomize));
    EXPECT_APPROX_EQUAL(-11.1, kpb_test_randomize__f4(&randomize), 0.001);
    kpb_random_from_buffer("\xff\xff\xff\xff\xff\xff\xff\xff", 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_f4(&randomize));
    EXPECT_APPROX_EQUAL(22.3, kpb_test_randomize__f4(&randomize), 0.001);
    kpb_random_from_buffer("\x7f\xff\xff\xff\xff\xff\xff\xff", 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_f4(&randomize));
    EXPECT_APPROX_EQUAL(5.6, kpb_test_randomize__f4(&randomize), 0.001);
    kpb_random_from_buffer("\x12\x34\x56\x78\x12\x34\x56\x78", 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_f4(&randomize));
    EXPECT_APPROX_EQUAL(-8.724890, kpb_test_randomize__f4(&randomize), 0.001);

    EXPECT_TRUE(kpb_test_randomize__set_count_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_count_max(&randomize, 3));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00", 4 + (8 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_f4_list(&randomize));
    EXPECT_APPROX_EQUAL(-11.1, kpb_test_randomize__f4_list_at(&randomize, 0), 0.001);
    EXPECT_APPROX_EQUAL(-11.1, kpb_test_randomize__f4_list_at(&randomize, 1), 0.001);
    EXPECT_APPROX_EQUAL(0.0, kpb_test_randomize__f4_list_at(&randomize, 2), 0.001);
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff\xff\xff\xff\xff\xff\xff" "\xff\xff\xff\xff\xff\xff\xff\xff" "\xff\xff\xff\xff\xff\xff\xff\xff", 4 + (8 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_f4_list(&randomize));
    EXPECT_APPROX_EQUAL(22.3, kpb_test_randomize__f4_list_at(&randomize, 0), 0.001);
    EXPECT_APPROX_EQUAL(22.3, kpb_test_randomize__f4_list_at(&randomize, 1), 0.001);
    EXPECT_APPROX_EQUAL(0.0, kpb_test_randomize__f4_list_at(&randomize, 2), 0.001);
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x7f\xff\xff\xff\xff\xff\xff\xff" "\x7f\xff\xff\xff\xff\xff\xff\xff" "\x7f\xff\xff\xff\xff\xff\xff\xff", 4 + (8 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_f4_list(&randomize));
    EXPECT_APPROX_EQUAL(5.6, kpb_test_randomize__f4_list_at(&randomize, 0), 0.001);
    EXPECT_APPROX_EQUAL(5.6, kpb_test_randomize__f4_list_at(&randomize, 1), 0.001);
    EXPECT_APPROX_EQUAL(0.0, kpb_test_randomize__f4_list_at(&randomize, 2), 0.001);
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x12\x34\x12\x34\x12\x34\x12\x34" "\x56\x78\x56\x78\x56\x78\x56\x78" "\x90\xab\x90\xab\x90\xab\x90\xab", 4 + (8 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_f4_list(&randomize));
    EXPECT_APPROX_EQUAL(-8.725025, kpb_test_randomize__f4_list_at(&randomize, 0), 0.001);
    EXPECT_APPROX_EQUAL(0.181642, kpb_test_randomize__f4_list_at(&randomize, 1), 0.001);
    EXPECT_APPROX_EQUAL(0.0, kpb_test_randomize__f4_list_at(&randomize, 2), 0.001);

    kpb_random_from_buffer("\x00\x00\x00\x00\x00\x00\x00\x00", 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_f8(&randomize));
    EXPECT_APPROX_EQUAL(-11.1, kpb_test_randomize__f8(&randomize), 0.001);
    kpb_random_from_buffer("\xff\xff\xff\xff\xff\xff\xff\xff", 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_f8(&randomize));
    EXPECT_APPROX_EQUAL(22.3, kpb_test_randomize__f8(&randomize), 0.001);
    kpb_random_from_buffer("\x7f\xff\xff\xff\xff\xff\xff\xff", 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_f8(&randomize));
    EXPECT_APPROX_EQUAL(5.6, kpb_test_randomize__f8(&randomize), 0.001);
    kpb_random_from_buffer("\x12\x34\x56\x78\x12\x34\x56\x78", 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_f8(&randomize));
    EXPECT_APPROX_EQUAL(-8.724890, kpb_test_randomize__f8(&randomize), 0.001);

    EXPECT_TRUE(kpb_test_randomize__set_count_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_count_max(&randomize, 3));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00", 4 + (8 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_f8_list(&randomize));
    EXPECT_APPROX_EQUAL(-11.1, kpb_test_randomize__f8_list_at(&randomize, 0), 0.001);
    EXPECT_APPROX_EQUAL(-11.1, kpb_test_randomize__f8_list_at(&randomize, 1), 0.001);
    EXPECT_APPROX_EQUAL(0.0, kpb_test_randomize__f8_list_at(&randomize, 2), 0.001);
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff\xff\xff\xff\xff\xff\xff" "\xff\xff\xff\xff\xff\xff\xff\xff" "\xff\xff\xff\xff\xff\xff\xff\xff", 4 + (8 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_f8_list(&randomize));
    EXPECT_APPROX_EQUAL(22.3, kpb_test_randomize__f8_list_at(&randomize, 0), 0.001);
    EXPECT_APPROX_EQUAL(22.3, kpb_test_randomize__f8_list_at(&randomize, 1), 0.001);
    EXPECT_APPROX_EQUAL(0.0, kpb_test_randomize__f8_list_at(&randomize, 2), 0.001);
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x7f\xff\xff\xff\xff\xff\xff\xff" "\x7f\xff\xff\xff\xff\xff\xff\xff" "\x7f\xff\xff\xff\xff\xff\xff\xff", 4 + (8 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_f8_list(&randomize));
    EXPECT_APPROX_EQUAL(5.6, kpb_test_randomize__f8_list_at(&randomize, 0), 0.001);
    EXPECT_APPROX_EQUAL(5.6, kpb_test_randomize__f8_list_at(&randomize, 1), 0.001);
    EXPECT_APPROX_EQUAL(0.0, kpb_test_randomize__f8_list_at(&randomize, 2), 0.001);
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x12\x34\x12\x34\x12\x34\x12\x34" "\x56\x78\x56\x78\x56\x78\x56\x78" "\x90\xab\x90\xab\x90\xab\x90\xab", 4 + (8 * 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_f8_list(&randomize));
    EXPECT_APPROX_EQUAL(-8.725025, kpb_test_randomize__f8_list_at(&randomize, 0), 0.001);
    EXPECT_APPROX_EQUAL(0.181642, kpb_test_randomize__f8_list_at(&randomize, 1), 0.001);
    EXPECT_APPROX_EQUAL(0.0, kpb_test_randomize__f8_list_at(&randomize, 2), 0.001);

    kpb_random_from_buffer("\x00\x00\x00\x00" "", 4 + 0);
    EXPECT_TRUE(kpb_test_randomize__randomize_data(&randomize));
    EXPECT_BYTES_EQUAL("", 0, kpb_test_randomize__data(&randomize), kpb_test_randomize__data_size(&randomize));
    kpb_random_from_buffer("\x00\x00\x00\x04" "\x00\x00\x00\x00", 4 + 4);
    EXPECT_TRUE(kpb_test_randomize__randomize_data(&randomize));
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, kpb_test_randomize__data(&randomize), kpb_test_randomize__data_size(&randomize));
    kpb_random_from_buffer("\x00\x00\x00\x04" "\xff\xff\xff\xff", 4 + 4);
    EXPECT_TRUE(kpb_test_randomize__randomize_data(&randomize));
    EXPECT_BYTES_EQUAL("\xff\xff\xff\xff", 4, kpb_test_randomize__data(&randomize), kpb_test_randomize__data_size(&randomize));
    kpb_random_from_buffer("\x00\x00\x00\x04" "\x12\x34\x56\x78", 4 + 4);
    EXPECT_TRUE(kpb_test_randomize__randomize_data(&randomize));
    EXPECT_BYTES_EQUAL("\x12\x34\x56\x78", 4, kpb_test_randomize__data(&randomize), kpb_test_randomize__data_size(&randomize));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xcc\xdd", 4 + 2);
    EXPECT_TRUE(kpb_test_randomize__randomize_data(&randomize));
    EXPECT_BYTES_EQUAL("\xcc\xdd", 2, kpb_test_randomize__data(&randomize), kpb_test_randomize__data_size(&randomize));

    EXPECT_TRUE(kpb_test_randomize__set_count_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_count_max(&randomize, 3));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "" "\x00\x00\x00\x04" "\x00\x00\x00\x00" "\x00\x00\x00\x03" "\x00\x00\x00", 4 + (4 + 0) + (4 + 4) + (4 + 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_data_list(&randomize));
    EXPECT_BYTES_EQUAL("", 0, kpb_test_randomize__data_list_at(&randomize, 0), kpb_test_randomize__data_list_size_at(&randomize, 0));
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, kpb_test_randomize__data_list_at(&randomize, 1), kpb_test_randomize__data_list_size_at(&randomize, 1));
    EXPECT_BYTES_EQUAL("", 0, kpb_test_randomize__data_list_at(&randomize, 2), kpb_test_randomize__data_list_size_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "" "\x00\x00\x00\x04" "\xff\xff\xff\xff" "\x00\x00\x00\x03" "\xff\xff\xff", 4 + (4 + 0) + (4 + 4) + (4 + 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_data_list(&randomize));
    EXPECT_BYTES_EQUAL("", 0, kpb_test_randomize__data_list_at(&randomize, 0), kpb_test_randomize__data_list_size_at(&randomize, 0));
    EXPECT_BYTES_EQUAL("\xff\xff\xff\xff", 4, kpb_test_randomize__data_list_at(&randomize, 1), kpb_test_randomize__data_list_size_at(&randomize, 1));
    EXPECT_BYTES_EQUAL("", 0, kpb_test_randomize__data_list_at(&randomize, 2), kpb_test_randomize__data_list_size_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x04" "\x12\x34\x12\x34" "\x00\x00\x00\x04" "\x56\x78\x56\x78" "\x00\x00\x00\x04" "\x90\xab\x90\xab", 4 + (4 + 4) + (4 + 4) + (4 + 4));
    EXPECT_TRUE(kpb_test_randomize__randomize_data_list(&randomize));
    EXPECT_BYTES_EQUAL("\x12\x34\x12\x34", 4, kpb_test_randomize__data_list_at(&randomize, 0), kpb_test_randomize__data_list_size_at(&randomize, 0));
    EXPECT_BYTES_EQUAL("\x56\x78\x56\x78", 4, kpb_test_randomize__data_list_at(&randomize, 1), kpb_test_randomize__data_list_size_at(&randomize, 1));
    EXPECT_BYTES_EQUAL("", 0, kpb_test_randomize__data_list_at(&randomize, 2), kpb_test_randomize__data_list_size_at(&randomize, 2));

    kpb_random_from_buffer("\x00\x00\x00\x00" "", 4 + 0);
    EXPECT_TRUE(kpb_test_randomize__randomize_ascii(&randomize));
    EXPECT_STRING_EQUAL("", kpb_test_randomize__ascii(&randomize));
    kpb_random_from_buffer("\x00\x00\x00\x04" "\x41\x41\x00\x41", 4 + 4);
    EXPECT_TRUE(kpb_test_randomize__randomize_ascii(&randomize));
    EXPECT_STRING_EQUAL("AA", kpb_test_randomize__ascii(&randomize));
    kpb_random_from_buffer("\x00\x00\x00\x04" "\x62\x62\x62\x62", 4 + 4);
    EXPECT_TRUE(kpb_test_randomize__randomize_ascii(&randomize));
    EXPECT_STRING_EQUAL("bbbb", kpb_test_randomize__ascii(&randomize));
    kpb_random_from_buffer("\x00\x00\x00\x04" "\x41\x42\x43\x44", 4 + 4);
    EXPECT_TRUE(kpb_test_randomize__randomize_ascii(&randomize));
    EXPECT_STRING_EQUAL("ABCD", kpb_test_randomize__ascii(&randomize));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x61\x62", 4 + 2);
    EXPECT_TRUE(kpb_test_randomize__randomize_ascii(&randomize));
    EXPECT_STRING_EQUAL("ab", kpb_test_randomize__ascii(&randomize));

    EXPECT_TRUE(kpb_test_randomize__set_count_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_count_max(&randomize, 3));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "" "\x00\x00\x00\x04" "\x41\x41\x00\x41" "\x00\x00\x00\x03" "\x42\x42\x42", 4 + (4 + 0) + (4 + 4) + (4 + 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_ascii_list(&randomize));
    EXPECT_STRING_EQUAL("", kpb_test_randomize__ascii_list_at(&randomize, 0));
    EXPECT_STRING_EQUAL("AA", kpb_test_randomize__ascii_list_at(&randomize, 1));
    EXPECT_STRING_EQUAL("", kpb_test_randomize__ascii_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "" "\x00\x00\x00\x04" "\x62\x62\x62\x62" "\x00\x00\x00\x03" "\x63\x63\x63", 4 + (4 + 0) + (4 + 4) + (4 + 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_ascii_list(&randomize));
    EXPECT_STRING_EQUAL("", kpb_test_randomize__ascii_list_at(&randomize, 0));
    EXPECT_STRING_EQUAL("bbbb", kpb_test_randomize__ascii_list_at(&randomize, 1));
    EXPECT_STRING_EQUAL("", kpb_test_randomize__ascii_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x04" "\x41\x42\x43\x44" "\x00\x00\x00\x04" "\x61\x62\x63\x64" "\x00\x00\x00\x04" "\x41\x42\x61\x62", 4 + (4 + 4) + (4 + 4) + (4 + 4));
    EXPECT_TRUE(kpb_test_randomize__randomize_ascii_list(&randomize));
    EXPECT_STRING_EQUAL("ABCD", kpb_test_randomize__ascii_list_at(&randomize, 0));
    EXPECT_STRING_EQUAL("abcd", kpb_test_randomize__ascii_list_at(&randomize, 1));
    EXPECT_STRING_EQUAL("", kpb_test_randomize__ascii_list_at(&randomize, 2));

    kpb_random_from_buffer("\x00\x00\x00\x00" "", 4 + 0);
    EXPECT_TRUE(kpb_test_randomize__randomize_utf16le(&randomize));
    EXPECT_STRING_EQUAL(L"", kpb_test_randomize__utf16le(&randomize));
    kpb_random_from_buffer("\x00\x00\x00\x08" "\x00\x41\x00\x41\x00\x00\x00\x41", 4 + 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_utf16le(&randomize));
    EXPECT_STRING_EQUAL(L"AA", kpb_test_randomize__utf16le(&randomize));
    kpb_random_from_buffer("\x00\x00\x00\x08" "\x00\x62\x00\x62\x00\x62\x00\x62", 4 + 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_utf16le(&randomize));
    EXPECT_STRING_EQUAL(L"bbbb", kpb_test_randomize__utf16le(&randomize));
    kpb_random_from_buffer("\x00\x00\x00\x08" "\x00\x41\x00\x42\x00\x43\x00\x44", 4 + 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_utf16le(&randomize));
    EXPECT_STRING_EQUAL(L"ABCD", kpb_test_randomize__utf16le(&randomize));
    kpb_random_from_buffer("\x00\x00\x00\x04" "\x00\x61\x00\x62", 4 + 4);
    EXPECT_TRUE(kpb_test_randomize__randomize_utf16le(&randomize));
    EXPECT_STRING_EQUAL(L"ab", kpb_test_randomize__utf16le(&randomize));

    EXPECT_TRUE(kpb_test_randomize__set_count_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_count_max(&randomize, 3));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "" "\x00\x00\x00\x08" "\x00\x41\x00\x41\x00\x00\x00\x41" "\x00\x00\x00\x06" "\x00\x42\x00\x42\x00\x42", 4 + (4 + 0) + (4 + 8) + (4 + 6));
    EXPECT_TRUE(kpb_test_randomize__randomize_utf16le_list(&randomize));
    EXPECT_STRING_EQUAL(L"", kpb_test_randomize__utf16le_list_at(&randomize, 0));
    EXPECT_STRING_EQUAL(L"AA", kpb_test_randomize__utf16le_list_at(&randomize, 1));
    EXPECT_STRING_EQUAL(L"", kpb_test_randomize__utf16le_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "" "\x00\x00\x00\x08" "\x00\x62\x00\x62\x00\x62\x00\x62" "\x00\x00\x00\x06" "\x00\x63\x00\x63\x00\x63", 4 + (4 + 0) + (4 + 8) + (4 + 6));
    EXPECT_TRUE(kpb_test_randomize__randomize_utf16le_list(&randomize));
    EXPECT_STRING_EQUAL(L"", kpb_test_randomize__utf16le_list_at(&randomize, 0));
    EXPECT_STRING_EQUAL(L"bbbb", kpb_test_randomize__utf16le_list_at(&randomize, 1));
    EXPECT_STRING_EQUAL(L"", kpb_test_randomize__utf16le_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x08" "\x00\x41\x00\x42\x00\x43\x00\x44" "\x00\x00\x00\x08" "\x00\x61\x00\x62\x00\x63\x00\x64" "\x00\x00\x00\x08" "\x00\x41\x00\x42\x00\x61\x00\x62", 4 + (4 + 8) + (4 + 8) + (4 + 8));
    EXPECT_TRUE(kpb_test_randomize__randomize_utf16le_list(&randomize));
    EXPECT_STRING_EQUAL(L"ABCD", kpb_test_randomize__utf16le_list_at(&randomize, 0));
    EXPECT_STRING_EQUAL(L"abcd", kpb_test_randomize__utf16le_list_at(&randomize, 1));
    EXPECT_STRING_EQUAL(L"", kpb_test_randomize__utf16le_list_at(&randomize, 2));

    kpb_random_from_buffer("\x00\x00\x00\x00" "", 4 + 0);
    EXPECT_TRUE(kpb_test_randomize__randomize_utf16be(&randomize));
    EXPECT_STRING_EQUAL(L"", kpb_test_randomize__utf16be(&randomize));
    kpb_random_from_buffer("\x00\x00\x00\x08" "\x00\x41\x00\x41\x00\x00\x00\x41", 4 + 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_utf16be(&randomize));
    EXPECT_STRING_EQUAL(L"AA", kpb_test_randomize__utf16be(&randomize));
    kpb_random_from_buffer("\x00\x00\x00\x08" "\x00\x62\x00\x62\x00\x62\x00\x62", 4 + 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_utf16be(&randomize));
    EXPECT_STRING_EQUAL(L"bbbb", kpb_test_randomize__utf16be(&randomize));
    kpb_random_from_buffer("\x00\x00\x00\x08" "\x00\x41\x00\x42\x00\x43\x00\x44", 4 + 8);
    EXPECT_TRUE(kpb_test_randomize__randomize_utf16be(&randomize));
    EXPECT_STRING_EQUAL(L"ABCD", kpb_test_randomize__utf16be(&randomize));
    kpb_random_from_buffer("\x00\x00\x00\x04" "\x00\x61\x00\x62", 4 + 4);
    EXPECT_TRUE(kpb_test_randomize__randomize_utf16be(&randomize));
    EXPECT_STRING_EQUAL(L"ab", kpb_test_randomize__utf16be(&randomize));

    EXPECT_TRUE(kpb_test_randomize__set_count_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_count_max(&randomize, 3));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "" "\x00\x00\x00\x08" "\x00\x41\x00\x41\x00\x00\x00\x41" "\x00\x00\x00\x06" "\x00\x42\x00\x42\x00\x42", 4 + (4 + 0) + (4 + 8) + (4 + 6));
    EXPECT_TRUE(kpb_test_randomize__randomize_utf16be_list(&randomize));
    EXPECT_STRING_EQUAL(L"", kpb_test_randomize__utf16be_list_at(&randomize, 0));
    EXPECT_STRING_EQUAL(L"AA", kpb_test_randomize__utf16be_list_at(&randomize, 1));
    EXPECT_STRING_EQUAL(L"", kpb_test_randomize__utf16be_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "" "\x00\x00\x00\x08" "\x00\x62\x00\x62\x00\x62\x00\x62" "\x00\x00\x00\x06" "\x00\x63\x00\x63\x00\x63", 4 + (4 + 0) + (4 + 8) + (4 + 6));
    EXPECT_TRUE(kpb_test_randomize__randomize_utf16be_list(&randomize));
    EXPECT_STRING_EQUAL(L"", kpb_test_randomize__utf16be_list_at(&randomize, 0));
    EXPECT_STRING_EQUAL(L"bbbb", kpb_test_randomize__utf16be_list_at(&randomize, 1));
    EXPECT_STRING_EQUAL(L"", kpb_test_randomize__utf16be_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x08" "\x00\x41\x00\x42\x00\x43\x00\x44" "\x00\x00\x00\x08" "\x00\x61\x00\x62\x00\x63\x00\x64" "\x00\x00\x00\x08" "\x00\x41\x00\x42\x00\x61\x00\x62", 4 + (4 + 8) + (4 + 8) + (4 + 8));
    EXPECT_TRUE(kpb_test_randomize__randomize_utf16be_list(&randomize));
    EXPECT_STRING_EQUAL(L"ABCD", kpb_test_randomize__utf16be_list_at(&randomize, 0));
    EXPECT_STRING_EQUAL(L"abcd", kpb_test_randomize__utf16be_list_at(&randomize, 1));
    EXPECT_STRING_EQUAL(L"", kpb_test_randomize__utf16be_list_at(&randomize, 2));

    kpb_random_from_buffer("\x00\x00\x00\x00" "", 4 + 0);
    EXPECT_TRUE(kpb_test_randomize__randomize_utf8(&randomize));
    EXPECT_STRING_EQUAL("", kpb_test_randomize__utf8(&randomize));
    kpb_random_from_buffer("\x00\x00\x00\x04" "\x41\x41\x00\x41", 4 + 4);
    EXPECT_TRUE(kpb_test_randomize__randomize_utf8(&randomize));
    EXPECT_STRING_EQUAL("AA", kpb_test_randomize__utf8(&randomize));
    kpb_random_from_buffer("\x00\x00\x00\x04" "\x62\x62\x62\x62", 4 + 4);
    EXPECT_TRUE(kpb_test_randomize__randomize_utf8(&randomize));
    EXPECT_STRING_EQUAL("bbbb", kpb_test_randomize__utf8(&randomize));
    kpb_random_from_buffer("\x00\x00\x00\x04" "\x41\x42\x43\x44", 4 + 4);
    EXPECT_TRUE(kpb_test_randomize__randomize_utf8(&randomize));
    EXPECT_STRING_EQUAL("ABCD", kpb_test_randomize__utf8(&randomize));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x61\x62", 4 + 2);
    EXPECT_TRUE(kpb_test_randomize__randomize_utf8(&randomize));
    EXPECT_STRING_EQUAL("ab", kpb_test_randomize__utf8(&randomize));

    EXPECT_TRUE(kpb_test_randomize__set_count_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_count_max(&randomize, 3));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "" "\x00\x00\x00\x04" "\x41\x41\x00\x41" "\x00\x00\x00\x03" "\x42\x42\x42", 4 + (4 + 0) + (4 + 4) + (4 + 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_utf8_list(&randomize));
    EXPECT_STRING_EQUAL("", kpb_test_randomize__utf8_list_at(&randomize, 0));
    EXPECT_STRING_EQUAL("AA", kpb_test_randomize__utf8_list_at(&randomize, 1));
    EXPECT_STRING_EQUAL("", kpb_test_randomize__utf8_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "" "\x00\x00\x00\x04" "\x62\x62\x62\x62" "\x00\x00\x00\x03" "\x63\x63\x63", 4 + (4 + 0) + (4 + 4) + (4 + 3));
    EXPECT_TRUE(kpb_test_randomize__randomize_utf8_list(&randomize));
    EXPECT_STRING_EQUAL("", kpb_test_randomize__utf8_list_at(&randomize, 0));
    EXPECT_STRING_EQUAL("bbbb", kpb_test_randomize__utf8_list_at(&randomize, 1));
    EXPECT_STRING_EQUAL("", kpb_test_randomize__utf8_list_at(&randomize, 2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x04" "\x41\x42\x43\x44" "\x00\x00\x00\x04" "\x61\x62\x63\x64" "\x00\x00\x00\x04" "\x41\x42\x61\x62", 4 + (4 + 4) + (4 + 4) + (4 + 4));
    EXPECT_TRUE(kpb_test_randomize__randomize_utf8_list(&randomize));
    EXPECT_STRING_EQUAL("ABCD", kpb_test_randomize__utf8_list_at(&randomize, 0));
    EXPECT_STRING_EQUAL("abcd", kpb_test_randomize__utf8_list_at(&randomize, 1));
    EXPECT_STRING_EQUAL("", kpb_test_randomize__utf8_list_at(&randomize, 2));

    kpb_random_from_buffer("\x00\x00\x00\x03" "\x00\x00\x00\x02" "\xde\xaf", 4 + (4 + 2));
    EXPECT_TRUE(kpb_test_randomize__randomize_object(&randomize));
    const kpb_test_random_object_t *random_object = kpb_test_randomize__object(&randomize);
    EXPECT_BYTES_EQUAL("\xde\xaf\x00", 3, kpb_test_random_object_data(random_object), kpb_test_random_object_data_size(random_object));

    EXPECT_TRUE(kpb_test_randomize__set_count_min(&randomize, 0));
    EXPECT_TRUE(kpb_test_randomize__set_count_max(&randomize, 3));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x03" "\x00\x00\x00\x01" "\xde\xaf" "\x00\x00\x00\x03" "\x00\x00\x00\x02" "\xca\xbe\x12\x34", 4 + (4 + (4 + 2)) + (4 + (4 + 4)));
    EXPECT_TRUE(kpb_test_randomize__randomize_object_list(&randomize));
    random_object = kpb_test_randomize__object_list_at(&randomize, 0);
    EXPECT_EQUAL(2,  kpb_test_random_object_data_size_(random_object));
    EXPECT_BYTES_EQUAL("\xde\xaf", 2, kpb_test_random_object_data(random_object), kpb_test_random_object_data_size(random_object));
    random_object = kpb_test_randomize__object_list_at(&randomize, 1);
    EXPECT_EQUAL(3,  kpb_test_random_object_data_size_(random_object));
    EXPECT_BYTES_EQUAL("\xca\xbe\x12", 3, kpb_test_random_object_data(random_object), kpb_test_random_object_data_size(random_object));
    random_object = kpb_test_randomize__object_list_at(&randomize, 2);
    EXPECT_EQUAL(0,  kpb_test_random_object_data_size_(random_object));
    EXPECT_BYTES_EQUAL("", 0, kpb_test_random_object_data(random_object), kpb_test_random_object_data_size(random_object));

    kpb_random_os();
    kpb_test_randomize__clear(&randomize);
    EXPECT_TRUE(kpb_test_randomize__set_count_min(&randomize, 1));
    EXPECT_TRUE(kpb_test_randomize__set_count_max(&randomize, 3));
    EXPECT_TRUE(kpb_test_randomize__set_s1_min(&randomize, ~((int8_t)0x7e)));
    EXPECT_TRUE(kpb_test_randomize__set_s1_max(&randomize, 0x7e));
    EXPECT_TRUE(kpb_test_randomize__set_s2_min(&randomize, ~((int16_t)0x7ffe)));
    EXPECT_TRUE(kpb_test_randomize__set_s2_max(&randomize, 0x7ffe));
    EXPECT_TRUE(kpb_test_randomize__set_s4_min(&randomize, ~((int32_t)0x7ffffffe)));
    EXPECT_TRUE(kpb_test_randomize__set_s4_max(&randomize, 0x7ffffffe));
    EXPECT_TRUE(kpb_test_randomize__set_s8_min(&randomize, ~((int64_t)0x7ffffffffffffffe)));
    EXPECT_TRUE(kpb_test_randomize__set_s8_max(&randomize, 0x7ffffffffffffffe));
    EXPECT_TRUE(kpb_test_randomize__set_u1_min(&randomize, 1));
    EXPECT_TRUE(kpb_test_randomize__set_u1_max(&randomize, 0xfe));
    EXPECT_TRUE(kpb_test_randomize__set_u2_min(&randomize, 1));
    EXPECT_TRUE(kpb_test_randomize__set_u2_max(&randomize, 0xfffe));
    EXPECT_TRUE(kpb_test_randomize__set_u4_min(&randomize, 1));
    EXPECT_TRUE(kpb_test_randomize__set_u4_max(&randomize, 0xfffffffe));
    EXPECT_TRUE(kpb_test_randomize__set_u8_min(&randomize, 1));
    EXPECT_TRUE(kpb_test_randomize__set_u8_max(&randomize, 0xfffffffffffffffe));
    for ( size_t i = 0; i < 10; i++ )
    {
        // randomize
        EXPECT_TRUE(kpb_test_randomize__randomize(&randomize));

        // serialize
        result_size = 0;
        result = kpb_test_randomize__serialize_to_string(&randomize, &result_size);
        EXPECT_NOT_NULL(result);
        kpb_io_free(result);
    }

    kpb_test_randomize__fini(&randomize);

    return 0;
}
