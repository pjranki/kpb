#ifndef _H_PROCESS_VALUE_STRZ_H_
#define _H_PROCESS_VALUE_STRZ_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <string.h>

#include <kpb_io.h>

#include "c/process_value.h"

struct process_value_strz {
    uint8_t memset_value_;
};

void process_value_strz_init(struct process_value_strz *_this, uint8_t memset_value);
const char *process_value_strz_getter(struct process_value_strz *_this, const char *value);
char *process_value_strz_setter(struct process_value_strz *_this, char *value);
void process_value_strz_fini(struct process_value_strz *_this);

#ifdef __cplusplus
};
#endif

#endif
