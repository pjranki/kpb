#include "test_pos.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/pos.h"

int kpb_test_pos(void)
{
    size_t result_size = 0;
    char *result = NULL;

    // check default state (after constructor, after a clear)
    kpb_test_sub_pos_t sub_pos;
    kpb_test_sub_pos_init(&sub_pos);
    EXPECT_EQUAL(11, kpb_test_sub_pos_size(&sub_pos));
    kpb_test_sub_pos_clear(&sub_pos);
    EXPECT_EQUAL(11, kpb_test_sub_pos_size(&sub_pos));

    // test that the size includes position instances
    kpb_test_sub_pos_clear(&sub_pos);
    EXPECT_TRUE(kpb_test_sub_pos_set_value1(&sub_pos, 0xdeadbeef));
    EXPECT_TRUE(kpb_test_sub_pos_set_value2(&sub_pos, 0xcafebabe));
    EXPECT_EQUAL(11, kpb_test_sub_pos_size(&sub_pos));
    result = kpb_test_sub_pos_serialize_to_string(&sub_pos, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(
        "\xef\xbe\xad\xde\x00\x00\x00\xbe\xba\xfe\xca", 11,
        result, result_size);
    kpb_io_free(result);

    // test that reads can't work out of bounds
    kpb_test_sub_pos_clear(&sub_pos);
    EXPECT_FALSE(kpb_test_sub_pos_parse_from_string(&sub_pos, "\xef\xbe\xad\x12\x00\x00\x00\x34\xba", 9));

    // successful read
    kpb_test_sub_pos_clear(&sub_pos);
    EXPECT_TRUE(kpb_test_sub_pos_parse_from_string(&sub_pos, "\xef\xbe\xad\x12\x00\x00\x00\x34\xba\xfe\xca", 11));
    EXPECT_EQUAL(0x12adbeef, kpb_test_sub_pos_value1(&sub_pos));
    EXPECT_EQUAL(0xcafeba34, kpb_test_sub_pos_value2(&sub_pos));

    // check default state (after constructor, after a clear)
    kpb_test_pos_t obj;
    kpb_test_pos_init(&obj);
    EXPECT_EQUAL(19, kpb_test_pos_size(&obj));
    kpb_test_pos_clear(&obj);
    EXPECT_EQUAL(19, kpb_test_pos_size(&obj));

    // read and stop at error code
    kpb_test_pos_clear(&obj);
    EXPECT_TRUE(kpb_test_pos_parse_from_string(&obj, "\x01\x00\x00\xc0", 4));
    EXPECT_EQUAL(0xc0000001, kpb_test_pos_error_code(&obj));
    EXPECT_FALSE(kpb_test_pos_has_sub_pos_offset(&obj));
    EXPECT_FALSE(kpb_test_pos_has_sub_pos_size_(&obj));
    EXPECT_FALSE(kpb_test_pos_has_sub_pos(&obj));
    EXPECT_EQUAL(4, kpb_test_pos_size(&obj));

    // fail to read, not enough data
    kpb_test_pos_clear(&obj);
    EXPECT_FALSE(kpb_test_pos_parse_from_string(&obj, "\x09\x00\x00\x00\x0b\x00\x00\x00\x00\xef\xbe\xad\xde\x00\x00\x00\xbe\xba\xfe", 19));

    // read all
    kpb_test_pos_clear(&obj);
    EXPECT_TRUE(kpb_test_pos_parse_from_string(&obj, "\x09\x00\x00\x00\x0b\x00\x00\x00\x00\xef\xbe\xad\xde\x00\x00\x00\xbe\xba\xfe\xca", 20));
    EXPECT_EQUAL(0, kpb_test_pos_error_code(&obj));
    EXPECT_TRUE(kpb_test_pos_has_sub_pos_offset(&obj));
    EXPECT_TRUE(kpb_test_pos_has_sub_pos_size_(&obj));
    EXPECT_TRUE(kpb_test_pos_has_sub_pos(&obj));
    EXPECT_EQUAL(9, kpb_test_pos_sub_pos_offset(&obj));
    EXPECT_EQUAL(11, kpb_test_pos_sub_pos_size_(&obj));
    EXPECT_EQUAL(0xdeadbeef, kpb_test_sub_pos_value1(kpb_test_pos_sub_pos(&obj)));
    EXPECT_EQUAL(0xcafebabe, kpb_test_sub_pos_value2(kpb_test_pos_sub_pos(&obj)));
    EXPECT_EQUAL(20, kpb_test_pos_size(&obj));

    // write error
    kpb_test_pos_clear(&obj);
    EXPECT_TRUE(kpb_test_pos_set_first_u4(&obj, 0xc0001234));
    result = kpb_test_pos_serialize_to_string(&obj, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(
        "\x34\x12\x00\xc0", 4,
        result, result_size);
    kpb_io_free(result);

    // write all
    kpb_test_pos_clear(&obj);
    EXPECT_TRUE(kpb_test_pos_set_sub_pos_offset(&obj, 9));
    kpb_test_sub_pos_t *mut_sub_pos = kpb_test_pos_mutable_sub_pos(&obj);
    EXPECT_NOT_NULL(mut_sub_pos);
    kpb_test_sub_pos_set_value1(mut_sub_pos, 0xdeadbeef);
    kpb_test_sub_pos_set_value2(mut_sub_pos, 0xcafebabe);
    result = kpb_test_pos_serialize_to_string(&obj, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(
        "\x09\x00\x00\x00\x0b\x00\x00\x00\x00\xef\xbe\xad\xde\x00\x00\x00\xbe\xba\xfe\xca", 20,
        result, result_size);
    kpb_io_free(result);

    kpb_test_sub_pos_fini(&sub_pos);
    kpb_test_pos_fini(&obj);
    return 0;
}
