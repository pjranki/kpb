#include "test_bitfield.h"

#include <stdio.h>

#include "../common/macro.h"

#include "c/bitfield.h"

#define BYTES_ZERO_16 "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
#define BYTES_ZERO_16_SIZE (sizeof(BYTES_ZERO_16)-1)
#define BYTES_ONES_16 "\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xfe"
#define BYTES_ONES_16_SIZE (sizeof(BYTES_ONES_16)-1)

int kpb_test_bitfield_single(void)
{
    size_t result_size = 0;
    char *result = NULL;
    kpb_test_bitfield_t bitfield;
    kpb_test_bitfield_init(&bitfield);
    result_size = 0;
    result = kpb_test_bitfield_serialize_to_string(&bitfield, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(BYTES_ZERO_16, BYTES_ZERO_16_SIZE, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_bitfield_set_bitfield_b1(&bitfield, 0x1));
    EXPECT_TRUE(kpb_test_bitfield_set_bitfield_b2(&bitfield, 0x3));
    EXPECT_TRUE(kpb_test_bitfield_set_bitfield_b4(&bitfield, 0xf));
    EXPECT_TRUE(kpb_test_bitfield_set_bitfield_b8(&bitfield, 0xff));
    EXPECT_TRUE(kpb_test_bitfield_set_bitfield_b16(&bitfield, 0xffff));
    EXPECT_TRUE(kpb_test_bitfield_set_bitfield_b32(&bitfield, 0xffffffff));
    EXPECT_TRUE(kpb_test_bitfield_set_bitfield_b64(&bitfield, 0xffffffffffffffff));
    result_size = 0;
    result = kpb_test_bitfield_serialize_to_string(&bitfield, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(BYTES_ONES_16, BYTES_ONES_16_SIZE, result, result_size);
    kpb_io_free(result);
    kpb_test_bitfield_clear(&bitfield);
    result_size = 0;
    result = kpb_test_bitfield_serialize_to_string(&bitfield, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL(BYTES_ZERO_16, BYTES_ZERO_16_SIZE, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_bitfield_set_bitfield_b1(&bitfield, 0x1));
    EXPECT_TRUE(kpb_test_bitfield_set_bitfield_b2(&bitfield, 0x2));
    EXPECT_TRUE(kpb_test_bitfield_set_bitfield_b4(&bitfield, 0x5));
    EXPECT_TRUE(kpb_test_bitfield_set_bitfield_b8(&bitfield, 0xaa));
    EXPECT_TRUE(kpb_test_bitfield_set_bitfield_b16(&bitfield, 0x5555));
    EXPECT_TRUE(kpb_test_bitfield_set_bitfield_b32(&bitfield, 0xaaaaaaaa));
    EXPECT_TRUE(kpb_test_bitfield_set_bitfield_b64(&bitfield, 0x5555555555555555));
    result_size = 0;
    result = kpb_test_bitfield_serialize_to_string(&bitfield, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\xcb\x54\xaa\xab\x55\x55\x55\x54\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa", 16, result, result_size);
    kpb_io_free(result);
    EXPECT_TRUE(kpb_test_bitfield_set_bitfield_b1(&bitfield, 0x0));
    EXPECT_TRUE(kpb_test_bitfield_set_bitfield_b2(&bitfield, 0x1));
    EXPECT_TRUE(kpb_test_bitfield_set_bitfield_b4(&bitfield, 0xa));
    EXPECT_TRUE(kpb_test_bitfield_set_bitfield_b8(&bitfield, 0x55));
    EXPECT_TRUE(kpb_test_bitfield_set_bitfield_b16(&bitfield, 0xaaaa));
    EXPECT_TRUE(kpb_test_bitfield_set_bitfield_b32(&bitfield, 0x55555555));
    EXPECT_TRUE(kpb_test_bitfield_set_bitfield_b64(&bitfield, 0xaaaaaaaaaaaaaaaa));
    result_size = 0;
    result = kpb_test_bitfield_serialize_to_string(&bitfield, &result_size);
    EXPECT_NOT_NULL(result);
    EXPECT_BYTES_EQUAL("\x34\xab\x55\x54\xaa\xaa\xaa\xab\x55\x55\x55\x55\x55\x55\x55\x54", 16, result, result_size);
    kpb_io_free(result);

    kpb_test_bitfield_clear(&bitfield);
    EXPECT_EQUAL(kpb_test_bitfield_bitfield_b1(&bitfield), 0);
    EXPECT_EQUAL(kpb_test_bitfield_bitfield_b2(&bitfield), 0);
    EXPECT_EQUAL(kpb_test_bitfield_bitfield_b4(&bitfield), 0);
    EXPECT_EQUAL(kpb_test_bitfield_bitfield_b8(&bitfield), 0);
    EXPECT_EQUAL(kpb_test_bitfield_bitfield_b16(&bitfield), 0);
    EXPECT_EQUAL(kpb_test_bitfield_bitfield_b32(&bitfield), 0);
    EXPECT_EQUAL(kpb_test_bitfield_bitfield_b64(&bitfield), 0);
    EXPECT_TRUE(kpb_test_bitfield_parse_from_string(&bitfield, "\xcb\x54\xaa\xab\x55\x55\x55\x54\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa", 16));
    EXPECT_EQUAL(kpb_test_bitfield_bitfield_b1(&bitfield), 0x1);
    EXPECT_EQUAL(kpb_test_bitfield_bitfield_b2(&bitfield), 0x2);
    EXPECT_EQUAL(kpb_test_bitfield_bitfield_b4(&bitfield), 0x5);
    EXPECT_EQUAL(kpb_test_bitfield_bitfield_b8(&bitfield), 0xaa);
    EXPECT_EQUAL(kpb_test_bitfield_bitfield_b16(&bitfield), 0x5555);
    EXPECT_EQUAL(kpb_test_bitfield_bitfield_b32(&bitfield), 0xaaaaaaaa);
    EXPECT_EQUAL(kpb_test_bitfield_bitfield_b64(&bitfield), 0x5555555555555555);
    EXPECT_TRUE(kpb_test_bitfield_parse_from_string(&bitfield, "\x34\xab\x55\x54\xaa\xaa\xaa\xab\x55\x55\x55\x55\x55\x55\x55\x55", 16));
    EXPECT_EQUAL(kpb_test_bitfield_bitfield_b1(&bitfield), 0x0);
    EXPECT_EQUAL(kpb_test_bitfield_bitfield_b2(&bitfield), 0x1);
    EXPECT_EQUAL(kpb_test_bitfield_bitfield_b4(&bitfield), 0xa);
    EXPECT_EQUAL(kpb_test_bitfield_bitfield_b8(&bitfield), 0x55);
    EXPECT_EQUAL(kpb_test_bitfield_bitfield_b16(&bitfield), 0xaaaa);
    EXPECT_EQUAL(kpb_test_bitfield_bitfield_b32(&bitfield), 0x55555555);
    EXPECT_EQUAL(kpb_test_bitfield_bitfield_b64(&bitfield), 0xaaaaaaaaaaaaaaaa);

    kpb_test_bitfield_fini(&bitfield);
    return 0;
}

int kpb_test_bitfield(void)
{
    int err = 0;

    err = kpb_test_bitfield_single();
    if ( err ) return err;

    return err;
}
