#ifndef _H_KPB_TEST_BYTES_SINGLE_H_
#define _H_KPB_TEST_BYTES_SINGLE_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_bytes_single(void);

#ifdef __cplusplus
};
#endif

#endif
