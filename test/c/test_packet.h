#ifndef _H_KPB_TEST_PACKET_H_
#define _H_KPB_TEST_PACKET_H_

#ifdef __cplusplus
extern "C" {
#endif

int kpb_test_packet(void);

#ifdef __cplusplus
};
#endif

#endif
