#ifndef _H_KPB_TEST_MACRO_H_
#define _H_KPB_TEST_MACRO_H_

#include <stdio.h>
#include <string.h>
#include <wchar.h>

#define STACK_BUFFER(buf,size) \
    char buf[size]; \
    size_t buf##_size = 0

#define STACK_BUFFER_APPEND(buf,data,size) \
    if ( (buf##_size + size) > (sizeof(buf)) ) \
    { \
        printf("FAILED (%s:%d): " #buf "[%zd] is full\n", __FILE__, __LINE__, sizeof(buf)); \
        return 1; \
    } \
    memcpy(buf + buf##_size, data, size); \
    buf##_size += size

#define STACK_BUFFER_SIZE(buf)      (buf##_size)
#define STACK_BUFFER_GET(buf)       (buf)

#define EXPECT_EQUAL(a,b) \
    if ( (a) != (b) ) \
    { \
        printf("FAILED (%s:%d): " #a " != " #b "\n", __FILE__, __LINE__); \
        return 1; \
    }

#define EXPECT_APPROX_EQUAL(a,b,tol) \
    if ( ( (b) > ((a)+(tol)) ) || ( (b) < ((a)-(tol)) ) ) \
    { \
        printf("FAILED (%s:%d): " #a " (%f) !~= " #b " (%f)\n", __FILE__, __LINE__, (float)(a), (float)(b)); \
        return 1; \
    }

#define EXPECT_TRUE(a) \
    EXPECT_EQUAL(a, true)

#define EXPECT_FALSE(a) \
    EXPECT_EQUAL(a, false)

#define EXPECT_NULL(a) \
    if ( (a) != (NULL) ) \
    { \
        printf("FAILED (%s:%d): " #a " != NULL\n", __FILE__, __LINE__); \
        return 1; \
    }

#define EXPECT_NOT_NULL(a) \
    if ( (a) == (NULL) ) \
    { \
        printf("FAILED (%s:%d): " #a " == NULL\n", __FILE__, __LINE__); \
        return 1; \
    }

#define EXPECT_BYTES_EQUAL(a,a_size,b,b_size) \
    if ( (size_t)(a_size) != (size_t)(b_size) ) \
    { \
        printf("FAILED (%s:%d): " #a " (size %zd) != " #b " (size %zd)\n", __FILE__, __LINE__, (size_t)(a_size), (size_t)(b_size)); \
        return 1; \
    } \
    if ( 0 != memcmp((const void*)(a), (const void*)(b), (size_t)a_size) ) \
    { \
        for ( size_t __i__ = 0; (__i__ + 1) < ((size_t)(a_size) + 1); __i__++ ) \
        { \
            printf("offset=%zd, 0x%02x 0x%02x\n", __i__, ((uint8_t*)a)[__i__], ((uint8_t*)b)[__i__]); \
        } \
        printf("FAILED (%s:%d): " #a " != " #b "\n", __FILE__, __LINE__); \
        return 1; \
    }

#define EXPECT_STD_STRING_EQUAL(a,b) \
    EXPECT_BYTES_EQUAL(a.c_str(), a.size(), b.c_str(), b.size())

#define EXPECT_STRING_EQUAL(a,b) \
    EXPECT_BYTES_EQUAL(a, (sizeof(a[0])==sizeof(wchar_t)?sizeof(wchar_t)*wcslen((const wchar_t*)(a)):strlen((const char*)(a))), b, (sizeof(b[0])==sizeof(wchar_t)?sizeof(wchar_t)*wcslen((const wchar_t*)(b)):strlen((const char*)(a))))

#endif
