#include "test_randomize.hpp"

#include <stdio.h>

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#include <Windows.h>
#include <Wincrypt.h>
#else
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#endif

#include "../common/macro.h"

#include "cpp_stl/randomize.hpp"

namespace kpb {
namespace test {

static bool custom_random_bytes(void *ctx, char *data, size_t size)
{
#ifdef WIN32
    (void)ctx;
    HCRYPTPROV hCryptProv;
    if ( !CryptAcquireContext(&hCryptProv, NULL, NULL, PROV_RSA_FULL, 0) )
    {
        return false;
    }
    if ( !CryptGenRandom(hCryptProv, (DWORD)size, (BYTE *)data) )
    {
        CryptReleaseContext(hCryptProv, 0);
        return false;
    }
    CryptReleaseContext(hCryptProv, 0);
    return true;
#else
    (void)ctx;
    int fd = open("/dev/urandom", O_RDONLY);
    if ( fd < 0 )
    {
        return false;
    }
    ssize_t recd = read(fd, data, (unsigned int)size);
    close(fd);
    if ( recd < 0 )
    {
        return false;
    }
    if ((size_t)recd != size)
    {
        return false;
    }
    return true;
#endif
}

static char g_random[50];
static size_t g_random_index = 0;
static size_t g_random_size = 0;

static bool custom_fixed_bytes(void *ctx, char *data, size_t size)
{
    (void)ctx;
    for ( size_t i = 0; i < size; i++ )
    {
        if ( g_random_index < g_random_size )
        {
            data[i] = g_random[g_random_index];
            g_random_index++;
        }
        else
        {
            data[i] = 0;
        }
    }
    return true;
}

static void kpb_random_from_buffer(const char *data, size_t size)
{
    // deterministic data
    memcpy(g_random, data, size);
    g_random_index = 0;
    g_random_size = size;
    kpb::random_api(custom_fixed_bytes, NULL);
}

static void kpb_random_os()
{
    // to make things "random", we need a way to get random bytes
    kpb::random_api(custom_random_bytes, NULL);
}

int randomize(void)
{

    kpb_test::Randomize_ randomize;
    std::string result;

    EXPECT_TRUE(randomize.set_s1_min(-128));
    EXPECT_TRUE(randomize.set_s1_max(127));
    kpb_random_from_buffer("\x00", 1);
    EXPECT_TRUE(randomize.randomize_s1());
    EXPECT_EQUAL(-128, randomize.s1());
    kpb_random_from_buffer("\xff", 1);
    EXPECT_TRUE(randomize.randomize_s1());
    EXPECT_EQUAL(127, randomize.s1());
    EXPECT_TRUE(randomize.set_s1_min(-127));
    EXPECT_TRUE(randomize.set_s1_max(126));
    kpb_random_from_buffer("\x00", 1);
    EXPECT_TRUE(randomize.randomize_s1());
    EXPECT_EQUAL(-127, randomize.s1());
    kpb_random_from_buffer("\xfd", 1);
    EXPECT_TRUE(randomize.randomize_s1());
    EXPECT_EQUAL(126, randomize.s1());
    EXPECT_TRUE(randomize.set_s1_min(0x42));
    EXPECT_TRUE(randomize.set_s1_max(0x42));
    kpb_random_from_buffer("\x12", 1);
    EXPECT_TRUE(randomize.randomize_s1());
    EXPECT_EQUAL(0x42, randomize.s1());

    EXPECT_TRUE(randomize.set_count_min(0));
    EXPECT_TRUE(randomize.set_count_max(3));
    EXPECT_TRUE(randomize.set_s1_min(-128));
    EXPECT_TRUE(randomize.set_s1_max(127));
    kpb_random_from_buffer("\x00\x00\x00\x02\x00\x00\x00", 7);
    EXPECT_TRUE(randomize.randomize_s1_list());
    EXPECT_EQUAL(-128, randomize.s1_list(0));
    EXPECT_EQUAL(-128, randomize.s1_list(1));
    EXPECT_EQUAL(0, randomize.s1_list(2));
    kpb_random_from_buffer("\x00\x00\x00\x02\xff\xff\xff", 7);
    EXPECT_TRUE(randomize.randomize_s1_list());
    EXPECT_EQUAL(127, randomize.s1_list(0));
    EXPECT_EQUAL(127, randomize.s1_list(1));
    EXPECT_EQUAL(0, randomize.s1_list(2));
    EXPECT_TRUE(randomize.set_s1_min(-127));
    EXPECT_TRUE(randomize.set_s1_max(126));
    kpb_random_from_buffer("\x00\x00\x00\x02\x00\x00\x00", 7);
    EXPECT_TRUE(randomize.randomize_s1_list());
    EXPECT_EQUAL(-127, randomize.s1_list(0));
    EXPECT_EQUAL(-127, randomize.s1_list(1));
    EXPECT_EQUAL(0, randomize.s1_list(2));
    kpb_random_from_buffer("\x00\x00\x00\x02\xfd\xfd\xfd", 7);
    EXPECT_TRUE(randomize.randomize_s1_list());
    EXPECT_EQUAL(126, randomize.s1_list(0));
    EXPECT_EQUAL(126, randomize.s1_list(1));
    EXPECT_EQUAL(0, randomize.s1_list(2));
    EXPECT_TRUE(randomize.set_s1_min(0x42));
    EXPECT_TRUE(randomize.set_s1_max(0x42));
    kpb_random_from_buffer("\x00\x00\x00\x02\x12\x34\x56", 7);
    EXPECT_TRUE(randomize.randomize_s1_list());
    EXPECT_EQUAL(0x42, randomize.s1_list(0));
    EXPECT_EQUAL(0x42, randomize.s1_list(1));
    EXPECT_EQUAL(0, randomize.s1_list(2));

    EXPECT_TRUE(randomize.set_s2_min(~((int16_t)0x7fff)));
    EXPECT_TRUE(randomize.set_s2_max(0x7fff));
    kpb_random_from_buffer("\x00\x00", 2);
    EXPECT_TRUE(randomize.randomize_s2());
    EXPECT_EQUAL(~((int16_t)0x7fff), randomize.s2());
    kpb_random_from_buffer("\xff\xff", 2);
    EXPECT_TRUE(randomize.randomize_s2());
    EXPECT_EQUAL(0x7fff, randomize.s2());
    EXPECT_TRUE(randomize.set_s2_min(~((int16_t)0x7ffe)));
    EXPECT_TRUE(randomize.set_s2_max(0x7ffe));
    kpb_random_from_buffer("\x00\x00", 2);
    EXPECT_TRUE(randomize.randomize_s2());
    EXPECT_EQUAL(~((int16_t)0x7ffe), randomize.s2());
    kpb_random_from_buffer("\xff\xfd", 2);
    EXPECT_TRUE(randomize.randomize_s2());
    EXPECT_EQUAL(0x7ffe, randomize.s2());
    EXPECT_TRUE(randomize.set_s2_min(0x7bcd));
    EXPECT_TRUE(randomize.set_s2_max(0x7bcd));
    kpb_random_from_buffer("\x12\x34", 2);
    EXPECT_TRUE(randomize.randomize_s2());
    EXPECT_EQUAL(0x7bcd, randomize.s2());

    EXPECT_TRUE(randomize.set_count_min(0));
    EXPECT_TRUE(randomize.set_count_max(3));
    EXPECT_TRUE(randomize.set_s2_min(~((int16_t)0x7fff)));
    EXPECT_TRUE(randomize.set_s2_max(0x7fff));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00" "\x00\x00" "\x00\x00", 4 + (2 * 3));
    EXPECT_TRUE(randomize.randomize_s2_list());
    EXPECT_EQUAL(~((int16_t)0x7fff), randomize.s2_list(0));
    EXPECT_EQUAL(~((int16_t)0x7fff), randomize.s2_list(1));
    EXPECT_EQUAL(0, randomize.s2_list(2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff" "\xff\xff" "\xff\xff", 4 + (2 * 3));
    EXPECT_TRUE(randomize.randomize_s2_list());
    EXPECT_EQUAL(0x7fff, randomize.s2_list(0));
    EXPECT_EQUAL(0x7fff, randomize.s2_list(1));
    EXPECT_EQUAL(0, randomize.s2_list(2));
    EXPECT_TRUE(randomize.set_s2_min(~((int16_t)0x7ffe)));
    EXPECT_TRUE(randomize.set_s2_max(0x7ffe));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00" "\x00\x00" "\x00\x00", 4 + (2 * 3));
    EXPECT_TRUE(randomize.randomize_s2_list());
    EXPECT_EQUAL(~((int16_t)0x7ffe), randomize.s2_list(0));
    EXPECT_EQUAL(~((int16_t)0x7ffe), randomize.s2_list(1));
    EXPECT_EQUAL(0, randomize.s2_list(2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xfd" "\xff\xfd" "\xff\xfd", 4 + (2 * 3));
    EXPECT_TRUE(randomize.randomize_s2_list());
    EXPECT_EQUAL(0x7ffe, randomize.s2_list(0));
    EXPECT_EQUAL(0x7ffe, randomize.s2_list(1));
    EXPECT_EQUAL(0, randomize.s2_list(2));
    EXPECT_TRUE(randomize.set_s2_min(0x7bcd));
    EXPECT_TRUE(randomize.set_s2_max(0x7bcd));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x12\x34" "\x56\x78" "\x90\xab", 4 + (2 * 3));
    EXPECT_TRUE(randomize.randomize_s2_list());
    EXPECT_EQUAL(0x7bcd, randomize.s2_list(0));
    EXPECT_EQUAL(0x7bcd, randomize.s2_list(1));
    EXPECT_EQUAL(0, randomize.s2_list(2));

    EXPECT_TRUE(randomize.set_s4_min(~((int32_t)0x7fffffff)));
    EXPECT_TRUE(randomize.set_s4_max(0x7fffffff));
    kpb_random_from_buffer("\x00\x00\x00\x00", 4);
    EXPECT_TRUE(randomize.randomize_s4());
    EXPECT_EQUAL(~((int32_t)0x7fffffff), randomize.s4());
    kpb_random_from_buffer("\xff\xff\xff\xff", 4);
    EXPECT_TRUE(randomize.randomize_s4());
    EXPECT_EQUAL(0x7fffffff, randomize.s4());
    EXPECT_TRUE(randomize.set_s4_min(~((int32_t)0x7ffffffe)));
    EXPECT_TRUE(randomize.set_s4_max(0x7ffffffe));
    kpb_random_from_buffer("\x00\x00\x00\x00", 4);
    EXPECT_TRUE(randomize.randomize_s4());
    EXPECT_EQUAL(~((int32_t)0x7ffffffe), randomize.s4());
    kpb_random_from_buffer("\xff\xff\xff\xfd", 4);
    EXPECT_TRUE(randomize.randomize_s4());
    EXPECT_EQUAL(0x7ffffffe, randomize.s4());
    EXPECT_TRUE(randomize.set_s4_min(0x7bcd1234));
    EXPECT_TRUE(randomize.set_s4_max(0x7bcd1234));
    kpb_random_from_buffer("\x12\x34\x56\x78", 4);
    EXPECT_TRUE(randomize.randomize_s4());
    EXPECT_EQUAL(0x7bcd1234, randomize.s4());

    EXPECT_TRUE(randomize.set_count_min(0));
    EXPECT_TRUE(randomize.set_count_max(3));
    EXPECT_TRUE(randomize.set_s4_min(~((int32_t)0x7fffffff)));
    EXPECT_TRUE(randomize.set_s4_max(0x7fffffff));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "\x00\x00\x00\x00" "\x00\x00\x00\x00", 4 + (4 * 3));
    EXPECT_TRUE(randomize.randomize_s4_list());
    EXPECT_EQUAL(~((int32_t)0x7fffffff), randomize.s4_list(0));
    EXPECT_EQUAL(~((int32_t)0x7fffffff), randomize.s4_list(1));
    EXPECT_EQUAL(0, randomize.s4_list(2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff\xff\xff" "\xff\xff\xff\xff" "\xff\xff\xff\xff", 4 + (4 * 3));
    EXPECT_TRUE(randomize.randomize_s4_list());
    EXPECT_EQUAL(0x7fffffff, randomize.s4_list(0));
    EXPECT_EQUAL(0x7fffffff, randomize.s4_list(1));
    EXPECT_EQUAL(0, randomize.s4_list(2));
    EXPECT_TRUE(randomize.set_s4_min(~((int32_t)0x7ffffffe)));
    EXPECT_TRUE(randomize.set_s4_max(0x7ffffffe));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "\x00\x00\x00\x00" "\x00\x00\x00\x00", 4 + (4 * 3));
    EXPECT_TRUE(randomize.randomize_s4_list());
    EXPECT_EQUAL(~((int32_t)0x7ffffffe), randomize.s4_list(0));
    EXPECT_EQUAL(~((int32_t)0x7ffffffe), randomize.s4_list(1));
    EXPECT_EQUAL(0, randomize.s4_list(2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff\xff\xfd" "\xff\xff\xff\xfd" "\xff\xff\xff\xfd", 4 + (4 * 3));
    EXPECT_TRUE(randomize.randomize_s4_list());
    EXPECT_EQUAL(0x7ffffffe, randomize.s4_list(0));
    EXPECT_EQUAL(0x7ffffffe, randomize.s4_list(1));
    EXPECT_EQUAL(0, randomize.s4_list(2));
    EXPECT_TRUE(randomize.set_s4_min(0x7bcd2345));
    EXPECT_TRUE(randomize.set_s4_max(0x7bcd2345));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x12\x34\x12\x34" "\x56\x78\x56\x78" "\x90\xab\x90\xab", 4 + (4 * 3));
    EXPECT_TRUE(randomize.randomize_s4_list());
    EXPECT_EQUAL(0x7bcd2345, randomize.s4_list(0));
    EXPECT_EQUAL(0x7bcd2345, randomize.s4_list(1));
    EXPECT_EQUAL(0, randomize.s4_list(2));

    EXPECT_TRUE(randomize.set_s8_min(~((int64_t)0x7fffffffffffffff)));
    EXPECT_TRUE(randomize.set_s8_max(0x7fffffffffffffff));
    kpb_random_from_buffer("\x00\x00\x00\x00\x00\x00\x00\x00", 8);
    EXPECT_TRUE(randomize.randomize_s8());
    EXPECT_EQUAL(~((int64_t)0x7fffffffffffffff), randomize.s8());
    kpb_random_from_buffer("\xff\xff\xff\xff\xff\xff\xff\xff", 8);
    EXPECT_TRUE(randomize.randomize_s8());
    EXPECT_EQUAL(0x7fffffffffffffff, randomize.s8());
    EXPECT_TRUE(randomize.set_s8_min(~((int64_t)0x7ffffffffffffffe)));
    EXPECT_TRUE(randomize.set_s8_max(0x7ffffffffffffffe));
    kpb_random_from_buffer("\x00\x00\x00\x00\x00\x00\x00\x00", 8);
    EXPECT_TRUE(randomize.randomize_s8());
    EXPECT_EQUAL(~((int64_t)0x7ffffffffffffffe), randomize.s8());
    kpb_random_from_buffer("\xff\xff\xff\xff\xff\xff\xff\xfd", 8);
    EXPECT_TRUE(randomize.randomize_s8());
    EXPECT_EQUAL(0x7ffffffffffffffe, randomize.s8());
    EXPECT_TRUE(randomize.set_s8_min(0x7bcd2345abcd5678));
    EXPECT_TRUE(randomize.set_s8_max(0x7bcd2345abcd5678));
    kpb_random_from_buffer("\x12\x34\x56\x78\x12\x34\x56\x78", 8);
    EXPECT_TRUE(randomize.randomize_s8());
    EXPECT_EQUAL(0x7bcd2345abcd5678, randomize.s8());

    EXPECT_TRUE(randomize.set_count_min(0));
    EXPECT_TRUE(randomize.set_count_max(3));
    EXPECT_TRUE(randomize.set_s8_min(~((int64_t)0x7fffffffffffffff)));
    EXPECT_TRUE(randomize.set_s8_max(0x7fffffffffffffff));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00", 4 + (8 * 3));
    EXPECT_TRUE(randomize.randomize_s8_list());
    EXPECT_EQUAL(~((int64_t)0x7fffffffffffffff), randomize.s8_list(0));
    EXPECT_EQUAL(~((int64_t)0x7fffffffffffffff), randomize.s8_list(1));
    EXPECT_EQUAL(0, randomize.s8_list(2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff\xff\xff\xff\xff\xff\xff" "\xff\xff\xff\xff\xff\xff\xff\xff" "\xff\xff\xff\xff\xff\xff\xff\xff", 4 + (8 * 3));
    EXPECT_TRUE(randomize.randomize_s8_list());
    EXPECT_EQUAL(0x7fffffffffffffff, randomize.s8_list(0));
    EXPECT_EQUAL(0x7fffffffffffffff, randomize.s8_list(1));
    EXPECT_EQUAL(0, randomize.s8_list(2));
    EXPECT_TRUE(randomize.set_s8_min(~((int64_t)0x7ffffffffffffffe)));
    EXPECT_TRUE(randomize.set_s8_max(0x7ffffffffffffffe));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00", 4 + (8 * 3));
    EXPECT_TRUE(randomize.randomize_s8_list());
    EXPECT_EQUAL(~((int64_t)0x7ffffffffffffffe), randomize.s8_list(0));
    EXPECT_EQUAL(~((int64_t)0x7ffffffffffffffe), randomize.s8_list(1));
    EXPECT_EQUAL(0, randomize.s8_list(2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff\xff\xff\xff\xff\xff\xfd" "\xff\xff\xff\xff\xff\xff\xff\xfd" "\xff\xff\xff\xff\xff\xff\xff\xfd", 4 + (8 * 3));
    EXPECT_TRUE(randomize.randomize_s8_list());
    EXPECT_EQUAL(0x7ffffffffffffffe, randomize.s8_list(0));
    EXPECT_EQUAL(0x7ffffffffffffffe, randomize.s8_list(1));
    EXPECT_EQUAL(0, randomize.s8_list(2));
    EXPECT_TRUE(randomize.set_s8_min(0x7bcd2345abcd5678));
    EXPECT_TRUE(randomize.set_s8_max(0x7bcd2345abcd5678));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x12\x34\x12\x34\x12\x34\x12\x34" "\x56\x78\x56\x78\x56\x78\x56\x78" "\x90\xab\x90\xab\x90\xab\x90\xab", 4 + (8 * 3));
    EXPECT_TRUE(randomize.randomize_s8_list());
    EXPECT_EQUAL(0x7bcd2345abcd5678, randomize.s8_list(0));
    EXPECT_EQUAL(0x7bcd2345abcd5678, randomize.s8_list(1));
    EXPECT_EQUAL(0, randomize.s8_list(2));

    EXPECT_TRUE(randomize.set_u1_min(0));
    EXPECT_TRUE(randomize.set_u1_max(0xff));
    kpb_random_from_buffer("\x00", 1);
    EXPECT_TRUE(randomize.randomize_u1());
    EXPECT_EQUAL(0, randomize.u1());
    kpb_random_from_buffer("\xff", 1);
    EXPECT_TRUE(randomize.randomize_u1());
    EXPECT_EQUAL(0xff, randomize.u1());
    EXPECT_TRUE(randomize.set_u1_min(1));
    EXPECT_TRUE(randomize.set_u1_max(0xfe));
    kpb_random_from_buffer("\x00", 1);
    EXPECT_TRUE(randomize.randomize_u1());
    EXPECT_EQUAL(1, randomize.u1());
    kpb_random_from_buffer("\xfd", 1);
    EXPECT_TRUE(randomize.randomize_u1());
    EXPECT_EQUAL(0xfe, randomize.u1());
    EXPECT_TRUE(randomize.set_u1_min(0x42));
    EXPECT_TRUE(randomize.set_u1_max(0x42));
    kpb_random_from_buffer("\x12", 1);
    EXPECT_TRUE(randomize.randomize_u1());
    EXPECT_EQUAL(0x42, randomize.u1());

    EXPECT_TRUE(randomize.set_count_min(0));
    EXPECT_TRUE(randomize.set_count_max(3));
    EXPECT_TRUE(randomize.set_u1_min(0));
    EXPECT_TRUE(randomize.set_u1_max(0xff));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00" "\x00" "\x00", 4 + (1 * 3));
    EXPECT_TRUE(randomize.randomize_u1_list());
    EXPECT_EQUAL(0, randomize.u1_list(0));
    EXPECT_EQUAL(0, randomize.u1_list(1));
    EXPECT_EQUAL(0, randomize.u1_list(2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff" "\xff" "\xff", 4 + (1 * 3));
    EXPECT_TRUE(randomize.randomize_u1_list());
    EXPECT_EQUAL(0xff, randomize.u1_list(0));
    EXPECT_EQUAL(0xff, randomize.u1_list(1));
    EXPECT_EQUAL(0, randomize.u1_list(2));
    EXPECT_TRUE(randomize.set_u1_min(1));
    EXPECT_TRUE(randomize.set_u1_max(0xfe));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00" "\x00" "\x00", 4 + (1 * 3));
    EXPECT_TRUE(randomize.randomize_u1_list());
    EXPECT_EQUAL(1, randomize.u1_list(0));
    EXPECT_EQUAL(1, randomize.u1_list(1));
    EXPECT_EQUAL(0, randomize.u1_list(2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xfd" "\xfd" "\xfd", 4 + (1 * 3));
    EXPECT_TRUE(randomize.randomize_u1_list());
    EXPECT_EQUAL(0xfe, randomize.u1_list(0));
    EXPECT_EQUAL(0xfe, randomize.u1_list(1));
    EXPECT_EQUAL(0, randomize.u1_list(2));
    EXPECT_TRUE(randomize.set_u1_min(0x42));
    EXPECT_TRUE(randomize.set_u1_max(0x42));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x12" "\x34" "\x56", 4 + (1 * 3));
    EXPECT_TRUE(randomize.randomize_u1_list());
    EXPECT_EQUAL(0x42, randomize.u1_list(0));
    EXPECT_EQUAL(0x42, randomize.u1_list(1));
    EXPECT_EQUAL(0, randomize.u1_list(2));

    EXPECT_TRUE(randomize.set_u2_min(0));
    EXPECT_TRUE(randomize.set_u2_max(0xffff));
    kpb_random_from_buffer("\x00\x00", 2);
    EXPECT_TRUE(randomize.randomize_u2());
    EXPECT_EQUAL(0, randomize.u2());
    kpb_random_from_buffer("\xff\xff", 2);
    EXPECT_TRUE(randomize.randomize_u2());
    EXPECT_EQUAL(0xffff, randomize.u2());
    EXPECT_TRUE(randomize.set_u2_min(1));
    EXPECT_TRUE(randomize.set_u2_max(0xfffe));
    kpb_random_from_buffer("\x00\x00", 2);
    EXPECT_TRUE(randomize.randomize_u2());
    EXPECT_EQUAL(1, randomize.u2());
    kpb_random_from_buffer("\xff\xfd", 2);
    EXPECT_TRUE(randomize.randomize_u2());
    EXPECT_EQUAL(0xfffe, randomize.u2());
    EXPECT_TRUE(randomize.set_u2_min(0x8442));
    EXPECT_TRUE(randomize.set_u2_max(0x8442));
    kpb_random_from_buffer("\x12\x34", 2);
    EXPECT_TRUE(randomize.randomize_u2());
    EXPECT_EQUAL(0x8442, randomize.u2());

    EXPECT_TRUE(randomize.set_count_min(0));
    EXPECT_TRUE(randomize.set_count_max(3));
    EXPECT_TRUE(randomize.set_u2_min(0));
    EXPECT_TRUE(randomize.set_u2_max(0xffff));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00" "\x00\x00" "\x00\x00", 4 + (2 * 3));
    EXPECT_TRUE(randomize.randomize_u2_list());
    EXPECT_EQUAL(0, randomize.u2_list(0));
    EXPECT_EQUAL(0, randomize.u2_list(1));
    EXPECT_EQUAL(0, randomize.u2_list(2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff" "\xff\xff" "\xff\xff", 4 + (2 * 3));
    EXPECT_TRUE(randomize.randomize_u2_list());
    EXPECT_EQUAL(0xffff, randomize.u2_list(0));
    EXPECT_EQUAL(0xffff, randomize.u2_list(1));
    EXPECT_EQUAL(0, randomize.u2_list(2));
    EXPECT_TRUE(randomize.set_u2_min(1));
    EXPECT_TRUE(randomize.set_u2_max(0xfffe));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00" "\x00\x00" "\x00\x00", 4 + (2 * 3));
    EXPECT_TRUE(randomize.randomize_u2_list());
    EXPECT_EQUAL(1, randomize.u2_list(0));
    EXPECT_EQUAL(1, randomize.u2_list(1));
    EXPECT_EQUAL(0, randomize.u2_list(2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xfd" "\xff\xfd" "\xff\xfd", 4 + (2 * 3));
    EXPECT_TRUE(randomize.randomize_u2_list());
    EXPECT_EQUAL(0xfffe, randomize.u2_list(0));
    EXPECT_EQUAL(0xfffe, randomize.u2_list(1));
    EXPECT_EQUAL(0, randomize.u2_list(2));
    EXPECT_TRUE(randomize.set_u2_min(0x8442));
    EXPECT_TRUE(randomize.set_u2_max(0x8442));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x12\x34" "\x34\x56" "\x56\x78", 4 + (2 * 3));
    EXPECT_TRUE(randomize.randomize_u2_list());
    EXPECT_EQUAL(0x8442, randomize.u2_list(0));
    EXPECT_EQUAL(0x8442, randomize.u2_list(1));
    EXPECT_EQUAL(0, randomize.u2_list(2));

    EXPECT_TRUE(randomize.set_u4_min(0));
    EXPECT_TRUE(randomize.set_u4_max(0xffffffff));
    kpb_random_from_buffer("\x00\x00\x00\x00", 4);
    EXPECT_TRUE(randomize.randomize_u4());
    EXPECT_EQUAL(0, randomize.u4());
    kpb_random_from_buffer("\xff\xff\xff\xff", 4);
    EXPECT_TRUE(randomize.randomize_u4());
    EXPECT_EQUAL(0xffffffff, randomize.u4());
    EXPECT_TRUE(randomize.set_u4_min(1));
    EXPECT_TRUE(randomize.set_u4_max(0xfffffffe));
    kpb_random_from_buffer("\x00\x00\x00\x00", 4);
    EXPECT_TRUE(randomize.randomize_u4());
    EXPECT_EQUAL(1, randomize.u4());
    kpb_random_from_buffer("\xff\xff\xff\xfd", 4);
    EXPECT_TRUE(randomize.randomize_u4());
    EXPECT_EQUAL(0xfffffffe, randomize.u4());
    EXPECT_TRUE(randomize.set_u4_min(0x8442a1b2));
    EXPECT_TRUE(randomize.set_u4_max(0x8442a1b2));
    kpb_random_from_buffer("\x12\x34\x56\x78", 4);
    EXPECT_TRUE(randomize.randomize_u4());
    EXPECT_EQUAL(0x8442a1b2, randomize.u4());

    EXPECT_TRUE(randomize.set_count_min(0));
    EXPECT_TRUE(randomize.set_count_max(3));
    EXPECT_TRUE(randomize.set_u4_min(0));
    EXPECT_TRUE(randomize.set_u4_max(0xffffffff));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "\x00\x00\x00\x00" "\x00\x00\x00\x00", 4 + (4 * 3));
    EXPECT_TRUE(randomize.randomize_u4_list());
    EXPECT_EQUAL(0, randomize.u4_list(0));
    EXPECT_EQUAL(0, randomize.u4_list(1));
    EXPECT_EQUAL(0, randomize.u4_list(2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff\xff\xff" "\xff\xff\xff\xff" "\xff\xff\xff\xff", 4 + (4 * 3));
    EXPECT_TRUE(randomize.randomize_u4_list());
    EXPECT_EQUAL(0xffffffff, randomize.u4_list(0));
    EXPECT_EQUAL(0xffffffff, randomize.u4_list(1));
    EXPECT_EQUAL(0, randomize.u4_list(2));
    EXPECT_TRUE(randomize.set_u4_min(1));
    EXPECT_TRUE(randomize.set_u4_max(0xfffffffe));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "\x00\x00\x00\x00" "\x00\x00\x00\x00", 4 + (4 * 3));
    EXPECT_TRUE(randomize.randomize_u4_list());
    EXPECT_EQUAL(1, randomize.u4_list(0));
    EXPECT_EQUAL(1, randomize.u4_list(1));
    EXPECT_EQUAL(0, randomize.u4_list(2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff\xff\xfd" "\xff\xff\xff\xfd" "\xff\xff\xff\xfd", 4 + (4 * 3));
    EXPECT_TRUE(randomize.randomize_u4_list());
    EXPECT_EQUAL(0xfffffffe, randomize.u4_list(0));
    EXPECT_EQUAL(0xfffffffe, randomize.u4_list(1));
    EXPECT_EQUAL(0, randomize.u4_list(2));
    EXPECT_TRUE(randomize.set_u4_min(0x8442a1b2));
    EXPECT_TRUE(randomize.set_u4_max(0x8442a1b2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x12\x34\x56\x78" "\x34\x56\x78\x90" "\x56\x78\xab\xcd", 4 + (4 * 3));
    EXPECT_TRUE(randomize.randomize_u4_list());
    EXPECT_EQUAL(0x8442a1b2, randomize.u4_list(0));
    EXPECT_EQUAL(0x8442a1b2, randomize.u4_list(1));
    EXPECT_EQUAL(0, randomize.u4_list(2));

    EXPECT_TRUE(randomize.set_u8_min(0));
    EXPECT_TRUE(randomize.set_u8_max(0xffffffffffffffff));
    kpb_random_from_buffer("\x00\x00\x00\x00\x00\x00\x00\x00", 8);
    EXPECT_TRUE(randomize.randomize_u8());
    EXPECT_EQUAL(0, randomize.u8());
    kpb_random_from_buffer("\xff\xff\xff\xff\xff\xff\xff\xff", 8);
    EXPECT_TRUE(randomize.randomize_u8());
    EXPECT_EQUAL(0xffffffffffffffff, randomize.u8());
    EXPECT_TRUE(randomize.set_u8_min(1));
    EXPECT_TRUE(randomize.set_u8_max(0xfffffffffffffffe));
    kpb_random_from_buffer("\x00\x00\x00\x00\x00\x00\x00\x00", 8);
    EXPECT_TRUE(randomize.randomize_u8());
    EXPECT_EQUAL(1, randomize.u8());
    kpb_random_from_buffer("\xff\xff\xff\xff\xff\xff\xff\xfd", 8);
    EXPECT_TRUE(randomize.randomize_u8());
    EXPECT_EQUAL(0xfffffffffffffffe, randomize.u8());
    EXPECT_TRUE(randomize.set_u8_min(0x8442a1b28442a1b2));
    EXPECT_TRUE(randomize.set_u8_max(0x8442a1b28442a1b2));
    kpb_random_from_buffer("\x12\x34\x56\x78\x12\x34\x56\x78", 8);
    EXPECT_TRUE(randomize.randomize_u8());
    EXPECT_EQUAL(0x8442a1b28442a1b2, randomize.u8());

    EXPECT_TRUE(randomize.set_count_min(0));
    EXPECT_TRUE(randomize.set_count_max(3));
    EXPECT_TRUE(randomize.set_u8_min(0));
    EXPECT_TRUE(randomize.set_u8_max(0xffffffffffffffff));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00", 4 + (8 * 3));
    EXPECT_TRUE(randomize.randomize_u8_list());
    EXPECT_EQUAL(0, randomize.u8_list(0));
    EXPECT_EQUAL(0, randomize.u8_list(1));
    EXPECT_EQUAL(0, randomize.u8_list(2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff\xff\xff\xff\xff\xff\xff" "\xff\xff\xff\xff\xff\xff\xff\xff" "\xff\xff\xff\xff\xff\xff\xff\xff", 4 + (8 * 3));
    EXPECT_TRUE(randomize.randomize_u8_list());
    EXPECT_EQUAL(0xffffffffffffffff, randomize.u8_list(0));
    EXPECT_EQUAL(0xffffffffffffffff, randomize.u8_list(1));
    EXPECT_EQUAL(0, randomize.u8_list(2));
    EXPECT_TRUE(randomize.set_u8_min(1));
    EXPECT_TRUE(randomize.set_u8_max(0xfffffffffffffffe));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00", 4 + (8 * 3));
    EXPECT_TRUE(randomize.randomize_u8_list());
    EXPECT_EQUAL(1, randomize.u8_list(0));
    EXPECT_EQUAL(1, randomize.u8_list(1));
    EXPECT_EQUAL(0, randomize.u8_list(2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff\xff\xff\xff\xff\xff\xfd" "\xff\xff\xff\xff\xff\xff\xff\xfd" "\xff\xff\xff\xff\xff\xff\xff\xfd", 4 + (8 * 3));
    EXPECT_TRUE(randomize.randomize_u8_list());
    EXPECT_EQUAL(0xfffffffffffffffe, randomize.u8_list(0));
    EXPECT_EQUAL(0xfffffffffffffffe, randomize.u8_list(1));
    EXPECT_EQUAL(0, randomize.u8_list(2));
    EXPECT_TRUE(randomize.set_u8_min(0x8442a1b28442a1b2));
    EXPECT_TRUE(randomize.set_u8_max(0x8442a1b28442a1b2));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x12\x34\x56\x78\x12\x34\x56\x78" "\x34\x56\x78\x90\x34\x56\x78\x90" "\x56\x78\xab\xcd\x56\x78\xab\xcd", 4 + (8 * 3));
    EXPECT_TRUE(randomize.randomize_u8_list());
    EXPECT_EQUAL(0x8442a1b28442a1b2, randomize.u8_list(0));
    EXPECT_EQUAL(0x8442a1b28442a1b2, randomize.u8_list(1));
    EXPECT_EQUAL(0, randomize.u8_list(2));

    EXPECT_TRUE(randomize.set_bool_min(false));
    EXPECT_TRUE(randomize.set_bool_max(false));
    kpb_random_from_buffer("\xff", 1);
    EXPECT_TRUE(randomize.randomize_bool_());
    EXPECT_EQUAL(false, randomize.bool_());
    kpb_random_from_buffer("\x00", 1);
    EXPECT_TRUE(randomize.randomize_bool_());
    EXPECT_EQUAL(false, randomize.bool_());
    EXPECT_TRUE(randomize.set_bool_min(true));
    EXPECT_TRUE(randomize.set_bool_max(true));
    kpb_random_from_buffer("\xff", 1);
    EXPECT_TRUE(randomize.randomize_bool_());
    EXPECT_EQUAL(true, randomize.bool_());
    kpb_random_from_buffer("\x00", 1);
    EXPECT_TRUE(randomize.randomize_bool_());
    EXPECT_EQUAL(true, randomize.bool_());
    EXPECT_TRUE(randomize.set_bool_min(false));
    EXPECT_TRUE(randomize.set_bool_max(true));
    kpb_random_from_buffer("\xff", 1);
    EXPECT_TRUE(randomize.randomize_bool_());
    EXPECT_EQUAL(true, randomize.bool_());
    kpb_random_from_buffer("\x00", 1);
    EXPECT_TRUE(randomize.randomize_bool_());
    EXPECT_EQUAL(false, randomize.bool_());

    kpb_random_from_buffer("\x00\x00\x00\x00\x00\x00\x00\x00", 8);
    EXPECT_TRUE(randomize.randomize_f4());
    EXPECT_APPROX_EQUAL(-11.1, randomize.f4(), 0.001);
    kpb_random_from_buffer("\xff\xff\xff\xff\xff\xff\xff\xff", 8);
    EXPECT_TRUE(randomize.randomize_f4());
    EXPECT_APPROX_EQUAL(22.3, randomize.f4(), 0.001);
    kpb_random_from_buffer("\x7f\xff\xff\xff\xff\xff\xff\xff", 8);
    EXPECT_TRUE(randomize.randomize_f4());
    EXPECT_APPROX_EQUAL(5.6, randomize.f4(), 0.001);
    kpb_random_from_buffer("\x12\x34\x56\x78\x12\x34\x56\x78", 8);
    EXPECT_TRUE(randomize.randomize_f4());
    EXPECT_APPROX_EQUAL(-8.724890, randomize.f4(), 0.001);

    EXPECT_TRUE(randomize.set_count_min(0));
    EXPECT_TRUE(randomize.set_count_max(3));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00", 4 + (8 * 3));
    EXPECT_TRUE(randomize.randomize_f4_list());
    EXPECT_APPROX_EQUAL(-11.1, randomize.f4_list(0), 0.001);
    EXPECT_APPROX_EQUAL(-11.1, randomize.f4_list(1), 0.001);
    EXPECT_APPROX_EQUAL(0.0, randomize.f4_list(2), 0.001);
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff\xff\xff\xff\xff\xff\xff" "\xff\xff\xff\xff\xff\xff\xff\xff" "\xff\xff\xff\xff\xff\xff\xff\xff", 4 + (8 * 3));
    EXPECT_TRUE(randomize.randomize_f4_list());
    EXPECT_APPROX_EQUAL(22.3, randomize.f4_list(0), 0.001);
    EXPECT_APPROX_EQUAL(22.3, randomize.f4_list(1), 0.001);
    EXPECT_APPROX_EQUAL(0.0, randomize.f4_list(2), 0.001);
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x7f\xff\xff\xff\xff\xff\xff\xff" "\x7f\xff\xff\xff\xff\xff\xff\xff" "\x7f\xff\xff\xff\xff\xff\xff\xff", 4 + (8 * 3));
    EXPECT_TRUE(randomize.randomize_f4_list());
    EXPECT_APPROX_EQUAL(5.6, randomize.f4_list(0), 0.001);
    EXPECT_APPROX_EQUAL(5.6, randomize.f4_list(1), 0.001);
    EXPECT_APPROX_EQUAL(0.0, randomize.f4_list(2), 0.001);
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x12\x34\x12\x34\x12\x34\x12\x34" "\x56\x78\x56\x78\x56\x78\x56\x78" "\x90\xab\x90\xab\x90\xab\x90\xab", 4 + (8 * 3));
    EXPECT_TRUE(randomize.randomize_f4_list());
    EXPECT_APPROX_EQUAL(-8.725025, randomize.f4_list(0), 0.001);
    EXPECT_APPROX_EQUAL(0.181642, randomize.f4_list(1), 0.001);
    EXPECT_APPROX_EQUAL(0.0, randomize.f4_list(2), 0.001);

    kpb_random_from_buffer("\x00\x00\x00\x00\x00\x00\x00\x00", 8);
    EXPECT_TRUE(randomize.randomize_f8());
    EXPECT_APPROX_EQUAL(-11.1, randomize.f8(), 0.001);
    kpb_random_from_buffer("\xff\xff\xff\xff\xff\xff\xff\xff", 8);
    EXPECT_TRUE(randomize.randomize_f8());
    EXPECT_APPROX_EQUAL(22.3, randomize.f8(), 0.001);
    kpb_random_from_buffer("\x7f\xff\xff\xff\xff\xff\xff\xff", 8);
    EXPECT_TRUE(randomize.randomize_f8());
    EXPECT_APPROX_EQUAL(5.6, randomize.f8(), 0.001);
    kpb_random_from_buffer("\x12\x34\x56\x78\x12\x34\x56\x78", 8);
    EXPECT_TRUE(randomize.randomize_f8());
    EXPECT_APPROX_EQUAL(-8.724890, randomize.f8(), 0.001);

    EXPECT_TRUE(randomize.set_count_min(0));
    EXPECT_TRUE(randomize.set_count_max(3));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00" "\x00\x00\x00\x00\x00\x00\x00\x00", 4 + (8 * 3));
    EXPECT_TRUE(randomize.randomize_f8_list());
    EXPECT_APPROX_EQUAL(-11.1, randomize.f8_list(0), 0.001);
    EXPECT_APPROX_EQUAL(-11.1, randomize.f8_list(1), 0.001);
    EXPECT_APPROX_EQUAL(0.0, randomize.f8_list(2), 0.001);
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xff\xff\xff\xff\xff\xff\xff\xff" "\xff\xff\xff\xff\xff\xff\xff\xff" "\xff\xff\xff\xff\xff\xff\xff\xff", 4 + (8 * 3));
    EXPECT_TRUE(randomize.randomize_f8_list());
    EXPECT_APPROX_EQUAL(22.3, randomize.f8_list(0), 0.001);
    EXPECT_APPROX_EQUAL(22.3, randomize.f8_list(1), 0.001);
    EXPECT_APPROX_EQUAL(0.0, randomize.f8_list(2), 0.001);
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x7f\xff\xff\xff\xff\xff\xff\xff" "\x7f\xff\xff\xff\xff\xff\xff\xff" "\x7f\xff\xff\xff\xff\xff\xff\xff", 4 + (8 * 3));
    EXPECT_TRUE(randomize.randomize_f8_list());
    EXPECT_APPROX_EQUAL(5.6, randomize.f8_list(0), 0.001);
    EXPECT_APPROX_EQUAL(5.6, randomize.f8_list(1), 0.001);
    EXPECT_APPROX_EQUAL(0.0, randomize.f8_list(2), 0.001);
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x12\x34\x12\x34\x12\x34\x12\x34" "\x56\x78\x56\x78\x56\x78\x56\x78" "\x90\xab\x90\xab\x90\xab\x90\xab", 4 + (8 * 3));
    EXPECT_TRUE(randomize.randomize_f8_list());
    EXPECT_APPROX_EQUAL(-8.725025, randomize.f8_list(0), 0.001);
    EXPECT_APPROX_EQUAL(0.181642, randomize.f8_list(1), 0.001);
    EXPECT_APPROX_EQUAL(0.0, randomize.f8_list(2), 0.001);

    kpb_random_from_buffer("\x00\x00\x00\x00" "", 4 + 0);
    EXPECT_TRUE(randomize.randomize_data());
    EXPECT_BYTES_EQUAL("", 0, randomize.data().c_str(), randomize.data().size());
    kpb_random_from_buffer("\x00\x00\x00\x04" "\x00\x00\x00\x00", 4 + 4);
    EXPECT_TRUE(randomize.randomize_data());
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, randomize.data().c_str(), randomize.data().size());
    kpb_random_from_buffer("\x00\x00\x00\x04" "\xff\xff\xff\xff", 4 + 4);
    EXPECT_TRUE(randomize.randomize_data());
    EXPECT_BYTES_EQUAL("\xff\xff\xff\xff", 4, randomize.data().c_str(), randomize.data().size());
    kpb_random_from_buffer("\x00\x00\x00\x04" "\x12\x34\x56\x78", 4 + 4);
    EXPECT_TRUE(randomize.randomize_data());
    EXPECT_BYTES_EQUAL("\x12\x34\x56\x78", 4, randomize.data().c_str(), randomize.data().size());
    kpb_random_from_buffer("\x00\x00\x00\x02" "\xcc\xdd", 4 + 2);
    EXPECT_TRUE(randomize.randomize_data());
    EXPECT_BYTES_EQUAL("\xcc\xdd", 2, randomize.data().c_str(), randomize.data().size());

    EXPECT_TRUE(randomize.set_count_min(0));
    EXPECT_TRUE(randomize.set_count_max(3));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "" "\x00\x00\x00\x04" "\x00\x00\x00\x00" "\x00\x00\x00\x03" "\x00\x00\x00", 4 + (4 + 0) + (4 + 4) + (4 + 3));
    EXPECT_TRUE(randomize.randomize_data_list());
    EXPECT_BYTES_EQUAL("", 0, randomize.data_list(0).c_str(), randomize.data_list(0).size());
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, randomize.data_list(1).c_str(), randomize.data_list(1).size());
    EXPECT_BYTES_EQUAL("", 0, randomize.data_list(2).c_str(), randomize.data_list(2).size());
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "" "\x00\x00\x00\x04" "\xff\xff\xff\xff" "\x00\x00\x00\x03" "\xff\xff\xff", 4 + (4 + 0) + (4 + 4) + (4 + 3));
    EXPECT_TRUE(randomize.randomize_data_list());
    EXPECT_BYTES_EQUAL("", 0, randomize.data_list(0).c_str(), randomize.data_list(0).size());
    EXPECT_BYTES_EQUAL("\xff\xff\xff\xff", 4, randomize.data_list(1).c_str(), randomize.data_list(1).size());
    EXPECT_BYTES_EQUAL("", 0, randomize.data_list(2).c_str(), randomize.data_list(2).size());
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x04" "\x12\x34\x12\x34" "\x00\x00\x00\x04" "\x56\x78\x56\x78" "\x00\x00\x00\x04" "\x90\xab\x90\xab", 4 + (4 + 4) + (4 + 4) + (4 + 4));
    EXPECT_TRUE(randomize.randomize_data_list());
    EXPECT_BYTES_EQUAL("\x12\x34\x12\x34", 4, randomize.data_list(0).c_str(), randomize.data_list(0).size());
    EXPECT_BYTES_EQUAL("\x56\x78\x56\x78", 4, randomize.data_list(1).c_str(), randomize.data_list(1).size());
    EXPECT_BYTES_EQUAL("", 0, randomize.data_list(2).c_str(), randomize.data_list(2).size());

    kpb_random_from_buffer("\x00\x00\x00\x00" "", 4 + 0);
    EXPECT_TRUE(randomize.randomize_ascii());
    EXPECT_STRING_EQUAL("", randomize.ascii().c_str());
    kpb_random_from_buffer("\x00\x00\x00\x04" "\x41\x41\x00\x41", 4 + 4);
    EXPECT_TRUE(randomize.randomize_ascii());
    EXPECT_STRING_EQUAL("AA", randomize.ascii().c_str());
    kpb_random_from_buffer("\x00\x00\x00\x04" "\x62\x62\x62\x62", 4 + 4);
    EXPECT_TRUE(randomize.randomize_ascii());
    EXPECT_STRING_EQUAL("bbbb", randomize.ascii().c_str());
    kpb_random_from_buffer("\x00\x00\x00\x04" "\x41\x42\x43\x44", 4 + 4);
    EXPECT_TRUE(randomize.randomize_ascii());
    EXPECT_STRING_EQUAL("ABCD", randomize.ascii().c_str());
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x61\x62", 4 + 2);
    EXPECT_TRUE(randomize.randomize_ascii());
    EXPECT_STRING_EQUAL("ab", randomize.ascii().c_str());

    EXPECT_TRUE(randomize.set_count_min(0));
    EXPECT_TRUE(randomize.set_count_max(3));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "" "\x00\x00\x00\x04" "\x41\x41\x00\x41" "\x00\x00\x00\x03" "\x42\x42\x42", 4 + (4 + 0) + (4 + 4) + (4 + 3));
    EXPECT_TRUE(randomize.randomize_ascii_list());
    EXPECT_STRING_EQUAL("", randomize.ascii_list(0).c_str());
    EXPECT_STRING_EQUAL("AA", randomize.ascii_list(1).c_str());
    EXPECT_STRING_EQUAL("", randomize.ascii_list(2).c_str());
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "" "\x00\x00\x00\x04" "\x62\x62\x62\x62" "\x00\x00\x00\x03" "\x63\x63\x63", 4 + (4 + 0) + (4 + 4) + (4 + 3));
    EXPECT_TRUE(randomize.randomize_ascii_list());
    EXPECT_STRING_EQUAL("", randomize.ascii_list(0).c_str());
    EXPECT_STRING_EQUAL("bbbb", randomize.ascii_list(1).c_str());
    EXPECT_STRING_EQUAL("", randomize.ascii_list(2).c_str());
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x04" "\x41\x42\x43\x44" "\x00\x00\x00\x04" "\x61\x62\x63\x64" "\x00\x00\x00\x04" "\x41\x42\x61\x62", 4 + (4 + 4) + (4 + 4) + (4 + 4));
    EXPECT_TRUE(randomize.randomize_ascii_list());
    EXPECT_STRING_EQUAL("ABCD", randomize.ascii_list(0).c_str());
    EXPECT_STRING_EQUAL("abcd", randomize.ascii_list(1).c_str());
    EXPECT_STRING_EQUAL("", randomize.ascii_list(2).c_str());

    kpb_random_from_buffer("\x00\x00\x00\x00" "", 4 + 0);
    EXPECT_TRUE(randomize.randomize_utf16le());
    EXPECT_STRING_EQUAL(L"", randomize.utf16le().c_str());
    kpb_random_from_buffer("\x00\x00\x00\x08" "\x00\x41\x00\x41\x00\x00\x00\x41", 4 + 8);
    EXPECT_TRUE(randomize.randomize_utf16le());
    EXPECT_STRING_EQUAL(L"AA", randomize.utf16le().c_str());
    kpb_random_from_buffer("\x00\x00\x00\x08" "\x00\x62\x00\x62\x00\x62\x00\x62", 4 + 8);
    EXPECT_TRUE(randomize.randomize_utf16le());
    EXPECT_STRING_EQUAL(L"bbbb", randomize.utf16le().c_str());
    kpb_random_from_buffer("\x00\x00\x00\x08" "\x00\x41\x00\x42\x00\x43\x00\x44", 4 + 8);
    EXPECT_TRUE(randomize.randomize_utf16le());
    EXPECT_STRING_EQUAL(L"ABCD", randomize.utf16le().c_str());
    kpb_random_from_buffer("\x00\x00\x00\x04" "\x00\x61\x00\x62", 4 + 4);
    EXPECT_TRUE(randomize.randomize_utf16le());
    EXPECT_STRING_EQUAL(L"ab", randomize.utf16le().c_str());

    EXPECT_TRUE(randomize.set_count_min(0));
    EXPECT_TRUE(randomize.set_count_max(3));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "" "\x00\x00\x00\x08" "\x00\x41\x00\x41\x00\x00\x00\x41" "\x00\x00\x00\x06" "\x00\x42\x00\x42\x00\x42", 4 + (4 + 0) + (4 + 8) + (4 + 6));
    EXPECT_TRUE(randomize.randomize_utf16le_list());
    EXPECT_STRING_EQUAL(L"", randomize.utf16le_list(0).c_str());
    EXPECT_STRING_EQUAL(L"AA", randomize.utf16le_list(1).c_str());
    EXPECT_STRING_EQUAL(L"", randomize.utf16le_list(2).c_str());
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "" "\x00\x00\x00\x08" "\x00\x62\x00\x62\x00\x62\x00\x62" "\x00\x00\x00\x06" "\x00\x63\x00\x63\x00\x63", 4 + (4 + 0) + (4 + 8) + (4 + 6));
    EXPECT_TRUE(randomize.randomize_utf16le_list());
    EXPECT_STRING_EQUAL(L"", randomize.utf16le_list(0).c_str());
    EXPECT_STRING_EQUAL(L"bbbb", randomize.utf16le_list(1).c_str());
    EXPECT_STRING_EQUAL(L"", randomize.utf16le_list(2).c_str());
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x08" "\x00\x41\x00\x42\x00\x43\x00\x44" "\x00\x00\x00\x08" "\x00\x61\x00\x62\x00\x63\x00\x64" "\x00\x00\x00\x08" "\x00\x41\x00\x42\x00\x61\x00\x62", 4 + (4 + 8) + (4 + 8) + (4 + 8));
    EXPECT_TRUE(randomize.randomize_utf16le_list());
    EXPECT_STRING_EQUAL(L"ABCD", randomize.utf16le_list(0).c_str());
    EXPECT_STRING_EQUAL(L"abcd", randomize.utf16le_list(1).c_str());
    EXPECT_STRING_EQUAL(L"", randomize.utf16le_list(2).c_str());

    kpb_random_from_buffer("\x00\x00\x00\x00" "", 4 + 0);
    EXPECT_TRUE(randomize.randomize_utf16be());
    EXPECT_STRING_EQUAL(L"", randomize.utf16be().c_str());
    kpb_random_from_buffer("\x00\x00\x00\x08" "\x00\x41\x00\x41\x00\x00\x00\x41", 4 + 8);
    EXPECT_TRUE(randomize.randomize_utf16be());
    EXPECT_STRING_EQUAL(L"AA", randomize.utf16be().c_str());
    kpb_random_from_buffer("\x00\x00\x00\x08" "\x00\x62\x00\x62\x00\x62\x00\x62", 4 + 8);
    EXPECT_TRUE(randomize.randomize_utf16be());
    EXPECT_STRING_EQUAL(L"bbbb", randomize.utf16be().c_str());
    kpb_random_from_buffer("\x00\x00\x00\x08" "\x00\x41\x00\x42\x00\x43\x00\x44", 4 + 8);
    EXPECT_TRUE(randomize.randomize_utf16be());
    EXPECT_STRING_EQUAL(L"ABCD", randomize.utf16be().c_str());
    kpb_random_from_buffer("\x00\x00\x00\x04" "\x00\x61\x00\x62", 4 + 4);
    EXPECT_TRUE(randomize.randomize_utf16be());
    EXPECT_STRING_EQUAL(L"ab", randomize.utf16be().c_str());

    EXPECT_TRUE(randomize.set_count_min(0));
    EXPECT_TRUE(randomize.set_count_max(3));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "" "\x00\x00\x00\x08" "\x00\x41\x00\x41\x00\x00\x00\x41" "\x00\x00\x00\x06" "\x00\x42\x00\x42\x00\x42", 4 + (4 + 0) + (4 + 8) + (4 + 6));
    EXPECT_TRUE(randomize.randomize_utf16be_list());
    EXPECT_STRING_EQUAL(L"", randomize.utf16be_list(0).c_str());
    EXPECT_STRING_EQUAL(L"AA", randomize.utf16be_list(1).c_str());
    EXPECT_STRING_EQUAL(L"", randomize.utf16be_list(2).c_str());
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "" "\x00\x00\x00\x08" "\x00\x62\x00\x62\x00\x62\x00\x62" "\x00\x00\x00\x06" "\x00\x63\x00\x63\x00\x63", 4 + (4 + 0) + (4 + 8) + (4 + 6));
    EXPECT_TRUE(randomize.randomize_utf16be_list());
    EXPECT_STRING_EQUAL(L"", randomize.utf16be_list(0).c_str());
    EXPECT_STRING_EQUAL(L"bbbb", randomize.utf16be_list(1).c_str());
    EXPECT_STRING_EQUAL(L"", randomize.utf16be_list(2).c_str());
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x08" "\x00\x41\x00\x42\x00\x43\x00\x44" "\x00\x00\x00\x08" "\x00\x61\x00\x62\x00\x63\x00\x64" "\x00\x00\x00\x08" "\x00\x41\x00\x42\x00\x61\x00\x62", 4 + (4 + 8) + (4 + 8) + (4 + 8));
    EXPECT_TRUE(randomize.randomize_utf16be_list());
    EXPECT_STRING_EQUAL(L"ABCD", randomize.utf16be_list(0).c_str());
    EXPECT_STRING_EQUAL(L"abcd", randomize.utf16be_list(1).c_str());
    EXPECT_STRING_EQUAL(L"", randomize.utf16be_list(2).c_str());

    kpb_random_from_buffer("\x00\x00\x00\x00" "", 4 + 0);
    EXPECT_TRUE(randomize.randomize_utf8());
    EXPECT_STRING_EQUAL("", randomize.utf8().c_str());
    kpb_random_from_buffer("\x00\x00\x00\x04" "\x41\x41\x00\x41", 4 + 4);
    EXPECT_TRUE(randomize.randomize_utf8());
    EXPECT_STRING_EQUAL("AA", randomize.utf8().c_str());
    kpb_random_from_buffer("\x00\x00\x00\x04" "\x62\x62\x62\x62", 4 + 4);
    EXPECT_TRUE(randomize.randomize_utf8());
    EXPECT_STRING_EQUAL("bbbb", randomize.utf8().c_str());
    kpb_random_from_buffer("\x00\x00\x00\x04" "\x41\x42\x43\x44", 4 + 4);
    EXPECT_TRUE(randomize.randomize_utf8());
    EXPECT_STRING_EQUAL("ABCD", randomize.utf8().c_str());
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x61\x62", 4 + 2);
    EXPECT_TRUE(randomize.randomize_utf8());
    EXPECT_STRING_EQUAL("ab", randomize.utf8().c_str());

    EXPECT_TRUE(randomize.set_count_min(0));
    EXPECT_TRUE(randomize.set_count_max(3));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "" "\x00\x00\x00\x04" "\x41\x41\x00\x41" "\x00\x00\x00\x03" "\x42\x42\x42", 4 + (4 + 0) + (4 + 4) + (4 + 3));
    EXPECT_TRUE(randomize.randomize_utf8_list());
    EXPECT_STRING_EQUAL("", randomize.utf8_list(0).c_str());
    EXPECT_STRING_EQUAL("AA", randomize.utf8_list(1).c_str());
    EXPECT_STRING_EQUAL("", randomize.utf8_list(2).c_str());
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x00" "" "\x00\x00\x00\x04" "\x62\x62\x62\x62" "\x00\x00\x00\x03" "\x63\x63\x63", 4 + (4 + 0) + (4 + 4) + (4 + 3));
    EXPECT_TRUE(randomize.randomize_utf8_list());
    EXPECT_STRING_EQUAL("", randomize.utf8_list(0).c_str());
    EXPECT_STRING_EQUAL("bbbb", randomize.utf8_list(1).c_str());
    EXPECT_STRING_EQUAL("", randomize.utf8_list(2).c_str());
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x04" "\x41\x42\x43\x44" "\x00\x00\x00\x04" "\x61\x62\x63\x64" "\x00\x00\x00\x04" "\x41\x42\x61\x62", 4 + (4 + 4) + (4 + 4) + (4 + 4));
    EXPECT_TRUE(randomize.randomize_utf8_list());
    EXPECT_STRING_EQUAL("ABCD", randomize.utf8_list(0).c_str());
    EXPECT_STRING_EQUAL("abcd", randomize.utf8_list(1).c_str());
    EXPECT_STRING_EQUAL("", randomize.utf8_list(2).c_str());

    kpb_random_from_buffer("\x00\x00\x00\x03" "\x00\x00\x00\x02" "\xde\xaf", 4 + (4 + 2));
    EXPECT_TRUE(randomize.randomize_object());
    EXPECT_BYTES_EQUAL("\xde\xaf\x00", 3, randomize.object().data().c_str(), randomize.object().data().size());

    EXPECT_TRUE(randomize.set_count_min(0));
    EXPECT_TRUE(randomize.set_count_max(3));
    kpb_random_from_buffer("\x00\x00\x00\x02" "\x00\x00\x00\x03" "\x00\x00\x00\x01" "\xde\xaf" "\x00\x00\x00\x03" "\x00\x00\x00\x02" "\xca\xbe\x12\x34", 4 + (4 + (4 + 2)) + (4 + (4 + 4)));
    EXPECT_TRUE(randomize.randomize_object_list());
    EXPECT_EQUAL(2, randomize.object_list(0).data_size_());
    EXPECT_BYTES_EQUAL("\xde\xaf", 2, randomize.object_list(0).data().c_str(), randomize.object_list(0).data().size());
    EXPECT_EQUAL(3, randomize.object_list(1).data_size_());
    EXPECT_BYTES_EQUAL("\xca\xbe\x12", 3, randomize.object_list(1).data().c_str(), randomize.object_list(1).data().size());
    EXPECT_EQUAL(0, randomize.object_list(2).data_size_());
    EXPECT_BYTES_EQUAL("", 0, randomize.object_list(2).data().c_str(), randomize.object_list(2).data().size());

    kpb_random_os();
    randomize.Clear();
    EXPECT_TRUE(randomize.set_count_min(1));
    EXPECT_TRUE(randomize.set_count_max(3));
    EXPECT_TRUE(randomize.set_s1_min(~((int8_t)0x7e)));
    EXPECT_TRUE(randomize.set_s1_max(0x7e));
    EXPECT_TRUE(randomize.set_s2_min(~((int16_t)0x7ffe)));
    EXPECT_TRUE(randomize.set_s2_max(0x7ffe));
    EXPECT_TRUE(randomize.set_s4_min(~((int32_t)0x7ffffffe)));
    EXPECT_TRUE(randomize.set_s4_max(0x7ffffffe));
    EXPECT_TRUE(randomize.set_s8_min(~((int64_t)0x7ffffffffffffffe)));
    EXPECT_TRUE(randomize.set_s8_max(0x7ffffffffffffffe));
    EXPECT_TRUE(randomize.set_u1_min(1));
    EXPECT_TRUE(randomize.set_u1_max(0xfe));
    EXPECT_TRUE(randomize.set_u2_min(1));
    EXPECT_TRUE(randomize.set_u2_max(0xfffe));
    EXPECT_TRUE(randomize.set_u4_min(1));
    EXPECT_TRUE(randomize.set_u4_max(0xfffffffe));
    EXPECT_TRUE(randomize.set_u8_min(1));
    EXPECT_TRUE(randomize.set_u8_max(0xfffffffffffffffe));
    for ( size_t i = 0; i < 10; i++ )
    {
        // randomize
        EXPECT_TRUE(randomize.Randomize());

        // serialize
        EXPECT_TRUE(randomize.SerializeToString(&result));
    }

    return 0;
}

} // test
} // kpb
