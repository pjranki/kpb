#include "test_basic_list.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/basic_list.hpp"

namespace kpb {
namespace test {

int basic_list(void)
{
    kpb_test::BasicList basic_list;
    std::string result;
    EXPECT_TRUE(basic_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 12, result.c_str(), result.size());
    EXPECT_EQUAL(basic_list.bitfield_size(), 0);
    EXPECT_EQUAL(basic_list.bitfield().size(), 0);
    EXPECT_EQUAL(basic_list.numbers_size(), 0);
    EXPECT_EQUAL(basic_list.numbers().size(), 0);

    EXPECT_TRUE(basic_list.add_bitfield(0x7));
    EXPECT_TRUE(basic_list.add_numbers(0xffff));
    EXPECT_TRUE(basic_list.add_numbers(0xffff));
    EXPECT_TRUE(basic_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xe0\x00\xff\xff\xff\xff\x00\x00\x00\x00\x00\x00", 12, result.c_str(), result.size());
    EXPECT_EQUAL(basic_list.bitfield_size(), 1);
    EXPECT_EQUAL(basic_list.bitfield().size(), 1);
    EXPECT_EQUAL(basic_list.numbers_size(), 2);
    EXPECT_EQUAL(basic_list.numbers().size(), 2);

    basic_list.Clear();
    EXPECT_EQUAL(basic_list.bitfield_size(), 0);
    EXPECT_EQUAL(basic_list.bitfield().size(), 0);
    EXPECT_EQUAL(basic_list.numbers_size(), 0);
    EXPECT_EQUAL(basic_list.numbers().size(), 0);

    basic_list.Clear();
    EXPECT_TRUE(basic_list.add_bitfield(0x7));
    EXPECT_TRUE(basic_list.add_bitfield(0x7));
    EXPECT_TRUE(basic_list.add_bitfield(0x7));
    EXPECT_TRUE(basic_list.add_bitfield(0x7));
    EXPECT_TRUE(basic_list.add_numbers(0xffff));
    EXPECT_TRUE(basic_list.add_numbers(0xffff));
    EXPECT_TRUE(basic_list.add_numbers(0xffff));
    EXPECT_TRUE(basic_list.add_numbers(0xffff));
    EXPECT_TRUE(basic_list.add_numbers(0xffff));
    EXPECT_TRUE(basic_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xff\xf0\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff", 12, result.c_str(), result.size());
    EXPECT_EQUAL(basic_list.bitfield_size(), 4);
    EXPECT_EQUAL(basic_list.bitfield().size(), 4);
    EXPECT_EQUAL(basic_list.numbers_size(), 5);
    EXPECT_EQUAL(basic_list.numbers().size(), 5);

    basic_list.Clear();
    EXPECT_TRUE(basic_list.add_bitfield(0x7));
    EXPECT_TRUE(basic_list.add_bitfield(0x7));
    EXPECT_TRUE(basic_list.add_bitfield(0x7));
    EXPECT_TRUE(basic_list.add_bitfield(0x7));
    EXPECT_TRUE(basic_list.add_bitfield(0x7));
    EXPECT_TRUE(basic_list.add_bitfield(0x7));
    EXPECT_TRUE(basic_list.add_bitfield(0x7));
    EXPECT_TRUE(basic_list.add_bitfield(0x7));
    EXPECT_TRUE(basic_list.add_numbers(0xffff));
    EXPECT_TRUE(basic_list.add_numbers(0xffff));
    EXPECT_TRUE(basic_list.add_numbers(0xffff));
    EXPECT_TRUE(basic_list.add_numbers(0xffff));
    EXPECT_TRUE(basic_list.add_numbers(0xffff));
    EXPECT_TRUE(basic_list.add_numbers(0xffff));
    EXPECT_TRUE(basic_list.add_numbers(0xffff));
    EXPECT_TRUE(basic_list.add_numbers(0xffff));
    EXPECT_TRUE(basic_list.add_numbers(0xffff));
    EXPECT_TRUE(basic_list.add_numbers(0xffff));
    EXPECT_TRUE(basic_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xff\xf0\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff", 12, result.c_str(), result.size());
    EXPECT_EQUAL(basic_list.bitfield_size(), 8);
    EXPECT_EQUAL(basic_list.bitfield().size(), 8);
    EXPECT_EQUAL(basic_list.numbers_size(), 10);
    EXPECT_EQUAL(basic_list.numbers().size(), 10);

    basic_list.Clear();
    EXPECT_TRUE(basic_list.add_bitfield(0x5));
    EXPECT_TRUE(basic_list.add_bitfield(0x2));
    EXPECT_TRUE(basic_list.add_bitfield(0x5));
    EXPECT_TRUE(basic_list.add_bitfield(0x2));
    EXPECT_TRUE(basic_list.add_numbers(0xabcd));
    EXPECT_TRUE(basic_list.add_numbers(0xef01));
    EXPECT_TRUE(basic_list.add_numbers(0x2345));
    EXPECT_TRUE(basic_list.add_numbers(0x6789));
    EXPECT_TRUE(basic_list.add_numbers(0xfeba));
    EXPECT_TRUE(basic_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xaa\xa0\xcd\xab\x01\xef\x45\x23\x89\x67\xba\xfe", 12, result.c_str(), result.size());
    EXPECT_EQUAL(basic_list.bitfield_size(), 4);
    EXPECT_EQUAL(basic_list.bitfield().size(), 4);
    EXPECT_EQUAL(basic_list.numbers_size(), 5);
    EXPECT_EQUAL(basic_list.numbers().size(), 5);

    basic_list.Clear();
    EXPECT_TRUE(basic_list.ParseFromString(std::string("\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff", 12)));
    EXPECT_EQUAL(basic_list.bitfield_size(), 4);
    EXPECT_EQUAL(basic_list.bitfield().size(), 4);
    EXPECT_EQUAL(basic_list.numbers_size(), 5);
    EXPECT_EQUAL(basic_list.numbers().size(), 5);
    EXPECT_EQUAL(basic_list.bitfield()[0], 0x7);
    EXPECT_EQUAL(basic_list.bitfield()[1], 0x7);
    EXPECT_EQUAL(basic_list.bitfield()[2], 0x7);
    EXPECT_EQUAL(basic_list.bitfield()[3], 0x7);
    EXPECT_EQUAL(basic_list.numbers()[0], 0xffff);
    EXPECT_EQUAL(basic_list.numbers()[1], 0xffff);
    EXPECT_EQUAL(basic_list.numbers()[2], 0xffff);
    EXPECT_EQUAL(basic_list.numbers()[3], 0xffff);
    EXPECT_EQUAL(basic_list.numbers()[4], 0xffff);
    EXPECT_TRUE(basic_list.ParseFromString(std::string("\x00\x0f\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 12)));
    EXPECT_EQUAL(basic_list.bitfield_size(), 4);
    EXPECT_EQUAL(basic_list.bitfield().size(), 4);
    EXPECT_EQUAL(basic_list.numbers_size(), 5);
    EXPECT_EQUAL(basic_list.numbers().size(), 5);
    EXPECT_EQUAL(basic_list.bitfield()[0], 0);
    EXPECT_EQUAL(basic_list.bitfield()[1], 0);
    EXPECT_EQUAL(basic_list.bitfield()[2], 0);
    EXPECT_EQUAL(basic_list.bitfield()[3], 0);
    EXPECT_EQUAL(basic_list.numbers()[0], 0);
    EXPECT_EQUAL(basic_list.numbers()[1], 0);
    EXPECT_EQUAL(basic_list.numbers()[2], 0);
    EXPECT_EQUAL(basic_list.numbers()[3], 0);
    EXPECT_EQUAL(basic_list.numbers()[4], 0);
    EXPECT_TRUE(basic_list.ParseFromString(std::string("\xaa\xaf\xcd\xab\x01\xef\x45\x23\x89\x67\xba\xfe", 12)));
    EXPECT_EQUAL(basic_list.bitfield_size(), 4);
    EXPECT_EQUAL(basic_list.bitfield().size(), 4);
    EXPECT_EQUAL(basic_list.numbers_size(), 5);
    EXPECT_EQUAL(basic_list.numbers().size(), 5);
    EXPECT_EQUAL(basic_list.bitfield()[0], 0x5);
    EXPECT_EQUAL(basic_list.bitfield()[1], 0x2);
    EXPECT_EQUAL(basic_list.bitfield()[2], 0x5);
    EXPECT_EQUAL(basic_list.bitfield()[3], 0x2);
    EXPECT_EQUAL(basic_list.numbers()[0], 0xabcd);
    EXPECT_EQUAL(basic_list.numbers()[1], 0xef01);
    EXPECT_EQUAL(basic_list.numbers()[2], 0x2345);
    EXPECT_EQUAL(basic_list.numbers()[3], 0x6789);
    EXPECT_EQUAL(basic_list.numbers()[4], 0xfeba);
    return 0;
}

} // test
} // kpb
