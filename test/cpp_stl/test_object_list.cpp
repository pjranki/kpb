#include "test_object_list.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/object_list.hpp"

namespace kpb {
namespace test {

int object_list(void)
{
    kpb_test::ObjectList object_list;
    kpb_test::ObjectItem *object_item;
    std::string result;
    EXPECT_EQUAL(0, object_list.list_size());
    EXPECT_EQUAL(0, object_list.list().size());
    EXPECT_TRUE(object_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 12, result.c_str(), result.size());
    object_item = object_list.add_list();
    EXPECT_NOT_NULL(object_item);
    object_item->set_value(0x12345678);
    EXPECT_EQUAL(1, object_list.list_size());
    EXPECT_EQUAL(1, object_list.list().size());
    EXPECT_EQUAL(0x12345678, object_list.list()[0].get().value());
    EXPECT_TRUE(object_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x78\x56\x34\x12\x00\x00\x00\x00\x00\x00\x00\x00", 12, result.c_str(), result.size());
    object_item = object_list.add_list();
    EXPECT_NOT_NULL(object_item);
    object_item->set_value(0xabcdef89);
    EXPECT_EQUAL(2, object_list.list_size());
    EXPECT_EQUAL(2, object_list.list().size());
    EXPECT_EQUAL(0x12345678, object_list.list()[0].get().value());
    EXPECT_EQUAL(0xabcdef89, object_list.list()[1].get().value());
    EXPECT_TRUE(object_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x78\x56\x34\x12\x89\xef\xcd\xab\x00\x00\x00\x00", 12, result.c_str(), result.size());
    object_item = object_list.add_list();
    EXPECT_NOT_NULL(object_item);
    object_item->set_value(0xa1a2a3a4);
    EXPECT_EQUAL(3, object_list.list_size());
    EXPECT_EQUAL(3, object_list.list().size());
    EXPECT_EQUAL(0x12345678, object_list.list()[0].get().value());
    EXPECT_EQUAL(0xabcdef89, object_list.list()[1].get().value());
    EXPECT_EQUAL(0xa1a2a3a4, object_list.list()[2].get().value());
    EXPECT_TRUE(object_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x78\x56\x34\x12\x89\xef\xcd\xab\xa4\xa3\xa2\xa1", 12, result.c_str(), result.size());
    object_item = object_list.add_list();
    EXPECT_NOT_NULL(object_item);
    object_item->set_value(0x52535455);
    EXPECT_EQUAL(4, object_list.list_size());
    EXPECT_EQUAL(4, object_list.list().size());
    EXPECT_EQUAL(0x12345678, object_list.list()[0].get().value());
    EXPECT_EQUAL(0xabcdef89, object_list.list()[1].get().value());
    EXPECT_EQUAL(0xa1a2a3a4, object_list.list()[2].get().value());
    EXPECT_EQUAL(0x52535455, object_list.list()[3].get().value());
    EXPECT_TRUE(object_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x78\x56\x34\x12\x89\xef\xcd\xab\xa4\xa3\xa2\xa1", 12, result.c_str(), result.size());
    object_list.Clear();
    EXPECT_EQUAL(0, object_list.list_size());
    EXPECT_EQUAL(0, object_list.list().size());
    EXPECT_TRUE(object_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 12, result.c_str(), result.size());

    object_list.Clear();
    EXPECT_TRUE(object_list.ParseFromString(std::string("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 12)));
    EXPECT_EQUAL(3, object_list.list_size());
    EXPECT_EQUAL(3, object_list.list().size());
    EXPECT_EQUAL(0x00000000, object_list.list()[0].get().value());
    EXPECT_EQUAL(0x00000000, object_list.list()[1].get().value());
    EXPECT_EQUAL(0x00000000, object_list.list()[2].get().value());
    EXPECT_TRUE(object_list.ParseFromString(std::string("\x78\x56\x34\x12\x00\x00\x00\x00\x00\x00\x00\x00", 12)));
    EXPECT_EQUAL(3, object_list.list_size());
    EXPECT_EQUAL(3, object_list.list().size());
    EXPECT_EQUAL(0x12345678, object_list.list()[0].get().value());
    EXPECT_EQUAL(0x00000000, object_list.list()[1].get().value());
    EXPECT_EQUAL(0x00000000, object_list.list()[2].get().value());
    EXPECT_TRUE(object_list.ParseFromString(std::string("\x78\x56\x34\x12\x89\xef\xcd\xab\x00\x00\x00\x00", 12)));
    EXPECT_EQUAL(3, object_list.list_size());
    EXPECT_EQUAL(3, object_list.list().size());
    EXPECT_EQUAL(0x12345678, object_list.list()[0].get().value());
    EXPECT_EQUAL(0xabcdef89, object_list.list()[1].get().value());
    EXPECT_EQUAL(0x00000000, object_list.list()[2].get().value());
    EXPECT_TRUE(object_list.ParseFromString(std::string("\x78\x56\x34\x12\x89\xef\xcd\xab\xa4\xa3\xa2\xa1", 12)));
    EXPECT_EQUAL(3, object_list.list_size());
    EXPECT_EQUAL(3, object_list.list().size());
    EXPECT_EQUAL(0x12345678, object_list.list()[0].get().value());
    EXPECT_EQUAL(0xabcdef89, object_list.list()[1].get().value());
    EXPECT_EQUAL(0xa1a2a3a4, object_list.list()[2].get().value());

    return 0;
}

} // test
} // kpb
