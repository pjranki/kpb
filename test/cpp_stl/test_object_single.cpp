#include "test_object_single.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/object_single.hpp"

namespace kpb {
namespace test {

int object_single(void)
{
    kpb_test::ObjectSingle object_single;
    kpb_test::SubObject *sub_object;
    std::string result;
    EXPECT_EQUAL(0x00000000, object_single.other().value());
    EXPECT_TRUE(object_single.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, result.c_str(), result.size());
    sub_object = object_single.mutable_other();
    EXPECT_NOT_NULL(sub_object);
    sub_object->set_value(0x12345678);
    EXPECT_EQUAL(0x12345678, sub_object->value());
    EXPECT_EQUAL(0x12345678, object_single.other().value());
    EXPECT_TRUE(object_single.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x78\x56\x34\x12", 4, result.c_str(), result.size());
    object_single.Clear();
    EXPECT_EQUAL(0x00000000, object_single.other().value());
    EXPECT_TRUE(object_single.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, result.c_str(), result.size());

    EXPECT_TRUE(object_single.ParseFromString(std::string("\xab\xcd\xef\x89", 4)));
    sub_object = object_single.mutable_other();
    EXPECT_EQUAL(0x89efcdab, sub_object->value());
    EXPECT_EQUAL(0x89efcdab, object_single.other().value());
    EXPECT_TRUE(object_single.ParseFromString(std::string("\x00\x00\x00\x00", 4)));
    sub_object = object_single.mutable_other();
    EXPECT_EQUAL(0x00000000, sub_object->value());
    EXPECT_EQUAL(0x00000000, object_single.other().value());

    return 0;
}

} // test
} // kpb
