#include "test_packet.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/packet.hpp"

#define TEST_EMPTY_PACKET           "\x0a\x00\x00\x00" "\x02\x00\x00\x00" "\x00\x00"
#define TEST_EMPTY_PACKET_SIZE      (sizeof(TEST_EMPTY_PACKET)-1)

#define TEST_DATA                   "\x41\x42\xff\x00\xcc"
#define TEST_DATA_SIZE              (sizeof(TEST_DATA)-1)

#define TEST_DATA_PACKET            "\x0f\x00\x00\x00" "\x07\x00\x00\x00" "\x05\x00" TEST_DATA
#define TEST_DATA_PACKET_SIZE       (sizeof(TEST_DATA_PACKET)-1)

namespace kpb {
namespace test {

int packet(void)
{
    std::string result;
    kpb_test::Packet packet;

    // -default is used to create the default sizes for the packet when empty
    EXPECT_EQUAL(8, packet.header().header_size_());
    EXPECT_EQUAL(2, packet.header().body_size_());
    EXPECT_EQUAL(10, packet.header().total_size_());

    // serialize as expected
    EXPECT_TRUE(packet.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(TEST_EMPTY_PACKET, TEST_EMPTY_PACKET_SIZE, result.c_str(), result.size());

    // calling Clear() calls Update() -> and should calculate the same values as -default
    packet.Clear();
    EXPECT_EQUAL(8, packet.header().header_size_());
    EXPECT_EQUAL(2, packet.header().body_size_());
    EXPECT_EQUAL(10, packet.header().total_size_());

    // serialize as expected
    EXPECT_TRUE(packet.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(TEST_EMPTY_PACKET, TEST_EMPTY_PACKET_SIZE, result.c_str(), result.size());

    // adding some data will update all size fields
    kpb_test::PacketBody *body = packet.mutable_body();
    EXPECT_NOT_NULL(body);
    EXPECT_TRUE(body->set_data(std::string(TEST_DATA, TEST_DATA_SIZE)));
    EXPECT_EQUAL(8, packet.header().header_size_());
    EXPECT_EQUAL(7, packet.header().body_size_());
    EXPECT_EQUAL(15, packet.header().total_size_());

    // serialize as expected
    EXPECT_TRUE(packet.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(TEST_DATA_PACKET, TEST_DATA_PACKET_SIZE, result.c_str(), result.size());

    // clear
    packet.Clear();

    // serialize as expected
    EXPECT_TRUE(packet.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(TEST_EMPTY_PACKET, TEST_EMPTY_PACKET_SIZE, result.c_str(), result.size());

    // parse as expected
    EXPECT_TRUE(packet.ParseFromString(std::string(TEST_DATA_PACKET, TEST_DATA_PACKET_SIZE)));
    EXPECT_EQUAL(8, packet.header().header_size_());
    EXPECT_EQUAL(7, packet.header().body_size_());
    EXPECT_EQUAL(15, packet.header().total_size_());
    EXPECT_BYTES_EQUAL(TEST_DATA, TEST_DATA_SIZE, packet.body().data().c_str(), packet.body().data().size());

    return 0;
}

} // test
} // kpb
