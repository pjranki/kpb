#include "process_value_strz.hpp"

#include <string.h>

ProcessValueStrz::ProcessValueStrz(uint8_t memset_value)
{
    memset_value_ = memset_value;
}

std::string ProcessValueStrz::getter(const std::string &value)
{
    std::string new_value = value;
    memset(const_cast<char*>(new_value.c_str()), memset_value_, new_value.size());
    return new_value;
}

std::string ProcessValueStrz::setter(std::string &value)
{
    std::string new_value = value;
    memset(const_cast<char*>(new_value.c_str()), memset_value_, new_value.size());
    return new_value;
}
