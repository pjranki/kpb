#ifndef _H_KPB_TEST_OBJECT_ALIGN_H_
#define _H_KPB_TEST_OBJECT_ALIGN_H_

namespace kpb {
namespace test {

int object_align(void);

} // test
} // kpb

#endif
