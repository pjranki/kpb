#include "test_integers.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/integers_be.hpp"
#include "cpp_stl/integers_le.hpp"
#include "cpp_stl/integers.hpp"

namespace kpb {
namespace test {

#define BYTES_ZERO_30 "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
#define BYTES_ZERO_30_SIZE (sizeof(BYTES_ZERO_30)-1)
#define BYTES_ONES_30 "\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff"
#define BYTES_ONES_30_SIZE (sizeof(BYTES_ONES_30)-1)

int integers(void)
{
    // defaults
    kpb_test::IntegersLe integers;
    EXPECT_EQUAL(integers.integer_s1(), 0);
    EXPECT_EQUAL(integers.integer_u1(), 0);
    EXPECT_EQUAL(integers.integer_s2(), 0);
    EXPECT_EQUAL(integers.integer_u2(), 0);
    EXPECT_EQUAL(integers.integer_s4(), 0);
    EXPECT_EQUAL(integers.integer_u4(), 0);
    EXPECT_EQUAL(integers.integer_s8(), 0);
    EXPECT_EQUAL(integers.integer_u8(), 0);

    kpb_test::IntegersLe integers_zeros;
    std::string expected_zeros;
    EXPECT_TRUE(integers_zeros.SerializeToString(&expected_zeros));
    EXPECT_BYTES_EQUAL(expected_zeros.c_str(), expected_zeros.size(), BYTES_ZERO_30, BYTES_ZERO_30_SIZE);

    kpb_test::IntegersLe integers_ones;
    integers_ones.set_integer_s1(-1);
    integers_ones.set_integer_u1(0xff);
    integers_ones.set_integer_s2(-1);
    integers_ones.set_integer_u2(0xffff);
    integers_ones.set_integer_s4(-1);
    integers_ones.set_integer_u4(0xffffffff);
    integers_ones.set_integer_s8(-1);
    integers_ones.set_integer_u8(0xffffffffffffffff);
    EXPECT_EQUAL(integers_ones.integer_s1(), -1);
    EXPECT_EQUAL(integers_ones.integer_u1(), 0xff);
    EXPECT_EQUAL(integers_ones.integer_s2(), -1);
    EXPECT_EQUAL(integers_ones.integer_u2(), 0xffff);
    EXPECT_EQUAL(integers_ones.integer_s4(), -1);
    EXPECT_EQUAL(integers_ones.integer_u4(), 0xffffffff);
    EXPECT_EQUAL(integers_ones.integer_s8(), -1);
    EXPECT_EQUAL(integers_ones.integer_u8(), 0xffffffffffffffff);
    std::string expected_ones;
    EXPECT_TRUE(integers_ones.SerializeToString(&expected_ones));
    EXPECT_BYTES_EQUAL(expected_ones.c_str(), expected_ones.size(), BYTES_ONES_30, BYTES_ONES_30_SIZE);

    integers.Clear();
    EXPECT_EQUAL(integers.integer_s1(), 0);
    EXPECT_EQUAL(integers.integer_u1(), 0);
    EXPECT_EQUAL(integers.integer_s2(), 0);
    EXPECT_EQUAL(integers.integer_u2(), 0);
    EXPECT_EQUAL(integers.integer_s4(), 0);
    EXPECT_EQUAL(integers.integer_u4(), 0);
    EXPECT_EQUAL(integers.integer_s8(), 0);
    EXPECT_EQUAL(integers.integer_u8(), 0);
    EXPECT_TRUE(integers.ParseFromString(std::string(BYTES_ONES_30, BYTES_ONES_30_SIZE)));
    EXPECT_EQUAL(integers.integer_s1(), -1);
    EXPECT_EQUAL(integers.integer_u1(), 0xff);
    EXPECT_EQUAL(integers.integer_s2(), -1);
    EXPECT_EQUAL(integers.integer_u2(), 0xffff);
    EXPECT_EQUAL(integers.integer_s4(), -1);
    EXPECT_EQUAL(integers.integer_u4(), 0xffffffff);
    EXPECT_EQUAL(integers.integer_s8(), -1);
    EXPECT_EQUAL(integers.integer_u8(), 0xffffffffffffffff);
    EXPECT_TRUE(integers.ParseFromString(std::string(BYTES_ZERO_30, BYTES_ZERO_30_SIZE)));
    EXPECT_EQUAL(integers.integer_s1(), 0);
    EXPECT_EQUAL(integers.integer_u1(), 0);
    EXPECT_EQUAL(integers.integer_s2(), 0);
    EXPECT_EQUAL(integers.integer_u2(), 0);
    EXPECT_EQUAL(integers.integer_s4(), 0);
    EXPECT_EQUAL(integers.integer_u4(), 0);
    EXPECT_EQUAL(integers.integer_s8(), 0);
    EXPECT_EQUAL(integers.integer_u8(), 0);

    kpb_test::IntegersBe be;
    kpb_test::IntegersLe le;
    std::string expected_be;
    std::string expected_le;
    be.set_integer_s1(-54);
    le.set_integer_s1(-54);
    expected_be += std::string("\xca", 1);
    expected_le += std::string("\xca", 1);
    be.set_integer_u1(0xfe);
    le.set_integer_u1(0xfe);
    expected_be += std::string("\xfe", 1);
    expected_le += std::string("\xfe", 1);
    be.set_integer_s2(-293);
    le.set_integer_s2(-293);
    expected_be += std::string("\xfe\xdb", 2);
    expected_le += std::string("\xdb\xfe", 2);
    be.set_integer_u2(0xc234);
    le.set_integer_u2(0xc234);
    expected_be += std::string("\xc2\x34", 2);
    expected_le += std::string("\x34\xc2", 2);
    be.set_integer_s4(-2023406815);
    le.set_integer_s4(-2023406815);
    expected_be += std::string("\x87\x65\x43\x21", 4);
    expected_le += std::string("\x21\x43\x65\x87", 4);
    be.set_integer_u4(0x98765432);
    le.set_integer_u4(0x98765432);
    expected_be += std::string("\x98\x76\x54\x32", 4);
    expected_le += std::string("\x32\x54\x76\x98", 4);
    be.set_integer_s8(-8690466096900485649);
    le.set_integer_s8(-8690466096900485649);
    expected_be += std::string("\x87\x65\x43\x21\x01\xab\xcd\xef", 8);
    expected_le += std::string("\xef\xcd\xab\x01\x21\x43\x65\x87", 8);
    be.set_integer_u8(0xfedcba9876543210);
    le.set_integer_u8(0xfedcba9876543210);
    expected_be += std::string("\xfe\xdc\xba\x98\x76\x54\x32\x10", 8);
    expected_le += std::string("\x10\x32\x54\x76\x98\xba\xdc\xfe", 8);
    EXPECT_EQUAL(expected_be.size(), 30);
    EXPECT_EQUAL(expected_le.size(), 30);

    std::string result_be;
    std::string result_le;
    EXPECT_TRUE(be.SerializeToString(&result_be));
    EXPECT_TRUE(le.SerializeToString(&result_le));
    EXPECT_BYTES_EQUAL(result_be.c_str(), result_be.size(), expected_be.c_str(), expected_be.size());
    EXPECT_BYTES_EQUAL(result_le.c_str(), result_le.size(), expected_le.c_str(), expected_le.size());
    be.Clear();
    le.Clear();
    EXPECT_TRUE(be.ParseFromString(result_be));
    EXPECT_EQUAL(be.integer_s1(), -54);
    EXPECT_EQUAL(be.integer_u1(), 0xfe);
    EXPECT_EQUAL(be.integer_s2(), -293);
    EXPECT_EQUAL(be.integer_u2(), 0xc234);
    EXPECT_EQUAL(be.integer_s4(), -2023406815);
    EXPECT_EQUAL(be.integer_u4(), 0x98765432);
    EXPECT_EQUAL(be.integer_s8(), -8690466096900485649);
    EXPECT_EQUAL(be.integer_u8(), 0xfedcba9876543210);
    EXPECT_TRUE(le.ParseFromString(result_le));
    EXPECT_EQUAL(le.integer_s1(), -54);
    EXPECT_EQUAL(le.integer_u1(), 0xfe);
    EXPECT_EQUAL(le.integer_s2(), -293);
    EXPECT_EQUAL(le.integer_u2(), 0xc234);
    EXPECT_EQUAL(le.integer_s4(), -2023406815);
    EXPECT_EQUAL(le.integer_u4(), 0x98765432);
    EXPECT_EQUAL(le.integer_s8(), -8690466096900485649);
    EXPECT_EQUAL(le.integer_u8(), 0xfedcba9876543210);

    kpb_test::Integers intergers_mixed;
    intergers_mixed.set_integer_s2le(0x1234);
    intergers_mixed.set_integer_u2le(0x1234);
    intergers_mixed.set_integer_s4le(0x12345678);
    intergers_mixed.set_integer_u4le(0x12345678);
    intergers_mixed.set_integer_s8le(0x1234567890abcdef);
    intergers_mixed.set_integer_u8le(0x1234567890abcdef);
    intergers_mixed.set_integer_s2be(0x1234);
    intergers_mixed.set_integer_u2be(0x1234);
    intergers_mixed.set_integer_s4be(0x12345678);
    intergers_mixed.set_integer_u4be(0x12345678);
    intergers_mixed.set_integer_s8be(0x1234567890abcdef);
    intergers_mixed.set_integer_u8be(0x1234567890abcdef);
    std::string result;
    EXPECT_TRUE(intergers_mixed.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(result.c_str(), result.size(),
        "\x34\x12" "\x34\x12" \
        "\x78\x56\x34\x12" "\x78\x56\x34\x12" \
        "\xef\xcd\xab\x90\x78\x56\x34\x12" "\xef\xcd\xab\x90\x78\x56\x34\x12" \
        "\x12\x34" "\x12\x34" \
        "\x12\x34\x56\x78" "\x12\x34\x56\x78" \
        "\x12\x34\x56\x78\x90\xab\xcd\xef" "\x12\x34\x56\x78\x90\xab\xcd\xef",
        (2 + 4 + 8) * 4);
    intergers_mixed.Clear();
    EXPECT_TRUE(intergers_mixed.ParseFromString(std::string( \
        "\x34\x12" "\x34\x12" \
        "\x78\x56\x34\x12" "\x78\x56\x34\x12" \
        "\xef\xcd\xab\x90\x78\x56\x34\x12" "\xef\xcd\xab\x90\x78\x56\x34\x12" \
        "\x12\x34" "\x12\x34" \
        "\x12\x34\x56\x78" "\x12\x34\x56\x78" \
        "\x12\x34\x56\x78\x90\xab\xcd\xef" "\x12\x34\x56\x78\x90\xab\xcd\xef",
        (2 + 4 + 8) * 4)));
    EXPECT_EQUAL(0x1234, intergers_mixed.integer_s2le());
    EXPECT_EQUAL(0x1234, intergers_mixed.integer_u2le());
    EXPECT_EQUAL(0x12345678, intergers_mixed.integer_s4le());
    EXPECT_EQUAL(0x12345678, intergers_mixed.integer_u4le());
    EXPECT_EQUAL(0x1234567890abcdef, intergers_mixed.integer_s8le());
    EXPECT_EQUAL(0x1234567890abcdef, intergers_mixed.integer_u8le());
    EXPECT_EQUAL(0x1234, intergers_mixed.integer_s2be());
    EXPECT_EQUAL(0x1234, intergers_mixed.integer_u2be());
    EXPECT_EQUAL(0x12345678, intergers_mixed.integer_s4be());
    EXPECT_EQUAL(0x12345678, intergers_mixed.integer_u4be());
    EXPECT_EQUAL(0x1234567890abcdef, intergers_mixed.integer_s8be());
    EXPECT_EQUAL(0x1234567890abcdef, intergers_mixed.integer_u8be());

    return 0;
}

} // test
} // kpb
