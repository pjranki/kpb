#include "test_update_member.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/update_member.hpp"

namespace kpb {
namespace test {

int update_member(void)
{
    kpb_test::MemberUpdate root;
    kpb_test::MemberUpdateObj *obj = nullptr;
    kpb_test::MemberUpdateSubObj *sub = nullptr;

    EXPECT_EQUAL(0, root.key());
    obj = root.mutable_obj();
    EXPECT_NOT_NULL(obj);

    EXPECT_EQUAL(0, root.key());
    EXPECT_EQUAL(0, obj->key());
    EXPECT_EQUAL(false, obj->has_u1());
    EXPECT_EQUAL(false, obj->has_u2());
    EXPECT_EQUAL(false, obj->has_u4());
    EXPECT_EQUAL(false, obj->has_u8());
    EXPECT_EQUAL(false, obj->has_number());
    EXPECT_EQUAL(false, obj->has_obj());
    EXPECT_EQUAL(false, obj->has_obj_list());

    EXPECT_TRUE(obj->set_u1(1));
    EXPECT_EQUAL(kpb_test::MemberUpdate::Key::KEY_U1, root.key());
    EXPECT_EQUAL(kpb_test::MemberUpdate::Key::KEY_U1, obj->key());
    EXPECT_EQUAL(true, obj->has_u1());
    EXPECT_EQUAL(false, obj->has_u2());
    EXPECT_EQUAL(false, obj->has_u4());
    EXPECT_EQUAL(false, obj->has_u8());
    EXPECT_EQUAL(false, obj->has_number());
    EXPECT_EQUAL(false, obj->has_obj());
    EXPECT_EQUAL(false, obj->has_obj_list());

    EXPECT_TRUE(obj->set_u2(1));
    EXPECT_EQUAL(kpb_test::MemberUpdate::Key::KEY_U2, root.key());
    EXPECT_EQUAL(kpb_test::MemberUpdate::Key::KEY_U2, obj->key());
    EXPECT_EQUAL(false, obj->has_u1());
    EXPECT_EQUAL(true, obj->has_u2());
    EXPECT_EQUAL(false, obj->has_u4());
    EXPECT_EQUAL(false, obj->has_u8());
    EXPECT_EQUAL(false, obj->has_number());
    EXPECT_EQUAL(false, obj->has_obj());
    EXPECT_EQUAL(false, obj->has_obj_list());

    EXPECT_TRUE(obj->set_u4(1));
    EXPECT_EQUAL(kpb_test::MemberUpdate::Key::KEY_U4, root.key());
    EXPECT_EQUAL(kpb_test::MemberUpdate::Key::KEY_U4, obj->key());
    EXPECT_EQUAL(false, obj->has_u1());
    EXPECT_EQUAL(false, obj->has_u2());
    EXPECT_EQUAL(true, obj->has_u4());
    EXPECT_EQUAL(false, obj->has_u8());
    EXPECT_EQUAL(false, obj->has_number());
    EXPECT_EQUAL(false, obj->has_obj());
    EXPECT_EQUAL(false, obj->has_obj_list());

    EXPECT_TRUE(obj->set_u8(1));
    EXPECT_EQUAL(kpb_test::MemberUpdate::Key::KEY_U8, root.key());
    EXPECT_EQUAL(kpb_test::MemberUpdate::Key::KEY_U8, obj->key());
    EXPECT_EQUAL(false, obj->has_u1());
    EXPECT_EQUAL(false, obj->has_u2());
    EXPECT_EQUAL(false, obj->has_u4());
    EXPECT_EQUAL(true, obj->has_u8());
    EXPECT_EQUAL(false, obj->has_number());
    EXPECT_EQUAL(false, obj->has_obj());
    EXPECT_EQUAL(false, obj->has_obj_list());

    EXPECT_TRUE(obj->set_number(1));
    EXPECT_EQUAL(kpb_test::MemberUpdate::Key::KEY_NUMBER, root.key());
    EXPECT_EQUAL(kpb_test::MemberUpdate::Key::KEY_NUMBER, obj->key());
    EXPECT_EQUAL(false, obj->has_u1());
    EXPECT_EQUAL(false, obj->has_u2());
    EXPECT_EQUAL(false, obj->has_u4());
    EXPECT_EQUAL(false, obj->has_u8());
    EXPECT_EQUAL(true, obj->has_number());
    EXPECT_EQUAL(false, obj->has_obj());
    EXPECT_EQUAL(false, obj->has_obj_list());

    sub = obj->mutable_obj();
    EXPECT_NOT_NULL(sub);
    EXPECT_EQUAL(kpb_test::MemberUpdate::Key::KEY_OBJ, root.key());
    EXPECT_EQUAL(kpb_test::MemberUpdate::Key::KEY_OBJ, obj->key());
    EXPECT_EQUAL(false, obj->has_u1());
    EXPECT_EQUAL(false, obj->has_u2());
    EXPECT_EQUAL(false, obj->has_u4());
    EXPECT_EQUAL(false, obj->has_u8());
    EXPECT_EQUAL(false, obj->has_number());
    EXPECT_EQUAL(true, obj->has_obj());
    EXPECT_EQUAL(false, obj->has_obj_list());

    sub = obj->add_obj_list();
    EXPECT_NOT_NULL(sub);
    EXPECT_EQUAL(kpb_test::MemberUpdate::Key::KEY_OBJ_LIST, root.key());
    EXPECT_EQUAL(kpb_test::MemberUpdate::Key::KEY_OBJ_LIST, obj->key());
    EXPECT_EQUAL(false, obj->has_u1());
    EXPECT_EQUAL(false, obj->has_u2());
    EXPECT_EQUAL(false, obj->has_u4());
    EXPECT_EQUAL(false, obj->has_u8());
    EXPECT_EQUAL(false, obj->has_number());
    EXPECT_EQUAL(false, obj->has_obj());
    EXPECT_EQUAL(true, obj->has_obj_list());

    root.Clear();
    EXPECT_EQUAL(0, root.key());
    EXPECT_EQUAL(0, root.obj().key());
    EXPECT_EQUAL(false, root.obj().has_u1());
    EXPECT_EQUAL(false, root.obj().has_u2());
    EXPECT_EQUAL(false, root.obj().has_u4());
    EXPECT_EQUAL(false, root.obj().has_u8());
    EXPECT_EQUAL(false, root.obj().has_number());
    EXPECT_EQUAL(false, root.obj().has_obj());
    EXPECT_EQUAL(false, root.obj().has_obj_list());

    return 0;
}

} // test
} // kpb
