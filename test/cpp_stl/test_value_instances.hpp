#ifndef _H_KPB_TEST_VALUE_INSTANCES_H_
#define _H_KPB_TEST_VALUE_INSTANCES_H_

namespace kpb {
namespace test {

int value_instances(void);

} // test
} // kpb

#endif
