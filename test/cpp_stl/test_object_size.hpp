#ifndef _H_KPB_TEST_OBJECT_SIZE_H_
#define _H_KPB_TEST_OBJECT_SIZE_H_

namespace kpb {
namespace test {

int object_size(void);

} // test
} // kpb

#endif
