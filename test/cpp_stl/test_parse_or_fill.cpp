#include "test_parse_or_fill.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/parse_or_fill.hpp"

namespace kpb {
namespace test {

int parse_or_fill(void)
{
    kpb_test::ParseOrFill obj;
    kpb_test::ParseOrFillEof obj2;

    // test a standard read
    obj.ParseFromString(std::string("\x12\x34\x56\x78\x90\xab\xcd\xef", 8));
    EXPECT_EQUAL(0x12345678, obj.value1());
    EXPECT_EQUAL(0x90abcdef, obj.other().value2());

    // test that reads can't work out of bounds
    EXPECT_FALSE(obj.ParseFromString(std::string("\x12\x34\x56\x78", 4)));

    // test a complete fill read with 0s
    obj.ParseFromStringOrFill(std::string("\x12\x34\x56\x78\x90\xab\xcd\xef", 8), 0);
    EXPECT_EQUAL(0x12345678, obj.value1());
    EXPECT_EQUAL(0x90abcdef, obj.other().value2());

    // test a complete fill read with 1s
    obj.ParseFromStringOrFill(std::string("\x12\x34\x56\x78\x90\xab\xcd\xef", 8), 1);
    EXPECT_EQUAL(0x12345678, obj.value1());
    EXPECT_EQUAL(0x90abcdef, obj.other().value2());

    // test a fill read with 0s
    obj.ParseFromStringOrFill(std::string("\x12\x34\x56", 3), 0);
    EXPECT_EQUAL(0x12345600, obj.value1());
    EXPECT_EQUAL(0x00000000, obj.other().value2());

    // test a fill read with 1s
    obj.ParseFromStringOrFill(std::string("\x12\x34\x56", 3), 1);
    EXPECT_EQUAL(0x123456ff, obj.value1());
    EXPECT_EQUAL(0xffffffff, obj.other().value2());

    // test a sub type fill read with 0s
    obj.ParseFromStringOrFill(std::string("\x12\x34\x56\x78\x89", 5), 0);
    EXPECT_EQUAL(0x12345678, obj.value1());
    EXPECT_EQUAL(0x89000000, obj.other().value2());

    // test a sub type fill read with 1s
    obj.ParseFromStringOrFill(std::string("\x12\x34\x56\x78\x89", 5), 1);
    EXPECT_EQUAL(0x12345678, obj.value1());
    EXPECT_EQUAL(0x89ffffff, obj.other().value2());

    // make sure eof is hit (even if filling)
    EXPECT_EQUAL(0, obj2.prefix());
    EXPECT_EQUAL(0, obj2.value_size());
    obj2.ParseFromStringOrFill(std::string("\xa5\x5f\xf1", 3), 0);
    EXPECT_EQUAL(0xa, obj2.prefix());
    EXPECT_EQUAL(3, obj2.value_size());
    EXPECT_EQUAL(0x55, obj2.value(0));
    EXPECT_EQUAL(0xff, obj2.value(1));
    EXPECT_EQUAL(0x10, obj2.value(2));

    return 0;
}

} // test
} // kpb
