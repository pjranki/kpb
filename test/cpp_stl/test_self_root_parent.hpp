#ifndef _H_KPB_TEST_SELF_ROOT_PARENT_H_
#define _H_KPB_TEST_SELF_ROOT_PARENT_H_

namespace kpb {
namespace test {

int self_root_parent(void);

} // test
} // kpb

#endif
