#ifndef _H_KPB_TEST_CONSTRUCTOR_RECURSION_H_
#define _H_KPB_TEST_CONSTRUCTOR_RECURSION_H_

namespace kpb {
namespace test {

int constructor_recursion(void);

} // test
} // kpb

#endif
