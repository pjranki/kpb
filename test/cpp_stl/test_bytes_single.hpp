#ifndef _H_KPB_TEST_BYTES_SINGLE_H_
#define _H_KPB_TEST_BYTES_SINGLE_H_

namespace kpb {
namespace test {

int bytes_single(void);

} // test
} // kpb

#endif
