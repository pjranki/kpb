#include "test_params.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/params.hpp"

namespace kpb {
namespace test {

int params(void)
{
    kpb_test::Params params;
    kpb_test::ParamObject2 *obj2 = nullptr;
    kpb_test::ParamObject3 *obj3 = nullptr;
    std::string result;
    EXPECT_TRUE(params.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 16, result.c_str(), result.size());
    obj2 = params.mutable_data2();
    EXPECT_EQUAL(0xcafe, obj2->arg1());
    obj3 = params.add_data3();
    EXPECT_EQUAL(42, obj3->arg1());
    EXPECT_EQUAL(0xbeef, obj3->arg2());
    obj3 = params.add_data3();
    EXPECT_EQUAL(42, obj3->arg1());
    EXPECT_EQUAL(0xbeef, obj3->arg2());
    EXPECT_TRUE(params.ParseFromString(std::string("\x01\x00\x00\x00\x02\x00\x00\x00\x03\x00\x00\x00\x04\x00\x00\x00", 16)));
    EXPECT_EQUAL(1, params.data1().value1());
    EXPECT_EQUAL(2, params.data2().value1());
    EXPECT_EQUAL(0xcafe, params.data2().arg1());
    EXPECT_EQUAL(2, params.data3_size());
    EXPECT_EQUAL(3, params.data3(0).value1());
    EXPECT_EQUAL(42, params.data3(0).arg1());
    EXPECT_EQUAL(0xbeef, params.data3(0).arg2());
    EXPECT_EQUAL(4, params.data3(1).value1());
    EXPECT_EQUAL(42, params.data3(1).arg1());
    EXPECT_EQUAL(0xbeef, params.data3(1).arg2());

    // test that list size changes AFTER parameter values are set
    params.Clear();
    obj3 = params.add_data3();
    EXPECT_NOT_NULL(obj3);
    EXPECT_EQUAL(0, obj3->index());
    EXPECT_EQUAL(1, obj3->current_count());
    obj3 = params.add_data3();
    EXPECT_NOT_NULL(obj3);
    EXPECT_EQUAL(1, obj3->index());
    EXPECT_EQUAL(2, obj3->current_count());

    return 0;
}

} // test
} // kpb
