#ifndef _H_PROCESS_VALUE_ROOT_CPP_H_
#define _H_PROCESS_VALUE_ROOT_CPP_H_

#include <cstdint>
#include <string>

#include "cpp_stl/process_value.hpp"

class ProcessValueRoot
{
    public:
        ProcessValueRoot(const kpb_test::ProcessValue &root, uint8_t arg2);
        const kpb_test::ProcessValue &getter(const kpb_test::ProcessValue &value);
        kpb_test::ProcessValue *setter(kpb_test::ProcessValue *value);
    private:
        const kpb_test::ProcessValue &root_;
        uint8_t arg2_;
};

#endif
