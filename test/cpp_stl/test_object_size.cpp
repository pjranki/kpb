#include "test_object_size.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/object_size.hpp"

namespace kpb {
namespace test {

int object_size(void)
{
    kpb_test::ObjectSize_ obj;
    kpb_test::ObjectSize4 *sub_obj = nullptr;
    std::string result;
    EXPECT_TRUE(obj.SerializeToString(&result));

    EXPECT_TRUE(obj.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00\x00\x00", 8, result.c_str(), result.size());
    EXPECT_FALSE(obj.ParseFromString(std::string("\x00\x00\x00\x00\x00\x00\x00\x00", 8)));
    EXPECT_TRUE(obj.ParseFromString(std::string("\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 16)));

    sub_obj = obj.mutable_object_var();
    EXPECT_NOT_NULL(sub_obj);
    EXPECT_TRUE(sub_obj->set_value(0x12345678));
    sub_obj = obj.mutable_object_fix();
    EXPECT_NOT_NULL(sub_obj);
    EXPECT_TRUE(sub_obj->set_value(0x09abcdef));

    EXPECT_TRUE(obj.set_object_size_(0));
    EXPECT_TRUE(obj.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\xef\xcd\xab\x09", 8, result.c_str(), result.size());

    EXPECT_TRUE(obj.set_object_size_(1));
    EXPECT_TRUE(obj.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x01\x00\x00\x00\x78\xef\xcd\xab\x09", 9, result.c_str(), result.size());

    EXPECT_TRUE(obj.set_object_size_(2));
    EXPECT_TRUE(obj.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x02\x00\x00\x00\x78\x56\xef\xcd\xab\x09", 10, result.c_str(), result.size());

    EXPECT_TRUE(obj.set_object_size_(3));
    EXPECT_TRUE(obj.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x03\x00\x00\x00\x78\x56\x34\xef\xcd\xab\x09", 11, result.c_str(), result.size());

    EXPECT_TRUE(obj.set_object_size_(4));
    EXPECT_TRUE(obj.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x04\x00\x00\x00\x78\x56\x34\x12\xef\xcd\xab\x09", 12, result.c_str(), result.size());

    EXPECT_TRUE(obj.set_object_size_(5));
    EXPECT_TRUE(obj.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x05\x00\x00\x00\x78\x56\x34\x12\x00\xef\xcd\xab\x09", 13, result.c_str(), result.size());

    EXPECT_TRUE(obj.set_object_size_(6));
    EXPECT_TRUE(obj.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x06\x00\x00\x00\x78\x56\x34\x12\x00\x00\xef\xcd\xab\x09", 14, result.c_str(), result.size());

    obj.Clear();
    EXPECT_TRUE(obj.ParseFromString(std::string("\x04\x00\x00\x00\x78\x56\x34\x12\xef\xcd\xab\x09", 12)));
    EXPECT_EQUAL(4, obj.object_size_());
    EXPECT_EQUAL(0x12345678, obj.object_var().value());
    EXPECT_EQUAL(0x09abcdef, obj.object_fix().value());

    obj.Clear();
    EXPECT_TRUE(obj.ParseFromString(std::string("\x05\x00\x00\x00\x78\x56\x34\x12\xff\xef\xcd\xab\x09", 13)));
    EXPECT_EQUAL(5, obj.object_size_());
    EXPECT_EQUAL(0x12345678, obj.object_var().value());
    EXPECT_EQUAL(0x09abcdef, obj.object_fix().value());

    obj.Clear();
    EXPECT_TRUE(obj.ParseFromString(std::string("\x06\x00\x00\x00\x78\x56\x34\x12\xff\xff\xef\xcd\xab\x09", 14)));
    EXPECT_EQUAL(6, obj.object_size_());
    EXPECT_EQUAL(0x12345678, obj.object_var().value());
    EXPECT_EQUAL(0x09abcdef, obj.object_fix().value());

    return 0;
}

} // test
} // kpb
