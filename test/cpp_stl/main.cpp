#include <stdio.h>

#include "test_integers.hpp"
#include "test_bitfield.hpp"
#include "test_basic_list.hpp"
#include "test_strings_single.hpp"
#include "test_strings_list.hpp"
#include "test_bytes_single.hpp"
#include "test_bytes_list.hpp"
#include "test_object_single.hpp"
#include "test_object_list.hpp"
#include "test_imports.hpp"
#include "test_enums.hpp"
#include "test_switchon.hpp"
#include "test_params.hpp"
#include "test_value_instances.hpp"
#include "test_expressions.hpp"
#include "test_update.hpp"
#include "test_update_member.hpp"
#include "test_typecast.hpp"
#include "test_name_deconflict.hpp"
#include "test_io_expr.hpp"
#include "test_boolean.hpp"
#include "test_object_size.hpp"
#include "test_object_align.hpp"
#include "test_self_root_parent.hpp"
#include "test_floats.hpp"
#include "test_packet.hpp"
#include "test_contents.hpp"
#include "test_randomize.hpp"
#include "test_process.hpp"
#include "test_constructor_recursion.hpp"
#include "test_pos.hpp"
#include "test_pos_root_parent.hpp"
#include "test_copy_from.hpp"
#include "test_parse_or_fill.hpp"
#include "test_bitalign.hpp"
#include "test_process_value.hpp"
#include "test_parent_child.hpp"
#include "test_set_instance_value.hpp"
#include "test_bit_pos_size.hpp"

int main(int argc, char *argv[])
{
    (void)argv;
    (void)argc;

    int err = 0;

    printf("Start C++ STL test\n");

    err = kpb::test::integers();
    if ( err )
    {
        return err;
    }

    err = kpb::test::bitfield();
    if ( err )
    {
        return err;
    }

    err = kpb::test::basic_list();
    if ( err )
    {
        return err;
    }

    err = kpb::test::strings_single();
    if ( err )
    {
        return err;
    }

    err = kpb::test::strings_list();
    if ( err )
    {
        return err;
    }

    err = kpb::test::bytes_single();
    if ( err )
    {
        return err;
    }

    err = kpb::test::bytes_list();
    if ( err )
    {
        return err;
    }

    err = kpb::test::object_single();
    if ( err )
    {
        return err;
    }

    err = kpb::test::object_list();
    if ( err )
    {
        return err;
    }

    err = kpb::test::imports();
    if ( err )
    {
        return err;
    }

    err = kpb::test::enums();
    if ( err )
    {
        return err;
    }

    err = kpb::test::switchon();
    if ( err )
    {
        return err;
    }

    err = kpb::test::params();
    if ( err )
    {
        return err;
    }

    err = kpb::test::value_instances();
    if ( err )
    {
        return err;
    }

    err = kpb::test::expressions();
    if ( err )
    {
        return err;
    }

    err = kpb::test::update();
    if ( err )
    {
        return err;
    }

    err = kpb::test::update_member();
    if ( err )
    {
        return err;
    }

    err = kpb::test::typecast();
    if ( err )
    {
        return err;
    }

    err = kpb::test::name_deconflict();
    if ( err )
    {
        return err;
    }

    err = kpb::test::io_expr();
    if ( err )
    {
        return err;
    }

    err = kpb::test::boolean();
    if ( err )
    {
        return err;
    }

    err = kpb::test::object_size();
    if ( err )
    {
        return err;
    }

    err = kpb::test::object_align();
    if ( err )
    {
        return err;
    }

    err = kpb::test::self_root_parent();
    if ( err )
    {
        return err;
    }

    err = kpb::test::floats();
    if ( err )
    {
        return err;
    }

    err = kpb::test::packet();
    if ( err )
    {
        return err;
    }

    err = kpb::test::contents();
    if ( err )
    {
        return err;
    }

    err = kpb::test::randomize();
    if ( err )
    {
        return err;
    }

    err = kpb::test::process();
    if ( err )
    {
        return err;
    }

    err = kpb::test::constructor_recursion();
    if ( err )
    {
        return err;
    }

    err = kpb::test::pos();
    if ( err )
    {
        return err;
    }

    err = kpb::test::pos_root_parent();
    if ( err )
    {
        return err;
    }

    err = kpb::test::copy_from();
    if ( err )
    {
        return err;
    }

    err = kpb::test::parse_or_fill();
    if ( err )
    {
        return err;
    }

    err = kpb::test::bitalign();
    if ( err )
    {
        return err;
    }

    err = kpb::test::process_value();
    if ( err )
    {
        return err;
    }

    err = kpb::test::parent_child();
    if ( err )
    {
        return err;
    }

    err = kpb::test::set_instance_value();
    if ( err )
    {
        return err;
    }

    err = kpb::test::bit_pos_size();
    if ( err )
    {
        return err;
    }

    printf("Done C++ STL test\n");
    return 0;
}
