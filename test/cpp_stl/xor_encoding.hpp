#ifndef _H_XOR_ENCODING_CPP_H_
#define _H_XOR_ENCODING_CPP_H_

#include <cstdint>
#include <string>

#include "cpp_stl/process.hpp"

class XorEncoding
{
    public:
        XorEncoding(uint8_t xor_key, const kpb_test::ProcessConfig &config);
        size_t encode_size(kpb::IO &_io);
        bool encode(kpb::IO &_io);
        bool decode(kpb::IO &_io);
    private:
        uint8_t xor_key_;
        const kpb_test::ProcessConfig &config_;
};

#endif
