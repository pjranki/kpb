#include "test_pos.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/pos.hpp"

namespace kpb {
namespace test {

int pos(void)
{
    std::string result;

    // check default state (after constructor, after a clear)
    kpb_test::SubPos sub_pos;
    EXPECT_EQUAL(11, sub_pos.Size());
    sub_pos.Clear();
    EXPECT_EQUAL(11, sub_pos.Size());

    // test that the size includes position instances
    sub_pos.Clear();
    sub_pos.set_value1(0xdeadbeef);
    sub_pos.set_value2(0xcafebabe);
    EXPECT_EQUAL(11, sub_pos.Size());
    EXPECT_TRUE(sub_pos.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(
        "\xef\xbe\xad\xde\x00\x00\x00\xbe\xba\xfe\xca", 11,
        result.c_str(), result.size()
    );

    // test that reads can't work out of bounds
    sub_pos.Clear();
    EXPECT_FALSE(sub_pos.ParseFromString(std::string("\xef\xbe\xad\x12\x00\x00\x00\x34\xba", 9)));

    // successful read
    sub_pos.Clear();
    EXPECT_TRUE(sub_pos.ParseFromString(std::string("\xef\xbe\xad\x12\x00\x00\x00\x34\xba\xfe\xca", 11)));
    EXPECT_EQUAL(0x12adbeef, sub_pos.value1());
    EXPECT_EQUAL(0xcafeba34, sub_pos.value2());

    // check default state (after constructor, after a clear)
    kpb_test::Pos obj;
    EXPECT_EQUAL(19, obj.Size());
    obj.Clear();
    EXPECT_EQUAL(19, obj.Size());

    // read and stop at error code
    obj.Clear();
    EXPECT_TRUE(obj.ParseFromString(std::string("\x01\x00\x00\xc0", 4)));
    EXPECT_EQUAL(0xc0000001, obj.error_code());
    EXPECT_FALSE(obj.has_sub_pos_offset());
    EXPECT_FALSE(obj.has_sub_pos_size_());
    EXPECT_FALSE(obj.has_sub_pos());
    EXPECT_EQUAL(4, obj.Size());

    // fail to read, not enough data
    obj.Clear();
    EXPECT_FALSE(obj.ParseFromString(std::string("\x09\x00\x00\x00\x0b\x00\x00\x00\x00\xef\xbe\xad\xde\x00\x00\x00\xbe\xba\xfe", 19)));

    // read all
    obj.Clear();
    EXPECT_TRUE(obj.ParseFromString(std::string("\x09\x00\x00\x00\x0b\x00\x00\x00\x00\xef\xbe\xad\xde\x00\x00\x00\xbe\xba\xfe\xca", 20)));
    EXPECT_EQUAL(0, obj.error_code());
    EXPECT_TRUE(obj.has_sub_pos_offset());
    EXPECT_TRUE(obj.has_sub_pos_size_());
    EXPECT_TRUE(obj.has_sub_pos());
    EXPECT_EQUAL(9, obj.sub_pos_offset());
    EXPECT_EQUAL(11, obj.sub_pos_size_());
    EXPECT_EQUAL(0xdeadbeef, obj.sub_pos().value1());
    EXPECT_EQUAL(0xcafebabe, obj.sub_pos().value2());
    EXPECT_EQUAL(20, obj.Size());

    // write error
    obj.Clear();
    obj.set_first_u4(0xc0001234);
    EXPECT_TRUE(obj.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(
        "\x34\x12\x00\xc0", 4,
        result.c_str(), result.size()
    );

    // write all
    obj.Clear();
    obj.set_sub_pos_offset(9);
    obj.mutable_sub_pos()->set_value1(0xdeadbeef);
    obj.mutable_sub_pos()->set_value2(0xcafebabe);
    EXPECT_TRUE(obj.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(
        "\x09\x00\x00\x00\x0b\x00\x00\x00\x00\xef\xbe\xad\xde\x00\x00\x00\xbe\xba\xfe\xca", 20,
        result.c_str(), result.size()
    );

    return 0;
}

} // test
} // kpb
