#include "test_bytes_single.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/bytes_single.hpp"

namespace kpb {
namespace test {

int bytes_single(void)
{
    kpb_test::BytesSingle bytes_single;
    std::string result;
    EXPECT_STD_STRING_EQUAL(std::string("", 0), bytes_single.data2());
    EXPECT_STD_STRING_EQUAL(std::string("", 0), bytes_single.data4());
    EXPECT_TRUE(bytes_single.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00", 6, result.c_str(), result.size());
    bytes_single.set_data2(std::string("\xfe", 1));
    bytes_single.set_data4(std::string("\x12", 1));
    EXPECT_TRUE(bytes_single.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xfe\x00\x12\x00\x00\x00", 6, result.c_str(), result.size());
    bytes_single.set_data2(std::string("\xfe\xdc", 2));
    bytes_single.set_data4(std::string("\x12\x34", 2));
    EXPECT_TRUE(bytes_single.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xfe\xdc\x12\x34\x00\x00", 6, result.c_str(), result.size());
    bytes_single.set_data2(std::string("\xfe\xdc\xab", 3));
    bytes_single.set_data4(std::string("\x12\x34\x45\x67", 4));
    EXPECT_TRUE(bytes_single.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xfe\xdc\x12\x34\x45\x67", 6, result.c_str(), result.size());
    bytes_single.set_data2(std::string("\xfe\xdc\xab\x90", 4));
    bytes_single.set_data4(std::string("\x12\x34\x45\x67\x89", 5));
    EXPECT_TRUE(bytes_single.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xfe\xdc\x12\x34\x45\x67", 6, result.c_str(), result.size());
    bytes_single.Clear();
    EXPECT_TRUE(bytes_single.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00", 6, result.c_str(), result.size());

    bytes_single.Clear();
    EXPECT_EQUAL(0, bytes_single.data2().size());
    EXPECT_EQUAL(0, bytes_single.data2_size());
    EXPECT_EQUAL(0, bytes_single.data4().size());
    EXPECT_EQUAL(0, bytes_single.data4_size());
    EXPECT_TRUE(bytes_single.ParseFromString(std::string("\x00\x00\x00\x00\x00\x00", 6)));
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_single.data2().c_str(), bytes_single.data2().size());
    EXPECT_EQUAL(2, bytes_single.data2_size());
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_single.data4().c_str(), bytes_single.data4().size());
    EXPECT_EQUAL(4, bytes_single.data4_size());
    EXPECT_TRUE(bytes_single.ParseFromString(std::string("\xfe\x00\x12\x00\x00\x00", 6)));
    EXPECT_BYTES_EQUAL("\xfe\x00", 2, bytes_single.data2().c_str(), bytes_single.data2().size());
    EXPECT_EQUAL(2, bytes_single.data2_size());
    EXPECT_BYTES_EQUAL("\x12\x00\x00\x00", 4, bytes_single.data4().c_str(), bytes_single.data4().size());
    EXPECT_EQUAL(4, bytes_single.data4_size());
    EXPECT_TRUE(bytes_single.ParseFromString(std::string("\xfe\xdc\x12\x34\x00\x00", 6)));
    EXPECT_BYTES_EQUAL("\xfe\xdc", 2, bytes_single.data2().c_str(), bytes_single.data2().size());
    EXPECT_EQUAL(2, bytes_single.data2_size());
    EXPECT_BYTES_EQUAL("\x12\x34\x00\x00", 4, bytes_single.data4().c_str(), bytes_single.data4().size());
    EXPECT_EQUAL(4, bytes_single.data4_size());
    EXPECT_TRUE(bytes_single.ParseFromString(std::string("\xfe\xdc\x12\x34\x56\x78", 6)));
    EXPECT_BYTES_EQUAL("\xfe\xdc", 2, bytes_single.data2().c_str(), bytes_single.data2().size());
    EXPECT_EQUAL(2, bytes_single.data2_size());
    EXPECT_BYTES_EQUAL("\x12\x34\x56\x78", 4, bytes_single.data4().c_str(), bytes_single.data4().size());
    EXPECT_EQUAL(4, bytes_single.data4_size());

    return 0;
}

} // test
} // kpb
