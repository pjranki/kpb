#include "test_typecast.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/typecast.hpp"

namespace kpb {
namespace test {

int typecast(void)
{
    kpb_test::Typecast typecast;
    std::string result;
    EXPECT_TRUE(typecast.SerializeToString(&result));
    return 0;
}

} // test
} // kpb
