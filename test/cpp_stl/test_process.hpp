#ifndef _H_KPB_TEST_PROCESS_H_
#define _H_KPB_TEST_PROCESS_H_

namespace kpb {
namespace test {

int process(void);

} // test
} // kpb

#endif
