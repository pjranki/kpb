#include "test_name_deconflict.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/name_deconflict.hpp"

namespace kpb {
namespace test {

int name_deconflict(void)
{
    kpb_test::FcObj obj;
    kpb_test::FcSingleParent *single = nullptr;
    kpb_test::FcMultiParent *multi = nullptr;
    std::string result;

    // call update
    obj.Clear();
    EXPECT_TRUE(obj.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(
        "\x20\x00\x00\x00" "\x00\x00\x00\x00" "\x00\x00\x00\x00" \
        "\x00\x00" "" \
        "\x00\x00" \
        "\x00" "" \
        "\x01" "\x00" \
        "\x00\x00" \
        "\x00" "" \
        "\x02" "\x00\x00" \
        "\x00\x00" \
        "\x00" "" \
        "\x00" \
        "\x00" "" \
        "\x00" "" \
        "\x00",
        32,
        result.c_str(),
        result.size()
    );
    EXPECT_EQUAL(32, obj.fm_total_size1());
    EXPECT_EQUAL(1, obj.fm_asciiz_size1());
    EXPECT_EQUAL(2, obj.fm_widez_size1());

    // change some values - others are (automatically) updated
    EXPECT_TRUE(obj.set_fm_total_size1(0xdeadbeef));
    EXPECT_TRUE(obj.set_fm_flags(0xdeadbeef));
    EXPECT_TRUE(obj.set_fm_unused(0xcafebabe));
    EXPECT_TRUE(obj.set_fm_buffer(std::string("\xab\x00\xcd", 3)));
    EXPECT_TRUE(obj.add_fm_buffer_list(std::string("\x12\x34\x56\x78\x90", 5)));
    EXPECT_TRUE(obj.set_fm_ascii("A"));
    EXPECT_TRUE(obj.set_fm_asciiz("BC"));
    EXPECT_TRUE(obj.add_fm_ascii_list("DEF"));
    EXPECT_TRUE(obj.set_fm_wide(L"a"));
    EXPECT_TRUE(obj.set_fm_widez(L"bc"));
    EXPECT_TRUE(obj.add_fm_wide_list(L"def"));

    single = obj.add_fm_single_data_list();
    EXPECT_NOT_NULL(single);
    EXPECT_TRUE(single->set_fm_data(std::string("\x33\x33\x33", 3)));

    multi = obj.add_fm_multi_data_list();
    EXPECT_NOT_NULL(multi);
    EXPECT_TRUE(multi->set_fm_data(std::string("\x55\x55\x55\x55\x55", 5)));

    multi = obj.mutable_fm_multi_data();
    EXPECT_NOT_NULL(multi);
    EXPECT_TRUE(multi->set_fm_data(std::string("\x44\x44\x44\x44", 4)));
    single = obj.mutable_fm_single_data();
    EXPECT_NOT_NULL(single);
    EXPECT_TRUE(single->set_fm_data(std::string("\x11", 1)));
    multi = single->mutable_fm_multi_data();
    EXPECT_NOT_NULL(multi);
    EXPECT_TRUE(multi->set_fm_data(std::string("\x22\x22", 2)));

    EXPECT_EQUAL(74, obj.fm_total_size1());
    EXPECT_EQUAL(0xdeadbeef, obj.fm_flags());
    EXPECT_EQUAL(0, obj.fm_unused());
    EXPECT_EQUAL(3, obj.fm_buffer_size1());
    EXPECT_EQUAL(1, obj.fm_buffer_list_count());
    EXPECT_EQUAL(1, obj.fm_ascii_size1());
    EXPECT_EQUAL(3, obj.fm_asciiz_size1());
    EXPECT_EQUAL(1, obj.fm_ascii_list_count());
    EXPECT_EQUAL(2, obj.fm_wide_size1());
    EXPECT_EQUAL(6, obj.fm_widez_size1());
    EXPECT_EQUAL(1, obj.fm_wide_list_count());

    EXPECT_TRUE(obj.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(
        "\x4a\x00\x00\x00" "\xef\xbe\xad\xde" "\x00\x00\x00\x00" \
        "\x03\x00" "\xab\x00\xcd" \
        "\x01\x00" "\x12\x34\x56\x78"\
        "\x01" "A" \
        "\x03" "BC\x00" \
        "\x01\x00" "DEF\x00" \
        "\x02" "a\x00" \
        "\x06" "b\x00" "c\x00" "\x00\x00" \
        "\x01\x00" "d\x00" "e\x00" \
        "\x01" "\x11" "\x02" "\x22\x22" \
        "\x01" "\x03" "\x33\x33\x33" "\x00" \
        "\x04" "\x44\x44\x44\x44" \
        "\x01" "\x05" "\x55\x55\x55\x55\x55",
        74,
        result.c_str(), result.size()
    );

    // changes in parent are reflected in children via instances
    obj.set_fm_flags(0xdeadbeef);
    EXPECT_EQUAL(0xdeadbeef, obj.fm_single_data().fm_parent_flags());
    EXPECT_EQUAL(0xdeadbeef, obj.fm_single_data().fm_multi_data().fm_flags());
    obj.set_fm_flags(0xcafebabe);
    EXPECT_EQUAL(0xcafebabe, obj.fm_single_data().fm_parent_flags());
    EXPECT_EQUAL(0xcafebabe, obj.fm_single_data().fm_multi_data().fm_flags());

    // clear
    obj.Clear();
    EXPECT_TRUE(obj.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(
        "\x20\x00\x00\x00" "\x00\x00\x00\x00" "\x00\x00\x00\x00" \
        "\x00\x00" "" \
        "\x00\x00" \
        "\x00" "" \
        "\x01" "\x00" \
        "\x00\x00" \
        "\x00" "" \
        "\x02" "\x00\x00" \
        "\x00\x00" \
        "\x00" "" \
        "\x00" \
        "\x00" "" \
        "\x00" "" \
        "\x00",
        32,
        result.c_str(), result.size()
    );

    // parse from string
    EXPECT_TRUE(obj.ParseFromString(std::string(
        "\x4a\x00\x00\x00" "\xef\xbe\xad\xde" "\xa5\xa5\xa5\xa5" \
        "\x03\x00" "\xab\x00\xcd" \
        "\x01\x00" "\x12\x34\x56\x78"\
        "\x01" "A" \
        "\x03" "BC\x00" \
        "\x01\x00" "DEF\x00" \
        "\x02" "a\x00" \
        "\x06" "b\x00" "c\x00" "\x00\x00" \
        "\x01\x00" "d\x00" "e\x00" \
        "\x01" "\x11" "\x02" "\x22\x22" \
        "\x01" "\x03" "\x33\x33\x33" "\x00" \
        "\x04" "\x44\x44\x44\x44" \
        "\x01" "\x05" "\x55\x55\x55\x55\x55",
        74
    )));
    EXPECT_EQUAL(74, obj.fm_total_size1());
    EXPECT_EQUAL(0xdeadbeef, obj.fm_flags());
    EXPECT_EQUAL(0xa5a5a5a5, obj.fm_unused());  // this is correct, parse/read should NOT call update
    EXPECT_EQUAL(3, obj.fm_buffer_size1());
    EXPECT_BYTES_EQUAL("\xab\x00\xcd", 3, obj.fm_buffer().c_str(), obj.fm_buffer().size());
    EXPECT_EQUAL(1, obj.fm_buffer_list_count());
    EXPECT_EQUAL(1, obj.fm_buffer_list().size());
    EXPECT_BYTES_EQUAL("\x12\x34\x56\x78", 4, obj.fm_buffer_list(0).c_str(), obj.fm_buffer_list(0).size());
    EXPECT_EQUAL(1, obj.fm_ascii_size1());
    EXPECT_STRING_EQUAL("A", obj.fm_ascii().c_str());
    EXPECT_EQUAL(3, obj.fm_asciiz_size1());
    EXPECT_STRING_EQUAL("BC", obj.fm_asciiz().c_str());
    EXPECT_EQUAL(1, obj.fm_ascii_list_count());
    EXPECT_EQUAL(1, obj.fm_ascii_list().size());
    EXPECT_STRING_EQUAL("DEF", obj.fm_ascii_list(0).c_str());
    EXPECT_EQUAL(2, obj.fm_wide_size1());
    EXPECT_STRING_EQUAL(L"a", obj.fm_wide().c_str());
    EXPECT_EQUAL(6, obj.fm_widez_size1());
    EXPECT_STRING_EQUAL(L"bc", obj.fm_widez().c_str());
    EXPECT_EQUAL(1, obj.fm_wide_list_count());
    EXPECT_EQUAL(1, obj.fm_wide_list().size());
    EXPECT_STRING_EQUAL(L"de", obj.fm_wide_list(0).c_str());
    EXPECT_EQUAL(1, obj.fm_single_data().fm_data_size1());
    EXPECT_BYTES_EQUAL("\x11", 1, obj.fm_single_data().fm_data().c_str(), obj.fm_single_data().fm_data().size());
    EXPECT_EQUAL(2, obj.fm_single_data().fm_multi_data().fm_data_size1());
    EXPECT_BYTES_EQUAL("\x22\x22", 2, obj.fm_single_data().fm_multi_data().fm_data().c_str(), obj.fm_single_data().fm_multi_data().fm_data().size());
    EXPECT_EQUAL(1, obj.fm_single_data_list_count());
    EXPECT_EQUAL(1, obj.fm_single_data_list().size());
    EXPECT_EQUAL(3, obj.fm_single_data_list(0).fm_data_size1());
    EXPECT_BYTES_EQUAL("\x33\x33\x33", 3, obj.fm_single_data_list(0).fm_data().c_str(), obj.fm_single_data_list(0).fm_data().size());
    EXPECT_EQUAL(0, obj.fm_single_data_list(0).fm_multi_data().fm_data_size1());
    EXPECT_EQUAL(0, obj.fm_single_data_list(0).fm_multi_data().fm_data().size());
    EXPECT_EQUAL(4, obj.fm_multi_data().fm_data_size1());
    EXPECT_BYTES_EQUAL("\x44\x44\x44\x44", 4, obj.fm_multi_data().fm_data().c_str(), obj.fm_multi_data().fm_data().size());
    EXPECT_EQUAL(1, obj.fm_multi_data_list_count());
    EXPECT_EQUAL(1, obj.fm_multi_data_list().size());
    EXPECT_EQUAL(5, obj.fm_multi_data_list(0).fm_data_size1());
    EXPECT_BYTES_EQUAL("\x55\x55\x55\x55\x55", 5, obj.fm_multi_data_list(0).fm_data().c_str(), obj.fm_multi_data_list(0).fm_data().size());

    // enum
    uint16_t value16 = kpb_test::FcObj::FeFoobar::FE_FOOBAR_FVE_V1;
    EXPECT_EQUAL(0x1234, value16);

    // instances
    EXPECT_EQUAL(1, obj.fm_0());
    EXPECT_TRUE(obj.fm_1());
    EXPECT_EQUAL(0, obj.fm_2().fm_data_size1());
    EXPECT_EQUAL(0, obj.fm_3());
    EXPECT_EQUAL(0, obj.fm_4());

    // correct use of size
    kpb_test::CorrectUseOfSize_ correct_size;
    correct_size.set_size_(0xdeadbeef);
    EXPECT_EQUAL(0xdeadbeef, correct_size.size_());
    EXPECT_EQUAL(0xdeadbeef, correct_size.value1());
    EXPECT_EQUAL(0xdeadbeef, correct_size.value2());
    correct_size.set_size_(0xcafebabe);
    EXPECT_EQUAL(0xcafebabe, correct_size.size_());
    EXPECT_EQUAL(0xcafebabe, correct_size.value1());
    EXPECT_EQUAL(0xcafebabe, correct_size.value2());

    // correct use of length
    kpb_test::CorrectUseOfLength correct_length;
    correct_length.set_length_(0xdeadbeef);
    EXPECT_EQUAL(0xdeadbeef, correct_length.length_());
    // update/set will use length as the size of the object (not the member's value)
    EXPECT_EQUAL(8, correct_length.value1());
    EXPECT_EQUAL(0xdeadbeef, correct_length.value2());
    correct_length.set_length_(0xcafebabe);
    EXPECT_EQUAL(0xcafebabe, correct_length.length_());
    // update/set will use length as the size of the object (not the member's value)
    EXPECT_EQUAL(8, correct_length.value1());
    EXPECT_EQUAL(0xcafebabe, correct_length.value2());

    return 0;
}

} // test
} // kpb
