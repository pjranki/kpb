#include "test_set_instance_value.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/set_instance_value.hpp"

namespace kpb {
namespace test {

int set_instance_value(void)
{
    kpb_test::_SetInstanceValue obj;

    // bool
    EXPECT_TRUE(obj.set_input_bool(true));
    EXPECT_EQUAL(true, obj.storage_bool());
    EXPECT_TRUE(obj.set_input_bool(false));
    EXPECT_EQUAL(false, obj.storage_bool());

    // bitfield
    EXPECT_TRUE(obj.set_input_b4(0xa));
    EXPECT_EQUAL(0xa, obj.storage_b4());
    EXPECT_TRUE(obj.set_input_b4(0x5));
    EXPECT_EQUAL(0x5, obj.storage_b4());

    // unsigned integer
    EXPECT_TRUE(obj.set_input_u4(123));
    EXPECT_EQUAL(123, obj.storage_u4());
    EXPECT_TRUE(obj.set_input_u4(456));
    EXPECT_EQUAL(456, obj.storage_u4());

    // signed integer
    EXPECT_TRUE(obj.set_input_s4(-12));
    EXPECT_EQUAL(-12, obj.storage_s4());
    EXPECT_TRUE(obj.set_input_s4(45));
    EXPECT_EQUAL(45, obj.storage_s4());

    // signed integer
    EXPECT_TRUE(obj.set_input_s4(-12));
    EXPECT_EQUAL(-12, obj.storage_s4());
    EXPECT_TRUE(obj.set_input_s4(45));
    EXPECT_EQUAL(45, obj.storage_s4());

    // float
    EXPECT_TRUE(obj.set_input_f4(1.1f));
    EXPECT_EQUAL(1.1f, obj.storage_f4());
    EXPECT_TRUE(obj.set_input_f4(-2.3f));
    EXPECT_EQUAL(-2.3f, obj.storage_f4());

    // calculate and set _value
    EXPECT_TRUE(obj.set_calc_value_u4(2));
    EXPECT_EQUAL(9, obj.storage_u4());

    // check nested sets
    EXPECT_TRUE(obj.set_nested_1(1));
    EXPECT_EQUAL(1 * 2 * 3 * 4, obj.nested_4());
    EXPECT_EQUAL(1 * 2 * 3 * 4 * 5, obj.nested_3());

    return 0;
}

} // test
} // kpb
