#include "test_copy_from.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/copy_from.hpp"

namespace kpb {
namespace test {

int copy_from(void)
{
    // TODO: write this test
    kpb_test::CopyFrom_ obj;
    std::string result;
    EXPECT_TRUE(obj.SerializeToString(&result));
    return 0;
}

} // test
} // kpb
