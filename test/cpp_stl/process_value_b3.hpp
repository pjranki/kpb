#ifndef _H_PROCESS_VALUE_B3_CPP_H_
#define _H_PROCESS_VALUE_B3_CPP_H_

#include <cstdint>
#include <string>

#include "cpp_stl/process_value.hpp"

class ProcessValueB3
{
    public:
        ProcessValueB3(const kpb_test::ProcessValue &root);
        uint8_t getter(uint8_t value);
        uint8_t setter(uint8_t value);
    private:
        const kpb_test::ProcessValue &root_;
};

#endif
