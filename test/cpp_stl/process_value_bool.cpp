#include "process_value_bool.hpp"


ProcessValueBool::ProcessValueBool(bool invert_logic)
{
    invert_logic_ = invert_logic;
}

bool ProcessValueBool::getter(bool value)
{
    return invert_logic_ ? !value : value;
}

bool ProcessValueBool::setter(bool value)
{
    return invert_logic_ ? !value : value;
}
