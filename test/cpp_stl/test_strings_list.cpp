#include "test_strings_list.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/strings_list.hpp"

namespace kpb {
namespace test {

int strings_list(void)
{
    kpb_test::StringsList strings_list;
    std::string result;
    EXPECT_TRUE(strings_list.SerializeToString(&result));
    EXPECT_EQUAL(0, strings_list.char_list_size());
    EXPECT_EQUAL(0, strings_list.wchar_list_size());
    EXPECT_EQUAL(0, strings_list.char_list().size());
    EXPECT_EQUAL(0, strings_list.wchar_list().size());
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 30, result.c_str(), result.size());
    strings_list.add_char_list("A");
    strings_list.add_wchar_list(L"a");
    EXPECT_EQUAL(1, strings_list.char_list_size());
    EXPECT_EQUAL(1, strings_list.wchar_list_size());
    EXPECT_EQUAL(1, strings_list.char_list().size());
    EXPECT_EQUAL(1, strings_list.wchar_list().size());
    EXPECT_TRUE(strings_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x41\x00\x00\x00\x00\x00\x00\x00\x00\x00\x61\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 30, result.c_str(), result.size());
    strings_list.add_char_list("BC");
    strings_list.add_wchar_list(L"bc");
    EXPECT_EQUAL(2, strings_list.char_list_size());
    EXPECT_EQUAL(2, strings_list.wchar_list_size());
    EXPECT_EQUAL(2, strings_list.char_list().size());
    EXPECT_EQUAL(2, strings_list.wchar_list().size());
    EXPECT_TRUE(strings_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x41\x00\x42\x43\x00\x00\x00\x00\x00\x00\x61\x00\x00\x00\x62\x00\x63\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 30, result.c_str(), result.size());
    strings_list.add_char_list("DEF");
    strings_list.add_char_list("DEF");
    strings_list.add_char_list("DEF");
    strings_list.add_wchar_list(L"def");
    strings_list.add_wchar_list(L"def");
    strings_list.add_wchar_list(L"def");
    EXPECT_EQUAL(5, strings_list.char_list_size());
    EXPECT_EQUAL(5, strings_list.wchar_list_size());
    EXPECT_EQUAL(5, strings_list.char_list().size());
    EXPECT_EQUAL(5, strings_list.wchar_list().size());
    EXPECT_TRUE(strings_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x41\x00\x42\x43\x44\x45\x44\x45\x44\x45\x61\x00\x00\x00\x62\x00\x63\x00\x64\x00\x65\x00\x64\x00\x65\x00\x64\x00\x65\x00", 30, result.c_str(), result.size());
    strings_list.add_char_list("G");
    strings_list.add_wchar_list(L"g");
    EXPECT_EQUAL(6, strings_list.char_list_size());
    EXPECT_EQUAL(6, strings_list.wchar_list_size());
    EXPECT_EQUAL(6, strings_list.char_list().size());
    EXPECT_EQUAL(6, strings_list.wchar_list().size());
    EXPECT_TRUE(strings_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x41\x00\x42\x43\x44\x45\x44\x45\x44\x45\x61\x00\x00\x00\x62\x00\x63\x00\x64\x00\x65\x00\x64\x00\x65\x00\x64\x00\x65\x00", 30, result.c_str(), result.size());
    strings_list.Clear();
    EXPECT_EQUAL(0, strings_list.char_list_size());
    EXPECT_EQUAL(0, strings_list.wchar_list_size());
    EXPECT_EQUAL(0, strings_list.char_list().size());
    EXPECT_EQUAL(0, strings_list.wchar_list().size());
    EXPECT_TRUE(strings_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 30, result.c_str(), result.size());

    strings_list.Clear();
    EXPECT_TRUE(strings_list.ParseFromString(std::string("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 30)));
    EXPECT_EQUAL(5, strings_list.char_list_size());
    EXPECT_EQUAL(5, strings_list.wchar_list_size());
    EXPECT_EQUAL(5, strings_list.char_list().size());
    EXPECT_EQUAL(5, strings_list.wchar_list().size());
    EXPECT_STD_STRING_EQUAL(std::string(""), strings_list.char_list()[0]);
    EXPECT_STD_STRING_EQUAL(std::string(""), strings_list.char_list()[1]);
    EXPECT_STD_STRING_EQUAL(std::string(""), strings_list.char_list()[2]);
    EXPECT_STD_STRING_EQUAL(std::string(""), strings_list.char_list()[3]);
    EXPECT_STD_STRING_EQUAL(std::string(""), strings_list.char_list()[4]);
    EXPECT_STD_STRING_EQUAL(std::wstring(L""), strings_list.wchar_list()[0]);
    EXPECT_STD_STRING_EQUAL(std::wstring(L""), strings_list.wchar_list()[1]);
    EXPECT_STD_STRING_EQUAL(std::wstring(L""), strings_list.wchar_list()[2]);
    EXPECT_STD_STRING_EQUAL(std::wstring(L""), strings_list.wchar_list()[3]);
    EXPECT_STD_STRING_EQUAL(std::wstring(L""), strings_list.wchar_list()[4]);
    strings_list.Clear();
    EXPECT_TRUE(strings_list.ParseFromString(std::string("\x41\x00\x00\x00\x00\x00\x00\x00\x00\x00\x61\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 30)));
    EXPECT_EQUAL(5, strings_list.char_list_size());
    EXPECT_EQUAL(5, strings_list.wchar_list_size());
    EXPECT_EQUAL(5, strings_list.char_list().size());
    EXPECT_EQUAL(5, strings_list.wchar_list().size());
    EXPECT_STD_STRING_EQUAL(std::string("A"), strings_list.char_list()[0]);
    EXPECT_STD_STRING_EQUAL(std::string(""), strings_list.char_list()[1]);
    EXPECT_STD_STRING_EQUAL(std::string(""), strings_list.char_list()[2]);
    EXPECT_STD_STRING_EQUAL(std::string(""), strings_list.char_list()[3]);
    EXPECT_STD_STRING_EQUAL(std::string(""), strings_list.char_list()[4]);
    EXPECT_STD_STRING_EQUAL(std::wstring(L"a"), strings_list.wchar_list()[0]);
    EXPECT_STD_STRING_EQUAL(std::wstring(L""), strings_list.wchar_list()[1]);
    EXPECT_STD_STRING_EQUAL(std::wstring(L""), strings_list.wchar_list()[2]);
    EXPECT_STD_STRING_EQUAL(std::wstring(L""), strings_list.wchar_list()[3]);
    EXPECT_STD_STRING_EQUAL(std::wstring(L""), strings_list.wchar_list()[4]);
    strings_list.Clear();
    EXPECT_TRUE(strings_list.ParseFromString(std::string("\x41\x00\x42\x43\x00\x00\x00\x00\x00\x00\x61\x00\x00\x00\x62\x00\x63\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 30)));
    EXPECT_EQUAL(5, strings_list.char_list_size());
    EXPECT_EQUAL(5, strings_list.wchar_list_size());
    EXPECT_EQUAL(5, strings_list.char_list().size());
    EXPECT_EQUAL(5, strings_list.wchar_list().size());
    EXPECT_STD_STRING_EQUAL(std::string("A"), strings_list.char_list()[0]);
    EXPECT_STD_STRING_EQUAL(std::string("BC"), strings_list.char_list()[1]);
    EXPECT_STD_STRING_EQUAL(std::string(""), strings_list.char_list()[2]);
    EXPECT_STD_STRING_EQUAL(std::string(""), strings_list.char_list()[3]);
    EXPECT_STD_STRING_EQUAL(std::string(""), strings_list.char_list()[4]);
    EXPECT_STD_STRING_EQUAL(std::wstring(L"a"), strings_list.wchar_list()[0]);
    EXPECT_STD_STRING_EQUAL(std::wstring(L"bc"), strings_list.wchar_list()[1]);
    EXPECT_STD_STRING_EQUAL(std::wstring(L""), strings_list.wchar_list()[2]);
    EXPECT_STD_STRING_EQUAL(std::wstring(L""), strings_list.wchar_list()[3]);
    EXPECT_STD_STRING_EQUAL(std::wstring(L""), strings_list.wchar_list()[4]);
    strings_list.Clear();
    EXPECT_TRUE(strings_list.ParseFromString(std::string("\x41\x00\x42\x43\x44\x45\x44\x45\x44\x45\x61\x00\x00\x00\x62\x00\x63\x00\x64\x00\x65\x00\x64\x00\x65\x00\x64\x00\x65\x00", 30)));
    EXPECT_EQUAL(5, strings_list.char_list_size());
    EXPECT_EQUAL(5, strings_list.wchar_list_size());
    EXPECT_EQUAL(5, strings_list.char_list().size());
    EXPECT_EQUAL(5, strings_list.wchar_list().size());
    EXPECT_STD_STRING_EQUAL(std::string("A"), strings_list.char_list()[0]);
    EXPECT_STD_STRING_EQUAL(std::string("BC"), strings_list.char_list()[1]);
    EXPECT_STD_STRING_EQUAL(std::string("DE"), strings_list.char_list()[2]);
    EXPECT_STD_STRING_EQUAL(std::string("DE"), strings_list.char_list()[3]);
    EXPECT_STD_STRING_EQUAL(std::string("DE"), strings_list.char_list()[4]);
    EXPECT_STD_STRING_EQUAL(std::wstring(L"a"), strings_list.wchar_list()[0]);
    EXPECT_STD_STRING_EQUAL(std::wstring(L"bc"), strings_list.wchar_list()[1]);
    EXPECT_STD_STRING_EQUAL(std::wstring(L"de"), strings_list.wchar_list()[2]);
    EXPECT_STD_STRING_EQUAL(std::wstring(L"de"), strings_list.wchar_list()[3]);
    EXPECT_STD_STRING_EQUAL(std::wstring(L"de"), strings_list.wchar_list()[4]);
    return 0;
}

} // test
} // kpb
