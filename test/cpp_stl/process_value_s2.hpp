#ifndef _H_PROCESS_VALUE_S2_CPP_H_
#define _H_PROCESS_VALUE_S2_CPP_H_

#include <cstdint>
#include <string>

#include "cpp_stl/process_value.hpp"

class ProcessValueS2
{
    public:
        ProcessValueS2();
        int16_t getter(int16_t value);
        int16_t setter(int16_t value);
};

#endif
