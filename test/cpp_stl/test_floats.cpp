#include "test_floats.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/floats.hpp"

namespace kpb {
namespace test {

#define FLOAT_0_0_LE            "\x00\x00\x00\x00"
#define DOUBLE_0_0_LE           "\x00\x00\x00\x00\x00\x00\x00\x00"
#define FLOAT_0_0_BE            "\x00\x00\x00\x00"
#define DOUBLE_0_0_BE           "\x00\x00\x00\x00\x00\x00\x00\x00"
#define FLOAT_1_1_LE            "\xcd\xcc\x8c\x3f"
#define DOUBLE_1_1_LE           "\x9a\x99\x99\x99\x99\x99\xf1\x3f"
#define FLOAT_1_1_BE            "\x3f\x8c\xcc\xcd"
#define DOUBLE_1_1_BE           "\x3f\xf1\x99\x99\x99\x99\x99\x9a"
#define FLOAT_NEG_2_2_LE        "\xcd\xcc\x0c\xc0"
#define DOUBLE_NEG_2_2_LE       "\x9a\x99\x99\x99\x99\x99\x01\xc0"
#define FLOAT_NEG_2_2_BE        "\xc0\x0c\xcc\xcd"
#define DOUBLE_NEG_2_2_BE       "\xc0\x01\x99\x99\x99\x99\x99\x9a"

#define EXPECTED_0_0            (FLOAT_0_0_LE DOUBLE_0_0_LE FLOAT_0_0_LE DOUBLE_0_0_LE FLOAT_0_0_BE DOUBLE_0_0_BE)
#define EXPECTED_0_0_SIZE       (sizeof(EXPECTED_0_0)-1)
#define EXPECTED_1_1            (FLOAT_1_1_LE DOUBLE_1_1_LE FLOAT_1_1_LE DOUBLE_1_1_LE FLOAT_1_1_BE DOUBLE_1_1_BE)
#define EXPECTED_1_1_SIZE       (sizeof(EXPECTED_1_1)-1)
#define EXPECTED_NEG_2_2        (FLOAT_NEG_2_2_LE DOUBLE_NEG_2_2_LE FLOAT_NEG_2_2_LE DOUBLE_NEG_2_2_LE FLOAT_NEG_2_2_BE DOUBLE_NEG_2_2_BE)
#define EXPECTED_NEG_2_2_SIZE   (sizeof(EXPECTED_NEG_2_2)-1)

int floats(void)
{
    std::string result;
    kpb_test::Floats floats;
    EXPECT_TRUE(floats.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(EXPECTED_0_0, EXPECTED_0_0_SIZE, result.c_str(), result.size());
    EXPECT_EQUAL(0.0, floats.float_());
    EXPECT_EQUAL(0.0, floats.double_());
    EXPECT_EQUAL(0.0, floats.floatle());
    EXPECT_EQUAL(0.0, floats.doublele());
    EXPECT_EQUAL(0.0, floats.floatbe());
    EXPECT_EQUAL(0.0, floats.doublebe());

    EXPECT_TRUE(floats.set_float_(1.1f));
    EXPECT_TRUE(floats.set_double_(1.1));
    EXPECT_TRUE(floats.set_floatle(1.1f));
    EXPECT_TRUE(floats.set_doublele(1.1));
    EXPECT_TRUE(floats.set_floatbe(1.1f));
    EXPECT_TRUE(floats.set_doublebe(1.1));
    EXPECT_EQUAL(1.1f, floats.float_());
    EXPECT_EQUAL(1.1, floats.double_());
    EXPECT_EQUAL(1.1f, floats.floatle());
    EXPECT_EQUAL(1.1, floats.doublele());
    EXPECT_EQUAL(1.1f, floats.floatbe());
    EXPECT_EQUAL(1.1, floats.doublebe());
    EXPECT_TRUE(floats.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(EXPECTED_1_1, EXPECTED_1_1_SIZE, result.c_str(), result.size());

    EXPECT_TRUE(floats.set_float_(-2.2f));
    EXPECT_TRUE(floats.set_double_(-2.2));
    EXPECT_TRUE(floats.set_floatle(-2.2f));
    EXPECT_TRUE(floats.set_doublele(-2.2));
    EXPECT_TRUE(floats.set_floatbe(-2.2f));
    EXPECT_TRUE(floats.set_doublebe(-2.2));
    EXPECT_EQUAL(-2.2f, floats.float_());
    EXPECT_EQUAL(-2.2, floats.double_());
    EXPECT_EQUAL(-2.2f, floats.floatle());
    EXPECT_EQUAL(-2.2, floats.doublele());
    EXPECT_EQUAL(-2.2f, floats.floatbe());
    EXPECT_EQUAL(-2.2, floats.doublebe());
    EXPECT_TRUE(floats.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(EXPECTED_NEG_2_2, EXPECTED_NEG_2_2_SIZE, result.c_str(), result.size());

    floats.Clear();
    EXPECT_EQUAL(0.0, floats.float_());
    EXPECT_EQUAL(0.0, floats.double_());
    EXPECT_EQUAL(0.0, floats.floatle());
    EXPECT_EQUAL(0.0, floats.doublele());
    EXPECT_EQUAL(0.0, floats.floatbe());
    EXPECT_EQUAL(0.0, floats.doublebe());
    EXPECT_TRUE(floats.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(EXPECTED_0_0, EXPECTED_0_0_SIZE, result.c_str(), result.size());

    EXPECT_TRUE(floats.ParseFromString(std::string(EXPECTED_1_1, EXPECTED_1_1_SIZE)));
    EXPECT_EQUAL(1.1f, floats.float_());
    EXPECT_EQUAL(1.1, floats.double_());
    EXPECT_EQUAL(1.1f, floats.floatle());
    EXPECT_EQUAL(1.1, floats.doublele());
    EXPECT_EQUAL(1.1f, floats.floatbe());
    EXPECT_EQUAL(1.1, floats.doublebe());

    EXPECT_TRUE(floats.ParseFromString(std::string(EXPECTED_NEG_2_2, EXPECTED_NEG_2_2_SIZE)));
    EXPECT_EQUAL(-2.2f, floats.float_());
    EXPECT_EQUAL(-2.2, floats.double_());
    EXPECT_EQUAL(-2.2f, floats.floatle());
    EXPECT_EQUAL(-2.2, floats.doublele());
    EXPECT_EQUAL(-2.2f, floats.floatbe());
    EXPECT_EQUAL(-2.2, floats.doublebe());

    EXPECT_TRUE(floats.ParseFromString(std::string(EXPECTED_0_0, EXPECTED_0_0_SIZE)));
    EXPECT_EQUAL(0.0, floats.float_());
    EXPECT_EQUAL(0.0, floats.double_());
    EXPECT_EQUAL(0.0, floats.floatle());
    EXPECT_EQUAL(0.0, floats.doublele());
    EXPECT_EQUAL(0.0, floats.floatbe());
    EXPECT_EQUAL(0.0, floats.doublebe());

    return 0;
}

} // test
} // kpb
