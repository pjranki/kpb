#include "process_value_root.hpp"


ProcessValueRoot::ProcessValueRoot(const kpb_test::ProcessValue &root, uint8_t arg2) : root_(root)
{
    arg2_ = arg2;
}

const kpb_test::ProcessValue &ProcessValueRoot::getter(const kpb_test::ProcessValue &value)
{
    kpb_test::ProcessValue* root = const_cast<kpb_test::ProcessValue*>(&root_);
    if ( arg2_ == 0xaa )
    {
        root->set_getter_count(root->getter_count() + 1);
    }
    return value;
}

kpb_test::ProcessValue *ProcessValueRoot::setter(kpb_test::ProcessValue *value)
{
    kpb_test::ProcessValue* root = const_cast<kpb_test::ProcessValue*>(&root_);
    if ( arg2_ == 0xaa )
    {
        root->set_setter_count(root->setter_count() + 1);
    }
    return value;
}
