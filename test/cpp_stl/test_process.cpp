#include "test_process.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/process.hpp"

#define HELLOWORLD_ALIGN32_BUFFER \
    "\x20\x00\x00\x00\x20\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00" \
    "\xe2\xcf\xc6\xc6\xc5\xfd\xc5\xd8\xc6\xce\x8b\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
#define HELLOWORLD_ALIGN32_BUFFER_SIZE (sizeof(HELLOWORLD_ALIGN32_BUFFER)-1)

#define HELLOWORLD_ALIGN8_BUFFER \
    "\x08\x00\x00\x00\x00\x00\x00\x00" \
    "\xe2\xcf\xc6\xc6\xc5\xfd\xc5\xd8\xc6\xce\x8b\xaa\xaa\xaa\xaa\xaa"
#define HELLOWORLD_ALIGN8_BUFFER_SIZE (sizeof(HELLOWORLD_ALIGN8_BUFFER)-1)

namespace kpb {
namespace test {

int process(void)
{
    std::string result;
    kpb_test::ProcessBodySize_ process;
    kpb_test::ProcessConfig *config = nullptr;
    kpb_test::ProcessBody *body = nullptr;
    config = process.mutable_config();
    EXPECT_NOT_NULL(config);
    EXPECT_TRUE(config->set_alignment(32));
    body = process.mutable_body();
    EXPECT_NOT_NULL(body);
    EXPECT_TRUE(body->set_message("HelloWorld!"));
    EXPECT_EQUAL(32, process.body_size_());
    EXPECT_TRUE(process.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(
        HELLOWORLD_ALIGN32_BUFFER,
        HELLOWORLD_ALIGN32_BUFFER_SIZE,
        result.c_str(),
        result.size()
    );

    process.Clear();
    EXPECT_EQUAL(0, process.config().alignment());
    EXPECT_STRING_EQUAL("", process.body().message().c_str());
    EXPECT_EQUAL(1, process.body_size_());
    EXPECT_TRUE(process.ParseFromString(std::string(HELLOWORLD_ALIGN32_BUFFER, HELLOWORLD_ALIGN32_BUFFER_SIZE)));
    EXPECT_EQUAL(32, process.config().alignment());
    EXPECT_STRING_EQUAL("HelloWorld!", process.body().message().c_str());
    EXPECT_EQUAL(32, process.body_size_());

    kpb_test::ProcessBodySizeEos process_eos;
    config = process_eos.mutable_config();
    EXPECT_NOT_NULL(config);
    EXPECT_TRUE(config->set_alignment(8));
    body = process_eos.mutable_body();
    EXPECT_NOT_NULL(body);
    EXPECT_TRUE(body->set_message("HelloWorld!"));
    EXPECT_TRUE(process_eos.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(
        HELLOWORLD_ALIGN8_BUFFER,
        HELLOWORLD_ALIGN8_BUFFER_SIZE,
        result.c_str(),
        result.size()
    );

    process_eos.Clear();
    EXPECT_EQUAL(0, process_eos.config().alignment());
    EXPECT_STRING_EQUAL("", process_eos.body().message().c_str());

    EXPECT_TRUE(process_eos.ParseFromString(std::string(HELLOWORLD_ALIGN8_BUFFER, HELLOWORLD_ALIGN8_BUFFER_SIZE)));
    EXPECT_EQUAL(8, process_eos.config().alignment());
    EXPECT_STRING_EQUAL("HelloWorld!", process_eos.body().message().c_str());

    return 0;
}

} // test
} // kpb
