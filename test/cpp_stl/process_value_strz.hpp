#ifndef _H_PROCESS_VALUE_STRZ_CPP_H_
#define _H_PROCESS_VALUE_STRZ_CPP_H_

#include <cstdint>
#include <string>

#include "cpp_stl/process_value.hpp"

class ProcessValueStrz
{
    public:
        ProcessValueStrz(uint8_t memset_value);
        std::string getter(const std::string &value);
        std::string setter(std::string &value);
    private:
        uint8_t memset_value_;
};

#endif
