#include "test_bit_pos_size.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/bit_pos_size.hpp"

namespace kpb {
namespace test {

int bit_pos_size(void)
{
    kpb_test::BitPosSize_ obj;
    std::string result;
    size_t i = 0;

    obj.Clear();
    EXPECT_TRUE(obj.set_bit_count(6));
    EXPECT_TRUE(obj.set_bit_align(7));
    EXPECT_TRUE(obj.set_expected_pos_in_bits(8));
    EXPECT_TRUE(obj.set_expected_size_in_bits(8));
    for ( i = 0; i < obj.bit_count(); i++ )
    {
        EXPECT_TRUE(obj.add_bits(1));
    }
    EXPECT_TRUE(obj.set_end(1));
    EXPECT_TRUE(obj.add_ok(0x55));
    EXPECT_TRUE(obj.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xfd\x55", 2, result.c_str(), result.size());

    obj.Clear();
    EXPECT_TRUE(obj.set_bit_count(7));
    EXPECT_TRUE(obj.set_bit_align(7));
    EXPECT_TRUE(obj.set_expected_pos_in_bits(8));
    EXPECT_TRUE(obj.set_expected_size_in_bits(8));
    for ( i = 0; i < obj.bit_count(); i++ )
    {
        EXPECT_TRUE(obj.add_bits(1));
    }
    EXPECT_TRUE(obj.set_end(1));
    EXPECT_TRUE(obj.add_ok(0x55));
    EXPECT_TRUE(obj.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xff\x55", 2, result.c_str(), result.size());

    obj.Clear();
    EXPECT_TRUE(obj.set_bit_count(8));
    EXPECT_TRUE(obj.set_bit_align(7));
    EXPECT_TRUE(obj.set_expected_pos_in_bits(15));
    EXPECT_TRUE(obj.set_expected_size_in_bits(15));
    for ( i = 0; i < obj.bit_count(); i++ )
    {
        EXPECT_TRUE(obj.add_bits(1));
    }
    EXPECT_TRUE(obj.set_end(1));
    EXPECT_TRUE(obj.add_ok(0x55));
    EXPECT_TRUE(obj.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xff\x02\xaa", 3,result.c_str(), result.size());

    obj.Clear();
    EXPECT_TRUE(obj.set_bit_count(6));
    EXPECT_TRUE(obj.set_bit_align(7));
    EXPECT_TRUE(obj.set_expected_pos_in_bits(8));
    EXPECT_TRUE(obj.set_expected_size_in_bits(16));
    EXPECT_TRUE(obj.ParseFromString(std::string("\xfd\x55", 2)));
    for ( i = 0; i < obj.bit_count(); i++ )
    {
        EXPECT_EQUAL(1, obj.bits(i));
    }
    EXPECT_EQUAL(1, obj.padding_size());
    EXPECT_EQUAL(1, obj.end());
    EXPECT_EQUAL(0x55, obj.ok(0));

    obj.Clear();
    EXPECT_TRUE(obj.set_bit_count(7));
    EXPECT_TRUE(obj.set_bit_align(7));
    EXPECT_TRUE(obj.set_expected_pos_in_bits(8));
    EXPECT_TRUE(obj.set_expected_size_in_bits(16));
    EXPECT_TRUE(obj.ParseFromString(std::string("\xff\x55", 2)));
    for ( i = 0; i < obj.bit_count(); i++ )
    {
        EXPECT_EQUAL(1, obj.bits(i));
    }
    EXPECT_EQUAL(0, obj.padding_size());
    EXPECT_EQUAL(1, obj.end());
    EXPECT_EQUAL(0x55, obj.ok(0));

    obj.Clear();
    EXPECT_TRUE(obj.set_bit_count(8));
    EXPECT_TRUE(obj.set_bit_align(7));
    EXPECT_TRUE(obj.set_expected_pos_in_bits(15));
    EXPECT_TRUE(obj.set_expected_size_in_bits(24));
    EXPECT_TRUE(obj.ParseFromString(std::string("\xff\x02\xaa", 3)));
    for ( i = 0; i < obj.bit_count(); i++ )
    {
        EXPECT_EQUAL(1, obj.bits(i));
    }
    EXPECT_EQUAL(6, obj.padding_size());
    EXPECT_EQUAL(1, obj.end());
    EXPECT_EQUAL(0x55, obj.ok(0));

    return 0;
}

} // test
} // kpb
