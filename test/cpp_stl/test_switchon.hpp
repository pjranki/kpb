#ifndef _H_KPB_TEST_SWITCHON_H_
#define _H_KPB_TEST_SWITCHON_H_

namespace kpb {
namespace test {

int switchon(void);

} // test
} // kpb

#endif
