#ifndef _H_KPB_TEST_PARENT_CHILD_H_
#define _H_KPB_TEST_PARENT_CHILD_H_

namespace kpb {
namespace test {

int parent_child(void);

} // test
} // kpb

#endif
