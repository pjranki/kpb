#include "process_value_b3.hpp"


ProcessValueB3::ProcessValueB3(const kpb_test::ProcessValue &root) : root_(root)
{

}

uint8_t ProcessValueB3::getter(uint8_t value)
{
    return value + root_.memset_value();
}

uint8_t ProcessValueB3::setter(uint8_t value)
{
    return value - root_.memset_value();
}
