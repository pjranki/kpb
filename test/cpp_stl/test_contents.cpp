#include "test_contents.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/contents.hpp"

namespace kpb {
namespace test {

int contents(void)
{
    kpb_test::Contents contents;

    EXPECT_BYTES_EQUAL("\xCA\xFE\xBA\xBE", 4, contents.magic().c_str(), contents.magic().size());
    EXPECT_BYTES_EQUAL("\x4A\x46\x49\x46", 4, contents.magic1().c_str(), contents.magic1().size());
    EXPECT_BYTES_EQUAL("\xCA\xFE\xBA\xBE", 4, contents.magic2().c_str(), contents.magic2().size());
    EXPECT_BYTES_EQUAL("\x43\x41\x46\x45\x00\x42\x41\x42\x45", 9, contents.magic3().c_str(), contents.magic3().size());
    EXPECT_BYTES_EQUAL("\x66\x6F\x6F\x00\x41\x0A\x2A", 7, contents.magic4().c_str(), contents.magic4().size());
    EXPECT_BYTES_EQUAL("\x01\x55\x41\x2C\x33\x03", 6, contents.magic5().c_str(), contents.magic5().size());

    EXPECT_TRUE(contents.set_magic(std::string("", 0)));
    EXPECT_TRUE(contents.set_magic1(std::string("\x11", 1)));
    EXPECT_TRUE(contents.set_magic2(std::string("\x11\x22", 2)));
    EXPECT_TRUE(contents.set_magic3(std::string("\x11\x22\x33", 3)));
    EXPECT_TRUE(contents.set_magic4(std::string("\x11\x22\x33\x44", 4)));
    EXPECT_TRUE(contents.set_magic5(std::string("\x11\x22\x33\x44\x55", 5)));

    EXPECT_EQUAL(0, contents.magic().size());
    EXPECT_BYTES_EQUAL("\x11", 1, contents.magic1().c_str(), contents.magic1().size());
    EXPECT_BYTES_EQUAL("\x11\x22", 2, contents.magic2().c_str(), contents.magic2().size());
    EXPECT_BYTES_EQUAL("\x11\x22\x33", 3, contents.magic3().c_str(), contents.magic3().size());
    EXPECT_BYTES_EQUAL("\x11\x22\x33\x44", 4, contents.magic4().c_str(), contents.magic4().size());
    EXPECT_BYTES_EQUAL("\x11\x22\x33\x44\x55", 5, contents.magic5().c_str(), contents.magic5().size());

    std::string result;
    EXPECT_TRUE(contents.SerializeToString(&result));
    EXPECT_TRUE(contents.ParseFromString(result));

    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, contents.magic().c_str(), contents.magic().size());
    EXPECT_BYTES_EQUAL("\x11\x00\x00\x00", 4, contents.magic1().c_str(), contents.magic1().size());
    EXPECT_BYTES_EQUAL("\x11\x22\x00\x00", 4, contents.magic2().c_str(), contents.magic2().size());
    EXPECT_BYTES_EQUAL("\x11\x22\x33\x00\x00\x00\x00\x00\x00", 9, contents.magic3().c_str(), contents.magic3().size());
    EXPECT_BYTES_EQUAL("\x11\x22\x33\x44\x00\x00\x00", 7, contents.magic4().c_str(), contents.magic4().size());
    EXPECT_BYTES_EQUAL("\x11\x22\x33\x44\x55\x00", 6, contents.magic5().c_str(), contents.magic5().size());

    contents.Clear();

    EXPECT_BYTES_EQUAL("\xCA\xFE\xBA\xBE", 4, contents.magic().c_str(), contents.magic().size());
    EXPECT_BYTES_EQUAL("\x4A\x46\x49\x46", 4, contents.magic1().c_str(), contents.magic1().size());
    EXPECT_BYTES_EQUAL("\xCA\xFE\xBA\xBE", 4, contents.magic2().c_str(), contents.magic2().size());
    EXPECT_BYTES_EQUAL("\x43\x41\x46\x45\x00\x42\x41\x42\x45", 9, contents.magic3().c_str(), contents.magic3().size());
    EXPECT_BYTES_EQUAL("\x66\x6F\x6F\x00\x41\x0A\x2A", 7, contents.magic4().c_str(), contents.magic4().size());
    EXPECT_BYTES_EQUAL("\x01\x55\x41\x2C\x33\x03", 6, contents.magic5().c_str(), contents.magic5().size());

    return 0;
}

} // test
} // kpb
