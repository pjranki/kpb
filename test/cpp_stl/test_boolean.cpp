#include "test_boolean.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/boolean.hpp"

namespace kpb {
namespace test {

int boolean(void)
{
    kpb_test::Boolean value;
    EXPECT_TRUE(value.set_value1(true));
    EXPECT_TRUE(value.value1());
    EXPECT_TRUE(value.set_value1(false));
    EXPECT_FALSE(value.value1());
    return 0;
}

} // test
} // kpb
