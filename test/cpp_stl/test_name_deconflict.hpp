#ifndef _H_KPB_TEST_NAME_DECONFLICT_H_
#define _H_KPB_TEST_NAME_DECONFLICT_H_

namespace kpb {
namespace test {

int name_deconflict(void);

} // test
} // kpb

#endif
