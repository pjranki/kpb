#ifndef _H_PROCESS_VALUE_F4_CPP_H_
#define _H_PROCESS_VALUE_F4_CPP_H_

#include <cstdint>
#include <string>

#include "cpp_stl/process_value.hpp"

class ProcessValueF4
{
    public:
        ProcessValueF4(bool multiple_float_by_4);
        float getter(float value);
        float setter(float value);
    private:
        bool multiple_float_by_4_;
};

#endif
