#ifndef _H_KPB_TEST_RANDOMIZE_H_
#define _H_KPB_TEST_RANDOMIZE_H_

namespace kpb {
namespace test {

int randomize(void);

} // test
} // kpb

#endif
