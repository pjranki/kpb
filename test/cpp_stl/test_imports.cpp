#include "test_imports.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/imports.hpp"

namespace kpb {
namespace test {

int imports(void)
{
    kpb_test::Imports imports;
    std::string result;
    EXPECT_TRUE(imports.SerializeToString(&result));
    return 0;
}

} // test
} // kpb
