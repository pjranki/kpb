#include "test_bytes_list.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/bytes_list.hpp"

namespace kpb {
namespace test {

int bytes_list(void)
{
    kpb_test::BytesList bytes_list;
    std::string result;
    EXPECT_EQUAL(0, bytes_list.data2_list_size());
    EXPECT_EQUAL(0, bytes_list.data4_list_size());
    EXPECT_EQUAL(0, bytes_list.data2_list().size());
    EXPECT_EQUAL(0, bytes_list.data4_list().size());
    EXPECT_TRUE(bytes_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 30, result.c_str(), result.size());
    bytes_list.add_data2_list(std::string("A", 1));
    bytes_list.add_data4_list(std::string("a", 1));
    EXPECT_EQUAL(1, bytes_list.data2_list_size());
    EXPECT_EQUAL(1, bytes_list.data4_list_size());
    EXPECT_EQUAL(1, bytes_list.data2_list().size());
    EXPECT_EQUAL(1, bytes_list.data4_list().size());
    EXPECT_BYTES_EQUAL("A", 1, bytes_list.data2_list()[0].c_str(), bytes_list.data2_list()[0].size());
    EXPECT_BYTES_EQUAL("a", 1, bytes_list.data4_list()[0].c_str(), bytes_list.data4_list()[0].size());
    EXPECT_TRUE(bytes_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x41\x00\x00\x00\x00\x00\x00\x00\x00\x00\x61\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 30, result.c_str(), result.size());
    bytes_list.add_data2_list(std::string("BC", 2));
    bytes_list.add_data4_list(std::string("bc", 2));
    EXPECT_EQUAL(2, bytes_list.data2_list_size());
    EXPECT_EQUAL(2, bytes_list.data4_list_size());
    EXPECT_EQUAL(2, bytes_list.data2_list().size());
    EXPECT_EQUAL(2, bytes_list.data4_list().size());
    EXPECT_BYTES_EQUAL("A", 1, bytes_list.data2_list()[0].c_str(), bytes_list.data2_list()[0].size());
    EXPECT_BYTES_EQUAL("BC", 2, bytes_list.data2_list()[1].c_str(), bytes_list.data2_list()[1].size());
    EXPECT_BYTES_EQUAL("a", 1, bytes_list.data4_list()[0].c_str(), bytes_list.data4_list()[0].size());
    EXPECT_BYTES_EQUAL("bc", 2, bytes_list.data4_list()[1].c_str(), bytes_list.data4_list()[1].size());
    EXPECT_TRUE(bytes_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x41\x00\x42\x43\x00\x00\x00\x00\x00\x00\x61\x00\x00\x00\x62\x63\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 30, result.c_str(), result.size());
    bytes_list.add_data2_list(std::string("DEF", 3));
    bytes_list.add_data2_list(std::string("DEF", 3));
    bytes_list.add_data2_list(std::string("DEF", 3));
    bytes_list.add_data4_list(std::string("def", 3));
    bytes_list.add_data4_list(std::string("def", 3));
    bytes_list.add_data4_list(std::string("def", 3));
    EXPECT_EQUAL(5, bytes_list.data2_list_size());
    EXPECT_EQUAL(5, bytes_list.data4_list_size());
    EXPECT_EQUAL(5, bytes_list.data2_list().size());
    EXPECT_EQUAL(5, bytes_list.data4_list().size());
    EXPECT_BYTES_EQUAL("A", 1, bytes_list.data2_list()[0].c_str(), bytes_list.data2_list()[0].size());
    EXPECT_BYTES_EQUAL("BC", 2, bytes_list.data2_list()[1].c_str(), bytes_list.data2_list()[1].size());
    EXPECT_BYTES_EQUAL("DEF", 3, bytes_list.data2_list()[2].c_str(), bytes_list.data2_list()[2].size());
    EXPECT_BYTES_EQUAL("DEF", 3, bytes_list.data2_list()[3].c_str(), bytes_list.data2_list()[3].size());
    EXPECT_BYTES_EQUAL("DEF", 3, bytes_list.data2_list()[4].c_str(), bytes_list.data2_list()[4].size());
    EXPECT_BYTES_EQUAL("a", 1, bytes_list.data4_list()[0].c_str(), bytes_list.data4_list()[0].size());
    EXPECT_BYTES_EQUAL("bc", 2, bytes_list.data4_list()[1].c_str(), bytes_list.data4_list()[1].size());
    EXPECT_BYTES_EQUAL("def", 3, bytes_list.data4_list()[2].c_str(), bytes_list.data4_list()[2].size());
    EXPECT_BYTES_EQUAL("def", 3, bytes_list.data4_list()[3].c_str(), bytes_list.data4_list()[3].size());
    EXPECT_BYTES_EQUAL("def", 3, bytes_list.data4_list()[4].c_str(), bytes_list.data4_list()[4].size());
    EXPECT_TRUE(bytes_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x41\x00\x42\x43\x44\x45\x44\x45\x44\x45\x61\x00\x00\x00\x62\x63\x00\x00\x64\x65\x66\x00\x64\x65\x66\x00\x64\x65\x66\x00", 30, result.c_str(), result.size());
    bytes_list.add_data2_list(std::string("G", 1));
    bytes_list.add_data4_list(std::string("g", 1));
    EXPECT_EQUAL(6, bytes_list.data2_list_size());
    EXPECT_EQUAL(6, bytes_list.data4_list_size());
    EXPECT_EQUAL(6, bytes_list.data2_list().size());
    EXPECT_EQUAL(6, bytes_list.data4_list().size());
    EXPECT_BYTES_EQUAL("A", 1, bytes_list.data2_list()[0].c_str(), bytes_list.data2_list()[0].size());
    EXPECT_BYTES_EQUAL("BC", 2, bytes_list.data2_list()[1].c_str(), bytes_list.data2_list()[1].size());
    EXPECT_BYTES_EQUAL("DEF", 3, bytes_list.data2_list()[2].c_str(), bytes_list.data2_list()[2].size());
    EXPECT_BYTES_EQUAL("DEF", 3, bytes_list.data2_list()[3].c_str(), bytes_list.data2_list()[3].size());
    EXPECT_BYTES_EQUAL("DEF", 3, bytes_list.data2_list()[4].c_str(), bytes_list.data2_list()[4].size());
    EXPECT_BYTES_EQUAL("G", 1, bytes_list.data2_list()[5].c_str(), bytes_list.data2_list()[5].size());
    EXPECT_BYTES_EQUAL("a", 1, bytes_list.data4_list()[0].c_str(), bytes_list.data4_list()[0].size());
    EXPECT_BYTES_EQUAL("bc", 2, bytes_list.data4_list()[1].c_str(), bytes_list.data4_list()[1].size());
    EXPECT_BYTES_EQUAL("def", 3, bytes_list.data4_list()[2].c_str(), bytes_list.data4_list()[2].size());
    EXPECT_BYTES_EQUAL("def", 3, bytes_list.data4_list()[3].c_str(), bytes_list.data4_list()[3].size());
    EXPECT_BYTES_EQUAL("def", 3, bytes_list.data4_list()[4].c_str(), bytes_list.data4_list()[4].size());
    EXPECT_BYTES_EQUAL("g", 1, bytes_list.data4_list()[5].c_str(), bytes_list.data4_list()[5].size());
    EXPECT_TRUE(bytes_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x41\x00\x42\x43\x44\x45\x44\x45\x44\x45\x61\x00\x00\x00\x62\x63\x00\x00\x64\x65\x66\x00\x64\x65\x66\x00\x64\x65\x66\x00", 30, result.c_str(), result.size());

    bytes_list.Clear();
    EXPECT_TRUE(bytes_list.ParseFromString(std::string("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 30)));
    EXPECT_EQUAL(5, bytes_list.data2_list_size());
    EXPECT_EQUAL(5, bytes_list.data4_list_size());
    EXPECT_EQUAL(5, bytes_list.data2_list().size());
    EXPECT_EQUAL(5, bytes_list.data4_list().size());
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list()[0].c_str(), bytes_list.data2_list()[0].size());
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list()[1].c_str(), bytes_list.data2_list()[1].size());
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list()[2].c_str(), bytes_list.data2_list()[2].size());
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list()[3].c_str(), bytes_list.data2_list()[3].size());
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list()[4].c_str(), bytes_list.data2_list()[4].size());
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list()[0].c_str(), bytes_list.data4_list()[0].size());
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list()[1].c_str(), bytes_list.data4_list()[1].size());
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list()[2].c_str(), bytes_list.data4_list()[2].size());
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list()[3].c_str(), bytes_list.data4_list()[3].size());
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list()[4].c_str(), bytes_list.data4_list()[4].size());
    bytes_list.Clear();
    EXPECT_TRUE(bytes_list.ParseFromString(std::string("\x41\x00\x00\x00\x00\x00\x00\x00\x00\x00\x61\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 30)));
    EXPECT_EQUAL(5, bytes_list.data2_list_size());
    EXPECT_EQUAL(5, bytes_list.data4_list_size());
    EXPECT_EQUAL(5, bytes_list.data2_list().size());
    EXPECT_EQUAL(5, bytes_list.data4_list().size());
    EXPECT_BYTES_EQUAL("A\x00", 2, bytes_list.data2_list()[0].c_str(), bytes_list.data2_list()[0].size());
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list()[1].c_str(), bytes_list.data2_list()[1].size());
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list()[2].c_str(), bytes_list.data2_list()[2].size());
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list()[3].c_str(), bytes_list.data2_list()[3].size());
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list()[4].c_str(), bytes_list.data2_list()[4].size());
    EXPECT_BYTES_EQUAL("a\x00\x00\x00", 4, bytes_list.data4_list()[0].c_str(), bytes_list.data4_list()[0].size());
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list()[1].c_str(), bytes_list.data4_list()[1].size());
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list()[2].c_str(), bytes_list.data4_list()[2].size());
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list()[3].c_str(), bytes_list.data4_list()[3].size());
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list()[4].c_str(), bytes_list.data4_list()[4].size());
    bytes_list.Clear();
    EXPECT_TRUE(bytes_list.ParseFromString(std::string("\x41\x00\x42\x43\x00\x00\x00\x00\x00\x00\x61\x00\x00\x00\x62\x63\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 30)));
    EXPECT_EQUAL(5, bytes_list.data2_list_size());
    EXPECT_EQUAL(5, bytes_list.data4_list_size());
    EXPECT_EQUAL(5, bytes_list.data2_list().size());
    EXPECT_EQUAL(5, bytes_list.data4_list().size());
    EXPECT_BYTES_EQUAL("A\x00", 2, bytes_list.data2_list()[0].c_str(), bytes_list.data2_list()[0].size());
    EXPECT_BYTES_EQUAL("BC", 2, bytes_list.data2_list()[1].c_str(), bytes_list.data2_list()[1].size());
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list()[2].c_str(), bytes_list.data2_list()[2].size());
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list()[3].c_str(), bytes_list.data2_list()[3].size());
    EXPECT_BYTES_EQUAL("\x00\x00", 2, bytes_list.data2_list()[4].c_str(), bytes_list.data2_list()[4].size());
    EXPECT_BYTES_EQUAL("a\x00\x00\x00", 4, bytes_list.data4_list()[0].c_str(), bytes_list.data4_list()[0].size());
    EXPECT_BYTES_EQUAL("bc\x00\x00", 4, bytes_list.data4_list()[1].c_str(), bytes_list.data4_list()[1].size());
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list()[2].c_str(), bytes_list.data4_list()[2].size());
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list()[3].c_str(), bytes_list.data4_list()[3].size());
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00", 4, bytes_list.data4_list()[4].c_str(), bytes_list.data4_list()[4].size());
    bytes_list.Clear();
    EXPECT_TRUE(bytes_list.ParseFromString(std::string("\x41\x00\x42\x43\x44\x45\x44\x45\x44\x45\x61\x00\x00\x00\x62\x63\x00\x00\x64\x65\x66\x00\x64\x65\x66\x00\x64\x65\x66\x00", 30)));
    EXPECT_EQUAL(5, bytes_list.data2_list_size());
    EXPECT_EQUAL(5, bytes_list.data4_list_size());
    EXPECT_EQUAL(5, bytes_list.data2_list().size());
    EXPECT_EQUAL(5, bytes_list.data4_list().size());
    EXPECT_BYTES_EQUAL("A\x00", 2, bytes_list.data2_list()[0].c_str(), bytes_list.data2_list()[0].size());
    EXPECT_BYTES_EQUAL("BC", 2, bytes_list.data2_list()[1].c_str(), bytes_list.data2_list()[1].size());
    EXPECT_BYTES_EQUAL("DE", 2, bytes_list.data2_list()[2].c_str(), bytes_list.data2_list()[2].size());
    EXPECT_BYTES_EQUAL("DE", 2, bytes_list.data2_list()[3].c_str(), bytes_list.data2_list()[3].size());
    EXPECT_BYTES_EQUAL("DE", 2, bytes_list.data2_list()[4].c_str(), bytes_list.data2_list()[4].size());
    EXPECT_BYTES_EQUAL("a\x00\x00\x00", 4, bytes_list.data4_list()[0].c_str(), bytes_list.data4_list()[0].size());
    EXPECT_BYTES_EQUAL("bc\x00\x00", 4, bytes_list.data4_list()[1].c_str(), bytes_list.data4_list()[1].size());
    EXPECT_BYTES_EQUAL("def\x00", 4, bytes_list.data4_list()[2].c_str(), bytes_list.data4_list()[2].size());
    EXPECT_BYTES_EQUAL("def\x00", 4, bytes_list.data4_list()[3].c_str(), bytes_list.data4_list()[3].size());
    EXPECT_BYTES_EQUAL("def\x00", 4, bytes_list.data4_list()[4].c_str(), bytes_list.data4_list()[4].size());

    return 0;
}

} // test
} // kpb
