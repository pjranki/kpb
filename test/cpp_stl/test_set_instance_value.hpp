#ifndef _H_KPB_TEST_SET_INSTANCE_VALUE_H_
#define _H_KPB_TEST_SET_INSTANCE_VALUE_H_

namespace kpb {
namespace test {

int set_instance_value(void);

} // test
} // kpb

#endif
