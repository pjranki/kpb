#include "test_parent_child.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/parent_child.hpp"

namespace kpb {
namespace test {

int parent_child(void)
{
    kpb_test::ParentChild child;
    EXPECT_EQUAL(42, child.parent_id());
    return 0;
}

} // test
} // kpb
