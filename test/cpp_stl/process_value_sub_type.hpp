#ifndef _H_PROCESS_VALUE_SUB_TYPE_CPP_H_
#define _H_PROCESS_VALUE_SUB_TYPE_CPP_H_

#include <cstdint>
#include <string>

#include "cpp_stl/process_value.hpp"

class ProcessValueSubType
{
    public:
        ProcessValueSubType(const kpb_test::ProcessValue &root);
        const kpb_test::SubProcessValue &getter(const kpb_test::SubProcessValue &value);
        kpb_test::SubProcessValue *setter(kpb_test::SubProcessValue *value);
    private:
        const kpb_test::ProcessValue &root_;
};

#endif
