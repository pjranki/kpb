#include "test_strings_single.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/strings_single.hpp"
#include "cpp_stl/strings_utf16be.hpp"
#include "cpp_stl/strings_utf8.hpp"

namespace kpb {
namespace test {

int strings_single(void)
{
    kpb_test::StringsSingle strings;
    std::string result;
    EXPECT_STD_STRING_EQUAL(std::string(""), strings.str_ascii());
    EXPECT_STD_STRING_EQUAL(std::wstring(L""), strings.str_wchar());
    EXPECT_STD_STRING_EQUAL(std::string(""), strings.strz_ascii());
    EXPECT_STD_STRING_EQUAL(std::wstring(L""), strings.strz_wchar());
    EXPECT_STD_STRING_EQUAL(std::string(""), strings.strz_ascii_sized());
    EXPECT_STD_STRING_EQUAL(std::wstring(L""), strings.strz_wchar_sized());
    EXPECT_TRUE(strings.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 18, result.c_str(), result.size());
    strings.set_str_ascii("\xfe");
    strings.set_str_wchar(L"\u8cfe");
    strings.set_strz_ascii("\xfe");
    strings.set_strz_wchar(L"\u8cfe");
    strings.set_strz_ascii_sized("\xfe");
    strings.set_strz_wchar_sized(L"\u8cfe");
    EXPECT_TRUE(strings.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xfe\x00\xfe\x8c\x00\x00\xfe\x00\xfe\x8c\x00\x00\xfe\x00\x00\xfe\x8c\x00\x00\x00\x00", 21, result.c_str(), result.size());
    strings.Clear();
    EXPECT_TRUE(strings.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 18, result.c_str(), result.size());
    strings.set_str_ascii("\xfe\xba");
    strings.set_str_wchar(L"\u8cfe\u09ba");
    strings.set_strz_ascii("\xfe\xba");
    strings.set_strz_wchar(L"\u8cfe\u09ba");
    strings.set_strz_ascii_sized("\xfe\xba");
    strings.set_strz_wchar_sized(L"\u8cfe\u09ba");
    EXPECT_TRUE(strings.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xfe\xba\xfe\x8c\xba\x09\xfe\xba\x00\xfe\x8c\xba\x09\x00\x00\xfe\xba\x00\xfe\x8c\xba\x09\x00\x00", 24, result.c_str(), result.size());
    strings.set_str_ascii("\xfe\xba\x09");
    strings.set_str_wchar(L"\u8cfe\u09ba\u6587");
    strings.set_strz_ascii("\xfe\xba\x09");
    strings.set_strz_wchar(L"\u8cfe\u09ba\u6587");
    strings.set_strz_ascii_sized("\xfe\xba\x09");
    strings.set_strz_wchar_sized(L"\u8cfe\u09ba\u6587");
    EXPECT_TRUE(strings.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xfe\xba\xfe\x8c\xba\x09\xfe\xba\x09\x00\xfe\x8c\xba\x09\x87\x65\x00\x00\xfe\xba\x00\xfe\x8c\xba\x09\x00\x00", 27, result.c_str(), result.size());

    strings.Clear();
    EXPECT_TRUE(strings.ParseFromString(std::string("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 18)));
    EXPECT_STD_STRING_EQUAL(std::string(""), strings.str_ascii());
    EXPECT_STD_STRING_EQUAL(std::wstring(L""), strings.str_wchar());
    EXPECT_STD_STRING_EQUAL(std::string(""), strings.strz_ascii());
    EXPECT_STD_STRING_EQUAL(std::wstring(L""), strings.strz_wchar());
    EXPECT_STD_STRING_EQUAL(std::string(""), strings.strz_ascii_sized());
    EXPECT_STD_STRING_EQUAL(std::wstring(L""), strings.strz_wchar_sized());
    EXPECT_TRUE(strings.ParseFromString(std::string("\xfe\x00\xfe\x8c\x00\x00\xfe\x00\xfe\x8c\x00\x00\xfe\x00\x00\xfe\x8c\x00\x00\x00\x00", 21)));
    EXPECT_STD_STRING_EQUAL(std::string("\xfe"), strings.str_ascii());
    EXPECT_STD_STRING_EQUAL(std::wstring(L"\u8cfe"), strings.str_wchar());
    EXPECT_STD_STRING_EQUAL(std::string("\xfe"), strings.strz_ascii());
    EXPECT_STD_STRING_EQUAL(std::wstring(L"\u8cfe"), strings.strz_wchar());
    EXPECT_STD_STRING_EQUAL(std::string("\xfe"), strings.strz_ascii_sized());
    EXPECT_STD_STRING_EQUAL(std::wstring(L"\u8cfe"), strings.strz_wchar_sized());
    EXPECT_TRUE(strings.ParseFromString(std::string("\xfe\xba\xfe\x8c\xba\x09\xfe\xba\x00\xfe\x8c\xba\x09\x00\x00\xfe\xba\x00\xfe\x8c\xba\x09\x00\x00", 24)));
    EXPECT_STD_STRING_EQUAL(std::string("\xfe\xba"), strings.str_ascii());
    EXPECT_STD_STRING_EQUAL(std::wstring(L"\u8cfe\u09ba"), strings.str_wchar());
    EXPECT_STD_STRING_EQUAL(std::string("\xfe\xba"), strings.strz_ascii());
    EXPECT_STD_STRING_EQUAL(std::wstring(L"\u8cfe\u09ba"), strings.strz_wchar());
    EXPECT_STD_STRING_EQUAL(std::string("\xfe\xba"), strings.strz_ascii_sized());
    EXPECT_STD_STRING_EQUAL(std::wstring(L"\u8cfe\u09ba"), strings.strz_wchar_sized());
    EXPECT_TRUE(strings.ParseFromString(std::string("\xfe\xba\xfe\x8c\xba\x09\xfe\xba\x09\x00\xfe\x8c\xba\x09\x87\x65\x00\x00\xfe\xba\x00\xfe\x8c\xba\x09\x00\x00", 27)));
    EXPECT_STD_STRING_EQUAL(std::string("\xfe\xba"), strings.str_ascii());
    EXPECT_STD_STRING_EQUAL(std::wstring(L"\u8cfe\u09ba"), strings.str_wchar());
    EXPECT_STD_STRING_EQUAL(std::string("\xfe\xba\x09"), strings.strz_ascii());
    EXPECT_STD_STRING_EQUAL(std::wstring(L"\u8cfe\u09ba\u6587"), strings.strz_wchar());
    EXPECT_STD_STRING_EQUAL(std::string("\xfe\xba"), strings.strz_ascii_sized());
    EXPECT_STD_STRING_EQUAL(std::wstring(L"\u8cfe\u09ba"), strings.strz_wchar_sized());

    kpb_test::StringsUtf16be utf16be;

    EXPECT_TRUE(utf16be.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00" "\x00\x00" "\x00\x00\x00\x00\x00\x00", 4 + 2 + 6, result.c_str(), result.size());
    utf16be.set_text(L"\u8cfe");
    utf16be.set_textz(L"\u8cfe");
    utf16be.set_textz_sized(L"\u8cfe");
    EXPECT_TRUE(utf16be.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x8c\xfe\x00\x00" "\x8c\xfe\x00\x00" "\x8c\xfe\x00\x00\x00\x00", 4 + 4 + 6, result.c_str(), result.size());
    utf16be.Clear();
    EXPECT_TRUE(utf16be.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00" "\x00\x00" "\x00\x00\x00\x00\x00\x00", 4 + 2 + 6, result.c_str(), result.size());
    utf16be.set_text(L"\u8cfe\u09ba");
    utf16be.set_textz(L"\u8cfe\u09ba");
    utf16be.set_textz_sized(L"\u8cfe\u09ba");
    EXPECT_TRUE(utf16be.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x8c\xfe\x09\xba" "\x8c\xfe\x09\xba\x00\x00" "\x8c\xfe\x09\xba\x00\x00", 4 + 6 + 6, result.c_str(), result.size());
    utf16be.set_text(L"\u8cfe\u09ba\u6587");
    utf16be.set_textz(L"\u8cfe\u09ba\u6587");
    utf16be.set_textz_sized(L"\u8cfe\u09ba\u6587");
    EXPECT_TRUE(utf16be.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x8c\xfe\x09\xba" "\x8c\xfe\x09\xba\x65\x87\x00\x00" "\x8c\xfe\x09\xba\x00\x00", 4 + 8 + 6, result.c_str(), result.size());

    utf16be.Clear();
    EXPECT_TRUE(utf16be.ParseFromString(std::string("\x00\x00\x00\x00" "\x00\x00" "\x00\x00\x00\x00\x00\x00", 4 + 2 + 6)));
    EXPECT_STD_STRING_EQUAL(std::wstring(L""), utf16be.text());
    EXPECT_STD_STRING_EQUAL(std::wstring(L""), utf16be.textz());
    EXPECT_STD_STRING_EQUAL(std::wstring(L""), utf16be.textz_sized());
    EXPECT_TRUE(utf16be.ParseFromString(std::string("\x8c\xfe\x00\x00" "\x8c\xfe\x00\x00" "\x8c\xfe\x00\x00\x00\x00", 4 + 4 + 6)));
    EXPECT_STD_STRING_EQUAL(std::wstring(L"\u8cfe"), utf16be.text());
    EXPECT_STD_STRING_EQUAL(std::wstring(L"\u8cfe"), utf16be.textz());
    EXPECT_STD_STRING_EQUAL(std::wstring(L"\u8cfe"), utf16be.textz_sized());
    EXPECT_TRUE(utf16be.ParseFromString(std::string("\x8c\xfe\x09\xba" "\x8c\xfe\x09\xba\x00\x00" "\x8c\xfe\x09\xba\x00\x00", 4 + 6 + 6)));
    EXPECT_STD_STRING_EQUAL(std::wstring(L"\u8cfe\u09ba"), utf16be.text());
    EXPECT_STD_STRING_EQUAL(std::wstring(L"\u8cfe\u09ba"), utf16be.textz());
    EXPECT_STD_STRING_EQUAL(std::wstring(L"\u8cfe\u09ba"), utf16be.textz_sized());
    EXPECT_TRUE(utf16be.ParseFromString(std::string("\x8c\xfe\x09\xba" "\x8c\xfe\x09\xba\x65\x87\x00\x00" "\x8c\xfe\x09\xba\x00\x00", 4 + 8 + 6)));
    EXPECT_STD_STRING_EQUAL(std::wstring(L"\u8cfe\u09ba"), utf16be.text());
    EXPECT_STD_STRING_EQUAL(std::wstring(L"\u8cfe\u09ba\u6587"), utf16be.textz());
    EXPECT_STD_STRING_EQUAL(std::wstring(L"\u8cfe\u09ba"), utf16be.textz_sized());

    kpb_test::StringsUtf8 utf8;

    EXPECT_TRUE(utf8.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00\x00" "\x00" "\x00\x00\x00", 2 + 1 + 3, result.c_str(), result.size());
    utf8.set_text("\xfe");
    utf8.set_textz("\xfe");
    utf8.set_textz_sized("\xfe");
    EXPECT_TRUE(utf8.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xfe\x00" "\xfe\x00" "\xfe\x00\x00", 2 + 2 + 3, result.c_str(), result.size());
    utf8.Clear();
    EXPECT_TRUE(utf8.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00\x00" "\x00" "\x00\x00\x00", 2 + 1 + 3, result.c_str(), result.size());
    utf8.set_text("\xfe\xba");
    utf8.set_textz("\xfe\xba");
    utf8.set_textz_sized("\xfe\xba");
    EXPECT_TRUE(utf8.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xfe\xba" "\xfe\xba\x00" "\xfe\xba\x00", 2 + 3 + 3, result.c_str(), result.size());
    utf8.set_text("\xfe\xba\x09");
    utf8.set_textz("\xfe\xba\x09");
    utf8.set_textz_sized("\xfe\xba\x09");
    EXPECT_TRUE(utf8.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xfe\xba" "\xfe\xba\x09\x00" "\xfe\xba\x00", 2 + 4 + 3, result.c_str(), result.size());

    utf8.Clear();
    EXPECT_TRUE(utf8.ParseFromString(std::string("\x00\x00" "\x00" "\x00\x00\x00", 2 + 1 + 3)));
    EXPECT_STD_STRING_EQUAL(std::string(""), utf8.text());
    EXPECT_STD_STRING_EQUAL(std::string(""), utf8.textz());
    EXPECT_STD_STRING_EQUAL(std::string(""), utf8.textz_sized());
    EXPECT_TRUE(utf8.ParseFromString(std::string("\xfe\x00" "\xfe\x00" "\xfe\x00\x00", 2 + 2 + 3)));
    EXPECT_STD_STRING_EQUAL(std::string("\xfe"), utf8.text());
    EXPECT_STD_STRING_EQUAL(std::string("\xfe"), utf8.textz());
    EXPECT_STD_STRING_EQUAL(std::string("\xfe"), utf8.textz_sized());
    EXPECT_TRUE(utf8.ParseFromString(std::string("\xfe\xba" "\xfe\xba\x00" "\xfe\xba\x00", 2 + 3 + 3)));
    EXPECT_STD_STRING_EQUAL(std::string("\xfe\xba"), utf8.text());
    EXPECT_STD_STRING_EQUAL(std::string("\xfe\xba"), utf8.textz());
    EXPECT_STD_STRING_EQUAL(std::string("\xfe\xba"), utf8.textz_sized());
    EXPECT_TRUE(utf8.ParseFromString(std::string("\xfe\xba" "\xfe\xba\x09\x00" "\xfe\xba\x00", 2 + 4 + 3)));
    EXPECT_STD_STRING_EQUAL(std::string("\xfe\xba"), utf8.text());
    EXPECT_STD_STRING_EQUAL(std::string("\xfe\xba\x09"), utf8.textz());
    EXPECT_STD_STRING_EQUAL(std::string("\xfe\xba"), utf8.textz_sized());

    return 0;
}

} // test
} // kpb
