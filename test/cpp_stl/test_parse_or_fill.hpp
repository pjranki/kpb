#ifndef _H_KPB_TEST_PARSE_OR_FILL_H_
#define _H_KPB_TEST_PARSE_OR_FILL_H_

namespace kpb {
namespace test {

int parse_or_fill(void);

} // test
} // kpb

#endif
