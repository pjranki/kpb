#ifndef _H_KPB_TEST_BOOLEAN_H_
#define _H_KPB_TEST_BOOLEAN_H_

namespace kpb {
namespace test {

int boolean(void);

} // test
} // kpb

#endif
