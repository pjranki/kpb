#include "xor_encoding.hpp"

#include <kpb_cpp_stl_io.hpp>


XorEncoding::XorEncoding(uint8_t xor_key, const kpb_test::ProcessConfig &config) : config_(config)
{
    this->xor_key_ = xor_key;
}

size_t XorEncoding::encode_size(kpb::IO &_io)
{
    // to calculate the encoded size, we will just encode the buffer and then get the size
    if ( !encode(_io) )
    {
        return 0;
    }
    return _io.size();
}

bool XorEncoding::encode(kpb::IO &_io)
{
    size_t alignment = config_.alignment();
    if ( alignment == 0 )
    {
        // no divide by zero please
        alignment = 1;
    }

    // get a copy of the buffer in the stream
    std::string buffer = _io.get();

    // create a buffer that will be big enough to hold the encoded output
    size_t buffer_size = buffer.size();
    if ( ( buffer_size % alignment ) != 0 )
    {
        buffer_size += ( alignment - ( buffer_size % alignment ) );
    }
    buffer.resize(buffer_size, '\0');

    // xor encode
    for ( size_t i = 0; i < buffer.size(); i++ )
    {
        ((uint8_t*)buffer.c_str())[i] ^= xor_key_;
    }

    // set the encode buffer
    _io.set(buffer);

    // done
    return true;
}

bool XorEncoding::decode(kpb::IO &_io)
{
    // check that buffer is correctly aligned
    size_t alignment = config_.alignment();
    if ( alignment == 0 )
    {
        // no divide by zero please
        alignment = 1;
    }

    // get a copy of the input buffer
    std::string buffer = _io.get();

    // check buffer is correctly aligned
    if ( ( buffer.size() % alignment) != 0 )
    {
        return false;
    }

    // xor decode
    for ( size_t i = 0; i < buffer.size(); i++ )
    {
        ((uint8_t*)buffer.c_str())[i] ^= xor_key_;
    }

    // set the decoded buffer
    _io.set(buffer);

    // done
    return true;
}
