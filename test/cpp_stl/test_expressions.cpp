#include "test_expressions.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/expressions.hpp"

namespace kpb {
namespace test {

int expressions(void)
{
    kpb_test::Expressions expressions;
    kpb_test::ExprObj *obj = nullptr;
    std::string result;

    EXPECT_TRUE(expressions.add_list_numbers(0xdeadbeef));
    EXPECT_TRUE(expressions.add_list_numbers(0xcafebabe));
    obj = expressions.add_list_objs();
    EXPECT_NOT_NULL(obj);
    obj->set_value1(0xdead0001);
    obj = expressions.add_list_objs();
    EXPECT_NOT_NULL(obj);
    obj->set_value1(0xdead0002);
    EXPECT_TRUE(obj->add_list_numbers(0xdeadbeef));
    EXPECT_TRUE(obj->add_list_numbers(0xbabebeef));
    obj = expressions.add_list_objs();
    EXPECT_NOT_NULL(obj);
    obj->set_value1(0xdeadcafe);

    EXPECT_TRUE(expressions.SerializeToString(&result));

    EXPECT_EQUAL(5, expressions.op_addition());
    EXPECT_EQUAL(3, expressions.op_subtract());
    EXPECT_EQUAL(20, expressions.op_multiplication());
    EXPECT_EQUAL(5, expressions.op_division());
    EXPECT_EQUAL(1, expressions.op_modulo());
    EXPECT_EQUAL(true, expressions.op_relational_lt_true());
    EXPECT_EQUAL(false, expressions.op_relational_lt_false());
    EXPECT_EQUAL(true, expressions.op_relational_gt_true());
    EXPECT_EQUAL(false, expressions.op_relational_gt_false());
    EXPECT_EQUAL(true, expressions.op_relational_lte_true());
    EXPECT_EQUAL(false, expressions.op_relational_lte_false());
    EXPECT_EQUAL(true, expressions.op_relational_gte_true());
    EXPECT_EQUAL(false, expressions.op_relational_gte_false());
    EXPECT_EQUAL(true, expressions.op_relational_eq_true());
    EXPECT_EQUAL(false, expressions.op_relational_eq_false());
    EXPECT_EQUAL(true, expressions.op_relational_ne_true());
    EXPECT_EQUAL(false, expressions.op_relational_ne_false());
    EXPECT_EQUAL(4, expressions.op_bitwise_lsh());
    EXPECT_EQUAL(4, expressions.op_bitwise_rsh());
    EXPECT_EQUAL(0x10, expressions.op_bitwise_and());
    EXPECT_EQUAL(0x0c, expressions.op_bitwise_or());
    EXPECT_EQUAL(0x55, expressions.op_bitwise_xor());
    EXPECT_EQUAL(0x0f, expressions.op_bitwise_inv());
    EXPECT_EQUAL(true, expressions.op_logical_and_true());
    EXPECT_EQUAL(false, expressions.op_logical_and_false());
    EXPECT_EQUAL(true, expressions.op_logical_or_true());
    EXPECT_EQUAL(false, expressions.op_logical_or_false());
    EXPECT_EQUAL(true, expressions.op_logical_not_true());
    EXPECT_EQUAL(false, expressions.op_logical_not_false());
    EXPECT_EQUAL(true, expressions.op_enum_eq_true());
    EXPECT_EQUAL(1, expressions.op_ternary_choose_1());
    EXPECT_EQUAL(2, expressions.op_ternary_choose_2());
    EXPECT_EQUAL(3, expressions.op_ternary_choose_3());
    EXPECT_EQUAL(4, expressions.op_ternary_choose_4());
    EXPECT_EQUAL(1, expressions.op_list_index());
    EXPECT_EQUAL(0xcafebabe, expressions.op_list_number());
    EXPECT_EQUAL(0xdeadcafe, expressions.op_list_obj());
    EXPECT_EQUAL(0xbabebeef, expressions.op_list_nested());
    EXPECT_EQUAL(47, expressions.op_order_47());
    EXPECT_EQUAL(37, expressions.op_order_37());
    EXPECT_EQUAL(65, expressions.op_order_65());
    EXPECT_EQUAL(77, expressions.op_order_77());
    EXPECT_EQUAL(24, expressions.op_same_op_24());
    EXPECT_EQUAL(true, expressions.op_logic_order_true());

    return 0;
}

} // test
} // kpb
