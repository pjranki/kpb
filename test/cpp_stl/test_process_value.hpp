#ifndef _H_KPB_TEST_PROCESS_VALUE_H_
#define _H_KPB_TEST_PROCESS_VALUE_H_

namespace kpb {
namespace test {

int process_value(void);

} // test
} // kpb

#endif
