#include "test_enums.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/enums.hpp"

namespace kpb {
namespace test {

int enums(void)
{
    kpb_test::Enums enums;
    std::string result;
    EXPECT_TRUE(enums.SerializeToString(&result));

    kpb_test::SubEnums sub_enums;
    EXPECT_TRUE(sub_enums.SerializeToString(&result));

    uint8_t test_value8 = kpb_test::Enums::TEST_ENUM_FOOBAR;
    EXPECT_EQUAL(1, test_value8);
    test_value8 = kpb_test::Enums::TEST_ENUM_CAFEBABE;
    EXPECT_EQUAL(2, test_value8);
    uint16_t test_value16 = kpb_test::SubEnums::TEST_ENUM_FOOBAR;
    EXPECT_EQUAL(0xffff, test_value16);
    test_value16 = kpb_test::SubEnums::TEST_ENUM_CAFEBABE;
    EXPECT_EQUAL(0x75a5, test_value16);

    uint8_t test_cast_8 = kpb_test::Enums::TEST_ENUM_CAFEBABE;
    EXPECT_EQUAL(2, test_cast_8);
    uint16_t test_cast_16 = kpb_test::Enums::TEST_ENUM_CAFEBABE;
    EXPECT_EQUAL(2, test_cast_16);
    uint32_t test_cast_32 = kpb_test::SubEnums::TEST_ENUM_CAFEBABE;
    EXPECT_EQUAL(0x75a5, test_cast_32);
    uint64_t test_cast_64 = kpb_test::SubEnums::TEST_ENUM_FOOBAR;
    EXPECT_EQUAL(0xffff, test_cast_64);

    return 0;
}

} // test
} // kpb
