#include "test_constructor_recursion.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/constructor_recursion.hpp"

namespace kpb {
namespace test {

int constructor_recursion(void)
{
    kpb_test::ConstructorRecursion obj;
    obj.Clear();
    return 0;
}

} // test
} // kpb
