#include "test_bitfield.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/bitfield.hpp"

namespace kpb {
namespace test {

#define BYTES_ZERO_16 "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
#define BYTES_ZERO_16_SIZE (sizeof(BYTES_ZERO_16)-1)
#define BYTES_ONES_16 "\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xfe"
#define BYTES_ONES_16_SIZE (sizeof(BYTES_ONES_16)-1)

int bitfield_single(void)
{
    kpb_test::Bitfield bitfield;
    std::string result;
    EXPECT_TRUE(bitfield.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(BYTES_ZERO_16, BYTES_ZERO_16_SIZE, result.c_str(), result.size());
    EXPECT_TRUE(bitfield.set_bitfield_b1(0x1));
    EXPECT_TRUE(bitfield.set_bitfield_b2(0x3));
    EXPECT_TRUE(bitfield.set_bitfield_b4(0xf));
    EXPECT_TRUE(bitfield.set_bitfield_b8(0xff));
    EXPECT_TRUE(bitfield.set_bitfield_b16(0xffff));
    EXPECT_TRUE(bitfield.set_bitfield_b32(0xffffffff));
    EXPECT_TRUE(bitfield.set_bitfield_b64(0xffffffffffffffff));
    EXPECT_TRUE(bitfield.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(BYTES_ONES_16, BYTES_ONES_16_SIZE, result.c_str(), result.size());
    bitfield.Clear();
    EXPECT_TRUE(bitfield.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(BYTES_ZERO_16, BYTES_ZERO_16_SIZE, result.c_str(), result.size());
    EXPECT_TRUE(bitfield.set_bitfield_b1(0x1));
    EXPECT_TRUE(bitfield.set_bitfield_b2(0x2));
    EXPECT_TRUE(bitfield.set_bitfield_b4(0x5));
    EXPECT_TRUE(bitfield.set_bitfield_b8(0xaa));
    EXPECT_TRUE(bitfield.set_bitfield_b16(0x5555));
    EXPECT_TRUE(bitfield.set_bitfield_b32(0xaaaaaaaa));
    EXPECT_TRUE(bitfield.set_bitfield_b64(0x5555555555555555));
    EXPECT_TRUE(bitfield.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xcb\x54\xaa\xab\x55\x55\x55\x54\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa", 16, result.c_str(), result.size());
    EXPECT_TRUE(bitfield.set_bitfield_b1(0x0));
    EXPECT_TRUE(bitfield.set_bitfield_b2(0x1));
    EXPECT_TRUE(bitfield.set_bitfield_b4(0xa));
    EXPECT_TRUE(bitfield.set_bitfield_b8(0x55));
    EXPECT_TRUE(bitfield.set_bitfield_b16(0xaaaa));
    EXPECT_TRUE(bitfield.set_bitfield_b32(0x55555555));
    EXPECT_TRUE(bitfield.set_bitfield_b64(0xaaaaaaaaaaaaaaaa));
    EXPECT_TRUE(bitfield.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x34\xab\x55\x54\xaa\xaa\xaa\xab\x55\x55\x55\x55\x55\x55\x55\x54", 16, result.c_str(), result.size());

    bitfield.Clear();
    EXPECT_EQUAL(bitfield.bitfield_b1(), 0);
    EXPECT_EQUAL(bitfield.bitfield_b2(), 0);
    EXPECT_EQUAL(bitfield.bitfield_b4(), 0);
    EXPECT_EQUAL(bitfield.bitfield_b8(), 0);
    EXPECT_EQUAL(bitfield.bitfield_b16(), 0);
    EXPECT_EQUAL(bitfield.bitfield_b32(), 0);
    EXPECT_EQUAL(bitfield.bitfield_b64(), 0);
    EXPECT_TRUE(bitfield.ParseFromString(std::string("\xcb\x54\xaa\xab\x55\x55\x55\x54\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa", 16)));
    EXPECT_EQUAL(bitfield.bitfield_b1(), 0x1);
    EXPECT_EQUAL(bitfield.bitfield_b2(), 0x2);
    EXPECT_EQUAL(bitfield.bitfield_b4(), 0x5);
    EXPECT_EQUAL(bitfield.bitfield_b8(), 0xaa);
    EXPECT_EQUAL(bitfield.bitfield_b16(), 0x5555);
    EXPECT_EQUAL(bitfield.bitfield_b32(), 0xaaaaaaaa);
    EXPECT_EQUAL(bitfield.bitfield_b64(), 0x5555555555555555);
    EXPECT_TRUE(bitfield.ParseFromString(std::string("\x34\xab\x55\x54\xaa\xaa\xaa\xab\x55\x55\x55\x55\x55\x55\x55\x55", 16)));
    EXPECT_EQUAL(bitfield.bitfield_b1(), 0x0);
    EXPECT_EQUAL(bitfield.bitfield_b2(), 0x1);
    EXPECT_EQUAL(bitfield.bitfield_b4(), 0xa);
    EXPECT_EQUAL(bitfield.bitfield_b8(), 0x55);
    EXPECT_EQUAL(bitfield.bitfield_b16(), 0xaaaa);
    EXPECT_EQUAL(bitfield.bitfield_b32(), 0x55555555);
    EXPECT_EQUAL(bitfield.bitfield_b64(), 0xaaaaaaaaaaaaaaaa);
    return 0;
}

int bitfield(void)
{
    int err = 0;

    err = bitfield_single();
    if ( err ) return err;

    return err;
}

} // test
} // kpb
