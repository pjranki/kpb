#include "test_update.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/update.hpp"

namespace kpb {
namespace test {

int update(void)
{
    kpb_test::ObjUpdate update;
    kpb_test::SingleParent *single = nullptr;
    kpb_test::MultiParent *multi = nullptr;
    std::string result;

    // constructor should not call update (need to use '-default' on values)
    EXPECT_TRUE(update.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(
        "\x20\x00\x00\x00" "\x00\x00\x00\x00" "\x00\x00\x00\x00" \
        "\x00\x00" "" \
        "\x00\x00" \
        "\x00" "" \
        "\x01" "\x00" \
        "\x00\x00" \
        "\x00" "" \
        "\x02" "\x00\x00" \
        "\x00\x00" \
        "\x00" "" \
        "\x00" \
        "\x00" "" \
        "\x00" "" \
        "\x00",
        32,
        result.c_str(),
        result.size()
    );
    EXPECT_EQUAL(32, update.total_size1());
    EXPECT_EQUAL(1, update.asciiz_size1());
    EXPECT_EQUAL(2, update.widez_size1());

    // clear calls update
    update.Clear();
    EXPECT_TRUE(update.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(
        "\x20\x00\x00\x00" "\x00\x00\x00\x00" "\x00\x00\x00\x00" \
        "\x00\x00" "" \
        "\x00\x00" \
        "\x00" "" \
        "\x01" "\x00" \
        "\x00\x00" \
        "\x00" "" \
        "\x02" "\x00\x00" \
        "\x00\x00" \
        "\x00" "" \
        "\x00" \
        "\x00" "" \
        "\x00" "" \
        "\x00",
        32,
        result.c_str(),
        result.size()
    );
    EXPECT_EQUAL(32, update.total_size1());
    EXPECT_EQUAL(1, update.asciiz_size1());
    EXPECT_EQUAL(2, update.widez_size1());

    // change some values - others are (automatically) updated
    EXPECT_TRUE(update.set_total_size1(0xdeadbeef));
    EXPECT_TRUE(update.set_flags(0xdeadbeef));
    EXPECT_TRUE(update.set_unused(0xcafebabe));
    EXPECT_TRUE(update.set_buffer(std::string("\xab\x00\xcd", 3)));
    EXPECT_TRUE(update.add_buffer_list(std::string("\x12\x34\x56\x78\x90", 5)));
    EXPECT_TRUE(update.set_ascii("A"));
    EXPECT_TRUE(update.set_asciiz("BC"));
    EXPECT_TRUE(update.add_ascii_list("DEF"));
    EXPECT_TRUE(update.set_wide(L"a"));
    EXPECT_TRUE(update.set_widez(L"bc"));
    EXPECT_TRUE(update.add_wide_list(L"def"));

    single = update.add_single_data_list();
    EXPECT_NOT_NULL(single);
    EXPECT_TRUE(single->set_data(std::string("\x33\x33\x33", 3)));

    multi = update.add_multi_data_list();
    EXPECT_NOT_NULL(multi);
    EXPECT_TRUE(multi->set_data(std::string("\x55\x55\x55\x55\x55", 5)));

    multi = update.mutable_multi_data();
    EXPECT_NOT_NULL(multi);
    EXPECT_TRUE(multi->set_data(std::string("\x44\x44\x44\x44", 4)));
    single = update.mutable_single_data();
    EXPECT_NOT_NULL(single);
    EXPECT_TRUE(single->set_data(std::string("\x11", 1)));
    multi = single->mutable_multi_data();
    EXPECT_NOT_NULL(multi);
    EXPECT_TRUE(multi->set_data(std::string("\x22\x22", 2)));

    EXPECT_EQUAL(74, update.total_size1());
    EXPECT_EQUAL(0xdeadbeef, update.flags());
    EXPECT_EQUAL(0, update.unused());
    EXPECT_EQUAL(3, update.buffer_size1());
    EXPECT_EQUAL(1, update.buffer_list_count());
    EXPECT_EQUAL(1, update.ascii_size1());
    EXPECT_EQUAL(3, update.asciiz_size1());
    EXPECT_EQUAL(1, update.ascii_list_count());
    EXPECT_EQUAL(2, update.wide_size1());
    EXPECT_EQUAL(6, update.widez_size1());
    EXPECT_EQUAL(1, update.wide_list_count());

    EXPECT_TRUE(update.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(
        "\x4a\x00\x00\x00" "\xef\xbe\xad\xde" "\x00\x00\x00\x00" \
        "\x03\x00" "\xab\x00\xcd" \
        "\x01\x00" "\x12\x34\x56\x78"\
        "\x01" "A" \
        "\x03" "BC\x00" \
        "\x01\x00" "DEF\x00" \
        "\x02" "a\x00" \
        "\x06" "b\x00" "c\x00" "\x00\x00" \
        "\x01\x00" "d\x00" "e\x00" \
        "\x01" "\x11" "\x02" "\x22\x22" \
        "\x01" "\x03" "\x33\x33\x33" "\x00" \
        "\x04" "\x44\x44\x44\x44" \
        "\x01" "\x05" "\x55\x55\x55\x55\x55",
        74,
        result.c_str(), result.size()
    );

    // changes in parent are reflected in children via instances
    update.set_flags(0xdeadbeef);
    EXPECT_EQUAL(0xdeadbeef, update.single_data().parent_flags());
    EXPECT_EQUAL(0xdeadbeef, update.single_data().multi_data().flags());
    update.set_flags(0xcafebabe);
    EXPECT_EQUAL(0xcafebabe, update.single_data().parent_flags());
    EXPECT_EQUAL(0xcafebabe, update.single_data().multi_data().flags());

    // clear
    update.Clear();
    EXPECT_TRUE(update.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(
        "\x20\x00\x00\x00" "\x00\x00\x00\x00" "\x00\x00\x00\x00" \
        "\x00\x00" "" \
        "\x00\x00" \
        "\x00" "" \
        "\x01" "\x00" \
        "\x00\x00" \
        "\x00" "" \
        "\x02" "\x00\x00" \
        "\x00\x00" \
        "\x00" "" \
        "\x00" \
        "\x00" "" \
        "\x00" "" \
        "\x00",
        32,
        result.c_str(), result.size()
    );

    // parse from string
    EXPECT_TRUE(update.ParseFromString(std::string(
        "\x4a\x00\x00\x00" "\xef\xbe\xad\xde" "\xa5\xa5\xa5\xa5" \
        "\x03\x00" "\xab\x00\xcd" \
        "\x01\x00" "\x12\x34\x56\x78"\
        "\x01" "A" \
        "\x03" "BC\x00" \
        "\x01\x00" "DEF\x00" \
        "\x02" "a\x00" \
        "\x06" "b\x00" "c\x00" "\x00\x00" \
        "\x01\x00" "d\x00" "e\x00" \
        "\x01" "\x11" "\x02" "\x22\x22" \
        "\x01" "\x03" "\x33\x33\x33" "\x00" \
        "\x04" "\x44\x44\x44\x44" \
        "\x01" "\x05" "\x55\x55\x55\x55\x55",
        74
    )));
    EXPECT_EQUAL(74, update.total_size1());
    EXPECT_EQUAL(0xdeadbeef, update.flags());
    EXPECT_EQUAL(0xa5a5a5a5, update.unused());  // this is correct, parse/read should NOT call update
    EXPECT_EQUAL(3, update.buffer_size1());
    EXPECT_BYTES_EQUAL("\xab\x00\xcd", 3, update.buffer().c_str(), update.buffer().size());
    EXPECT_EQUAL(1, update.buffer_list_count());
    EXPECT_EQUAL(1, update.buffer_list().size());
    EXPECT_BYTES_EQUAL("\x12\x34\x56\x78", 4, update.buffer_list(0).c_str(), update.buffer_list(0).size());
    EXPECT_EQUAL(1, update.ascii_size1());
    EXPECT_STRING_EQUAL("A", update.ascii().c_str());
    EXPECT_EQUAL(3, update.asciiz_size1());
    EXPECT_STRING_EQUAL("BC", update.asciiz().c_str());
    EXPECT_EQUAL(1, update.ascii_list_count());
    EXPECT_EQUAL(1, update.ascii_list().size());
    EXPECT_STRING_EQUAL("DEF", update.ascii_list(0).c_str());
    EXPECT_EQUAL(2, update.wide_size1());
    EXPECT_STRING_EQUAL(L"a", update.wide().c_str());
    EXPECT_EQUAL(6, update.widez_size1());
    EXPECT_STRING_EQUAL(L"bc", update.widez().c_str());
    EXPECT_EQUAL(1, update.wide_list_count());
    EXPECT_EQUAL(1, update.wide_list().size());
    EXPECT_STRING_EQUAL(L"de", update.wide_list(0).c_str());
    EXPECT_EQUAL(1, update.single_data().data_size1());
    EXPECT_BYTES_EQUAL("\x11", 1, update.single_data().data().c_str(), update.single_data().data().size());
    EXPECT_EQUAL(2, update.single_data().multi_data().data_size1());
    EXPECT_BYTES_EQUAL("\x22\x22", 2, update.single_data().multi_data().data().c_str(), update.single_data().multi_data().data().size());
    EXPECT_EQUAL(1, update.single_data_list_count());
    EXPECT_EQUAL(1, update.single_data_list().size());
    EXPECT_EQUAL(3, update.single_data_list(0).data_size1());
    EXPECT_BYTES_EQUAL("\x33\x33\x33", 3, update.single_data_list(0).data().c_str(), update.single_data_list(0).data().size());
    EXPECT_EQUAL(0, update.single_data_list(0).multi_data().data_size1());
    EXPECT_EQUAL(0, update.single_data_list(0).multi_data().data().size());
    EXPECT_EQUAL(4, update.multi_data().data_size1());
    EXPECT_BYTES_EQUAL("\x44\x44\x44\x44", 4, update.multi_data().data().c_str(), update.multi_data().data().size());
    EXPECT_EQUAL(1, update.multi_data_list_count());
    EXPECT_EQUAL(1, update.multi_data_list().size());
    EXPECT_EQUAL(5, update.multi_data_list(0).data_size1());
    EXPECT_BYTES_EQUAL("\x55\x55\x55\x55\x55", 5, update.multi_data_list(0).data().c_str(), update.multi_data_list(0).data().size());

    return 0;
}

} // test
} // kpb
