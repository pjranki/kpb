#include "test_self_root_parent.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/self_root_parent.hpp"

namespace kpb {
namespace test {

int self_root_parent(void)
{
    kpb_test::Root_ root_obj;
    kpb_test::Child *child_obj = nullptr;
    kpb_test::GrandChild *grand_child_obj = nullptr;

    child_obj = root_obj.mutable_child();
    EXPECT_NOT_NULL(child_obj);
    grand_child_obj = child_obj->mutable_child();
    EXPECT_NOT_NULL(grand_child_obj);

    EXPECT_EQUAL(&root_obj, &(child_obj->root()));
    EXPECT_EQUAL(&root_obj, &(grand_child_obj->root()));

    EXPECT_TRUE(root_obj.set_value(0xdeadbeef));
    EXPECT_TRUE(child_obj->set_value(0xcafebabe));
    EXPECT_TRUE(grand_child_obj->set_value(0xf00ba123));

    EXPECT_EQUAL(0xdeadbeef, root_obj.value());
    EXPECT_EQUAL(0xcafebabe, child_obj->value());
    EXPECT_EQUAL(0xf00ba123, grand_child_obj->value());

    EXPECT_EQUAL(0xdeadbeef, root_obj.root_value());
    EXPECT_EQUAL(0xdeadbeef, root_obj.self_value());

    EXPECT_EQUAL(0xdeadbeef, child_obj->root_value());
    EXPECT_EQUAL(0xdeadbeef, child_obj->parent_value());
    EXPECT_EQUAL(0xcafebabe, child_obj->self_value());

    EXPECT_EQUAL(0xdeadbeef, grand_child_obj->root_value());
    EXPECT_EQUAL(0xcafebabe, grand_child_obj->parent_value());
    EXPECT_EQUAL(0xf00ba123, grand_child_obj->self_value());

    root_obj.Clear();

    child_obj = root_obj.add_child_list();
    EXPECT_NOT_NULL(child_obj);
    grand_child_obj = child_obj->add_grand_child_list();
    EXPECT_NOT_NULL(grand_child_obj);

    EXPECT_EQUAL(&root_obj, &(child_obj->root()));
    EXPECT_EQUAL(&root_obj, &(grand_child_obj->root()));

    EXPECT_TRUE(root_obj.set_value(0xdeadbeef));
    EXPECT_TRUE(child_obj->set_value(0xcafebabe));
    EXPECT_TRUE(grand_child_obj->set_value(0xf00ba123));

    EXPECT_EQUAL(0xdeadbeef, root_obj.value());
    EXPECT_EQUAL(0xcafebabe, child_obj->value());
    EXPECT_EQUAL(0xf00ba123, grand_child_obj->value());

    EXPECT_EQUAL(0xdeadbeef, root_obj.root_value());
    EXPECT_EQUAL(0xdeadbeef, root_obj.self_value());

    EXPECT_EQUAL(0xdeadbeef, child_obj->root_value());
    EXPECT_EQUAL(0xdeadbeef, child_obj->parent_value());
    EXPECT_EQUAL(0xcafebabe, child_obj->self_value());

    EXPECT_EQUAL(0xdeadbeef, grand_child_obj->root_value());
    EXPECT_EQUAL(0xcafebabe, grand_child_obj->parent_value());
    EXPECT_EQUAL(0xf00ba123, grand_child_obj->self_value());

    child_obj = root_obj.mutable_child_list(0);
    EXPECT_NOT_NULL(child_obj);
    grand_child_obj = child_obj->mutable_grand_child_list(0);
    EXPECT_NOT_NULL(grand_child_obj);

    EXPECT_EQUAL(&root_obj, &(child_obj->root()));
    EXPECT_EQUAL(&root_obj, &(grand_child_obj->root()));

    EXPECT_TRUE(root_obj.set_value(0xdeadbeef));
    EXPECT_TRUE(child_obj->set_value(0xcafebabe));
    EXPECT_TRUE(grand_child_obj->set_value(0xf00ba123));

    EXPECT_EQUAL(0xdeadbeef, root_obj.value());
    EXPECT_EQUAL(0xcafebabe, child_obj->value());
    EXPECT_EQUAL(0xf00ba123, grand_child_obj->value());

    EXPECT_EQUAL(0xdeadbeef, root_obj.root_value());
    EXPECT_EQUAL(0xdeadbeef, root_obj.self_value());

    EXPECT_EQUAL(0xdeadbeef, child_obj->root_value());
    EXPECT_EQUAL(0xdeadbeef, child_obj->parent_value());
    EXPECT_EQUAL(0xcafebabe, child_obj->self_value());

    EXPECT_EQUAL(0xdeadbeef, grand_child_obj->root_value());
    EXPECT_EQUAL(0xcafebabe, grand_child_obj->parent_value());
    EXPECT_EQUAL(0xf00ba123, grand_child_obj->self_value());

    kpb_test::Child orphan;

    EXPECT_EQUAL(0, orphan.root_value());
    EXPECT_EQUAL(0, orphan.self_value());
    EXPECT_EQUAL(0, orphan.parent_value());

    orphan.Clear();

    EXPECT_EQUAL(0, orphan.root_value());
    EXPECT_EQUAL(0, orphan.self_value());
    EXPECT_EQUAL(0, orphan.parent_value());

    return 0;
}

} // test
} // kpb
