#include "test_object_align.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/object_align.hpp"

namespace kpb {
namespace test {

int object_align(void)
{
    kpb_test::ObjectAlign obj;
    kpb_test::ObjectB4 *sub_obj = nullptr;
    std::string result;

    EXPECT_TRUE(obj.set_value_1(0xa));
    sub_obj = obj.mutable_object_b4();
    EXPECT_NOT_NULL(sub_obj);
    EXPECT_TRUE(sub_obj->set_value( 0xc));
    EXPECT_TRUE(obj.set_value_2(0x5));
    EXPECT_TRUE(obj.add_value_list(0x1));
    EXPECT_TRUE(obj.add_value_list(0x2));
    EXPECT_TRUE(obj.add_value_list(0x3));
    EXPECT_TRUE(obj.add_value_list(0x4));

    EXPECT_TRUE(obj.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xa0\xc0\x51\x23\x40", 5, result.c_str(), result.size());

    obj.Clear();
    EXPECT_TRUE(obj.ParseFromString(std::string("\x50\x30\xa4\x32\x10", 5)));
    EXPECT_EQUAL(0x5, obj.value_1());
    EXPECT_EQUAL(0x3, obj.object_b4().value());
    EXPECT_EQUAL(0xa, obj.value_2());
    EXPECT_EQUAL(0x4, obj.value_list()[0]);
    EXPECT_EQUAL(0x3, obj.value_list()[1]);
    EXPECT_EQUAL(0x2, obj.value_list()[2]);
    EXPECT_EQUAL(0x1, obj.value_list()[3]);

    return 0;
}

} // test
} // kpb
