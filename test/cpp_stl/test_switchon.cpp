#include "test_switchon.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/switchon.hpp"

namespace kpb {
namespace test {

int switchon(void)
{
    kpb_test::Switchon switchon;
    kpb_test::Object1 *object1 = nullptr;
    kpb_test::Object2 *object2 = nullptr;
    std::string result;
    EXPECT_TRUE(switchon.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 11, result.c_str(), result.size());
    object1 = switchon.mutable_object();
    EXPECT_NOT_NULL(object1);
    EXPECT_TRUE(object1->set_value1(0xdeadbeef));
    EXPECT_TRUE(switchon.set_data_type(kpb_test::Switchon::OneofType::ONEOF_TYPE_OBJECT));
    EXPECT_TRUE(switchon.set_u2(0xcafe));
    EXPECT_TRUE(switchon.set_value_type(2));
    EXPECT_TRUE(switchon.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x01\xef\xbe\xad\xde\x02\x00\xfe\xca", 9, result.c_str(), result.size());
    object2 = switchon.mutable_number();
    EXPECT_NOT_NULL(object2);
    EXPECT_TRUE(object2->set_value2(0xa537));
    EXPECT_TRUE(switchon.set_data_type(kpb_test::Switchon::OneofType::ONEOF_TYPE_NUMBER));
    EXPECT_TRUE(switchon.set_u4(0x12345678));
    EXPECT_TRUE(switchon.set_value_type(4));
    EXPECT_TRUE(switchon.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x03\x37\xa5\x04\x00\x78\x56\x34\x12", 9, result.c_str(), result.size());
    EXPECT_TRUE(switchon.set_data_type(0xff));
    EXPECT_TRUE(switchon.set_u8(0xa1a2a3a4a5a6a7a8));
    EXPECT_TRUE(switchon.set_value_type(0xeecc));
    EXPECT_TRUE(switchon.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xff\xcc\xee\xa8\xa7\xa6\xa5\xa4\xa3\xa2\xa1", 11, result.c_str(), result.size());
    switchon.Clear();
    EXPECT_TRUE(switchon.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 11, result.c_str(), result.size());

    EXPECT_TRUE(switchon.ParseFromString(std::string("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 11)));
    EXPECT_EQUAL(0, switchon.data_type());
    EXPECT_FALSE(switchon.has_object());
    EXPECT_FALSE(switchon.has_number());
    EXPECT_EQUAL(0, switchon.value_type());
    EXPECT_FALSE(switchon.has_u2());
    EXPECT_FALSE(switchon.has_u4());
    EXPECT_TRUE(switchon.has_u8());
    EXPECT_EQUAL(0, switchon.u8());
    EXPECT_TRUE(switchon.ParseFromString(std::string("\x01\xef\xbe\xad\xde\x02\x00\xfe\xca", 9)));
    EXPECT_EQUAL(1, switchon.data_type());
    EXPECT_TRUE(switchon.has_object());
    EXPECT_EQUAL(0xdeadbeef, switchon.object().value1());
    EXPECT_FALSE(switchon.has_number());
    EXPECT_EQUAL(2, switchon.value_type());
    EXPECT_TRUE(switchon.has_u2());
    EXPECT_EQUAL(0xcafe, switchon.u2());
    EXPECT_FALSE(switchon.has_u4());
    EXPECT_FALSE(switchon.has_u8());
    EXPECT_TRUE(switchon.ParseFromString(std::string("\x03\x37\xa5\x04\x00\x78\x56\x34\x12", 9)));
    EXPECT_EQUAL(3, switchon.data_type());
    EXPECT_FALSE(switchon.has_object());
    EXPECT_TRUE(switchon.has_number());
    EXPECT_EQUAL(0xa537, switchon.number().value2());
    EXPECT_EQUAL(4, switchon.value_type());
    EXPECT_FALSE(switchon.has_u2());
    EXPECT_TRUE(switchon.has_u4());
    EXPECT_EQUAL(0x12345678, switchon.u4());
    EXPECT_FALSE(switchon.has_u8());
    EXPECT_TRUE(switchon.ParseFromString(std::string("\xff\xcc\xee\xa8\xa7\xa6\xa5\xa4\xa3\xa2\xa1", 11)));
    EXPECT_EQUAL(0xff, switchon.data_type());
    EXPECT_FALSE(switchon.has_object());
    EXPECT_FALSE(switchon.has_number());
    EXPECT_EQUAL(0xeecc, switchon.value_type());
    EXPECT_FALSE(switchon.has_u2());
    EXPECT_FALSE(switchon.has_u4());
    EXPECT_TRUE(switchon.has_u8());
    EXPECT_EQUAL(0xa1a2a3a4a5a6a7a8, switchon.u8());

    kpb_test::DupEnumNumber dup_number;
    dup_number.mutable_data_1();
    dup_number.mutable_data_2();

    kpb_test::DupEnumName dup_name;
    dup_name.mutable_data_false_();
    dup_name.mutable_data_true_();
    return 0;
}

} // test
} // kpb
