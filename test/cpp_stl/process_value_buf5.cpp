#include "process_value_buf5.hpp"

#include <string.h>

ProcessValueBuf5::ProcessValueBuf5(uint8_t memset_value)
{
    memset_value_ = memset_value;
}

std::string ProcessValueBuf5::getter(const std::string &value)
{
    std::string new_value(value.c_str(), value.size());
    memset(const_cast<char*>(new_value.c_str()), memset_value_, new_value.size());
    return new_value;
}

std::string ProcessValueBuf5::setter(std::string &value)
{
    std::string new_value(value.c_str(), value.size());
    memset(const_cast<char*>(new_value.c_str()), memset_value_, new_value.size());
    return new_value;
}
