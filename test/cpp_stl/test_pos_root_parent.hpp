#ifndef _H_KPB_TEST_POS_ROOT_PARENT_H_
#define _H_KPB_TEST_POS_ROOT_PARENT_H_

namespace kpb {
namespace test {

int pos_root_parent(void);

} // test
} // kpb

#endif
