#ifndef _H_KPB_TEST_CONTENTS_H_
#define _H_KPB_TEST_CONTENTS_H_

namespace kpb {
namespace test {

int contents(void);

} // test
} // kpb

#endif
