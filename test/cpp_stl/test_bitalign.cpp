#include "test_bitalign.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/bitalign.hpp"

namespace kpb {
namespace test {

int bitalign(void)
{
    kpb_test::BitalignDefault objd;
    kpb_test::Bitalign8 obj8;
    kpb_test::Bitalign1 obj1;
    std::string result;

    // serialize objd with default bit alignment
    objd.set_v1(1);
    objd.mutable_v2()->set_v(0x7);
    objd.mutable_v3()->set_v(0x7);
    objd.set_v4(0xfff);
    EXPECT_TRUE(objd.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(result.c_str(), result.size(), "\x80\xe0\xe0\xff\xf0", 5);

    // parse objd with default bit alignment
    objd.Clear();
    EXPECT_TRUE(objd.ParseFromString(std::string("\x80\xe0\xe0\xff\xf0", 5)));
    EXPECT_EQUAL(objd.v1(), 1);
    EXPECT_EQUAL(objd.v2().v(), 0x7);
    EXPECT_EQUAL(objd.v3().v(), 0x7);
    EXPECT_EQUAL(objd.v4(), 0xfff);

    // serialize obj8 with 8 bit alignment
    obj8.set_v1(1);
    obj8.mutable_v2()->set_v(0x7);
    obj8.mutable_v3()->set_v(0x7);
    obj8.set_v4(0xfff);
    EXPECT_TRUE(obj8.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(result.c_str(), result.size(), "\x80\xe0\xe0\xff\xf0", 5);

    // parse obj8 with 8 bit alignment
    obj8.Clear();
    EXPECT_TRUE(obj8.ParseFromString(std::string("\x80\xe0\xe0\xff\xf0", 5)));
    EXPECT_EQUAL(obj8.v1(), 1);
    EXPECT_EQUAL(obj8.v2().v(), 0x7);
    EXPECT_EQUAL(obj8.v3().v(), 0x7);
    EXPECT_EQUAL(obj8.v4(), 0xfff);

    // serialize obj1 with 1 bit alignment
    obj1.set_v1(1);
    obj1.mutable_v2()->set_v(0x7);
    obj1.mutable_v3()->set_v(0x7);
    obj1.set_v4(0xfff);
    EXPECT_TRUE(obj1.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(result.c_str(), result.size(), "\xff\xff\xe0", 3);

    // parse obj1 with 1 bit alignment
    obj1.Clear();
    EXPECT_TRUE(obj1.ParseFromString(std::string("\xff\xff\xe0", 3)));
    EXPECT_EQUAL(obj1.v1(), 1);
    EXPECT_EQUAL(obj1.v2().v(), 0x7);
    EXPECT_EQUAL(obj1.v3().v(), 0x7);
    EXPECT_EQUAL(obj1.v4(), 0xfff);

    return 0;
}

} // test
} // kpb
