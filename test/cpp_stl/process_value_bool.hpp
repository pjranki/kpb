#ifndef _H_PROCESS_VALUE_BOOL_CPP_H_
#define _H_PROCESS_VALUE_BOOL_CPP_H_

#include <cstdint>
#include <string>

#include "cpp_stl/process_value.hpp"

class ProcessValueBool
{
    public:
        ProcessValueBool(bool invert_logic);
        bool getter(bool value);
        bool setter(bool value);
    private:
        bool invert_logic_;
};

#endif
