#include "process_value_f4.hpp"


ProcessValueF4::ProcessValueF4(bool multiple_float_by_4)
{
    multiple_float_by_4_ = multiple_float_by_4;
}

float ProcessValueF4::getter(float value)
{
    return multiple_float_by_4_ ? (value * 4.0f) : value;
}

float ProcessValueF4::setter(float value)
{
    return multiple_float_by_4_ ? (value * 4.0f) : value;
}
