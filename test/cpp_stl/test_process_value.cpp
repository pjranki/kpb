#include "test_process_value.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/process_value.hpp"

namespace kpb {
namespace test {

int process_value(void)
{
    kpb_test::ProcessValue obj;

    // value1 tests
    EXPECT_EQUAL(10, obj.value1());
    EXPECT_TRUE(obj.set_value1(20));
    EXPECT_EQUAL(25, obj.value1());

    // value2 tests
    EXPECT_EQUAL(false, obj.value2());
    EXPECT_TRUE(obj.set_value2(true));
    EXPECT_EQUAL(true, obj.value2());
    EXPECT_TRUE(obj.set_value2(false));
    EXPECT_EQUAL(false, obj.value2());
    EXPECT_TRUE(obj.set_invert_logic(true));
    EXPECT_TRUE(obj.set_value2(true));
    EXPECT_TRUE(obj.set_invert_logic(false));
    EXPECT_EQUAL(false, obj.value2());
    EXPECT_TRUE(obj.set_invert_logic(true));
    EXPECT_TRUE(obj.set_value2(false));
    EXPECT_TRUE(obj.set_invert_logic(false));
    EXPECT_EQUAL(true, obj.value2());
    EXPECT_TRUE(obj.set_value2(true));
    EXPECT_TRUE(obj.set_invert_logic(true));
    EXPECT_EQUAL(false, obj.value2());
    EXPECT_TRUE(obj.set_invert_logic(false));
    EXPECT_TRUE(obj.set_value2(false));
    EXPECT_TRUE(obj.set_invert_logic(true));
    EXPECT_EQUAL(true, obj.value2());
    EXPECT_TRUE(obj.set_invert_logic(false));

    // value3 tests
    EXPECT_EQUAL(10, obj.value3());
    EXPECT_TRUE(obj.set_value3(20));
    EXPECT_EQUAL(25, obj.value3());

    // value4 tests
    EXPECT_EQUAL(0, obj.value4());
    EXPECT_TRUE(obj.set_memset_value(10));
    EXPECT_EQUAL(10, obj.value4());
    EXPECT_TRUE(obj.set_memset_value(4));
    EXPECT_TRUE(obj.set_value4(12));
    EXPECT_TRUE(obj.set_memset_value(1));
    EXPECT_EQUAL(9, obj.value4());
    EXPECT_TRUE(obj.set_memset_value(0));

    // value5 tests
    EXPECT_STD_STRING_EQUAL(std::string(""), obj.value5());
    EXPECT_TRUE(obj.set_value5("abc"));
    EXPECT_STD_STRING_EQUAL(std::string("\x00\x00\x00", 3), obj.value5());
    EXPECT_TRUE(obj.set_memset_value('z'));
    EXPECT_TRUE(obj.set_value5("abcd"));
    EXPECT_STD_STRING_EQUAL(std::string("zzzz"), obj.value5());
    EXPECT_TRUE(obj.set_memset_value('y'));
    EXPECT_STD_STRING_EQUAL(std::string("yyyy"), obj.value5());
    EXPECT_TRUE(obj.set_memset_value(0));

    // value6 tests
    EXPECT_STD_STRING_EQUAL(std::string("", 0), obj.value6());
    EXPECT_TRUE(obj.set_value6(std::string("abc", 3)));
    EXPECT_STD_STRING_EQUAL(std::string("\x00\x00\x00", 3), obj.value6());
    EXPECT_TRUE(obj.set_memset_value('z'));
    EXPECT_TRUE(obj.set_value6(std::string("abcd", 4)));
    EXPECT_STD_STRING_EQUAL(std::string("zzzz", 4), obj.value6());
    EXPECT_TRUE(obj.set_memset_value('y'));
    EXPECT_STD_STRING_EQUAL(std::string("yyyy", 4), obj.value6());
    EXPECT_TRUE(obj.set_memset_value(0));

    // value7 tests
    EXPECT_EQUAL(0.0f, obj.value7());
    EXPECT_TRUE(obj.set_value7(1.0f));
    EXPECT_EQUAL(1.0f, obj.value7());
    EXPECT_TRUE(obj.set_multiple_float_by_4(true));
    EXPECT_EQUAL(4.0f, obj.value7());
    EXPECT_TRUE(obj.set_value7(2.0f));
    EXPECT_TRUE(obj.set_multiple_float_by_4(false));
    EXPECT_EQUAL(8.0f, obj.value7());

    // value8 tests
    EXPECT_TRUE(obj.set_getter_count(0));
    EXPECT_TRUE(obj.set_setter_count(0));
    EXPECT_EQUAL(0xff, obj.value8().input_value());
    EXPECT_EQUAL(1, obj.getter_count());
    EXPECT_EQUAL(0, obj.setter_count());
    EXPECT_TRUE(obj.mutable_value8()->set_input_value(0xaa));
    EXPECT_EQUAL(0xaa, obj.value8().input_value());
    EXPECT_EQUAL(2, obj.getter_count());
    EXPECT_EQUAL(1, obj.setter_count());

    // value9 tests
    EXPECT_TRUE(obj.set_getter_count(0));
    EXPECT_TRUE(obj.set_setter_count(0));
    EXPECT_EQUAL(0x00, obj.value9().memset_value());
    EXPECT_EQUAL(1, obj.getter_count());
    EXPECT_EQUAL(0, obj.setter_count());
    EXPECT_EQUAL(0x00, obj.value9().memset_value());
    EXPECT_EQUAL(2, obj.getter_count());
    EXPECT_EQUAL(0, obj.setter_count());

    // value10 tests
    EXPECT_EQUAL(false, obj.value10());
    EXPECT_TRUE(obj.set_invert_logic(true));
    EXPECT_EQUAL(true, obj.value10());
    EXPECT_TRUE(obj.set_invert_logic(false));
    EXPECT_EQUAL(false, obj.value10());

    return 0;
}

} // test
} // kpb
