#include "test_io_expr.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/io_expr.hpp"

namespace kpb {
namespace test {

int io_expr(void)
{
    kpb_test::IoExpr io_expr;
    std::string result;

    // _io.pos
    EXPECT_TRUE(io_expr.ParseFromString(std::string("\xa1\xa2\xa3\xa4\xa5\xc1\xc2\xc3", 8)));
    EXPECT_BYTES_EQUAL("\xa1\xa2\xa3\xa4\xa5", 5, io_expr.data().c_str(), io_expr.data().size());
    EXPECT_BYTES_EQUAL("\xc1\xc2\xc3", 3, io_expr.align().c_str(), io_expr.align().size());
    EXPECT_TRUE(io_expr.ParseFromString(std::string("\xa1\xa2\xa3\xa4\xa5\xc1\xc2\xc3\xff\xff", 10)));
    EXPECT_BYTES_EQUAL("\xa1\xa2\xa3\xa4\xa5", 5, io_expr.data().c_str(), io_expr.data().size());
    EXPECT_BYTES_EQUAL("\xc1\xc2\xc3", 3, io_expr.align().c_str(), io_expr.align().size());
    io_expr.Clear();
    EXPECT_TRUE(io_expr.set_data(std::string("\xaa", 1)));
    EXPECT_TRUE(io_expr.set_align(std::string("\xe1\xe2", 2)));
    EXPECT_TRUE(io_expr.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xaa\x00\x00\x00\x00\xe1\xe2\x00", 8, result.c_str(), result.size());
    EXPECT_TRUE(io_expr.set_data(std::string("\xa1\xa2\xa3\xa4\xa5", 5)));
    EXPECT_TRUE(io_expr.set_align(std::string("\xc1\xc2\xc3", 3)));
    EXPECT_TRUE(io_expr.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xa1\xa2\xa3\xa4\xa5\xc1\xc2\xc3", 8, result.c_str(), result.size());
    EXPECT_TRUE(io_expr.set_data(std::string("\xa1\xa2\xa3\xa4\xa5\xff", 6)));
    EXPECT_TRUE(io_expr.set_align(std::string("\xc1\xc2\xc3\xff", 4)));
    EXPECT_TRUE(io_expr.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xa1\xa2\xa3\xa4\xa5\xc1\xc2\xc3", 8, result.c_str(), result.size());

    // _io.size and size-eos: true
    kpb_test::IoExprBytes obj_bytes;
    EXPECT_TRUE(obj_bytes.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00", 1, result.c_str(), result.size());
    EXPECT_TRUE(obj_bytes.set_fixed(0xcc));
    EXPECT_TRUE(obj_bytes.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xcc", 1, result.c_str(), result.size());
    EXPECT_TRUE(obj_bytes.set_variable(std::string("\x12\x34\x56\x78", 4)));
    EXPECT_TRUE(obj_bytes.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xcc\x12\x34\x56\x78", 5, result.c_str(), result.size());
    EXPECT_FALSE(obj_bytes.ParseFromString(std::string("", 0)));
    EXPECT_TRUE(obj_bytes.ParseFromString(std::string("\xdd", 1)));
    EXPECT_EQUAL(0xdd, obj_bytes.fixed());
    EXPECT_EQUAL(0, obj_bytes.variable().size());
    EXPECT_TRUE(obj_bytes.ParseFromString(std::string("\xee\xfe\xab", 3)));
    EXPECT_EQUAL(0xee, obj_bytes.fixed());
    EXPECT_BYTES_EQUAL("\xfe\xab", 2, obj_bytes.variable().c_str(), obj_bytes.variable().size());

    // _io.eof and repeat: eos
    kpb_test::IoExprEos obj_eos;
    EXPECT_TRUE(obj_eos.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00", 1, result.c_str(), result.size());
    EXPECT_TRUE(obj_eos.set_fixed(0xcc));
    EXPECT_TRUE(obj_eos.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xcc", 1, result.c_str(), result.size());
    EXPECT_TRUE(obj_eos.add_variable(0x3412));
    EXPECT_TRUE(obj_eos.add_variable(0x7856));
    EXPECT_TRUE(obj_eos.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xcc\x12\x34\x56\x78", 5, result.c_str(), result.size());
    EXPECT_FALSE(obj_eos.ParseFromString(std::string("", 0)));
    EXPECT_FALSE(obj_eos.ParseFromString(std::string("\xdd\xdd", 2)));
    EXPECT_TRUE(obj_eos.ParseFromString(std::string("\xdd", 1)));
    EXPECT_EQUAL(0xdd, obj_eos.fixed());
    EXPECT_EQUAL(0, obj_eos.variable().size());
    EXPECT_TRUE(obj_eos.ParseFromString(std::string("\xee\xab\xcd\x67\x89", 5)));
    EXPECT_EQUAL(0xee, obj_eos.fixed());
    EXPECT_EQUAL(2, obj_eos.variable().size());
    EXPECT_EQUAL(0xcdab, obj_eos.variable(0));
    EXPECT_EQUAL(0x8967, obj_eos.variable(1));

    // _ and repeat-until
    kpb_test::IoExprUntil obj_until;
    EXPECT_TRUE(obj_until.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x00", 1, result.c_str(), result.size());
    EXPECT_TRUE(obj_until.set_fixed(0xcc));
    EXPECT_TRUE(obj_until.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xcc", 1, result.c_str(), result.size());
    EXPECT_TRUE(obj_until.add_variable(0x3412));
    EXPECT_TRUE(obj_until.add_variable(0x7856));
    EXPECT_TRUE(obj_until.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xcc\x12\x34\x56\x78", 5, result.c_str(), result.size());
    EXPECT_FALSE(obj_until.ParseFromString(std::string("", 0)));
    EXPECT_FALSE(obj_until.ParseFromString(std::string("\xdd\xdd", 2)));
    EXPECT_FALSE(obj_until.ParseFromString(std::string("\xdd", 1)));
    EXPECT_TRUE(obj_until.ParseFromString(std::string("\xdd\xad\x0b\xab\xcd", 5)));
    EXPECT_EQUAL(0xdd, obj_until.fixed());
    EXPECT_EQUAL(1, obj_until.variable().size());
    EXPECT_EQUAL(0xbad, obj_until.variable(0));
    EXPECT_TRUE(obj_until.ParseFromString(std::string("\xee\xab\xcd\xad\x0b", 5)));
    EXPECT_EQUAL(0xee, obj_until.fixed());
    EXPECT_EQUAL(2, obj_until.variable().size());
    EXPECT_EQUAL(0xcdab, obj_until.variable(0));
    EXPECT_EQUAL(0xbad, obj_until.variable(1));

    // regression test for list of classes when using a class to check its value
    kpb_test::IoExprUntilClassList class_list;
    EXPECT_EQUAL(0, class_list.items().size());
    EXPECT_TRUE(class_list.ParseFromString(std::string("\x05", 1)));
    EXPECT_EQUAL(1, class_list.items().size());
    EXPECT_EQUAL(0x05, class_list.items(0).info().value());
    EXPECT_TRUE(class_list.ParseFromString(std::string("\x06\x07", 2)));
    EXPECT_EQUAL(2, class_list.items().size());
    EXPECT_EQUAL(0x06, class_list.items(0).info().value());
    EXPECT_EQUAL(0x07, class_list.items(1).info().value());
    EXPECT_TRUE(class_list.ParseFromString(std::string("\x07\x08\x03", 3)));
    EXPECT_EQUAL(3, class_list.items().size());
    EXPECT_EQUAL(0x07, class_list.items(0).info().value());
    EXPECT_EQUAL(0x08, class_list.items(1).info().value());
    EXPECT_EQUAL(0x03, class_list.items(2).info().value());
    EXPECT_TRUE(class_list.ParseFromString(std::string("\x09\x0a\x03\x0f", 4)));
    EXPECT_EQUAL(3, class_list.items().size());
    EXPECT_EQUAL(0x09, class_list.items(0).info().value());
    EXPECT_EQUAL(0x0a, class_list.items(1).info().value());
    EXPECT_EQUAL(0x03, class_list.items(2).info().value());
    class_list.Clear();
    EXPECT_TRUE(class_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("", 0, result.c_str(), result.size());
    EXPECT_TRUE(class_list.add_items()->mutable_info()->set_value(0x01));
    EXPECT_TRUE(class_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x01", 1, result.c_str(), result.size());
    EXPECT_TRUE(class_list.add_items()->mutable_info()->set_value(0x02));
    EXPECT_TRUE(class_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x01\x02", 2, result.c_str(), result.size());
    EXPECT_TRUE(class_list.add_items()->mutable_info()->set_value(0x03));
    EXPECT_TRUE(class_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x01\x02\x03", 3, result.c_str(), result.size());
    EXPECT_TRUE(class_list.add_items()->mutable_info()->set_value(0x04));
    EXPECT_TRUE(class_list.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\x01\x02\x03", 3, result.c_str(), result.size());

    return 0;
}

} // test
} // kpb
