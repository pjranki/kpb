#include "process_value_sub_type.hpp"


ProcessValueSubType::ProcessValueSubType(const kpb_test::ProcessValue &root) : root_(root)
{

}

const kpb_test::SubProcessValue &ProcessValueSubType::getter(const kpb_test::SubProcessValue &value)
{
    kpb_test::ProcessValue* root = const_cast<kpb_test::ProcessValue*>(&root_);
    root->set_getter_count(root->getter_count() + 1);
    return value;
}

kpb_test::SubProcessValue *ProcessValueSubType::setter(kpb_test::SubProcessValue *value)
{
    kpb_test::ProcessValue* root = const_cast<kpb_test::ProcessValue*>(&root_);
    root->set_setter_count(root->setter_count() + 1);
    return value;
}
