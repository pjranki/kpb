#include "test_pos_root_parent.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/pos_root_parent.hpp"

namespace kpb {
namespace test {

int pos_root_parent(void)
{
    std::string result;

    // check default state (after constructor, after a clear)
    kpb_test::PosRootParent root;
    kpb_test::PosChild *child = nullptr;
    kpb_test::PosGrandchild *grandchild = nullptr;
    EXPECT_EQUAL(28, root.Size());
    root.Clear();
    EXPECT_EQUAL(28, root.Size());

    // parse at correct positions in correct substreams
    EXPECT_TRUE(root.ParseFromString(std::string("\x11\x11\x11\x11\x22\x22\x22\x22\x33\x33\x33\x33\x44\x44\x44\x44\x55\x55\x55\x55\x66\x66\x66\x66\x77\x77\x77\x77", 28)));
    EXPECT_EQUAL(0x11111111, root.value());
    EXPECT_EQUAL(0x22222222, root.child().value_parent());
    EXPECT_EQUAL(0x33333333, root.child().value_root());
    EXPECT_EQUAL(0x44444444, root.child().grandchild().value_root());
    EXPECT_EQUAL(0x55555555, root.child().value());
    EXPECT_EQUAL(0x66666666, root.child().grandchild().value_parent());
    EXPECT_EQUAL(0x77777777, root.child().grandchild().value());

    // serialize at correct positions in correct substreams
    root.Clear();
    child = root.mutable_child();
    EXPECT_NOT_NULL(child);
    grandchild = child->mutable_grandchild();
    EXPECT_NOT_NULL(grandchild);
    EXPECT_TRUE(root.set_value(0x77777777));
    EXPECT_TRUE(child->set_value_parent(0x66666666));
    EXPECT_TRUE(child->set_value_root(0x55555555));
    EXPECT_TRUE(grandchild->set_value_root(0x41414141));
    EXPECT_TRUE(child->set_value(0x33333333));
    EXPECT_TRUE(grandchild->set_value_parent(0x22222222));
    EXPECT_TRUE(grandchild->set_value(0x11111111));
    EXPECT_TRUE(root.SerializeToString(&result));
    EXPECT_BYTES_EQUAL(
        "\x77\x77\x77\x77\x66\x66\x66\x66\x55\x55\x55\x55\x41\x41\x41\x41\x33\x33\x33\x33\x22\x22\x22\x22\x11\x11\x11\x11", 28,
        result.c_str(), result.size()
    );

    return 0;
}

} // test
} // kpb
