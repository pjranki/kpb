#ifndef _H_PROCESS_VALUE_U4_CPP_H_
#define _H_PROCESS_VALUE_U4_CPP_H_

#include <cstdint>
#include <string>

#include "cpp_stl/process_value.hpp"

class ProcessValueU4
{
    public:
        ProcessValueU4();
        uint32_t getter(uint32_t value);
        uint32_t setter(uint32_t value);
};

#endif
