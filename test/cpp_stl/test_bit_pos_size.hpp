#ifndef _H_KPB_TEST_BIT_POS_SIZE_H_
#define _H_KPB_TEST_BIT_POS_SIZE_H_

namespace kpb {
namespace test {

int bit_pos_size(void);

} // test
} // kpb

#endif
