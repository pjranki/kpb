#include "test_value_instances.hpp"

#include <stdio.h>

#include "../common/macro.h"

#include "cpp_stl/value_instances.hpp"

namespace kpb {
namespace test {

int value_instances(void)
{
    kpb_test::ValueInstances value_instances;
    std::string result;
    EXPECT_TRUE(value_instances.set_value1(0xdeadbeef));
    EXPECT_TRUE(value_instances.SerializeToString(&result));
    EXPECT_BYTES_EQUAL("\xef\xbe\xad\xde", 4, result.c_str(), result.size());
    EXPECT_EQUAL(0xdeadbeef, value_instances.value1());
    EXPECT_EQUAL(-1234, value_instances.instance1());
    EXPECT_EQUAL(5678, value_instances.instance2());
    EXPECT_TRUE(value_instances.ParseFromString(std::string("\xbe\xba\xfe\xca", 4)));
    EXPECT_EQUAL(0xcafebabe, value_instances.value1());
    EXPECT_EQUAL(-1234, value_instances.instance1());
    EXPECT_EQUAL(5678, value_instances.instance2());
    return 0;
}

} // test
} // kpb
