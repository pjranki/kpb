#include "kpb_io.h"

#include <string.h>

typedef uint8_t kpb_io_endian_t;
#define KPB_IO_ENDIAN_BE ((kpb_io_endian_t)0)
#define KPB_IO_ENDIAN_LE ((kpb_io_endian_t)1)

static kpb_io_endian_t byte_order(void)
{
    if (*((uint16_t*)"\x01\x23") != 0x0123)
    {
        return KPB_IO_ENDIAN_LE;
    }
    return KPB_IO_ENDIAN_BE;
}

static int8_t byte_swap_s1(int8_t value)
{
    return value;
}

static int16_t byte_swap_s2(int16_t value)
{
    int16_t swapped = 0;
    swapped |= (value >> 8) & 0x00ff;
    swapped |= (value << 8) & 0xff00;
    return swapped;
}

static int32_t byte_swap_s4(int32_t value)
{
    int32_t swapped = 0;
    swapped |= (value >> 24) & 0x000000ff;
    swapped |= (value >> 8)  & 0x0000ff00;
    swapped |= (value << 8)  & 0x00ff0000;
    swapped |= (value << 24) & 0xff000000;
    return swapped;
}

static int64_t byte_swap_s8(int64_t value)
{
    int64_t swapped = 0;
    swapped |= (value >> 56) & 0x00000000000000ff;
    swapped |= (value >> 40) & 0x000000000000ff00;
    swapped |= (value >> 24) & 0x0000000000ff0000;
    swapped |= (value >> 8)  & 0x00000000ff000000;
    swapped |= (value << 8)  & 0x000000ff00000000;
    swapped |= (value << 24) & 0x0000ff0000000000;
    swapped |= (value << 40) & 0x00ff000000000000;
    swapped |= (value << 56) & 0xff00000000000000;
    return swapped;
}

static uint8_t byte_swap_u1(uint8_t value)
{
    return value;
}

static uint16_t byte_swap_u2(uint16_t value)
{
    uint16_t swapped = 0;
    swapped |= (value >> 8) & 0x00ff;
    swapped |= (value << 8) & 0xff00;
    return swapped;
}

static uint32_t byte_swap_u4(uint32_t value)
{
    uint32_t swapped = 0;
    swapped |= (value >> 24) & 0x000000ff;
    swapped |= (value >> 8)  & 0x0000ff00;
    swapped |= (value << 8)  & 0x00ff0000;
    swapped |= (value << 24) & 0xff000000;
    return swapped;
}

static uint64_t byte_swap_u8(uint64_t value)
{
    uint64_t swapped = 0;
    swapped |= (value >> 56) & 0x00000000000000ff;
    swapped |= (value >> 40) & 0x000000000000ff00;
    swapped |= (value >> 24) & 0x0000000000ff0000;
    swapped |= (value >> 8)  & 0x00000000ff000000;
    swapped |= (value << 8)  & 0x000000ff00000000;
    swapped |= (value << 24) & 0x0000ff0000000000;
    swapped |= (value << 40) & 0x00ff000000000000;
    swapped |= (value << 56) & 0xff00000000000000;
    return swapped;
}

static float byte_swap_f4(float value)
{
    uint32_t out_value = byte_swap_u4(*((uint32_t*)&value));
    return *((float*)(&out_value));
}

static double byte_swap_f8(double value)
{
    uint64_t out_value = byte_swap_u8(*((uint64_t*)&value));
    return *((double*)(&out_value));
}

void *kpb_io_malloc(size_t size)
{
    return malloc(size);
}

void kpb_io_free(void *buffer)
{
    free(buffer);
}

kpb_io_utf16_t kpb_io_utf16_wchar(wchar_t chr, kpb_io_encoding_t enc)
{
    // TODO: this is not entirely correct on linux systems (where wchar_t is UTF-32)
    if ( (byte_order() == KPB_IO_ENDIAN_LE) && (enc == KPB_IO_ENCODING_UTF16LE))
    {
        return (kpb_io_utf16_t)chr;
    }
    if ( (byte_order() == KPB_IO_ENDIAN_BE) && (enc == KPB_IO_ENCODING_UTF16BE))
    {
        return (kpb_io_utf16_t)chr;
    }
    return (kpb_io_utf16_t)byte_swap_s2((int16_t)chr);
}

wchar_t kpb_io_wchar_utf16(kpb_io_utf16_t chr, kpb_io_encoding_t enc)
{
    // TODO: this is not entirely correct on linux systems (where wchar_t is UTF-32)
    if ( (byte_order() == KPB_IO_ENDIAN_LE) && (enc == KPB_IO_ENCODING_UTF16LE))
    {
        return ((wchar_t)chr) & 0xffff;
    }
    if ( (byte_order() == KPB_IO_ENDIAN_BE) && (enc == KPB_IO_ENCODING_UTF16BE))
    {
        return ((wchar_t)chr) & 0xffff;
    }
    return ((wchar_t)byte_swap_s2((int16_t)chr)) & 0xffff;
}

kpb_io_utf16_t *kpb_io_utf16_dupwchar(const wchar_t *str, kpb_io_encoding_t enc)
{
    // TODO: this is not entirely correct on linux systems (where wchar_t is UTF-32)
    size_t length = wcslen(str);
    size_t size = (length + 1) * sizeof(kpb_io_utf16_t);
    kpb_io_utf16_t *utf16_str = kpb_io_malloc(size);
    if ( !utf16_str )
    {
        return NULL;
    }
    for ( size_t i = 0; i < (length + 1); i++ )
    {
        utf16_str[i] = kpb_io_utf16_wchar(str[i], enc);
    }
    return utf16_str;
}

size_t kpb_io_utf16_len(const kpb_io_utf16_t *str)
{
    size_t len = 0;
    while ( *str != 0 )
    {
        str++;
        len++;
    }
    return len;
}

const kpb_io_utf16_t *kpb_io_utf16_chr(const kpb_io_utf16_t *str, kpb_io_utf16_t chr)
{
    while ( *str != 0 )
    {
        if ( *str == chr )
        {
            return str;
        }
        str++;
    }
    return NULL;
}

void kpb_buffer_init(kpb_buffer_t *_buffer)
{
    if ( !_buffer )
    {
        return;
    }
    _buffer->buffer = "";
    _buffer->buffer_size = 0;
    _buffer->buffer_allocated_size = 0;
    _buffer->writeable = true;
}

void kpb_buffer_fini(kpb_buffer_t *_buffer)
{
    if ( !_buffer )
    {
        return;
    }
    if ( (_buffer->buffer_allocated_size > 0) && _buffer->buffer )
    {
        kpb_io_free(_buffer->buffer);
    }
    memset(_buffer, 0, sizeof(kpb_buffer_t));
}

bool kpb_buffer_writable(const kpb_buffer_t *_buffer)
{
    if ( !_buffer )
    {
        return false;
    }
    return _buffer->writeable;
}

bool kpb_buffer_readonly(const kpb_buffer_t *_buffer)
{
    if ( !_buffer )
    {
        return false;
    }
    return !(_buffer->writeable);
}

bool kpb_buffer_set(kpb_buffer_t *_buffer, const char *data, size_t size)
{
    if ( !_buffer )
    {
        return false;
    }
    if ( (size > 0) && !data )
    {
        return false;
    }
    if ( _buffer->buffer_allocated_size < size )
    {
        // alocate a larger buffer
        char *new_buffer = kpb_io_malloc(size);
        if ( !new_buffer )
        {
            return false;
        }

        // free previously allocated buffer
        if ( (_buffer->buffer_allocated_size > 0) && _buffer->buffer )
        {
            kpb_io_free(_buffer->buffer);
        }

        // assign new buffer
        _buffer->buffer = new_buffer;
        _buffer->buffer_size = size;
        _buffer->buffer_allocated_size = size;
    }
    memcpy(_buffer->buffer, data, size);
    _buffer->buffer_size = size;
    _buffer->writeable = true;
    return true;
}

bool kpb_buffer_set_readonly(kpb_buffer_t *_buffer, const char *data, size_t size)
{
    if ( !_buffer )
    {
        return false;
    }
    if ( (size > 0) && !data )
    {
        return false;
    }
    if ( !data )
    {
        data = "";
    }
    if ( (_buffer->buffer_allocated_size > 0) && _buffer->buffer )
    {
        kpb_io_free(_buffer->buffer);
    }
    _buffer->buffer = (char*)data;
    _buffer->buffer_size = size;
    _buffer->buffer_allocated_size = 0;
    _buffer->writeable = false;
    return true;
}

size_t kpb_buffer_size(const kpb_buffer_t *_buffer)
{
    if ( !_buffer )
    {
        return 0;
    }
    return _buffer->buffer_size;
}

char *kpb_buffer_get(const kpb_buffer_t *_buffer)
{
    if ( !_buffer )
    {
        return "";
    }
    return _buffer->buffer;
}

bool kpb_buffer_resize(kpb_buffer_t *_buffer, size_t size)
{
    if ( !_buffer )
    {
        return false;
    }
    if ( !_buffer->writeable )
    {
        return false;
    }
    if ( size > _buffer->buffer_size )
    {
        if ( size > _buffer->buffer_allocated_size )
        {
            // alocate a larger buffer
            char *new_buffer = kpb_io_malloc(size);
            if ( !new_buffer )
            {
                return false;
            }

            // copy buffer contents
            memcpy(new_buffer, _buffer->buffer, _buffer->buffer_size);

            // free previously allocated buffer
            if ( (_buffer->buffer_allocated_size > 0) && _buffer->buffer )
            {
                kpb_io_free(_buffer->buffer);
            }

            // assign new buffer
            _buffer->buffer = new_buffer;
            _buffer->buffer_allocated_size = size;
        }
        memset(_buffer->buffer + _buffer->buffer_size, 0, size - _buffer->buffer_size);
    }
    _buffer->buffer_size = size;
    return true;
}

bool kpb_buffer_read(const kpb_buffer_t *_buffer, size_t pos, void *data, size_t size)
{
    if ( !_buffer )
    {
        return false;
    }
    if ( pos + size < pos )
    {
        return false;
    }
    if ( (size > 0) && !data )
    {
        return false;
    }
    if ( (pos + size) > _buffer->buffer_size )
    {
        return false;
    }
    memcpy(data, _buffer->buffer + pos, size);
    return true;
}

bool kpb_buffer_write(kpb_buffer_t *_buffer, size_t pos, const char *data, size_t size)
{
    if ( !_buffer )
    {
        return false;
    }
    if ( pos + size < pos )
    {
        return false;
    }
    if ( (size > 0) && !data )
    {
        return false;
    }
    if ( !_buffer->writeable )
    {
        return false;
    }
    if ( (pos + size) > _buffer->buffer_size )
    {
        if ( !kpb_buffer_resize(_buffer, pos + size) )
        {
            return false;
        }
    }
    memcpy(_buffer->buffer + pos, data, size);
    return true;
}

bool kpb_buffer_zero(kpb_buffer_t *_buffer, size_t pos, size_t size)
{
    if ( !_buffer )
    {
        return false;
    }
    if ( pos + size < pos )
    {
        return false;
    }
    if ( !_buffer->writeable )
    {
        return false;
    }
    if ( (pos + size) > _buffer->buffer_size )
    {
        if ( !kpb_buffer_resize(_buffer, pos + size) )
        {
            return false;
        }
    }
    memset(_buffer->buffer + pos, 0, size);
    return true;
}

void kpb_io_init(kpb_io_t *_io)
{
    memset(_io, 0, sizeof(kpb_io_t));

    // owner
    _io->stream_owner = _io;
    _io->write_mode = true;
    _io->ok = true;

    // root
    _io->stream_root = NULL;

    // current stream
    _io->stream_current = _io;
    _io->stream_next = NULL;
    _io->stream_prev = NULL;
    _io->consume_on_pop = false;

    // current stream data
    _io->stream_data = _io;
    _io->buffer = "";
    _io->buffer_size = 0;
    _io->buffer_allocated = false;

    // current stream view
    _io->stream_view = _io;
    _io->view_size_fixed = false;
    _io->view_offset = 0;
    _io->view_size = 0;
    _io->pos = 0;
    _io->bits_left = 0;
    _io->bits = 0;

    // fill with 0s or 1s
    _io->fill = false;
    _io->fill_byte = 0x00;
    _io->fill_eof = false;

    // keep track of last read_bytes call (so size OR buffer can be queried after read)
    _io->last_read_bytes_size = 0;
    _io->last_call_read_bytes = false;
    _io->last_call_read_bytes_size = false;
}

void kpb_io_fini(kpb_io_t *_io)
{
    if ( !_io ) return;
    kpb_io_fini( _io->stream_next );
    if ( _io->buffer_allocated )
    {
        kpb_io_free(_io->buffer);
    }
    if ( _io->tmp_buffer )
    {
        kpb_io_free(_io->tmp_buffer);
    }
    memset(_io, 0, sizeof(kpb_io_t));
}

static bool kpb_io_buffer_resize(kpb_io_t *_io, size_t buffer_size)
{
    if ( !kpb_io_ok(_io) ) return false;

    // nothing to do if the buffer is the same size
    if ( buffer_size == _io->stream_data->buffer_size )
    {
        return true;
    }

    // allocate new buffer
    size_t alloc_size = buffer_size > 0 ? buffer_size : 1;
    char *new_buffer = (char*)kpb_io_malloc(alloc_size);
    if ( !new_buffer )
    {
        kpb_io_set_error(_io);
        return false;
    }

    // keep existing data (cap if its too big)
    size_t keep_size = _io->stream_data->buffer_size > buffer_size ? buffer_size : _io->stream_data->buffer_size;
    if ( _io->stream_data->buffer && keep_size )
    {
        memcpy(new_buffer, _io->stream_data->buffer, keep_size);
    }

    // zero additional bytes
    if ( keep_size < buffer_size )
    {
        memset(new_buffer + keep_size, 0, buffer_size - keep_size);
    }

    // free old buffer
    if ( _io->stream_data->buffer_allocated )
    {
        kpb_io_free(_io->stream_data->buffer);
    }

    // assign new buffer
    _io->stream_data->buffer = new_buffer;
    _io->stream_data->buffer_size = buffer_size;
    _io->stream_data->buffer_allocated = true;

    // cap the position so it does not point past end of buffer
    _io->stream_view->pos = _io->stream_view->pos > buffer_size ? buffer_size : _io->stream_view->pos;
    return true;
}

void kpb_io_set_error(kpb_io_t *_io)
{
    _io->stream_owner->ok = false;
}

bool kpb_io_ok(kpb_io_t *_io)
{
    return _io->stream_owner->ok;
}

bool kpb_io_writable(const kpb_io_t *_io)
{
    return _io->stream_owner->write_mode;
}

bool kpb_io_readonly(const kpb_io_t *_io)
{
    return !(_io->stream_owner->write_mode);
}

void kpb_io_set_readonly(kpb_io_t *_io, const char *data, size_t size)
{
    if ( !kpb_io_ok(_io) ) return;

    // cannot call push from any other stream accept the owner
    if ( _io->stream_owner != _io )
    {
        kpb_io_set_error(_io);
        return;
    }

    // change data and view settings to the current stream (rather than referencing other streams)
    _io->stream_data = _io->stream_current;
    _io->stream_view = _io->stream_current;

    // disable writes to steam (and sub-streams)
    _io->stream_owner->write_mode = false;

    // assign & position the fixed data to the stream
    if ( _io->stream_data->buffer_allocated )
    {
        kpb_io_free(_io->stream_data->buffer);
    }
    _io->stream_data->buffer = (char*)data;
    _io->stream_data->buffer_size = size;
    _io->stream_data->buffer_allocated = false;
    _io->stream_view->view_offset = 0;
    _io->stream_view->view_size_fixed = true;
    _io->stream_view->view_size = size;
    _io->stream_view->pos = 0;
    _io->stream_view->bits_left = 0;
    _io->stream_view->bits = 0;
}

void kpb_io_set_readonly_or_fill(kpb_io_t *_io, const char *data, size_t size, uint8_t fill)
{
    kpb_io_set_readonly(_io, data, size);
    _io->fill = true;
    _io->fill_byte = fill != 0 ? 0xff : 0x00;
    _io->fill_eof = false;
}

bool kpb_io_set(kpb_io_t *_io, const char *data, size_t size)
{
    if ( !kpb_io_ok(_io) ) return false;

    // cannot call set from any other stream accept the owner
    if ( _io->stream_owner != _io )
    {
        kpb_io_set_error(_io);
        return false;
    }

    // keep the fixed/unfixed stream setting
    bool view_size_fixed = _io->stream_view->view_size_fixed;

    // change data and view settings to the current stream (rather than referencing other streams)
    _io->stream_current->stream_data = _io->stream_current; // input data is stored in current stream object
    _io->stream_current->stream_view = _io->stream_current; // changes to view are made to current stream object
    _io->stream_data = _io->stream_current->stream_data;
    _io->stream_view = _io->stream_current->stream_view;

    // write no-longer changes the parent stream, so we need to write the buffer on pop
    kpb_io_t *parent = _io->stream_current->stream_prev;
    if ( parent && kpb_io_writable(_io) )
    {
        _io->stream_current->consume_on_pop = true;
    }

    // assign & position the fixed data to the stream
    if ( !kpb_io_buffer_resize(_io, size) ) return false;
    memcpy(_io->stream_data->buffer, data, size);
    _io->stream_view->view_offset = 0;
    _io->stream_view->view_size_fixed = view_size_fixed;
    _io->stream_view->view_size = view_size_fixed ? size : 0;
    _io->stream_view->pos = 0;
    _io->stream_view->bits_left = 0;
    _io->stream_view->bits = 0;

    // and position the buffer accordingly
    if ( kpb_io_writable(_io) )
    {
        // buffer has been written, and position is at the end
        _io->stream_view->pos = size;
    }
    else
    {
        // reading of this new buffer is at the start
        _io->stream_view->pos = 0;
    }
    return true;
}

char *kpb_io_get(kpb_io_t *_io, size_t *size)
{
    if ( !kpb_io_ok(_io) ) return NULL;
    kpb_io_align_to_byte(_io);  // make sure remaining bitfields are added to the buffer
    if ( !kpb_io_ok(_io) ) return NULL;
    size_t output_size = kpb_io_size(_io);
    if ( 0 == output_size )
    {
        *size = 0;
        return kpb_io_malloc(1);
    }
    if ( !_io->stream_data->buffer ) return NULL;
    char *output = kpb_io_malloc(output_size);
    if ( !output ) return NULL;
    memcpy(output, _io->stream_data->buffer + _io->stream_view->view_offset, output_size);
    *size = output_size;
    return output;
}

size_t kpb_io_get_size(kpb_io_t *_io)
{
    kpb_io_align_to_byte(_io);  // make sure remaining bitfields are added to the buffer
    return kpb_io_size(_io);
}

void *kpb_io_tmp_buffer(kpb_io_t *_io, size_t size)
{
    if ( _io->tmp_buffer && ( _io->tmp_buffer_size >= size ) )
    {
        return _io->tmp_buffer;
    }
    if ( !size )
    {
        size = 1;
    }
    void *new_tmp_buffer = kpb_io_malloc(size);
    if ( !new_tmp_buffer )
    {
        kpb_io_set_error(_io);
        return NULL;
    }
    memset(new_tmp_buffer, 0, size);
    kpb_io_free(_io->tmp_buffer);
    _io->tmp_buffer = new_tmp_buffer;
    _io->tmp_buffer_size = size;
    return _io->tmp_buffer;
}

kpb_io_t *kpb_io_root(kpb_io_t *_io)
{
    if ( _io->stream_owner->stream_root )
    {
        return _io->stream_owner->stream_root;
    }

    // create a new stream to represent the root (as the owner's data/view context can change)
    kpb_io_t *root = kpb_io_malloc(sizeof(kpb_io_t));
    if ( !root )
    {
        // don't want to return a NULL pointer for this API
        // so we will set the error code & return the owner
        kpb_io_set_error(_io);
        return _io->stream_owner;
    }
    kpb_io_init(root);
    root->stream_owner = _io->stream_owner;
    if ( _io->stream_owner->stream_next )
    {
        root->stream_prev = _io->stream_owner->stream_next->stream_prev;
    }
    root->stream_next = _io->stream_owner->stream_next;
    if ( root->stream_next )
    {
        root->stream_next->stream_prev = root;
    }
    _io->stream_owner->stream_next = root;

    // root stream is just an alias to the original stream
    root->stream_data = _io->stream_owner;
    root->stream_view = _io->stream_owner;

    // use this new stream as the root in future
    _io->stream_owner->stream_root = root;

    // move all sub-streams to use root
    kpb_io_t *stream = _io->stream_owner;
    while ( stream )
    {
        // don't adjust the root stream (just all the other streams)
        if ( stream != root )
        {
            if ( stream->stream_current == _io->stream_owner )
            {
                stream->stream_current = root;
            }
        }
        stream = stream->stream_next;
    }
    return _io->stream_owner->stream_root;
}

kpb_io_t *kpb_io_parent(kpb_io_t *_io)
{
    kpb_io_t *parent = NULL;
    if ( _io == _io->stream_owner )
    {
        parent = _io->stream_owner->stream_current->stream_prev;
    }
    else
    {
        parent = _io->stream_prev;
    }
    if ( !parent )
    {
        parent = kpb_io_root(_io);
    }
    return parent;
}

void kpb_io_push_substream(kpb_io_t *_io, kpb_io_t *substream, bool set_pos, size_t pos, bool set_size, size_t size)
{
    if ( !kpb_io_ok(_io) ) return;

    // must be a valid substream
    if ( !substream )
    {
        kpb_io_set_error(_io);
        return;
    }

    // current active stream will become the parent
    kpb_io_t *parent = _io->stream_current;
    if ( parent == _io->stream_owner )
    {
        // if parent point to the owner, treat it as the root
        parent = kpb_io_root(_io);
    }

    // if substream is the owner, we are being asked to use the current (active) substream
    kpb_io_t *target = substream;
    if ( target == _io->stream_owner )
    {
        if ( _io->stream_current == _io->stream_owner )
        {
            target = kpb_io_root(_io);
        }
        else
        {
            target = _io->stream_current;
        }
    }

    // flush bits & make sure parent stream is byte-aligned
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;

    // creating & link stream into the list of streams
    kpb_io_t *new_stream = kpb_io_malloc(sizeof(kpb_io_t));
    if ( !new_stream )
    {
        kpb_io_set_error(_io);
        return;
    }
    kpb_io_init(new_stream);
    new_stream->stream_owner = _io->stream_owner;
    new_stream->stream_next = NULL;
    new_stream->stream_prev = parent;
    parent->stream_next = new_stream;

    // overflow check
    if ( ( pos + size ) < size )
    {
        kpb_io_set_error(_io);
        return;
    }

    // additional checks when working with a fixed read-only buffer (i.e. parsing)
    if ( kpb_io_readonly(_io) )
    {
        size_t start = set_pos ? pos : kpb_io_pos(target);
        size_t end = start + (set_size ? size : 0);

        // in read-only, cannot set a position & size outside the stream
        if ( end > kpb_io_size(target) )
        {
            if ( !_io->fill )
            {
                kpb_io_set_error(_io);
                return;
            }
            else
            {
                end = kpb_io_size(target);
            }
        }
    }

    // this is the offset of the view we want (it must begin at the start of the target stream)
    size_t view_offset = target->stream_view->view_offset;
    if ( set_pos )
    {
        // place the view at this offset into the target's stream
        view_offset += pos;
    }
    else
    {
        // we are reading/writing after the current position of the target stream
        // place the view at the current position of the target stream
        view_offset += target->stream_view->pos;
    }

    // this is the size of the view we want
    bool view_size_fixed = false;  // keep writing until the end of the stream
    size_t view_size = 0;
    if ( set_size )
    {
        view_size_fixed = true;
        view_size = size;
    }

    // further restrict the view if the target's stream is also restricted
    if ( target->stream_view->view_size_fixed )
    {
        // maximum allowed offset of a new view inside the target
        size_t view_offset_max = target->stream_view->view_offset + target->stream_view->view_size;
        if ( view_offset > view_offset_max )
        {
            view_offset = view_offset_max;
        }

        // maximum allowed size of a new view inside the target stream
        size_t view_size_max = view_offset_max - view_offset;
        if ( !view_size_fixed )
        {
            // no restriction was asked for, make all target data available
            view_size_fixed = true;
            view_size = view_size_max;
        }
        if ( view_size > view_size_max )
        {
            view_size = view_size_max;
        }
    }

    // determine the required size of the backing buffer (and fill with zeros if needed)
    size_t required_buffer_size = view_offset + view_size;
    if ( target->stream_data->buffer_size < required_buffer_size )
    {
        if ( kpb_io_writable(_io) )
        {
            // resize if writing
            if ( !kpb_io_buffer_resize(target, required_buffer_size) ) return;
        }
        else
        {
            // error if reading (cannot invent bytes out of nowhere)
            kpb_io_set_error(_io);
            return;
        }
    }

    // data will be read/written directly to/from target stream
    new_stream->stream_data = target->stream_data;

    // view of the new stream's data which will be restricted
    new_stream->stream_view = new_stream;
    new_stream->stream_view->view_offset = view_offset;
    new_stream->stream_view->view_size_fixed = view_size_fixed;
    new_stream->stream_view->view_size = view_size;

    // only consume data if we are reading/writing sequentially (not at a new position)
    new_stream->consume_on_pop = !set_pos;

    // make this stream active
    _io->stream_current = new_stream;
    _io->stream_data = _io->stream_current->stream_data;
    _io->stream_view = _io->stream_current->stream_view;
}

void kpb_io_pop_substream(kpb_io_t *_io)
{
    if ( !kpb_io_ok(_io) ) return;

    // cannot call pop from any other stream accept the owner
    if ( _io->stream_owner != _io )
    {
        kpb_io_set_error(_io);
        return;
    }

    // must have a parent to pop
    if ( !_io->stream_current->stream_prev )
    {
        kpb_io_set_error(_io);
        return;
    }

    // flush bits & and byte align before writing parent stream or re-positioning it
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;

    // parent stream
    kpb_io_t *parent = _io->stream_current->stream_prev;

    // consume data??
    if ( _io->stream_current->consume_on_pop )
    {
        if ( kpb_io_writable(_io) && ( parent->stream_data != _io->stream_data ) )
        {
            // write changes into parent
            if ( _io->stream_view->view_size_fixed )
            {
                // size: <value>
                kpb_io_write_bytes(parent, _io->stream_data->buffer, _io->stream_data->buffer_size, _io->stream_view->view_size);
            }
            else
            {
                // size-eos: true
                kpb_io_write_bytes(parent, _io->stream_data->buffer, _io->stream_data->buffer_size, _io->stream_data->buffer_size);
            }
        }
        else
        {
            // move position forward in the parent stream
            if ( _io->stream_view->view_size_fixed )
            {
                // size: <value>
                parent->stream_view->pos += _io->stream_view->view_size;
            }
            else
            {
                // size-eos: true
                parent->stream_view->pos += _io->stream_view->pos;
            }
        }

        // check reads/writes ok
        if ( !kpb_io_ok(_io) ) return;
    }

    // move back to previously active stream
    kpb_io_t *unused_stream = _io->stream_current;
    _io->stream_current = _io->stream_current->stream_prev;
    _io->stream_data = _io->stream_current->stream_data;
    _io->stream_view = _io->stream_current->stream_view;
    _io->stream_current->stream_next = NULL;

    // delete the unused stream
    kpb_io_fini(unused_stream);
    kpb_io_free(unused_stream);
}

size_t kpb_io_size(const kpb_io_t *_io)
{
    // if the offset into the buffer for the view is beyound the buffer, the view has no space
    if ( _io->stream_view->view_offset > _io->stream_data->buffer_size )
    {
        return 0;
    }

    // size can be determined by the size of the buffer (starting from where the view starts)
    size_t size = _io->stream_data->buffer_size - _io->stream_view->view_offset;

    // if the size of the view of the buffer is fixed - we know the size
    if ( _io->stream_view->view_size_fixed )
    {
        // for safety, only use the view size if the stream has enough data in it
        if ( size > _io->stream_view->view_size )
        {
            size = _io->stream_view->view_size;
        }
    }

    // done
    return size;
}

size_t kpb_io_pos(const kpb_io_t *_io)
{
    // if position is outside, cap at very end of stream
    if ( _io->stream_view->pos > kpb_io_size(_io) )
    {
        return kpb_io_size(_io);
    }
    return _io->stream_view->pos;
}

size_t kpb_io_bit_size(const kpb_io_t *_io)
{
    if ( kpb_io_writable(_io) )
    {
        return (kpb_io_size(_io) * 8) + _io->stream_view->bits_left;
    }
    else
    {
        // FUTURE: update this when we support bit streams of bit-size
        return kpb_io_size(_io) * 8;
    }
}

size_t kpb_io_bit_pos(const kpb_io_t *_io)
{
    size_t bits_left = _io->stream_view->bits_left;
    if ( kpb_io_writable(_io) )
    {
        return (kpb_io_pos(_io) * 8) + bits_left;
    }
    if (bits_left == 0)
    {
        return (kpb_io_pos(_io) * 8);
    }
    if ( bits_left > 8 )
    {
        // error state, just return byte position as bit position
        return kpb_io_pos(_io) * 8;
    }
    // in read-only, bits_left is how many bits we can use (not where we are)
    bits_left = 8 - bits_left;
    // -1 because we moved forward 8-bits when we filled the bit register
    return ((kpb_io_pos(_io) - 1) * 8) + bits_left;
}

bool kpb_io_eof(const kpb_io_t *_io)
{
    return _io->stream_view->fill_eof || ((kpb_io_pos(_io) >= kpb_io_size(_io)) && (_io->stream_view->bits_left == 0));
}

size_t kpb_io_remaining(kpb_io_t *_io)
{
    if ( kpb_io_size(_io) > kpb_io_pos(_io) )
    {
        return kpb_io_size(_io) - kpb_io_pos(_io);
    }
    return 0;
}

void kpb_io_align_to_byte(kpb_io_t *_io)
{
    if ( !kpb_io_ok(_io) ) return;
    if ( 0 == _io->stream_view->bits_left )
    {
        _io->stream_view->bits_left = 0;
        _io->stream_view->bits = 0;
        return;
    }
    if ( kpb_io_writable(_io) )
    {
        if ( _io->stream_view->bits_left >= 8 )
        {
            kpb_io_set_error(_io);
            return;
        }
        uint8_t left = (uint8_t)_io->stream_view->bits;
        left <<= (8 - _io->stream_view->bits_left);
        kpb_io_write(_io, &left, 1);
    }
    _io->stream_view->bits_left = 0;
    _io->stream_view->bits = 0;
}

int8_t kpb_io_read_s1(kpb_io_t *_io)
{
    if ( !kpb_io_ok(_io) ) return 0;
    kpb_io_align_to_byte(_io);
    int8_t value = 0;
    if ( !kpb_io_read(_io, &value, sizeof(value)) ) return 0;
    return value;
}

int16_t kpb_io_read_s2be(kpb_io_t *_io)
{
    if ( !kpb_io_ok(_io) ) return 0;
    kpb_io_align_to_byte(_io);
    int16_t value = 0;
    if ( !kpb_io_read(_io, &value, sizeof(value)) ) return 0;
    if ( byte_order() != KPB_IO_ENDIAN_BE ) return byte_swap_s2(value);
    return value;
}

int32_t kpb_io_read_s4be(kpb_io_t *_io)
{
    if ( !kpb_io_ok(_io) ) return 0;
    kpb_io_align_to_byte(_io);
    int32_t value = 0;
    if ( !kpb_io_read(_io, &value, sizeof(value)) ) return 0;
    if ( byte_order() != KPB_IO_ENDIAN_BE ) return byte_swap_s4(value);
    return value;
}

int64_t kpb_io_read_s8be(kpb_io_t *_io)
{
    if ( !kpb_io_ok(_io) ) return 0;
    kpb_io_align_to_byte(_io);
    int64_t value = 0;
    if ( !kpb_io_read(_io, &value, sizeof(value)) ) return 0;
    if ( byte_order() != KPB_IO_ENDIAN_BE ) return byte_swap_s8(value);
    return value;
}

int16_t kpb_io_read_s2le(kpb_io_t *_io)
{
    if ( !kpb_io_ok(_io) ) return 0;
    kpb_io_align_to_byte(_io);
    int16_t value = 0;
    if ( !kpb_io_read(_io, &value, sizeof(value)) ) return 0;
    if ( byte_order() != KPB_IO_ENDIAN_LE ) return byte_swap_s2(value);
    return value;
}

int32_t kpb_io_read_s4le(kpb_io_t *_io)
{
    if ( !kpb_io_ok(_io) ) return 0;
    kpb_io_align_to_byte(_io);
    int32_t value = 0;
    if ( !kpb_io_read(_io, &value, sizeof(value)) ) return 0;
    if ( byte_order() != KPB_IO_ENDIAN_LE ) return byte_swap_s4(value);
    return value;
}

int64_t kpb_io_read_s8le(kpb_io_t *_io)
{
    if ( !kpb_io_ok(_io) ) return 0;
    kpb_io_align_to_byte(_io);
    int64_t value = 0;
    if ( !kpb_io_read(_io, &value, sizeof(value)) ) return 0;
    if ( byte_order() != KPB_IO_ENDIAN_LE ) return byte_swap_s8(value);
    return value;
}

uint8_t kpb_io_read_u1(kpb_io_t *_io)
{
    if ( !kpb_io_ok(_io) ) return 0;
    kpb_io_align_to_byte(_io);
    uint8_t value = 0;
    if ( !kpb_io_read(_io, &value, sizeof(value)) ) return 0;
    return value;
}

uint16_t kpb_io_read_u2be(kpb_io_t *_io)
{
    if ( !kpb_io_ok(_io) ) return 0;
    kpb_io_align_to_byte(_io);
    uint16_t value = 0;
    if ( !kpb_io_read(_io, &value, sizeof(value)) ) return 0;
    if ( byte_order() != KPB_IO_ENDIAN_BE ) return byte_swap_u2(value);
    return value;
}

uint32_t kpb_io_read_u4be(kpb_io_t *_io)
{
    if ( !kpb_io_ok(_io) ) return 0;
    kpb_io_align_to_byte(_io);
    uint32_t value = 0;
    if ( !kpb_io_read(_io, &value, sizeof(value)) ) return 0;
    if ( byte_order() != KPB_IO_ENDIAN_BE ) return byte_swap_u4(value);
    return value;
}

uint64_t kpb_io_read_u8be(kpb_io_t *_io)
{
    if ( !kpb_io_ok(_io) ) return 0;
    kpb_io_align_to_byte(_io);
    uint64_t value = 0;
    if ( !kpb_io_read(_io, &value, sizeof(value)) ) return 0;
    if ( byte_order() != KPB_IO_ENDIAN_BE ) return byte_swap_u8(value);
    return value;
}

uint16_t kpb_io_read_u2le(kpb_io_t *_io)
{
    if ( !kpb_io_ok(_io) ) return 0;
    kpb_io_align_to_byte(_io);
    uint16_t value = 0;
    if ( !kpb_io_read(_io, &value, sizeof(value)) ) return 0;
    if ( byte_order() != KPB_IO_ENDIAN_LE ) return byte_swap_u2(value);
    return value;
}

uint32_t kpb_io_read_u4le(kpb_io_t *_io)
{
    if ( !kpb_io_ok(_io) ) return 0;
    kpb_io_align_to_byte(_io);
    uint32_t value = 0;
    if ( !kpb_io_read(_io, &value, sizeof(value)) ) return 0;
    if ( byte_order() != KPB_IO_ENDIAN_LE ) return byte_swap_u4(value);
    return value;
}

uint64_t kpb_io_read_u8le(kpb_io_t *_io)
{
    if ( !kpb_io_ok(_io) ) return 0;
    kpb_io_align_to_byte(_io);
    uint64_t value = 0;
    if ( !kpb_io_read(_io, &value, sizeof(value)) ) return 0;
    if ( byte_order() != KPB_IO_ENDIAN_LE ) return byte_swap_u8(value);
    return value;
}

float kpb_io_read_f4le(kpb_io_t *_io) {
    if ( !kpb_io_ok(_io) ) return 0.0;
    kpb_io_align_to_byte(_io);
    float value = 0.0;
    if ( !kpb_io_read(_io, &value, sizeof(value)) ) return 0.0;
    if ( byte_order() != KPB_IO_ENDIAN_LE ) return byte_swap_f4(value);
    return value;
}

double kpb_io_read_f8le(kpb_io_t *_io) {
    if ( !kpb_io_ok(_io) ) return 0.0;
    kpb_io_align_to_byte(_io);
    double value = 0.0;
    if ( !kpb_io_read(_io, &value, sizeof(value)) ) return 0.0;
    if ( byte_order() != KPB_IO_ENDIAN_LE ) return byte_swap_f8(value);
    return value;
}

float kpb_io_read_f4be(kpb_io_t *_io) {
    if ( !kpb_io_ok(_io) ) return 0.0;
    kpb_io_align_to_byte(_io);
    float value = 0.0;
    if ( !kpb_io_read(_io, &value, sizeof(value)) ) return 0.0;
    if ( byte_order() != KPB_IO_ENDIAN_BE ) return byte_swap_f4(value);
    return value;
}

double kpb_io_read_f8be(kpb_io_t *_io) {
    if ( !kpb_io_ok(_io) ) return 0.0;
    kpb_io_align_to_byte(_io);
    double value = 0.0;
    if ( !kpb_io_read(_io, &value, sizeof(value)) ) return 0.0;
    if ( byte_order() != KPB_IO_ENDIAN_BE ) return byte_swap_f8(value);
    return value;
}

uint64_t kpb_io_read_bits_int_be(kpb_io_t *_io, int n)
{
    if ( !kpb_io_ok(_io) ) return 0;
    uint64_t value = 0;
    while ( n > 0 )
    {
        if ( _io->stream_view->bits_left == 0 )
        {
            uint8_t value_byte = 0;
            if ( !kpb_io_read(_io, &value_byte, sizeof(value_byte)) )
            {
                return 0;
            }
            _io->stream_view->bits = ((uint64_t)value_byte) & 0xff;
            _io->stream_view->bits_left = 8;
        }
        n--;
        _io->stream_view->bits_left--;
        value <<= 1;
        value &=~ 1;
        value |= (_io->stream_view->bits >> _io->stream_view->bits_left) & 0x1;
    }
    return value;
}

uint64_t kpb_io_read_bits_int(kpb_io_t *_io, int n)
{
    return kpb_io_read_bits_int_be(_io, n);
}

uint64_t kpb_io_read_bits_int_le(kpb_io_t *_io, int n)
{
    // TODO: implemented bits le
    (void)n;
    kpb_io_set_error(_io);
    return 0;
}

char* kpb_io_read_char_str(kpb_io_t *_io, size_t size, char term, bool use_size, bool include_term, bool consume, kpb_io_encoding_t src_enc)
{
    (void)include_term;

    // stop if stream is already broken
    if ( !kpb_io_ok(_io) ) return "";

    // not implemented yet
    if ( !consume )
    {
        kpb_io_set_error(_io);
        return "";
    }

    // only ASCII and UTF8 is supported here.
    if ( (KPB_IO_ENCODING_ASCII != src_enc) && (KPB_IO_ENCODING_UTF8 != src_enc) )
    {
        kpb_io_set_error(_io);
        return "";
    }

    // strings must be byte aligned
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return "";

    // calculate size if not provided (scaning forward to find the terminator)
    if ( !use_size )
    {
        size_t orig_pos = _io->stream_view->pos;
        size = 0;
        while ( true )
        {
            char current = 0;
            if ( kpb_io_remaining(_io) < sizeof(term) )
            {
                kpb_io_set_error(_io);
                return "";
            }
            if ( !kpb_io_read(_io, &current, sizeof(current)) ) return "";
            size += sizeof(current);
            if ( current == term )
            {
                break;
            }
        }
        _io->stream_view->pos = orig_pos;
    }

    // easy
    if ( size == 0 )
    {
        return "";
    }

    // TODO: handle include_term

    // read string
    if ( size > (size + sizeof(char)) )
    {
        kpb_io_set_error(_io);
        return "";
    }
    char *data = (char *)kpb_io_tmp_buffer(_io, size + sizeof(char));
    if ( !data ) return "";
    if ( !kpb_io_read(_io, data, size) ) return "";

    // shorten to first nul
    data[size] = '\x00';
    data[strlen(data)] = '\x00';
    return data;
}

wchar_t* kpb_io_read_wchar_str(kpb_io_t *_io, size_t size, wchar_t term, bool use_size, bool include_term, bool consume, kpb_io_encoding_t src_enc)
{
    (void)include_term;

    // stop if stream is already broken
    if ( !kpb_io_ok(_io) ) return L"";

    // not implemented yet
    if ( !consume )
    {
        kpb_io_set_error(_io);
        return L"";
    }

    // only UTF-16LE is supported here.
    if ( (KPB_IO_ENCODING_UTF16LE != src_enc) && (KPB_IO_ENCODING_UTF16BE != src_enc) )
    {
        kpb_io_set_error(_io);
        return L"";
    }

    // strings must be byte aligned
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return L"";

    // calculate size if not provided (scaning forward to find the terminator)
    kpb_io_utf16_t utf16_term = kpb_io_utf16_wchar(term, src_enc);
    if ( !use_size )
    {
        size_t orig_pos = _io->stream_view->pos;
        size = 0;
        while ( true )
        {
            kpb_io_utf16_t utf16_current = 0;
            if ( kpb_io_remaining(_io) < sizeof(utf16_term) )
            {
                kpb_io_set_error(_io);
                return L"";
            }
            if ( !kpb_io_read(_io, &utf16_current, sizeof(utf16_current)) ) return L"";
            size += sizeof(utf16_current);
            if ( utf16_current == utf16_term )
            {
                break;
            }
        }
        _io->stream_view->pos = orig_pos;
    }

    // easy
    if ( size == 0 )
    {
        return L"";
    }

    // TODO: handle include_term

    // read string
    if ( size > (size + 1) )
    {
        kpb_io_set_error(_io);
        return L"";
    }

    // utf16 string to wide character string
    size_t wchar_size = size * (sizeof(wchar_t) / sizeof(kpb_io_utf16_t));
    wchar_t *data = (wchar_t*)kpb_io_tmp_buffer(_io, wchar_size + sizeof(wchar_t));
    if ( !data ) return L"";
    for ( size_t i = 0; i < (size / sizeof(kpb_io_utf16_t)); i++ )
    {
        kpb_io_utf16_t utf16_current = 0;
        if ( !kpb_io_read(_io, &utf16_current, sizeof(utf16_current)) ) return L"";
        data[i] = kpb_io_wchar_utf16(utf16_current, src_enc);
    }

    // shorten to first nul
    data[(wchar_size / sizeof(wchar_t))] = (wchar_t)0;
    data[wcslen(data)] = (wchar_t)0;

    // done
    return data;
}

static char *_kpb_io_read_bytes(kpb_io_t *_io, size_t size)
{
    _io->last_read_bytes_size = 0;
    if ( !kpb_io_ok(_io) ) return NULL;
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return NULL;
    char *data = (char*)kpb_io_tmp_buffer(_io, size);
    if ( !data ) return NULL;
    if ( !kpb_io_read(_io, data, size) ) return NULL;
    _io->last_read_bytes_size = size;
    return data;
}

char *kpb_io_read_bytes(kpb_io_t *_io, size_t size)
{
    if ( _io->last_call_read_bytes_size )
    {
        _io->last_call_read_bytes = false;
        _io->last_call_read_bytes_size = false;
        return _io->tmp_buffer;
    }
    char *data = _kpb_io_read_bytes(_io, size);
    if ( !data ) return "";
    _io->last_call_read_bytes = true;
    _io->last_call_read_bytes_size = false;
    return data;
}

size_t kpb_io_read_bytes_size(kpb_io_t *_io, size_t size)
{
    if ( _io->last_call_read_bytes )
    {
        _io->last_call_read_bytes = false;
        _io->last_call_read_bytes_size = false;
        return _io->last_read_bytes_size;
    }
    char *data = _kpb_io_read_bytes(_io, size);
    if ( !data ) return 0;
    _io->last_call_read_bytes = false;
    _io->last_call_read_bytes_size = true;
    return _io->last_read_bytes_size;
}

void kpb_io_write_s1(kpb_io_t *_io, int8_t _value)
{
    if ( !kpb_io_ok(_io) ) return;
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;
    kpb_io_write(_io, &_value, sizeof(_value));
}

void kpb_io_write_s2be(kpb_io_t *_io, int16_t _value)
{
    if ( !kpb_io_ok(_io) ) return;
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;
    if ( byte_order() != KPB_IO_ENDIAN_BE ) _value = byte_swap_s2(_value);
    kpb_io_write(_io, &_value, sizeof(_value));
}

void kpb_io_write_s4be(kpb_io_t *_io, int32_t _value)
{
    if ( !kpb_io_ok(_io) ) return;
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;
    if ( byte_order() != KPB_IO_ENDIAN_BE ) _value = byte_swap_s4(_value);
    kpb_io_write(_io, &_value, sizeof(_value));
}

void kpb_io_write_s8be(kpb_io_t *_io, int64_t _value)
{
    if ( !kpb_io_ok(_io) ) return;
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;
    if ( byte_order() != KPB_IO_ENDIAN_BE ) _value = byte_swap_s8(_value);
    kpb_io_write(_io, &_value, sizeof(_value));
}

void kpb_io_write_s2le(kpb_io_t *_io, int16_t _value)
{
    if ( !kpb_io_ok(_io) ) return;
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;
    if ( byte_order() != KPB_IO_ENDIAN_LE ) _value = byte_swap_s2(_value);
    kpb_io_write(_io, &_value, sizeof(_value));
}

void kpb_io_write_s4le(kpb_io_t *_io, int32_t _value)
{
    if ( !kpb_io_ok(_io) ) return;
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;
    if ( byte_order() != KPB_IO_ENDIAN_LE ) _value = byte_swap_s4(_value);
    kpb_io_write(_io, &_value, sizeof(_value));
}

void kpb_io_write_s8le(kpb_io_t *_io, int64_t _value)
{
    if ( !kpb_io_ok(_io) ) return;
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;
    if ( byte_order() != KPB_IO_ENDIAN_LE ) _value = byte_swap_s8(_value);
    kpb_io_write(_io, &_value, sizeof(_value));
}

void kpb_io_write_u1(kpb_io_t *_io, uint8_t _value)
{
    if ( !kpb_io_ok(_io) ) return;
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;
    kpb_io_write(_io, &_value, sizeof(_value));
}

void kpb_io_write_u2be(kpb_io_t *_io, uint16_t _value)
{
    if ( !kpb_io_ok(_io) ) return;
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;
    if ( byte_order() != KPB_IO_ENDIAN_BE ) _value = byte_swap_u2(_value);
    kpb_io_write(_io, &_value, sizeof(_value));
}

void kpb_io_write_u4be(kpb_io_t *_io, uint32_t _value)
{
    if ( !kpb_io_ok(_io) ) return;
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;
    if ( byte_order() != KPB_IO_ENDIAN_BE ) _value = byte_swap_u4(_value);
    kpb_io_write(_io, &_value, sizeof(_value));
}

void kpb_io_write_u8be(kpb_io_t *_io, uint64_t _value)
{
    if ( !kpb_io_ok(_io) ) return;
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;
    if ( byte_order() != KPB_IO_ENDIAN_BE ) _value = byte_swap_u8(_value);
    kpb_io_write(_io, &_value, sizeof(_value));
}

void kpb_io_write_u2le(kpb_io_t *_io, uint16_t _value)
{
    if ( !kpb_io_ok(_io) ) return;
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;
    if ( byte_order() != KPB_IO_ENDIAN_LE ) _value = byte_swap_u2(_value);
    kpb_io_write(_io, &_value, sizeof(_value));
}

void kpb_io_write_u4le(kpb_io_t *_io, uint32_t _value)
{
    if ( !kpb_io_ok(_io) ) return;
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;
    if ( byte_order() != KPB_IO_ENDIAN_LE ) _value = byte_swap_u4(_value);
    kpb_io_write(_io, &_value, sizeof(_value));
}

void kpb_io_write_u8le(kpb_io_t *_io, uint64_t _value)
{
    if ( !kpb_io_ok(_io) ) return;
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;
    if ( byte_order() != KPB_IO_ENDIAN_LE ) _value = byte_swap_u8(_value);
    kpb_io_write(_io, &_value, sizeof(_value));
}

void kpb_io_write_f4le(kpb_io_t *_io, float _value) {
    if ( !kpb_io_ok(_io) ) return;
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;
    if ( byte_order() != KPB_IO_ENDIAN_LE ) _value = byte_swap_f4(_value);
    kpb_io_write(_io, &_value, sizeof(_value));
}

void kpb_io_write_f8le(kpb_io_t *_io, double _value) {
    if ( !kpb_io_ok(_io) ) return;
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;
    if ( byte_order() != KPB_IO_ENDIAN_LE ) _value = byte_swap_f8(_value);
    kpb_io_write(_io, &_value, sizeof(_value));
}

void kpb_io_write_f4be(kpb_io_t *_io, float _value) {
    if ( !kpb_io_ok(_io) ) return;
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;
    if ( byte_order() != KPB_IO_ENDIAN_BE ) _value = byte_swap_f4(_value);
    kpb_io_write(_io, &_value, sizeof(_value));
}

void kpb_io_write_f8be(kpb_io_t *_io, double _value) {
    if ( !kpb_io_ok(_io) ) return;
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;
    if ( byte_order() != KPB_IO_ENDIAN_BE ) _value = byte_swap_f8(_value);
    kpb_io_write(_io, &_value, sizeof(_value));
}

void kpb_io_write_bits_int_be(kpb_io_t *_io, int n, uint64_t _bits)
{
    if ( !kpb_io_ok(_io) ) return;
    while ( n > 0 )
    {
        while ( _io->stream_view->bits_left >= 8 )
        {
            uint8_t value = (uint8_t)((_io->stream_view->bits >> (_io->stream_view->bits_left - 8)) & 0xff);
            if ( !kpb_io_write(_io, &value, sizeof(value)) )
            {
                return;
            }
            // no need to zero top bits
            _io->stream_view->bits_left -= 8;
        }
        n--;
        _io->stream_view->bits_left++;  // this might take us back up to 8-bits
        _io->stream_view->bits <<= 1;
        _io->stream_view->bits &=~ 1;
        _io->stream_view->bits |= (_bits >> n) & 1;
    }
    if ( _io->stream_view->bits_left >= 8 ) // if back up to 8-bits - flush
    {
        uint8_t value = (uint8_t)((_io->stream_view->bits >> (_io->stream_view->bits_left - 8)) & 0xff);
        if ( !kpb_io_write(_io, &value, sizeof(value)) )
        {
            return;
        }
        // no need to zero top bits
        _io->stream_view->bits_left -= 8;
    }
}

void kpb_io_write_bits_int(kpb_io_t *_io, int n, uint64_t _bits)
{
    kpb_io_write_bits_int_be(_io, n, _bits);
}

void kpb_io_write_bits_int_le(kpb_io_t *_io, int n, uint64_t _bits)
{
    (void)n;
    (void)_bits;
    // TODO: implemented bits le
    kpb_io_set_error(_io);
}

void kpb_io_write_char_str(kpb_io_t *_io, const char *str, size_t size, char term, bool use_size, bool include_term, kpb_io_encoding_t src_enc)
{
    // stop if stream is already broken
    if ( !kpb_io_ok(_io) ) return;

    // only ASCII is supported here.
    if ( (KPB_IO_ENCODING_ASCII != src_enc) && (KPB_IO_ENCODING_UTF8 != src_enc) )
    {
        kpb_io_set_error(_io);
        return;
    }

    // strings must be byte aligned
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;

    // stop right now if not writing anything
    if ( use_size && (0 == size) )
    {
        return;
    }

    // calculate length
    size_t length = 0;
    const char *term_pos = strchr(str, term);
    if ( term_pos )
    {
        // use the position of the terminator to determine string length
        length = (size_t)(term_pos - str);
    }
    else
    {
        // use the nul to determine the string length
        length = strlen(str);
    }

    // calculate size if not provided (scaning forward to find the terminator)
    if ( use_size )
    {
        if ( include_term )
        {
            if ( size < sizeof(char) )
            {
                // not enough space to include the terminator
                kpb_io_set_error(_io);
                return;
            }

            // leave space to include the terminator
            size -= sizeof(char);
        }

        // clip the string so it fits.
        if ( length > size )
        {
            length = size;
        }
    }
    else
    {
        // we are using as much as we need
        size = length;
    }

    // write characters in string
    if ( !kpb_io_write(_io, str, length) ) return;

    // fill with terminators
    while ( length < size )
    {
        if ( !kpb_io_write(_io, &term, sizeof(term)) ) return;
        length += sizeof(term);
    }

    // terminate
    if ( include_term )
    {
        if ( !kpb_io_write(_io, &term, sizeof(term)) ) return;
    }

    // done
    return;
}

void kpb_io_write_wchar_str(kpb_io_t *_io, const wchar_t *str, size_t size, wchar_t term, bool use_size, bool include_term, kpb_io_encoding_t src_enc)
{
    // stop if stream is already broken
    if ( !kpb_io_ok(_io) ) return;

    // only UTF-16[LE|BE] is supported here.
    if ( (KPB_IO_ENCODING_UTF16LE != src_enc) && (KPB_IO_ENCODING_UTF16BE != src_enc) )
    {
        kpb_io_set_error(_io);
        return;
    }

    // strings must be byte aligned
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;

    // stop right now if not writing anything
    if ( use_size && (0 == size) )
    {
        return;
    }

    // convert wchar
    kpb_io_utf16_t utf16_term = kpb_io_utf16_wchar(term, src_enc);
    kpb_io_utf16_t *utf16_str = kpb_io_utf16_dupwchar(str, src_enc);
    if ( !utf16_str )
    {
        kpb_io_set_error(_io);
        return;
    }

    // calculate length
    size_t length = 0;
    const kpb_io_utf16_t *term_pos = kpb_io_utf16_chr(utf16_str, utf16_term);
    if ( term_pos )
    {
        // use the position of the terminator to determine string length
        length = ((size_t)(term_pos - utf16_str));
    }
    else
    {
        // use the nul to determine the string length
        length = kpb_io_utf16_len(utf16_str);
    }

    // calculate size if not provided (scaning forward to find the terminator)
    if ( use_size )
    {
        if ( include_term )
        {
            if ( size < sizeof(kpb_io_utf16_t) )
            {
                // not enough space to include the terminator
                kpb_io_set_error(_io);
                goto out_cleaup;
            }

            // leave space to include the terminator
            size -= sizeof(kpb_io_utf16_t);
        }

        // clip the string so it fits.
        if ( (length * sizeof(kpb_io_utf16_t)) > size )
        {
            length = size / sizeof(kpb_io_utf16_t);
        }
    }
    else
    {
        // we are using as much as we need
        size = length * sizeof(kpb_io_utf16_t);
    }

    // write characters in string
    if ( !kpb_io_write(_io, utf16_str, length * sizeof(kpb_io_utf16_t)) ) goto out_cleaup;

    // fill with terminators
    while ( length < (size / sizeof(kpb_io_utf16_t)) )
    {
        if ( !kpb_io_write(_io, &utf16_term, sizeof(utf16_term)) ) goto out_cleaup;
        length += 1;
    }

    // terminate
    if ( include_term )
    {
        if ( !kpb_io_write(_io, &utf16_term, sizeof(utf16_term)) ) goto out_cleaup;
    }

    // add zeros for odd sizes
    if ( (size % sizeof(kpb_io_utf16_t)) != 0 )
    {
        char pad = 0x00;
        if ( !kpb_io_write(_io, &pad, sizeof(pad)) ) goto out_cleaup;
    }

    // done
out_cleaup:
    kpb_io_free(utf16_str);
    return;
}

void kpb_io_write_bytes(kpb_io_t *_io, const char *data, size_t size, size_t write_size)
{
    if ( !kpb_io_ok(_io) ) return;
    kpb_io_align_to_byte(_io);
    if ( !kpb_io_ok(_io) ) return;
    if ( size >= write_size )
    {
        kpb_io_write(_io, data, write_size);
        return;
    }
    if ( !kpb_io_write(_io, data, size) ) return;
    write_size -= size;
    while (write_size > 0)
    {
        if ( !kpb_io_write(_io, "\x00", 1) ) return;
        write_size--;
    }
}

bool kpb_io_read(kpb_io_t *_io, void *data, size_t size)
{
    size_t padding_size = 0;

    if ( !kpb_io_ok(_io) ) return false;

    // cannot read from a writable stream
    if ( kpb_io_writable(_io) )
    {
        kpb_io_set_error(_io);
        return false;
    }

    // how to handle not enough data for read
    padding_size = 0;
    if ( size > kpb_io_remaining(_io) )
    {
        if ( !_io->fill )
        {
            kpb_io_set_error(_io);
            return false;
        }
        else
        {
            _io->stream_view->fill_eof = true;
            padding_size = size - kpb_io_remaining(_io);
            size = kpb_io_remaining(_io);
        }
    }

    // read & move stream
    memcpy(data, _io->stream_data->buffer + _io->stream_view->view_offset + kpb_io_pos(_io), size);
    memset(((char*)data) + size, _io->fill_byte, padding_size);
    _io->stream_view->pos += size; // TODO: need to move through padding if fill=true
    return true;
}

bool kpb_io_write(kpb_io_t *_io, const void *data, size_t size)
{
    if ( !kpb_io_ok(_io) ) return false;

    // cannot write a read-only stream
    if ( kpb_io_readonly(_io) )
    {
        kpb_io_set_error(_io);
        return false;
    }

    // resize buffer OR cap the size of the write
    if ( _io->stream_view->view_size_fixed )
    {
        // cap the write for fixed-sized buffers
        size = (size < kpb_io_remaining(_io) ) ? size : kpb_io_remaining(_io);
    }
    else
    {
        // calculate the minimum size of the buffer to complete the write
        size_t minimum_buffer_size = _io->stream_view->view_offset;
        if ( (minimum_buffer_size + _io->stream_view->pos) < minimum_buffer_size )
        {
            // overflow
            kpb_io_set_error(_io);
            return false;
        }
        minimum_buffer_size += _io->stream_view->pos;
        if ( (minimum_buffer_size + size) < minimum_buffer_size )
        {
            // overflow
            kpb_io_set_error(_io);
            return false;
        }
        minimum_buffer_size += size;

        // fill with zeros until the buffer reaches the desired write position and size
        if  ( _io->stream_data->buffer_size < minimum_buffer_size )
        {
            if ( !kpb_io_buffer_resize(_io, minimum_buffer_size) ) return false;
        }
    }

    // stop here if there is nothing to write
    if ( size == 0 )
    {
        return true;
    }

    // place data within existing buffer
    size_t abs_pos = _io->stream_view->view_offset + kpb_io_pos(_io);
    memcpy(_io->stream_data->buffer + abs_pos, data, size);
    _io->stream_view->pos += size;
    return true;
}

void kpb_list_clear(kpb_list_t *list)
{
    kpb_io_free(list->mem);
    list->list = ((void*)"");
    list->size = 0;
    list->mem = NULL;
    list->mem_size = 0;
}

bool kpb_list_add(kpb_list_t *list, void *value, size_t value_size)
{
    if ( (list->size + 1) < list->size )
    {
        return false;
    }
    size_t new_mem_size = (list->size + 1) * value_size;
    if ( new_mem_size < (list->size + 1) )
    {
        return false;
    }
    void *new_mem = kpb_io_malloc(new_mem_size);
    if ( !new_mem )
    {
        return false;
    }
    memcpy(new_mem, list->list, list->size * value_size);
    memcpy((char*)new_mem + (list->size * value_size), value, value_size);
    kpb_io_free(list->mem);
    list->mem = new_mem;
    list->mem_size = new_mem_size;
    list->list = list->mem;
    list->size = list->size + 1;
    return true;
}

void kpb_string_clear(kpb_string_t *str)
{
    kpb_io_free(str->mem);
    str->str = ((void*)L"");
    str->mem = NULL;
    str->mem_size = 0;
}

static void *kpb_string_alloc(size_t size, size_t char_size, size_t *out_new_mem_size)
{
    size_t new_mem_size = 0;
    switch(char_size)
    {
        case sizeof(char):
            new_mem_size = size;
            if ( new_mem_size > (new_mem_size + 1) )
            {
                return NULL;
            }
            new_mem_size += 1;
            break;
        case sizeof(wchar_t):
            new_mem_size = (size / sizeof(kpb_io_utf16_t));
            if ( new_mem_size > ((new_mem_size + 1) * sizeof(wchar_t)) )
            {
                return NULL;
            }
            new_mem_size = ((new_mem_size + 1) * sizeof(wchar_t));
            break;
        default:
            return NULL;
    }
    void *new_mem = kpb_io_malloc(new_mem_size);
    if ( !new_mem )
    {
        return NULL;
    }
    memset(new_mem, 0, new_mem_size);
    if ( out_new_mem_size )
    {
        *out_new_mem_size = new_mem_size;
    }
    return new_mem;
}

static void *kpb_string_dup(const void *value, size_t char_size, size_t *out_new_mem_size)
{
    if ( !value )
    {
        return NULL;
    }
    size_t new_mem_size = 0;
    switch(char_size)
    {
        case sizeof(char):
            new_mem_size = strlen((const char*)value);
            if ( new_mem_size > (new_mem_size + 1) )
            {
                return NULL;
            }
            new_mem_size += 1;
            break;
        case sizeof(wchar_t):
            new_mem_size = wcslen((const wchar_t*)value);
            if ( new_mem_size > ((new_mem_size + 1) * sizeof(wchar_t)) )
            {
                return NULL;
            }
            new_mem_size = ((new_mem_size + 1) * sizeof(wchar_t));
            break;
        default:
            return NULL;
    }
    void *new_mem = kpb_io_malloc(new_mem_size);
    if ( !new_mem )
    {
        return NULL;
    }
    memcpy(new_mem, value, new_mem_size);
    if ( out_new_mem_size )
    {
        *out_new_mem_size = new_mem_size;
    }
    return new_mem;
}

bool kpb_string_set(kpb_string_t *str, const void *value, size_t char_size)
{
    size_t new_mem_size = 0;
    void *new_mem = kpb_string_dup(value, char_size, &new_mem_size);
    if ( !new_mem )
    {
        return false;
    }
    kpb_io_free(str->mem);
    str->mem = new_mem;
    str->mem_size = new_mem_size;
    str->str = new_mem;
    return true;
}

bool kpb_string_resize(kpb_string_t *str, size_t size, size_t char_size)
{
    size_t new_mem_size = 0;
    void *new_mem = kpb_string_alloc(size, char_size, &new_mem_size);
    if ( !new_mem )
    {
        return false;
    }
    kpb_io_free(str->mem);
    str->mem = new_mem;
    str->mem_size = new_mem_size;
    str->str = new_mem;
    return true;
}

size_t kpb_string_size(const void *str, size_t char_size)
{
    if ( str )
    {
        switch (char_size)
        {
            case sizeof(char):
                return strlen((const char*)str);
            case sizeof(wchar_t):
                return wcslen((const wchar_t*)str);
        }
    }
    return 0;
}

void kpb_string_list_clear(kpb_string_list_t *list)
{
    // mem_priv has pointers to memory allocated internally
    // these are the ones we want to free
    if ( list->mem_priv )
    {
        for ( size_t i = 0; i < list->mem_size; i++ )
        {
            void *str = list->mem_priv[i];
            if ( str )
            {
                kpb_io_free(str);
            }
        }
    }
    kpb_io_free(list->mem);
    kpb_io_free(list->mem_priv);
    list->list = ((void*)"");
    list->size = 0;
    list->mem = NULL;
    list->mem_priv = NULL;
    list->mem_size = 0;
}

bool kpb_string_list_add(kpb_string_list_t *list, const void *value, size_t char_size)
{
    // new list size
    if ( (list->size + 1) < list->size )
    {
        return false;
    }
    size_t new_mem_size = (list->size + 1) * sizeof(void*);
    if ( new_mem_size < (list->size + 1) )
    {
        return false;
    }

    // allocate new memory
    void *new_str_mem = kpb_string_dup(value, char_size, NULL);
    void **new_mem = (void **)kpb_io_malloc(new_mem_size);
    void **new_mem_priv = (void **)kpb_io_malloc(new_mem_size);
    if ( !new_str_mem || !new_mem || !new_mem_priv )
    {
        kpb_io_free(new_str_mem);
        kpb_io_free(new_mem);
        kpb_io_free(new_mem_priv);
        return false;
    }

    // copy the new list (with pointers that can be externally changed)
    memcpy(new_mem, list->list, new_mem_size);
    new_mem[list->size] = new_str_mem;

    // copy pointers to existing internal allocations that need to be tracked
    memset(new_mem_priv, 0, new_mem_size);
    if ( list->mem_priv )
    {
        memcpy(new_mem_priv, list->mem_priv,  list->mem_size * sizeof(void*));
    }
    new_mem_priv[list->size] = new_str_mem;

    // copy pointers to that can be manipulated externally
    memset(new_mem, 0, new_mem_size);
    if ( list->list )
    {
        memcpy(new_mem, list->list, list->size * sizeof(void*));
    }
    for ( size_t i = 0; i < list->size; i++ )
    {
        if ( !new_mem[i] )
        {
            new_mem[i] = (void*)L"";
        }
    }
    new_mem[list->size] = new_str_mem;

    // assign new
    kpb_io_free(list->mem);
    list->mem = new_mem;
    list->list = list->mem;
    kpb_io_free(list->mem_priv);
    list->mem_priv = new_mem_priv;
    list->size += 1;
    list->mem_size = list->size;

    // done
    return true;
}

bool kpb_string_list_set_at(kpb_string_list_t *list, size_t index, const void *value, size_t char_size)
{
    // must be in bounds (and must be in bounds of an allocated memory list - so it will get freed)
    if ( (index >= list->size ) || (index >= list->mem_size) || (list->list != list->mem) )
    {
        return false;
    }

    // duplicate string
    void *new_str_mem = kpb_string_dup(value, char_size, NULL);
    if ( !new_str_mem )
    {
        return false;
    }

    // swap to new string allocation
    kpb_io_free(list->mem_priv[index]);
    list->mem_priv[index] = new_str_mem;

    // put allocation in string
    list->mem[index] = list->mem_priv[index];

    // done
    return true;
}

bool kpb_string_list_resize_at(kpb_string_list_t *list, size_t index, size_t size, size_t char_size)
{
    // must be in bounds (and must be in bounds of an allocated memory list - so it will get freed)
    if ( (index >= list->size ) || (index >= list->mem_size) || (list->list != list->mem) )
    {
        return false;
    }

    // duplicate string
    void *new_str_mem = kpb_string_alloc(size, char_size, NULL);
    if ( !new_str_mem )
    {
        return false;
    }

    // swap to new string allocation
    kpb_io_free(list->mem_priv[index]);
    list->mem_priv[index] = new_str_mem;

    // put allocation in string
    list->mem[index] = list->mem_priv[index];

    // done
    return true;
}

void kpb_bytes_clear(kpb_bytes_t *bytes, const char *ro_default, size_t ro_default_size)
{
    kpb_io_free(bytes->mem);
    bytes->data = ((void*)ro_default);
    bytes->size = ro_default_size;
    bytes->mem = NULL;
    bytes->mem_size = 0;
}

bool kpb_bytes_set(kpb_bytes_t *bytes, const char *data, size_t size)
{
    if ( 0 == size )
    {
        bytes->data = "";
        bytes->size = 0;
        return true;
    }
    if ( bytes->mem_size < size )
    {
        void *new_mem = kpb_io_malloc(size);
        if ( !new_mem )
        {
            return false;
        }
        kpb_io_free(bytes->mem);
        bytes->mem = new_mem;
        bytes->mem_size = size;
    }
    if ( data )
    {
        memcpy(bytes->mem, data, size);
    }
    else
    {
        memset(bytes->mem, 0, size);
    }
    bytes->data = bytes->mem;
    bytes->size = size;
    return true;
}

bool kpb_bytes_resize(kpb_bytes_t *bytes, size_t size)
{
    return kpb_bytes_set(bytes, NULL, size);
}

void kpb_bytes_list_clear(kpb_bytes_list_t *list)
{
    // mem_priv has pointers to memory allocated internally
    // these are the ones we want to free
    if ( list->mem_priv )
    {
        for ( size_t i = 0; i < list->mem_size; i++ )
        {
            void *str = list->mem_priv[i];
            if ( str )
            {
                kpb_io_free(str);
            }
        }
    }
    kpb_io_free(list->mem);
    kpb_io_free(list->mem_sizeof);
    kpb_io_free(list->mem_priv);
    list->list = ((void*)"");
    list->list_sizeof = ((size_t*)"");
    list->size = 0;
    list->mem = NULL;
    list->mem_sizeof = NULL;
    list->mem_priv = NULL;
    list->mem_size = 0;
}

bool kpb_bytes_list_add(kpb_bytes_list_t *list, const char *data, size_t size)
{
    // must have data
    if ( !data )
    {
        return false;
    }

    // data size 
    size_t new_data_mem_size = size;
    if ( 0 == new_data_mem_size )
    {
        new_data_mem_size = 1;
    }

    // new list size
    if ( (list->size + 1) < list->size )
    {
        return false;
    }
    size_t new_mem_size = (list->size + 1) * sizeof(void*);
    if ( new_mem_size < (list->size + 1) )
    {
        return false;
    }
    size_t new_mem_sizeof_size = (list->size + 1) * sizeof(size_t);
    if ( new_mem_sizeof_size < (list->size + 1) )
    {
        return false;
    }

    // allocate new memory
    void *new_data_mem = kpb_io_malloc(new_data_mem_size);
    void **new_mem = (void **)kpb_io_malloc(new_mem_size);
    size_t *new_mem_sizeof = (size_t *)kpb_io_malloc(new_mem_sizeof_size);
    void **new_mem_priv = (void **)kpb_io_malloc(new_mem_size);
    if ( !new_data_mem || !new_mem || !new_mem_sizeof || !new_mem_priv )
    {
        kpb_io_free(new_data_mem);
        kpb_io_free(new_mem);
        kpb_io_free(new_mem_sizeof);
        kpb_io_free(new_mem_priv);
        return false;
    }

    // copy data
    memcpy(new_data_mem, data, size);

    // copy the new list (with pointers that can be externally changed)
    memcpy(new_mem, list->list, new_mem_size);
    new_mem[list->size] = new_data_mem;

    // copy the new sizeof list (with sizes that can be externally changed)
    memcpy(new_mem_sizeof, list->list_sizeof, new_mem_sizeof_size);
    new_mem_sizeof[list->size] = size;

    // copy pointers to existing internal allocations that need to be tracked
    memset(new_mem_priv, 0, new_mem_size);
    if ( list->mem_priv )
    {
        memcpy(new_mem_priv, list->mem_priv,  list->mem_size * sizeof(void*));
    }
    new_mem_priv[list->size] = new_data_mem;

    // copy pointers to that can be manipulated externally
    memset(new_mem, 0, new_mem_size);
    if ( list->list )
    {
        memcpy(new_mem, list->list, list->size * sizeof(void*));
    }
    for ( size_t i = 0; i < list->size; i++ )
    {
        if ( !new_mem[i] )
        {
            new_mem[i] = (void*)L"";
            new_mem_sizeof[i] = 0;
        }
    }
    new_mem[list->size] = new_data_mem;

    // assign new
    kpb_io_free(list->mem);
    list->mem = new_mem;
    list->list = list->mem;
    kpb_io_free(list->mem_sizeof);
    list->mem_sizeof = new_mem_sizeof;
    list->list_sizeof = list->mem_sizeof;
    kpb_io_free(list->mem_priv);
    list->mem_priv = new_mem_priv;
    list->size += 1;
    list->mem_size = list->size;

    // done
    return true;
}

bool kpb_bytes_list_set_at(kpb_bytes_list_t *list, size_t index, const char *data, size_t size)
{
    // must be in bounds (and must be in bounds of an allocated memory list - so it will get freed)
    if ( (index >= list->size ) || (index >= list->mem_size) || (list->list != list->mem) || (list->list_sizeof != list->mem_sizeof) )
    {
        return false;
    }

    // new memory size
    size_t new_mem_size = size ? size : 1;

    // realloc if not big enough
    if ( list->mem_sizeof[index] < new_mem_size )
    {
        // alloc
        void *new_mem = kpb_io_malloc(new_mem_size);
        if ( !new_mem )
        {
            return false;
        }

        // swap to new allocation
        kpb_io_free(list->mem_priv[index]);
        list->mem_priv[index] = new_mem;
        list->mem_sizeof[index] = new_mem_size;

        // put allocation in string
        list->mem[index] = list->mem_priv[index];
    }

    // set data
    if ( data && size )
    {
        // copy
        memcpy(list->mem_priv[index], data, size);

        // zero remaining
        if ( list->mem_sizeof[index] > size )
        {
            memset(((char*)(list->mem_priv[index])) + size, 0, list->mem_sizeof[index] - size);
        }
    }
    else
    {
        memset(list->mem_priv[index], 0, list->mem_sizeof[index]);
    }

    // set size
    list->list_sizeof[index] = size;

    // done
    return true;
}

bool kpb_bytes_list_resize_at(kpb_bytes_list_t *list, size_t index, size_t size)
{
    return kpb_bytes_list_set_at(list, index, NULL, size);
}

void kpb_object_clear(kpb_object_t *obj, kpb_object_fini_t fini, kpb_object_default_t default_)
{
    (void)default_;
    if ( obj->mem )
    {
        fini(obj->mem);
        kpb_io_free(obj->mem);
        obj->mem = NULL;
    }
    obj->obj = NULL;
}

void *kpb_object_lazy_constructor(kpb_object_t *obj, size_t obj_size, kpb_object_fini_t init)
{
    if ( !obj->mem )
    {
        obj->mem = kpb_io_malloc(obj_size);
        if ( obj->mem )
        {
            init(obj->mem);
        }
    }
    obj->obj = obj->mem;
    return obj->obj;
}

void kpb_object_list_clear(kpb_object_list_t *list, size_t obj_size, kpb_object_fini_t fini)
{
    (void)obj_size;
    // mem_priv has pointers to memory allocated internally
    // these are the ones we want to free
    if ( list->mem_priv )
    {
        for ( size_t i = 0; i < list->mem_size; i++ )
        {
            void *obj = list->mem_priv[i];
            if ( obj )
            {
                fini(obj);
                kpb_io_free(obj);
            }
        }
    }
    kpb_io_free(list->mem);
    kpb_io_free(list->mem_priv);
    list->list = ((void*)"");
    list->size = 0;
    list->mem = NULL;
    list->mem_priv = NULL;
    list->mem_size = 0;
}

void *kpb_object_list_add(kpb_object_list_t *list, size_t obj_size, kpb_object_fini_t init)
{
    // new list size
    if ( (list->size + 1) < list->size )
    {
        return NULL;
    }
    size_t new_mem_size = (list->size + 1) * sizeof(void*);
    if ( new_mem_size < (list->size + 1) )
    {
        return NULL;
    }

    // allocate new memory
    void *new_obj_mem = kpb_io_malloc(obj_size);
    void **new_mem = (void **)kpb_io_malloc(new_mem_size);
    void **new_mem_priv = (void **)kpb_io_malloc(new_mem_size);
    if ( !new_obj_mem || !new_mem || !new_mem_priv )
    {
        kpb_io_free(new_obj_mem);
        kpb_io_free(new_mem);
        kpb_io_free(new_mem_priv);
        return NULL;
    }

    // init obj
    init(new_obj_mem);

    // copy the new list (with pointers that can be externally changed)
    memcpy(new_mem, list->list, new_mem_size);
    new_mem[list->size] = new_obj_mem;

    // copy pointers to existing internal allocations that need to be tracked
    memset(new_mem_priv, 0, new_mem_size);
    if ( list->mem_priv )
    {
        memcpy(new_mem_priv, list->mem_priv,  list->mem_size * sizeof(void*));
    }
    new_mem_priv[list->size] = new_obj_mem;

    // copy pointers to that can be manipulated externally
    memset(new_mem, 0, new_mem_size);
    if ( list->list )
    {
        memcpy(new_mem, list->list, list->size * sizeof(void*));
    }
    new_mem[list->size] = new_obj_mem;

    // assign new
    kpb_io_free(list->mem);
    list->mem = new_mem;
    list->list = list->mem;
    kpb_io_free(list->mem_priv);
    list->mem_priv = new_mem_priv;
    list->size += 1;
    list->mem_size = list->size;

    // done
    return new_obj_mem;
}

static random_bytes_fn_t g_random_bytes_fn = NULL;
static void *g_random_bytes_ctx = NULL;

void kpb_random_api(random_bytes_fn_t func, void *ctx)
{
    g_random_bytes_fn = func;
    g_random_bytes_ctx = ctx;
}

static bool kpb_random_fill(void *data, size_t size)
{
    kpb_io_memset(data, 0, size);
    if ( !g_random_bytes_fn )
    {
        return false;
    }
    return g_random_bytes_fn(g_random_bytes_ctx, (char*)data, size);
}

#define KPB_SWAP_MINMAX_OR_RETURN_IF_EQUAL(t,a,b) \
    if (a == b) \
    { \
        return a; \
    } \
    if (a > b) \
    { \
        t tmp = b; \
        b = a; \
        a = tmp; \
    }

#define KPB_RETURN_RANDOM_UNSIGNED_INT(t,kt,a,b) \
    KPB_SWAP_MINMAX_OR_RETURN_IF_EQUAL(t,a,b) \
    t value = 0; \
    (void)kpb_random_fill(&value, sizeof(value)); \
    if ( byte_order() != KPB_IO_ENDIAN_BE ) value = byte_swap_##kt(value); \
    t maxv = 1; \
    maxv <<= ((sizeof(t)*8)-1); \
    maxv |= ~maxv; \
    if ((a == 0) && (b == maxv)) \
    { \
        return value; \
    } \
    value = (value % (b - a + 1)) + a; \
    return value;

#define KPB_RETURN_RANDOM_SIGNED_INT(t,kt,a,b) \
    KPB_SWAP_MINMAX_OR_RETURN_IF_EQUAL(t,a,b) \
    t value = 0; \
    (void)kpb_random_fill(&value, sizeof(value)); \
    if ( byte_order() != KPB_IO_ENDIAN_BE ) value = byte_swap_##kt(value); \
    t minv = 1; \
    minv <<= ((sizeof(t)*8)-1); \
    t maxv = minv - 1; \
    if ( (a == minv) && (b == maxv) ) \
    { \
        return value + a; \
    } \
    u##t range = b - a + 1; \
    value = (t)(((*((u##t*)(&value))) % range) + (*((u##t*)(&a)))); \
    return value;

int8_t kpb_random_s1(int8_t min_inclusive, int8_t max_inclusive)
{
    KPB_RETURN_RANDOM_SIGNED_INT(int8_t, s1, min_inclusive, max_inclusive);
}

int16_t kpb_random_s2(int16_t min_inclusive, int16_t max_inclusive)
{
    KPB_RETURN_RANDOM_SIGNED_INT(int16_t, s2, min_inclusive, max_inclusive);
}

int32_t kpb_random_s4(int32_t min_inclusive, int32_t max_inclusive)
{
    KPB_RETURN_RANDOM_SIGNED_INT(int32_t, s4, min_inclusive, max_inclusive);
}

int64_t kpb_random_s8(int64_t min_inclusive, int64_t max_inclusive)
{
    KPB_RETURN_RANDOM_SIGNED_INT(int64_t, s8, min_inclusive, max_inclusive);
}

uint8_t kpb_random_u1(uint8_t min_inclusive, uint8_t max_inclusive)
{
    KPB_RETURN_RANDOM_UNSIGNED_INT(uint8_t, u1, min_inclusive, max_inclusive);
}

uint16_t kpb_random_u2(uint16_t min_inclusive, uint16_t max_inclusive)
{
    KPB_RETURN_RANDOM_UNSIGNED_INT(uint16_t, u2, min_inclusive, max_inclusive);
}

uint32_t kpb_random_u4(uint32_t min_inclusive, uint32_t max_inclusive)
{
    KPB_RETURN_RANDOM_UNSIGNED_INT(uint32_t, u4, min_inclusive, max_inclusive);
}

uint64_t kpb_random_u8(uint64_t min_inclusive, uint64_t max_inclusive)
{
    KPB_RETURN_RANDOM_UNSIGNED_INT(uint64_t, u8, min_inclusive, max_inclusive);
}

float kpb_random_f4(float min_inclusive, float max_inclusive)
{
    KPB_SWAP_MINMAX_OR_RETURN_IF_EQUAL(float, min_inclusive, max_inclusive);
    float range = max_inclusive - min_inclusive;
    uint64_t random_range = 0xffffffffffffffff;
    uint64_t random_value = kpb_random_u8(0, random_range);
    if ( random_value == 0 )
    {
        return min_inclusive;
    }
    if ( random_value == random_range )
    {
        return max_inclusive;
    }
    float offset = (float)((((double)random_value) * ((double)range)) / ((double)random_range));
    float value = min_inclusive + offset;
    if ( value < min_inclusive )
    {
        return min_inclusive;
    }
    if ( value > max_inclusive )
    {
        return max_inclusive;
    }
    return value;
}

double kpb_random_f8(double min_inclusive, double max_inclusive)
{
    KPB_SWAP_MINMAX_OR_RETURN_IF_EQUAL(double, min_inclusive, max_inclusive);
    double range = max_inclusive - min_inclusive;
    uint64_t random_range = 0xffffffffffffffff;
    uint64_t random_value = kpb_random_u8(0, random_range);
    if ( random_value == 0 )
    {
        return min_inclusive;
    }
    if ( random_value == random_range )
    {
        return max_inclusive;
    }
    double offset = (((double)random_value) * range) / ((double)random_range);
    double value = min_inclusive + offset;
    if ( value < min_inclusive )
    {
        return min_inclusive;
    }
    if ( value > max_inclusive )
    {
        return max_inclusive;
    }
    return value;
}

bool kpb_random_bool(bool min_inclusive, bool max_inclusive)
{
    if ( min_inclusive == max_inclusive )
    {
        return min_inclusive;
    }
    return (kpb_random_u1(0x00, 0xff) & 0x01) != 0;
}

size_t kpb_random_size(size_t min_inclusive, size_t max_inclusive)
{
    if (min_inclusive > 0xffffffff)
    {
        min_inclusive = 0xffffffff;
    }
    if (max_inclusive > 0xffffffff)
    {
        max_inclusive = 0xffffffff;
    }
    return (size_t)kpb_random_u4((uint32_t)min_inclusive, (uint32_t)max_inclusive);
}

bool kpb_random_bytes(kpb_bytes_t *bytes, size_t min_size, size_t max_size, uint8_t min_byte, uint8_t max_byte)
{
    size_t size = kpb_random_size(min_size, max_size);
    if ( !kpb_bytes_resize(bytes, size) )
    {
        return false;
    }
    for ( size_t i = 0; i < size; i++ )
    {
        ((char*)(bytes->data))[i] = (char)kpb_random_u1(min_byte, max_byte);
    }
    return true;
}

bool kpb_random_char_str(kpb_string_t *str, size_t min_size, size_t max_size, uint32_t min_char, uint32_t max_char, kpb_io_encoding_t enc)
{
    size_t size = kpb_random_size(min_size, max_size);
    if ( !kpb_string_resize(str, size, sizeof(char)) )
    {
        return false;
    }
    // TODO: this may not be correct
    (void)enc;
    if (min_char > 0xff)
    {
        min_char = 0xff;
    }
    if (max_char > 0xff)
    {
        max_char = 0xff;
    }
    for ( size_t i = 0; i < size; i++ )
    {
        ((uint8_t*)(str->str))[i] = kpb_random_u1((uint8_t)min_char, (uint8_t)max_char);
    }
    return true;
}

bool kpb_random_wchar_str(kpb_string_t *str, size_t min_size, size_t max_size, uint32_t min_char, uint32_t max_char, kpb_io_encoding_t enc)
{
    size_t size = kpb_random_size(min_size, max_size);
    if ( !kpb_string_resize(str, size, sizeof(wchar_t)) )
    {
        return false;
    }
    // TODO: this may not be correct
    (void)enc;
    if (min_char > 0xffff)
    {
        min_char = 0xffff;
    }
    if (max_char > 0xffff)
    {
        max_char = 0xffff;
    }
    for ( size_t i = 0; i < (size / 2); i++ )
    {
        ((wchar_t*)(str->str))[i] = (wchar_t)kpb_random_u2((uint16_t)min_char, (uint16_t)max_char);
    }
    return true;
}

bool kpb_random_bytes_list(kpb_bytes_list_t *list, size_t index, size_t min_size, size_t max_size, uint8_t min_byte, uint8_t max_byte)
{
    size_t size = kpb_random_size(min_size, max_size);
    if ( !kpb_bytes_list_resize_at(list, index, size) )
    {
        return false;
    }
    for ( size_t i = 0; i < size; i++ )
    {
        ((char*)(list->list[index]))[i] = (char)kpb_random_u1(min_byte, max_byte);
    }
    return true;
}

bool kpb_random_char_str_list(kpb_string_list_t *list, size_t index, size_t min_size, size_t max_size, uint32_t min_char, uint32_t max_char, kpb_io_encoding_t enc)
{
    size_t size = kpb_random_size(min_size, max_size);
    if ( !kpb_string_list_resize_at(list, index, size, sizeof(char)) )
    {
        return false;
    }
    // TODO: this may not be correct
    (void)enc;
    if (min_char > 0xff)
    {
        min_char = 0xff;
    }
    if (max_char > 0xff)
    {
        max_char = 0xff;
    }
    for ( size_t i = 0; i < size; i++ )
    {
        ((uint8_t*)(list->list[index]))[i] = kpb_random_u1((uint8_t)min_char, (uint8_t)max_char);
    }
    return true;
}

bool kpb_random_wchar_str_list(kpb_string_list_t *list, size_t index, size_t min_size, size_t max_size, uint32_t min_char, uint32_t max_char, kpb_io_encoding_t enc)
{
    size_t size = kpb_random_size(min_size, max_size);
    if ( !kpb_string_list_resize_at(list, index, size, sizeof(wchar_t)) )
    {
        return false;
    }
    // TODO: this may not be correct
    (void)enc;
    if (min_char > 0xffff)
    {
        min_char = 0xffff;
    }
    if (max_char > 0xffff)
    {
        max_char = 0xffff;
    }
    for ( size_t i = 0; i < (size / 2); i++ )
    {
        ((wchar_t*)(list->list[index]))[i] = (wchar_t)kpb_random_u2((uint16_t)min_char, (uint16_t)max_char);
    }
    return true;
}
