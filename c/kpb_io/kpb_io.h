#ifndef _H_KPB_IO_C_H_
#define _H_KPB_IO_C_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#ifdef __cplusplus
extern "C" {
#endif

void *kpb_io_malloc(size_t size);
void kpb_io_free(void *buffer);
#define kpb_io_memset(a,b,c)        memset(a,b,c)

#define KPB_IO_ISINSTANCE(ptr, struct_name) \
    ((ptr) ? (*((const size_t *)(ptr)) == ((size_t) struct_name##_default )) : false)

typedef uint32_t kpb_io_encoding_t;
#define KPB_IO_ENCODING_ASCII       ((kpb_io_encoding_t)0)
#define KPB_IO_ENCODING_UTF16LE     ((kpb_io_encoding_t)1)
#define KPB_IO_ENCODING_UTF16BE     ((kpb_io_encoding_t)2)
#define KPB_IO_ENCODING_UTF8        ((kpb_io_encoding_t)3)

typedef int16_t kpb_io_utf16_t;
kpb_io_utf16_t kpb_io_utf16_wchar(wchar_t chr, kpb_io_encoding_t enc);
wchar_t kpb_io_wchar_utf16(kpb_io_utf16_t chr, kpb_io_encoding_t enc);
kpb_io_utf16_t *kpb_io_utf16_dupwchar(const wchar_t *str, kpb_io_encoding_t enc);
size_t kpb_io_utf16_len(const kpb_io_utf16_t *str);
const kpb_io_utf16_t *kpb_io_utf16_chr(const kpb_io_utf16_t *str, kpb_io_utf16_t chr);

typedef struct kpb_buffer {
    char *buffer;
    size_t buffer_size;
    size_t buffer_allocated_size;
    bool writeable;
} kpb_buffer_t;

void kpb_buffer_init(kpb_buffer_t *_buffer);
void kpb_buffer_fini(kpb_buffer_t *_buffer);

bool kpb_buffer_writable(const kpb_buffer_t *_buffer);
bool kpb_buffer_readonly(const kpb_buffer_t *_buffer);

bool kpb_buffer_set(kpb_buffer_t *_buffer, const char *data, size_t size);
bool kpb_buffer_set_readonly(kpb_buffer_t *_buffer, const char *data, size_t size);

size_t kpb_buffer_size(const kpb_buffer_t *_buffer);
char *kpb_buffer_get(const kpb_buffer_t *_buffer);

bool kpb_buffer_resize(kpb_buffer_t *_buffer, size_t size);
bool kpb_buffer_read(const kpb_buffer_t *_buffer, size_t pos, void *data, size_t size);
bool kpb_buffer_write(kpb_buffer_t *_buffer, size_t pos, const char *data, size_t size);
bool kpb_buffer_zero(kpb_buffer_t *_buffer, size_t pos, size_t size);

typedef struct kpb_io {
    // owner
    struct kpb_io *stream_owner;
    bool write_mode;
    bool ok;

    // root
    struct kpb_io *stream_root;

    // current stream
    struct kpb_io *stream_current;
    struct kpb_io *stream_next;
    struct kpb_io *stream_prev;
    bool consume_on_pop;

    // current stream data
    struct kpb_io *stream_data;
    char *buffer;
    size_t buffer_size;
    bool buffer_allocated;

    // current stream view
    struct kpb_io *stream_view;
    bool view_size_fixed;
    size_t view_offset;
    size_t view_size;
    size_t pos;
    int bits_left;
    uint64_t bits;

    // fill with 0s or 1s
    bool fill;
    uint8_t fill_byte;
    bool fill_eof;

    // temporary buffer (freed on fini)
    char *tmp_buffer;
    size_t tmp_buffer_size;

    // keep track of last read_bytes call (so size OR buffer can be queried after read)
    size_t last_read_bytes_size;
    bool last_call_read_bytes;
    bool last_call_read_bytes_size;
} kpb_io_t;

void kpb_io_init(kpb_io_t *_io);
void kpb_io_fini(kpb_io_t *_io);

void kpb_io_set_error(kpb_io_t *_io);
bool kpb_io_ok(kpb_io_t *_io);

bool kpb_io_writable(const kpb_io_t *_io);
bool kpb_io_readonly(const kpb_io_t *_io);

void kpb_io_set_readonly(kpb_io_t *_io, const char *data, size_t size);
void kpb_io_set_readonly_or_fill(kpb_io_t *_io, const char *data, size_t size, uint8_t fill);
bool kpb_io_set(kpb_io_t *_io, const char *data, size_t size);
char *kpb_io_get(kpb_io_t *_io, size_t *size);  // caller must free buffer with kpb_io_free()
size_t kpb_io_get_size(kpb_io_t *_io);

void *kpb_io_tmp_buffer(kpb_io_t *_io, size_t size);

kpb_io_t *kpb_io_root(kpb_io_t *_io);
kpb_io_t *kpb_io_parent(kpb_io_t *_io);

void kpb_io_push_substream(kpb_io_t *_io, kpb_io_t *substream, bool set_pos, size_t pos, bool set_size, size_t size);
void kpb_io_pop_substream(kpb_io_t *_io);

size_t kpb_io_size(const kpb_io_t *_io);
size_t kpb_io_pos(const kpb_io_t *_io);
size_t kpb_io_bit_size(const kpb_io_t *_io);
size_t kpb_io_bit_pos(const kpb_io_t *_io);
bool kpb_io_eof(const kpb_io_t *_io);

size_t kpb_io_remaining(kpb_io_t *_io);

void kpb_io_align_to_byte(kpb_io_t *_io);

int8_t kpb_io_read_s1(kpb_io_t *_io);
int16_t kpb_io_read_s2be(kpb_io_t *_io);
int32_t kpb_io_read_s4be(kpb_io_t *_io);
int64_t kpb_io_read_s8be(kpb_io_t *_io);
int16_t kpb_io_read_s2le(kpb_io_t *_io);
int32_t kpb_io_read_s4le(kpb_io_t *_io);
int64_t kpb_io_read_s8le(kpb_io_t *_io);
uint8_t kpb_io_read_u1(kpb_io_t *_io);
uint16_t kpb_io_read_u2be(kpb_io_t *_io);
uint32_t kpb_io_read_u4be(kpb_io_t *_io);
uint64_t kpb_io_read_u8be(kpb_io_t *_io);
uint16_t kpb_io_read_u2le(kpb_io_t *_io);
uint32_t kpb_io_read_u4le(kpb_io_t *_io);
uint64_t kpb_io_read_u8le(kpb_io_t *_io);
float kpb_io_read_f4le(kpb_io_t *_io);
double kpb_io_read_f8le(kpb_io_t *_io);
float kpb_io_read_f4be(kpb_io_t *_io);
double kpb_io_read_f8be(kpb_io_t *_io);
uint64_t kpb_io_read_bits_int_be(kpb_io_t *_io, int n);
uint64_t kpb_io_read_bits_int(kpb_io_t *_io, int n);
uint64_t kpb_io_read_bits_int_le(kpb_io_t *_io, int n);

char* kpb_io_read_char_str(kpb_io_t *_io, size_t size, char term, bool use_size, bool include_term, bool consume, kpb_io_encoding_t src_enc);
wchar_t* kpb_io_read_wchar_str(kpb_io_t *_io, size_t size, wchar_t term, bool use_size, bool include_term, bool consume, kpb_io_encoding_t src_enc);

char *kpb_io_read_bytes(kpb_io_t *_io, size_t size);
size_t kpb_io_read_bytes_size(kpb_io_t *_io, size_t size);

void kpb_io_write_s1(kpb_io_t *_io, int8_t _value);
void kpb_io_write_s2be(kpb_io_t *_io, int16_t _value);
void kpb_io_write_s4be(kpb_io_t *_io, int32_t _value);
void kpb_io_write_s8be(kpb_io_t *_io, int64_t _value);
void kpb_io_write_s2le(kpb_io_t *_io, int16_t _value);
void kpb_io_write_s4le(kpb_io_t *_io, int32_t _value);
void kpb_io_write_s8le(kpb_io_t *_io, int64_t _value);
void kpb_io_write_u1(kpb_io_t *_io, uint8_t _value);
void kpb_io_write_u2be(kpb_io_t *_io, uint16_t _value);
void kpb_io_write_u4be(kpb_io_t *_io, uint32_t _value);
void kpb_io_write_u8be(kpb_io_t *_io, uint64_t _value);
void kpb_io_write_u2le(kpb_io_t *_io, uint16_t _value);
void kpb_io_write_u4le(kpb_io_t *_io, uint32_t _value);
void kpb_io_write_u8le(kpb_io_t *_io, uint64_t _value);
void kpb_io_write_f4le(kpb_io_t *_io, float _value);
void kpb_io_write_f8le(kpb_io_t *_io, double _value);
void kpb_io_write_f4be(kpb_io_t *_io, float _value);
void kpb_io_write_f8be(kpb_io_t *_io, double _value);
void kpb_io_write_bits_int_be(kpb_io_t *_io, int n, uint64_t _bits);
void kpb_io_write_bits_int(kpb_io_t *_io, int n, uint64_t _bits);
void kpb_io_write_bits_int_le(kpb_io_t *_io, int n, uint64_t _bits);

void kpb_io_write_char_str(kpb_io_t *_io, const char *str, size_t size, char term, bool use_size, bool include_term, kpb_io_encoding_t src_enc);
void kpb_io_write_wchar_str(kpb_io_t *_io, const wchar_t *str, size_t size, wchar_t term, bool use_size, bool include_term, kpb_io_encoding_t src_enc);

void kpb_io_write_bytes(kpb_io_t *_io, const char *data, size_t size, size_t write_size);

bool kpb_io_read(kpb_io_t *_io, void *data, size_t size);
bool kpb_io_write(kpb_io_t *_io, const void *data, size_t size);

// --------- LIST ---------

#define KPB_LIST(typename, name) \
    typename *name; \
    size_t _##name##_size; \
    void *_##name##_mem; \
    size_t _##name##_mem_size

typedef struct kpb_list {
    void *list;
    size_t size;
    void *mem;
    size_t mem_size;
} kpb_list_t;

void kpb_list_clear(kpb_list_t *list);
bool kpb_list_add(kpb_list_t *list, void *value, size_t value_size);

#define KPB_LIST_CLEAR(typename, var)   kpb_list_clear((kpb_list_t*)&(var))
#define KPB_LIST_GET(typename, var)     ((var) ? (const typename *)(var) : ((const typename *)""))
#define KPB_LIST_SIZE(typename, var)    (((kpb_list_t*)&(var))->size)
#define KPB_LIST_ADD(typename, var, v)  kpb_list_add(((kpb_list_t*)&(var)), &v, sizeof(typename))

// --------- STRING ---------

#define KPB_STRING(typename, name) \
    typename *name; \
    void *_##name##_mem; \
    size_t _##name##_mem_size

typedef struct kpb_string {
    void *str;
    void *mem;
    size_t mem_size;
} kpb_string_t;

void kpb_string_clear(kpb_string_t *list);
bool kpb_string_set(kpb_string_t *list, const void *value, size_t char_size);
size_t kpb_string_size(const void *str, size_t char_size);
bool kpb_string_resize(kpb_string_t *str, size_t size, size_t char_size);

#define KPB_STRING_CLEAR(typename, var)   kpb_string_clear((kpb_string_t*)&(var))
#define KPB_STRING_GET(typename, var)     ((var) ? (const typename *)(var) : ((const typename *)L""))
#define KPB_STRING_SET(typename, var, v)  kpb_string_set(((kpb_string_t*)&(var)), ((const void*)v), sizeof(typename))
#define KPB_STRING_SIZE(typename, var)    kpb_string_size(((kpb_string_t*)&(var))->str, sizeof(typename))

// --------- STRING LIST ---------

#define KPB_STRING_LIST(typename, name) \
    typename **name; \
    size_t _##name##_size; \
    void **_##name##_mem; \
    void **_##name##_mem_priv; \
    size_t _##name##_mem_size

typedef struct kpb_string_list {
    void **list;
    size_t size;
    void **mem;
    void **mem_priv;
    size_t mem_size;
} kpb_string_list_t;

void kpb_string_list_clear(kpb_string_list_t *list);
bool kpb_string_list_add(kpb_string_list_t *list, const void *value, size_t char_size);
size_t kpb_string_size_at(kpb_string_list_t *list, size_t char_size);
bool kpb_string_list_set_at(kpb_string_list_t *list, size_t index, const void *value, size_t char_size);
bool kpb_string_list_resize_at(kpb_string_list_t *list, size_t index, size_t size, size_t char_size);

#define KPB_STRING_LIST_CLEAR(typename, var)        kpb_string_list_clear((kpb_string_list_t*)&(var))
#define KPB_STRING_LIST_GET(typename, var)          ((var) ? (const typename **)(var) : ((const typename **)L""))
#define KPB_STRING_SIZE_AT(typename, var, index)    (((kpb_string_list_t*)&(var))->size > (index) ? kpb_string_size(((kpb_string_list_t*)&(var))->list[index],sizeof(typename)) : 0)
#define KPB_STRING_LIST_SIZE(typename, var)         (((kpb_string_list_t*)&(var))->size)
#define KPB_STRING_LIST_ADD(typename, var, v)       kpb_string_list_add(((kpb_string_list_t*)&(var)), v, sizeof(typename))

// --------- BYTES ---------

#define KPB_BYTES(typename, name) \
    typename *name; \
    size_t _##name##_size; \
    void *_##name##_mem; \
    size_t _##name##_mem_size

typedef struct kpb_bytes {
    void *data;
    size_t size;
    void *mem;
    size_t mem_size;
} kpb_bytes_t;

void kpb_bytes_clear(kpb_bytes_t *list, const char *ro_default, size_t ro_default_size);
bool kpb_bytes_set(kpb_bytes_t *list, const char *data, size_t size);
bool kpb_bytes_resize(kpb_bytes_t *list, size_t size);

#define KPB_BYTES_CLEAR(typename, var, c_str, size) kpb_bytes_clear((kpb_bytes_t*)&(var),c_str,size)
#define KPB_BYTES_SIZE(typename, var)               ((var) ? ((kpb_bytes_t*)&(var))->size : 0)
#define KPB_BYTES_GET(typename, var)                ((var) ? (const typename *)(var) : ((const typename *)""))
#define KPB_BYTES_SET(typename, var, data, size)    kpb_bytes_set(((kpb_bytes_t*)&(var)), ((const char*)data), size)

// --------- BYTES LIST ---------

#define KPB_BYTES_LIST(typename, name) \
    typename **name; \
    size_t *_##name##_sizeof; \
    size_t _##name##_size; \
    void **_##name##_mem; \
    size_t *_##name##_mem_sizeof; \
    void **_##name##_mem_priv; \
    size_t _##name##_mem_size

typedef struct kpb_bytes_list {
    void **list;
    size_t *list_sizeof;
    size_t size;
    void **mem;
    size_t *mem_sizeof;
    void **mem_priv;
    size_t mem_size;
} kpb_bytes_list_t;

void kpb_bytes_list_clear(kpb_bytes_list_t *list);
bool kpb_bytes_list_add(kpb_bytes_list_t *list, const char *data, size_t size);
bool kpb_bytes_list_set_at(kpb_bytes_list_t *list, size_t index, const char *data, size_t size);
bool kpb_bytes_list_resize_at(kpb_bytes_list_t *list, size_t index, size_t size);

#define KPB_BYTES_LIST_CLEAR(typename, var)             kpb_bytes_list_clear((kpb_bytes_list_t*)&(var))
#define KPB_BYTES_LIST_GET(typename, var)               ((var) ? (const typename **)(var) : ((const typename **)""))
#define KPB_BYTES_SIZE_AT(typename, var, index)         (((kpb_bytes_list_t*)&(var))->size > (index) ? ((kpb_bytes_list_t*)&(var))->list_sizeof[index] : 0)
#define KPB_BYTES_LIST_SIZE(typename, var)              (((kpb_bytes_list_t*)&(var))->size)
#define KPB_BYTES_LIST_ADD(typename, var, data, size)   kpb_bytes_list_add(((kpb_bytes_list_t*)&(var)), ((const char*)data), size)

// --------- OBJECT ---------

#define KPB_OBJECT(typename, name) \
    typename *name; \
    void *_##name##_mem

typedef struct kpb_object {
    void *obj;
    void *mem;
} kpb_object_t;

typedef void (*kpb_object_init_t)(void *obj);
typedef void (*kpb_object_fini_t)(void *obj);
typedef const void* (*kpb_object_default_t)(void);

void kpb_object_clear(kpb_object_t *obj, kpb_object_fini_t fini, kpb_object_default_t default_);
void *kpb_object_lazy_constructor(kpb_object_t *obj, size_t obj_size, kpb_object_fini_t init);

#define KPB_OBJECT_CLEAR(typename, var, fini, default_)      kpb_object_clear((kpb_object_t*)&(var), (kpb_object_fini_t)(fini), (kpb_object_default_t)(default_))
#define KPB_OBJECT_LAZY_CONSTRUCTOR(typename, var, init)     ((typename*)kpb_object_lazy_constructor((kpb_object_t*)&(var), sizeof(typename), (kpb_object_init_t)(init)))
#define KPB_OBJECT_GET(typename, var, default_)              ((var) ? (var) : default_())

// --------- OBJECT LIST ---------

#define KPB_OBJECT_LIST(typename, name) \
    typename **name; \
    size_t _##name##_size; \
    void **_##name##_mem; \
    void **_##name##_mem_priv; \
    size_t _##name##_mem_size

typedef struct kpb_object_list {
    void **list;
    size_t size;
    void **mem;
    void **mem_priv;
    size_t mem_size;
} kpb_object_list_t;

void kpb_object_list_clear(kpb_object_list_t *list, size_t obj_size, kpb_object_fini_t fini);
void *kpb_object_list_add(kpb_object_list_t *list, size_t obj_size, kpb_object_fini_t init);

#define KPB_OBJECT_LIST_CLEAR(typename, var, fini)  kpb_object_list_clear((kpb_object_list_t*)&(var), sizeof(typename), (kpb_object_fini_t)(fini))
#define KPB_OBJECT_LIST_GET(typename, var)          ((var) ? (const typename **)(var) : ((const typename **)""))
#define KPB_OBJECT_LIST_SIZE(typename, var)         (((kpb_object_list_t*)&(var))->size)
#define KPB_OBJECT_LIST_ADD(typename, var, init)    ((typename*)(kpb_object_list_add(((kpb_object_list_t*)&(var)), sizeof(typename), (kpb_object_init_t)(init))))

// --------- RANDOMIZE ---------

typedef bool(*random_bytes_fn_t)(void *ctx, char *data, size_t size);
void kpb_random_api(random_bytes_fn_t func, void *ctx);  // NOT THREAD SAFE

int8_t kpb_random_s1(int8_t min_inclusive, int8_t max_inclusive);
int16_t kpb_random_s2(int16_t min_inclusive, int16_t max_inclusive);
int32_t kpb_random_s4(int32_t min_inclusive, int32_t max_inclusive);
int64_t kpb_random_s8(int64_t min_inclusive, int64_t max_inclusive);
uint8_t kpb_random_u1(uint8_t min_inclusive, uint8_t max_inclusive);
uint16_t kpb_random_u2(uint16_t min_inclusive, uint16_t max_inclusive);
uint32_t kpb_random_u4(uint32_t min_inclusive, uint32_t max_inclusive);
uint64_t kpb_random_u8(uint64_t min_inclusive, uint64_t max_inclusive);
float kpb_random_f4(float min_inclusive, float max_inclusive);
double kpb_random_f8(double min_inclusive, double max_inclusive);
bool kpb_random_bool(bool min_inclusive, bool max_inclusive);
size_t kpb_random_size(size_t min_inclusive, size_t max_inclusive);
bool kpb_random_bytes(kpb_bytes_t *bytes, size_t min_size, size_t max_size, uint8_t min_byte, uint8_t max_byte);
bool kpb_random_char_str(kpb_string_t *str, size_t min_size, size_t max_size, uint32_t min_char, uint32_t max_char, kpb_io_encoding_t enc);
bool kpb_random_wchar_str(kpb_string_t *str, size_t min_size, size_t max_size, uint32_t min_char, uint32_t max_char, kpb_io_encoding_t enc);
bool kpb_random_bytes_list(kpb_bytes_list_t *list, size_t index, size_t min_size, size_t max_size, uint8_t min_byte, uint8_t max_byte);
bool kpb_random_char_str_list(kpb_string_list_t *list, size_t index, size_t min_size, size_t max_size, uint32_t min_char, uint32_t max_char, kpb_io_encoding_t enc);
bool kpb_random_wchar_str_list(kpb_string_list_t *list, size_t index, size_t min_size, size_t max_size, uint32_t min_char, uint32_t max_char, kpb_io_encoding_t enc);

#define KPB_RANDOM_BYTES(var, min_size, max_size, min_byte, max_byte) \
    kpb_random_bytes(((kpb_bytes_t*)&(var)), min_size, max_size, min_byte, max_byte)
#define KPB_RANDOM_CHAR_STR(var, min_size, max_size, min_char, max_char, enc) \
    kpb_random_char_str(((kpb_string_t*)&(var)), min_size, max_size, min_char, max_char, enc)
#define KPB_RANDOM_WCHAR_STR(var, min_size, max_size, min_char, max_char, enc) \
    kpb_random_wchar_str(((kpb_string_t*)&(var)), min_size, max_size, min_char, max_char, enc)
#define KPB_RANDOM_BYTES_LIST(var, index, min_size, max_size, min_byte, max_byte) \
    kpb_random_bytes_list(((kpb_bytes_list_t*)&(var)), index, min_size, max_size, min_byte, max_byte)
#define KPB_RANDOM_CHAR_STR_LIST(var, index, min_size, max_size, min_char, max_char, enc) \
    kpb_random_char_str_list(((kpb_string_list_t*)&(var)), index, min_size, max_size, min_char, max_char, enc)
#define KPB_RANDOM_WCHAR_STR_LIST(var, index, min_size, max_size, min_char, max_char, enc) \
    kpb_random_wchar_str_list(((kpb_string_list_t*)&(var)), index, min_size, max_size, min_char, max_char, enc)

#ifdef __cplusplus
};
#endif

#endif  // _H_KPB_IO_C_H_
