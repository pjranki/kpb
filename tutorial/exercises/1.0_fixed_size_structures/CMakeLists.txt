add_executable(animal_record_c
    main.c
)
target_kpb_c(animal_record_c
    animal_record.ksy  # generates animal_record.c, animal_record.h
)
target_link_libraries(animal_record_c
    kpb_io  # C runtime for KPB i.e. common C code for KPB
)

add_executable(animal_record_cpp
    main.cpp
)
target_kpb_cpp(animal_record_cpp
    animal_record.ksy  # generates animal_record.cpp, animal_record.hpp
)
target_link_libraries(animal_record_cpp
    kpb_cpp_stl_io  # C++ STL runtime for KPB i.e. common C++ code for KPB
)
