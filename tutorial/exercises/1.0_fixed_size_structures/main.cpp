#include <iostream>
#include <string.h>

#include "animal_record.hpp"

#define TEST_UUID "\x11\x22\x33\x44\x55\x66\x77\x88\x99\x00\xaa\xbb\xcc\xdd\xee\xff"
#define TEST_UUID_SIZE  16

int main(int argc, char *argv[])
{
    AnimalRecord record;
    AnimalRecord record_copy;

    // fill in record
    /* TODO: set uuid to TEST_UUID */
    /* TODO: set name to "Fido" */
    /* TODO: set birth year 2020 */
    /* TODO: set weight 9.1 */
    /* TODO: set rating to -5 */

    // serailize one record, parse into other record (aka a deep copy)
    std::string buffer;
    record.SerializeToString(&buffer);
    record_copy.ParseFromString(buffer);

    // print the copy
    if ( 0 == memcmp(TEST_UUID, "" /* TODO: get uuid buffer */, 0 /* TODO: get uuid size */))
    {
        std::cout << "UUID  : match" << std::endl;
    }
    else
    {
        std::cout << "UUID  : MISMATCH" << std::endl;
    }
    std::cout << "NAME  : " << "" /* TODO: get name */ << std::endl;
    std::cout << "YEAR  : " << "" /* TODO: get year */ << std::endl;
    std::cout << "WEIGHT: " << "" /* TODO: get weight */ << std::endl;
    std::cout << "RATING: " << "" /* TODO: get rating */ << std::endl;

    return 0;
}
