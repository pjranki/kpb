#include <stdio.h>
#include <string.h>

#include "animal_record.h"

#define TEST_UUID "\x11\x22\x33\x44\x55\x66\x77\x88\x99\x00\xaa\xbb\xcc\xdd\xee\xff"
#define TEST_UUID_SIZE  16

int main(int argc, char *argv[])
{
    animal_record_t record;
    animal_record_t record_copy;

    // must always init in C
    animal_record_init(&record);
    animal_record_init(&record_copy);

    // fill in record
    /* TODO: set uuid to TEST_UUID */
    /* TODO: set name to "Fido" */
    /* TODO: set birth year 2020 */
    /* TODO: set weight 9.1 */
    /* TODO: set rating to -5 */

    // serailize one record, parse into other record (aka a deep copy)
    size_t buffer_size = 0;
    char *buffer = animal_record_serialize_to_string(&record, &buffer_size);
    animal_record_parse_from_string(&record_copy, buffer, buffer_size);
    kpb_io_free(buffer);

    // print the copy
    if ( 0 == memcmp(TEST_UUID, "" /* TODO: get uuid buffer */, 0 /* TODO: get uuid size */))
    {
        printf("UUID  : match\n");
    }
    else
    {
        printf("UUID  : MISMATCH\n");
    }
    printf("NAME  : %s\n", "" /* TODO: get name */);
    printf("YEAR  : %d\n", 0 /* TODO: get birth year */);
    printf("WEIGHT: %2.2f\n", 0.0 /* TODO: weight */);
    printf("RATING: %d\n", 0 /* TODO: get rating */);

    // must always cleanup your messages in C
    animal_record_fini(&record);
    animal_record_fini(&record_copy);
    return 0;
}
