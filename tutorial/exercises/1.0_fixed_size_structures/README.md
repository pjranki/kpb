# Fixed-size structures

## Kaitai Struct User Guide

Section 4.1 "Fixed-size structures"

## A simple C struct in KPB

An example of a fixed-sized C struct:

```c
struct _PERSON {
    char uuid[16];
    char name[50];
    wchar_t name[25];  // UTF-16LE
    uint8_t age;
};
```

To write the same in a ```ksy``` file, it would look like this:

```
meta:
  id: person
  endian: le
seq:
  - id: uuid
    size: 16
  - id: name
    type: str
    size: 50
    encoding: ascii
  - id: surname
    type: str
    size: 50
    encoding: utf-16le
  - id: age
    type: u1
```

The ```meta``` section is for top-level information. Here we have named the top-level structure defined as ```person``` using ```id: person```. When dealing with serializing numbers we need to set the default endian using ```endian: le``` for little endian. We could also have used ```be``` for big endian.

We can create/destroy this "structure" in C using the following:
```c
#include "person.h"

int main()
{
    person_t msg;
    person_init(&msg);  // must be called, think of it as memset

    // do stuff

    person_fini(&msg);  // must be called, frees internal allocations.
    return 0;
}
```

For C++, it a lot easier
```cpp
#include "person.hpp"

int main()
{
    Person msg;  // constructor/destructor do the heavy lifting

    // do stuff

    return 0;
}
```

The ```seq``` section is an ordered list of what will be parsed/serailized to/from binary.

To add a fixed-sized buffer of 16 bytes, we used ```size: 16 ```.

Strings of different encodings can be used by setting ```type: str``` and an encoding ```encoding: ascii```. This will generate source code that will use a ```char``` for each character. To use ```wchar_t``` wide characters, we use ```encoding: utf-16le```. In both cases, ```size``` is the number of bytes parsed/serailized to/from the binary data - this means that the above example can have a maximum surname of 25 characters.

For simple types like numbers and floats, only a ```type`` is needed. The following are different types you can use:
 - (no type) - a binary buffer with bytes and a length.
 - ```s1``` - e.g. ```int8_t```
 - ```s2``` - e.g. ```int16_t```
 - ```s4``` - e.g. ```int32_t```
 - ```s8``` - e.g. ```int64_t```
 - ```u1``` - e.g. ```uint8_t```
 - ```u2``` - e.g. ```uint16_t```
 - ```u4``` - e.g. ```uint32_t```
 - ```u8``` - e.g. ```uint64_t```
 - ```f4``` - e.g. ```float```
 - ```f8``` - e.g. ```double```
 - ```str``` - a string with an ```encoding``` that will be parsed/serialized with the value of ```size```.
 - ```strz``` - a string with an ```encoding``` that will be parsed/serialized with a nul-terminator.

Here are the setters & getters for the structure in C:
```c
// setters
person_set_uuid(&msg, "\x11\x22\x33\x44\x55\x66\x77\x88\x99\x00\xaa\xbb\xcc\xdd\xee\xff", 16);
person_set_name(&msg, "Joe");
person_set_surname(&msg, L"Smith");  // wide characeters here
person_set_age(&msg, 42);

// getters
const char *uuid = person_uuid(&msg);
size_t uuid_size = person_uuid_size(&msg);
const char *name = person_name(&msg);
const wchar_t *surname = person_surname(&msg);
uint8_t age = person_age(&msg);
```

Here are the setters & getters for the structure in C++:
```cpp
// setters
msg.set_uuid(std::string("\x11\x22\x33\x44\x55\x66\x77\x88\x99\x00\xaa\xbb\xcc\xdd\xee\xff", 16));
msg.set_name("Joe");
msg.set_surname(L"Smith");  // wide characeters here
msg.set_age(42);

// getters
const char *uuid = msg.uuid().c_str();
size_t uuid_size = msg.uuid().size();
const char *name = msg.name().c_str();
const wchar_t *surname = msg.surname().c_str();
uint8_t age = msg.age();
```

## Exercise 1 - animal record

Using the templates ```animal_record.ksy```, ```main.c``` and ```main.cpp```, implement the following example from the Kaitai Struct User Guide:

```c
struct {
    char uuid[16];       /* 128-bit UUID */
    char name[24];       /* Name of the animal */
    uint16_t birth_year; /* Year of birth, used to calculate the age */
    double weight;       /* Current weight in kg */
    int32_t rating;      /* Rating, can be negative */
} animal_record;
```

Look for the ```TODO```s in the files.
