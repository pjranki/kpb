#include <stdio.h>

#include "track.h"

int main(int argc, char *argv[])
{
    track_t track;
    track_t track_copy;

    //str_with_len_t *str = NULL;

    // must always init in C
    track_init(&track);
    track_init(&track_copy);

    // fill in track
    /* TODO: set the following on the track
        - TRACK:  "Star Treatment"
        - ALBUM:  "Tranquility Base Hotel & Casino"
        - ARTIST: "Arctic Monkeys"
    */

    // serailize one track, parse into other track (aka a deep copy)
    size_t buffer_size = 0;
    char *buffer = track_serialize_to_string(&track, &buffer_size);
    track_parse_from_string(&track_copy, buffer, buffer_size);
    kpb_io_free(buffer);

    // print the copy
    printf("TRACK  : %s\n", "" /* get the track title */);
    printf("ALBUM  : %s\n", "" /* get the album title */);
    printf("ARTIST : %s\n", "" /* get the artist name */);

    // must always cleanup your messages in C
    track_fini(&track);
    track_fini(&track_copy);
    return 0;
}
