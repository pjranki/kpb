#include <iostream>

#include "track.hpp"

int main(int argc, char *argv[])
{
    Track track;
    Track track_copy;

    // fill in track
    /* TODO: set the following on the track
        - TRACK:  "Star Treatment"
        - ALBUM:  "Tranquility Base Hotel & Casino"
        - ARTIST: "Arctic Monkeys"
    */

    // serailize one track, parse into other track (aka a deep copy)
    std::string buffer;
    track.SerializeToString(&buffer);
    track_copy.ParseFromString(buffer);

    // print the copy
    std::cout << "TRACK  : " << "" /* get the track title */ << std::endl;
    std::cout << "ALBUM  : " << "" /* get the album title */ << std::endl;
    std::cout << "ARTIST : " << "" /* get the artist name */ << std::endl;

    return 0;
}
