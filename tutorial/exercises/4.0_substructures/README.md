# Substructures

## Kaitai Struct User Guide

Section 4.7 "Substructures (subtypes)"

## Adding substructures

To add addition structures (substructures) to a ```ksy``` file, use a ```types``` block where each key is the name of the substructure.

Example:
```
seq:
  - id: myline
    type: line
  - id: mycircle
    type: circle

types:
  line:
    seq:
      - id: x0
        type: s2
      - id: y0
        type: s2
      - id: x1
        type: s2
      - id: y1
        type: s2
  circle:
    seq:
      - id: x0
        type: s2
      - id: y0
        type: s2
      - id: radius
        type: u2
```

A substructure can also have additional blocks like ```-update``` and ```enums```:
```
meta:
  id: example
  endian: le

seq:
  - id: value
    type: tag

types:
  tag:
    seq:
      - id: tag_type
        type: u1
        enum: tag
      - id: tag
        type: strz
        encoding: ascii
      - id: data_len
        type: u4
      - id: data
        size: data_len
    -update:
      - id: data_len
        value: data.length
    enums:
      tag:
        0: type_a
        1: type_b
        2: type_c
```

Here is an example of how to modify a substructure in C:
```c
example_t example;
example_init(&example);

// 'mutable' will allocate the substructure and return it (NULL if it fails).
// The allocation is stored inside 'example', and 'example_fini' will free it.
// hence you do not need to 'free' substructures accessed by 'mutable' calls.
tag_t *tag = example_mutable_value(&example);

// Then you can treat the substructure like any other structure.
tag_set_tag_type(tag, TAG_TAG_TYPE_B);
tag_set_tag(tag, "Key");
tag_set_data(tag, "\x41\x41\x41", 3);

// this will free the memory allocated to 'tag' as well
example_fini(&example);
```

Here is an example of how to modify a substructure in C++:
```cpp
Example example;

// 'mutable' will allocate the substructure and return it (nullptr if it fails).
// The allocation is stored inside 'example', and 'Example's destructor will free it.
// hence you do not need to 'free' substructures accessed by 'mutable' calls.
Tag *tag = example.mutable_value();

// Then you can treat the substructure like any other structure.
tag->set_tag_type(Tag::Tag::TYPE_B);
tag->set_tag("Key");
tag->set_data(std::string("\x41\x41\x41", 3));

// 'Example's destructor frees the 'Tag' substructure.
```

## Exercise 1 - Track

Implement a substructure called ```str_with_len``` inside ```track.ksy```.

The substructure should contain:
 - ```len```: a ```uint32_t``` which is the length (in characters) of an ASCII string.
 - ```value```: an ASCII string whose length comes from ```len```.

Then complete the ```track``` structure by adding the ```str_with_len``` type to the attributes in ```seq```.

Complete ```track.ksy```, ```main.c``` and ```main.cpp``` by filling in the ```TODO```s.

HINT: You will also need an ```-update``` block inside your substructure so that ```len``` is updated when the ```value``` is changed.
