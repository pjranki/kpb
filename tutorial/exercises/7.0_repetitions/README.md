# Repetitions

## Kaitai Struct User Guide

Section 4.10 "Repetitions"

## Repeating attributes (List of attributes)

Numbers, strings, buffers and substructures can be repeated by just adding a ```repeat``` to the attribute in the ```ksy``` file.

To repeat an attribute until the end of the stream:
```
seq:
  - id: number
    type: u2
    repeat: eos  # NOTE: this must be the last attribute in the structure.
```

We can also use previous attributes to store the number of repetitions:
```
seq:
  - id: count  # we will use this as the count of 'number's
    type: u4
  - id: number
    type: u2
    repeat: expr  # the number of repeats is an 'expression' - covered in later topics
    repeat-expr: count  # repeat 'count' number of times
```

And to complete the above example, we need to update ```count``` when ```number``` changes:
```
seq:
  - id: count
    type: u4
  - id: number
    type: u2
    repeat: expr
    repeat-expr: count
-update:
  - id: count
    value: number.size  # for repeated values, '.size' is the count
```

## ADD/ITERATE numbers

Take the following example:
```
seq:
  - id: number
    type: u2
    repeat: eos
```

To add/iterate in C:
```c
example_add_number(&example, 1);
example_add_number(&example, 2);
example_add_number(&example, 3);
for ( size_t i = 0; i < example_number_size(&example); i++ )  // _size prefix means the count of items in list
{
    uint16_t number = example_number_at(&example, i);  // _at prefix gets the item at that index
}
```

To add/iterate in C++:
```cpp
example.add_number(1);
example.add_number(2);
example.add_number(3);
size_t count = example.number_size();
for ( auto n : example.number() )
{
    uint16_t number = n;
}
```

## Exercise 1 - List of numbers

Complete ```number_list.ksy```, ```main.c``` and ```main.cpp``` by filling in the ```TODO```s.


## ADD/ITERATE strings

Take the following example:
```
seq:
  - id: string
    type: strz
    encoding: ASCII
    repeat: eos
```

To add/iterate in C:
```c
example_add_string(&example, "A");
example_add_string(&example, "B");
example_add_string(&example, "C");
for ( size_t i = 0; i < example_string_size(&example); i++ )  // _size prefix means the count of items in list
{
    const char* s = example_string_at(&example, i);  // _at prefix gets the item at that index
}
```

To add/iterate in C++:
```cpp
example.add_string("A");
example.add_string("B");
example.add_string("C");
size_t count = example.string_size();
for ( auto s : example.string() )
{
    const char *text = s.c_str();
}
```

## Exercise 2 - List of strings

Complete ```string_list.ksy```, ```main.c``` and ```main.cpp``` by filling in the ```TODO```s.

## ADD/ITERATE buffer

Take the following example:
```
seq:
  - id: buffer
    size: 5
    repeat: eos
```

To add/iterate in C:
```c
example_add_buffer(&example, "\xcc", 1);
example_add_buffer(&example, "\x11\x22", 2);
example_add_buffer(&example, "\x12\x34\x56\x78", 4);
for ( size_t i = 0; i < example_buffer_size(&example); i++ )  // _size prefix means the count of items in list
{
    const char* s = example_buffer_at(&example, i);  // _at prefix gets the item at that index
}
```

To add/iterate in C++:
```cpp
example.add_buffer(std::string("\xcc", 1));
example.add_buffer(std::string("\x11\x22", 2));
example.add_buffer(std::string("\x12\x34\x56\x78", 4));
size_t count = example.buffer_size();
for ( auto b : example.buffer() )
{
    size_t data_size = b.size();
    const char *data = b.c_str();
}
```

## Exercise 3 - List of strings

Complete ```buffer_list.ksy```, ```main.c``` and ```main.cpp``` by filling in the ```TODO```s.

## ADD/ITERATE substructures

Take the following example:
```
seq:
  - id: buffer
    type: buffer
    repeat: eos
types:
  buffer:
    seq:
      - id: count
        type: u4
      - id: data
        size: count
    -update:
      - id: count
        value: data.size
```

To add/iterate in C:
```c
buffer_t *buffer = NULL;
buffer = example_add_buffer(&example);  // don't need to free this allocation, managed by 'example' object.
buffer_set_data(buffer, "\xcc", 1);
buffer = example_add_buffer(&example);  // don't need to free this allocation, managed by 'example' object.
buffer_set_data(buffer, "\x11\x22", 2);
buffer = example_add_buffer(&example);  // don't need to free this allocation, managed by 'example' object.
buffer_set_data(buffer, "\x12\x34\x56\x78", 4);
for ( size_t i = 0; i < example_buffer_size(&example); i++ )  // _size prefix means the count of items in list
{
    const buffer_t *const_buffer = example_buffer_at(&example, i);  // _at prefix gets the item at that index
    size_t data_size = buffer_data_size(const_buffer);
    const char *data = buffer_data(const_buffer);
}
```

To add/iterate in C++:
```cpp
Buffer *buffer = nullptr;
buffer = example.add_buffer();
buffer->set_data(std::string("\xcc", 1));  // don't need to free this allocation, managed by 'example' object.
buffer = example.add_buffer();
buffer->set_data(std::string("\x11\x22", 2));  // don't need to free this allocation, managed by 'example' object.
buffer = example.add_buffer();
buffer->set_data(std::string("\x12\x34\x56\x78", 4));  // don't need to free this allocation, managed by 'example' object.
size_t count = example.buffer_size();
for ( auto b : example.buffer() )
{
    size_t data_size = b.get().data().size();  // need to use '.get' as b is a C++ reference object.
    const char *data = b.get().data().c_str(); // need to use '.get' as b is a C++ reference object.
}
```

## Exercise 4 - Phone book

Complete ```phone_book.ksy```, ```main.c``` and ```main.cpp``` by filling in the ```TODO```s.
