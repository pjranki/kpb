meta:
  id: phone_book
  endian: be

doc: |
  EXERCISE 4

seq:
  - id: count
    type: u4
  - id: entry
    type: entry
    # TODO: make this repeat 'count' number of times

#-update:
# TODO: update count when a new entry is added

types:
  entry:
    seq:
      - id: name
        type: strz
        encoding: ASCII
      - id: number
        type: strz
        encoding: ASCII
