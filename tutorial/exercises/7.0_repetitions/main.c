#include <stdio.h>

#include "number_list.h"
#include "string_list.h"
#include "buffer_list.h"
#include "phone_book.h"

int main(int argc, char *argv[])
{
    size_t buffer_size = 0;
    const char *buffer = NULL;
    size_t i = 0;
    size_t j = 0;

    /************** EXERCISE 1 *******************/

    number_list_t number_list, number_list_copy;

    number_list_init(&number_list);
    number_list_init(&number_list_copy);

    // populate with numbers
/* TODO: add 3 numbers:
    - 10
    - 20
    - 30
*/

    // serailize one number_list, parse into other number_list (aka a deep copy)
    buffer_size = 0;
    buffer = number_list_serialize_to_string(&number_list, &buffer_size);
    number_list_parse_from_string(&number_list_copy, buffer, buffer_size);
    kpb_io_free((void*)buffer);

    // print numbers
/* TODO: loop and print all numbers
    for number in number_list_copy
        print number
*/

    number_list_fini(&number_list);
    number_list_fini(&number_list_copy);

    printf("\n");

    /************** EXERCISE 2 *******************/

    string_list_t string_list, string_list_copy;

    string_list_init(&string_list);
    string_list_init(&string_list_copy);

    // populate with strings
/* TODO: add 3 strings:
    - "ABC"
    - "DEFG"
    - "HIJKL"
*/

    // serailize one string_list, parse into other string_list (aka a deep copy)
    buffer_size = 0;
    buffer = string_list_serialize_to_string(&string_list, &buffer_size);
    string_list_parse_from_string(&string_list_copy, buffer, buffer_size);
    kpb_io_free((void*)buffer);

    // print strings
/* TODO: loop and print all strings
    for s in string_list_copy
        print s
*/

    string_list_fini(&string_list);
    string_list_fini(&string_list_copy);

    printf("\n");

    /************** EXERCISE 3 *******************/

    buffer_list_t buffer_list, buffer_list_copy;

    buffer_list_init(&buffer_list);
    buffer_list_init(&buffer_list_copy);

    // populate with numbers
/* TODO: add 3 buffers:
    - "\xcc\xdd\xee", 3
    - "\x01\x02\x03\x04\x05", 5
    - "\x90\x80\x70\x60\x50\x40\x30", 7
*/

    // serailize one buffer_list, parse into other buffer_list (aka a deep copy)
    buffer_size = 0;
    buffer = buffer_list_serialize_to_string(&buffer_list, &buffer_size);
    buffer_list_parse_from_string(&buffer_list_copy, buffer, buffer_size);
    kpb_io_free((void*)buffer);

    // print numbers
/* TODO: loop and print all buffers
    for b in buffer_list_copy
        print b
*/

    buffer_list_fini(&buffer_list);
    buffer_list_fini(&buffer_list_copy);

    printf("\n");

    /************** EXERCISE 4 *******************/

    phone_book_t phone_book, phone_book_copy;
    entry_t *entry = NULL;

    // must always init in C
    phone_book_init(&phone_book);
    phone_book_init(&phone_book_copy);

    // populate phone book
/* TODO: add 3 entries:
    - Tom   0411111111
    - Dick  0422222222
    - Harry 0433333333
*/

    // serailize one phone_book, parse into other phone_book (aka a deep copy)
    buffer_size = 0;
    buffer = phone_book_serialize_to_string(&phone_book, &buffer_size);
    phone_book_parse_from_string(&phone_book_copy, buffer, buffer_size);
    kpb_io_free((void*)buffer);

    // print phone book copy
    printf("Entries: %d\n", phone_book_count(&phone_book_copy));
/* TODO: loop and print all entries
    for entry in phone_book
        print entry name
        print entry number
*/

    // must always cleanup your messages in C
    phone_book_fini(&phone_book);
    phone_book_fini(&phone_book_copy);
    return 0;
}
