#include <iostream>

#include "number_list.hpp"
#include "string_list.hpp"
#include "buffer_list.hpp"
#include "phone_book.hpp"

int main(int argc, char *argv[])
{
    std::string buffer;

    /************** EXERCISE 1 *******************/

    NumberList number_list, number_list_copy;

    // populate with numbers
/* TODO: add 3 numbers:
    - 10
    - 20
    - 30
*/

    // serailize one number_list, parse into other number_list (aka a deep copy)
    number_list.SerializeToString(&buffer);
    number_list_copy.ParseFromString(buffer);

    // print numbers
/* TODO: loop and print all numbers
    for number in number_list_copy
        print number
*/

    std::cout << std::endl;

    /************** EXERCISE 2 *******************/

    StringList string_list, string_list_copy;

    // populate with strings
/* TODO: add 3 strings:
    - "ABC"
    - "DEFG"
    - "HIJKL"
*/

    // serailize one string_list, parse into other string_list (aka a deep copy)
    string_list.SerializeToString(&buffer);
    string_list_copy.ParseFromString(buffer);

    // print strings
/* TODO: loop and print all strings
    for s in string_list_copy
        print s
*/

    std::cout << std::endl;

    /************** EXERCISE 3 *******************/

    BufferList buffer_list, buffer_list_copy;

    // populate with buffers
/* TODO: add 3 buffers:
    - "\xcc\xdd\xee", 3
    - "\x01\x02\x03\x04\x05", 5
    - "\x90\x80\x70\x60\x50\x40\x30", 7
*/

    // serailize one buffer_list, parse into other buffer_list (aka a deep copy)
    buffer_list.SerializeToString(&buffer);
    buffer_list_copy.ParseFromString(buffer);

    // print buffers
/* TODO: loop and print all buffers
    for b in buffer_list_copy
        print b
*/

    std::cout << std::endl;

    /************** EXERCISE 4 *******************/

    PhoneBook phone_book, phone_book_copy;
    Entry *entry = nullptr;

    // populate phone book
/* TODO: add 3 entries:
    - Tom   0411111111
    - Dick  0422222222
    - Harry 0433333333
*/

    // serailize one metadata, parse into other metadata (aka a deep copy)
    phone_book.SerializeToString(&buffer);
    phone_book_copy.ParseFromString(buffer);

    // print phone book copy
    std::cout << "Entries: " << static_cast<int>(phone_book_copy.count()) << std::endl;
/* TODO: loop and print all entries
    for entry in phone_book_copy
        print entry name
        print entry number
*/

    return 0;
}
