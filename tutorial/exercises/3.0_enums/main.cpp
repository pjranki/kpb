#include <iostream>

#include "protocol.hpp"

int main(int argc, char *argv[])
{
    Protocol msg;

    // lets try each enumerator
    for ( uint8_t e = 0; e < 20; e++ )
    {
        // set the protocol
        msg.set_protocol(e);

        // switch-on enumerator
        switch(msg.protocol())
        {
            case 1 /* TODO: use a generated enumerator here */:
                std::cout << static_cast<int>(msg.protocol()) << ": ICMP" << std::endl;
                break;
            case 17 /* TODO: use a generated enumerator here */:
                std::cout << static_cast<int>(msg.protocol()) << ": UDP" << std::endl;
                break;
            case 6 /* TODO: use a generated enumerator here */:
                std::cout << static_cast<int>(msg.protocol()) << ": TCP" << std::endl;
                break;
            default:
                std::cout << static_cast<int>(msg.protocol()) << ": (unknown)" << std::endl;
                break;
        }
    }

    return 0;
}
