# Enumerators

## Kaitai Struct User Guide

Section 4.6 "Enums (named integer constants)"

## Defining enumerators

Enumerators can be added by using an ```enum``` block. Each key here is the name of an enumerator and each enumerator contains multiple value/name pairs.

As an example, here is a simple enumerator defined in a ```ksy``` file:
```
meta:
  id: message
  endian: le

enums:
  os:
    1: windows
    2: linux
  arch:
    0: unknown
    0x86: x86
    0x64: amd64
```

Enumerators in C will be generated as ```#defined``` values:
```c
#define MESSAGE_OS_WINDOWS
#define MESSAGE_OS_LINUX
#define MESSAGE_ARCH_UNKNOWN
#define MESSAGE_ARCH_X86
#define MESSAGE_ARCH_AMD64
```

Enumerators in C++ will be scoped to the class:
```cpp
Message::Os::OS_WINDOWS
Message::Os::OS_LINUX
Message::Arch::ARCH_UNKNOWN
Message::Arch::ARCH_X86
Message::Arch::ARCH_AMD64
```

If a ```NAMESPACE``` is provided on the ```kpb``` command line or inside the ```CMakeLists.txt```, the enumerators will also be scoped.

A namespace of ```foobar``` will generate the following C enumerators:
```c
#define FOOBAR_MESSAGE_OS_WINDOWS
#define FOOBAR_MESSAGE_OS_LINUX
#define FOOBAR_MESSAGE_ARCH_UNKNOWN
#define FOOBAR_MESSAGE_ARCH_X86
#define FOOBAR_MESSAGE_ARCH_AMD64
```

And for C++, the following enumerators:
```cpp
foobar::Message::Os::OS_WINDOWS
foobar::Message::Os::OS_LINUX
foobar::Message::Arch::ARCH_UNKNOWN
foobar::Message::Arch::ARCH_X86
foobar::Message::Arch::ARCH_AMD64
```

Attributes can be set to enumerators too (this helps with KPB ```switch-on``` statements, more on this later).

To update our ```message``` example from above:
```
meta:
  id: message
  endian: le

seq:
  - id: os
    type: u1
    enum: os  # 'os' enumerator
  - id: architecture
    type: u4
    enum: arch  # 'arch' enumerator

enums:
  os:
    1: windows
    2: linux
  arch:
    0: unknown
    0x86: x86
    0x64: amd64
```

Notice how the the type/size of the attribute (e.g. ```u1``` and ```u4```) is separate to the enumerator? You have full control of "how" an enumerator will be parsed/serialized to/from a binary message.

## Exercise 1 - IP packet enumerator

Complete ```protocol.ksy```, ```main.c``` and ```main.cpp``` by filling in the ```TODO```s.
