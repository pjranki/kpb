#include <stdio.h>

#include "protocol.h"

int main(int argc, char *argv[])
{
    protocol_t msg;

    // must always init in C
    protocol_init(&msg);

    // lets try each enumerator
    for ( uint8_t e = 0; e < 20; e++ )
    {
        // set the protocol
        protocol_set_protocol(&msg, e);

        // switch-on enumerator
        switch(protocol_protocol(&msg))
        {
            case 1 /* TODO: use a generated enumerator here */:
                printf("%d: ICMP\n", protocol_protocol(&msg));
                break;
            case 17 /* TODO: use a generated enumerator here */:
                printf("%d: UDP\n", protocol_protocol(&msg));
                break;
            case 6 /* TODO: use a generated enumerator here */:
                printf("%d: TCP\n", protocol_protocol(&msg));
                break;
            default:
                printf("%d: (unknown)\n", protocol_protocol(&msg));
                break;
        }
    }

    // must always cleanup your messages in C
    protocol_fini(&msg);
    return 0;
}
