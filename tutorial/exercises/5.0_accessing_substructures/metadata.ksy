meta:
  id: metadata
  endian: le

seq:
  - id: max_name_len
    type: u4
  - id: child
    type: child
  - id: name
    size: 0 # TODO: use 'max_name_len'
    type: str
    encoding: ASCII

types:
  child:
    seq:
      - id: grand_child
        type: grand_child
      - id: name
        size: 0 # TODO: use 'max_name_len' from the parent
        type: str
        encoding: ASCII
  grand_child:
    seq:
      - id: name
        size: 0 # TODO: use 'max_name_len' from the root
        type: str
        encoding: ASCII
