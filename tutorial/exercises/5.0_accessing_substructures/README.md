# Substructures

## Kaitai Struct User Guide

Section 4.8 "Accessing attributes in other types"

## Access attributes in self

Accessing attributes in self can be done by just using the attributes name.

Here ```name_len``` is accessed inside self:
```
seq:
  - id: name_len
    type: u4
  - id: name
    type: str
    encoding: ASCII
    size: name_len  # think of this like self.name_len / this->name_len
```

An attribute in a parent can be accessed using the special value of ```_parent```:
```
seq:
  - id: name_len
    type: u4
  - id: child
    type: child
types:
  child:
    seq:
      - id: name
        type: str
        encoding: ASCII
        size: _parent.name_len  # access attribute in parent
```

Deeply nested substructures can access the top-level structure using ```_root```:
```
seq:
  - id: name_len
    type: u4
  - id: layer_1
    type: layer_1
types:
  layer_1:
    seq:
      - id: layer_2
        type: layer_2
  layer_2:
    seq:
      - id: layer_3
        type: layer_3
  layer_3:
    seq:
      - id: name
        type: str
        encoding: ASCII
        size: _root.name_len  # access attribute in top-level substructure
        # NOTE: _parent._parent._parent.name_len (would have also worked, not as clean though).
```

## Exercise 1 - Metadata with self, root and parent access

Complete ```metadata.ksy``` by filling in the ```TODO```s.

The exercise should print out the names of the 'ROOT', 'CHILD' and 'GRAND_CHILD' when implemented.
