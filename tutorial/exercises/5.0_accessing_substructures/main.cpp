#include <iostream>

#include "metadata.hpp"

int main(int argc, char *argv[])
{
    Metadata metadata;
    Metadata metadata_copy;

    // set max name length to 10
    metadata.set_max_name_len(10);

    // fill in metadata
    metadata.set_name("Tom");
    metadata.mutable_child()->set_name("Dick");
    metadata.mutable_child()->mutable_grand_child()->set_name("Harry");

    // serailize one metadata, parse into other metadata (aka a deep copy)
    std::string buffer;
    metadata.SerializeToString(&buffer);
    metadata_copy.ParseFromString(buffer);

    // print the copy
    std::cout << "ROOT        : " << metadata_copy.name() << std::endl;
    std::cout << "CHILD       : " << metadata_copy.child().name() << std::endl;
    std::cout << "GRAND_CHILD : " << metadata_copy.child().grand_child().name() << std::endl;

    return 0;
}
