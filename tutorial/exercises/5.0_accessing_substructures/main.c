#include <stdio.h>

#include "metadata.h"

int main(int argc, char *argv[])
{
    metadata_t metadata;
    metadata_t metadata_copy;

    // must always init in C
    metadata_init(&metadata);
    metadata_init(&metadata_copy);

    // set max name length to 10
    metadata_set_max_name_len(&metadata, 10);

    // create children
    metadata_t *root = &metadata;
    child_t *child = metadata_mutable_child(root);
    grand_child_t *grand_child = child_mutable_grand_child(child);

    // fill in metadata
    metadata_set_name(root, "Tom");
    child_set_name(child, "Dick");
    grand_child_set_name(grand_child, "Harry");

    // serailize one metadata, parse into other metadata (aka a deep copy)
    size_t buffer_size = 0;
    char *buffer = metadata_serialize_to_string(&metadata, &buffer_size);
    metadata_parse_from_string(&metadata_copy, buffer, buffer_size);
    kpb_io_free(buffer);

    // print the copy
    const metadata_t *const_root = &metadata_copy;
    const child_t *const_child = metadata_child(const_root);
    const grand_child_t *const_grand_child = child_grand_child(const_child);
    printf("ROOT        : %s\n", metadata_name(const_root));
    printf("CHILD       : %s\n", child_name(const_child));
    printf("GRAND_CHILD : %s\n", grand_child_name(const_grand_child));

    // must always cleanup your messages in C
    metadata_fini(&metadata);
    metadata_fini(&metadata_copy);
    return 0;
}
