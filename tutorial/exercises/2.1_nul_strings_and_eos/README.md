# Variable size structures

## Kaitai Struct User Guide

Section 4.5 "Delimited structures"

## NUL-terminated strings

Rather than using a ```size``` for a string, we can use the type ```strz```. This will continue to parse characters until it reaches a nul-terminator (e.g. ```\0```). This also works for wide-character (e.g. UTF-16LE) strings where it will continue till it reaches a double-nul-terminator (e.g. ```\x00\x00```) aligned on 2-byte boundary. KPB is safe and will stop if it reaches the end of the binary data (even if no nul-terminator is found).

A simple nul-terminated string:
```
seq:
  - id: name
    type: strz  # the 'z' makes it nul-terminated
    encoding: UTF-16LE
```

For binary data at the end of a structure, you may want its size to be all remaining data. We can add ```size-eos: true``` to achieve this.

Filling a buffer to the end of the stream:
```
seq:
  - id: data
    size-eos: true
```

## Exercise 1 - using nul-terminated strings and EOS buffers

Complete ```person_namez.ksy```, ```main.c``` and ```main.cpp``` by filling in the ```TODO```s.

You should not need any "size" or "length" atttributes in ```seq```, use ```strz``` and ```size-eos``` instead.
