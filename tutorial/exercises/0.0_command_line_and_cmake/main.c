#include <stdio.h>

#include "helloworld.h"

int main(int argc, char *argv[])
{
    hw_helloworld_t msg;

    // always init the message - think of it as memset'ing all atttributes to zero.
    hw_helloworld_init(&msg);

    // parse a stream of bytes into a message.
    if ( hw_helloworld_parse_from_string(&msg, "HelloWorld!", sizeof("HelloWorld!")) )
    {
        printf("Parsed from buffer\n");
    }
    else
    {
        printf("Failed to parse message\n");
        hw_helloworld_fini(&msg);
        return 1;
    }

    // print the value of a attribute
    printf("GOT '%s'\n", hw_helloworld_helloworld(&msg));

    // change atttributes on the message as needed.
    if ( hw_helloworld_set_helloworld(&msg, "Goodbye!") )
    {
        printf("Message changed\n");
    }
    else
    {
        printf("Failed to set message\n");
        hw_helloworld_fini(&msg);
        return 1;
    }

    // allocate & serialize to a buffer (you must call kpb_io_free when your done).
    size_t buffer_size = 0;
    char *buffer = hw_helloworld_serialize_to_string(&msg, &buffer_size);
    if ( buffer )
    {
        printf("Serailized to buffer '%s\\x00' [%zd]\n", buffer, buffer_size);
        kpb_io_free(buffer);
    }
    else
    {
        printf("Failed to serailize message\n");
        hw_helloworld_fini(&msg);
        return 1;
    }

    // this will cleanup all internal allocations
    // you must call this when you are done
    hw_helloworld_fini(&msg);
    return 0;
}
