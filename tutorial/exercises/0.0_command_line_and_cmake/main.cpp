#include <iostream>

#include "helloworld.hpp"

int main(int argc, char *argv[])
{
    hw::Helloworld msg;

    // no need to init - class constructor will do this for you.

    // parse a C++ STL string into a message
    if ( msg.ParseFromString(std::string("HelloWorld!", sizeof("HelloWorld!"))) )
    {
        std::cout << "Parsed from buffer" << std::endl;
    }
    else
    {
        std::cout << "Failed to parse message\n" << std::endl;
        return 1;
    }

    // print the value of a attribute
    std::cout << "GOT '" << msg.helloworld() << "'" << std::endl;

    // change atttributes on the message as needed.
    if ( msg.set_helloworld("Goodbye!") )
    {
        std::cout << "Message changed" << std::endl;
    }
    else
    {
        std::cout << "Failed to set message" << std::endl;
        return 1;
    }

    // serialize to C++ STL string.
    std::string buffer;
    if ( msg.SerializeToString(&buffer) )
    {
        std::cout << "Serailized to buffer '" << buffer << "\\x00' [" << buffer.size() << "]" << std::endl;
    }
    else
    {
        std::cout << "Failed to serailize message" << std::endl;
        return 1;
    }

    // no need to cleanup, class destructor will take care of that
    return 0;
}
