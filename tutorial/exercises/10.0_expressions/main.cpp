#include <iostream>

#include "packet.hpp"

int main(int argc, char *argv[])
{
    Packet packet;
    Packet packet_copy;

    // put "Helloworld" into the packet
    PacketBody *body = packet.mutable_body();
    body->set_data(std::string("Helloworld", sizeof("Helloworld")));

    // serailize one packet, parse into other packet (aka a deep copy)
    std::string buffer;
    packet.SerializeToString(&buffer);
    packet_copy.ParseFromString(buffer);

    // get "Helloworld" from the copy
    std::cout << "DATA: '" << packet_copy.body().data() << "\\x00' [size " \
        << (int)packet_copy.body().data().size() << " bytes]" << std::endl;

    return 0;
}
