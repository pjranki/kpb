# Expressions

## Kaitai Struct User Guide

Section 6 "Expression language"

## Substructure size expression

Many formats have a "total size" or "body size" field that must be set the size of the substructure.

Example:
```
meta:
  - id: message
    endian: be

seq:
  - id: total_size
    type: u4
  - id: body
    type: message_body
    size: 'total_size > 4 ? total_size - 4 : 0'

types:
  message_body:
    - id: data_size
      type: u2
    - id: data
      size: data_size
```

To make KPB automatically recalculate the value of ```total_size```, we can use expression language in an update block.

Example:
```
meta:
  - id: message
    endian: be

seq:
  - id: total_size
    type: u4
  - id: body
    type: message_body
    size: 'total_size > 4 ? total_size - 4 : 0'

-update:
  - id: total_size  # we want to set the value of 'total_size'
    value: 4 + body.length  # the 'length' attribute on a substructure is its serialized size in bytes

types:
  message_body:
    - id: data_size
      type: u2
    - id: data
      size: data_size
```

## DEFAULT value

To set a default value for an attribute, you can use ```-default```. This will be the value set when the object is initialized, constructed or cleared.

Example:
```
seq:
  - id: answer_to_universe
    type: u4
    -default: 42  # answer_to_universe will be '42' when the object is initialized, constructed or cleared.
```

Please not that ```-default``` cannot take an expression (e.g. no ```40 + 2``` etc.), it must be the final value e.g. ```42```.

## Expression language limitation when serializing in KPB

Object initializers and constructors do not call the ```-update``` block - all values are set to the equivalent of zero or empty. This means that the previous example will have a ```total_size``` of ```0``` when the ```Message``` is initialized or constructed. This is incorrect, as the size will always be at least 4 bytes (```sizeof(total_size)```) plus the ```message_body``` which will have a minimum size of 2 bytes (```sizeof(data_size)```). Hence the default value of ```total_size``` should be ```6```.

Example:
```
meta:
  - id: message
    endian: be

seq:
  - id: total_size
    type: u4
    -default: 6  # default total_size (size of an empty message) will be 6.
  - id: body
    type: message_body
    size: 'total_size > 4 ? total_size - 4 : 0'

-update:
  - id: total_size
    value: 4 + body.length

types:
  message_body:
    - id: data_size
      type: u2
    - id: data
      size: data_size
```

## Exercise 1 - Packet

Complete ```packet.ksy``` by filling in the ```TODO```s.
