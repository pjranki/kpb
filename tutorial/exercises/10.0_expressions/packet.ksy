meta:
  id: packet
  endian: be

seq:
  - id: header
    type: packet_header
  - id: body
    type: packet_body
    # TODO: set the size (parse size) of the body to 'body_size' from the header.

#-update:
#   TODO: update header when body changes

types:
  packet_header:
    seq:
      - id: total_size
        type: u4
        # TODO: make the default total size 10 (8 bytes for header, 2 bytes for default body)
      - id: body_size
        type: u4
        # TODO: make the default body size 2 (2 bytes for default body)
    #instances:
    #   TODO: create a value instance called 'header_size' and return the size of the header
    # -update:
    #   TODO: set the value of 'body_size'
    #   TODO: set the value of 'total_size' to header size plus body size
  packet_body:
    seq:
      - id: data_size
        type: u2
      - id: data
        size: data_size
    #-update:
    #   TODO: set the value of 'data_size'
