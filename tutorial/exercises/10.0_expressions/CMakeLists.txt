add_executable(packet_c
    main.c
)
target_kpb_c(packet_c
    packet.ksy  # generates packet.c, packet.h
)
target_link_libraries(packet_c
    kpb_io  # C runtime for KPB i.e. common C code for KPB
)

add_executable(packet_cpp
    main.cpp
)
target_kpb_cpp(packet_cpp
    packet.ksy  # generates packet.cpp, packet.hpp
)
target_link_libraries(packet_cpp
    kpb_cpp_stl_io  # C++ STL runtime for KPB i.e. common C++ code for KPB
)
