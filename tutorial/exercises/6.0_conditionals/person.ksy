meta:
  id: person
  endian: be

seq:
  - id: flags
    type: u2
    enum: flag
  - id: name
    type: strz
    encoding: ASCII
    # TODO: add 'if' attribute (check for name flag)
    # TODO: add '-set' attribute (set name flag)
  - id: age
    type: u1
    # TODO: add 'if' attribute (check for age flag)
    # TODO: add '-set' attribute (set age flag)
  - id: mobile
    type: strz
    encoding: ASCII
    # TODO: add 'if' attribute (check for mobile flag)
    # TODO: add '-set' attribute (set mobile flag)

enums:
  flag:
    0x0001: name
    0x0002: age
    0x0010: mobile
