# Conditionals

## Kaitai Struct User Guide

Section 4.9 "Conditionals"

## IF statements and HAS methods

Some binary messages have attributes that are only present based on a previous attribute (e.g. flags). KPB has an ```if``` statement that can be used in a ```ksy``` file to control when a attribute is parse/serialized.

Have a look at the following example:
```
seq:
  - id: crc32_present
    type: u1
  - id: crc32
    type: u4
    if: crc32_present != 0
```

In this example, ```crc32``` would only be parsed/serialized if ```crc32_present``` is non-zero.

Here we can do the same thing, but with a flag:
```
seq:
  - id: flags
    type: u2
  - id: crc32
    type: u4
    if: (flags & 0x08) != 0
```

In this example, flag ```0x08``` determines if ```crc32``` is parsed/serialized or not.

A cleaner way of handling flags is with enumerators:
```
seq:
  - id: flags
    type: u2
    enum: flag
  - id: crc32
    type: u4
    if: '(flags.to_i & flag::crc32.to_i) != 0'
enums:
  flag:
    0x08: crc32
```

Important things to note in the above example:
 - We wrap the ```if``` statement in quotes ```''``` as we need to use colons ```::``` in the statement.
 - We use ```to_i``` to treat the enumerator as a number (so that we can do the ```&``` operation).

## SET block

The previous example has one drawback - it won't set the flag when you set the ```crc32``` value. To achieve this, we can use a ```-set``` block that only gets run when that specific attribute changes.

And finally, a complete example:
```
seq:
  - id: flags
    type: u2
    enum: flag
  - id: crc32
    type: u4
    if: '(flags.to_i & flag::crc32.to_i) != 0'
    -set:
      - id: flags
        value: 'flags.to_i | flag::crc32.to_i'
enums:
  flag:
    0x08: crc32
```

The only difference between a ```-set``` block and an ```-update``` block:
 - ```-set```: called when a SINGLE attribute is changed.
 - ```-update```: called when ANY attribute in the structure changes.

## HAS method

All attributes in a structure are generated with a ```has_``` method.

C example:
```c
structure_t s;
structure_init(&s);
bool present = structure_has_crc32(&s);
```

C++ example:
```c
Structure s;

bool present = s.has_crc32();
```

## Exercise 1 - Using flags to include atttributes

Complete ```person.ksy``` by filling in the ```TODO```s.

HINT: wrap your ```if``` statements with quotes when using colons (`:` or `::`) e.g. ```if: '...'```
HINT: wrap your ```value``` statements (in the ```-set```) with quotes when using colons too.
