#include <iostream>

#include "person.hpp"

int main(int argc, char *argv[])
{
    Person person[3];

    // fill in person 1
    person[0].set_name("Tom");
    person[0].set_age(22);

    // fill in person 2
    person[1].set_name("Harry");
    person[1].set_mobile("0412345678");

    // fill in person 3
    person[2].set_age(42);
    person[2].set_mobile("0412345678");

    // print people
    for ( uint8_t i = 0; i < 3; i++ )
    {
        if ( person[i].has_name() )
        {
            std::cout << "NAME : " << person[i].name() << std::endl;
        }
        else
        {
            std::cout << "NAME : (no name)" << std::endl;
        }
        if ( person[i].has_age() )
        {
            std::cout << "AGE  : " << static_cast<int>(person[i].age()) << std::endl;
        }
        else
        {
            std::cout << "AGE  : (no age)" << std::endl;
        }
        if ( person[i].has_mobile() )
        {
            std::cout << "MOBILE: " << person[i].mobile() << std::endl;
        }
        else
        {
            std::cout << "MOBILE: (no mobile)" << std::endl;
        }
        std::cout << std::endl;
    }

    return 0;
}
