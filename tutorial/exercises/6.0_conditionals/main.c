#include <stdio.h>

#include "person.h"

int main(int argc, char *argv[])
{
    person_t person[3];

    // must always init in C
    person_init(&person[0]);
    person_init(&person[1]);
    person_init(&person[2]);

    // fill in person 1
    person_set_name(&person[0], "Tom");
    person_set_age(&person[0], 22);

    // fill in person 2
    person_set_name(&person[1], "Harry");
    person_set_mobile(&person[1], "0412345678");

    // fill in person 3
    person_set_age(&person[2], 42);
    person_set_mobile(&person[2], "0411222333");

    // print people
    for ( uint8_t i = 0; i < 3; i++ )
    {
        if ( person_has_name(&person[i]) )
        {
            printf("NAME : %s\n", person_name(&person[i]));
        }
        else
        {
            printf("NAME : (no name)\n");
        }
        if ( person_has_age(&person[i]) )
        {
            printf("AGE  : %d\n", person_age(&person[i]));
        }
        else
        {
            printf("AGE  : (no age)\n");
        }
        if ( person_has_mobile(&person[i]) )
        {
            printf("MOBILE: %s\n", person_mobile(&person[i]));
        }
        else
        {
            printf("MOBILE: (no mobile)\n");
        }
        printf("\n");
    }

    // must always cleanup your messages in C
    person_fini(&person[0]);
    person_fini(&person[1]);
    person_fini(&person[2]);
    return 0;
}
