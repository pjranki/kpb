# Advanced switching

## Kaitai Struct User Guide

Section 7.1 "Advanced switching"

## Advance switching with an enumerator

Many protocols have polymorphic attributes that contain a different substructure depending on the value of another attribute.

KPB supports this with a special ```type``` section:
```
seq:
  - id: select_based_on_this_value
    type: u2
    enum: select_enum
  - id: content
    type:  # the substructure used for 'content' depends on the value of 'select_based_on_this_value'
      switch-on: select_based_on_this_value
      -name: enum  # function names will use the enumerator name (e.g. 'type_1'). To use type name, use 'type' here.
      cases:
         'select_type::type_1': substructure_1
         'select_type::type_2': substructure_2
         'select_type::type_3': substructure_3

enums:
  select_enum:
    1: type_1
    2: type_2
    3: type_3

types:
  substructure_1:
    ...
  substructure_2:
    ...
  substructure_3:
    ...
```

When binary data gets parsed, ```select_based_on_this_value``` is read first. Then the parser for ```content``` will be selected based on the value of ```select_based_on_this_value```.

When modifying the structure (e.g. selecting one of the ```cases``` for ```content```), we need to also update the value of ```select_based_on_this_value``` so that it will serialize the selected type. To do this, we use a ```-set``` block and the special value of ```_case``` which will be set to the value of the enumerator selected.

A complete example:
```
seq:
  - id: select_based_on_this_value
    type: u2
    enum: select_enum
  - id: content
    -set:
      - id: select_based_on_this_value   # when content changes, 'select_based_on_this_value' will get updated.
        value: _case  # this will be the selected enumerator e.g. the value 11 if substructure_2 is selected.
    type:
      switch-on: select_based_on_this_value
      -name: enum
      cases:
         'select_type::type_1': substructure_1
         'select_type::type_2': substructure_2
         'select_type::type_3': substructure_3

enums:
  select_enum:
    10: type_1
    11: type_2
    12: type_3

types:
  substructure_1:
    ...
  substructure_2:
    ...
  substructure_3:
    ...
```

## Exercise 1 - Container

Complete ```container.ksy``` by filling in the ```TODO```s.

Then uncomment the ```TODO```s in ```main.c``` and ```main.cpp``` and test you implementation.
