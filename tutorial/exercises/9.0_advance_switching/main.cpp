#include <iostream>

#include "container.hpp"

/* TODO: uncomment once you have implemented 'content' attribute in container.ksy file
static void print_container_contents(const Container &container)
{
    switch(container.content_type())
    {
        case Container::Type::TYPE_NUMBER:
            std::cout << "NUMBER: " << (int)container.container_number().value() << std::endl;
            break;
        case Container::Type::TYPE_TEXT:
            std::cout << "TEXT  : '" << container.container_text().text().c_str() << "'" << std::endl;
            break;
        case Container::Type::TYPE_DATA:
            std::cout << "DATA  : (" << (int)container.container_data().data().size() << " bytes)" << std::endl;
            break;
        default:
            std::cout << "(unknown content type " << (int)container.content_type() << ")" << std::endl;
            break;
    }
}
*/

int main(int argc, char *argv[])
{
    Container container;

/* TODO: uncomment once you have implemented 'content' attribute in container.ksy file
    // store a number in the container
    container.mutable_container_number()->set_value(42);
    print_container_contents(container);

    // store some text in the container
    container.mutable_container_text()->set_text("Hello");
    print_container_contents(container);

    // store some bytes in the container
    container.mutable_container_data()->set_data(std::string("\xff\x00\xcc", 3));
    print_container_contents(container);
*/

    return 0;
}
