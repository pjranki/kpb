meta:
  id: container
  endian: be

seq:
  - id: content_type
    type: u1
    enum: type
  #- id: content
  #  TODO: implement 'content' so that it can be a number, text or data
  #  HINT: use "-name: type" in the 'type' area to make it use the object names when generating functions.

enums:
  type:
    1: number
    2: text
    3: data

types:
  container_number:
    seq:
      - id: value
        type: u4
  container_text:
    seq:
      - id: text
        type: strz
        encoding: ASCII
  container_data:
    seq:
      - id: data_size
        type: u4
      - id: data
        size: data_size
    -update:
      - id: data_size
        value: data.length
