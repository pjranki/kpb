# Variable size structures

## Kaitai Struct User Guide

Section 4.4 "Variable-length structures"

## Referencing a size attribute

Binary buffers and strings have an associated size/length. Many binary protocols use a separate attribute to store the length. With KPB, we can reference a separate attribute to store size/length.

For a binary buffer, we would define this in a ```ksy``` file as follows:
```
seq:
  - data_len
    type: u2
  - id: data
    size: data_len
```

In the above example, the amount of bytes parsed into ```data``` is taken from the previously parsed value ```data_len```. When we serialize, we need to update ```data_len``` so that it matches the size of ```data```. To do this, we use a KPB ```-update``` block which will be called every time a change is made to any attribute.

A more complete example:
```
seq:
  - data_len
    type: u2
  - id: data
    size: data_len
-update:
  - id: data_len
    value: data.length
```

In the above example, we are setting ```data_len``` to ```data.length``` every time changes are made to any attribute. It should be noted that ```data.length``` is the actual size (in bytes) allocated to the ```data``` attribute. When we call ```set_data```, update is called and the value of ```data_len``` is set.

## ASCII strings

An ASCII string attribute is handled almost identically:
```
seq:
  - name_len
    type: u2
  - id: name
    type: str
    size: name_len
    encoding: ASCII
-update:
  - id: name_len
    value: name.length
```

## Wide charater strings

When it comes to wide character strings (e.g. ```wchar_t```), length and size mean different things. There are two ways we can implement this in a ```ksy``` file.

We can have a ```name_size``` attribute that has the number of bytes in the name:
```
seq:
  - name_size
    type: u2
  - id: name
    type: str
    size: name_size
    encoding: UTF-16LE
-update:
  - id: name_size
    value: name.length * 2  # We must multiply by sizeof(wchar_t) so that name_size is the number of bytes.
```

Or we can have a ```name_len``` attribute that has the number of characters in the name:
```
seq:
  - name_len
    type: u2
  - id: name
    type: str
    size: name_len  * 2  # We must multiply by sizeof(wchar_t) so that size is the number of bytes.
    encoding: UTF-16LE
-update:
  - id: name_len
    value: name.length
```

## Exercise 1 - using variable-size data and strings

Complete ```person_name.ksy```, ```main.c``` and ```main.cpp``` by filling in the ```TODO```s.

```person_name.ksy``` needs to contain the following:
 - ```name_len```: uint8_t
 - ```name```: ascii string
 - ```surname_len```: uint16_t
 - ```surname```: utf-16le string
 - ```data_len```: uint32_t
 - ```data```: bytes
