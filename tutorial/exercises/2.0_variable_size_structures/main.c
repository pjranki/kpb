#include <stdio.h>
#include <string.h>

#include "person_name.h"

int main(int argc, char *argv[])
{
    person_name_t record;
    person_name_t record_copy;

    // must always init in C
    person_name_init(&record);
    person_name_init(&record_copy);

    // fill in record
/* TODO: uncomment this when you have updated the person_name.ksy file
    person_name_set_name(&record, "Joe");
    person_name_set_surname(&record, L"Smith");
    person_name_set_data(&record, "\x01\x02\x03", 3);
*/

    // serailize one record, parse into other record (aka a deep copy)
    size_t buffer_size = 0;
    char *buffer = person_name_serialize_to_string(&record, &buffer_size);
    person_name_parse_from_string(&record_copy, buffer, buffer_size);
    kpb_io_free(buffer);

/* TODO: uncomment this when you have updated the person_name.ksy file
    // print the copy
    printf("NAME   : %s\n", person_name_name(&record_copy));
    printf("SURNAME: %S\n", person_name_surname(&record_copy));
    if ( 0 == memcmp("\x01\x02\x03", person_name_data(&record_copy), person_name_data_size(&record_copy)))
    {
        printf("DATA   : match\n");
    }
    else
    {
        printf("DATA   : MISMATCH\n");
    }
*/

    // must always cleanup your messages in C
    person_name_fini(&record);
    person_name_fini(&record_copy);
    return 0;
}
