meta:
  id: square
  endian: le

seq:
  - id: x0
    type: s2
  - id: y0
    type: s2
  - id: x1
    type: s2
  - id: y1
    type: s2

#instances:
# TODO: add 'width' instance
# TODO: add 'height' instance
# TODO: add 'area' instance
# TODO: add 'perimeter' instance
