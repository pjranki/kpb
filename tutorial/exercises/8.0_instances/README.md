# Instances

## Kaitai Struct User Guide

Section 4.12 "Instances: data beyond the sequence"

## VALUE instances

A value instance is a calculated/generated value from other attributes in the sequence (or other structures).

A basic example of a value instance:
```
seq:
  - id: a
    type: s2
  - id: b
    type: s2
instances:
  addition:
    value: a + b
  subtraction
    value: a - b
  multiplication:
    value: a * b
  division:
    value: a /b
```

Each ```value``` can be populated with an expression - to be discussed in a later section.

Basic control flow can also be implemented in an instance too:
```
seq:
  - id: a
    type: s2
  - id: b
    type: s2
instances:
  min:
    value: 'a < b ? a : b'  # NOTE: we need to wrap in quotes as we use a colon (':')
  max:
    value: 'a > b ? a : b'  # NOTE: we need to wrap in quotes as we use a colon (':')
```

And instances can be accessed like any other attribute (C example):
```c
int16_t a = example_a(&example);
int16_t b = example_b(&example);
int16_t min = example_min(&example);
int16_t max = example_max(&example);
```

C++ example:
```cpp
int16_t a = example.a();
int16_t b = example.b();
int16_t min = example.min();
int16_t max = example.max();
```

## Exercise 1 - Square

Complete ```square.ksy``` by adding instances for ```width```, ```height```, ```area```, ```perimeter```.

Then complete the ```main.c``` and ```main.cpp``` by filling in the ```TODO```s.

## POSITIONAL instances

Although these are supported in Kaitai Struct, they have not been implemented for KPB.
