add_executable(animal_record_c_soln
    main.c
)
target_kpb_c(animal_record_c_soln
    animal_record.ksy  # generates animal_record.c, animal_record.h
)
target_link_libraries(animal_record_c_soln
    kpb_io  # C runtime for KPB i.e. common C code for KPB
)

add_executable(animal_record_cpp_soln
    main.cpp
)
target_kpb_cpp(animal_record_cpp_soln
    animal_record.ksy  # generates animal_record.cpp, animal_record.hpp
)
target_link_libraries(animal_record_cpp_soln
    kpb_cpp_stl_io  # C++ STL runtime for KPB i.e. common C++ code for KPB
)
