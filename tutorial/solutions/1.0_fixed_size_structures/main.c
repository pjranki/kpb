#include <stdio.h>
#include <string.h>

#include "animal_record.h"

#define TEST_UUID "\x11\x22\x33\x44\x55\x66\x77\x88\x99\x00\xaa\xbb\xcc\xdd\xee\xff"
#define TEST_UUID_SIZE  16

int main(int argc, char *argv[])
{
    animal_record_t record;
    animal_record_t record_copy;

    // must always init in C
    animal_record_init(&record);
    animal_record_init(&record_copy);

    // fill in record
    animal_record_set_uuid(&record, TEST_UUID, TEST_UUID_SIZE);
    animal_record_set_name(&record, "Fido");
    animal_record_set_birth_year(&record, 2020);
    animal_record_set_weight(&record, 9.1);
    animal_record_set_rating(&record, -5);

    // serailize one record, parse into other record (aka a deep copy)
    size_t buffer_size = 0;
    char *buffer = animal_record_serialize_to_string(&record, &buffer_size);
    animal_record_parse_from_string(&record_copy, buffer, buffer_size);
    kpb_io_free(buffer);

    // print the copy
    if ( 0 == memcmp(TEST_UUID, animal_record_uuid(&record_copy), animal_record_uuid_size(&record_copy)))
    {
        printf("UUID  : match\n");
    }
    else
    {
        printf("UUID  : MISMATCH\n");
    }
    printf("NAME  : %s\n", animal_record_name(&record_copy));
    printf("YEAR  : %d\n", animal_record_birth_year(&record_copy));
    printf("WEIGHT: %2.2f\n", animal_record_weight(&record_copy));
    printf("RATING: %d\n", animal_record_rating(&record_copy));

    // must always cleanup your messages in C
    animal_record_fini(&record);
    animal_record_fini(&record_copy);
    return 0;
}
