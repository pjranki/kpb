#include <iostream>
#include <string.h>

#include "animal_record.hpp"

#define TEST_UUID "\x11\x22\x33\x44\x55\x66\x77\x88\x99\x00\xaa\xbb\xcc\xdd\xee\xff"
#define TEST_UUID_SIZE  16

int main(int argc, char *argv[])
{
    AnimalRecord record;
    AnimalRecord record_copy;

    // fill in record
    record.set_uuid(std::string(TEST_UUID, TEST_UUID_SIZE));
    record.set_name("Fido");
    record.set_birth_year(2020);
    record.set_weight(9.1);
    record.set_rating(-5);

    // serailize one record, parse into other record (aka a deep copy)
    std::string buffer;
    record.SerializeToString(&buffer);
    record_copy.ParseFromString(buffer);

    // print the copy
    if ( 0 == memcmp(TEST_UUID, record_copy.uuid().c_str(), record_copy.uuid().size()))
    {
        std::cout << "UUID  : match" << std::endl;
    }
    else
    {
        std::cout << "UUID  : MISMATCH" << std::endl;
    }
    std::cout << "NAME  : " << record_copy.name() << std::endl;
    std::cout << "YEAR  : " << record_copy.birth_year() << std::endl;
    std::cout << "WEIGHT: " << record_copy.weight() << std::endl;
    std::cout << "RATING: " << record_copy.rating() << std::endl;

    return 0;
}
