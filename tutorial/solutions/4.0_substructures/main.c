#include <stdio.h>

#include "track.h"

int main(int argc, char *argv[])
{
    track_t track;
    track_t track_copy;
    str_with_len_t *str = NULL;

    // must always init in C
    track_init(&track);
    track_init(&track_copy);

    // fill in track
    str = track_mutable_track_title(&track);
    str_with_len_set_value(str, "Star Treatment");
    str = track_mutable_album_title(&track);
    str_with_len_set_value(str, "Tranquility Base Hotel & Casino");
    str = track_mutable_artist_name(&track);
    str_with_len_set_value(str, "Arctic Monkeys");

    // serailize one track, parse into other track (aka a deep copy)
    size_t buffer_size = 0;
    char *buffer = track_serialize_to_string(&track, &buffer_size);
    track_parse_from_string(&track_copy, buffer, buffer_size);
    kpb_io_free(buffer);

    // print the copy
    printf("TRACK  : %s\n", str_with_len_value(track_track_title(&track_copy)));
    printf("ALBUM  : %s\n", str_with_len_value(track_album_title(&track_copy)));
    printf("ARTIST : %s\n", str_with_len_value(track_artist_name(&track_copy)));

    // must always cleanup your messages in C
    track_fini(&track);
    track_fini(&track_copy);
    return 0;
}
