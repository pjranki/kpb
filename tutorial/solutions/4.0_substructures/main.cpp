#include <iostream>

#include "track.hpp"

int main(int argc, char *argv[])
{
    Track track;
    Track track_copy;

    // fill in track
    track.mutable_track_title()->set_value("Star Treatment");
    track.mutable_album_title()->set_value("Tranquility Base Hotel & Casino");
    track.mutable_artist_name()->set_value("Arctic Monkeys");

    // serailize one track, parse into other track (aka a deep copy)
    std::string buffer;
    track.SerializeToString(&buffer);
    track_copy.ParseFromString(buffer);

    // print the copy
    std::cout << "TRACK  : " << track_copy.track_title().value() << std::endl;
    std::cout << "ALBUM  : " << track_copy.album_title().value() << std::endl;
    std::cout << "ARTIST : " << track_copy.artist_name().value() << std::endl;

    return 0;
}
