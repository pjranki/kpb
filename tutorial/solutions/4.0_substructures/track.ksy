meta:
  id: track
  endian: le

seq:
  - id: track_title
    type: str_with_len
  - id: album_title
    type: str_with_len
  - id: artist_name
    type: str_with_len

types:
  str_with_len:
    seq:
      - id: len
        type: u4
      - id: value
        type: str
        encoding: ascii
        size: len
    -update:
      - id: len
        value: value.length
