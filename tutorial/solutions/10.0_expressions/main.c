#include <stdio.h>

#include "packet.h"

int main(int argc, char *argv[])
{
    packet_t packet;
    packet_t packet_copy;

    // must always init in C
    packet_init(&packet);
    packet_init(&packet_copy);

    // put "Helloworld" into the packet
    packet_body_t *body = packet_mutable_body(&packet);
    packet_body_set_data(body, "Helloworld", sizeof("Helloworld"));

    // serailize one packet, parse into other packet (aka a deep copy)
    size_t buffer_size = 0;
    char *buffer = packet_serialize_to_string(&packet, &buffer_size);
    packet_parse_from_string(&packet_copy, buffer, buffer_size);
    kpb_io_free(buffer);

    // get "Helloworld" from the copy
    const packet_body_t *const_body = packet_body(&packet_copy);
    const char *data = packet_body_data(const_body);
    size_t data_size = packet_body_data_size(const_body);
    data = data_size == 0 ? "" : data;
    printf("DATA: '%s\\x00' [size %d bytes]\n", data, (int)data_size);

    // must always cleanup your messages in C
    packet_fini(&packet);
    packet_fini(&packet_copy);
    return 0;
}
