#include <iostream>

#include "number_list.hpp"
#include "string_list.hpp"
#include "buffer_list.hpp"
#include "phone_book.hpp"

int main(int argc, char *argv[])
{
    std::string buffer;
    int i = 0;

    /************** EXERCISE 1 *******************/

    NumberList number_list, number_list_copy;

    // populate with numbers
    number_list.add_number(10);
    number_list.add_number(20);
    number_list.add_number(30);

    // serailize one number_list, parse into other number_list (aka a deep copy)
    number_list.SerializeToString(&buffer);
    number_list_copy.ParseFromString(buffer);

    // print numbers
    i = 0;
    for ( auto number : number_list_copy.number() )
    {
        std::cout << "NUMBER[" << (int)i << "] = " << (int)number << std::endl;
        i++;
    }

    std::cout << std::endl;

    /************** EXERCISE 2 *******************/

    StringList string_list, string_list_copy;

    // populate with strings
    string_list.add_string("ABC");
    string_list.add_string("DEFG");
    string_list.add_string("HIJKL");

    // serailize one string_list, parse into other string_list (aka a deep copy)
    string_list.SerializeToString(&buffer);
    string_list_copy.ParseFromString(buffer);

    // print strings
    i = 0;
    for ( auto s : string_list_copy.string() )
    {
        std::cout << "STRING[" << (int)i << "] = '" << s << "'" << std::endl;
        i++;
    }

    std::cout << std::endl;

    /************** EXERCISE 3 *******************/

    BufferList buffer_list, buffer_list_copy;

    // populate with buffers
    buffer_list.add_buffer(std::string("\xcc\xdd\xee", 3));
    buffer_list.add_buffer(std::string("\x01\x02\x03\x04\x05", 5));
    buffer_list.add_buffer(std::string("\x90\x80\x70\x60\x50\x40\x30", 7));

    // serailize one buffer_list, parse into other buffer_list (aka a deep copy)
    buffer_list.SerializeToString(&buffer);
    buffer_list_copy.ParseFromString(buffer);

    // print buffers
    i = 0;
    for ( auto b : buffer_list_copy.buffer() )
    {
        std::cout << "BUFFER[" << (int)i << "] = ";
        for ( size_t j = 0; j < b.size(); j++ )
        {
            printf("0x%02x ", (uint8_t)(b.c_str()[j]));
        }
        std::cout << "[size = " << (int)b.size() << "]" << std::endl;
        i++;
    }

    std::cout << std::endl;

    /************** EXERCISE 4 *******************/

    PhoneBook phone_book, phone_book_copy;
    Entry *entry = nullptr;

    // populate phone book
    // NOTE: We are not leaking "entry" objects here.
    //       The allocations are stored inside "phone_book" and
    //       Freed when the destructor is called.
    entry = phone_book.add_entry();
    entry->set_name("Tom");
    entry->set_number("0411111111");
    entry = phone_book.add_entry();
    entry->set_name("Dick");
    entry->set_number("0422222222");
    entry = phone_book.add_entry();
    entry->set_name("Harry");
    entry->set_number("0433333333");

    // serailize one phone_book, parse into other phone_book (aka a deep copy)
    phone_book.SerializeToString(&buffer);
    phone_book_copy.ParseFromString(buffer);

    // print phone book copy
    std::cout << "Entries: " << static_cast<int>(phone_book_copy.count()) << std::endl;
    for ( auto const_entry_ref : phone_book_copy.entry() )
    {
        std::cout << std::endl;
        std::cout << "NAME   : " << const_entry_ref.get().name() << std::endl;
        std::cout << "NUMBER : " << const_entry_ref.get().number() << std::endl;
    }

    return 0;
}
