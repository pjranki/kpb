meta:
  id: phone_book
  endian: be

doc: |
  EXERCISE 4

seq:
  - id: count
    type: u4
  - id: entry
    type: entry
    repeat: expr
    repeat-expr: count

-update:
  - id: count
    value: entry.size

types:
  entry:
    seq:
      - id: name
        type: strz
        encoding: ASCII
      - id: number
        type: strz
        encoding: ASCII
