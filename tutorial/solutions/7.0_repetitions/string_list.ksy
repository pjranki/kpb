meta:
  id: string_list
  endian: le

doc: |
  EXERCISE 2

seq:
  - id: string
    type: strz
    encoding: ASCII
    repeat: eos
