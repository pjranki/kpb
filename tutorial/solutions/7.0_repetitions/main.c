#include <stdio.h>

#include "number_list.h"
#include "string_list.h"
#include "buffer_list.h"
#include "phone_book.h"

int main(int argc, char *argv[])
{
    size_t buffer_size = 0;
    const char *buffer = NULL;
    size_t i = 0;
    size_t j = 0;

    /************** EXERCISE 1 *******************/

    number_list_t number_list, number_list_copy;

    number_list_init(&number_list);
    number_list_init(&number_list_copy);

    // populate with numbers
    number_list_add_number(&number_list, 10);
    number_list_add_number(&number_list, 20);
    number_list_add_number(&number_list, 30);

    // serailize one number_list, parse into other number_list (aka a deep copy)
    buffer_size = 0;
    buffer = number_list_serialize_to_string(&number_list, &buffer_size);
    number_list_parse_from_string(&number_list_copy, buffer, buffer_size);
    kpb_io_free((void*)buffer);

    // print numbers
    for ( i = 0; i < number_list_number_size(&number_list_copy); i++ )
    {
        printf("NUMBER[%d] = %d\n", (int)i, number_list_number_at(&number_list_copy, i));
    }

    number_list_fini(&number_list);
    number_list_fini(&number_list_copy);

    printf("\n");

    /************** EXERCISE 2 *******************/

    string_list_t string_list, string_list_copy;

    string_list_init(&string_list);
    string_list_init(&string_list_copy);

    // populate with strings
    string_list_add_string(&string_list, "ABC");
    string_list_add_string(&string_list, "DEFG");
    string_list_add_string(&string_list, "HIJKL");

    // serailize one string_list, parse into other string_list (aka a deep copy)
    buffer_size = 0;
    buffer = string_list_serialize_to_string(&string_list, &buffer_size);
    string_list_parse_from_string(&string_list_copy, buffer, buffer_size);
    kpb_io_free((void*)buffer);

    // print strings
    for ( i = 0; i < string_list_string_size(&string_list_copy); i++ )
    {
        printf("STRING[%d] = '%s'\n", (int)i, string_list_string_at(&string_list_copy, i));
    }

    string_list_fini(&string_list);
    string_list_fini(&string_list_copy);

    printf("\n");

    /************** EXERCISE 3 *******************/

    buffer_list_t buffer_list, buffer_list_copy;

    buffer_list_init(&buffer_list);
    buffer_list_init(&buffer_list_copy);

    // populate with buffers
    buffer_list_add_buffer(&buffer_list, "\xcc\xdd\xee", 3);
    buffer_list_add_buffer(&buffer_list, "\x01\x02\x03\x04\x05", 5);
    buffer_list_add_buffer(&buffer_list, "\x90\x80\x70\x60\x50\x40\x30", 7);

    // serailize one buffer_list, parse into other buffer_list (aka a deep copy)
    buffer_size = 0;
    buffer = buffer_list_serialize_to_string(&buffer_list, &buffer_size);
    buffer_list_parse_from_string(&buffer_list_copy, buffer, buffer_size);
    kpb_io_free((void*)buffer);

    // print buffers
    for ( i = 0; i < buffer_list_buffer_size(&buffer_list_copy); i++ )
    {
        buffer_size = buffer_list_buffer_size_at(&buffer_list_copy, i);
        buffer = buffer_list_buffer_at(&buffer_list_copy, i);
        printf("BUFFER[%d] = ", (int)i);
        for ( j = 0; j < buffer_size; j++ )
        {
            printf("0x%02x ", (uint8_t)(buffer[j]));
        }
        printf("[size = %d]\n", (int)buffer_size);
    }

    buffer_list_fini(&buffer_list);
    buffer_list_fini(&buffer_list_copy);

    printf("\n");

    /************** EXERCISE 4 *******************/

    phone_book_t phone_book, phone_book_copy;
    entry_t *entry = NULL;

    phone_book_init(&phone_book);
    phone_book_init(&phone_book_copy);

    // populate phone book
    // NOTE: We are not leaking "entry" objects here.
    //       The allocations are stored inside "phone_book" and
    //       Freed when the "phone_book_fini" is called.
    entry = phone_book_add_entry(&phone_book);
    entry_set_name(entry, "Tom");
    entry_set_number(entry, "0411111111");
    entry = phone_book_add_entry(&phone_book);
    entry_set_name(entry, "Dick");
    entry_set_number(entry, "0422222222");
    entry = phone_book_add_entry(&phone_book);
    entry_set_name(entry, "Harry");
    entry_set_number(entry, "0433333333");

    // serailize one phone_book, parse into other phone_book (aka a deep copy)
    buffer_size = 0;
    buffer = phone_book_serialize_to_string(&phone_book, &buffer_size);
    phone_book_parse_from_string(&phone_book_copy, buffer, buffer_size);
    kpb_io_free((void*)buffer);

    // print phone book copy
    printf("Entries: %d\n", phone_book_count(&phone_book_copy));
    for ( i = 0; i < phone_book_entry_size(&phone_book_copy); i++ )
    {
        const entry_t *const_entry = phone_book_entry_at(&phone_book_copy, i);
        printf("\n");
        printf("NAME   : %s\n", entry_name(const_entry));
        printf("NUMBER : %s\n", entry_number(const_entry));
    }

    phone_book_fini(&phone_book);
    phone_book_fini(&phone_book_copy);
    return 0;
}
