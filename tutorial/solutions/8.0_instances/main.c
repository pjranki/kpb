#include <stdio.h>

#include "square.h"

int main(int argc, char *argv[])
{
    square_t square;

    // must always init in C
    square_init(&square);

    // top-left corner (-1, -2)
    square_set_x0(&square, -1);
    square_set_y0(&square, -2);

    // bottom-right corner (3, 4)
    square_set_x1(&square, 3);
    square_set_y1(&square, 4);

    // print square
    printf("SQUARE: (%d, %d) -> (%d, %d)\n",
        square_x0(&square),
        square_y0(&square),
        square_x1(&square),
        square_y1(&square));

    // print width, height
    printf("SQUARE: width=%d [expected %d], height=%d [expected %d]\n",
        (int)square_width(&square), 4,
        (int)square_height(&square), 6);

    // print area
    printf("SQUARE: area=%d [expected %d]\n", (int)square_area(&square), 24);

    // print perimeter
    printf("SQUARE: perimeter=%d [expected %d]\n", (int)square_perimeter(&square), 20);

    // must always cleanup your messages in C
    square_fini(&square);
    return 0;
}
