meta:
  id: square
  endian: le

seq:
  - id: x0
    type: s2
  - id: y0
    type: s2
  - id: x1
    type: s2
  - id: y1
    type: s2

instances:
  width:
    value: 'x1 > x0 ? x1 - x0 : x0 - x1'
  height:
    value: 'y1 > y0 ? y1 - y0 : y0 - y1'
  area:
    value: width * height
  perimeter:
    value: (2 * width)  + (2 * height)
