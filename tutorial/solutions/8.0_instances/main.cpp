#include <iostream>

#include "square.hpp"

int main(int argc, char *argv[])
{
    Square square;

    // top-left corner (-1, -2)
    square.set_x0(-1);
    square.set_y0(-2);

    // bottom-right corner (3, 4)
    square.set_x1(3);
    square.set_y1(4);

    // print square
    std::cout << "SQUARE: (" << square.x0() << ", " << square.y0() << ") -> (" \
        << square.x1() << ", " << square.y1() << ")" << std::endl;

    // print width, height
    std::cout << "SQUARE: width=" << square.width() << " [expected " << 4 << "], height=" \
        << square.height() << " [expected " << 6 << "]" << std::endl;

    // print area
    std::cout << "SQUARE: area=" << square.area() << " [expected " << 24 << "]" << std::endl;

    // print perimeter
    std::cout << "SQUARE: perimeter=" << square.perimeter() << " [expected " << 20 << "]" << std::endl;

    return 0;
}
