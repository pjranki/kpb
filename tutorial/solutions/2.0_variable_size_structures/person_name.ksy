meta:
  id: person_name
  endian: be

seq:
  - id: name_len
    type: u1
  - id: name
    type: str
    size: name_len
    encoding: ASCII
  - id: surname_len
    type: u2
  - id: surname
    type: str
    size: surname_len * 2
    encoding: UTF-16LE
  - id: data_len
    type: u4
  - id: data
    size: data_len

-update:
  - id: name_len
    value: name.length
  - id: surname_len
    value: surname.length
  - id: data_len
    value: data.length
