meta:
  id: container
  endian: be

seq:
  - id: content_type
    type: u1
    enum: type
  - id: content
    -set:
      - id: content_type
        value: _case  # when we change content, '_case' will be the selected enumerator from the cases.
    type:
      switch-on: content_type
      -name: type
      cases:
         'type::number': container_number
         'type::text': container_text
         'type::data': container_data

enums:
  type:
    1: number
    2: text
    3: data

types:
  container_number:
    seq:
      - id: value
        type: u4
  container_text:
    seq:
      - id: text
        type: strz
        encoding: ASCII
  container_data:
    seq:
      - id: data_size
        type: u4
      - id: data
        size: data_size
    -update:
      - id: data_size
        value: data.length
