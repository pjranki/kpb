#include <stdio.h>

#include "container.h"

static void print_container_contents(const container_t *container)
{
    const container_number_t *number = NULL;
    const container_text_t *text = NULL;
    const container_data_t *data = NULL;
    switch(container_content_type(container))
    {
        case CONTAINER_TYPE_NUMBER:
            number = container_container_number(container);
            printf("NUMBER: %d\n", container_number_value(number));
            break;
        case CONTAINER_TYPE_TEXT:
            text = container_container_text(container);
            printf("TEXT  : '%s'\n", container_text_text(text));
            break;
        case CONTAINER_TYPE_DATA:
            data = container_container_data(container);
            printf("DATA  : (%d bytes)\n", (int)container_data_data_size(data));
            break;
        default:
            printf("(unknown content type %d)\n", container_content_type(container));
            break;
    }
}

int main(int argc, char *argv[])
{
    container_t container;

    // must always init in C
    container_init(&container);

    // store a number in the container
    container_number_t *number = container_mutable_container_number(&container);
    container_number_set_value(number, 42);
    print_container_contents(&container);

    // store some text in the container
    container_text_t *text = container_mutable_container_text(&container);
    container_text_set_text(text, "Hello");
    print_container_contents(&container);

    // store some bytes in the container
    container_data_t *data = container_mutable_container_data(&container);
    container_data_set_data(data, "\xff\x00\xcc", 3);
    print_container_contents(&container);

    // must always cleanup your messages in C
    container_fini(&container);
    return 0;
}
