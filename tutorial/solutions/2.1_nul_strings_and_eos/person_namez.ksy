meta:
  id: person_namez
  endian: be

seq:
  - id: name
    type: strz
    encoding: ASCII
  - id: surname
    type: strz
    encoding: UTF-16LE
  - id: data
    size-eos: true
