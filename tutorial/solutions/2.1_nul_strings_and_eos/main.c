#include <stdio.h>
#include <string.h>

#include "person_namez.h"

int main(int argc, char *argv[])
{
    person_namez_t record;
    person_namez_t record_copy;

    // must always init in C
    person_namez_init(&record);
    person_namez_init(&record_copy);

    // fill in record
    person_namez_set_name(&record, "Joe");
    person_namez_set_surname(&record, L"Smith");
    person_namez_set_data(&record, "\x01\x02\x03", 3);

    // serailize one record, parse into other record (aka a deep copy)
    size_t buffer_size = 0;
    char *buffer = person_namez_serialize_to_string(&record, &buffer_size);
    person_namez_parse_from_string(&record_copy, buffer, buffer_size);
    kpb_io_free(buffer);

    // print the copy
    printf("NAME   : %s\n", person_namez_name(&record_copy));
    printf("SURNAME: %S\n", person_namez_surname(&record_copy));
    if ( 0 == memcmp("\x01\x02\x03", person_namez_data(&record_copy), person_namez_data_size(&record_copy)))
    {
        printf("DATA   : match\n");
    }
    else
    {
        printf("DATA   : MISMATCH\n");
    }

    // must always cleanup your messages in C
    person_namez_fini(&record);
    person_namez_fini(&record_copy);
    return 0;
}
