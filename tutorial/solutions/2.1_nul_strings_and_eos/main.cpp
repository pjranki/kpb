#include <iostream>
#include <string.h>

#include "person_namez.hpp"

int main(int argc, char *argv[])
{
    PersonNamez record;
    PersonNamez record_copy;

    // fill in record
    record.set_name("Joe");
    record.set_surname(L"Smith");
    record.set_data(std::string("\x01\x02\x03", 3));

    // serailize one record, parse into other record (aka a deep copy)
    std::string buffer;
    record.SerializeToString(&buffer);
    record_copy.ParseFromString(buffer);

    // print the copy
    std::cout << "NAME   : " << record_copy.name() << std::endl;
    std::cout << "SURNAME: " << reinterpret_cast<const wchar_t *>(record_copy.surname().c_str()) << std::endl;
    if ( 0 == memcmp("\x01\x02\x03", record_copy.data().c_str(), record_copy.data().size()))
    {
        std::cout << "DATA   : match" << std::endl;
    }
    else
    {
        std::cout << "DATA   : MISMATCH" << std::endl;
    }

    return 0;
}
