meta:
  id: person
  endian: be

seq:
  - id: flags
    type: u2
    enum: flag
  - id: name
    type: strz
    encoding: ASCII
    if: '(flags.to_i & flag::name.to_i) != 0'
    -set:
      - id: flags
        value: 'flags.to_i | flag::name.to_i'
  - id: age
    type: u1
    if: '(flags.to_i & flag::age.to_i) != 0'
    -set:
      - id: flags
        value: 'flags.to_i | flag::age.to_i'
  - id: mobile
    type: strz
    encoding: ASCII
    if: '(flags.to_i & flag::mobile.to_i) != 0'
    -set:
      - id: flags
        value: 'flags.to_i | flag::mobile.to_i'

enums:
  flag:
    0x0001: name
    0x0002: age
    0x0010: mobile
