meta:
  id: protocol
  endian: be

seq:
  - id: protocol
    type: u1
    enum: ip_protocol

enums:
  ip_protocol:
    1: icmp
    6: tcp
    17: udp
