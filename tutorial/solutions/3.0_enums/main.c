#include <stdio.h>

#include "protocol.h"

int main(int argc, char *argv[])
{
    protocol_t msg;

    // must always init in C
    protocol_init(&msg);

    // lets try each enumerator
    for ( uint8_t e = 0; e < 20; e++ )
    {
        // set the protocol
        protocol_set_protocol(&msg, e);

        // switch-on enumerator
        switch(protocol_protocol(&msg))
        {
            case PROTOCOL_IP_PROTOCOL_ICMP:
                printf("%d: ICMP\n", protocol_protocol(&msg));
                break;
            case PROTOCOL_IP_PROTOCOL_UDP:
                printf("%d: UDP\n", protocol_protocol(&msg));
                break;
            case PROTOCOL_IP_PROTOCOL_TCP:
                printf("%d: TCP\n", protocol_protocol(&msg));
                break;
            default:
                printf("%d: (unknown)\n", protocol_protocol(&msg));
                break;
        }
    }

    // must always cleanup your messages in C
    protocol_fini(&msg);
    return 0;
}
