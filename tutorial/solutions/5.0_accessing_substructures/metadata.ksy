meta:
  id: metadata
  endian: le

seq:
  - id: max_name_len
    type: u4
  - id: child
    type: child
  - id: name
    size: max_name_len
    type: str
    encoding: ASCII

types:
  child:
    seq:
      - id: grand_child
        type: grand_child
      - id: name
        size: _parent.max_name_len
        type: str
        encoding: ASCII
  grand_child:
    seq:
      - id: name
        size: _root.max_name_len
        type: str
        encoding: ASCII
