add_executable(metadata_c_soln
    main.c
)
target_kpb_c(metadata_c_soln
    metadata.ksy  # generates metadata.c, metadata.h
)
target_link_libraries(metadata_c_soln
    kpb_io  # C runtime for KPB i.e. common C code for KPB
)

add_executable(metadata_cpp_soln
    main.cpp
)
target_kpb_cpp(metadata_cpp_soln
    metadata.ksy  # generates metadata.cpp, metadata.hpp
)
target_link_libraries(metadata_cpp_soln
    kpb_cpp_stl_io  # C++ STL runtime for KPB i.e. common C++ code for KPB
)
